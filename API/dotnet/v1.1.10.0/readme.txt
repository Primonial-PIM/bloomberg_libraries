Bloomberg data may be downloaded via the API into customized spreadsheets in accordance with DATAFEED ADDENDUM guidelines:

Desktop applications must only make local requests for data (centralized requests 
are only allowed via Server API) and must remain within the daily limits and monthly 
unique security limits.  

Please contact your Sales Representative for information on limits and application development.