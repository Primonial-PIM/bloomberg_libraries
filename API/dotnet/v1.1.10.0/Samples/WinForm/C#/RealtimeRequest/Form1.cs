/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	/// <summary> 
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnGetData;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.ComboBox cboSubMode;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListBox listboxSecurities;
		private System.Windows.Forms.ListBox listboxFields;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblDisplay;
		private System.Windows.Forms.Button btnStop;

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ToolTip directionsToolTip;
		private System.ComponentModel.IContainer components;
		private RequestForRealtime request = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.btnGetData = new System.Windows.Forms.Button();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.cboSubMode = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.listboxSecurities = new System.Windows.Forms.ListBox();
			this.listboxFields = new System.Windows.Forms.ListBox();
			this.label3 = new System.Windows.Forms.Label();
			this.lblDisplay = new System.Windows.Forms.Label();
			this.btnStop = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.directionsToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// btnGetData
			// 
			this.btnGetData.Location = new System.Drawing.Point(392, 124);
			this.btnGetData.Name = "btnGetData";
			this.btnGetData.Size = new System.Drawing.Size(72, 32);
			this.btnGetData.TabIndex = 0;
			this.btnGetData.Text = "Get Data";
			this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 173);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(472, 16);
			this.statusBar1.TabIndex = 1;
			// 
			// cboSubMode
			// 
			this.cboSubMode.Location = new System.Drawing.Point(8, 132);
			this.cboSubMode.Name = "cboSubMode";
			this.cboSubMode.Size = new System.Drawing.Size(160, 21);
			this.cboSubMode.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 116);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "Subscription Mode:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 17);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(144, 16);
			this.label2.TabIndex = 4;
			this.label2.Text = "Securities:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// listboxSecurities
			// 
			this.listboxSecurities.Location = new System.Drawing.Point(8, 36);
			this.listboxSecurities.Name = "listboxSecurities";
			this.listboxSecurities.Size = new System.Drawing.Size(160, 69);
			this.listboxSecurities.TabIndex = 5;
			// 
			// listboxFields
			// 
			this.listboxFields.Location = new System.Drawing.Point(184, 36);
			this.listboxFields.Name = "listboxFields";
			this.listboxFields.Size = new System.Drawing.Size(200, 69);
			this.listboxFields.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(184, 18);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(144, 16);
			this.label3.TabIndex = 6;
			this.label3.Text = "Fields:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDisplay
			// 
			this.lblDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblDisplay.Location = new System.Drawing.Point(184, 132);
			this.lblDisplay.Name = "lblDisplay";
			this.lblDisplay.Size = new System.Drawing.Size(200, 24);
			this.lblDisplay.TabIndex = 9;
			this.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnStop
			// 
			this.btnStop.Enabled = false;
			this.btnStop.Location = new System.Drawing.Point(392, 84);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(72, 32);
			this.btnStop.TabIndex = 10;
			this.btnStop.Text = "Stop";
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(392, 36);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(24, 20);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 11;
			this.pictureBox1.TabStop = false;
			// 
			// directionsToolTip
			// 
			this.directionsToolTip.AutoPopDelay = 5000;
			this.directionsToolTip.InitialDelay = 100;
			this.directionsToolTip.ReshowDelay = 100;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(472, 189);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.btnStop);
			this.Controls.Add(this.lblDisplay);
			this.Controls.Add(this.listboxFields);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.listboxSecurities);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cboSubMode);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.btnGetData);
			this.Name = "Form1";
			this.Text = "Realtime Request using API for .NET";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private int _ReplyCt;
		private string[] _securities = null;
		private Field[] _fields = null;
		private string _selSecurity = ""; 
		private Field _selField = null;
		private Guid _reqId;
		private FieldTable ftbl = null;

		private void Form1_Load(object sender, System.EventArgs e)
		{
			// Directions on how to run sample available as tooltip on pictureBox question-mark.
			directionsToolTip.SetToolTip(this.pictureBox1, 
				"Instructions: Select one field/security pairing and then click the Get Data button.");

			// Now, startup the MarketDataAdapter.
			// This will cause several things to happen:
			//   - Bloomberg configuration information will be acquired from the 
			//	   environment (Windows Registry, config files, the Bloomberg terminal, etc)
			//	 - A connection to the local bbcomm will be established.
			//	 - BBCOMM protocol handshaking will be initiated.
			//	 - Adapter resources will be allocated.
			try
			{
				MarketDataAdapter.Startup();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Close();
			}

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapter.Startup method
			ftbl = MarketDataAdapter.FieldTable;

			_fields = new Field[]
				{ftbl["LAST_PRICE"], ftbl["BID"], ftbl["ASK"], ftbl["Volume"], ftbl["LAST_TRADE"]};

			_securities = new string[]
				{"MSFT US Equity", "IBM US Equity", "GOOG US Equity", "SIRI US Equity", "CSCO US Equity"};

			// Subscribe for any status and reply messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnRealtime_StatusEvent);
			MarketDataAdapter.ReplyEvent += new ReplyEventHandler(OnRealtime_ReplyEvent);

			// Populate security listbox
			foreach (string s in _securities)
			{
				listboxSecurities.Items.Add(s);
			}

			// Populate field listbox
			foreach (Field f in _fields)
			{
				listboxFields.Items.Add(f.Mnemonic);
			}

			listboxSecurities.SetSelected(0, true);
			listboxFields.SetSelected(0, true);

			// Populate SubscriptionMode combobox with enumerator values
			cboSubMode.DataSource = Enum.GetValues(typeof(SubscriptionMode));

			statusBar1.ShowPanels = false;
		}

		private void OnRealtime_ReplyEvent(Reply reply)
		{			
			// Check to see if we will need to marshall Reply to the Form's thread (we will).
			// Then marshall back onto the Form's thread via the message pump.	
			if(InvokeRequired)
			{
				Invoke(new ReplyEventHandler(OnRealtime_ReplyEvent), new object[]{reply});
				return;
			}
			else
			{
				if(reply.ReplyError != null)
				{
					statusBar1.Text = "Reply Error has occurred: " + 
						reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description;
					return;
				}
			}

			try
			{
                // Loop through each security
				foreach(SecurityDataItem sdi in reply.GetSecurityDataItems())
				{
					// Loop through each field
					foreach(FieldDataItem fdi in sdi.FieldsData)
					{
						// Loop through each data point
						foreach(DataPoint dp in fdi.DataPoints)
						{
							if(dp.IsError)
							{
								statusBar1.Text = dp.ReplyError.DisplayName;
							}
							else
							{
								lblDisplay.Text = _selField + ": " + dp.Value.ToString();
								statusBar1.Text = "Receiving data...";
							}
						}
					}
				}

				++_ReplyCt;
				statusBar1.Text = "Reply Event fired " + _ReplyCt.ToString() + " times";
			}
			catch (Exception ex)
			{
				statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message;
			}
		}

		private void OnRealtime_StatusEvent(StatusCode status, string description)
		{
			try
			{
				if(InvokeRequired)
					Invoke(new StatusEventHandler(OnRealtime_StatusEvent), new object[]{status, description});
				else
				{
					statusBar1.Text = "Status: " + status + "  Desc: " + description;
				}
			}
			catch (Exception ex)
			{
				statusBar1.Text = "Error has occurred in StatusEvent handler: " + ex.Message;
			}
		}

		private void btnGetData_Click(object sender, System.EventArgs e)
		{
			_ReplyCt = 0;

			// Check that a security has been selected
			if (listboxSecurities.SelectedIndex != -1)
				_selSecurity = listboxSecurities.SelectedItem.ToString();

			// Check that a field has been selected
			if (listboxFields.SelectedIndex != -1)
				_selField = _fields[listboxFields.SelectedIndex];

			statusBar1.Text = "Preparing request...";

			// Issue realtime request
			request = new RequestForRealtime();
			
			// Add field and security to RequestForRealtime object
            request.Fields.Add(_selField);
            request.Securities.Add(_selSecurity);

			// Set subscription mode
			request.SubscriptionMode = (SubscriptionMode)cboSubMode.SelectedValue;

			// To monitor realtime requests, the Monitor property must be set to true (default)
			request.Monitor = true;

			statusBar1.Text = "Submitting request for " + _selSecurity;

			_reqId = MarketDataAdapter.SendRequest(request);

			// Disable Get Data button and enable Stop button
			btnGetData.Enabled = false;
			btnStop.Enabled = true;
		}

		private void btnStop_Click(object sender, System.EventArgs e)
		{
			// Pass guid that is returned from SendRequest() call
			MarketDataAdapter.CancelRequest(_reqId);
			btnGetData.Enabled = true;
			btnStop.Enabled = false;
			statusBar1.Text = "Request has been cancelled by user.";
		}
	}
}
