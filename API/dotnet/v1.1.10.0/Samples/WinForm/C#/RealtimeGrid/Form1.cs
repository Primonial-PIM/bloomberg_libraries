/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;

namespace Bloomberg.Api.Samples
{
	/// <summary>
	/// This application will display a fixed set of real time Bloomberg 
	/// data fields (Last Price, Volume, Bid, Ask, Open, Close, etc)
	/// for a set of Securities that has been dragged from another container
	/// (Bloomberg terminal, Excel)
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

#region Declarations: Private instance properties

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		// The DataGrid for displaying real time updates.
		private System.Windows.Forms.DataGrid grdMonitor;

		// The StatusBar will be displayed at the bottom of the form it will contain a single panel
		private System.Windows.Forms.StatusBar statusBar1;
		// The status bar panel that will display the status messages returned from MarketDataAdapter
		private StatusBarPanel		_sbpAdapterStatus;

		// The list of fields will be populated and validated after we have started the MarketDataAdapter
		private Field[]				_fieldList; 
		// DataSet and DataTable Used to bind the data returned from MarketDataAdpater.ReplyEvent 
		// to the DataGrid (grdMonitor)
		private DataSet				_realTimeDataSet;
		private DataTable			_realTimeDataTable;

		// This correlation id will be incremented for each row added to the grid.
		// The current values will be passed as a "State" variable to the Request object 
		// that is passed to the MarketDataAdapter.SendRequest method. This "State" variable
		// will be returned by MarketDataAdapter.ReplyEvent in the Reply object. Logic
		// in the ReplyEvent handler (see MarketDataAdapter_ReplyEvent below) will
		// map this value (via the Hashtable _htRows) back to a particular row on the grid
		// which will then be updated with the new values.
		private int					_blpDataCorrelationId;
		private Hashtable			_htRows;
#endregion

#region Constructor

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			// Create the hashtable for mapping a Reply to a row on the grid.
			_htRows = new Hashtable();
		}
#endregion

#region IDisposable implementation

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{

			if( disposing )
			{
				// Cleanly shutdown the Adaper, this is not 
				// really necessary in this context as the Application is 
				// exiting.
				MarketDataAdapter.Shutdown();

				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
#endregion

#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grdMonitor = new System.Windows.Forms.DataGrid();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			((System.ComponentModel.ISupportInitialize)(this.grdMonitor)).BeginInit();
			this.SuspendLayout();
			// 
			// grdMonitor
			// 
			this.grdMonitor.AllowDrop = true;
			this.grdMonitor.AlternatingBackColor = System.Drawing.Color.LightGray;
			this.grdMonitor.BackColor = System.Drawing.Color.Gainsboro;
			this.grdMonitor.BackgroundColor = System.Drawing.Color.Silver;
			this.grdMonitor.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.grdMonitor.CaptionBackColor = System.Drawing.Color.LightSteelBlue;
			this.grdMonitor.CaptionForeColor = System.Drawing.Color.MidnightBlue;
			this.grdMonitor.CaptionText = "Current Market ";
			this.grdMonitor.CausesValidation = false;
			this.grdMonitor.DataMember = "";
			this.grdMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.grdMonitor.FlatMode = true;
			this.grdMonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grdMonitor.ForeColor = System.Drawing.Color.Black;
			this.grdMonitor.GridLineColor = System.Drawing.Color.DimGray;
			this.grdMonitor.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None;
			this.grdMonitor.HeaderBackColor = System.Drawing.Color.MidnightBlue;
			this.grdMonitor.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grdMonitor.HeaderForeColor = System.Drawing.Color.White;
			this.grdMonitor.LinkColor = System.Drawing.Color.MidnightBlue;
			this.grdMonitor.Location = new System.Drawing.Point(0, 0);
			this.grdMonitor.Name = "grdMonitor";
			this.grdMonitor.ParentRowsBackColor = System.Drawing.Color.DarkGray;
			this.grdMonitor.ParentRowsForeColor = System.Drawing.Color.Black;
			this.grdMonitor.SelectionBackColor = System.Drawing.Color.CadetBlue;
			this.grdMonitor.SelectionForeColor = System.Drawing.Color.White;
			this.grdMonitor.Size = new System.Drawing.Size(864, 462);
			this.grdMonitor.TabIndex = 0;
			this.grdMonitor.DragDrop += new System.Windows.Forms.DragEventHandler(this.grdMonitor_DragDrop);
			this.grdMonitor.DragEnter += new System.Windows.Forms.DragEventHandler(this.grdMonitor_DragEnter);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 440);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.ShowPanels = true;
			this.statusBar1.Size = new System.Drawing.Size(864, 22);
			this.statusBar1.TabIndex = 1;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(864, 462);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.grdMonitor);
			this.Name = "Form1";
			this.Tag = "";
			this.Text = "Realtime Grid Example: Drop a list (for example: \"MOST<GO>\") on grid";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.grdMonitor)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

#region FormLoad, Initialization
		private void Form1_Load(object sender, System.EventArgs e)
		{

			// Create the StatusBarPanel for displaying adapter status
			_sbpAdapterStatus = new StatusBarPanel();
			_sbpAdapterStatus.BorderStyle = StatusBarPanelBorderStyle.Sunken;
			_sbpAdapterStatus.Text = "MarketDataAdapter uninitialized";
			_sbpAdapterStatus.AutoSize = StatusBarPanelAutoSize.Contents;
			// Add the newly created panel to the StatusBar
			statusBar1.Panels.Add(_sbpAdapterStatus);

			// Create the DataTable that will contain the data elements
			// returned from the MarketdDataAdapter.
			_realTimeDataTable = new DataTable("Realtime");
			_realTimeDataTable.DefaultView.ApplyDefaultSort = true;

			// The first column in the table (and hence the grid) will contain 
			// the Security identifier.
			_realTimeDataTable.Columns.Add("Security");

			// The grid must be bound to a DataSet (that will contain a single DataTable)
			_realTimeDataSet = new DataSet("DSMASTER");
			// Add our DataTable to the DataSet
			_realTimeDataSet.Tables.Add(_realTimeDataTable);

			// Bind the grid's DataSource to our DataSet 
			grdMonitor.DataSource = _realTimeDataSet;
			// Select the DataTable that we created above for binding to the grid.
			grdMonitor.DataMember = "Realtime";
	
			// Setup our status event handler
			MarketDataAdapter.StatusEvent +=new StatusEventHandler(MarketDataAdapter_StatusEvent);
			
			// Now startup the MarketDataAdapter.
			// This will cause several things to happen:
			// - Bloomberg configuration information will be acquired from the 
			//   environment (Windows Registry, config files, the Bloomberg terminal, etc)
			// - A connection to the local bbcomm will be established.
			// - BBCOMM protocol handshaking will be initiated.
			// - Adapter resources will be allocated.
			try
			{
				// The TimerSyncResponseInterceptor ensures that the status and reply
				// event handlers are called on the user interface threads at a
				// specified time interval. We have set it at 1 second (1000 milliseconds) 
				MarketDataAdapter.Startup(new TimerSyncResponseInterceptor(1000));
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, 
					"MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				this.Close();
			}

			// Setup our reply event handler
			MarketDataAdapter.ReplyEvent +=new ReplyEventHandler(MarketDataAdapter_ReplyEvent);
	
			// After MarketDataAdapter.Startup has completed successfully, appropriate metadata 
			// (bbfields.tbl, etc, ) information will have been loaded, so we can validate 
			// the fields that we want to add to our fieldList against the metatdata.
			// MarketDataAdapter.CreateValidField will throw an exception if you 
			// try to add an invalid field.
			try
			{
				_fieldList =  new Field[] 
				{
					MarketDataAdapter.FieldTable["Last Price"], 
					MarketDataAdapter.FieldTable["Volume"], 
					MarketDataAdapter.FieldTable["Bid"], 
					MarketDataAdapter.FieldTable["Ask"], 
					MarketDataAdapter.FieldTable["Time"], 
					MarketDataAdapter.FieldTable["High Tdy"], 
					MarketDataAdapter.FieldTable["Low Tdy"], 
					MarketDataAdapter.FieldTable["Open Tdy"] 
				};
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, 
					"MarketDataAdapter.CreateValidField", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				this.Close();
			}

			// Add a column to the DataTable (and hence the grid) for each Field in the list above.
			// We could do add some nice UI touches here, such as adjusting the size of 
			// each column to the size of the Field Description...
			foreach(Field fld in _fieldList)
				_realTimeDataTable.Columns.Add(new DataColumn(fld.Description, System.Type.GetType("System.String")));
		}
#endregion

#region Drag/Drop event handlers
		// This handler will be called as the cursor enters our DataGrid's window.
		// It gives us the opportunit to indicate the format of the data 
		// we can handle. In this case we want to consume text data only.
		private void grdMonitor_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if(e.Data.GetDataPresent(DataFormats.Text))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;		
		}

		// This handler will be called when a Data object is dropped on our DataGrid's window.
		// At this point we will have to parse the CR/LF delimited text that is dropped on us.
		// In a real application we would probably want to do more validation of the input...
		private void grdMonitor_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			// Get the entire text object that has been dropped on us.
			string tmp = e.Data.GetData(DataFormats.Text).ToString();

			// Tokenize the string into what (we hope) are Security strings
			char[] sep = {'\r', '\n', '\t'};
			string [] words = tmp.Split(sep);

			AddSecuritiesToGrid(words);		
		}
#endregion

#region Dropped securities list parser, Bloomberg.Api Request builder
		private void AddSecuritiesToGrid(string [] secs)
		{
			try
			{
				// Cancel any outstanding requests
//				MarketDataAdapter.CancelAllRequests();
				// Clear down the row mapper and data table
				_htRows.Clear();
				_realTimeDataTable.Clear();
				_realTimeDataTable.Rows.Clear();

			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Current Market: DesubscribeAll", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}

			try
			{
				// Now parse the security list, add a row to the grid for each security, and
				// subscribe via the MarketDataAdapter
				MarketDataAdapterSubscribe(secs);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Current Market: AddRequests", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}

		}

		private void MarketDataAdapterSubscribe(string[] secs)
		{
			if (secs.Length == 0)
				return;
	
			// Create a temporary container for all the requests.
			ArrayList aReqs = new ArrayList();

			// This is an optimization - turns off notifications and index maintenance
			// while loading the table
			_realTimeDataTable.BeginLoadData();

			// Iterate through the input list of securities.
			foreach(string sec in secs)
			{
				// Trim leading and trailing whitespace.
				string s = sec.Trim();
				// Check to see if we have anything left.
				if (s.Length > 0)
				{
					// Create a new row in the DataTable
					DataRow realTimeDataRow = _realTimeDataTable.NewRow();
					// Add the Security to the "Security" column of the new row.
					realTimeDataRow["Security"] = s;
					// Add the new row to the Datatable
					_realTimeDataTable.Rows.Add(realTimeDataRow);
					// Add the new row to the hashtable - and use the current value
					// of the correlation id as a key.
					_htRows.Add(_blpDataCorrelationId, realTimeDataRow);
					// Create a new Request object
					RequestForRealtime rtreq = new RequestForRealtime();
					// Add the security, this will also validate the security string.
					// An invalid string will generate an exception that will be
					// caught by the caller of this method.
					rtreq.Securities.Add(s);
					// Add all of the fields in the fieldList to the Request. These
					// fields mnemonics will also be validated against the currently
					// loaded (it was loaded when we called MarketDataAdapter.Startup) field table.
					rtreq.Fields.AddRange(_fieldList);
					// Of course, we want to monitor the real time updates
					rtreq.Monitor = true;
					// All replies will be returned by field.
					rtreq.SubscriptionMode = SubscriptionMode.ByField;
					// Set the Request state object to the curent value of 
					// the correlation id. This will be returned with the 
					// reply and we will use it in the MarketDataAdapter_ReplyEvent (below)
					// to quickly map a given reply to the correct row in the DataTable
					rtreq.State = _blpDataCorrelationId;
					// Add the request to the temporary storage
					aReqs.Add(rtreq);
					// Increment the correlation id
					_blpDataCorrelationId++;	
				}		

			}
			// We're done modifying the data table.
			_realTimeDataTable.EndLoadData();	

			// Cast all requests to the base (IRequestCommon) 
			// and send all the requests to the Adapter in one shot
			MarketDataAdapter.SendRequest((IRequestBase[])aReqs.ToArray(typeof(IRequestBase)));

		}
#endregion

#region Bloomberg.Api event handlers
		private void MarketDataAdapter_ReplyEvent(Reply reply)
		{
			if (reply.ReplyError != null)
				return;

			try
			{
				// Once again turn off notifications and index maintenance
				// while loading the table
				_realTimeDataTable.BeginLoadData();

				// Look up the row using the value in Request.State as a key.
				// This value was generated from the correlation id we set in 
				// MarketDataAdapterSubscribe above
				int state = (int)reply.Request.State;
				DataRow rowMarket = (DataRow)_htRows[(int)reply.Request.State];

				if (rowMarket != null)
				{	
					// Get the Securities Collection
					SecuritiesDataCollection sdcol = reply.GetSecurityDataItems();

					// Just a sanity check - since each Request contains
					// only 1 Security - we should never get back more than
					// 1 Security in a reply.
					System.Diagnostics.Debug.Assert (sdcol.Count == 1);

					// Get the Security Mnemonic from the appropriate cell on the grid.
					string securityMnemonic = rowMarket[_realTimeDataTable.Columns["Security"]].ToString();
					
					SecurityDataItem sdi = null;

					try
					{
						// Find that security in the SecuritiesDataCollection, there should be 
						// only one security in the Collection.
						sdi = sdcol[securityMnemonic];
					}
					catch(Exception)
					{
						sdi = null;
					}

					// We should always find the SecurityDataItem!
					if (sdi != null)
					{
						// There may be data for several of the fields
						foreach(FieldDataItem fdi in sdi.FieldsData)
						{
							DataPoint dp = fdi.DataPoints[0];

							if (dp != null)
							{
								string val = null;
								// Get either the value or an error (one or the other will be filled)
								if (dp.IsError == true)
									val = dp.ReplyError.DisplayName;
								else 
									val = dp.Value.ToString();
								
								if (val != null)
								{					
									// Iterate through the list of requested fields to
									// get to the column in the DataTable
									for(int i = 0; i < _fieldList.Length; i++)
									{
										if (fdi.Field.Equals(_fieldList[i]))
										{
											// Don't bother to update if the new value is equal to 
											// the value that is already being displayed.
											if (rowMarket[i + 1] == System.DBNull.Value || val.CompareTo(rowMarket[i + 1].ToString()) != 0)
												rowMarket[i + 1] = val;
											break;
										}
									}
								}
							}
						}
					}
				}
				// We're done modifying the table.
				_realTimeDataTable.EndLoadData();

			}
			catch(Exception)
			{
				throw;
			}

		}

		private void MarketDataAdapter_StatusEvent(StatusCode status, string description)
		{
			// Display the status message on the StatusBarPanel
			_sbpAdapterStatus.Text = description;

		}
#endregion
	}
}
