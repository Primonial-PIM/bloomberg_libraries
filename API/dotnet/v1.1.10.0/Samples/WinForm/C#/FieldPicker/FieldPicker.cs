/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/
using System;
using System.Reflection;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	/// <summary>
	/// Summary description for FieldPicker.
	/// </summary>
	internal sealed class FieldPicker : System.Windows.Forms.UserControl
	{
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblFindWhat;
        private System.Windows.Forms.TextBox txtSearch;

        private DataTable _fields;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFilter;
		private System.Windows.Forms.CheckBox cbSearchDesc;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DataGrid gridFields;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtExtendedDescription;
		private System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Splitter splitter1;
		private FieldTable _fieldTable;

		public delegate void AddFieldsHandler(Field[] fields);
		public event AddFieldsHandler AddFields;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FieldPicker():this(FILTER.Historical)
		{
		}
        public enum FILTER { None, Historical, Realtime, Static, BulkData, Intraday, Deprecated};
		public FieldPicker(FILTER filter)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_fieldTable = new FieldTable();
			_fieldTable.LoadAll();
			_fields = _fieldTable.GetDataTable();

            cbFilter.Items.Add(FILTER.None.ToString());
            cbFilter.Items.Add(FILTER.Historical.ToString());
            cbFilter.Items.Add(FILTER.Realtime.ToString());
            cbFilter.Items.Add(FILTER.Static.ToString());
			cbFilter.Items.Add(FILTER.Intraday.ToString());
            cbFilter.Items.Add(FILTER.BulkData.ToString());
			cbFilter.Items.Add(FILTER.Deprecated.ToString());
            cbFilter.SelectedItem = filter.ToString();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSearch = new System.Windows.Forms.Button();
			this.lblFindWhat = new System.Windows.Forms.Label();
			this.txtSearch = new System.Windows.Forms.TextBox();
			this.cbFilter = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cbSearchDesc = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.gridFields = new System.Windows.Forms.DataGrid();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label3 = new System.Windows.Forms.Label();
			this.txtExtendedDescription = new System.Windows.Forms.TextBox();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnAdd = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridFields)).BeginInit();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnSearch
			// 
			this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSearch.Location = new System.Drawing.Point(384, 8);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(121, 23);
			this.btnSearch.TabIndex = 2;
			this.btnSearch.Text = "Find";
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// lblFindWhat
			// 
			this.lblFindWhat.Location = new System.Drawing.Point(0, 8);
			this.lblFindWhat.Name = "lblFindWhat";
			this.lblFindWhat.TabIndex = 0;
			this.lblFindWhat.Text = "Mnemonic:";
			// 
			// txtSearch
			// 
			this.txtSearch.AcceptsReturn = true;
			this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtSearch.Location = new System.Drawing.Point(104, 8);
			this.txtSearch.MaxLength = 32;
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(272, 22);
			this.txtSearch.TabIndex = 1;
			this.txtSearch.Text = "";
			this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
			// 
			// cbFilter
			// 
			this.cbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbFilter.Location = new System.Drawing.Point(384, 64);
			this.cbFilter.Name = "cbFilter";
			this.cbFilter.Size = new System.Drawing.Size(121, 24);
			this.cbFilter.TabIndex = 5;
			this.cbFilter.SelectionChangeCommitted += new System.EventHandler(this.cbFilter_SelectionChangeCommitted);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(304, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 23);
			this.label1.TabIndex = 4;
			this.label1.Text = "Filter by:";
			// 
			// cbSearchDesc
			// 
			this.cbSearchDesc.Location = new System.Drawing.Point(104, 40);
			this.cbSearchDesc.Name = "cbSearchDesc";
			this.cbSearchDesc.Size = new System.Drawing.Size(208, 24);
			this.cbSearchDesc.TabIndex = 3;
			this.cbSearchDesc.Text = "Include description in search";
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.Controls.Add(this.splitter1);
			this.panel1.Controls.Add(this.gridFields);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Location = new System.Drawing.Point(0, 104);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(504, 320);
			this.panel1.TabIndex = 13;
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter1.Location = new System.Drawing.Point(0, 157);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(504, 3);
			this.splitter1.TabIndex = 2;
			this.splitter1.TabStop = false;
			// 
			// gridFields
			// 
			this.gridFields.AlternatingBackColor = System.Drawing.Color.Lavender;
			this.gridFields.BackColor = System.Drawing.Color.WhiteSmoke;
			this.gridFields.BackgroundColor = System.Drawing.Color.LightGray;
			this.gridFields.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.gridFields.CaptionBackColor = System.Drawing.Color.LightSteelBlue;
			this.gridFields.CaptionForeColor = System.Drawing.Color.MidnightBlue;
			this.gridFields.DataMember = "";
			this.gridFields.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridFields.FlatMode = true;
			this.gridFields.Font = new System.Drawing.Font("Tahoma", 8F);
			this.gridFields.ForeColor = System.Drawing.Color.MidnightBlue;
			this.gridFields.GridLineColor = System.Drawing.Color.Gainsboro;
			this.gridFields.HeaderBackColor = System.Drawing.Color.MidnightBlue;
			this.gridFields.HeaderFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.gridFields.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
			this.gridFields.LinkColor = System.Drawing.Color.Teal;
			this.gridFields.Location = new System.Drawing.Point(0, 0);
			this.gridFields.Name = "gridFields";
			this.gridFields.ParentRowsBackColor = System.Drawing.Color.Gainsboro;
			this.gridFields.ParentRowsForeColor = System.Drawing.Color.MidnightBlue;
			this.gridFields.PreferredColumnWidth = 85;
			this.gridFields.PreferredRowHeight = 20;
			this.gridFields.ReadOnly = true;
			this.gridFields.RowHeaderWidth = 25;
			this.gridFields.SelectionBackColor = System.Drawing.Color.CadetBlue;
			this.gridFields.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
			this.gridFields.Size = new System.Drawing.Size(504, 160);
			this.gridFields.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.txtExtendedDescription);
			this.panel2.Controls.Add(this.txtDescription);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.btnAdd);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 160);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(504, 160);
			this.panel2.TabIndex = 0;
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label3.Location = new System.Drawing.Point(216, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(184, 23);
			this.label3.TabIndex = 1;
			this.label3.Text = "Extended Description:";
			// 
			// txtExtendedDescription
			// 
			this.txtExtendedDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtExtendedDescription.BackColor = System.Drawing.Color.LightGray;
			this.txtExtendedDescription.ForeColor = System.Drawing.Color.MidnightBlue;
			this.txtExtendedDescription.Location = new System.Drawing.Point(216, 33);
			this.txtExtendedDescription.Multiline = true;
			this.txtExtendedDescription.Name = "txtExtendedDescription";
			this.txtExtendedDescription.ReadOnly = true;
			this.txtExtendedDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtExtendedDescription.Size = new System.Drawing.Size(288, 55);
			this.txtExtendedDescription.TabIndex = 3;
			this.txtExtendedDescription.Text = "";
			// 
			// txtDescription
			// 
			this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtDescription.BackColor = System.Drawing.Color.LightGray;
			this.txtDescription.ForeColor = System.Drawing.Color.MidnightBlue;
			this.txtDescription.Location = new System.Drawing.Point(0, 33);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.ReadOnly = true;
			this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtDescription.Size = new System.Drawing.Size(200, 55);
			this.txtDescription.TabIndex = 2;
			this.txtDescription.Text = "";
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.label2.Location = new System.Drawing.Point(0, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(168, 23);
			this.label2.TabIndex = 0;
			this.label2.Text = "Description:";
			// 
			// btnAdd
			// 
			this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btnAdd.Location = new System.Drawing.Point(176, 120);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(152, 23);
			this.btnAdd.TabIndex = 4;
			this.btnAdd.Text = "Add Selected Field(s)";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// FieldPicker
			// 
			this.BackColor = System.Drawing.Color.LightGray;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.cbSearchDesc);
			this.Controls.Add(this.txtSearch);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbFilter);
			this.Controls.Add(this.lblFindWhat);
			this.Controls.Add(this.btnSearch);
			this.Name = "FieldPicker";
			this.Size = new System.Drawing.Size(504, 432);
			this.Load += new System.EventHandler(this.FieldPicker_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridFields)).EndInit();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

        private Field[] _selectedFields;
        public Field[] SelectedFields
        {
            get { return _selectedFields; }
        }
        private void btnSearch_Click(object sender, System.EventArgs e)
        {
			BuildFilter();
        }

        private void FieldPicker_Load(object sender, System.EventArgs e)
        {
            gridFields.DataSource = _fields.DefaultView;
			txtDescription.DataBindings.Clear();
            txtDescription.DataBindings.Add("Text", _fields.DefaultView, "Description");
			txtExtendedDescription.DataBindings.Clear();
            txtExtendedDescription.DataBindings.Add("Text", _fields.DefaultView, "ExtendedDescription");
			_selectedFields = new Field[0];
			BuildFilter();
        }
		public ArrayList GetSelectedRows(DataGrid dg)
		{
			ArrayList al = new ArrayList();
			CurrencyManager cm = (CurrencyManager)this.BindingContext[dg.DataSource, dg.DataMember];
			DataView dv = (DataView)cm.List;
			if(cm.Position >= 0 && cm.Current != null)
				al.Add(((DataRowView)cm.Current).Row["Mnemonic"]);
			for(int i = 0; i < dv.Count; ++i)
			{
				if(dg.IsSelected(i) && dv[i] != cm.Current)
					al.Add(dv[i].Row["Mnemonic"]);
				
			}
			return al;
		}
        private void cbFilter_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
			BuildFilter();
        }
		private void BuildFilter()
		{
			string filter ="";
			if(cbFilter.SelectedItem.ToString().Equals("None"))
				filter = "";
			else
				filter = cbFilter.SelectedItem + " = True";
			if(txtSearch.Text.Trim() != string.Empty)
			{
				if(_fields.DefaultView.RowFilter != string.Empty)
				{
					if(filter.Length > 0)
						filter += " AND (Mnemonic LIKE '*" + txtSearch.Text.Trim() +"*'";
					else
						filter += "(Mnemonic LIKE '*" + txtSearch.Text.Trim() +"*'";
				}
				else 
					filter = "(Mnemonic LIKE '*" + txtSearch.Text.Trim() +"*'";
				if(cbSearchDesc.Checked)
					filter += " OR ExtendedDescription LIKE '*" + txtSearch.Text.Trim() +"*'";
				
				filter += ")";
			}
			_fields.DefaultView.RowFilter = filter;
			if(_fields.DefaultView.Count > 0)
				gridFields.Select(0);
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			ArrayList selectedRows = GetSelectedRows(gridFields);
			_selectedFields = new Field[selectedRows.Count];
			for(int row =0; row < _selectedFields.Length; ++row)
				_selectedFields[row] = _fieldTable[(string)selectedRows[row]];

			if(AddFields != null)
				AddFields(SelectedFields);
		}

		private void txtSearch_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Enter)
			{
				BuildFilter();
			}
		}
	}
}
