/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/
using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	/// <summary>
	/// Summary description for FieldsForm.
	/// </summary>
	[DesignTimeVisible(false)]
	internal sealed class FieldsForm : System.Windows.Forms.Form
	{
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.PropertyGrid gridFields;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.ListBox lbFields;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
		private Bloomberg.Api.Samples.FieldPicker fieldPicker1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuFileSave;
		private System.Windows.Forms.MenuItem menuFileExit;
		private System.Windows.Forms.SaveFileDialog saveFieldsDlg;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FieldsForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FieldsForm));
			this.btnOK = new System.Windows.Forms.Button();
			this.gridFields = new System.Windows.Forms.PropertyGrid();
			this.lbFields = new System.Windows.Forms.ListBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.btnUp = new System.Windows.Forms.Button();
			this.btnDown = new System.Windows.Forms.Button();
			this.fieldPicker1 = new Bloomberg.Api.Samples.FieldPicker();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuFileSave = new System.Windows.Forms.MenuItem();
			this.menuFileExit = new System.Windows.Forms.MenuItem();
			this.saveFieldsDlg = new System.Windows.Forms.SaveFileDialog();
			this.SuspendLayout();
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.Location = new System.Drawing.Point(664, 535);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(96, 32);
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// gridFields
			// 
			this.gridFields.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.gridFields.CommandsBackColor = System.Drawing.Color.LightGray;
			this.gridFields.CommandsVisibleIfAvailable = true;
			this.gridFields.HelpVisible = false;
			this.gridFields.LargeButtons = false;
			this.gridFields.LineColor = System.Drawing.SystemColors.ScrollBar;
			this.gridFields.Location = new System.Drawing.Point(312, 32);
			this.gridFields.Name = "gridFields";
			this.gridFields.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this.gridFields.Size = new System.Drawing.Size(576, 443);
			this.gridFields.TabIndex = 9;
			this.gridFields.Text = "PropertyGrid";
			this.gridFields.ToolbarVisible = false;
			this.gridFields.ViewBackColor = System.Drawing.SystemColors.Window;
			this.gridFields.ViewForeColor = System.Drawing.SystemColors.WindowText;
			// 
			// lbFields
			// 
			this.lbFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.lbFields.BackColor = System.Drawing.SystemColors.Window;
			this.lbFields.ItemHeight = 16;
			this.lbFields.Location = new System.Drawing.Point(24, 32);
			this.lbFields.Name = "lbFields";
			this.lbFields.Size = new System.Drawing.Size(232, 404);
			this.lbFields.TabIndex = 7;
			this.lbFields.SelectedIndexChanged += new System.EventHandler(this.lbFields_SelectedIndexChanged);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(784, 535);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(96, 32);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnRemove
			// 
			this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnRemove.Location = new System.Drawing.Point(160, 479);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(96, 32);
			this.btnRemove.TabIndex = 1;
			this.btnRemove.Text = "Remove";
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnAdd.Location = new System.Drawing.Point(24, 479);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(96, 32);
			this.btnAdd.TabIndex = 0;
			this.btnAdd.Text = "Hide <<";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Location = new System.Drawing.Point(24, 519);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(864, 3);
			this.label1.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 8);
			this.label2.Name = "label2";
			this.label2.TabIndex = 6;
			this.label2.Text = "Members";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(312, 8);
			this.label3.Name = "label3";
			this.label3.TabIndex = 8;
			this.label3.Text = "Properties";
			// 
			// btnUp
			// 
			this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
			this.btnUp.Location = new System.Drawing.Point(264, 32);
			this.btnUp.Name = "btnUp";
			this.btnUp.Size = new System.Drawing.Size(32, 40);
			this.btnUp.TabIndex = 4;
			this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
			// 
			// btnDown
			// 
			this.btnDown.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.Image")));
			this.btnDown.Location = new System.Drawing.Point(264, 96);
			this.btnDown.Name = "btnDown";
			this.btnDown.Size = new System.Drawing.Size(32, 40);
			this.btnDown.TabIndex = 5;
			this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
			// 
			// fieldPicker1
			// 
			this.fieldPicker1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.fieldPicker1.BackColor = System.Drawing.Color.LightGray;
			this.fieldPicker1.Location = new System.Drawing.Point(312, 8);
			this.fieldPicker1.Name = "fieldPicker1";
			this.fieldPicker1.Size = new System.Drawing.Size(576, 472);
			this.fieldPicker1.TabIndex = 11;
			this.fieldPicker1.AddFields += new Bloomberg.Api.Samples.FieldPicker.AddFieldsHandler(this.fieldPicker1_AddFields);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuFileSave,
																					  this.menuFileExit});
			this.menuItem1.Text = "&File";
			// 
			// menuFileSave
			// 
			this.menuFileSave.Index = 0;
			this.menuFileSave.Text = "&Save";
			this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
			// 
			// menuFileExit
			// 
			this.menuFileExit.Index = 1;
			this.menuFileExit.Text = "&Exit";
			this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
			// 
			// saveFieldsDlg
			// 
			this.saveFieldsDlg.DefaultExt = "txt";
			this.saveFieldsDlg.FileName = "fields";
			this.saveFieldsDlg.Filter = "(*.txt)|*.txt|All files (*.*)|*.*\"";
			this.saveFieldsDlg.Title = "Save Fields";
			// 
			// FieldsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.BackColor = System.Drawing.Color.LightGray;
			this.ClientSize = new System.Drawing.Size(904, 591);
			this.Controls.Add(this.fieldPicker1);
			this.Controls.Add(this.btnDown);
			this.Controls.Add(this.btnUp);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.lbFields);
			this.Controls.Add(this.gridFields);
			this.Controls.Add(this.btnOK);
			this.Menu = this.mainMenu1;
			this.MinimumSize = new System.Drawing.Size(500, 400);
			this.Name = "FieldsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Request Fields";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

        private FieldCollection _fields;
        public FieldCollection Fields
        {
            get 
            {  
                if(_fields == null)
                    _fields = new FieldCollection();
                return _fields;  
            }
            set { _fields = value; }
        }
        private void btnOK_Click(object sender, System.EventArgs e)
        {
			if(lbFields.Items.Count > 0)
			{
				if(MessageBox.Show("Would you like to save changes?", "Save changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) 
					== DialogResult.Yes)
				{
					menuFileSave_Click(this, new EventArgs()); 
				}
			}
            DialogResult = DialogResult.OK;
			Close();
        }

        private void btnUp_Click(object sender, System.EventArgs e)
        {
            if(lbFields.SelectedIndex > 0)
                --lbFields.SelectedIndex;
        }

        private void btnDown_Click(object sender, System.EventArgs e)
        {
            if(lbFields.SelectedIndex < lbFields.Items.Count - 1)
                ++lbFields.SelectedIndex;
        }

        private void btnRemove_Click(object sender, System.EventArgs e)
        {
			int currSel = lbFields.SelectedIndex;
			if(currSel >= 0)
			{
				lbFields.Items.RemoveAt(currSel);
				if(lbFields.Items.Count > 0)
					lbFields.SelectedIndex = lbFields.Items.Count > currSel?currSel:currSel-1;
			}
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
			fieldPicker1.Visible = !fieldPicker1.Visible;
			if(fieldPicker1.Visible)
				btnAdd.Text = "Hide <<";
			else
				btnAdd.Text = "Show >>";
        }

        private void lbFields_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            gridFields.SelectedObject = lbFields.SelectedItem;
        }

		private void fieldPicker1_AddFields(Field[] fields)
		{
			lbFields.Items.AddRange(fields);
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void menuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void menuFileSave_Click(object sender, System.EventArgs e)
		{
			Fields.Clear();
			foreach(Field fld in lbFields.Items)
				Fields.Add(fld);

			if(saveFieldsDlg.ShowDialog() == DialogResult.OK)
			{
				using(StreamWriter writer = new StreamWriter(saveFieldsDlg.OpenFile()))
				{
					foreach(Field fld in Fields)
					{
						writer.WriteLine("{0}", fld.Mnemonic);
					}
				}
			}
		}
	}
}
