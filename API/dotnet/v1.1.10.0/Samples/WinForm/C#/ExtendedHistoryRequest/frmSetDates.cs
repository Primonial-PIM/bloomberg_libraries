/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR    *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using System.Drawing;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	/// <summary>
	/// Summary description for frmSetDates.
	/// </summary>
	public class frmSetDates : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label lblEndDate;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TabControl tabControlSD;
		private System.Windows.Forms.TabPage tabFixedSD;
		private System.Windows.Forms.TabPage tabRelativeSD;
		private System.Windows.Forms.ComboBox cboCustomFiscalTypeSD;
		private System.Windows.Forms.ComboBox cboCustomFiscalPeriodSD;
		private System.Windows.Forms.ComboBox cboMoveDirectionSD;
		private System.Windows.Forms.RadioButton optAsOfTodaySD;
		private System.Windows.Forms.RadioButton optAsOfEndDateSD;
		private System.Windows.Forms.RadioButton optAsOfCustomSD;
		private System.Windows.Forms.ComboBox cboCustomFiscalQtySD;
		private System.Windows.Forms.CheckBox chboxCustomFiscalSD;
		private System.Windows.Forms.NumericUpDown spinMoveQtySD;
		private System.Windows.Forms.ComboBox cboMoveTypeSD;
		private System.Windows.Forms.ComboBox cboMovePeriodSD;
		private System.Windows.Forms.DateTimePicker dateCustomFiscalDateSD;
		private System.Windows.Forms.Label lblTodayTextSD;
		private System.Windows.Forms.CheckBox cbxFixedSpecDateTdySD;
		private System.Windows.Forms.ComboBox cboFixedFisCalPeriodSD;
		private System.Windows.Forms.ComboBox cboFixedFisCalTypeSD;
		private System.Windows.Forms.Label lblDisplaySD;
		private System.Windows.Forms.RadioButton optFixedSpecDateSD;
		private System.Windows.Forms.RadioButton optFixedFiscCalSD;
		private System.Windows.Forms.DateTimePicker dtFixedSpecDateSD;
		private System.Windows.Forms.ComboBox cboFixedFisCalQtySD;
		private System.Windows.Forms.NumericUpDown spinFixedFisCalYearSD;
		private System.Windows.Forms.Label lblTodayLineSD;
		private System.Windows.Forms.Label lblEndDateLineSD;
		private System.Windows.Forms.Label lblCustomLineSD;
		private System.Windows.Forms.Label lblDisplayED;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TabControl tabControlED;
		private System.Windows.Forms.TabPage tabFixedED;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TabPage tabRelativeED;
		private System.Windows.Forms.RadioButton optAsOfCustomED;
		private System.Windows.Forms.RadioButton optAsOfStartDateED;
		private System.Windows.Forms.RadioButton optAsOfTodayED;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label lblTodayLineED;
		private System.Windows.Forms.Label lblStartDateLineED;
		private System.Windows.Forms.Label lblCustomLineED;
		private System.Windows.Forms.Label lblAsOfBoxSD;
		private System.Windows.Forms.NumericUpDown spinCustomFiscalYearSD;
		private System.Windows.Forms.NumericUpDown spinFixedFisCalYearED;
		private System.Windows.Forms.ComboBox cboFixedFisCalQtyED;
		private System.Windows.Forms.DateTimePicker dtFixedSpecDateED;
		private System.Windows.Forms.RadioButton optFixedFiscCalED;
		private System.Windows.Forms.RadioButton optFixedSpecDateED;
		private System.Windows.Forms.CheckBox cbxFixedSpecDateTdyED;
		private System.Windows.Forms.ComboBox cboFixedFisCalPeriodED;
		private System.Windows.Forms.ComboBox cboFixedFisCalTypeED;
		private System.Windows.Forms.Label lblTodayTextED;
		private System.Windows.Forms.DateTimePicker dateCustomFiscalDateED;
		private System.Windows.Forms.ComboBox cboMovePeriodED;
		private System.Windows.Forms.ComboBox cboMoveTypeED;
		private System.Windows.Forms.NumericUpDown spinMoveQtyED;
		private System.Windows.Forms.NumericUpDown spinCustomFiscalYearED;
		private System.Windows.Forms.CheckBox chboxCustomFiscalED;
		private System.Windows.Forms.ComboBox cboCustomFiscalQtyED;
		private System.Windows.Forms.ComboBox cboCustomFiscalTypeED;
		private System.Windows.Forms.ComboBox cboCustomFiscalPeriodED;
		private System.Windows.Forms.ComboBox cboMoveDirectionED;
		private System.Windows.Forms.Label lblAsOfBoxED;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private System.Windows.Forms.Label lblEndDateTextSD;
		private System.Windows.Forms.Label lblStartDateTextED;

		private ExtendedDate SdExtendedDate = null;
		private ExtendedDate EdExtendedDate = null;
		private DateAnchor SdDateAnchor = null;
		private DateAnchor EdDateAnchor = null;
		private DateOffset SdDateOffset = null;
		private DateOffset EdDateOffset = null;
		private DATE_TYPE SdCurrentType;
		private DATE_TYPE EdCurrentType;

		OffsetDirection[] moveDirection = {OffsetDirection.Negative, OffsetDirection.Positive};
		CalendarType[] calTypeAll =	{CalendarType.Actual, CalendarType.Calendar, CalendarType.Fiscal};
		CalendarType[] calTypeCalFisc = {CalendarType.Calendar, CalendarType.Fiscal};
		PeriodUnit[] periodUnitActCal = {PeriodUnit.Day, PeriodUnit.Week, PeriodUnit.Month, PeriodUnit.Quarter, 
                                            PeriodUnit.SemiAnnual, PeriodUnit.Year};
		PeriodUnit[] periodUnitFiscal = {PeriodUnit.Quarter, PeriodUnit.SemiAnnual, PeriodUnit.Year};
		String strDisplayedDateSD = "";
		String strDisplayedDateED = "";
		bool gIsFormSaved = false;

		#region public enum SECTION

		public enum AS_OF_TOPICS
		{
			Today,  
			ED_SD,  
			Custom, 
		};

		public enum DATE_TYPE
		{
			Fixed,
			Relative
		}

		#endregion

		public frmSetDates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public frmSetDates(ExtendedDate sd, ExtendedDate ed)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			SdExtendedDate = new ExtendedDate(sd.Anchor, sd.Offset);
			EdExtendedDate = new ExtendedDate(ed.Anchor, ed.Offset);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.tabControlSD = new System.Windows.Forms.TabControl();
			this.tabFixedSD = new System.Windows.Forms.TabPage();
			this.spinFixedFisCalYearSD = new System.Windows.Forms.NumericUpDown();
			this.cboFixedFisCalQtySD = new System.Windows.Forms.ComboBox();
			this.dtFixedSpecDateSD = new System.Windows.Forms.DateTimePicker();
			this.optFixedFiscCalSD = new System.Windows.Forms.RadioButton();
			this.optFixedSpecDateSD = new System.Windows.Forms.RadioButton();
			this.label10 = new System.Windows.Forms.Label();
			this.lblEndDate = new System.Windows.Forms.Label();
			this.cbxFixedSpecDateTdySD = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cboFixedFisCalPeriodSD = new System.Windows.Forms.ComboBox();
			this.cboFixedFisCalTypeSD = new System.Windows.Forms.ComboBox();
			this.tabRelativeSD = new System.Windows.Forms.TabPage();
			this.lblEndDateTextSD = new System.Windows.Forms.Label();
			this.lblTodayTextSD = new System.Windows.Forms.Label();
			this.dateCustomFiscalDateSD = new System.Windows.Forms.DateTimePicker();
			this.cboMovePeriodSD = new System.Windows.Forms.ComboBox();
			this.cboMoveTypeSD = new System.Windows.Forms.ComboBox();
			this.spinMoveQtySD = new System.Windows.Forms.NumericUpDown();
			this.spinCustomFiscalYearSD = new System.Windows.Forms.NumericUpDown();
			this.chboxCustomFiscalSD = new System.Windows.Forms.CheckBox();
			this.cboCustomFiscalQtySD = new System.Windows.Forms.ComboBox();
			this.optAsOfCustomSD = new System.Windows.Forms.RadioButton();
			this.optAsOfEndDateSD = new System.Windows.Forms.RadioButton();
			this.optAsOfTodaySD = new System.Windows.Forms.RadioButton();
			this.label11 = new System.Windows.Forms.Label();
			this.cboCustomFiscalTypeSD = new System.Windows.Forms.ComboBox();
			this.label12 = new System.Windows.Forms.Label();
			this.cboCustomFiscalPeriodSD = new System.Windows.Forms.ComboBox();
			this.cboMoveDirectionSD = new System.Windows.Forms.ComboBox();
			this.lblTodayLineSD = new System.Windows.Forms.Label();
			this.lblEndDateLineSD = new System.Windows.Forms.Label();
			this.lblCustomLineSD = new System.Windows.Forms.Label();
			this.lblAsOfBoxSD = new System.Windows.Forms.Label();
			this.lblDisplaySD = new System.Windows.Forms.Label();
			this.lblDisplayED = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tabControlED = new System.Windows.Forms.TabControl();
			this.tabFixedED = new System.Windows.Forms.TabPage();
			this.spinFixedFisCalYearED = new System.Windows.Forms.NumericUpDown();
			this.cboFixedFisCalQtyED = new System.Windows.Forms.ComboBox();
			this.dtFixedSpecDateED = new System.Windows.Forms.DateTimePicker();
			this.optFixedFiscCalED = new System.Windows.Forms.RadioButton();
			this.optFixedSpecDateED = new System.Windows.Forms.RadioButton();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.cbxFixedSpecDateTdyED = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.cboFixedFisCalPeriodED = new System.Windows.Forms.ComboBox();
			this.cboFixedFisCalTypeED = new System.Windows.Forms.ComboBox();
			this.tabRelativeED = new System.Windows.Forms.TabPage();
			this.lblStartDateTextED = new System.Windows.Forms.Label();
			this.lblTodayTextED = new System.Windows.Forms.Label();
			this.dateCustomFiscalDateED = new System.Windows.Forms.DateTimePicker();
			this.cboMovePeriodED = new System.Windows.Forms.ComboBox();
			this.cboMoveTypeED = new System.Windows.Forms.ComboBox();
			this.spinMoveQtyED = new System.Windows.Forms.NumericUpDown();
			this.spinCustomFiscalYearED = new System.Windows.Forms.NumericUpDown();
			this.chboxCustomFiscalED = new System.Windows.Forms.CheckBox();
			this.cboCustomFiscalQtyED = new System.Windows.Forms.ComboBox();
			this.optAsOfCustomED = new System.Windows.Forms.RadioButton();
			this.optAsOfStartDateED = new System.Windows.Forms.RadioButton();
			this.optAsOfTodayED = new System.Windows.Forms.RadioButton();
			this.label9 = new System.Windows.Forms.Label();
			this.cboCustomFiscalTypeED = new System.Windows.Forms.ComboBox();
			this.label13 = new System.Windows.Forms.Label();
			this.cboCustomFiscalPeriodED = new System.Windows.Forms.ComboBox();
			this.cboMoveDirectionED = new System.Windows.Forms.ComboBox();
			this.lblTodayLineED = new System.Windows.Forms.Label();
			this.lblStartDateLineED = new System.Windows.Forms.Label();
			this.lblCustomLineED = new System.Windows.Forms.Label();
			this.lblAsOfBoxED = new System.Windows.Forms.Label();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.groupBox3.SuspendLayout();
			this.tabControlSD.SuspendLayout();
			this.tabFixedSD.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinFixedFisCalYearSD)).BeginInit();
			this.tabRelativeSD.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinMoveQtySD)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinCustomFiscalYearSD)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.tabControlED.SuspendLayout();
			this.tabFixedED.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinFixedFisCalYearED)).BeginInit();
			this.tabRelativeED.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinMoveQtyED)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinCustomFiscalYearED)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.tabControlSD);
			this.groupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox3.Location = new System.Drawing.Point(16, 17);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(440, 173);
			this.groupBox3.TabIndex = 27;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Start Date";
			// 
			// tabControlSD
			// 
			this.tabControlSD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControlSD.Controls.Add(this.tabFixedSD);
			this.tabControlSD.Controls.Add(this.tabRelativeSD);
			this.tabControlSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tabControlSD.Location = new System.Drawing.Point(8, 27);
			this.tabControlSD.Name = "tabControlSD";
			this.tabControlSD.SelectedIndex = 0;
			this.tabControlSD.Size = new System.Drawing.Size(425, 138);
			this.tabControlSD.TabIndex = 43;
			this.tabControlSD.Click += new System.EventHandler(this.tabSD_Click);
			// 
			// tabFixedSD
			// 
			this.tabFixedSD.Controls.Add(this.spinFixedFisCalYearSD);
			this.tabFixedSD.Controls.Add(this.cboFixedFisCalQtySD);
			this.tabFixedSD.Controls.Add(this.dtFixedSpecDateSD);
			this.tabFixedSD.Controls.Add(this.optFixedFiscCalSD);
			this.tabFixedSD.Controls.Add(this.optFixedSpecDateSD);
			this.tabFixedSD.Controls.Add(this.label10);
			this.tabFixedSD.Controls.Add(this.lblEndDate);
			this.tabFixedSD.Controls.Add(this.cbxFixedSpecDateTdySD);
			this.tabFixedSD.Controls.Add(this.label3);
			this.tabFixedSD.Controls.Add(this.label2);
			this.tabFixedSD.Controls.Add(this.cboFixedFisCalPeriodSD);
			this.tabFixedSD.Controls.Add(this.cboFixedFisCalTypeSD);
			this.tabFixedSD.Location = new System.Drawing.Point(4, 22);
			this.tabFixedSD.Name = "tabFixedSD";
			this.tabFixedSD.Size = new System.Drawing.Size(417, 112);
			this.tabFixedSD.TabIndex = 0;
			this.tabFixedSD.Text = "Fixed";
			// 
			// spinFixedFisCalYearSD
			// 
			this.spinFixedFisCalYearSD.Location = new System.Drawing.Point(356, 69);
			this.spinFixedFisCalYearSD.Maximum = new System.Decimal(new int[] {
																				  9999,
																				  0,
																				  0,
																				  0});
			this.spinFixedFisCalYearSD.Minimum = new System.Decimal(new int[] {
																				  1901,
																				  0,
																				  0,
																				  0});
			this.spinFixedFisCalYearSD.Name = "spinFixedFisCalYearSD";
			this.spinFixedFisCalYearSD.Size = new System.Drawing.Size(50, 21);
			this.spinFixedFisCalYearSD.TabIndex = 7;
			this.spinFixedFisCalYearSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.spinFixedFisCalYearSD.Value = new System.Decimal(new int[] {
																				2005,
																				0,
																				0,
																				0});
			this.spinFixedFisCalYearSD.ValueChanged += new System.EventHandler(this.FixedSD_Click);
			// 
			// cboFixedFisCalQtySD
			// 
			this.cboFixedFisCalQtySD.BackColor = System.Drawing.SystemColors.Window;
			this.cboFixedFisCalQtySD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFixedFisCalQtySD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboFixedFisCalQtySD.ItemHeight = 13;
			this.cboFixedFisCalQtySD.Location = new System.Drawing.Point(295, 68);
			this.cboFixedFisCalQtySD.Name = "cboFixedFisCalQtySD";
			this.cboFixedFisCalQtySD.Size = new System.Drawing.Size(43, 21);
			this.cboFixedFisCalQtySD.TabIndex = 6;
			this.cboFixedFisCalQtySD.SelectionChangeCommitted += new System.EventHandler(this.FixedSD_Click);
			// 
			// dtFixedSpecDateSD
			// 
			this.dtFixedSpecDateSD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtFixedSpecDateSD.Location = new System.Drawing.Point(107, 27);
			this.dtFixedSpecDateSD.Name = "dtFixedSpecDateSD";
			this.dtFixedSpecDateSD.Size = new System.Drawing.Size(86, 21);
			this.dtFixedSpecDateSD.TabIndex = 2;
			this.dtFixedSpecDateSD.ValueChanged += new System.EventHandler(this.FixedSD_Click);
			// 
			// optFixedFiscCalSD
			// 
			this.optFixedFiscCalSD.Location = new System.Drawing.Point(13, 68);
			this.optFixedFiscCalSD.Name = "optFixedFiscCalSD";
			this.optFixedFiscCalSD.Size = new System.Drawing.Size(93, 24);
			this.optFixedFiscCalSD.TabIndex = 1;
			this.optFixedFiscCalSD.Text = "Fisc/Calendar:";
			this.optFixedFiscCalSD.CheckedChanged += new System.EventHandler(this.optFixedSD_Click);
			// 
			// optFixedSpecDateSD
			// 
			this.optFixedSpecDateSD.Location = new System.Drawing.Point(13, 26);
			this.optFixedSpecDateSD.Name = "optFixedSpecDateSD";
			this.optFixedSpecDateSD.Size = new System.Drawing.Size(93, 24);
			this.optFixedSpecDateSD.TabIndex = 0;
			this.optFixedSpecDateSD.Text = "Specific Date:";
			this.optFixedSpecDateSD.Click += new System.EventHandler(this.cbxFixedSpecDateTdySD_CheckStateChanged);
			this.optFixedSpecDateSD.CheckedChanged += new System.EventHandler(this.optFixedSD_Click);
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.SystemColors.Control;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label10.ForeColor = System.Drawing.Color.Black;
			this.label10.Location = new System.Drawing.Point(176, 128);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(54, 20);
			this.label10.TabIndex = 55;
			this.label10.Text = "EndDate: ";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblEndDate
			// 
			this.lblEndDate.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(221)), ((System.Byte)(216)));
			this.lblEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEndDate.Location = new System.Drawing.Point(231, 128);
			this.lblEndDate.Name = "lblEndDate";
			this.lblEndDate.Size = new System.Drawing.Size(220, 20);
			this.lblEndDate.TabIndex = 53;
			this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbxFixedSpecDateTdySD
			// 
			this.cbxFixedSpecDateTdySD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbxFixedSpecDateTdySD.Location = new System.Drawing.Point(199, 28);
			this.cbxFixedSpecDateTdySD.Name = "cbxFixedSpecDateTdySD";
			this.cbxFixedSpecDateTdySD.Size = new System.Drawing.Size(66, 20);
			this.cbxFixedSpecDateTdySD.TabIndex = 3;
			this.cbxFixedSpecDateTdySD.Text = "Today";
			this.cbxFixedSpecDateTdySD.CheckStateChanged += new System.EventHandler(this.cbxFixedSpecDateTdySD_CheckStateChanged);
			this.cbxFixedSpecDateTdySD.CheckedChanged += new System.EventHandler(this.FixedSD_Click);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(12, 138);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(47, 16);
			this.label3.TabIndex = 51;
			this.label3.Text = "Move:";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(339, 71);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(18, 16);
			this.label2.TabIndex = 50;
			this.label2.Text = "of";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboFixedFisCalPeriodSD
			// 
			this.cboFixedFisCalPeriodSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboFixedFisCalPeriodSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFixedFisCalPeriodSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboFixedFisCalPeriodSD.ItemHeight = 13;
			this.cboFixedFisCalPeriodSD.Location = new System.Drawing.Point(198, 68);
			this.cboFixedFisCalPeriodSD.Name = "cboFixedFisCalPeriodSD";
			this.cboFixedFisCalPeriodSD.Size = new System.Drawing.Size(96, 21);
			this.cboFixedFisCalPeriodSD.TabIndex = 5;
			this.cboFixedFisCalPeriodSD.SelectionChangeCommitted += new System.EventHandler(this.FixedPeriodicitySD_Click);
			// 
			// cboFixedFisCalTypeSD
			// 
			this.cboFixedFisCalTypeSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboFixedFisCalTypeSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFixedFisCalTypeSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboFixedFisCalTypeSD.ItemHeight = 13;
			this.cboFixedFisCalTypeSD.Location = new System.Drawing.Point(106, 68);
			this.cboFixedFisCalTypeSD.Name = "cboFixedFisCalTypeSD";
			this.cboFixedFisCalTypeSD.Size = new System.Drawing.Size(90, 21);
			this.cboFixedFisCalTypeSD.TabIndex = 4;
			this.cboFixedFisCalTypeSD.SelectionChangeCommitted += new System.EventHandler(this.FixedSD_Click);
			// 
			// tabRelativeSD
			// 
			this.tabRelativeSD.Controls.Add(this.lblEndDateTextSD);
			this.tabRelativeSD.Controls.Add(this.lblTodayTextSD);
			this.tabRelativeSD.Controls.Add(this.dateCustomFiscalDateSD);
			this.tabRelativeSD.Controls.Add(this.cboMovePeriodSD);
			this.tabRelativeSD.Controls.Add(this.cboMoveTypeSD);
			this.tabRelativeSD.Controls.Add(this.spinMoveQtySD);
			this.tabRelativeSD.Controls.Add(this.spinCustomFiscalYearSD);
			this.tabRelativeSD.Controls.Add(this.chboxCustomFiscalSD);
			this.tabRelativeSD.Controls.Add(this.cboCustomFiscalQtySD);
			this.tabRelativeSD.Controls.Add(this.optAsOfCustomSD);
			this.tabRelativeSD.Controls.Add(this.optAsOfEndDateSD);
			this.tabRelativeSD.Controls.Add(this.optAsOfTodaySD);
			this.tabRelativeSD.Controls.Add(this.label11);
			this.tabRelativeSD.Controls.Add(this.cboCustomFiscalTypeSD);
			this.tabRelativeSD.Controls.Add(this.label12);
			this.tabRelativeSD.Controls.Add(this.cboCustomFiscalPeriodSD);
			this.tabRelativeSD.Controls.Add(this.cboMoveDirectionSD);
			this.tabRelativeSD.Controls.Add(this.lblTodayLineSD);
			this.tabRelativeSD.Controls.Add(this.lblEndDateLineSD);
			this.tabRelativeSD.Controls.Add(this.lblCustomLineSD);
			this.tabRelativeSD.Controls.Add(this.lblAsOfBoxSD);
			this.tabRelativeSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tabRelativeSD.Location = new System.Drawing.Point(4, 22);
			this.tabRelativeSD.Name = "tabRelativeSD";
			this.tabRelativeSD.Size = new System.Drawing.Size(417, 112);
			this.tabRelativeSD.TabIndex = 1;
			this.tabRelativeSD.Text = "Relative";
			// 
			// lblEndDateTextSD
			// 
			this.lblEndDateTextSD.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.lblEndDateTextSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEndDateTextSD.Location = new System.Drawing.Point(193, 43);
			this.lblEndDateTextSD.Name = "lblEndDateTextSD";
			this.lblEndDateTextSD.Size = new System.Drawing.Size(63, 15);
			this.lblEndDateTextSD.TabIndex = 45;
			this.lblEndDateTextSD.Text = "ED";
			this.lblEndDateTextSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblEndDateTextSD.Visible = false;
			// 
			// lblTodayTextSD
			// 
			this.lblTodayTextSD.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.lblTodayTextSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTodayTextSD.Location = new System.Drawing.Point(74, 43);
			this.lblTodayTextSD.Name = "lblTodayTextSD";
			this.lblTodayTextSD.Size = new System.Drawing.Size(63, 15);
			this.lblTodayTextSD.TabIndex = 83;
			this.lblTodayTextSD.Text = "TDY";
			this.lblTodayTextSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblTodayTextSD.Visible = false;
			// 
			// dateCustomFiscalDateSD
			// 
			this.dateCustomFiscalDateSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.dateCustomFiscalDateSD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateCustomFiscalDateSD.Location = new System.Drawing.Point(256, 40);
			this.dateCustomFiscalDateSD.MinDate = new System.DateTime(1901, 1, 1, 0, 0, 0, 0);
			this.dateCustomFiscalDateSD.Name = "dateCustomFiscalDateSD";
			this.dateCustomFiscalDateSD.Size = new System.Drawing.Size(83, 21);
			this.dateCustomFiscalDateSD.TabIndex = 8;
			this.dateCustomFiscalDateSD.Visible = false;
			this.dateCustomFiscalDateSD.ValueChanged += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// cboMovePeriodSD
			// 
			this.cboMovePeriodSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboMovePeriodSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMovePeriodSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMovePeriodSD.ItemHeight = 13;
			this.cboMovePeriodSD.Location = new System.Drawing.Point(299, 76);
			this.cboMovePeriodSD.Name = "cboMovePeriodSD";
			this.cboMovePeriodSD.Size = new System.Drawing.Size(102, 21);
			this.cboMovePeriodSD.TabIndex = 12;
			this.cboMovePeriodSD.SelectionChangeCommitted += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// cboMoveTypeSD
			// 
			this.cboMoveTypeSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboMoveTypeSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMoveTypeSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMoveTypeSD.ItemHeight = 13;
			this.cboMoveTypeSD.Location = new System.Drawing.Point(206, 76);
			this.cboMoveTypeSD.Name = "cboMoveTypeSD";
			this.cboMoveTypeSD.Size = new System.Drawing.Size(89, 21);
			this.cboMoveTypeSD.TabIndex = 11;
			this.cboMoveTypeSD.SelectionChangeCommitted += new System.EventHandler(this.cboMoveTypeSD_SelectionChangeCommitted);
			// 
			// spinMoveQtySD
			// 
			this.spinMoveQtySD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.spinMoveQtySD.Location = new System.Drawing.Point(148, 76);
			this.spinMoveQtySD.Maximum = new System.Decimal(new int[] {
																		  9999,
																		  0,
																		  0,
																		  0});
			this.spinMoveQtySD.Name = "spinMoveQtySD";
			this.spinMoveQtySD.Size = new System.Drawing.Size(55, 21);
			this.spinMoveQtySD.TabIndex = 10;
			this.spinMoveQtySD.Value = new System.Decimal(new int[] {
																		1,
																		0,
																		0,
																		0});
			this.spinMoveQtySD.ValueChanged += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// spinCustomFiscalYearSD
			// 
			this.spinCustomFiscalYearSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.spinCustomFiscalYearSD.Location = new System.Drawing.Point(287, 40);
			this.spinCustomFiscalYearSD.Maximum = new System.Decimal(new int[] {
																				   2999,
																				   0,
																				   0,
																				   0});
			this.spinCustomFiscalYearSD.Minimum = new System.Decimal(new int[] {
																				   1965,
																				   0,
																				   0,
																				   0});
			this.spinCustomFiscalYearSD.Name = "spinCustomFiscalYearSD";
			this.spinCustomFiscalYearSD.Size = new System.Drawing.Size(52, 21);
			this.spinCustomFiscalYearSD.TabIndex = 6;
			this.spinCustomFiscalYearSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.spinCustomFiscalYearSD.Value = new System.Decimal(new int[] {
																				 2005,
																				 0,
																				 0,
																				 0});
			this.spinCustomFiscalYearSD.ValueChanged += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// chboxCustomFiscalSD
			// 
			this.chboxCustomFiscalSD.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.chboxCustomFiscalSD.Checked = true;
			this.chboxCustomFiscalSD.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chboxCustomFiscalSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.chboxCustomFiscalSD.Location = new System.Drawing.Point(343, 42);
			this.chboxCustomFiscalSD.Name = "chboxCustomFiscalSD";
			this.chboxCustomFiscalSD.Size = new System.Drawing.Size(56, 19);
			this.chboxCustomFiscalSD.TabIndex = 7;
			this.chboxCustomFiscalSD.Text = "Fiscal";
			this.chboxCustomFiscalSD.CheckedChanged += new System.EventHandler(this.chboxCustomFiscalSD_CheckedChanged);
			// 
			// cboCustomFiscalQtySD
			// 
			this.cboCustomFiscalQtySD.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomFiscalQtySD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomFiscalQtySD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCustomFiscalQtySD.ItemHeight = 13;
			this.cboCustomFiscalQtySD.Location = new System.Drawing.Point(247, 40);
			this.cboCustomFiscalQtySD.Name = "cboCustomFiscalQtySD";
			this.cboCustomFiscalQtySD.Size = new System.Drawing.Size(39, 21);
			this.cboCustomFiscalQtySD.TabIndex = 5;
			this.cboCustomFiscalQtySD.SelectedIndexChanged += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// optAsOfCustomSD
			// 
			this.optAsOfCustomSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.optAsOfCustomSD.Location = new System.Drawing.Point(290, 15);
			this.optAsOfCustomSD.Name = "optAsOfCustomSD";
			this.optAsOfCustomSD.Size = new System.Drawing.Size(60, 20);
			this.optAsOfCustomSD.TabIndex = 2;
			this.optAsOfCustomSD.Text = "Custom";
			this.optAsOfCustomSD.CheckedChanged += new System.EventHandler(this.optAsOfSD_CheckedChanged);
			// 
			// optAsOfEndDateSD
			// 
			this.optAsOfEndDateSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.optAsOfEndDateSD.Location = new System.Drawing.Point(172, 15);
			this.optAsOfEndDateSD.Name = "optAsOfEndDateSD";
			this.optAsOfEndDateSD.Size = new System.Drawing.Size(69, 20);
			this.optAsOfEndDateSD.TabIndex = 1;
			this.optAsOfEndDateSD.Text = "End Date";
			this.optAsOfEndDateSD.CheckedChanged += new System.EventHandler(this.optAsOfSD_CheckedChanged);
			// 
			// optAsOfTodaySD
			// 
			this.optAsOfTodaySD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.optAsOfTodaySD.Location = new System.Drawing.Point(67, 15);
			this.optAsOfTodaySD.Name = "optAsOfTodaySD";
			this.optAsOfTodaySD.Size = new System.Drawing.Size(55, 20);
			this.optAsOfTodaySD.TabIndex = 0;
			this.optAsOfTodaySD.Text = "Today";
			this.optAsOfTodaySD.CheckedChanged += new System.EventHandler(this.optAsOfSD_CheckedChanged);
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label11.Location = new System.Drawing.Point(16, 79);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(39, 16);
			this.label11.TabIndex = 64;
			this.label11.Text = "Move:";
			// 
			// cboCustomFiscalTypeSD
			// 
			this.cboCustomFiscalTypeSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomFiscalTypeSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomFiscalTypeSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCustomFiscalTypeSD.ItemHeight = 13;
			this.cboCustomFiscalTypeSD.Location = new System.Drawing.Point(72, 40);
			this.cboCustomFiscalTypeSD.Name = "cboCustomFiscalTypeSD";
			this.cboCustomFiscalTypeSD.Size = new System.Drawing.Size(88, 21);
			this.cboCustomFiscalTypeSD.TabIndex = 3;
			this.cboCustomFiscalTypeSD.SelectionChangeCommitted += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label12.Location = new System.Drawing.Point(17, 16);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(40, 16);
			this.label12.TabIndex = 63;
			this.label12.Text = "As of:";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboCustomFiscalPeriodSD
			// 
			this.cboCustomFiscalPeriodSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomFiscalPeriodSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomFiscalPeriodSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCustomFiscalPeriodSD.ItemHeight = 13;
			this.cboCustomFiscalPeriodSD.Location = new System.Drawing.Point(161, 40);
			this.cboCustomFiscalPeriodSD.Name = "cboCustomFiscalPeriodSD";
			this.cboCustomFiscalPeriodSD.Size = new System.Drawing.Size(85, 21);
			this.cboCustomFiscalPeriodSD.TabIndex = 4;
			this.cboCustomFiscalPeriodSD.SelectionChangeCommitted += new System.EventHandler(this.cboCustomFiscalPeriodSD_SelectionChangeCommitted);
			// 
			// cboMoveDirectionSD
			// 
			this.cboMoveDirectionSD.BackColor = System.Drawing.SystemColors.Window;
			this.cboMoveDirectionSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMoveDirectionSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMoveDirectionSD.ItemHeight = 13;
			this.cboMoveDirectionSD.Location = new System.Drawing.Point(66, 76);
			this.cboMoveDirectionSD.Name = "cboMoveDirectionSD";
			this.cboMoveDirectionSD.Size = new System.Drawing.Size(79, 21);
			this.cboMoveDirectionSD.TabIndex = 9;
			this.cboMoveDirectionSD.SelectionChangeCommitted += new System.EventHandler(this.optAsOfSD_Click);
			// 
			// lblTodayLineSD
			// 
			this.lblTodayLineSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTodayLineSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTodayLineSD.ForeColor = System.Drawing.Color.White;
			this.lblTodayLineSD.Location = new System.Drawing.Point(67, 23);
			this.lblTodayLineSD.Name = "lblTodayLineSD";
			this.lblTodayLineSD.Size = new System.Drawing.Size(76, 13);
			this.lblTodayLineSD.TabIndex = 72;
			this.lblTodayLineSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblEndDateLineSD
			// 
			this.lblEndDateLineSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblEndDateLineSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEndDateLineSD.ForeColor = System.Drawing.Color.White;
			this.lblEndDateLineSD.Location = new System.Drawing.Point(177, 22);
			this.lblEndDateLineSD.Name = "lblEndDateLineSD";
			this.lblEndDateLineSD.Size = new System.Drawing.Size(86, 14);
			this.lblEndDateLineSD.TabIndex = 73;
			this.lblEndDateLineSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCustomLineSD
			// 
			this.lblCustomLineSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblCustomLineSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCustomLineSD.ForeColor = System.Drawing.Color.White;
			this.lblCustomLineSD.Location = new System.Drawing.Point(296, 21);
			this.lblCustomLineSD.Name = "lblCustomLineSD";
			this.lblCustomLineSD.Size = new System.Drawing.Size(104, 15);
			this.lblCustomLineSD.TabIndex = 74;
			this.lblCustomLineSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblAsOfBoxSD
			// 
			this.lblAsOfBoxSD.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.lblAsOfBoxSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblAsOfBoxSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAsOfBoxSD.ForeColor = System.Drawing.Color.White;
			this.lblAsOfBoxSD.Location = new System.Drawing.Point(67, 35);
			this.lblAsOfBoxSD.Name = "lblAsOfBoxSD";
			this.lblAsOfBoxSD.Size = new System.Drawing.Size(333, 31);
			this.lblAsOfBoxSD.TabIndex = 61;
			this.lblAsOfBoxSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDisplaySD
			// 
			this.lblDisplaySD.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblDisplaySD.AutoSize = true;
			this.lblDisplaySD.BackColor = System.Drawing.SystemColors.Control;
			this.lblDisplaySD.Font = new System.Drawing.Font("Tahoma", 8.25F);
			this.lblDisplaySD.ForeColor = System.Drawing.Color.Black;
			this.lblDisplaySD.Location = new System.Drawing.Point(382, 16);
			this.lblDisplaySD.Name = "lblDisplaySD";
			this.lblDisplaySD.Size = new System.Drawing.Size(67, 17);
			this.lblDisplaySD.TabIndex = 44;
			this.lblDisplaySD.Text = "FY1111-2CM";
			this.lblDisplaySD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDisplayED
			// 
			this.lblDisplayED.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblDisplayED.AutoSize = true;
			this.lblDisplayED.BackColor = System.Drawing.SystemColors.Control;
			this.lblDisplayED.Font = new System.Drawing.Font("Tahoma", 8.25F);
			this.lblDisplayED.ForeColor = System.Drawing.Color.Black;
			this.lblDisplayED.Location = new System.Drawing.Point(382, 204);
			this.lblDisplayED.Name = "lblDisplayED";
			this.lblDisplayED.Size = new System.Drawing.Size(67, 17);
			this.lblDisplayED.TabIndex = 46;
			this.lblDisplayED.Text = "FY1111-2CM";
			this.lblDisplayED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.tabControlED);
			this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(16, 205);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(440, 174);
			this.groupBox1.TabIndex = 45;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "End Date";
			// 
			// tabControlED
			// 
			this.tabControlED.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControlED.Controls.Add(this.tabFixedED);
			this.tabControlED.Controls.Add(this.tabRelativeED);
			this.tabControlED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tabControlED.Location = new System.Drawing.Point(8, 27);
			this.tabControlED.Name = "tabControlED";
			this.tabControlED.SelectedIndex = 0;
			this.tabControlED.Size = new System.Drawing.Size(425, 139);
			this.tabControlED.TabIndex = 43;
			this.tabControlED.Click += new System.EventHandler(this.tabED_Click);
			// 
			// tabFixedED
			// 
			this.tabFixedED.Controls.Add(this.spinFixedFisCalYearED);
			this.tabFixedED.Controls.Add(this.cboFixedFisCalQtyED);
			this.tabFixedED.Controls.Add(this.dtFixedSpecDateED);
			this.tabFixedED.Controls.Add(this.optFixedFiscCalED);
			this.tabFixedED.Controls.Add(this.optFixedSpecDateED);
			this.tabFixedED.Controls.Add(this.label4);
			this.tabFixedED.Controls.Add(this.label5);
			this.tabFixedED.Controls.Add(this.cbxFixedSpecDateTdyED);
			this.tabFixedED.Controls.Add(this.label6);
			this.tabFixedED.Controls.Add(this.label7);
			this.tabFixedED.Controls.Add(this.cboFixedFisCalPeriodED);
			this.tabFixedED.Controls.Add(this.cboFixedFisCalTypeED);
			this.tabFixedED.Location = new System.Drawing.Point(4, 22);
			this.tabFixedED.Name = "tabFixedED";
			this.tabFixedED.Size = new System.Drawing.Size(417, 113);
			this.tabFixedED.TabIndex = 0;
			this.tabFixedED.Text = "Fixed";
			// 
			// spinFixedFisCalYearED
			// 
			this.spinFixedFisCalYearED.Location = new System.Drawing.Point(356, 69);
			this.spinFixedFisCalYearED.Maximum = new System.Decimal(new int[] {
																				  9999,
																				  0,
																				  0,
																				  0});
			this.spinFixedFisCalYearED.Minimum = new System.Decimal(new int[] {
																				  1901,
																				  0,
																				  0,
																				  0});
			this.spinFixedFisCalYearED.Name = "spinFixedFisCalYearED";
			this.spinFixedFisCalYearED.Size = new System.Drawing.Size(50, 21);
			this.spinFixedFisCalYearED.TabIndex = 8;
			this.spinFixedFisCalYearED.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.spinFixedFisCalYearED.Value = new System.Decimal(new int[] {
																				2005,
																				0,
																				0,
																				0});
			this.spinFixedFisCalYearED.ValueChanged += new System.EventHandler(this.FixedED_Click);
			// 
			// cboFixedFisCalQtyED
			// 
			this.cboFixedFisCalQtyED.BackColor = System.Drawing.SystemColors.Window;
			this.cboFixedFisCalQtyED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFixedFisCalQtyED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboFixedFisCalQtyED.ItemHeight = 13;
			this.cboFixedFisCalQtyED.Location = new System.Drawing.Point(295, 68);
			this.cboFixedFisCalQtyED.Name = "cboFixedFisCalQtyED";
			this.cboFixedFisCalQtyED.Size = new System.Drawing.Size(43, 21);
			this.cboFixedFisCalQtyED.TabIndex = 7;
			this.cboFixedFisCalQtyED.SelectionChangeCommitted += new System.EventHandler(this.FixedED_Click);
			// 
			// dtFixedSpecDateED
			// 
			this.dtFixedSpecDateED.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtFixedSpecDateED.Location = new System.Drawing.Point(107, 27);
			this.dtFixedSpecDateED.Name = "dtFixedSpecDateED";
			this.dtFixedSpecDateED.Size = new System.Drawing.Size(86, 21);
			this.dtFixedSpecDateED.TabIndex = 3;
			this.dtFixedSpecDateED.ValueChanged += new System.EventHandler(this.FixedED_Click);
			// 
			// optFixedFiscCalED
			// 
			this.optFixedFiscCalED.Location = new System.Drawing.Point(13, 68);
			this.optFixedFiscCalED.Name = "optFixedFiscCalED";
			this.optFixedFiscCalED.Size = new System.Drawing.Size(93, 24);
			this.optFixedFiscCalED.TabIndex = 2;
			this.optFixedFiscCalED.Text = "Fisc/Calendar:";
			this.optFixedFiscCalED.CheckedChanged += new System.EventHandler(this.optFixedED_Click);
			// 
			// optFixedSpecDateED
			// 
			this.optFixedSpecDateED.Location = new System.Drawing.Point(13, 26);
			this.optFixedSpecDateED.Name = "optFixedSpecDateED";
			this.optFixedSpecDateED.Size = new System.Drawing.Size(93, 24);
			this.optFixedSpecDateED.TabIndex = 0;
			this.optFixedSpecDateED.Text = "Specific Date:";
			this.optFixedSpecDateED.Click += new System.EventHandler(this.cbxFixedSpecDateTdyED_CheckStateChanged);
			this.optFixedSpecDateED.CheckedChanged += new System.EventHandler(this.optFixedED_Click);
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.SystemColors.Control;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Location = new System.Drawing.Point(176, 128);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(54, 20);
			this.label4.TabIndex = 55;
			this.label4.Text = "EndDate: ";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(221)), ((System.Byte)(216)));
			this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(231, 128);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(220, 20);
			this.label5.TabIndex = 53;
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cbxFixedSpecDateTdyED
			// 
			this.cbxFixedSpecDateTdyED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbxFixedSpecDateTdyED.Location = new System.Drawing.Point(199, 28);
			this.cbxFixedSpecDateTdyED.Name = "cbxFixedSpecDateTdyED";
			this.cbxFixedSpecDateTdyED.Size = new System.Drawing.Size(66, 20);
			this.cbxFixedSpecDateTdyED.TabIndex = 4;
			this.cbxFixedSpecDateTdyED.Text = "Today";
			this.cbxFixedSpecDateTdyED.CheckStateChanged += new System.EventHandler(this.cbxFixedSpecDateTdyED_CheckStateChanged);
			this.cbxFixedSpecDateTdyED.CheckedChanged += new System.EventHandler(this.FixedED_Click);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(12, 138);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(47, 16);
			this.label6.TabIndex = 51;
			this.label6.Text = "Move:";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(339, 71);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(18, 16);
			this.label7.TabIndex = 50;
			this.label7.Text = "of";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboFixedFisCalPeriodED
			// 
			this.cboFixedFisCalPeriodED.BackColor = System.Drawing.SystemColors.Window;
			this.cboFixedFisCalPeriodED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFixedFisCalPeriodED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboFixedFisCalPeriodED.ItemHeight = 13;
			this.cboFixedFisCalPeriodED.Location = new System.Drawing.Point(198, 68);
			this.cboFixedFisCalPeriodED.Name = "cboFixedFisCalPeriodED";
			this.cboFixedFisCalPeriodED.Size = new System.Drawing.Size(96, 21);
			this.cboFixedFisCalPeriodED.TabIndex = 6;
			this.cboFixedFisCalPeriodED.SelectionChangeCommitted += new System.EventHandler(this.FixedPeriodicityED_Click);
			// 
			// cboFixedFisCalTypeED
			// 
			this.cboFixedFisCalTypeED.BackColor = System.Drawing.SystemColors.Window;
			this.cboFixedFisCalTypeED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboFixedFisCalTypeED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboFixedFisCalTypeED.ItemHeight = 13;
			this.cboFixedFisCalTypeED.Location = new System.Drawing.Point(106, 68);
			this.cboFixedFisCalTypeED.Name = "cboFixedFisCalTypeED";
			this.cboFixedFisCalTypeED.Size = new System.Drawing.Size(90, 21);
			this.cboFixedFisCalTypeED.TabIndex = 5;
			this.cboFixedFisCalTypeED.SelectionChangeCommitted += new System.EventHandler(this.FixedED_Click);
			// 
			// tabRelativeED
			// 
			this.tabRelativeED.Controls.Add(this.lblStartDateTextED);
			this.tabRelativeED.Controls.Add(this.lblTodayTextED);
			this.tabRelativeED.Controls.Add(this.dateCustomFiscalDateED);
			this.tabRelativeED.Controls.Add(this.cboMovePeriodED);
			this.tabRelativeED.Controls.Add(this.cboMoveTypeED);
			this.tabRelativeED.Controls.Add(this.spinMoveQtyED);
			this.tabRelativeED.Controls.Add(this.spinCustomFiscalYearED);
			this.tabRelativeED.Controls.Add(this.chboxCustomFiscalED);
			this.tabRelativeED.Controls.Add(this.cboCustomFiscalQtyED);
			this.tabRelativeED.Controls.Add(this.optAsOfCustomED);
			this.tabRelativeED.Controls.Add(this.optAsOfStartDateED);
			this.tabRelativeED.Controls.Add(this.optAsOfTodayED);
			this.tabRelativeED.Controls.Add(this.label9);
			this.tabRelativeED.Controls.Add(this.cboCustomFiscalTypeED);
			this.tabRelativeED.Controls.Add(this.label13);
			this.tabRelativeED.Controls.Add(this.cboCustomFiscalPeriodED);
			this.tabRelativeED.Controls.Add(this.cboMoveDirectionED);
			this.tabRelativeED.Controls.Add(this.lblTodayLineED);
			this.tabRelativeED.Controls.Add(this.lblStartDateLineED);
			this.tabRelativeED.Controls.Add(this.lblCustomLineED);
			this.tabRelativeED.Controls.Add(this.lblAsOfBoxED);
			this.tabRelativeED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tabRelativeED.Location = new System.Drawing.Point(4, 22);
			this.tabRelativeED.Name = "tabRelativeED";
			this.tabRelativeED.Size = new System.Drawing.Size(417, 113);
			this.tabRelativeED.TabIndex = 1;
			this.tabRelativeED.Text = "Relative";
			this.tabRelativeED.Visible = false;
			// 
			// lblStartDateTextED
			// 
			this.lblStartDateTextED.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.lblStartDateTextED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblStartDateTextED.Location = new System.Drawing.Point(193, 43);
			this.lblStartDateTextED.Name = "lblStartDateTextED";
			this.lblStartDateTextED.Size = new System.Drawing.Size(63, 15);
			this.lblStartDateTextED.TabIndex = 45;
			this.lblStartDateTextED.Text = "SD";
			this.lblStartDateTextED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblStartDateTextED.Visible = false;
			// 
			// lblTodayTextED
			// 
			this.lblTodayTextED.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.lblTodayTextED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTodayTextED.Location = new System.Drawing.Point(74, 43);
			this.lblTodayTextED.Name = "lblTodayTextED";
			this.lblTodayTextED.Size = new System.Drawing.Size(63, 15);
			this.lblTodayTextED.TabIndex = 83;
			this.lblTodayTextED.Text = "TDY";
			this.lblTodayTextED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.lblTodayTextED.Visible = false;
			// 
			// dateCustomFiscalDateED
			// 
			this.dateCustomFiscalDateED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.dateCustomFiscalDateED.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dateCustomFiscalDateED.Location = new System.Drawing.Point(256, 40);
			this.dateCustomFiscalDateED.MinDate = new System.DateTime(1901, 1, 1, 0, 0, 0, 0);
			this.dateCustomFiscalDateED.Name = "dateCustomFiscalDateED";
			this.dateCustomFiscalDateED.Size = new System.Drawing.Size(83, 21);
			this.dateCustomFiscalDateED.TabIndex = 8;
			this.dateCustomFiscalDateED.Visible = false;
			this.dateCustomFiscalDateED.ValueChanged += new System.EventHandler(this.optAsOfED_Click);
			// 
			// cboMovePeriodED
			// 
			this.cboMovePeriodED.BackColor = System.Drawing.SystemColors.Window;
			this.cboMovePeriodED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMovePeriodED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMovePeriodED.ItemHeight = 13;
			this.cboMovePeriodED.Location = new System.Drawing.Point(299, 76);
			this.cboMovePeriodED.Name = "cboMovePeriodED";
			this.cboMovePeriodED.Size = new System.Drawing.Size(102, 21);
			this.cboMovePeriodED.TabIndex = 12;
			this.cboMovePeriodED.SelectionChangeCommitted += new System.EventHandler(this.optAsOfED_Click);
			// 
			// cboMoveTypeED
			// 
			this.cboMoveTypeED.BackColor = System.Drawing.SystemColors.Window;
			this.cboMoveTypeED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMoveTypeED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMoveTypeED.ItemHeight = 13;
			this.cboMoveTypeED.Location = new System.Drawing.Point(206, 76);
			this.cboMoveTypeED.Name = "cboMoveTypeED";
			this.cboMoveTypeED.Size = new System.Drawing.Size(89, 21);
			this.cboMoveTypeED.TabIndex = 11;
			this.cboMoveTypeED.SelectionChangeCommitted += new System.EventHandler(this.cboMoveTypeED_SelectionChangeCommitted);
			// 
			// spinMoveQtyED
			// 
			this.spinMoveQtyED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.spinMoveQtyED.Location = new System.Drawing.Point(148, 76);
			this.spinMoveQtyED.Maximum = new System.Decimal(new int[] {
																		  9999,
																		  0,
																		  0,
																		  0});
			this.spinMoveQtyED.Name = "spinMoveQtyED";
			this.spinMoveQtyED.Size = new System.Drawing.Size(55, 21);
			this.spinMoveQtyED.TabIndex = 10;
			this.spinMoveQtyED.Value = new System.Decimal(new int[] {
																		1,
																		0,
																		0,
																		0});
			this.spinMoveQtyED.ValueChanged += new System.EventHandler(this.optAsOfED_Click);
			// 
			// spinCustomFiscalYearED
			// 
			this.spinCustomFiscalYearED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.spinCustomFiscalYearED.Location = new System.Drawing.Point(287, 40);
			this.spinCustomFiscalYearED.Maximum = new System.Decimal(new int[] {
																				   2999,
																				   0,
																				   0,
																				   0});
			this.spinCustomFiscalYearED.Minimum = new System.Decimal(new int[] {
																				   1965,
																				   0,
																				   0,
																				   0});
			this.spinCustomFiscalYearED.Name = "spinCustomFiscalYearED";
			this.spinCustomFiscalYearED.Size = new System.Drawing.Size(52, 21);
			this.spinCustomFiscalYearED.TabIndex = 6;
			this.spinCustomFiscalYearED.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.spinCustomFiscalYearED.Value = new System.Decimal(new int[] {
																				 2005,
																				 0,
																				 0,
																				 0});
			this.spinCustomFiscalYearED.ValueChanged += new System.EventHandler(this.optAsOfED_Click);
			// 
			// chboxCustomFiscalED
			// 
			this.chboxCustomFiscalED.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.chboxCustomFiscalED.Checked = true;
			this.chboxCustomFiscalED.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chboxCustomFiscalED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.chboxCustomFiscalED.Location = new System.Drawing.Point(343, 42);
			this.chboxCustomFiscalED.Name = "chboxCustomFiscalED";
			this.chboxCustomFiscalED.Size = new System.Drawing.Size(56, 19);
			this.chboxCustomFiscalED.TabIndex = 7;
			this.chboxCustomFiscalED.Text = "Fiscal";
			this.chboxCustomFiscalED.CheckedChanged += new System.EventHandler(this.chboxCustomFiscalED_CheckedChanged);
			// 
			// cboCustomFiscalQtyED
			// 
			this.cboCustomFiscalQtyED.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomFiscalQtyED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomFiscalQtyED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCustomFiscalQtyED.ItemHeight = 13;
			this.cboCustomFiscalQtyED.Location = new System.Drawing.Point(247, 40);
			this.cboCustomFiscalQtyED.Name = "cboCustomFiscalQtyED";
			this.cboCustomFiscalQtyED.Size = new System.Drawing.Size(39, 21);
			this.cboCustomFiscalQtyED.TabIndex = 5;
			this.cboCustomFiscalQtyED.SelectedIndexChanged += new System.EventHandler(this.optAsOfED_Click);
			// 
			// optAsOfCustomED
			// 
			this.optAsOfCustomED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.optAsOfCustomED.Location = new System.Drawing.Point(290, 15);
			this.optAsOfCustomED.Name = "optAsOfCustomED";
			this.optAsOfCustomED.Size = new System.Drawing.Size(60, 20);
			this.optAsOfCustomED.TabIndex = 2;
			this.optAsOfCustomED.Text = "Custom";
			this.optAsOfCustomED.CheckedChanged += new System.EventHandler(this.optAsOfED_CheckedChanged);
			// 
			// optAsOfStartDateED
			// 
			this.optAsOfStartDateED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.optAsOfStartDateED.Location = new System.Drawing.Point(172, 15);
			this.optAsOfStartDateED.Name = "optAsOfStartDateED";
			this.optAsOfStartDateED.Size = new System.Drawing.Size(73, 20);
			this.optAsOfStartDateED.TabIndex = 1;
			this.optAsOfStartDateED.Text = "Start Date";
			this.optAsOfStartDateED.CheckedChanged += new System.EventHandler(this.optAsOfED_CheckedChanged);
			// 
			// optAsOfTodayED
			// 
			this.optAsOfTodayED.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.optAsOfTodayED.Location = new System.Drawing.Point(62, 15);
			this.optAsOfTodayED.Name = "optAsOfTodayED";
			this.optAsOfTodayED.Size = new System.Drawing.Size(55, 20);
			this.optAsOfTodayED.TabIndex = 0;
			this.optAsOfTodayED.Text = "Today";
			this.optAsOfTodayED.CheckedChanged += new System.EventHandler(this.optAsOfED_CheckedChanged);
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label9.Location = new System.Drawing.Point(16, 79);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(39, 16);
			this.label9.TabIndex = 64;
			this.label9.Text = "Move:";
			// 
			// cboCustomFiscalTypeED
			// 
			this.cboCustomFiscalTypeED.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomFiscalTypeED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomFiscalTypeED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCustomFiscalTypeED.ItemHeight = 13;
			this.cboCustomFiscalTypeED.Location = new System.Drawing.Point(72, 40);
			this.cboCustomFiscalTypeED.Name = "cboCustomFiscalTypeED";
			this.cboCustomFiscalTypeED.Size = new System.Drawing.Size(88, 21);
			this.cboCustomFiscalTypeED.TabIndex = 3;
			this.cboCustomFiscalTypeED.SelectionChangeCommitted += new System.EventHandler(this.optAsOfED_Click);
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label13.Location = new System.Drawing.Point(17, 16);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(40, 16);
			this.label13.TabIndex = 63;
			this.label13.Text = "As of:";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboCustomFiscalPeriodED
			// 
			this.cboCustomFiscalPeriodED.BackColor = System.Drawing.SystemColors.Window;
			this.cboCustomFiscalPeriodED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCustomFiscalPeriodED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCustomFiscalPeriodED.ItemHeight = 13;
			this.cboCustomFiscalPeriodED.Location = new System.Drawing.Point(161, 40);
			this.cboCustomFiscalPeriodED.Name = "cboCustomFiscalPeriodED";
			this.cboCustomFiscalPeriodED.Size = new System.Drawing.Size(85, 21);
			this.cboCustomFiscalPeriodED.TabIndex = 4;
			this.cboCustomFiscalPeriodED.SelectionChangeCommitted += new System.EventHandler(this.cboCustomFiscalPeriodED_SelectionChangeCommitted);
			// 
			// cboMoveDirectionED
			// 
			this.cboMoveDirectionED.BackColor = System.Drawing.SystemColors.Window;
			this.cboMoveDirectionED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMoveDirectionED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMoveDirectionED.ItemHeight = 13;
			this.cboMoveDirectionED.Location = new System.Drawing.Point(66, 76);
			this.cboMoveDirectionED.Name = "cboMoveDirectionED";
			this.cboMoveDirectionED.Size = new System.Drawing.Size(79, 21);
			this.cboMoveDirectionED.TabIndex = 9;
			this.cboMoveDirectionED.SelectionChangeCommitted += new System.EventHandler(this.optAsOfED_Click);
			// 
			// lblTodayLineED
			// 
			this.lblTodayLineED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTodayLineED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTodayLineED.ForeColor = System.Drawing.Color.White;
			this.lblTodayLineED.Location = new System.Drawing.Point(67, 21);
			this.lblTodayLineED.Name = "lblTodayLineED";
			this.lblTodayLineED.Size = new System.Drawing.Size(76, 15);
			this.lblTodayLineED.TabIndex = 72;
			this.lblTodayLineED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblStartDateLineED
			// 
			this.lblStartDateLineED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblStartDateLineED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblStartDateLineED.ForeColor = System.Drawing.Color.White;
			this.lblStartDateLineED.Location = new System.Drawing.Point(177, 21);
			this.lblStartDateLineED.Name = "lblStartDateLineED";
			this.lblStartDateLineED.Size = new System.Drawing.Size(86, 15);
			this.lblStartDateLineED.TabIndex = 73;
			this.lblStartDateLineED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCustomLineED
			// 
			this.lblCustomLineED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblCustomLineED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCustomLineED.ForeColor = System.Drawing.Color.White;
			this.lblCustomLineED.Location = new System.Drawing.Point(296, 21);
			this.lblCustomLineED.Name = "lblCustomLineED";
			this.lblCustomLineED.Size = new System.Drawing.Size(104, 15);
			this.lblCustomLineED.TabIndex = 74;
			this.lblCustomLineED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblAsOfBoxED
			// 
			this.lblAsOfBoxED.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(224)), ((System.Byte)(224)));
			this.lblAsOfBoxED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblAsOfBoxED.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAsOfBoxED.ForeColor = System.Drawing.Color.White;
			this.lblAsOfBoxED.Location = new System.Drawing.Point(67, 35);
			this.lblAsOfBoxED.Name = "lblAsOfBoxED";
			this.lblAsOfBoxED.Size = new System.Drawing.Size(333, 31);
			this.lblAsOfBoxED.TabIndex = 61;
			this.lblAsOfBoxED.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.BackColor = System.Drawing.SystemColors.Control;
			this.btnOk.Location = new System.Drawing.Point(310, 396);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(68, 27);
			this.btnOk.TabIndex = 14;
			this.btnOk.Text = "&OK";
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.SystemColors.Control;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(387, 396);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(68, 27);
			this.btnCancel.TabIndex = 15;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// frmSetDates
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(471, 440);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.lblDisplayED);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.lblDisplaySD);
			this.Controls.Add(this.groupBox3);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSetDates";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Set Start and End Dates";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmSetDates_Closing);
			this.Load += new System.EventHandler(this.frmSetDates_Load);
			this.groupBox3.ResumeLayout(false);
			this.tabControlSD.ResumeLayout(false);
			this.tabFixedSD.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinFixedFisCalYearSD)).EndInit();
			this.tabRelativeSD.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinMoveQtySD)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinCustomFiscalYearSD)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.tabControlED.ResumeLayout(false);
			this.tabFixedED.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinFixedFisCalYearED)).EndInit();
			this.tabRelativeED.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinMoveQtyED)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinCustomFiscalYearED)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		
		private void frmSetDates_Load(object sender, System.EventArgs e)
		{
			//**************************
			// Populate dropdown lists:
			//**************************

			//  1) Fixed tab
			cboFixedFisCalTypeSD.DataSource = calTypeCalFisc.Clone();
			cboFixedFisCalTypeED.DataSource = calTypeCalFisc.Clone();
			cboFixedFisCalPeriodSD.DataSource = periodUnitFiscal.Clone();
			cboFixedFisCalPeriodED.DataSource = periodUnitFiscal.Clone();
			cboFixedFisCalQtySD.Items.AddRange(new object[]{"1", "2", "3", "4"});
			cboFixedFisCalQtyED.Items.AddRange(new object[]{"1", "2", "3", "4"});

			//  2) Relative Tab
			cboCustomFiscalTypeSD.DataSource = calTypeCalFisc.Clone();
			cboCustomFiscalTypeED.DataSource = calTypeCalFisc.Clone();
			cboCustomFiscalPeriodSD.DataSource = periodUnitFiscal.Clone();
			cboCustomFiscalPeriodED.DataSource = periodUnitFiscal.Clone();
			cboCustomFiscalQtySD.Items.AddRange(new object[]{"1", "2", "3", "4"});
			cboCustomFiscalQtyED.Items.AddRange(new object[]{"1", "2", "3", "4"});
			cboMoveDirectionSD.DataSource = moveDirection.Clone();
			cboMoveDirectionED.DataSource = moveDirection.Clone();
			cboMoveTypeSD.DataSource = calTypeAll.Clone();
			cboMoveTypeED.DataSource = calTypeAll.Clone();
			cboMovePeriodSD.DataSource = periodUnitActCal.Clone();
			cboMovePeriodED.DataSource = periodUnitActCal.Clone();

			SetDefaults();
			SetEnvironment();  
			DisplayDates();
		}

		private void SetDefaults()
		{
			// Start Date section
			// 1) Relative
			optAsOfTodaySD.Checked = true;
			cboCustomFiscalQtySD.SelectedIndex = 0;
			cboMoveDirectionSD.SelectedIndex = 0;
			spinMoveQtySD.Value = 1;
			cboMoveTypeSD.SelectedIndex = 0;
			cboMovePeriodSD.SelectedIndex = 0;
			// 2) Fixed
			optFixedSpecDateSD.Checked = true;
			cboFixedFisCalTypeSD.SelectedIndex = 0;
			cboFixedFisCalPeriodSD.SelectedIndex = 0;
			cboFixedFisCalQtySD.SelectedIndex = 0;
			spinFixedFisCalYearSD.Value = DateTime.Now.Year;

			// End Date section
			// 1) Relative
			optAsOfTodayED.Checked = true;
			cboCustomFiscalQtyED.SelectedIndex = 0;
			cboMoveDirectionED.SelectedIndex = 0;
			spinMoveQtyED.Value = 1;
			cboMoveTypeED.SelectedIndex = 0;
			cboMovePeriodED.SelectedIndex = 0;
			// 2) Fixed
			optFixedSpecDateED.Checked = true;
			cboFixedFisCalTypeED.SelectedIndex = 0;
            cboFixedFisCalPeriodED.SelectedIndex = 0;
            cboFixedFisCalQtyED.SelectedIndex = 0;
            spinFixedFisCalYearED.Value = DateTime.Now.Year;
		}

		private void SetEnvironment()
		{	
			// **********************
			//   Start Date Section
			// **********************
			// Check if Today
			if (SdExtendedDate.IsToday)
			{
				SdCurrentType = DATE_TYPE.Fixed;
				tabControlSD.SelectedTab = tabFixedSD;
				cbxFixedSpecDateTdySD.Checked = true;
				return;
			}

			// Check if Relative or Fixed
			if (SdExtendedDate.IsFixed)
			{
				SdCurrentType = DATE_TYPE.Fixed;
				tabControlSD.SelectedTab = tabFixedSD;

				// Populate Quantity dropdown list
				switch (SdExtendedDate.Anchor.PeriodUnit)
				{
					case PeriodUnit.Quarter:
						cboFixedFisCalQtySD.Items.Clear();
						cboFixedFisCalQtySD.Items.AddRange(new object[]{"1", "2", "3", "4"});
						break;

					case PeriodUnit.SemiAnnual:
						cboFixedFisCalQtySD.Items.Clear();
						cboFixedFisCalQtySD.Items.AddRange(new object[]{"1", "2"});
						break;
								
					case PeriodUnit.Year:
						cboFixedFisCalQtySD.Items.Clear();
						cboFixedFisCalQtySD.Items.Add("1");
						break;
				}

				// Check if extended date - set applicable value in each control
				if (SdExtendedDate.IsExtended)
				{
					cboFixedFisCalTypeSD.SelectedItem = SdExtendedDate.Anchor.CalendarType.ToString();
					cboFixedFisCalPeriodSD.SelectedItem = SdExtendedDate.Anchor.PeriodUnit.ToString();
					cboFixedFisCalQtySD.SelectedIndex = SdExtendedDate.Anchor.Count - 1;
					spinFixedFisCalYearSD.Value = SdExtendedDate.Anchor.Year;
					optFixedFiscCalSD.Checked = true;
				}
				else
				{
					dtFixedSpecDateSD.Value = SdExtendedDate.Anchor.Date;
					optFixedSpecDateSD.Checked = true;
				}
			}
			else
			{
				SdCurrentType = DATE_TYPE.Relative;
				tabControlSD.SelectedTab = tabRelativeSD;

				// -- "AS OF" Section --
				// Determine if TODAY is Anchor
				if(SdExtendedDate.Anchor == null)
				{
					optAsOfTodaySD.Checked = true;
					ToggleAsOfSection(true, AS_OF_TOPICS.Today);
				}
				// Determine if ED is Anchor
				else if(SdExtendedDate.Anchor == DateAnchor.End)
				{
					optAsOfEndDateSD.Checked = true;
					optAsOfStartDateED.Enabled = false;
					ToggleAsOfSection(true, AS_OF_TOPICS.ED_SD);
				}
				// Custom is expected
				else
				{
					optAsOfCustomSD.Checked = true;
					ToggleAsOfSection(true, AS_OF_TOPICS.Custom);

					// Expected that the Anchor is a fiscal-calendar value
					if (SdExtendedDate.Anchor.CalendarType != CalendarType.None)
					{
						ToggleFiscalControls(true, true);

						// Populate Quantity dropdown list
						switch (SdExtendedDate.Anchor.PeriodUnit)
						{
							case PeriodUnit.Quarter:
								cboCustomFiscalQtySD.Items.Clear();
								cboCustomFiscalQtySD.Items.AddRange(new object[]{"1", "2", "3", "4"});
								cboCustomFiscalPeriodSD.SelectedIndex = 0;
								break;

							case PeriodUnit.SemiAnnual:
								cboCustomFiscalQtySD.Items.Clear();
								cboCustomFiscalQtySD.Items.AddRange(new object[]{"1", "2"});
								cboCustomFiscalPeriodSD.SelectedIndex = 1;
								break;
								
							case PeriodUnit.Year:
								cboCustomFiscalQtySD.Items.Clear();
								cboCustomFiscalQtySD.Items.Add("1");
								cboCustomFiscalPeriodSD.SelectedIndex = 2;
								break;
						}

						cboCustomFiscalTypeSD.SelectedItem = SdExtendedDate.Anchor.CalendarType;
						cboCustomFiscalPeriodSD.SelectedItem = SdExtendedDate.Anchor.PeriodUnit;
						cboCustomFiscalQtySD.SelectedIndex = SdExtendedDate.Anchor.Count - 1;
						spinCustomFiscalYearSD.Value = SdExtendedDate.Anchor.Year;
					}
					// Then it is expected that the Anchor is a date/time value
					else
					{
						ToggleFiscalControls(false, true);
						dateCustomFiscalDateSD.Value = SdExtendedDate.Anchor.Date;
					}
				}

				// -- "MOVE" Section --
				// Populate Quantity dropdown list according to what is selected
				switch (SdExtendedDate.Offset.CalendarType)
				{
					case CalendarType.Actual:	// Actual
					case CalendarType.Calendar: // Calendar
						cboMovePeriodSD.DataSource = periodUnitActCal.Clone();
						break;

					case CalendarType.Fiscal:	// Fiscal
						cboMovePeriodSD.DataSource = periodUnitFiscal.Clone();
						break;
				}

				cboMoveDirectionSD.SelectedItem = SdExtendedDate.Offset.Direction;
				spinMoveQtySD.Value = SdExtendedDate.Offset.Count;
				cboMoveTypeSD.SelectedItem = SdExtendedDate.Offset.CalendarType;
				cboMovePeriodSD.SelectedItem = SdExtendedDate.Offset.PeriodUnit;
			}

			// ********************
			//   End Date Section
			// ********************

			// Check if Today
			if (EdExtendedDate.IsToday)
			{
				EdCurrentType = DATE_TYPE.Fixed;
				tabControlED.SelectedTab = tabFixedED;
				cbxFixedSpecDateTdyED.Checked = true;
				return;
			}

			// Check if Relative or Fixed
			if (EdExtendedDate.IsFixed)
			{
				EdCurrentType = DATE_TYPE.Fixed;
				tabControlED.SelectedTab = tabFixedED;

				// Populate Quantity dropdown list
				switch (EdExtendedDate.Anchor.PeriodUnit)
				{
					case PeriodUnit.Quarter:
						cboFixedFisCalQtyED.Items.Clear();
						cboFixedFisCalQtyED.Items.AddRange(new object[]{"1", "2", "3", "4"});
						break;

					case PeriodUnit.SemiAnnual:
						cboFixedFisCalQtyED.Items.Clear();
						cboFixedFisCalQtyED.Items.AddRange(new object[]{"1", "2"});
						break;
								
					case PeriodUnit.Year:
						cboFixedFisCalQtyED.Items.Clear();
						cboFixedFisCalQtyED.Items.Add("1");
						break;
				}

				// Check if extended date
				if (EdExtendedDate.IsExtended)
				{
					cboFixedFisCalTypeED.SelectedItem = EdExtendedDate.Anchor.CalendarType;
					cboFixedFisCalPeriodED.SelectedItem = EdExtendedDate.Anchor.PeriodUnit;
					cboFixedFisCalQtyED.SelectedIndex = EdExtendedDate.Anchor.Count - 1;
					spinFixedFisCalYearED.Value = EdExtendedDate.Anchor.Year;
					optFixedFiscCalED.Checked = true;
				}
				else
				{
					dtFixedSpecDateED.Value = EdExtendedDate.Anchor.Date;
					optFixedSpecDateED.Checked = true;
				}
			}
			else		// Relative
			{
				tabControlED.SelectedTab = tabRelativeED;
				EdCurrentType = DATE_TYPE.Relative;

				// -- "AS OF" Section --
				// Determine if TODAY is Anchor
				if(EdExtendedDate.Anchor == null)
				{
					optAsOfTodayED.Checked = true;
					ToggleAsOfSection(false, AS_OF_TOPICS.Today);
				}
				// Determine if SD is Anchor
				else if(EdExtendedDate.Anchor == DateAnchor.Start)
				{
					optAsOfStartDateED.Checked = true;
					optAsOfEndDateSD.Enabled = false;
					ToggleAsOfSection(false, AS_OF_TOPICS.ED_SD);
				}
				// Custom is expected
				else
				{
					optAsOfCustomED.Checked = true;
					ToggleAsOfSection(false, AS_OF_TOPICS.Custom);

					if (EdExtendedDate.Anchor.CalendarType != CalendarType.None)
					{
						ToggleFiscalControls(true, false);

						// Populate Quantity dropdown list
						switch (EdExtendedDate.Anchor.PeriodUnit)
						{
							case PeriodUnit.Quarter:
								cboCustomFiscalQtyED.Items.Clear();
								cboCustomFiscalQtyED.Items.AddRange(new object[]{"1", "2", "3", "4"});
								break;

							case PeriodUnit.SemiAnnual:
								cboCustomFiscalQtyED.Items.Clear();
								cboCustomFiscalQtyED.Items.AddRange(new object[]{"1", "2"});
								break;
								
							case PeriodUnit.Year:
								cboCustomFiscalQtyED.Items.Clear();
								cboCustomFiscalQtyED.Items.Add("1");
								break;
						}

						cboCustomFiscalTypeED.SelectedItem = EdExtendedDate.Anchor.CalendarType;
						cboCustomFiscalPeriodED.SelectedItem = EdExtendedDate.Anchor.PeriodUnit;
						cboCustomFiscalQtyED.SelectedIndex = EdExtendedDate.Anchor.Count - 1;
						spinCustomFiscalYearED.Value = EdExtendedDate.Anchor.Year;
					}
					else
					{
						ToggleFiscalControls(false, false);
						dateCustomFiscalDateED.Value = EdExtendedDate.Anchor.Date;
					}
				}

				// -- "MOVE" Section --
				// Populate Quantity dropdown list according to what is selected
				switch (EdExtendedDate.Offset.CalendarType)
				{
					case CalendarType.Actual:	// Actual
					case CalendarType.Calendar: // Calendar
						cboMovePeriodED.DataSource = periodUnitActCal.Clone();
						break;

					case CalendarType.Fiscal:	// Fiscal
						cboMovePeriodED.DataSource = periodUnitFiscal.Clone();
						break;
				}

				cboMoveDirectionED.SelectedItem = EdExtendedDate.Offset.Direction;
				spinMoveQtyED.Value = EdExtendedDate.Offset.Count;
				cboMoveTypeED.SelectedItem = EdExtendedDate.Offset.CalendarType;
				cboMovePeriodED.SelectedItem = EdExtendedDate.Offset.PeriodUnit;
			}
		}

		private void SaveEnvironment()
		{	
			PeriodUnit selPeriodUnit;

			// ********************
			//  Start Date Section
			// ********************
			if (SdCurrentType == DATE_TYPE.Fixed)		// ** Fixed Date **
			{
				SdDateOffset = null;					// Not applicable, therefore, set to null

				if (optFixedSpecDateSD.Checked)			// "Specific Date" selected
				{
					if (cbxFixedSpecDateTdySD.Checked)	// Today
					{
						SdDateAnchor = null;
					}
					else								// Date-Time
					{									
						SdDateAnchor = new DateAnchor(dtFixedSpecDateSD.Value);
					}
				}
				else									// "Fisc/Calendar" selected
				{
					SdDateAnchor = new DateAnchor(
						calTypeCalFisc[cboFixedFisCalTypeSD.SelectedIndex], 
						periodUnitFiscal[cboFixedFisCalPeriodSD.SelectedIndex],
						Convert.ToInt16(cboFixedFisCalQtySD.SelectedIndex + 1), 
						Convert.ToInt16(spinFixedFisCalYearSD.Value)
					);
				}
			}
			else										// ** Relative Date **
			{
				// Update Anchor attributes
				if (optAsOfTodaySD.Checked)				// Today
				{
					SdDateAnchor = null;
				}
				else if (optAsOfEndDateSD.Checked)		// End Date
				{
					SdDateAnchor = DateAnchor.End;
				}
				else									// Custom
				{
					if (chboxCustomFiscalSD.Checked)	// Fiscal-Calendar
					{
						SdDateAnchor = new DateAnchor(
							calTypeCalFisc[cboCustomFiscalTypeSD.SelectedIndex], 
							periodUnitFiscal[cboCustomFiscalPeriodSD.SelectedIndex],
							Convert.ToInt16(cboCustomFiscalQtySD.SelectedIndex + 1), 
							Convert.ToInt16(spinCustomFiscalYearSD.Value)
						);
					}
					else								// DateTime
					{
						SdDateAnchor = new DateAnchor(dateCustomFiscalDateSD.Value);
					}
				}

				// Update Offset attributes
				if (2 == cboMoveTypeSD.SelectedIndex)  // denotes Fiscal Type
					selPeriodUnit = periodUnitFiscal[cboMovePeriodSD.SelectedIndex];
				else  // denotes Calendar or Actual type
					selPeriodUnit = periodUnitActCal[cboMovePeriodSD.SelectedIndex];

				SdDateOffset = new DateOffset(
					moveDirection[cboMoveDirectionSD.SelectedIndex],
					Convert.ToInt16(spinMoveQtySD.Value), 
					calTypeAll[cboMoveTypeSD.SelectedIndex], 
					selPeriodUnit
				);
			}				

			// ********************
			//   End Date Section
			// ********************
			if (EdCurrentType == DATE_TYPE.Fixed)		// ** Fixed Date **
			{
				EdDateOffset = null;					// Not applicable, therefore, set to null

				if (optFixedSpecDateED.Checked)			// "Specific Date" selected
				{
					if (cbxFixedSpecDateTdyED.Checked)	// Today
					{
						EdDateAnchor = null;
					}
					else								// Date-Time
					{									
						EdDateAnchor = new DateAnchor(dtFixedSpecDateED.Value);
					}
				}
				else									// "Fisc/Calendar" selected
				{
					EdDateAnchor = new DateAnchor(
						calTypeCalFisc[cboFixedFisCalTypeED.SelectedIndex], 
						periodUnitFiscal[cboFixedFisCalPeriodED.SelectedIndex],
						Convert.ToInt16(cboFixedFisCalQtyED.SelectedIndex + 1), 
						Convert.ToInt16(spinFixedFisCalYearED.Value)
					);
				}
			}
			else										// ** Relative Date **
			{
				// Update Anchor attributes
				if (optAsOfTodayED.Checked)				// Today
				{
					EdDateAnchor = null;
				}
				else if (optAsOfStartDateED.Checked)		// End Date
				{
					EdDateAnchor = DateAnchor.End;
				}
				else									// Custom
				{
					if (chboxCustomFiscalED.Checked)	// Fiscal-Calendar
					{
						EdDateAnchor = new DateAnchor(
							calTypeCalFisc[cboCustomFiscalTypeED.SelectedIndex], 
							periodUnitFiscal[cboCustomFiscalPeriodED.SelectedIndex],
							Convert.ToInt16(cboCustomFiscalQtyED.SelectedIndex + 1), 
							Convert.ToInt16(spinCustomFiscalYearED.Value)
						);
					}
					else								// DateTime
					{
						EdDateAnchor = new DateAnchor(dateCustomFiscalDateSD.Value);
					}
				}

				// Update Offset attributes
				if (2 == cboMoveTypeED.SelectedIndex)  // denotes Fiscal Type
					selPeriodUnit = periodUnitFiscal[cboMovePeriodED.SelectedIndex];
				else  // denotes Calendar or Actual type
					selPeriodUnit = periodUnitActCal[cboMovePeriodED.SelectedIndex];

				EdDateOffset = new DateOffset(
					moveDirection[cboMoveDirectionED.SelectedIndex],
					Convert.ToInt16(spinMoveQtyED.Value), 
					calTypeAll[cboMoveTypeED.SelectedIndex], 
					selPeriodUnit
				);
			}

			// Assign displayed dates to private variables
			strDisplayedDateSD = lblDisplaySD.Text;
			strDisplayedDateED = lblDisplayED.Text;

			SdExtendedDate = new ExtendedDate(SdDateAnchor, SdDateOffset);
			EdExtendedDate = new ExtendedDate(EdDateAnchor, EdDateOffset);
		}

		private void DisplayDates()
		{
			DisplayDate(true);
			DisplayDate(false);
		}

		private void DisplayDate(bool isStartDate)
		{
			StringBuilder strSD = new StringBuilder();
			StringBuilder strED = new StringBuilder();

			// ******************
			// Start Date Section
			// ******************
			if (isStartDate)
			{
				if (SdCurrentType == DATE_TYPE.Fixed)		// ** Fixed Date **
				{
					if (optFixedSpecDateSD.Checked)			// "Specific Date" selected
					{
						if (cbxFixedSpecDateTdySD.Checked)	// Today
						{
							strSD.Append("Today");
						}
						else								// Date-Time
						{									
							strSD.Append(dtFixedSpecDateSD.Value.ToShortDateString().ToString());
						}
					}
					else									// "Fisc/Calendar" selected
					{
						strSD.Append(cboFixedFisCalTypeSD.Text.ToString().Substring(0,1));
						strSD.Append(cboFixedFisCalPeriodSD.Text.ToString().Substring(0,1));
						strSD.Append(cboFixedFisCalQtySD.Text.ToString());
						strSD.Append(spinFixedFisCalYearSD.Value.ToString());
					}
				}
				else										// ** Relative Date **
				{
					if (optAsOfTodaySD.Checked)				// Today
					{
						strSD.Append("");
					}
					else if (optAsOfEndDateSD.Checked)		// End Date
					{
						strSD.Append("ED");
					}
					else									// Custom
					{
						if (chboxCustomFiscalSD.Checked)	// Fiscal-Calendar
						{
							strSD.Append(cboCustomFiscalTypeSD.Text.ToString().Substring(0,1));
							strSD.Append(cboCustomFiscalPeriodSD.Text.ToString().Substring(0,1));
							strSD.Append(cboCustomFiscalQtySD.Text.ToString());
							strSD.Append(spinCustomFiscalYearSD.Value.ToString());
						}
						else								// DateTime
						{
							strSD.Append(dateCustomFiscalDateSD.Value.ToShortDateString().ToString());
						}
					}

					// Offset attributes
					if(cboMoveDirectionSD.SelectedIndex == 0)	// Negative
						strSD.Append("-");	
					else
						strSD.Append("+");

					strSD.Append(spinMoveQtySD.Value);
					strSD.Append(cboMoveTypeSD.Text.ToString().Substring(0,1));
					strSD.Append(cboMovePeriodSD.Text.ToString().Substring(0,1));
				}			
	
				// Output to SD display box and anchor to right position
				lblDisplaySD.Text = strSD.ToString();
				AdjustDisplayArea(lblDisplaySD);
			}

			// ****************
			// End Date Section
			// ****************
			else
			{
				if (EdCurrentType == DATE_TYPE.Fixed)		// ** Fixed Date **
				{
					if (optFixedSpecDateED.Checked)			// "Specific Date" selected
					{
						if (cbxFixedSpecDateTdyED.Checked)	// Today
						{
							strED.Append("Today");
						}
						else								// Date-Time
						{									
							strED.Append(dtFixedSpecDateED.Value.ToShortDateString().ToString());
						}
					}
					else									// "Fisc/Calendar" selected
					{
						strED.Append(cboFixedFisCalTypeED.Text.ToString().Substring(0,1));
						strED.Append(cboFixedFisCalPeriodED.Text.ToString().Substring(0,1));
						strED.Append(cboFixedFisCalQtyED.Text.ToString());
						strED.Append(spinFixedFisCalYearED.Value.ToString());
					}
				}
				else										// ** Relative Date **
				{
					if (optAsOfTodayED.Checked)				// Today
					{
						strED.Append("");
					}
					else if (optAsOfStartDateED.Checked)		// Start Date
					{
						strED.Append("SD");
					}
					else									// Custom
					{
						if (chboxCustomFiscalED.Checked)	// Fiscal-Calendar
						{
							strED.Append(cboCustomFiscalTypeED.Text.ToString().Substring(0,1));
							strED.Append(cboCustomFiscalPeriodED.Text.ToString().Substring(0,1));
							strED.Append(cboCustomFiscalQtyED.Text.ToString());
							strED.Append(spinCustomFiscalYearED.Value.ToString());
						}
						else								// DateTime
						{
							strED.Append(dateCustomFiscalDateED.Text);
						}
					}

					// Offset attributes
					if(cboMoveDirectionED.SelectedIndex == 0)	// Negative
						strED.Append("-");	
					else
						strED.Append("+");

					strED.Append(spinMoveQtyED.Value);
					strED.Append(cboMoveTypeED.Text.ToString().Substring(0,1));
					strED.Append(cboMovePeriodED.Text.ToString().Substring(0,1));
				}			
	
				// Output to SD display box and anchor to right position
				lblDisplayED.Text = strED.ToString();
				AdjustDisplayArea(lblDisplayED);
			}
		}

		private void AdjustDisplayArea(Label DisplayCtrl)
		{
			const int DISTANCE_FROM_RIGHT_SIDE = 30;
			int xPos = 0;

			xPos = this.Size.Width - DisplayCtrl.Width - DISTANCE_FROM_RIGHT_SIDE;
			DisplayCtrl.Location = new Point(xPos, DisplayCtrl.Location.Y);
		}

		private void ToggleFiscalControls(bool isFiscalOn, bool isStartDate)
		{
			if (isStartDate)
			{
				cboCustomFiscalTypeSD.Visible = isFiscalOn;
				cboCustomFiscalPeriodSD.Visible = isFiscalOn;
				cboCustomFiscalQtySD.Visible = isFiscalOn;
				spinCustomFiscalYearSD.Visible = isFiscalOn;
				dateCustomFiscalDateSD.Visible = !isFiscalOn;
				chboxCustomFiscalSD.Checked = isFiscalOn;
			}
			else
			{
				cboCustomFiscalTypeED.Visible = isFiscalOn;
				cboCustomFiscalPeriodED.Visible = isFiscalOn;
				cboCustomFiscalQtyED.Visible = isFiscalOn;
				spinCustomFiscalYearED.Visible = isFiscalOn;
				dateCustomFiscalDateED.Visible = !isFiscalOn;
				chboxCustomFiscalED.Checked = isFiscalOn;
			}
		}

		private void ToggleAsOfSection(bool isStartDate, AS_OF_TOPICS topic)
		{
			if (isStartDate)
			{
				switch (topic)
				{
					case AS_OF_TOPICS.Today:
						lblTodayLineSD.Visible = true;
						lblEndDateLineSD.Visible = false;
						lblCustomLineSD.Visible = false;
						
						lblTodayTextSD.Visible = true;
						lblEndDateTextSD.Visible = false;
						cboCustomFiscalTypeSD.Visible = false;
						cboCustomFiscalPeriodSD.Visible = false;
						cboCustomFiscalQtySD.Visible = false;
						spinCustomFiscalYearSD.Visible = false;
						dateCustomFiscalDateSD.Visible = false;
						chboxCustomFiscalSD.Visible = false;

						// Change width of outline label
						lblAsOfBoxSD.Width = 76;

						break;

					case AS_OF_TOPICS.ED_SD:
						lblTodayLineSD.Visible = false;
						lblEndDateLineSD.Visible = true;
						lblCustomLineSD.Visible = false;

						lblTodayTextSD.Visible = false;
						lblEndDateTextSD.Visible = true;
						cboCustomFiscalTypeSD.Visible = false;
						cboCustomFiscalPeriodSD.Visible = false;
						cboCustomFiscalQtySD.Visible = false;
						spinCustomFiscalYearSD.Visible = false;
						dateCustomFiscalDateSD.Visible = false;
						chboxCustomFiscalSD.Visible = false;

						// Change width of outline label
						lblAsOfBoxSD.Width = 196;

						break;

					case AS_OF_TOPICS.Custom:
						lblTodayLineSD.Visible = false;
						lblEndDateLineSD.Visible = false;
						lblCustomLineSD.Visible = true;

						lblTodayTextSD.Visible = false;
						lblEndDateTextSD.Visible = false;
						chboxCustomFiscalSD.Visible = true;

						if (chboxCustomFiscalSD.Checked)
						{
							cboCustomFiscalTypeSD.Visible = true;
							cboCustomFiscalPeriodSD.Visible = true;
							cboCustomFiscalQtySD.Visible = true;
							spinCustomFiscalYearSD.Visible = true;
							dateCustomFiscalDateSD.Visible = false;
						}
						else
						{
							cboCustomFiscalTypeSD.Visible = false;
							cboCustomFiscalPeriodSD.Visible = false;
							cboCustomFiscalQtySD.Visible = false;
							spinCustomFiscalYearSD.Visible = false;
							dateCustomFiscalDateSD.Visible = true;
						}

						// Change width of outline label
						lblAsOfBoxSD.Width = 333;

						break;
				}
			}
			else
			{
				switch (topic)
				{
					case AS_OF_TOPICS.Today:
						lblTodayLineED.Visible = true;
						lblStartDateLineED.Visible = false;
						lblCustomLineED.Visible = false;

						lblTodayTextED.Visible = true;
						lblStartDateTextED.Visible = false;
						cboCustomFiscalTypeED.Visible = false;
						cboCustomFiscalPeriodED.Visible = false;
						cboCustomFiscalQtyED.Visible = false;
						spinCustomFiscalYearED.Visible = false;
						dateCustomFiscalDateED.Visible = false;
						chboxCustomFiscalED.Visible = false;

						// Change width of outline label
						lblAsOfBoxED.Width = 76;

						break;

					case AS_OF_TOPICS.ED_SD:
						lblTodayLineED.Visible = false;
						lblStartDateLineED.Visible = true;
						lblCustomLineED.Visible = false;

						lblTodayTextED.Visible = false;
						lblStartDateTextED.Visible = true;
						cboCustomFiscalTypeED.Visible = false;
						cboCustomFiscalPeriodED.Visible = false;
						cboCustomFiscalQtyED.Visible = false;
						spinCustomFiscalYearED.Visible = false;
						dateCustomFiscalDateED.Visible = false;
						chboxCustomFiscalED.Visible = false;

						// Change width of outline label
						lblAsOfBoxED.Width = 196;

						break;

					case AS_OF_TOPICS.Custom:
						lblTodayLineED.Visible = false;
						lblStartDateLineED.Visible = false;
						lblCustomLineED.Visible = true;

						lblTodayTextED.Visible = false;
						lblStartDateTextED.Visible = false;
						chboxCustomFiscalED.Visible = true;

						if (chboxCustomFiscalED.Checked)
						{
							cboCustomFiscalTypeED.Visible = true;
							cboCustomFiscalPeriodED.Visible = true;
							cboCustomFiscalQtyED.Visible = true;
							spinCustomFiscalYearED.Visible = true;
							dateCustomFiscalDateED.Visible = false;
						}
						else
						{
							cboCustomFiscalTypeED.Visible = false;
							cboCustomFiscalPeriodED.Visible = false;
							cboCustomFiscalQtyED.Visible = false;
							spinCustomFiscalYearED.Visible = false;
							dateCustomFiscalDateED.Visible = true;
						}

						// Change width of outline label
						lblAsOfBoxED.Width = 333;

						break;
				}
			}
		}

		private void optAsOfSD_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optAsOfTodaySD.Checked)
			{
				SdDateAnchor = null;
				ToggleAsOfSection(true, AS_OF_TOPICS.Today);
				optAsOfStartDateED.Enabled = true;
			}
			else if(optAsOfEndDateSD.Checked)
			{
				SdDateAnchor = new DateAnchor("ED");
				ToggleAsOfSection(true, AS_OF_TOPICS.ED_SD);
				optAsOfStartDateED.Enabled = false;
			}
			else if (optAsOfCustomSD.Checked)
			{
				ToggleAsOfSection(true, AS_OF_TOPICS.Custom);
				optAsOfStartDateED.Enabled = true;
			}

			DisplayDate(true);
		}

		private void optAsOfED_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optAsOfTodayED.Checked)
			{
				EdDateAnchor = null;
				ToggleAsOfSection(false, AS_OF_TOPICS.Today);
				optAsOfEndDateSD.Enabled = true;
			}
			else if(optAsOfStartDateED.Checked)
			{
				EdDateAnchor = new DateAnchor("SD");
				ToggleAsOfSection(false, AS_OF_TOPICS.ED_SD);
				optAsOfEndDateSD.Enabled = false;
			}
			else if (optAsOfCustomED.Checked)
			{
				ToggleAsOfSection(false, AS_OF_TOPICS.Custom);
				optAsOfEndDateSD.Enabled = true;
			}

			DisplayDate(false);
		}

		private void chboxCustomFiscalED_CheckedChanged(object sender, System.EventArgs e)
		{
			ToggleFiscalControls(chboxCustomFiscalED.Checked, false);
			DisplayDate(false);
		}

		private void chboxCustomFiscalSD_CheckedChanged(object sender, System.EventArgs e)
		{
			ToggleFiscalControls(chboxCustomFiscalSD.Checked, true);
			DisplayDate(true);
		}

		private void optAsOfSD_Click(object sender, System.EventArgs e)
		{
			DisplayDate(true);
		}

		private void optAsOfED_Click(object sender, System.EventArgs e)
		{
			DisplayDate(false);
		}

		private void FixedSD_Click(object sender, System.EventArgs e)
		{
			DisplayDate(true);
		}

		private void FixedED_Click(object sender, System.EventArgs e)
		{
			DisplayDate(false);
		}

		private void cboMoveTypeSD_SelectionChangeCommitted(object sender, System.EventArgs e)
		{
			// Populate Quantity dropdown list according to what is selected
			switch (cboMoveTypeSD.SelectedIndex)
			{
				case 0:	// Actual
				case 1: // Calendar
					cboMovePeriodSD.DataSource = periodUnitActCal.Clone();
					break;

				case 2: // Fiscal
					cboMovePeriodSD.DataSource = periodUnitFiscal.Clone();
					break;
			}

			cboMovePeriodSD.SelectedIndex = 0;

			DisplayDate(true);
		}

		private void cboMoveTypeED_SelectionChangeCommitted(object sender, System.EventArgs e)
		{
			// Populate Quantity dropdown list according to what is selected
			switch (cboMoveTypeED.SelectedIndex)
			{
				case 0:	// Actual
				case 1: // Calendar
					cboMovePeriodED.DataSource = periodUnitActCal.Clone();
					break;

				case 2: // Fiscal
					cboMovePeriodED.SelectedIndex = 0;
					cboMovePeriodED.DataSource = periodUnitFiscal.Clone();
					break;
			}

			cboMovePeriodED.SelectedIndex = 0;

			DisplayDate(false);
		}

		private void cboCustomFiscalPeriodSD_SelectionChangeCommitted(object sender, System.EventArgs e)
		{
			cboCustomFiscalQtySD.Items.Clear();

			// Populate Quantity dropdown list according to what is selected
			switch (cboCustomFiscalPeriodSD.SelectedIndex)
			{
				case 0:	// Quarterly
					cboCustomFiscalQtySD.Items.AddRange(new object[]{"1", "2", "3", "4"});
					break;

				case 1: // Semi-Annually
					cboCustomFiscalQtySD.Items.AddRange(new object[]{"1", "2"});
					break;

				case 2: // Yearly
					cboCustomFiscalQtySD.Items.Add("1");
					break;
			}

			cboCustomFiscalQtySD.SelectedIndex = 0;

			DisplayDate(true);
		}

		private void cboCustomFiscalPeriodED_SelectionChangeCommitted(object sender, System.EventArgs e)
		{
			cboCustomFiscalQtyED.Items.Clear();

			// Populate Quantity dropdown list according to what is selected
			switch (cboCustomFiscalPeriodED.SelectedIndex)
			{
				case 0:	// Quarterly
					cboCustomFiscalQtyED.Items.AddRange(new object[]{"1", "2", "3", "4"});
					break;

				case 1: // Semi-Annually
					cboCustomFiscalQtyED.Items.AddRange(new object[]{"1", "2"});
					break;

				case 2: // Yearly
					cboCustomFiscalQtyED.Items.Add("1");
					break;
			}

			cboCustomFiscalQtyED.SelectedIndex = 0;

			DisplayDate(false);
		}

		private void optFixedSD_Click(object sender, System.EventArgs e)
		{
			if (optFixedSpecDateSD.Checked)	
				toggleFixedGroups(true, true);
			else
				toggleFixedGroups(true, false);

			DisplayDate(true);
		}

		private void optFixedED_Click(object sender, System.EventArgs e)
		{
			if (optFixedSpecDateED.Checked)	
				toggleFixedGroups(false, true);
			else
				toggleFixedGroups(false, false);

			DisplayDate(false);
		}

		private void toggleFixedGroups(bool isStartDate, bool isSpecificDate)
		{
			if (isStartDate)
			{
				optFixedSpecDateSD.Checked = isSpecificDate;
				optFixedFiscCalSD.Checked = !isSpecificDate;
				dtFixedSpecDateSD.Enabled = isSpecificDate;
				cbxFixedSpecDateTdySD.Enabled = isSpecificDate;
				cboFixedFisCalTypeSD.Enabled = !isSpecificDate;
				cboFixedFisCalPeriodSD.Enabled = !isSpecificDate;
				cboFixedFisCalQtySD.Enabled = !isSpecificDate;
				spinFixedFisCalYearSD.Enabled = !isSpecificDate;
			}
			else // End Date
			{
				optFixedSpecDateED.Checked = isSpecificDate;
				optFixedFiscCalED.Checked = !isSpecificDate;
				dtFixedSpecDateED.Enabled = isSpecificDate;
				cbxFixedSpecDateTdyED.Enabled = isSpecificDate;
				cboFixedFisCalTypeED.Enabled = !isSpecificDate;
				cboFixedFisCalPeriodED.Enabled = !isSpecificDate;
				cboFixedFisCalQtyED.Enabled = !isSpecificDate;
				spinFixedFisCalYearED.Enabled = !isSpecificDate;
			}
		}

		private void tabSD_Click(object sender, System.EventArgs e)
		{
			if (tabControlSD.SelectedTab == tabFixedSD)
			{
				SdCurrentType = DATE_TYPE.Fixed;

				if (!optAsOfStartDateED.Enabled)
					optAsOfStartDateED.Enabled = true;
			}
			else
			{
				SdCurrentType = DATE_TYPE.Relative;

				if (optAsOfStartDateED.Checked)
					optAsOfTodaySD.Checked = true;	// Set to Today.

				if (optAsOfEndDateSD.Checked)
					optAsOfStartDateED.Enabled = false;	// Disable SD in End Date Section
			}

			DisplayDate(true);
		}

		private void tabED_Click(object sender, System.EventArgs e)
		{
			if (tabControlED.SelectedTab == tabFixedED)
			{
				EdCurrentType = DATE_TYPE.Fixed;

				if (!optAsOfEndDateSD.Enabled)
					optAsOfEndDateSD.Enabled = true;
			}
			else
			{
				EdCurrentType = DATE_TYPE.Relative;

				if (optAsOfEndDateSD.Checked)
					optAsOfTodayED.Checked = true;	// Set to Today.

				if (optAsOfStartDateED.Checked)
					optAsOfEndDateSD.Enabled = false;	// Disable ED in Start Date Section
			}

			DisplayDate(false);
		}

		private void FixedPeriodicitySD_Click(object sender, System.EventArgs e)
		{
			cboFixedFisCalQtySD.Items.Clear();

			// Populate quantity combobox according to selected item
			switch (cboFixedFisCalPeriodSD.SelectedIndex)
			{
				case 0:	// Quarterly
					cboFixedFisCalQtySD.Items.AddRange(new object[]{"1", "2", "3", "4"});
					break;

				case 1: // Semi-Annually
					cboFixedFisCalQtySD.Items.AddRange(new object[]{"1", "2"});
					break;
									
				case 2: // Yearly
					cboFixedFisCalQtySD.Items.Add("1");
					break;
			}

			cboFixedFisCalQtySD.SelectedIndex = 0;
			DisplayDate(true);
		}

		private void FixedPeriodicityED_Click(object sender, System.EventArgs e)
		{
			cboFixedFisCalQtyED.Items.Clear();

			// Populate quantity combobox according to selected item
			switch (cboFixedFisCalPeriodED.SelectedIndex)
			{
				case 0:	// Quarterly
					cboFixedFisCalQtyED.Items.AddRange(new object[]{"1", "2", "3", "4"});
					break;

				case 1: // Semi-Annually
					cboFixedFisCalQtyED.Items.AddRange(new object[]{"1", "2"});
					break;
									
				case 2: // Yearly
					cboFixedFisCalQtyED.Items.Add("1");
					break;
			}

			cboFixedFisCalQtyED.SelectedIndex = 0;
			DisplayDate(false);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			SaveEnvironment();
			gIsFormSaved = true;
			this.Close();
		}

		private void frmSetDates_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!gIsFormSaved)
			{
				DialogResult result;

				result = MessageBox.Show("Closing this form will result in a loss of all changes. Do you wish to proceed?",
					"Last Chance", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
					this.Close();
				else
					e.Cancel = true;
			}
		}

		private void cbxFixedSpecDateTdyED_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (cbxFixedSpecDateTdyED.Checked)
				dtFixedSpecDateED.Enabled = false;
			else
				dtFixedSpecDateED.Enabled = true;
		}

		private void cbxFixedSpecDateTdySD_CheckStateChanged(object sender, System.EventArgs e)
		{
			if (cbxFixedSpecDateTdySD.Checked)
				dtFixedSpecDateSD.Enabled = false;
			else
				dtFixedSpecDateSD.Enabled = true;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			gIsFormSaved = false;
		}

		#region Public Functions
		
		public ExtendedDate GetExtendedDate(bool isStartDate)
		{
			if (isStartDate)
			{
				return SdExtendedDate;
			}
			else
			{
				return EdExtendedDate;
			}
		}

		public String GetDisplayedDate(bool isStartDate)
		{
			if (isStartDate)
			{
				return strDisplayedDateSD;
			}
			else
			{
				return strDisplayedDateED;
			}
		}

		public bool IsSaved()
		{
			return gIsFormSaved;
		}


		#endregion

	}
}
