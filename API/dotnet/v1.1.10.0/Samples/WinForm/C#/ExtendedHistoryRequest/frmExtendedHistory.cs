/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using System.Drawing;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	/// <summary> 
	/// Summary description for frmExtendedHistory.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.ToolTip directionsToolTip;
		private System.Windows.Forms.ListBox listboxFields;
		private System.Windows.Forms.ListBox listboxSecurities;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ComboBox cboMrktSector;
		private System.Windows.Forms.Button btnAddSec;
		private System.Windows.Forms.TextBox txtField;
		private System.Windows.Forms.Button btnAddField;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cboNonTradDayVal;
		private System.Windows.Forms.ComboBox cboPeriodicity;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cboSubMode;
		private System.Windows.Forms.CheckBox cbxRevChron;
		private System.Windows.Forms.CheckBox cbxDisplayAsYld;
		private System.Windows.Forms.Button btnSetDates;
		private System.Windows.Forms.Label lblStartDate;
		private System.Windows.Forms.Label lblEndDate;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox txtDisplay;
		private System.Windows.Forms.Button btnGetData;
		private System.Windows.Forms.Button btnClearOutput;
		private System.Windows.Forms.Button btnRemoveSec;
		private System.Windows.Forms.Button btnRemoveAllSec;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ComboBox cboTickerType;
		private System.Windows.Forms.ComboBox cboDisplayNonTradDay;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnRemoveAllFlds;
		private System.Windows.Forms.Button btnRemoveFld;
		private System.Windows.Forms.Button btnImportSecurities;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.ComboBox cboCurrency;
		private System.Windows.Forms.TextBox txtTicker;
		private System.ComponentModel.IContainer components;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region public enum SECTION

		public enum SECTION
		{
			Security,  
			Field,  
			Data_Output 
		};
		#endregion


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.directionsToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.btnRemoveSec = new System.Windows.Forms.Button();
			this.btnRemoveAllSec = new System.Windows.Forms.Button();
			this.btnRemoveAllFlds = new System.Windows.Forms.Button();
			this.btnRemoveFld = new System.Windows.Forms.Button();
			this.listboxFields = new System.Windows.Forms.ListBox();
			this.listboxSecurities = new System.Windows.Forms.ListBox();
			this.txtField = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.btnImportSecurities = new System.Windows.Forms.Button();
			this.cboTickerType = new System.Windows.Forms.ComboBox();
			this.cboMrktSector = new System.Windows.Forms.ComboBox();
			this.btnAddSec = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.btnAddField = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label11 = new System.Windows.Forms.Label();
			this.cboCurrency = new System.Windows.Forms.ComboBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.lblEndDate = new System.Windows.Forms.Label();
			this.lblStartDate = new System.Windows.Forms.Label();
			this.btnSetDates = new System.Windows.Forms.Button();
			this.cbxDisplayAsYld = new System.Windows.Forms.CheckBox();
			this.cbxRevChron = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cboDisplayNonTradDay = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.cboNonTradDayVal = new System.Windows.Forms.ComboBox();
			this.cboPeriodicity = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cboSubMode = new System.Windows.Forms.ComboBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.txtDisplay = new System.Windows.Forms.TextBox();
			this.btnGetData = new System.Windows.Forms.Button();
			this.btnClearOutput = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.txtTicker = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 478);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(668, 16);
			this.statusBar1.TabIndex = 1;
			// 
			// directionsToolTip
			// 
			this.directionsToolTip.AutoPopDelay = 5000;
			this.directionsToolTip.InitialDelay = 100;
			this.directionsToolTip.ReshowDelay = 100;
			// 
			// btnRemoveSec
			// 
			this.btnRemoveSec.BackColor = System.Drawing.SystemColors.Control;
			this.btnRemoveSec.Enabled = false;
			this.btnRemoveSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnRemoveSec.Location = new System.Drawing.Point(296, 69);
			this.btnRemoveSec.Name = "btnRemoveSec";
			this.btnRemoveSec.Size = new System.Drawing.Size(32, 23);
			this.btnRemoveSec.TabIndex = 5;
			this.btnRemoveSec.Text = "<";
			this.directionsToolTip.SetToolTip(this.btnRemoveSec, "Remove Selected Security");
			this.btnRemoveSec.Click += new System.EventHandler(this.btnRemoveSec_Click);
			// 
			// btnRemoveAllSec
			// 
			this.btnRemoveAllSec.BackColor = System.Drawing.SystemColors.Control;
			this.btnRemoveAllSec.Enabled = false;
			this.btnRemoveAllSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnRemoveAllSec.Location = new System.Drawing.Point(296, 97);
			this.btnRemoveAllSec.Name = "btnRemoveAllSec";
			this.btnRemoveAllSec.Size = new System.Drawing.Size(32, 23);
			this.btnRemoveAllSec.TabIndex = 6;
			this.btnRemoveAllSec.Text = "<<";
			this.directionsToolTip.SetToolTip(this.btnRemoveAllSec, "Remove all securities");
			this.btnRemoveAllSec.Click += new System.EventHandler(this.btnRemoveAllSec_Click);
			// 
			// btnRemoveAllFlds
			// 
			this.btnRemoveAllFlds.BackColor = System.Drawing.SystemColors.Control;
			this.btnRemoveAllFlds.Enabled = false;
			this.btnRemoveAllFlds.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnRemoveAllFlds.Location = new System.Drawing.Point(176, 97);
			this.btnRemoveAllFlds.Name = "btnRemoveAllFlds";
			this.btnRemoveAllFlds.Size = new System.Drawing.Size(32, 23);
			this.btnRemoveAllFlds.TabIndex = 12;
			this.btnRemoveAllFlds.Text = "<<";
			this.directionsToolTip.SetToolTip(this.btnRemoveAllFlds, "Remove all fields");
			this.btnRemoveAllFlds.Click += new System.EventHandler(this.btnRemoveAllFlds_Click);
			// 
			// btnRemoveFld
			// 
			this.btnRemoveFld.BackColor = System.Drawing.SystemColors.Control;
			this.btnRemoveFld.Enabled = false;
			this.btnRemoveFld.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnRemoveFld.Location = new System.Drawing.Point(176, 69);
			this.btnRemoveFld.Name = "btnRemoveFld";
			this.btnRemoveFld.Size = new System.Drawing.Size(32, 23);
			this.btnRemoveFld.TabIndex = 11;
			this.btnRemoveFld.Text = "<";
			this.directionsToolTip.SetToolTip(this.btnRemoveFld, "Remove Selected Field");
			this.btnRemoveFld.Click += new System.EventHandler(this.btnRemoveFld_Click);
			// 
			// listboxFields
			// 
			this.listboxFields.BackColor = System.Drawing.SystemColors.Window;
			this.listboxFields.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.listboxFields.Location = new System.Drawing.Point(7, 64);
			this.listboxFields.Name = "listboxFields";
			this.listboxFields.Size = new System.Drawing.Size(161, 95);
			this.listboxFields.TabIndex = 10;
			this.listboxFields.MouseHover += new System.EventHandler(this.listboxFields_MouseHover);
			// 
			// listboxSecurities
			// 
			this.listboxSecurities.BackColor = System.Drawing.SystemColors.Window;
			this.listboxSecurities.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.listboxSecurities.Location = new System.Drawing.Point(8, 64);
			this.listboxSecurities.Name = "listboxSecurities";
			this.listboxSecurities.Size = new System.Drawing.Size(280, 95);
			this.listboxSecurities.TabIndex = 4;
			this.listboxSecurities.MouseHover += new System.EventHandler(this.listboxSecurities_MouseHover);
			// 
			// txtField
			// 
			this.txtField.BackColor = System.Drawing.SystemColors.Window;
			this.txtField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtField.Location = new System.Drawing.Point(7, 39);
			this.txtField.Name = "txtField";
			this.txtField.Size = new System.Drawing.Size(161, 20);
			this.txtField.TabIndex = 8;
			this.txtField.Text = "";
			this.txtField.Enter += new System.EventHandler(this.txtField_Enter);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.btnRemoveAllSec);
			this.groupBox1.Controls.Add(this.btnRemoveSec);
			this.groupBox1.Controls.Add(this.btnImportSecurities);
			this.groupBox1.Controls.Add(this.cboTickerType);
			this.groupBox1.Controls.Add(this.txtTicker);
			this.groupBox1.Controls.Add(this.cboMrktSector);
			this.groupBox1.Controls.Add(this.listboxSecurities);
			this.groupBox1.Controls.Add(this.btnAddSec);
			this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(16, 13);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(384, 167);
			this.groupBox1.TabIndex = 19;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Securities ";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(208, 21);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 16);
			this.label7.TabIndex = 10;
			this.label7.Text = "Ticker Type:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(128, 21);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(72, 16);
			this.label6.TabIndex = 9;
			this.label6.Text = "Sector:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 21);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 16);
			this.label5.TabIndex = 8;
			this.label5.Text = "Security Name:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnImportSecurities
			// 
			this.btnImportSecurities.BackColor = System.Drawing.SystemColors.Control;
			this.btnImportSecurities.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnImportSecurities.Location = new System.Drawing.Point(296, 135);
			this.btnImportSecurities.Name = "btnImportSecurities";
			this.btnImportSecurities.Size = new System.Drawing.Size(80, 23);
			this.btnImportSecurities.TabIndex = 7;
			this.btnImportSecurities.Text = "Import...";
			this.btnImportSecurities.Click += new System.EventHandler(this.btnImportSecurities_Click);
			// 
			// cboTickerType
			// 
			this.cboTickerType.BackColor = System.Drawing.SystemColors.Window;
			this.cboTickerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboTickerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboTickerType.Location = new System.Drawing.Point(206, 39);
			this.cboTickerType.Name = "cboTickerType";
			this.cboTickerType.Size = new System.Drawing.Size(106, 21);
			this.cboTickerType.TabIndex = 2;
			// 
			// cboMrktSector
			// 
			this.cboMrktSector.BackColor = System.Drawing.SystemColors.Window;
			this.cboMrktSector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMrktSector.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboMrktSector.Location = new System.Drawing.Point(125, 39);
			this.cboMrktSector.Name = "cboMrktSector";
			this.cboMrktSector.Size = new System.Drawing.Size(78, 21);
			this.cboMrktSector.TabIndex = 1;
			// 
			// btnAddSec
			// 
			this.btnAddSec.BackColor = System.Drawing.SystemColors.Control;
			this.btnAddSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnAddSec.Location = new System.Drawing.Point(320, 39);
			this.btnAddSec.Name = "btnAddSec";
			this.btnAddSec.Size = new System.Drawing.Size(56, 22);
			this.btnAddSec.TabIndex = 3;
			this.btnAddSec.Text = "Add";
			this.btnAddSec.Click += new System.EventHandler(this.btnAddSec_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.btnRemoveAllFlds);
			this.groupBox2.Controls.Add(this.btnRemoveFld);
			this.groupBox2.Controls.Add(this.btnAddField);
			this.groupBox2.Controls.Add(this.txtField);
			this.groupBox2.Controls.Add(this.listboxFields);
			this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(411, 13);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(241, 167);
			this.groupBox2.TabIndex = 20;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Fields";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label8.Location = new System.Drawing.Point(8, 21);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 16);
			this.label8.TabIndex = 13;
			this.label8.Text = "Field Mnemonic:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnAddField
			// 
			this.btnAddField.BackColor = System.Drawing.SystemColors.Control;
			this.btnAddField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnAddField.Location = new System.Drawing.Point(176, 39);
			this.btnAddField.Name = "btnAddField";
			this.btnAddField.Size = new System.Drawing.Size(56, 22);
			this.btnAddField.TabIndex = 9;
			this.btnAddField.Text = "Add";
			this.btnAddField.Click += new System.EventHandler(this.btnAddField_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Controls.Add(this.cboCurrency);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.lblEndDate);
			this.groupBox3.Controls.Add(this.lblStartDate);
			this.groupBox3.Controls.Add(this.btnSetDates);
			this.groupBox3.Controls.Add(this.cbxDisplayAsYld);
			this.groupBox3.Controls.Add(this.cbxRevChron);
			this.groupBox3.Controls.Add(this.label3);
			this.groupBox3.Controls.Add(this.cboDisplayNonTradDay);
			this.groupBox3.Controls.Add(this.label2);
			this.groupBox3.Controls.Add(this.cboNonTradDayVal);
			this.groupBox3.Controls.Add(this.cboPeriodicity);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.label1);
			this.groupBox3.Controls.Add(this.cboSubMode);
			this.groupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox3.Location = new System.Drawing.Point(13, 196);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(302, 271);
			this.groupBox3.TabIndex = 26;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Properties";
			this.groupBox3.Enter += new System.EventHandler(this.SetGetDataControlAsAcceptButton);
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label11.Location = new System.Drawing.Point(12, 167);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(144, 16);
			this.label11.TabIndex = 44;
			this.label11.Text = "Currency:";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboCurrency
			// 
			this.cboCurrency.BackColor = System.Drawing.SystemColors.Window;
			this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboCurrency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboCurrency.Location = new System.Drawing.Point(12, 185);
			this.cboCurrency.Name = "cboCurrency";
			this.cboCurrency.Size = new System.Drawing.Size(142, 21);
			this.cboCurrency.TabIndex = 43;
			// 
			// label10
			// 
			this.label10.BackColor = System.Drawing.SystemColors.Control;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label10.ForeColor = System.Drawing.Color.Black;
			this.label10.Location = new System.Drawing.Point(14, 85);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(54, 20);
			this.label10.TabIndex = 42;
			this.label10.Text = "EndDate: ";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label9
			// 
			this.label9.BackColor = System.Drawing.SystemColors.Control;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label9.ForeColor = System.Drawing.Color.Black;
			this.label9.Location = new System.Drawing.Point(13, 58);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(55, 20);
			this.label9.TabIndex = 41;
			this.label9.Text = "StartDate: ";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblEndDate
			// 
			this.lblEndDate.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(221)), ((System.Byte)(216)));
			this.lblEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEndDate.Location = new System.Drawing.Point(69, 85);
			this.lblEndDate.Name = "lblEndDate";
			this.lblEndDate.Size = new System.Drawing.Size(220, 20);
			this.lblEndDate.TabIndex = 40;
			this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblStartDate
			// 
			this.lblStartDate.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(224)), ((System.Byte)(221)), ((System.Byte)(216)));
			this.lblStartDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblStartDate.Location = new System.Drawing.Point(69, 58);
			this.lblStartDate.Name = "lblStartDate";
			this.lblStartDate.Size = new System.Drawing.Size(220, 20);
			this.lblStartDate.TabIndex = 39;
			this.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnSetDates
			// 
			this.btnSetDates.BackColor = System.Drawing.SystemColors.Control;
			this.btnSetDates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnSetDates.Location = new System.Drawing.Point(12, 26);
			this.btnSetDates.Name = "btnSetDates";
			this.btnSetDates.Size = new System.Drawing.Size(88, 23);
			this.btnSetDates.TabIndex = 13;
			this.btnSetDates.Text = "Set Dates...";
			this.btnSetDates.Click += new System.EventHandler(this.btnSetDates_Click);
			// 
			// cbxDisplayAsYld
			// 
			this.cbxDisplayAsYld.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbxDisplayAsYld.Location = new System.Drawing.Point(160, 167);
			this.cbxDisplayAsYld.Name = "cbxDisplayAsYld";
			this.cbxDisplayAsYld.Size = new System.Drawing.Size(107, 26);
			this.cbxDisplayAsYld.TabIndex = 17;
			this.cbxDisplayAsYld.Text = "DisplayAsYield";
			// 
			// cbxRevChron
			// 
			this.cbxRevChron.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbxRevChron.Location = new System.Drawing.Point(160, 186);
			this.cbxRevChron.Name = "cbxRevChron";
			this.cbxRevChron.Size = new System.Drawing.Size(135, 26);
			this.cbxRevChron.TabIndex = 18;
			this.cbxRevChron.Text = "ReverseChronological";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(13, 219);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(130, 16);
			this.label3.TabIndex = 33;
			this.label3.Text = "DisplayNonTradingDays:";
			// 
			// cboDisplayNonTradDay
			// 
			this.cboDisplayNonTradDay.BackColor = System.Drawing.SystemColors.Window;
			this.cboDisplayNonTradDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDisplayNonTradDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboDisplayNonTradDay.Location = new System.Drawing.Point(12, 236);
			this.cboDisplayNonTradDay.Name = "cboDisplayNonTradDay";
			this.cboDisplayNonTradDay.Size = new System.Drawing.Size(140, 21);
			this.cboDisplayNonTradDay.TabIndex = 19;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(156, 218);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(118, 16);
			this.label2.TabIndex = 31;
			this.label2.Text = "NonTradingDayValue:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboNonTradDayVal
			// 
			this.cboNonTradDayVal.BackColor = System.Drawing.SystemColors.Window;
			this.cboNonTradDayVal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboNonTradDayVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboNonTradDayVal.Location = new System.Drawing.Point(158, 236);
			this.cboNonTradDayVal.Name = "cboNonTradDayVal";
			this.cboNonTradDayVal.Size = new System.Drawing.Size(135, 21);
			this.cboNonTradDayVal.TabIndex = 16;
			// 
			// cboPeriodicity
			// 
			this.cboPeriodicity.BackColor = System.Drawing.SystemColors.Window;
			this.cboPeriodicity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboPeriodicity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboPeriodicity.Location = new System.Drawing.Point(160, 135);
			this.cboPeriodicity.Name = "cboPeriodicity";
			this.cboPeriodicity.Size = new System.Drawing.Size(132, 21);
			this.cboPeriodicity.TabIndex = 15;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(156, 118);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 28;
			this.label4.Text = "Periodicity:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 117);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 16);
			this.label1.TabIndex = 27;
			this.label1.Text = "Subscription Mode:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboSubMode
			// 
			this.cboSubMode.BackColor = System.Drawing.SystemColors.Window;
			this.cboSubMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSubMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cboSubMode.Location = new System.Drawing.Point(12, 134);
			this.cboSubMode.Name = "cboSubMode";
			this.cboSubMode.Size = new System.Drawing.Size(142, 21);
			this.cboSubMode.TabIndex = 14;
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.txtDisplay);
			this.groupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox4.Location = new System.Drawing.Point(322, 196);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(331, 240);
			this.groupBox4.TabIndex = 36;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Data Output";
			this.groupBox4.Enter += new System.EventHandler(this.SetGetDataControlAsAcceptButton);
			// 
			// txtDisplay
			// 
			this.txtDisplay.AcceptsReturn = true;
			this.txtDisplay.BackColor = System.Drawing.SystemColors.Window;
			this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtDisplay.Location = new System.Drawing.Point(10, 28);
			this.txtDisplay.Multiline = true;
			this.txtDisplay.Name = "txtDisplay";
			this.txtDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtDisplay.Size = new System.Drawing.Size(314, 204);
			this.txtDisplay.TabIndex = 37;
			this.txtDisplay.Text = "";
			// 
			// btnGetData
			// 
			this.btnGetData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGetData.BackColor = System.Drawing.SystemColors.Control;
			this.btnGetData.Location = new System.Drawing.Point(580, 440);
			this.btnGetData.Name = "btnGetData";
			this.btnGetData.Size = new System.Drawing.Size(72, 27);
			this.btnGetData.TabIndex = 22;
			this.btnGetData.Text = "Get Data";
			this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
			// 
			// btnClearOutput
			// 
			this.btnClearOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClearOutput.Location = new System.Drawing.Point(322, 440);
			this.btnClearOutput.Name = "btnClearOutput";
			this.btnClearOutput.Size = new System.Drawing.Size(78, 27);
			this.btnClearOutput.TabIndex = 21;
			this.btnClearOutput.Text = "Clear Output";
			this.btnClearOutput.Click += new System.EventHandler(this.btnClearOutput_Click);
			// 
			// txtTicker
			// 
			this.txtTicker.BackColor = System.Drawing.SystemColors.Window;
			this.txtTicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtTicker.Location = new System.Drawing.Point(8, 39);
			this.txtTicker.Name = "txtTicker";
			this.txtTicker.Size = new System.Drawing.Size(112, 20);
			this.txtTicker.TabIndex = 0;
			this.txtTicker.Text = "";
			this.directionsToolTip.SetToolTip(this.txtTicker, "Enter ticker and exchange code (optional)");
			this.txtTicker.Enter += new System.EventHandler(this.txtTicker_Enter);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(668, 494);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.btnClearOutput);
			this.Controls.Add(this.btnGetData);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Extended History using API for .NET";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private Security _selSecurity; 
		private Field _selField;
		private RequestForHistory request = null;
		private String importSecurityDialogDir = "";
		private long _errCounter;
		private FieldTable ftbl = null;
		private StringBuilder strDisplay = null;
		private DateOffset SdOffset = null;
		private DateAnchor SdAnchor = null;
		private ExtendedDate SdExtDate = null;
		private ExtendedDate EdExtDate = null;
		private string[] arrIsoCodes = null;

		private void Form1_Load(object sender, System.EventArgs e)
		{
			// Subscribe for any status event messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnHistory_StatusEvent);

			// Now, startup the MarketDataAdapter.
			// This will cause several things to happen:
			//   - Bloomberg configuration information will be acquired from the 
			//	   environment (Windows Registry, config files, the Bloomberg terminal, etc)
			//	 - A connection to the local bbcomm will be established.
			//	 - BBCOMM protocol handshaking will be initiated.
			//	 - Adapter resources will be allocated.
			try
			{
				// The ThreadSafeResponseInterceptor ensures that the status and reply
				// event handlers are called on the user interface threads. 
				MarketDataAdapter.Startup(new ThreadSafeResponseInterceptor(this));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Close();
			}

			// Subscribe for any reply event messages
			MarketDataAdapter.ReplyEvent += new ReplyEventHandler(OnHistory_ReplyEvent);

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapter.Startup method
			ftbl = MarketDataAdapter.FieldTable;

			// Populate Market Sector dropdown list
			String[] arrMarketSectors = Enum.GetNames(typeof(MARKETSECTOR));
			Array.Sort(arrMarketSectors);
			cboMrktSector.Items.AddRange(arrMarketSectors);
			cboMrktSector.SelectedIndex = 5;	// Set to "Equity"

			// Populate Ticker Type dropdown list
			String[] arrTickerTypes = Enum.GetNames(typeof(QUALIFIER));
			Array.Sort(arrTickerTypes);
			cboTickerType.Items.AddRange(arrTickerTypes);
			cboTickerType.SelectedIndex = 35; // Set to "Ticker-Exchange"

			// Populate SubscriptionMode combobox with enumerator values
			cboSubMode.DataSource = Enum.GetValues(typeof(SubscriptionMode));
			cboSubMode.SelectedIndex = 0; // Set to "ByField"

			// Populate Periodicity combobox with enumerator values
			cboPeriodicity.DataSource = Enum.GetValues(typeof(Periodicity));
			cboPeriodicity.SelectedIndex = 0; // Set to "ActualDaily"
	
			// Populate Currency combobox with ISO Codes and select USD as default
			arrIsoCodes = CurrencyTable.Instance.GetISOCodes();
			cboCurrency.DataSource = arrIsoCodes;
			cboCurrency.SelectedItem = "USD";  

			// Populate DisplayNonTradingDays combobox with enumerator values
			cboDisplayNonTradDay.DataSource = Enum.GetValues(typeof(DisplayNonTradingDays));
			cboDisplayNonTradDay.SelectedIndex = 2; // Set to "AllCalendar"
	
			// Populate NonTradingDayValue combobox with enumerator values
			cboNonTradDayVal.DataSource = Enum.GetValues(typeof(NonTradingDayValue));
			cboNonTradDayVal.SelectedIndex = 1; // Set to "PreviousDay"

			// Set default date ranges for Start date (End date is today by default).
			SdOffset = new DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day); 
			SdAnchor = null;	// Set to TODAY in relative date.
			SdExtDate = new ExtendedDate(SdAnchor, SdOffset);
			EdExtDate = new ExtendedDate();

			// Display values in Start/End date labels
			lblStartDate.Text = "-5AD";
			lblEndDate.Text = "Today";

			importSecurityDialogDir = "c:\\";

			statusBar1.ShowPanels = false;
		}

		private void OnHistory_ReplyEvent(Reply reply)
		{			
			if(reply.ReplyError != null)
			{
				statusBar1.Text = "Reply Error has occurred: " + 
					reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description;
				return;
			}

			_errCounter = 0;

			strDisplay = new StringBuilder();
			strDisplay.Append(txtDisplay.Text.ToString() + "\r\n");

			try
			{
				// Loop through each security
				foreach(SecurityDataItem sdi in reply.GetSecurityDataItems())
				{
					strDisplay.Append(sdi.Security.FullName.ToString() + ":" + "\r\n");

					// Loop through each field
					foreach(FieldDataItem fdi in sdi.FieldsData)
					{
						strDisplay.Append(fdi.Field.Mnemonic.ToString() + ":" + "\r\n");

						// Loop through each data point
						foreach(DataPoint dp in fdi.DataPoints)
						{
							if (dp != null)
							{
								if(dp.IsError)
								{
									strDisplay.Append(dp.ReplyError.DisplayName);
									_errCounter++;
								}
								else
								{
									strDisplay.Append(dp.Time.ToShortDateString() + " - " + dp.Value + "\r\n");
									statusBar1.Text = "Receiving data...";
								}
							}
						}
						strDisplay.Append("\r\n");
					}
				}
				txtDisplay.Text = strDisplay.ToString();
				statusBar1.Text = " Finished receiving data with " + _errCounter + " error(s).";
			}
			catch (Exception ex)
			{
				statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message;
			}
		}

		private void OnHistory_StatusEvent(StatusCode status, string description)
		{
			statusBar1.Text = "Status: " + status + "  Desc: " + description;
		}

		private void btnGetData_Click(object sender, System.EventArgs e)
		{
			// Clear output textbox
			txtDisplay.Clear();

			// Check that at least one security has been selected
			if (listboxSecurities.Items.Count < 1)
			{
				MessageBox.Show(this, "You must first add at least one security to the request before sending", "Missing Information");
				txtTicker.Select();
				return;
			}

			// Check that at least one field has been selected
			if (listboxFields.Items.Count < 1)
			{
				MessageBox.Show(this, "You must first add at least one field to the request before sending", "Missing Information");
				txtField.Select();
				return;
			}

			// Issue historical request
			request = new RequestForHistory();

			// Add securities to RequestForHistory object
			foreach (String s in listboxSecurities.Items)
			{
				request.Securities.Add(s);
			}

			// Add fields to RequestForHistory object
			foreach (Field f in listboxFields.Items)
			{
				request.Fields.Add(f);
			}

			// Set RequestForHistory parameters
			//request.Currencies.Add(new Currency("USD"));
			request.Periodicity = (Periodicity)cboPeriodicity.SelectedValue;
			request.SubscriptionMode = (SubscriptionMode)cboSubMode.SelectedValue;
			request.DisplayNonTradingDays = (DisplayNonTradingDays)cboDisplayNonTradDay.SelectedValue;
			request.NonTradingDayValue = (NonTradingDayValue)cboNonTradDayVal.SelectedValue;
			request.ReverseChronological = cbxRevChron.Checked;
			request.DisplayAsYield = cbxDisplayAsYld.Checked;
			request.Currencies.Add(arrIsoCodes[cboCurrency.SelectedIndex]);
			
			DateOffset offset = new DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day);
			request.StartDate = SdExtDate;
			request.EndDate = EdExtDate;
			statusBar1.Text = "Submitting request(s)...";

			MarketDataAdapter.SendRequest(request);
		}

		private void txtTicker_Enter(object sender, System.EventArgs e)
		{
			// Set Security Add button to accept Enter key
			this.AcceptButton = this.btnAddSec;
		}

		private void txtField_Enter(object sender, System.EventArgs e)
		{
			// Set Field Add button to accept Enter key
			this.AcceptButton = this.btnAddField;
		}

		private void SetGetDataControlAsAcceptButton(object sender, System.EventArgs e)
		{
			// Set Field Add button to accept Enter key
			this.AcceptButton = this.btnGetData;
		}

		private void btnAddField_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (txtField.Text != string.Empty)
				{
					// An exception will be thrown if field is not found in the Field Table
					_selField = ftbl[txtField.Text.ToUpper()];	

					listboxFields.Items.Add(_selField);
					listboxFields.SetSelected(listboxFields.Items.Count - 1, true);
					ToggleRemoveButtons(true, SECTION.Field);
					txtField.SelectAll();
				}
			}
			catch
			{
				MessageBox.Show(txtField.Text.ToUpper() + " is an invalid field. Please try again.", "Field Not Found");
				txtField.SelectAll();
			}
		}

		private void btnAddSec_Click(object sender, System.EventArgs e)
		{
			if (txtTicker.Text != string.Empty)
			{
				// Create Security object based on selected security 
				_selSecurity = new Security(txtTicker.Text.ToUpper() + " " + cboMrktSector.SelectedItem.ToString() + " " + 
					cboTickerType.SelectedItem.ToString());
				
				if (_selSecurity.Qualifier.CompareTo(QUALIFIER.TICKERX) == 0)	// If "Tickerx" is selected (default), assign empty string
					listboxSecurities.Items.Add(_selSecurity.Ticker + " " + _selSecurity.MarketSector);
				else
					listboxSecurities.Items.Add(_selSecurity.FullName);

				listboxSecurities.SetSelected(listboxSecurities.Items.Count - 1, true);
				ToggleRemoveButtons(true, SECTION.Security);
				txtTicker.SelectAll();
			}
		}

		private void btnClearOutput_Click(object sender, System.EventArgs e)
		{
			// Clear output textbox
			txtDisplay.Clear();
		}

		private void btnImportSecurities_Click(object sender, System.EventArgs e)
		{
			Stream myStream;
			bool isFileValid = false;

			OpenFileDialog openFileDialog1 = new OpenFileDialog();
			openFileDialog1.Title = "Import List of Securities";
			openFileDialog1.InitialDirectory = importSecurityDialogDir;
			openFileDialog1.Filter = "txt files (*.txt)|*.txt" ;

			if(openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				importSecurityDialogDir = openFileDialog1.FileName;

				if((myStream = openFileDialog1.OpenFile())!= null)
				{
					StreamReader file = new StreamReader(openFileDialog1.FileName);
					String oneLine = "";
					oneLine = file.ReadLine();

					while (oneLine != string.Empty && oneLine != null)
					{
						listboxSecurities.Items.Add(oneLine);
						oneLine = file.ReadLine();
						isFileValid = true;
					}

					file.Close();
				}

				if (!isFileValid)
				{
					MessageBox.Show("Make sure you have one security per line with no empty lines", "File Import Failure");
				}
				else
				{
					listboxSecurities.SetSelected(0, true);
					ToggleRemoveButtons(true, SECTION.Security);
				}
			}
		}

		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			MarketDataAdapter.Shutdown();
		}

		private void btnRemoveAllSec_Click(object sender, System.EventArgs e)
		{
			DialogResult dr = MessageBox.Show(this, "Are you sure you want to delete all of the securities in this list?",
				"Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

			if (dr == DialogResult.Yes)
			{
				listboxSecurities.Items.Clear();
				ToggleRemoveButtons(false, SECTION.Security);
			}
		}

		private void btnRemoveSec_Click(object sender, System.EventArgs e)
		{
			listboxSecurities.Items.RemoveAt(listboxSecurities.SelectedIndex);

			if (listboxSecurities.Items.Count < 1)
				ToggleRemoveButtons(false, SECTION.Security);
			else
				listboxSecurities.SetSelected(0, true);
		}

		private void btnRemoveAllFlds_Click(object sender, System.EventArgs e)
		{
			DialogResult dr = MessageBox.Show(this, "Are you sure you want to delete all of the fields in this list?",
				"Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

			if (dr == DialogResult.Yes)
			{
				listboxFields.Items.Clear();
				ToggleRemoveButtons(false, SECTION.Field);
			}
		}

		private void btnRemoveFld_Click(object sender, System.EventArgs e)
		{
			listboxFields.Items.RemoveAt(listboxFields.SelectedIndex);

			if (listboxFields.Items.Count < 1)
				ToggleRemoveButtons(false, SECTION.Field);
			else
				listboxFields.SetSelected(0, true);
		}

		private void ToggleRemoveButtons(bool enableButtons, SECTION sect)
		{
			switch (sect)
			{
				case SECTION.Field:
					btnRemoveFld.Enabled = enableButtons;
					btnRemoveAllFlds.Enabled = enableButtons;
					break;

				case SECTION.Security:
					btnRemoveSec.Enabled = enableButtons;
					btnRemoveAllSec.Enabled = enableButtons;
					break;
			}
		}

		private void listboxSecurities_MouseHover(object sender, System.EventArgs e)
		{
			directionsToolTip.SetToolTip(listboxSecurities, "There are currently " + 
				listboxSecurities.Items.Count.ToString() + " securities in this list.");
		}

		private void listboxFields_MouseHover(object sender, System.EventArgs e)
		{
			directionsToolTip.SetToolTip(listboxFields, "There are currently " + 
				listboxFields.Items.Count.ToString() + " fields in this list.");
		}

		private void btnSetDates_Click(object sender, System.EventArgs e)
		{
			frmSetDates frmDates = new frmSetDates(SdExtDate, EdExtDate);	
			frmDates.ShowDialog();

			if (frmDates.IsSaved())
			{
				lblStartDate.Text = frmDates.GetDisplayedDate(true);	
				lblEndDate.Text = frmDates.GetDisplayedDate(false);	
				SdExtDate = frmDates.GetExtendedDate(true);
				EdExtDate = frmDates.GetExtendedDate(false);
			}

			frmDates = null;
		}
	}
}
