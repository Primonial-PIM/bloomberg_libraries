/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;
using System.Text;

namespace Bloomberg.Api.Samples
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ListBox listboxFields;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ListBox listboxSecurities;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cboSubMode;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.Button btnGetData;
		private System.Windows.Forms.TextBox txtDisplay;
		private System.Windows.Forms.ToolTip directionsToolTip;
		private System.Windows.Forms.Label lblDataOutput;
		private System.ComponentModel.IContainer components;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				MarketDataAdapter.Shutdown();
				
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.lblDataOutput = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.listboxFields = new System.Windows.Forms.ListBox();
			this.label3 = new System.Windows.Forms.Label();
			this.listboxSecurities = new System.Windows.Forms.ListBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cboSubMode = new System.Windows.Forms.ComboBox();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.btnGetData = new System.Windows.Forms.Button();
			this.txtDisplay = new System.Windows.Forms.TextBox();
			this.directionsToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// lblDataOutput
			// 
			this.lblDataOutput.Location = new System.Drawing.Point(8, 166);
			this.lblDataOutput.Name = "lblDataOutput";
			this.lblDataOutput.Size = new System.Drawing.Size(408, 16);
			this.lblDataOutput.TabIndex = 27;
			this.lblDataOutput.Text = "Data Output:";
			this.lblDataOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(432, 40);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(24, 20);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 24;
			this.pictureBox1.TabStop = false;
			// 
			// listboxFields
			// 
			this.listboxFields.Location = new System.Drawing.Point(221, 37);
			this.listboxFields.Name = "listboxFields";
			this.listboxFields.Size = new System.Drawing.Size(200, 69);
			this.listboxFields.TabIndex = 23;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(221, 19);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(144, 16);
			this.label3.TabIndex = 22;
			this.label3.Text = "Fields:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// listboxSecurities
			// 
			this.listboxSecurities.Location = new System.Drawing.Point(8, 37);
			this.listboxSecurities.Name = "listboxSecurities";
			this.listboxSecurities.Size = new System.Drawing.Size(200, 69);
			this.listboxSecurities.TabIndex = 21;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 18);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(144, 16);
			this.label2.TabIndex = 20;
			this.label2.Text = "Securities:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 118);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 16);
			this.label1.TabIndex = 19;
			this.label1.Text = "Subscription Mode:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cboSubMode
			// 
			this.cboSubMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSubMode.Location = new System.Drawing.Point(8, 136);
			this.cboSubMode.Name = "cboSubMode";
			this.cboSubMode.Size = new System.Drawing.Size(176, 21);
			this.cboSubMode.TabIndex = 18;
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 277);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(472, 16);
			this.statusBar1.TabIndex = 17;
			// 
			// btnGetData
			// 
			this.btnGetData.Location = new System.Drawing.Point(352, 128);
			this.btnGetData.Name = "btnGetData";
			this.btnGetData.Size = new System.Drawing.Size(72, 32);
			this.btnGetData.TabIndex = 16;
			this.btnGetData.Text = "Get Data";
			this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
			// 
			// txtDisplay
			// 
			this.txtDisplay.Location = new System.Drawing.Point(8, 186);
			this.txtDisplay.Multiline = true;
			this.txtDisplay.Name = "txtDisplay";
			this.txtDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtDisplay.Size = new System.Drawing.Size(416, 72);
			this.txtDisplay.TabIndex = 29;
			this.txtDisplay.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(472, 293);
			this.Controls.Add(this.txtDisplay);
			this.Controls.Add(this.lblDataOutput);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.listboxFields);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.listboxSecurities);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cboSubMode);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.btnGetData);
			this.Name = "Form1";
			this.Text = "Intraday Raw Tick Request using API for .NET";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private string[] _securities = null;
		private Field[] _fields = null;
		private string _selSecurity = ""; 
		private Field _selField = null;
		private RequestForIntradayRaw rawReq = null;
		private long _errCounter;
		private FieldTable ftbl = null;
		private StringBuilder strDisplay = null;

		private void Form1_Load(object sender, System.EventArgs e)
		{
			// Directions on how to run sample available as tooltip on pictureBox question-mark.
			directionsToolTip.SetToolTip(this.pictureBox1, 
				"Instructions: Select one field/security pairing, along with a SubscriptionMode, and then click the Get Data button.");

			// Subscribe for any status event messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnIntradayRaw_StatusEvent);

			// Now, startup the MarketDataAdapter.
			// This will cause several things to happen:
			//   - Bloomberg configuration information will be acquired from the 
			//	   environment (Windows Registry, config files, the Bloomberg terminal, etc)
			//	 - A connection to the local bbcomm will be established.
			//	 - BBCOMM protocol handshaking will be initiated.
			//	 - Adapter resources will be allocated.
			try
			{
				// The ThreadSafeResponseInterceptor ensures that the status and reply
				// event handlers are called on the user interface threads. 
				MarketDataAdapter.Startup(new ThreadSafeResponseInterceptor(this));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Close();
			}

			// Subscribe for any reply event messages
			MarketDataAdapter.ReplyEvent += new ReplyEventHandler(OnIntradayRaw_ReplyEvent);

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapter.Startup method
			ftbl = MarketDataAdapter.FieldTable;

			_fields = new Field[]
				{ftbl["LAST_PRICE"], ftbl["BEST_BID"], ftbl["BEST_ASK"], ftbl["SIZE_LAST_TRADE"], ftbl["ALL_PRICE_COND_CODE"]};

			_securities = new string[]{
				"MSFT US Equity", "IBM US Equity", "GOOG US Equity", "SIRI US Equity", "CSCO US Equity"};

			// Populate security listbox
			foreach (string s in _securities)
			{
				listboxSecurities.Items.Add(s);
			}

			// Populate field listbox
			foreach (Field f in _fields)
			{
				listboxFields.Items.Add(f.Mnemonic);
			}

			listboxSecurities.SetSelected(0, true);
			listboxFields.SetSelected(0, true);

			// Populate SubscriptionMode combobox with enumerator values
			cboSubMode.DataSource = Enum.GetValues(typeof(SubscriptionMode));

			statusBar1.ShowPanels = false;
		}
		

		private void OnIntradayRaw_ReplyEvent(Reply reply)
		{			
			if(reply.ReplyError != null)
			{
				statusBar1.Text = "Reply Error has occurred: " + 
					reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description;
				return;
			}

			_errCounter = 0;

			try
			{
				strDisplay = new StringBuilder();

				// Loop through each security
				foreach(SecurityDataItem sdi in reply.GetSecurityDataItems())
				{
					// Loop through each field
					foreach(FieldDataItem fdi in sdi.FieldsData)
					{
						// Loop through each data point
						foreach(DataPoint dp in fdi.DataPoints)
						{
							if(dp != null)
							{
								if(dp.IsError)
								{
									strDisplay.Append(dp.ReplyError.DisplayName + "/r/n");
									_errCounter++;
								}
								else
								{
									if(dp.HasChildren)
									{
										string output = null;
										foreach(DataPoint dp2 in dp.DataPoints)
										{
											output += dp2.ToString(); 
										}

										if(dp.Time != System.DateTime.MinValue)
										{
											strDisplay.Append(dp.Time.ToString() + ": " + output + "\r\n");
										}
										else
										{
											strDisplay.Append(output + "\r\n");
										}
									}
									else
									{
										if(dp.Time != System.DateTime.MinValue)
										{
											strDisplay.Append(dp.Time.ToString() + ": " + dp.ToString() + "\r\n");
										}
										else
										{
											strDisplay.Append(dp.ToString() + "\r\n");
										}
									}
									statusBar1.Text = "Receiving data...";
								}
							}
						}
					}
				}				
				txtDisplay.Text = strDisplay.ToString();
				statusBar1.Text = " Finished receiving data with " + _errCounter + " error(s).";
			}
			catch (Exception ex)
			{
				statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message;
			}
		}

		private void OnIntradayRaw_StatusEvent(StatusCode status, string description)
		{
			statusBar1.Text = "Status: " + status + "  Desc: " + description;
		}

		private void btnGetData_Click(object sender, System.EventArgs e)
		{
			// Clear textbox
			txtDisplay.Clear();

			// Check that a security has been selected
			if (listboxSecurities.SelectedIndex != -1)
				_selSecurity = listboxSecurities.SelectedItem.ToString();

			// Check that a main field has been selected
			if (listboxFields.SelectedIndex != -1)
				_selField = _fields[listboxFields.SelectedIndex];

			statusBar1.Text = "Preparing request...";

			Subscribe();
		}

		private void Subscribe()
		{
			// Issue intraday raw tick request
			rawReq = new RequestForIntradayRaw();

			// Add fields and security to RequestForIntradayRaw object
			rawReq.Fields.Add(_selField);
			rawReq.Securities.Add(_selSecurity);

			// Set RequestForIntradayRaw parameters
			rawReq.StartDateTime = DateTime.Now.Subtract(new TimeSpan(0, 0, 30, 0));
			rawReq.EndDateTime = DateTime.Now;
			rawReq.SubscriptionMode = (SubscriptionMode)cboSubMode.SelectedValue;
			rawReq.ReverseChronological = false;

			MarketDataAdapter.SendRequest(rawReq);

			lblDataOutput.Text = "Data Output (From " + rawReq.StartDateTime + " to " + rawReq.EndDateTime + "):";
		}
	}
}
