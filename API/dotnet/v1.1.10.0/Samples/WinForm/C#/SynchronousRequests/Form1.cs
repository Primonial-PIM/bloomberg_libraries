/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;
using Bloomberg.Api;

namespace Bloomberg.Api.Samples
{
	/// <summary>
	/// This application allows you to enter any Request that can be handled by Bloomberg API
	/// MarketDataAdapter in either Synchronous or Asynchronous mode. It uses the Reply.ToDataSet()
	/// method to convert any Reply object that is returned by the MarketDataAdapter into a 
	/// standard .NET DataSet object - consisting of 1 or more tables.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		#region Private WinForm properties
		private System.Windows.Forms.Panel panelInputs;
		private System.Windows.Forms.ListBox listBoxSecurities;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGrid dataGridResults;
		private System.Windows.Forms.TabPage tabPageStatic;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.Button btnRequest;
		private System.Windows.Forms.TabPage tabPageHistory;
		private System.Windows.Forms.TabPage tabPageIntradayBars;
		private System.Windows.Forms.TabPage tabPageIntradayRaw;
		private System.Windows.Forms.ListBox listBoxLog;
		private System.Windows.Forms.ListBox listBoxStaticFields;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DateTimePicker dateTimeHistoryStart;
		private System.Windows.Forms.DateTimePicker dateTimeHistoryEnd;
		private System.Windows.Forms.ListBox listBoxInterdayFields;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButtonSynch;
		private System.Windows.Forms.RadioButton radioButtonAsynch;
		private System.Windows.Forms.ListBox listBoxIntradayBarsFields;
		private System.Windows.Forms.DateTimePicker dateTimePickerIntradayBarsStart;
		private System.Windows.Forms.DateTimePicker dateTimePickerIntradayBarsEnd;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.DateTimePicker dateTimePickerIntradayRawEnd;
		private System.Windows.Forms.DateTimePicker dateTimePickerIntradayRawStart;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown numericUpDownIntradayBars;
		private System.Windows.Forms.ListBox listBoxIntradayRawFields;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
#endregion

		#region Form1 Constructor
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Setup MarketDataAdapter Event handlers
			MarketDataAdapter.StatusEvent +=new StatusEventHandler(MarketDataAdapter_StatusEvent);
			MarketDataAdapter.ReplyEvent +=new ReplyEventHandler(MarketDataAdapter_ReplyEvent);
			MarketDataAdapter.Startup(new ThreadSafeResponseInterceptor(this));
			//
			// Setup the default and min dates and times for the various DateTime controls
			dateTimeHistoryStart.Value = DateTime.Now.AddMonths(-1);

			dateTimePickerIntradayBarsStart.MinDate = DateTime.Now.AddDays(-20);
			dateTimePickerIntradayBarsEnd.MinDate = DateTime.Now.AddDays(-20);
			dateTimePickerIntradayRawStart.MinDate = DateTime.Now.AddDays(-20);
			dateTimePickerIntradayRawEnd.MinDate = DateTime.Now.AddDays(-20);
	
			DateTime dtNow = DateTime.Now;
			dateTimePickerIntradayBarsStart.Value = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 9, 30, 0);
			dateTimePickerIntradayBarsEnd.Value = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 16, 0, 0);

			dateTimePickerIntradayRawStart.Value = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 9, 30, 0);
			dateTimePickerIntradayRawEnd.Value = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 16, 0, 0);

		}
		#endregion

		#region IDisposable implementation
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}

				MarketDataAdapter.Shutdown();
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panelInputs = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButtonSynch = new System.Windows.Forms.RadioButton();
			this.radioButtonAsynch = new System.Windows.Forms.RadioButton();
			this.btnRequest = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPageStatic = new System.Windows.Forms.TabPage();
			this.listBoxStaticFields = new System.Windows.Forms.ListBox();
			this.tabPageHistory = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.dateTimeHistoryEnd = new System.Windows.Forms.DateTimePicker();
			this.dateTimeHistoryStart = new System.Windows.Forms.DateTimePicker();
			this.listBoxInterdayFields = new System.Windows.Forms.ListBox();
			this.tabPageIntradayBars = new System.Windows.Forms.TabPage();
			this.label8 = new System.Windows.Forms.Label();
			this.numericUpDownIntradayBars = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.dateTimePickerIntradayBarsEnd = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerIntradayBarsStart = new System.Windows.Forms.DateTimePicker();
			this.listBoxIntradayBarsFields = new System.Windows.Forms.ListBox();
			this.tabPageIntradayRaw = new System.Windows.Forms.TabPage();
			this.listBoxIntradayRawFields = new System.Windows.Forms.ListBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.dateTimePickerIntradayRawEnd = new System.Windows.Forms.DateTimePicker();
			this.dateTimePickerIntradayRawStart = new System.Windows.Forms.DateTimePicker();
			this.listBoxSecurities = new System.Windows.Forms.ListBox();
			this.dataGridResults = new System.Windows.Forms.DataGrid();
			this.listBoxLog = new System.Windows.Forms.ListBox();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.panelInputs.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPageStatic.SuspendLayout();
			this.tabPageHistory.SuspendLayout();
			this.tabPageIntradayBars.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownIntradayBars)).BeginInit();
			this.tabPageIntradayRaw.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridResults)).BeginInit();
			this.SuspendLayout();
			// 
			// panelInputs
			// 
			this.panelInputs.Controls.Add(this.groupBox1);
			this.panelInputs.Controls.Add(this.btnRequest);
			this.panelInputs.Controls.Add(this.label1);
			this.panelInputs.Controls.Add(this.tabControl1);
			this.panelInputs.Controls.Add(this.listBoxSecurities);
			this.panelInputs.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelInputs.Location = new System.Drawing.Point(0, 0);
			this.panelInputs.Name = "panelInputs";
			this.panelInputs.Size = new System.Drawing.Size(712, 150);
			this.panelInputs.TabIndex = 2;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.radioButtonSynch);
			this.groupBox1.Controls.Add(this.radioButtonAsynch);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(392, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(224, 32);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Request Mode:";
			// 
			// radioButtonSynch
			// 
			this.radioButtonSynch.Checked = true;
			this.radioButtonSynch.Location = new System.Drawing.Point(124, 12);
			this.radioButtonSynch.Name = "radioButtonSynch";
			this.radioButtonSynch.Size = new System.Drawing.Size(92, 16);
			this.radioButtonSynch.TabIndex = 1;
			this.radioButtonSynch.TabStop = true;
			this.radioButtonSynch.Text = "Synchronous";
			// 
			// radioButtonAsynch
			// 
			this.radioButtonAsynch.Location = new System.Drawing.Point(16, 12);
			this.radioButtonAsynch.Name = "radioButtonAsynch";
			this.radioButtonAsynch.Size = new System.Drawing.Size(96, 16);
			this.radioButtonAsynch.TabIndex = 0;
			this.radioButtonAsynch.Text = "Asynchronous";
			// 
			// btnRequest
			// 
			this.btnRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRequest.Location = new System.Drawing.Point(632, 8);
			this.btnRequest.Name = "btnRequest";
			this.btnRequest.TabIndex = 1;
			this.btnRequest.Text = "Go";
			this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(368, 24);
			this.label1.TabIndex = 2;
			this.label1.Text = "Select your request parameters and then hit <GO>";
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPageStatic);
			this.tabControl1.Controls.Add(this.tabPageHistory);
			this.tabControl1.Controls.Add(this.tabPageIntradayBars);
			this.tabControl1.Controls.Add(this.tabPageIntradayRaw);
			this.tabControl1.Location = new System.Drawing.Point(136, 40);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.ShowToolTips = true;
			this.tabControl1.Size = new System.Drawing.Size(572, 106);
			this.tabControl1.TabIndex = 2;
			// 
			// tabPageStatic
			// 
			this.tabPageStatic.Controls.Add(this.listBoxStaticFields);
			this.tabPageStatic.Location = new System.Drawing.Point(4, 22);
			this.tabPageStatic.Name = "tabPageStatic";
			this.tabPageStatic.Size = new System.Drawing.Size(564, 80);
			this.tabPageStatic.TabIndex = 0;
			this.tabPageStatic.Text = "Static/Bulk";
			this.tabPageStatic.ToolTipText = "Paramaters for static (requests that reutrn 1 response) and bulk (requests that r" +
				"eturn a set of values)";
			// 
			// listBoxStaticFields
			// 
			this.listBoxStaticFields.AllowDrop = true;
			this.listBoxStaticFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.listBoxStaticFields.Items.AddRange(new object[] {
																	 "PX_BID",
																	 "PX_ASK",
																	 "PX_LAST",
																	 "PX_VOLUME",
																	 "EQY_DVD_HIST_ALL",
																	 "PRODUCT_SEG"});
			this.listBoxStaticFields.Location = new System.Drawing.Point(8, 8);
			this.listBoxStaticFields.Name = "listBoxStaticFields";
			this.listBoxStaticFields.Size = new System.Drawing.Size(152, 69);
			this.listBoxStaticFields.TabIndex = 0;
			this.listBoxStaticFields.TabStop = false;
			this.listBoxStaticFields.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragDrop);
			this.listBoxStaticFields.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragEnter);
			// 
			// tabPageHistory
			// 
			this.tabPageHistory.Controls.Add(this.label4);
			this.tabPageHistory.Controls.Add(this.label3);
			this.tabPageHistory.Controls.Add(this.dateTimeHistoryEnd);
			this.tabPageHistory.Controls.Add(this.dateTimeHistoryStart);
			this.tabPageHistory.Controls.Add(this.listBoxInterdayFields);
			this.tabPageHistory.Location = new System.Drawing.Point(4, 22);
			this.tabPageHistory.Name = "tabPageHistory";
			this.tabPageHistory.Size = new System.Drawing.Size(564, 80);
			this.tabPageHistory.TabIndex = 1;
			this.tabPageHistory.Text = "Interday (History)";
			this.tabPageHistory.ToolTipText = "Enter paramaters for historical (daily) requests.";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(168, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 5;
			this.label4.Text = "End Date:";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(168, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Start Date:";
			// 
			// dateTimeHistoryEnd
			// 
			this.dateTimeHistoryEnd.Location = new System.Drawing.Point(248, 48);
			this.dateTimeHistoryEnd.Name = "dateTimeHistoryEnd";
			this.dateTimeHistoryEnd.TabIndex = 3;
			// 
			// dateTimeHistoryStart
			// 
			this.dateTimeHistoryStart.Location = new System.Drawing.Point(248, 16);
			this.dateTimeHistoryStart.Name = "dateTimeHistoryStart";
			this.dateTimeHistoryStart.TabIndex = 2;
			// 
			// listBoxInterdayFields
			// 
			this.listBoxInterdayFields.AllowDrop = true;
			this.listBoxInterdayFields.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.listBoxInterdayFields.Items.AddRange(new object[] {
																	   "PX_BID",
																	   "PX_ASK",
																	   "PX_LAST",
																	   "PX_VOLUME"});
			this.listBoxInterdayFields.Location = new System.Drawing.Point(8, 7);
			this.listBoxInterdayFields.Name = "listBoxInterdayFields";
			this.listBoxInterdayFields.Size = new System.Drawing.Size(152, 69);
			this.listBoxInterdayFields.TabIndex = 1;
			this.listBoxInterdayFields.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragDrop);
			this.listBoxInterdayFields.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragEnter);
			// 
			// tabPageIntradayBars
			// 
			this.tabPageIntradayBars.Controls.Add(this.label8);
			this.tabPageIntradayBars.Controls.Add(this.numericUpDownIntradayBars);
			this.tabPageIntradayBars.Controls.Add(this.label5);
			this.tabPageIntradayBars.Controls.Add(this.label2);
			this.tabPageIntradayBars.Controls.Add(this.dateTimePickerIntradayBarsEnd);
			this.tabPageIntradayBars.Controls.Add(this.dateTimePickerIntradayBarsStart);
			this.tabPageIntradayBars.Controls.Add(this.listBoxIntradayBarsFields);
			this.tabPageIntradayBars.Location = new System.Drawing.Point(4, 22);
			this.tabPageIntradayBars.Name = "tabPageIntradayBars";
			this.tabPageIntradayBars.Size = new System.Drawing.Size(564, 80);
			this.tabPageIntradayBars.TabIndex = 2;
			this.tabPageIntradayBars.Text = "Intraday (Bars)";
			this.tabPageIntradayBars.ToolTipText = "Enter paramaters for intraday timebar requests";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label8.Location = new System.Drawing.Point(456, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(60, 40);
			this.label8.TabIndex = 8;
			this.label8.Text = "Bar interval (minutes):";
			// 
			// numericUpDownIntradayBars
			// 
			this.numericUpDownIntradayBars.Location = new System.Drawing.Point(520, 32);
			this.numericUpDownIntradayBars.Maximum = new System.Decimal(new int[] {
																					  60,
																					  0,
																					  0,
																					  0});
			this.numericUpDownIntradayBars.Minimum = new System.Decimal(new int[] {
																					  1,
																					  0,
																					  0,
																					  0});
			this.numericUpDownIntradayBars.Name = "numericUpDownIntradayBars";
			this.numericUpDownIntradayBars.Size = new System.Drawing.Size(40, 20);
			this.numericUpDownIntradayBars.TabIndex = 7;
			this.numericUpDownIntradayBars.Value = new System.Decimal(new int[] {
																					15,
																					0,
																					0,
																					0});
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(168, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 6;
			this.label5.Text = "End Date:";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(168, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 5;
			this.label2.Text = "Start Date:";
			// 
			// dateTimePickerIntradayBarsEnd
			// 
			this.dateTimePickerIntradayBarsEnd.CustomFormat = "MMMMdd, yyyy - HH:mm:ss";
			this.dateTimePickerIntradayBarsEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerIntradayBarsEnd.Location = new System.Drawing.Point(248, 48);
			this.dateTimePickerIntradayBarsEnd.MinDate = new System.DateTime(2005, 1, 1, 0, 0, 0, 0);
			this.dateTimePickerIntradayBarsEnd.Name = "dateTimePickerIntradayBarsEnd";
			this.dateTimePickerIntradayBarsEnd.TabIndex = 2;
			// 
			// dateTimePickerIntradayBarsStart
			// 
			this.dateTimePickerIntradayBarsStart.CustomFormat = "MMMMdd, yyyy - HH:mm:ss";
			this.dateTimePickerIntradayBarsStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerIntradayBarsStart.Location = new System.Drawing.Point(248, 16);
			this.dateTimePickerIntradayBarsStart.Name = "dateTimePickerIntradayBarsStart";
			this.dateTimePickerIntradayBarsStart.TabIndex = 1;
			// 
			// listBoxIntradayBarsFields
			// 
			this.listBoxIntradayBarsFields.AllowDrop = true;
			this.listBoxIntradayBarsFields.Items.AddRange(new object[] {
																		   "LAST_PRICE",
																		   "BEST_BID",
																		   "BEST_ASK"});
			this.listBoxIntradayBarsFields.Location = new System.Drawing.Point(8, 7);
			this.listBoxIntradayBarsFields.Name = "listBoxIntradayBarsFields";
			this.listBoxIntradayBarsFields.Size = new System.Drawing.Size(152, 69);
			this.listBoxIntradayBarsFields.TabIndex = 0;
			this.listBoxIntradayBarsFields.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragDrop);
			this.listBoxIntradayBarsFields.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragEnter);
			// 
			// tabPageIntradayRaw
			// 
			this.tabPageIntradayRaw.Controls.Add(this.listBoxIntradayRawFields);
			this.tabPageIntradayRaw.Controls.Add(this.label6);
			this.tabPageIntradayRaw.Controls.Add(this.label7);
			this.tabPageIntradayRaw.Controls.Add(this.dateTimePickerIntradayRawEnd);
			this.tabPageIntradayRaw.Controls.Add(this.dateTimePickerIntradayRawStart);
			this.tabPageIntradayRaw.Location = new System.Drawing.Point(4, 22);
			this.tabPageIntradayRaw.Name = "tabPageIntradayRaw";
			this.tabPageIntradayRaw.Size = new System.Drawing.Size(564, 80);
			this.tabPageIntradayRaw.TabIndex = 3;
			this.tabPageIntradayRaw.Text = "Intraday (Raw)";
			this.tabPageIntradayRaw.ToolTipText = "Enter paramaters for intraday raw requests";
			// 
			// listBoxIntradayRawFields
			// 
			this.listBoxIntradayRawFields.AllowDrop = true;
			this.listBoxIntradayRawFields.Items.AddRange(new object[] {
																		  "LAST_PRICE",
																		  "BEST_BID",
																		  "BEST_ASK"});
			this.listBoxIntradayRawFields.Location = new System.Drawing.Point(8, 6);
			this.listBoxIntradayRawFields.Name = "listBoxIntradayRawFields";
			this.listBoxIntradayRawFields.Size = new System.Drawing.Size(152, 69);
			this.listBoxIntradayRawFields.TabIndex = 11;
			this.listBoxIntradayRawFields.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragDrop);
			this.listBoxIntradayRawFields.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxStaticFields_DragEnter);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(168, 48);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "End Date:";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(168, 16);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 16);
			this.label7.TabIndex = 9;
			this.label7.Text = "Start Date:";
			// 
			// dateTimePickerIntradayRawEnd
			// 
			this.dateTimePickerIntradayRawEnd.CustomFormat = "MMMMdd, yyyy - HH:mm:ss";
			this.dateTimePickerIntradayRawEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerIntradayRawEnd.Location = new System.Drawing.Point(248, 48);
			this.dateTimePickerIntradayRawEnd.Name = "dateTimePickerIntradayRawEnd";
			this.dateTimePickerIntradayRawEnd.TabIndex = 8;
			// 
			// dateTimePickerIntradayRawStart
			// 
			this.dateTimePickerIntradayRawStart.CustomFormat = "MMMMdd, yyyy - HH:mm:ss";
			this.dateTimePickerIntradayRawStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePickerIntradayRawStart.Location = new System.Drawing.Point(248, 16);
			this.dateTimePickerIntradayRawStart.Name = "dateTimePickerIntradayRawStart";
			this.dateTimePickerIntradayRawStart.TabIndex = 7;
			// 
			// listBoxSecurities
			// 
			this.listBoxSecurities.AllowDrop = true;
			this.listBoxSecurities.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.listBoxSecurities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBoxSecurities.Items.AddRange(new object[] {
																   "IBM US Equity",
																   "T US Equity",
																   "MSFT Equity",
																   "SEBL Equity"});
			this.listBoxSecurities.Location = new System.Drawing.Point(8, 40);
			this.listBoxSecurities.Name = "listBoxSecurities";
			this.listBoxSecurities.Size = new System.Drawing.Size(120, 106);
			this.listBoxSecurities.TabIndex = 0;
			this.listBoxSecurities.TabStop = false;
			this.listBoxSecurities.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBoxSecurities_DragDrop);
			this.listBoxSecurities.DragEnter += new System.Windows.Forms.DragEventHandler(this.listBoxSecurities_DragEnter);
			// 
			// dataGridResults
			// 
			this.dataGridResults.CaptionText = "Result Set:";
			this.dataGridResults.DataMember = "";
			this.dataGridResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridResults.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridResults.Location = new System.Drawing.Point(0, 150);
			this.dataGridResults.Name = "dataGridResults";
			this.dataGridResults.Size = new System.Drawing.Size(712, 368);
			this.dataGridResults.TabIndex = 3;
			this.dataGridResults.TabStop = false;
			this.dataGridResults.Tag = "";
			// 
			// listBoxLog
			// 
			this.listBoxLog.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.listBoxLog.HorizontalScrollbar = true;
			this.listBoxLog.Location = new System.Drawing.Point(0, 423);
			this.listBoxLog.Name = "listBoxLog";
			this.listBoxLog.Size = new System.Drawing.Size(712, 95);
			this.listBoxLog.TabIndex = 2;
			this.listBoxLog.TabStop = false;
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Location = new System.Drawing.Point(0, 150);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(712, 4);
			this.splitter1.TabIndex = 3;
			this.splitter1.TabStop = false;
			// 
			// splitter2
			// 
			this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter2.Location = new System.Drawing.Point(0, 420);
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(712, 3);
			this.splitter2.TabIndex = 4;
			this.splitter2.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(712, 518);
			this.Controls.Add(this.splitter2);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.listBoxLog);
			this.Controls.Add(this.dataGridResults);
			this.Controls.Add(this.panelInputs);
			this.Name = "Form1";
			this.Text = "Synchronous Sample";
			this.panelInputs.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPageStatic.ResumeLayout(false);
			this.tabPageHistory.ResumeLayout(false);
			this.tabPageIntradayBars.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownIntradayBars)).EndInit();
			this.tabPageIntradayRaw.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridResults)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		#region Private Application Methods
/// <summary>
/// Timestamps and writes log messages to the Log console (listBoxLog)
/// </summary>
/// <param name="msg"></param>
		private void LogMessage(string msg)
		{
			DateTime dtNow = DateTime.Now;
			string logMessage = string.Format("<{0,2:00}:{1,2:00}:{2,2:00}:{3,3:000}> {4}", 
				dtNow.Hour, dtNow.Minute, dtNow.Second, dtNow.Millisecond, msg);
			
			if (listBoxLog.Items.Count >= 1024)
				listBoxLog.Items.RemoveAt(0);		
			
			listBoxLog.Items.Add(logMessage);
			listBoxLog.SelectedIndex = listBoxLog.Items.Count - 1;
			
		}
/// <summary>
/// Submits a synchronous request to the MarketDataAdapter, waits for response and displays result 
/// in DataGrid
/// </summary>
/// <param name="req"></param>
		private void SubmitSynchronous(Request req, int timeout)
		{
			if (req != null)
			{
				dataGridResults.DataSource = null;
				dataGridResults.DataMember = null;

				dataGridResults.Refresh();

				string msg = string.Format("Sending Synchronous request for {0} - {1} securities and {2} fields. Timeout is {3} seconds.",
					tabControl1.SelectedTab, req.Securities.Count, req.Fields.Count, timeout / 1000.0);

				LogMessage(msg);

				Reply reply = MarketDataAdapter.SynchronousRequest(req, timeout);	

				ProcessReply(reply);

			}
		}
/// <summary>
/// Submits an Asynchronous request to the MarketDataAdapter. Resonse will be returned
/// via the ReplyEvent
/// </summary>
/// <param name="req"></param>
		private void SubmitAsynchronous(Request req)
		{
			string msg = string.Format("Sending Asynchronous request for {0} - {1} securities and {2} fields.",
				tabControl1.SelectedTab, req.Securities.Count, req.Fields.Count);
			LogMessage(msg);	
			MarketDataAdapter.SendRequest(req);
		}
/// <summary>
/// Generic routine for converting a Reply object to a dataset, and binding
/// it to a DataGrid. Callled to process results that have been returned 
/// both synchronously and asynchronously.
/// </summary>
/// <param name="reply">The reply returned from the MarketDataAdapter</param>
		private void ProcessReply(Reply reply)
		{
			LogMessage("Received response, now converting to DataSet.");
			DataSet resultDataSet = null;
			try
			{
				resultDataSet = reply.ToDataSet();
			}
			catch(Exception ex)
			{
				LogMessage("Error converting Reply ToDataSet: " + ex.ToString());
				return;
			}

			LogMessage("Dataset conversion has completed.");

			dataGridResults.CaptionText = string.Format("Result Set: ({0})",
				resultDataSet.DataSetName);
			dataGridResults.DataSource = resultDataSet;

			// Unless you have requested duplicate Securities, DataSet will
			// have DataRelations between Securities master (resultDataSet.Tables[0])
			// and the other DataTables in the DataSet - allowing for a "Master/Detail"
			// view on the grid.
			if (resultDataSet.Relations.Count > 0)
				dataGridResults.DataMember = resultDataSet.Tables[0].TableName;	
			else
			{
				// If you have requested dupicate securities,
				// then it is not possible to setup the DataRelation betweent 
				// the Securities (resultDataSet.Tables[0]) master table and the 
				// other (resultDataSet.Tables[1-N] tables in the DataSet.
				dataGridResults.DataMember = resultDataSet.Tables[1].TableName;	
			}
			
			
		}
/// <summary>
/// Build and return a static request from the contents of the applicable GUI controls
/// </summary>
/// <returns></returns>
		private Request MakeStaticRequest()
		{
			Request req = new Request();

			foreach(string fldStr in listBoxStaticFields.Items)
			{
				try
				{
					Field fld = MarketDataAdapter.FieldTable[fldStr];
					if (fld.IsStatic == true || fld.IsBulkData == true)
						req.Fields.Add(MarketDataAdapter.FieldTable[fld]);
					else
						LogMessage(fld.Mnemonic + " is not bulk or static.");
				}
				catch(Exception ex)
				{
					LogMessage("Unable to add a Field to Static Request: " + ex.ToString());
				}

			}

			return req;
		}

/// <summary>
/// Build and return an IntradayBars request from the contents of the applicable GUI controls
/// </summary>
/// <returns>A RequestForIntradayBars object</returns>
		private Request MakeIntradayBarsRequest()
		{
			Request req = new RequestForIntradayBars();

			foreach(string fld in listBoxIntradayBarsFields.Items)
				req.Fields.Add(MarketDataAdapter.FieldTable[fld]);
			
			req.AspectFields.Add(AspectField.AspectTime);
			req.AspectFields.Add(AspectField.AspectOpen);;
			req.AspectFields.Add(AspectField.AspectClose);
			req.AspectFields.Add(AspectField.AspectHigh);
			req.AspectFields.Add(AspectField.AspectLow);	
			req.AspectFields.Add(AspectField.AspectVolume);

			req.StartDateTime = dateTimePickerIntradayBarsStart.Value;
			req.EndDateTime = dateTimePickerIntradayBarsEnd.Value;
	
			req.AspectTimeBarSize = decimal.ToInt32(numericUpDownIntradayBars.Value);

			return req;
		}
/// <summary>
/// Build and return an Intraday Raw request from the contents of the applicable GUI controls.
/// </summary>
/// <returns>A RequestForIntradayRaw</returns>
		private Request MakeIntradayRawRequest()
		{
			Request req = new RequestForIntradayRaw();

			foreach(string fld in listBoxIntradayRawFields.Items)
				req.Fields.Add(MarketDataAdapter.FieldTable[fld]);
			

			req.StartDateTime = dateTimePickerIntradayRawStart.Value;
			req.EndDateTime = dateTimePickerIntradayRawEnd.Value;
	
			return req;
		}
/// <summary>
/// Build and return a RequestForHistory object from the contents of the applicable GUI controls.
/// </summary>
/// <returns>A RequestForHistory object</returns>
		
		private Request MakeHistoricalRequest()
		{
			Request req = new RequestForHistory();

			foreach(string fld in listBoxInterdayFields.Items)
				req.Fields.Add(MarketDataAdapter.FieldTable[fld]);

			req.Periodicity = Periodicity.ActualDaily;	
			req.StartExtendedDate = new ExtendedDate( dateTimeHistoryStart.Value);
			req.EndExtendedDate = new ExtendedDate(dateTimeHistoryEnd.Value);
			

			return req;
		}
		private void listBoxAddFields(System.Windows.Forms.ListBox lb, string tmp)
		{
			lb.Items.Clear();

			// Tokenize the string into what (we hope) are Security strings
			char[] sep = {'\r', '\n', '\t'};
			string [] words = tmp.Split(sep);
			FieldCollection scol = new FieldCollection();
			foreach(string s in words)
			{
				string sfld = s.Trim();
				if (sfld.Length == 0)
					continue;

				try
				{
					Field fld = MarketDataAdapter.FieldTable[sfld];
					lb.Items.Add(fld.Mnemonic);
				}
				catch(Exception ex)
				{
					LogMessage("Invalid Field: " + sfld + ". " + ex.ToString());
			
				}
			}
		}

#endregion

		#region DragDrop Event Handlers

		private void listBoxSecurities_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if(e.Data.GetDataPresent(DataFormats.Text))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;	
		
		}

		private void listBoxSecurities_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			// Get the entire text object that has been dropped on us.
			string tmp = e.Data.GetData(DataFormats.Text).ToString();

			listBoxSecurities.Items.Clear();

			// Tokenize the string into what (we hope) are Security strings
			char[] sep = {'\r', '\n', '\t'};
			string [] words = tmp.Split(sep);
			SecurityCollection scol = new SecurityCollection();
			foreach(string s in words)
			{
				string ssec = s.Trim();
				if (ssec.Length == 0)
					continue;

				try
				{
					Security sec = new Security(ssec);
					listBoxSecurities.Items.Add(sec.DisplayName);
				}
				catch(Exception ex)
				{
					LogMessage("Invalid security: " + ssec + ". " + ex.ToString());
			
				}
			}
		}

		private void listBoxStaticFields_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if(e.Data.GetDataPresent(DataFormats.Text))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;			
		}
#endregion

		#region WinForm event handlers

		private void btnRequest_Click(object sender, System.EventArgs e)
		{
			Request req = null;
			int timeout = 30000;
		
			try
			{
				if (tabControl1.SelectedTab.Name == "tabPageStatic")
					req = MakeStaticRequest();
				else if (tabControl1.SelectedTab.Name == "tabPageHistory")
					req = MakeHistoricalRequest();
				else if (tabControl1.SelectedTab.Name == "tabPageIntradayBars")
				{
					req = MakeIntradayBarsRequest();
					timeout = 60000;
				}
				else if (tabControl1.SelectedTab.Name == "tabPageIntradayRaw")
				{
					req = MakeIntradayRawRequest();
					timeout = 60000;
				}


				if (req != null)
				{
					req.SubscriptionMode = SubscriptionMode.ByRequest;
					foreach(string sec in listBoxSecurities.Items)
						req.Securities.Add(sec);
				}
			}
			catch(Exception ex)
			{
				LogMessage("Unable to create a Request: " + ex.ToString());
				return;
			}

			try
			{
				if (radioButtonSynch.Checked == true)
					SubmitSynchronous(req, timeout);
				else
					SubmitAsynchronous(req);
			}
			catch(Exception ex)
			{
				LogMessage("Failed to submit Request: " + ex.ToString());
			}

		}
#endregion

		#region MarketDataAdapater event handlers
		private void MarketDataAdapter_StatusEvent(StatusCode status, string description)
		{
			LogMessage("MarketDataAdapter Status message: " + description);
		}

		private void MarketDataAdapter_ReplyEvent(Reply reply)
		{
			ProcessReply(reply);
		}
	#endregion

		private void listBoxStaticFields_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			listBoxAddFields((System.Windows.Forms.ListBox)sender, e.Data.GetData(DataFormats.Text).ToString());		
		}


	}
}
