/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	/// <summary> 
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ToolTip directionsToolTip;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txt5YrPrice;
		private System.Windows.Forms.TextBox txt5YrYield;
		private System.Windows.Forms.TextBox txt10YrPrice;
		private System.Windows.Forms.TextBox txt10YrYield;
		private System.Windows.Forms.TextBox txt20YrPrice;
		private System.Windows.Forms.TextBox txt20YrYield;
		private System.Windows.Forms.TextBox txt30YrPrice;
		private System.Windows.Forms.TextBox txt30YrYield;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label lbl30YrPrice;
		private System.Windows.Forms.Label lbl5YrPrice;
		private System.Windows.Forms.Label lbl20YrPrice;
		private System.Windows.Forms.Label lbl10YrPrice;
		private System.Windows.Forms.Label lbl30YrYield;
		private System.Windows.Forms.Label lbl5YrYield;
		private System.Windows.Forms.Label lbl20YrYield;
		private System.Windows.Forms.Label lbl10YrYield;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.GroupBox grpOverrides;
		private System.Windows.Forms.GroupBox grpResults;
		private System.Windows.Forms.Button btnReset;
		private System.ComponentModel.IContainer components;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.directionsToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.grpOverrides = new System.Windows.Forms.GroupBox();
			this.btnReset = new System.Windows.Forms.Button();
			this.txt30YrYield = new System.Windows.Forms.TextBox();
			this.txt30YrPrice = new System.Windows.Forms.TextBox();
			this.txt20YrYield = new System.Windows.Forms.TextBox();
			this.txt20YrPrice = new System.Windows.Forms.TextBox();
			this.txt10YrYield = new System.Windows.Forms.TextBox();
			this.txt10YrPrice = new System.Windows.Forms.TextBox();
			this.txt5YrYield = new System.Windows.Forms.TextBox();
			this.txt5YrPrice = new System.Windows.Forms.TextBox();
			this.grpResults = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.lbl10YrYield = new System.Windows.Forms.Label();
			this.lbl30YrPrice = new System.Windows.Forms.Label();
			this.lbl5YrPrice = new System.Windows.Forms.Label();
			this.lbl20YrYield = new System.Windows.Forms.Label();
			this.lbl20YrPrice = new System.Windows.Forms.Label();
			this.lbl30YrYield = new System.Windows.Forms.Label();
			this.lbl5YrYield = new System.Windows.Forms.Label();
			this.lbl10YrPrice = new System.Windows.Forms.Label();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.groupBox1.SuspendLayout();
			this.grpOverrides.SuspendLayout();
			this.grpResults.SuspendLayout();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(21, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(85, 20);
			this.label2.TabIndex = 4;
			this.label2.Text = "Price";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(118, 28);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(85, 20);
			this.label3.TabIndex = 6;
			this.label3.Text = "Yield";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// directionsToolTip
			// 
			this.directionsToolTip.AutoPopDelay = 5000;
			this.directionsToolTip.InitialDelay = 100;
			this.directionsToolTip.ReshowDelay = 100;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Location = new System.Drawing.Point(16, 18);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(96, 160);
			this.groupBox1.TabIndex = 12;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "US Treasuries";
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label7.Location = new System.Drawing.Point(27, 128);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(56, 20);
			this.label7.TabIndex = 13;
			this.label7.Text = "30 Year:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(27, 56);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 20);
			this.label6.TabIndex = 12;
			this.label6.Text = "5 Year:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(27, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(56, 20);
			this.label5.TabIndex = 11;
			this.label5.Text = "20 Year:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(27, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(56, 20);
			this.label4.TabIndex = 10;
			this.label4.Text = "10 Year:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// grpOverrides
			// 
			this.grpOverrides.Controls.Add(this.btnReset);
			this.grpOverrides.Controls.Add(this.txt30YrYield);
			this.grpOverrides.Controls.Add(this.txt30YrPrice);
			this.grpOverrides.Controls.Add(this.txt20YrYield);
			this.grpOverrides.Controls.Add(this.txt20YrPrice);
			this.grpOverrides.Controls.Add(this.txt10YrYield);
			this.grpOverrides.Controls.Add(this.txt10YrPrice);
			this.grpOverrides.Controls.Add(this.txt5YrYield);
			this.grpOverrides.Controls.Add(this.txt5YrPrice);
			this.grpOverrides.Controls.Add(this.label3);
			this.grpOverrides.Controls.Add(this.label2);
			this.grpOverrides.Location = new System.Drawing.Point(120, 18);
			this.grpOverrides.Name = "grpOverrides";
			this.grpOverrides.Size = new System.Drawing.Size(216, 190);
			this.grpOverrides.TabIndex = 13;
			this.grpOverrides.TabStop = false;
			this.grpOverrides.Text = "Overrides";
			// 
			// btnReset
			// 
			this.btnReset.Location = new System.Drawing.Point(80, 157);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(64, 24);
			this.btnReset.TabIndex = 8;
			this.btnReset.Text = "Reset";
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// txt30YrYield
			// 
			this.txt30YrYield.Location = new System.Drawing.Point(117, 128);
			this.txt30YrYield.Name = "txt30YrYield";
			this.txt30YrYield.Size = new System.Drawing.Size(88, 20);
			this.txt30YrYield.TabIndex = 7;
			this.txt30YrYield.Tag = "31";
			this.txt30YrYield.Text = "";
			this.txt30YrYield.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt30YrPrice
			// 
			this.txt30YrPrice.Location = new System.Drawing.Point(19, 128);
			this.txt30YrPrice.Name = "txt30YrPrice";
			this.txt30YrPrice.Size = new System.Drawing.Size(88, 20);
			this.txt30YrPrice.TabIndex = 6;
			this.txt30YrPrice.Tag = "30";
			this.txt30YrPrice.Text = "";
			this.txt30YrPrice.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt20YrYield
			// 
			this.txt20YrYield.Location = new System.Drawing.Point(117, 104);
			this.txt20YrYield.Name = "txt20YrYield";
			this.txt20YrYield.Size = new System.Drawing.Size(88, 20);
			this.txt20YrYield.TabIndex = 5;
			this.txt20YrYield.Tag = "21";
			this.txt20YrYield.Text = "";
			this.txt20YrYield.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt20YrPrice
			// 
			this.txt20YrPrice.Location = new System.Drawing.Point(19, 104);
			this.txt20YrPrice.Name = "txt20YrPrice";
			this.txt20YrPrice.Size = new System.Drawing.Size(88, 20);
			this.txt20YrPrice.TabIndex = 4;
			this.txt20YrPrice.Tag = "20";
			this.txt20YrPrice.Text = "";
			this.txt20YrPrice.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt10YrYield
			// 
			this.txt10YrYield.Location = new System.Drawing.Point(117, 80);
			this.txt10YrYield.Name = "txt10YrYield";
			this.txt10YrYield.Size = new System.Drawing.Size(88, 20);
			this.txt10YrYield.TabIndex = 3;
			this.txt10YrYield.Tag = "11";
			this.txt10YrYield.Text = "";
			this.txt10YrYield.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt10YrPrice
			// 
			this.txt10YrPrice.Location = new System.Drawing.Point(19, 80);
			this.txt10YrPrice.Name = "txt10YrPrice";
			this.txt10YrPrice.Size = new System.Drawing.Size(88, 20);
			this.txt10YrPrice.TabIndex = 2;
			this.txt10YrPrice.Tag = "10";
			this.txt10YrPrice.Text = "";
			this.txt10YrPrice.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt5YrYield
			// 
			this.txt5YrYield.Location = new System.Drawing.Point(117, 56);
			this.txt5YrYield.Name = "txt5YrYield";
			this.txt5YrYield.Size = new System.Drawing.Size(88, 20);
			this.txt5YrYield.TabIndex = 1;
			this.txt5YrYield.Tag = "01";
			this.txt5YrYield.Text = "";
			this.txt5YrYield.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// txt5YrPrice
			// 
			this.txt5YrPrice.Location = new System.Drawing.Point(19, 56);
			this.txt5YrPrice.Name = "txt5YrPrice";
			this.txt5YrPrice.Size = new System.Drawing.Size(88, 20);
			this.txt5YrPrice.TabIndex = 0;
			this.txt5YrPrice.Tag = "00";
			this.txt5YrPrice.Text = "";
			this.txt5YrPrice.Validating += new System.ComponentModel.CancelEventHandler(this.TabGetData);
			// 
			// grpResults
			// 
			this.grpResults.Controls.Add(this.label1);
			this.grpResults.Controls.Add(this.label8);
			this.grpResults.Controls.Add(this.lbl10YrYield);
			this.grpResults.Controls.Add(this.lbl30YrPrice);
			this.grpResults.Controls.Add(this.lbl5YrPrice);
			this.grpResults.Controls.Add(this.lbl20YrYield);
			this.grpResults.Controls.Add(this.lbl20YrPrice);
			this.grpResults.Controls.Add(this.lbl30YrYield);
			this.grpResults.Controls.Add(this.lbl5YrYield);
			this.grpResults.Controls.Add(this.lbl10YrPrice);
			this.grpResults.Location = new System.Drawing.Point(344, 18);
			this.grpResults.Name = "grpResults";
			this.grpResults.Size = new System.Drawing.Size(248, 160);
			this.grpResults.TabIndex = 14;
			this.grpResults.TabStop = false;
			this.grpResults.Text = "Results";
			// 
			// label1
			// 
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(128, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 20);
			this.label1.TabIndex = 23;
			this.label1.Text = "Yield";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label8
			// 
			this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label8.Location = new System.Drawing.Point(16, 28);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(104, 20);
			this.label8.TabIndex = 22;
			this.label8.Text = "Price";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbl10YrYield
			// 
			this.lbl10YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl10YrYield.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl10YrYield.Location = new System.Drawing.Point(129, 80);
			this.lbl10YrYield.Name = "lbl10YrYield";
			this.lbl10YrYield.Size = new System.Drawing.Size(104, 21);
			this.lbl10YrYield.TabIndex = 18;
			this.lbl10YrYield.Tag = "11";
			this.lbl10YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl30YrPrice
			// 
			this.lbl30YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl30YrPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl30YrPrice.Location = new System.Drawing.Point(17, 128);
			this.lbl30YrPrice.Name = "lbl30YrPrice";
			this.lbl30YrPrice.Size = new System.Drawing.Size(104, 21);
			this.lbl30YrPrice.TabIndex = 17;
			this.lbl30YrPrice.Tag = "30";
			this.lbl30YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl5YrPrice
			// 
			this.lbl5YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl5YrPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl5YrPrice.Location = new System.Drawing.Point(17, 56);
			this.lbl5YrPrice.Name = "lbl5YrPrice";
			this.lbl5YrPrice.Size = new System.Drawing.Size(104, 21);
			this.lbl5YrPrice.TabIndex = 16;
			this.lbl5YrPrice.Tag = "00";
			this.lbl5YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl20YrYield
			// 
			this.lbl20YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl20YrYield.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl20YrYield.Location = new System.Drawing.Point(129, 104);
			this.lbl20YrYield.Name = "lbl20YrYield";
			this.lbl20YrYield.Size = new System.Drawing.Size(104, 21);
			this.lbl20YrYield.TabIndex = 19;
			this.lbl20YrYield.Tag = "21";
			this.lbl20YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl20YrPrice
			// 
			this.lbl20YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl20YrPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl20YrPrice.Location = new System.Drawing.Point(17, 104);
			this.lbl20YrPrice.Name = "lbl20YrPrice";
			this.lbl20YrPrice.Size = new System.Drawing.Size(104, 21);
			this.lbl20YrPrice.TabIndex = 15;
			this.lbl20YrPrice.Tag = "20";
			this.lbl20YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl30YrYield
			// 
			this.lbl30YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl30YrYield.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl30YrYield.Location = new System.Drawing.Point(129, 128);
			this.lbl30YrYield.Name = "lbl30YrYield";
			this.lbl30YrYield.Size = new System.Drawing.Size(104, 21);
			this.lbl30YrYield.TabIndex = 21;
			this.lbl30YrYield.Tag = "31";
			this.lbl30YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl5YrYield
			// 
			this.lbl5YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl5YrYield.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl5YrYield.Location = new System.Drawing.Point(129, 56);
			this.lbl5YrYield.Name = "lbl5YrYield";
			this.lbl5YrYield.Size = new System.Drawing.Size(104, 21);
			this.lbl5YrYield.TabIndex = 20;
			this.lbl5YrYield.Tag = "01";
			this.lbl5YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbl10YrPrice
			// 
			this.lbl10YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbl10YrPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lbl10YrPrice.Location = new System.Drawing.Point(17, 80);
			this.lbl10YrPrice.Name = "lbl10YrPrice";
			this.lbl10YrPrice.Size = new System.Drawing.Size(104, 21);
			this.lbl10YrPrice.TabIndex = 14;
			this.lbl10YrPrice.Tag = "10";
			this.lbl10YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 223);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(608, 22);
			this.statusBar1.TabIndex = 15;
			this.statusBar1.Text = "statusBar1";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(568, 8);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(24, 20);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 14;
			this.pictureBox1.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(608, 245);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.grpResults);
			this.Controls.Add(this.grpOverrides);
			this.Controls.Add(this.groupBox1);
			this.Name = "Form1";
			this.Text = "Static Overrides Request using API for .NET";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.grpOverrides.ResumeLayout(false);
			this.grpResults.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private string[] _securities = null;
		private Field[] _fields = null;
		private RequestForStaticOverride reqOvr = null;
		private FieldTable ftbl = null;
		private int colCount = 0;
		private int rowCount = 0;
		private int rowOfRequest, colOfRequest;

		private void Form1_Load(object sender, System.EventArgs e)
		{
			// Directions on how to run sample available as tooltip on pictureBox question-mark.
			directionsToolTip.SetToolTip(this.pictureBox1, 
				"Instructions: Enter an override value in one of the entry boxes and press the TAB key to see the results in the right-side frame.");

			// Subscribe for any status messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnStatic_StatusEvent);

			// Now, startup the MarketDataAdapter.
			// This will cause several things to happen:
			//   - Bloomberg configuration information will be acquired from the 
			//	   environment (Windows Registry, config files, the Bloomberg terminal, etc)
			//	 - A connection to the local bbcomm will be established.
			//	 - BBCOMM protocol handshaking will be initiated.
			//	 - Adapter resources will be allocated.
			try
			{
				// The ThreadSafeResponseInterceptor ensures that the status and reply
				// event handlers are called on the user interface threads. 
				MarketDataAdapter.Startup(new ThreadSafeResponseInterceptor(this));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				Close();
			}

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapter.Startup method
			ftbl = MarketDataAdapter.FieldTable;

			_fields = new Field[]
				{ftbl["PX_ASK"], ftbl["YLD_CNV_ASK"]};

			_securities = new string[]
				{"CT5 Govt", "CT10 Govt", "CT20 Govt", "CT30 Govt"};

			// Subscribe for any reply messages
			MarketDataAdapter.ReplyEvent += new ReplyEventHandler(OnStatic_ReplyEvent);

			MakeRequest();

			statusBar1.ShowPanels = false;
		}

		private void OnStatic_ReplyEvent(Reply reply)
		{			
			if(reply.ReplyError != null)
			{
				statusBar1.Text = "Reply Error has occurred: " + 
					reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description;
				return;
			}

			try
			{
				// Loop through each security
				foreach(SecurityDataItem sdi in reply.GetSecurityDataItems())
				{
					// Loop through each field
					foreach(FieldDataItem fdi in sdi.FieldsData)
					{
						// Loop through each data point
						foreach(DataPoint dp in fdi.DataPoints)
						{
							if(dp != null)
							{
								if(dp.IsError)
								{
									statusBar1.Text = dp.ReplyError.DisplayName;
								}
								else
								{
									if (rowOfRequest == -1)	
										DisplayData(rowCount, colCount, dp.Value.ToString());
									else
										DisplayData(rowOfRequest, colCount, dp.Value.ToString());

									statusBar1.Text = "Data received successfully.";
								}
							}
						}
						colCount++; // Update the column count
					}
					colCount = 0; // Reset the column count
					rowCount++; // Update the row count
				}
			}
			catch (Exception ex)
			{
				statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message;
			}
		}

		private void OnStatic_StatusEvent(StatusCode status, string description)
		{
			statusBar1.Text = "Status: " + status + "  Desc: " + description;
		}

		private void TabGetData(object sender, System.ComponentModel.CancelEventArgs e)
		{
			TextBox t = (TextBox)sender;

			if(e.Cancel == false)
			{
				if(t.Text != "")
                    MakeRequest(t);
			}
		}

		private void MakeRequest()
		{
			// Issue new static request
			reqOvr = new RequestForStaticOverride();

			// Add field and security to RequestForStaticOverride object
			reqOvr.Fields.AddRange(_fields);
			reqOvr.Securities.AddRange(_securities);

			// Set subscription mode
			reqOvr.SubscriptionMode = SubscriptionMode.ByRequest;
					
			rowOfRequest = -1;	// Signifies that all rows should be requested.
			rowCount = 0;  // Reset the row count
			statusBar1.Text = "Submitting initial request...";

			MarketDataAdapter.SendRequest(reqOvr);
		}

		// This overloaded method accepts the textbox object that contains the override for the request
		private void MakeRequest(TextBox txtBox)
		{
			string strTag;

			// Issue new static request
			reqOvr = new RequestForStaticOverride();

			strTag = txtBox.Tag.ToString();
			rowOfRequest = Convert.ToInt32(strTag.Substring(0,1));
			colOfRequest = Convert.ToInt32(strTag.Substring(1,1));

			// Add field and security to RequestForStaticOverride object
			reqOvr.Fields.AddRange(_fields);
			reqOvr.Securities.Add(_securities[rowOfRequest]);

			// Set subscription mode
			reqOvr.SubscriptionMode = SubscriptionMode.BySecurity;
					
			// Add override fields
			// By mnemonic
			OverrideFieldValue ofldval1 = new OverrideFieldValue(_fields[colOfRequest], txtBox.Text);
			reqOvr.OverrideFieldValues.Add(ofldval1);

			MarketDataAdapter.SendRequest(reqOvr);

			rowCount = 0;  // Reset the row count for subsequent requests
			statusBar1.Text = "Submitting override request...";
		}

		private void DisplayData(int rowVal, int colVal, string dpVal)
		{
			foreach(Control cnt in this.grpResults.Controls)
			{
				if(cnt.Tag != null)
				{
					if((rowVal.ToString() + colVal.ToString()) == cnt.Tag.ToString())
						cnt.Text = dpVal;
				}
			}
		}

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			foreach(Control cnt in this.grpOverrides.Controls)
			{
				if(cnt.Tag != null)
				{
					cnt.Text = "";
				}
			}
			MakeRequest();
		}
	}
}
