'*
' *********************************************************
' * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
' * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
' * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
' * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
' * PURPOSE.												 *
' *********************************************************
'*/

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()

        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Setup MarketDataAdapter Event handlers
        AddHandler MarketDataAdapter.StatusEvent, AddressOf MarketDataAdapter_StatusEvent
        MarketDataAdapter.Startup(New ThreadSafeResponseInterceptor(Me))
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf MarketDataAdapter_ReplyEvent

        ' Setup the default and min dates and times for the various DateTime controls
        dateTimeHistoryStart.Value = DateTime.Now.AddMonths(-1)

        dateTimePickerIntradayBarsStart.MinDate = DateTime.Now.AddDays(-20)
        dateTimePickerIntradayBarsEnd.MinDate = DateTime.Now.AddDays(-20)
        dateTimePickerIntradayRawStart.MinDate = DateTime.Now.AddDays(-20)
        dateTimePickerIntradayRawEnd.MinDate = DateTime.Now.AddDays(-20)

        Dim dtNow As DateTime = DateTime.Now
        dateTimePickerIntradayBarsStart.Value = New DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 9, 30, 0)
        dateTimePickerIntradayBarsEnd.Value = New DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 16, 0, 0)

        dateTimePickerIntradayRawStart.Value = New DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 9, 30, 0)
        dateTimePickerIntradayRawEnd.Value = New DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 16, 0, 0)

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents panelInputs As System.Windows.Forms.Panel
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radioButtonSynch As System.Windows.Forms.RadioButton
    Friend WithEvents radioButtonAsynch As System.Windows.Forms.RadioButton
    Friend WithEvents btnRequest As System.Windows.Forms.Button
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents tabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabPageStatic As System.Windows.Forms.TabPage
    Friend WithEvents listBoxStaticFields As System.Windows.Forms.ListBox
    Friend WithEvents tabPageIntradayBars As System.Windows.Forms.TabPage
    Friend WithEvents label8 As System.Windows.Forms.Label
    Friend WithEvents numericUpDownIntradayBars As System.Windows.Forms.NumericUpDown
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents dateTimePickerIntradayBarsEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dateTimePickerIntradayBarsStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents listBoxIntradayBarsFields As System.Windows.Forms.ListBox
    Friend WithEvents tabPageHistory As System.Windows.Forms.TabPage
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents dateTimeHistoryEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dateTimeHistoryStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents listBoxInterdayFields As System.Windows.Forms.ListBox
    Friend WithEvents tabPageIntradayRaw As System.Windows.Forms.TabPage
    Friend WithEvents listBoxIntradayRawFields As System.Windows.Forms.ListBox
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents label7 As System.Windows.Forms.Label
    Friend WithEvents dateTimePickerIntradayRawEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dateTimePickerIntradayRawStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents listBoxSecurities As System.Windows.Forms.ListBox
    Friend WithEvents splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents listBoxLog As System.Windows.Forms.ListBox
    Friend WithEvents dataGridResults As System.Windows.Forms.DataGrid
    Friend WithEvents splitter1 As System.Windows.Forms.Splitter
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.panelInputs = New System.Windows.Forms.Panel
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.radioButtonSynch = New System.Windows.Forms.RadioButton
        Me.radioButtonAsynch = New System.Windows.Forms.RadioButton
        Me.btnRequest = New System.Windows.Forms.Button
        Me.label1 = New System.Windows.Forms.Label
        Me.tabControl1 = New System.Windows.Forms.TabControl
        Me.tabPageStatic = New System.Windows.Forms.TabPage
        Me.listBoxStaticFields = New System.Windows.Forms.ListBox
        Me.tabPageIntradayBars = New System.Windows.Forms.TabPage
        Me.label8 = New System.Windows.Forms.Label
        Me.numericUpDownIntradayBars = New System.Windows.Forms.NumericUpDown
        Me.label5 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.dateTimePickerIntradayBarsEnd = New System.Windows.Forms.DateTimePicker
        Me.dateTimePickerIntradayBarsStart = New System.Windows.Forms.DateTimePicker
        Me.listBoxIntradayBarsFields = New System.Windows.Forms.ListBox
        Me.tabPageHistory = New System.Windows.Forms.TabPage
        Me.label4 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.dateTimeHistoryEnd = New System.Windows.Forms.DateTimePicker
        Me.dateTimeHistoryStart = New System.Windows.Forms.DateTimePicker
        Me.listBoxInterdayFields = New System.Windows.Forms.ListBox
        Me.tabPageIntradayRaw = New System.Windows.Forms.TabPage
        Me.listBoxIntradayRawFields = New System.Windows.Forms.ListBox
        Me.label6 = New System.Windows.Forms.Label
        Me.label7 = New System.Windows.Forms.Label
        Me.dateTimePickerIntradayRawEnd = New System.Windows.Forms.DateTimePicker
        Me.dateTimePickerIntradayRawStart = New System.Windows.Forms.DateTimePicker
        Me.listBoxSecurities = New System.Windows.Forms.ListBox
        Me.splitter2 = New System.Windows.Forms.Splitter
        Me.listBoxLog = New System.Windows.Forms.ListBox
        Me.dataGridResults = New System.Windows.Forms.DataGrid
        Me.splitter1 = New System.Windows.Forms.Splitter
        Me.panelInputs.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        Me.tabControl1.SuspendLayout()
        Me.tabPageStatic.SuspendLayout()
        Me.tabPageIntradayBars.SuspendLayout()
        CType(Me.numericUpDownIntradayBars, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPageHistory.SuspendLayout()
        Me.tabPageIntradayRaw.SuspendLayout()
        CType(Me.dataGridResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'panelInputs
        '
        Me.panelInputs.Controls.Add(Me.groupBox1)
        Me.panelInputs.Controls.Add(Me.btnRequest)
        Me.panelInputs.Controls.Add(Me.label1)
        Me.panelInputs.Controls.Add(Me.tabControl1)
        Me.panelInputs.Controls.Add(Me.listBoxSecurities)
        Me.panelInputs.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelInputs.Location = New System.Drawing.Point(0, 0)
        Me.panelInputs.Name = "panelInputs"
        Me.panelInputs.Size = New System.Drawing.Size(712, 150)
        Me.panelInputs.TabIndex = 6
        '
        'groupBox1
        '
        Me.groupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox1.Controls.Add(Me.radioButtonSynch)
        Me.groupBox1.Controls.Add(Me.radioButtonAsynch)
        Me.groupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox1.Location = New System.Drawing.Point(392, 4)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(224, 32)
        Me.groupBox1.TabIndex = 0
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Request Mode:"
        '
        'radioButtonSynch
        '
        Me.radioButtonSynch.Checked = True
        Me.radioButtonSynch.Location = New System.Drawing.Point(124, 12)
        Me.radioButtonSynch.Name = "radioButtonSynch"
        Me.radioButtonSynch.Size = New System.Drawing.Size(92, 16)
        Me.radioButtonSynch.TabIndex = 1
        Me.radioButtonSynch.TabStop = True
        Me.radioButtonSynch.Text = "Synchronous"
        '
        'radioButtonAsynch
        '
        Me.radioButtonAsynch.Location = New System.Drawing.Point(16, 12)
        Me.radioButtonAsynch.Name = "radioButtonAsynch"
        Me.radioButtonAsynch.Size = New System.Drawing.Size(96, 16)
        Me.radioButtonAsynch.TabIndex = 0
        Me.radioButtonAsynch.Text = "Asynchronous"
        '
        'btnRequest
        '
        Me.btnRequest.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRequest.Location = New System.Drawing.Point(632, 8)
        Me.btnRequest.Name = "btnRequest"
        Me.btnRequest.TabIndex = 1
        Me.btnRequest.Text = "Go"
        '
        'label1
        '
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(8, 8)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(368, 24)
        Me.label1.TabIndex = 2
        Me.label1.Text = "Select your request parameters and then hit <GO>"
        '
        'tabControl1
        '
        Me.tabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabControl1.Controls.Add(Me.tabPageStatic)
        Me.tabControl1.Controls.Add(Me.tabPageIntradayBars)
        Me.tabControl1.Controls.Add(Me.tabPageHistory)
        Me.tabControl1.Controls.Add(Me.tabPageIntradayRaw)
        Me.tabControl1.Location = New System.Drawing.Point(136, 40)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.ShowToolTips = True
        Me.tabControl1.Size = New System.Drawing.Size(572, 106)
        Me.tabControl1.TabIndex = 2
        '
        'tabPageStatic
        '
        Me.tabPageStatic.Controls.Add(Me.listBoxStaticFields)
        Me.tabPageStatic.Location = New System.Drawing.Point(4, 22)
        Me.tabPageStatic.Name = "tabPageStatic"
        Me.tabPageStatic.Size = New System.Drawing.Size(564, 80)
        Me.tabPageStatic.TabIndex = 0
        Me.tabPageStatic.Text = "Static/Bulk"
        Me.tabPageStatic.ToolTipText = "Paramaters for static (requests that reutrn 1 response) and bulk (requests that r" & _
        "eturn a set of values)"
        '
        'listBoxStaticFields
        '
        Me.listBoxStaticFields.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.listBoxStaticFields.Items.AddRange(New Object() {"PX_BID", "PX_ASK", "PX_LAST", "PX_VOLUME", "EQY_DVD_HIST_ALL", "PRODUCT_SEG"})
        Me.listBoxStaticFields.Location = New System.Drawing.Point(8, 8)
        Me.listBoxStaticFields.Name = "listBoxStaticFields"
        Me.listBoxStaticFields.Size = New System.Drawing.Size(152, 69)
        Me.listBoxStaticFields.TabIndex = 0
        Me.listBoxStaticFields.TabStop = False
        '
        'tabPageIntradayBars
        '
        Me.tabPageIntradayBars.Controls.Add(Me.label8)
        Me.tabPageIntradayBars.Controls.Add(Me.numericUpDownIntradayBars)
        Me.tabPageIntradayBars.Controls.Add(Me.label5)
        Me.tabPageIntradayBars.Controls.Add(Me.label2)
        Me.tabPageIntradayBars.Controls.Add(Me.dateTimePickerIntradayBarsEnd)
        Me.tabPageIntradayBars.Controls.Add(Me.dateTimePickerIntradayBarsStart)
        Me.tabPageIntradayBars.Controls.Add(Me.listBoxIntradayBarsFields)
        Me.tabPageIntradayBars.Location = New System.Drawing.Point(4, 22)
        Me.tabPageIntradayBars.Name = "tabPageIntradayBars"
        Me.tabPageIntradayBars.Size = New System.Drawing.Size(564, 80)
        Me.tabPageIntradayBars.TabIndex = 2
        Me.tabPageIntradayBars.Text = "Intraday (Bars)"
        Me.tabPageIntradayBars.ToolTipText = "Enter paramaters for intraday timebar requests"
        Me.tabPageIntradayBars.Visible = False
        '
        'label8
        '
        Me.label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.Location = New System.Drawing.Point(456, 24)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(60, 40)
        Me.label8.TabIndex = 8
        Me.label8.Text = "Bar interval (minutes):"
        '
        'numericUpDownIntradayBars
        '
        Me.numericUpDownIntradayBars.Location = New System.Drawing.Point(520, 32)
        Me.numericUpDownIntradayBars.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.numericUpDownIntradayBars.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numericUpDownIntradayBars.Name = "numericUpDownIntradayBars"
        Me.numericUpDownIntradayBars.Size = New System.Drawing.Size(40, 20)
        Me.numericUpDownIntradayBars.TabIndex = 7
        Me.numericUpDownIntradayBars.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        'label5
        '
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.Location = New System.Drawing.Point(168, 48)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(64, 16)
        Me.label5.TabIndex = 6
        Me.label5.Text = "End Date:"
        '
        'label2
        '
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(168, 16)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(64, 16)
        Me.label2.TabIndex = 5
        Me.label2.Text = "Start Date:"
        '
        'dateTimePickerIntradayBarsEnd
        '
        Me.dateTimePickerIntradayBarsEnd.CustomFormat = "MMMMdd, yyyy - HH:mm:ss"
        Me.dateTimePickerIntradayBarsEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dateTimePickerIntradayBarsEnd.Location = New System.Drawing.Point(248, 48)
        Me.dateTimePickerIntradayBarsEnd.MinDate = New Date(2005, 1, 1, 0, 0, 0, 0)
        Me.dateTimePickerIntradayBarsEnd.Name = "dateTimePickerIntradayBarsEnd"
        Me.dateTimePickerIntradayBarsEnd.TabIndex = 2
        '
        'dateTimePickerIntradayBarsStart
        '
        Me.dateTimePickerIntradayBarsStart.CustomFormat = "MMMMdd, yyyy - HH:mm:ss"
        Me.dateTimePickerIntradayBarsStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dateTimePickerIntradayBarsStart.Location = New System.Drawing.Point(248, 16)
        Me.dateTimePickerIntradayBarsStart.Name = "dateTimePickerIntradayBarsStart"
        Me.dateTimePickerIntradayBarsStart.TabIndex = 1
        '
        'listBoxIntradayBarsFields
        '
        Me.listBoxIntradayBarsFields.Items.AddRange(New Object() {"LAST_PRICE", "BEST_BID", "BEST_ASK"})
        Me.listBoxIntradayBarsFields.Location = New System.Drawing.Point(8, 7)
        Me.listBoxIntradayBarsFields.Name = "listBoxIntradayBarsFields"
        Me.listBoxIntradayBarsFields.Size = New System.Drawing.Size(152, 69)
        Me.listBoxIntradayBarsFields.TabIndex = 0
        '
        'tabPageHistory
        '
        Me.tabPageHistory.Controls.Add(Me.label4)
        Me.tabPageHistory.Controls.Add(Me.label3)
        Me.tabPageHistory.Controls.Add(Me.dateTimeHistoryEnd)
        Me.tabPageHistory.Controls.Add(Me.dateTimeHistoryStart)
        Me.tabPageHistory.Controls.Add(Me.listBoxInterdayFields)
        Me.tabPageHistory.Location = New System.Drawing.Point(4, 22)
        Me.tabPageHistory.Name = "tabPageHistory"
        Me.tabPageHistory.Size = New System.Drawing.Size(564, 80)
        Me.tabPageHistory.TabIndex = 1
        Me.tabPageHistory.Text = "Interday (History)"
        Me.tabPageHistory.ToolTipText = "Enter paramaters for historical (daily) requests."
        Me.tabPageHistory.Visible = False
        '
        'label4
        '
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.Location = New System.Drawing.Point(168, 48)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(64, 16)
        Me.label4.TabIndex = 5
        Me.label4.Text = "End Date:"
        '
        'label3
        '
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(168, 16)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(64, 16)
        Me.label3.TabIndex = 4
        Me.label3.Text = "Start Date:"
        '
        'dateTimeHistoryEnd
        '
        Me.dateTimeHistoryEnd.Location = New System.Drawing.Point(248, 48)
        Me.dateTimeHistoryEnd.Name = "dateTimeHistoryEnd"
        Me.dateTimeHistoryEnd.TabIndex = 3
        '
        'dateTimeHistoryStart
        '
        Me.dateTimeHistoryStart.Location = New System.Drawing.Point(248, 16)
        Me.dateTimeHistoryStart.Name = "dateTimeHistoryStart"
        Me.dateTimeHistoryStart.TabIndex = 2
        '
        'listBoxInterdayFields
        '
        Me.listBoxInterdayFields.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.listBoxInterdayFields.Items.AddRange(New Object() {"PX_BID", "PX_ASK", "PX_LAST", "PX_VOLUME"})
        Me.listBoxInterdayFields.Location = New System.Drawing.Point(8, 7)
        Me.listBoxInterdayFields.Name = "listBoxInterdayFields"
        Me.listBoxInterdayFields.Size = New System.Drawing.Size(152, 69)
        Me.listBoxInterdayFields.TabIndex = 1
        '
        'tabPageIntradayRaw
        '
        Me.tabPageIntradayRaw.Controls.Add(Me.listBoxIntradayRawFields)
        Me.tabPageIntradayRaw.Controls.Add(Me.label6)
        Me.tabPageIntradayRaw.Controls.Add(Me.label7)
        Me.tabPageIntradayRaw.Controls.Add(Me.dateTimePickerIntradayRawEnd)
        Me.tabPageIntradayRaw.Controls.Add(Me.dateTimePickerIntradayRawStart)
        Me.tabPageIntradayRaw.Location = New System.Drawing.Point(4, 22)
        Me.tabPageIntradayRaw.Name = "tabPageIntradayRaw"
        Me.tabPageIntradayRaw.Size = New System.Drawing.Size(564, 80)
        Me.tabPageIntradayRaw.TabIndex = 3
        Me.tabPageIntradayRaw.Text = "Intraday (Raw)"
        Me.tabPageIntradayRaw.ToolTipText = "Enter paramaters for intraday raw requests"
        Me.tabPageIntradayRaw.Visible = False
        '
        'listBoxIntradayRawFields
        '
        Me.listBoxIntradayRawFields.Items.AddRange(New Object() {"LAST_PRICE", "BEST_BID", "BEST_ASK"})
        Me.listBoxIntradayRawFields.Location = New System.Drawing.Point(8, 6)
        Me.listBoxIntradayRawFields.Name = "listBoxIntradayRawFields"
        Me.listBoxIntradayRawFields.Size = New System.Drawing.Size(152, 69)
        Me.listBoxIntradayRawFields.TabIndex = 11
        '
        'label6
        '
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.Location = New System.Drawing.Point(168, 48)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(64, 16)
        Me.label6.TabIndex = 10
        Me.label6.Text = "End Date:"
        '
        'label7
        '
        Me.label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.Location = New System.Drawing.Point(168, 16)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(64, 16)
        Me.label7.TabIndex = 9
        Me.label7.Text = "Start Date:"
        '
        'dateTimePickerIntradayRawEnd
        '
        Me.dateTimePickerIntradayRawEnd.CustomFormat = "MMMMdd, yyyy - HH:mm:ss"
        Me.dateTimePickerIntradayRawEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dateTimePickerIntradayRawEnd.Location = New System.Drawing.Point(248, 48)
        Me.dateTimePickerIntradayRawEnd.Name = "dateTimePickerIntradayRawEnd"
        Me.dateTimePickerIntradayRawEnd.TabIndex = 8
        '
        'dateTimePickerIntradayRawStart
        '
        Me.dateTimePickerIntradayRawStart.CustomFormat = "MMMMdd, yyyy - HH:mm:ss"
        Me.dateTimePickerIntradayRawStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dateTimePickerIntradayRawStart.Location = New System.Drawing.Point(248, 16)
        Me.dateTimePickerIntradayRawStart.Name = "dateTimePickerIntradayRawStart"
        Me.dateTimePickerIntradayRawStart.TabIndex = 7
        '
        'listBoxSecurities
        '
        Me.listBoxSecurities.AllowDrop = True
        Me.listBoxSecurities.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.listBoxSecurities.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listBoxSecurities.Items.AddRange(New Object() {"IBM US Equity", "T US Equity", "MSFT Equity", "SEBL Equity"})
        Me.listBoxSecurities.Location = New System.Drawing.Point(8, 40)
        Me.listBoxSecurities.Name = "listBoxSecurities"
        Me.listBoxSecurities.Size = New System.Drawing.Size(120, 106)
        Me.listBoxSecurities.TabIndex = 0
        Me.listBoxSecurities.TabStop = False
        '
        'splitter2
        '
        Me.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.splitter2.Location = New System.Drawing.Point(0, 420)
        Me.splitter2.Name = "splitter2"
        Me.splitter2.Size = New System.Drawing.Size(712, 3)
        Me.splitter2.TabIndex = 9
        Me.splitter2.TabStop = False
        '
        'listBoxLog
        '
        Me.listBoxLog.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.listBoxLog.HorizontalScrollbar = True
        Me.listBoxLog.Location = New System.Drawing.Point(0, 423)
        Me.listBoxLog.Name = "listBoxLog"
        Me.listBoxLog.Size = New System.Drawing.Size(712, 95)
        Me.listBoxLog.TabIndex = 5
        Me.listBoxLog.TabStop = False
        '
        'dataGridResults
        '
        Me.dataGridResults.CaptionText = "Result Set:"
        Me.dataGridResults.DataMember = ""
        Me.dataGridResults.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dataGridResults.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dataGridResults.Location = New System.Drawing.Point(0, 154)
        Me.dataGridResults.Name = "dataGridResults"
        Me.dataGridResults.Size = New System.Drawing.Size(712, 266)
        Me.dataGridResults.TabIndex = 8
        Me.dataGridResults.TabStop = False
        Me.dataGridResults.Tag = ""
        '
        'splitter1
        '
        Me.splitter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.splitter1.Location = New System.Drawing.Point(0, 150)
        Me.splitter1.Name = "splitter1"
        Me.splitter1.Size = New System.Drawing.Size(712, 4)
        Me.splitter1.TabIndex = 7
        Me.splitter1.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(712, 518)
        Me.Controls.Add(Me.dataGridResults)
        Me.Controls.Add(Me.splitter2)
        Me.Controls.Add(Me.listBoxLog)
        Me.Controls.Add(Me.splitter1)
        Me.Controls.Add(Me.panelInputs)
        Me.Name = "Form1"
        Me.Text = "Synchronous Sample"
        Me.panelInputs.ResumeLayout(False)
        Me.groupBox1.ResumeLayout(False)
        Me.tabControl1.ResumeLayout(False)
        Me.tabPageStatic.ResumeLayout(False)
        Me.tabPageIntradayBars.ResumeLayout(False)
        CType(Me.numericUpDownIntradayBars, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPageHistory.ResumeLayout(False)
        Me.tabPageIntradayRaw.ResumeLayout(False)
        CType(Me.dataGridResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Private Application Methods"

    ' Timestamps and writes log messages to the Log console (listBoxLog)
    Private Sub LogMessage(ByVal msg As String)

        Dim dtNow As DateTime = DateTime.Now
        Dim logMessage As String = String.Format("<{0,2:00}:{1,2:00}:{2,2:00}:{3,3:000}> {4}", _
            dtNow.Hour, dtNow.Minute, dtNow.Second, dtNow.Millisecond, msg)

        If listBoxLog.Items.Count >= 1024 Then
            listBoxLog.Items.RemoveAt(0)
        End If

        listBoxLog.Items.Add(logMessage)
        listBoxLog.SelectedIndex = listBoxLog.Items.Count - 1

    End Sub

    ' Submits a synchronous request to the MarketDataAdapter, waits for response and displays result 
    ' in DataGrid
    Private Sub SubmitSynchronous(ByVal req As Request, ByVal timeout As Integer)

        If Not req Is Nothing Then
            dataGridResults.DataSource = Nothing
            dataGridResults.DataMember = Nothing

            dataGridResults.Refresh()

            Dim msg As String = String.Format("Sending Synchronous request for {0} - {1} securities and {2} fields. Timeout is {3} seconds.", _
                tabControl1.SelectedTab, req.Securities.Count, req.Fields.Count, timeout / 1000.0)

            LogMessage(msg)
            Dim reply As Reply = MarketDataAdapter.SynchronousRequest(req, timeout)
            ProcessReply(reply)
        End If

    End Sub

    ' Submits an Asynchronous request to the MarketDataAdapter. Resonse will be returned
    ' via the ReplyEvent
    Private Sub SubmitAsynchronous(ByVal req As Request)

        Dim msg As String = String.Format("Sending Asynchronous request for {0} - {1} securities and {2} fields.", _
            tabControl1.SelectedTab, req.Securities.Count, req.Fields.Count)
        LogMessage(msg)
        MarketDataAdapter.SendRequest(req)

    End Sub

    ' Generic routine for converting a Reply object to a dataset, and binding
    ' it to a DataGrid. Callled to process results that have been returned 
    ' both synchronously and asynchronously.
    Private Sub ProcessReply(ByVal reply As Reply)

        LogMessage("Received response, now converting to DataSet.")
        Dim resultDataSet As DataSet = Nothing

        Try
            resultDataSet = reply.ToDataSet()

        Catch ex As Exception
            LogMessage("Error converting Reply ToDataSet: " + ex.ToString())
            Return
        End Try

        LogMessage("Dataset conversion has completed.")

        dataGridResults.CaptionText = String.Format("Result Set: ({0})", _
         resultDataSet.DataSetName)
        dataGridResults.DataSource = resultDataSet
        dataGridResults.DataMember = resultDataSet.Tables(0).TableName

    End Sub

    ' Build and return a static request from the contents of the applicable GUI controls
    ' Returns: A RequestForStatic object</returns>
    Private Function MakeStaticRequest() As Request

        Dim req As Request = New RequestForStatic().ToRequest()

        For Each fld As String In listBoxStaticFields.Items
            req.Fields.Add(MarketDataAdapter.FieldTable(fld))
        Next fld

        Return req

    End Function

    ' Build and return an IntradayBars request from the contents of the applicable GUI controls
    ' Returns: A RequestForIntradayBars object</returns>
    Private Function MakeIntradayBarsRequest() As Request

        Dim req As Request = New RequestForIntradayBars().ToRequest()

        For Each fld As String In listBoxIntradayBarsFields.Items
            req.Fields.Add(MarketDataAdapter.FieldTable(fld))
        Next fld

        req.AspectFields.Add(AspectField.AspectTime)
        req.AspectFields.Add(AspectField.AspectOpen)
        req.AspectFields.Add(AspectField.AspectClose)
        req.AspectFields.Add(AspectField.AspectHigh)
        req.AspectFields.Add(AspectField.AspectLow)
        req.AspectFields.Add(AspectField.AspectVolume)

        req.StartDateTime = dateTimePickerIntradayBarsStart.Value
        req.EndDateTime = dateTimePickerIntradayBarsEnd.Value

        req.AspectTimeBarSize = Decimal.ToInt32(numericUpDownIntradayBars.Value)

        Return req

    End Function

    ' Build and return an Intraday Raw request from the contents of the applicable GUI controls.
    ' Returns: A RequestForIntradayRaw object</returns>
    Private Function MakeIntradayRawRequest() As Request

        Dim req As Request = New RequestForIntradayRaw().ToRequest()

        For Each fld As String In listBoxIntradayRawFields.Items
            req.Fields.Add(MarketDataAdapter.FieldTable(fld))
        Next fld

        req.StartDateTime = dateTimePickerIntradayRawStart.Value
        req.EndDateTime = dateTimePickerIntradayRawEnd.Value

        Return req

    End Function

    ' Build and return a history request from the contents of the applicable GUI controls.
    ' Returns: A RequestForHistory object</returns>
    Private Function MakeHistoricalRequest() As Request

        Dim req As Request = New RequestForHistory().ToRequest()

        For Each fld As String In listBoxInterdayFields.Items
            req.Fields.Add(MarketDataAdapter.FieldTable(fld))
        Next fld

        req.Periodicity = Periodicity.ActualDaily
        req.StartExtendedDate = New ExtendedDate(dateTimeHistoryStart.Value)
        req.EndExtendedDate = New ExtendedDate(dateTimeHistoryEnd.Value)

        Return req

    End Function

#End Region

#Region "DragDrop Event Handlers"

    Private Sub listBoxSecurities_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles listBoxSecurities.DragEnter

        If e.Data.GetDataPresent(DataFormats.Text) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If

    End Sub

    Private Sub listBoxSecurities_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles listBoxSecurities.DragDrop

        ' Get the entire text object that has been dropped on us.
        Dim tmp As String = e.Data.GetData(DataFormats.Text).ToString()

        listBoxSecurities.Items.Clear()

        ' Tokenize the string into what (we hope) are Security strings
        Dim sep() As Char = {vbLf, vbTab, vbCr}
        Dim words() As String = tmp.Split(sep)

        Dim scol As SecurityCollection = New SecurityCollection

        For Each s As String In words
            Dim ssec As String = s.Trim()

            If Not ssec.Length = 0 Then
                Try
                    Dim sec As Security = New Security(ssec)
                    listBoxSecurities.Items.Add(sec.DisplayName)
                Catch ex As Exception
                    LogMessage("Invalid security: " + ssec + ". " + ex.ToString())
                End Try
            End If

        Next s

    End Sub

#End Region

#Region "WinForm event handlers"

    Private Sub btnRequest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRequest.Click

        Dim req As Request = Nothing
        Dim timeout As Integer = 30000

        Try
            If tabControl1.SelectedTab.Name = "tabPageStatic" Then
                req = MakeStaticRequest()
            ElseIf (tabControl1.SelectedTab.Name = "tabPageHistory") Then
                req = MakeHistoricalRequest()
            ElseIf (tabControl1.SelectedTab.Name = "tabPageIntradayBars") Then
                req = MakeIntradayBarsRequest()
                timeout = 60000
            ElseIf (tabControl1.SelectedTab.Name = "tabPageIntradayRaw") Then
                req = MakeIntradayRawRequest()
                timeout = 60000
            End If

            If Not req Is Nothing Then
                req.SubscriptionMode = SubscriptionMode.ByRequest

                For Each sec As String In listBoxSecurities.Items
                    req.Securities.Add(sec)
                Next sec
            End If
        Catch ex As Exception
            LogMessage("Unable to create a Request: " + ex.ToString())
            Return
        End Try

        Try
            If radioButtonSynch.Checked = True Then
                SubmitSynchronous(req, timeout)
            Else
                SubmitAsynchronous(req)
            End If
        Catch ex As Exception
            LogMessage("Failed to submit Request: " + ex.ToString())
        End Try

    End Sub

#End Region

#Region "MarketDataAdapater event handlers"

    Private Sub MarketDataAdapter_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        LogMessage("MarketDataAdapter Status message: " + description)

    End Sub

    Private Sub MarketDataAdapter_ReplyEvent(ByVal reply As Reply)

        ProcessReply(reply)

    End Sub

#End Region

End Class
