'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports System.Text
Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents listBoxAspectFields As System.Windows.Forms.ListBox
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents txtDisplay As System.Windows.Forms.TextBox
    Friend WithEvents cboNonTradingDayValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblDataOutput As System.Windows.Forms.Label
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents listboxFields As System.Windows.Forms.ListBox
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents listboxSecurities As System.Windows.Forms.ListBox
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents cboSubMode As System.Windows.Forms.ComboBox
    Friend WithEvents statusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents btnGetData As System.Windows.Forms.Button
    Friend WithEvents directionsToolTip As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.listBoxAspectFields = New System.Windows.Forms.ListBox
        Me.label6 = New System.Windows.Forms.Label
        Me.txtDisplay = New System.Windows.Forms.TextBox
        Me.cboNonTradingDayValue = New System.Windows.Forms.ComboBox
        Me.lblDataOutput = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.listboxFields = New System.Windows.Forms.ListBox
        Me.label3 = New System.Windows.Forms.Label
        Me.listboxSecurities = New System.Windows.Forms.ListBox
        Me.label2 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.cboSubMode = New System.Windows.Forms.ComboBox
        Me.statusBar1 = New System.Windows.Forms.StatusBar
        Me.btnGetData = New System.Windows.Forms.Button
        Me.directionsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'listBoxAspectFields
        '
        Me.listBoxAspectFields.Location = New System.Drawing.Point(336, 36)
        Me.listBoxAspectFields.Name = "listBoxAspectFields"
        Me.listBoxAspectFields.Size = New System.Drawing.Size(152, 69)
        Me.listBoxAspectFields.TabIndex = 46
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(336, 20)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(144, 16)
        Me.label6.TabIndex = 45
        Me.label6.Text = "Aspect Fields:"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDisplay
        '
        Me.txtDisplay.Location = New System.Drawing.Point(8, 188)
        Me.txtDisplay.Multiline = True
        Me.txtDisplay.Name = "txtDisplay"
        Me.txtDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDisplay.Size = New System.Drawing.Size(480, 72)
        Me.txtDisplay.TabIndex = 44
        Me.txtDisplay.Text = ""
        '
        'cboNonTradingDayValue
        '
        Me.cboNonTradingDayValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNonTradingDayValue.Location = New System.Drawing.Point(201, 135)
        Me.cboNonTradingDayValue.Name = "cboNonTradingDayValue"
        Me.cboNonTradingDayValue.Size = New System.Drawing.Size(186, 21)
        Me.cboNonTradingDayValue.TabIndex = 43
        '
        'lblDataOutput
        '
        Me.lblDataOutput.Location = New System.Drawing.Point(8, 168)
        Me.lblDataOutput.Name = "lblDataOutput"
        Me.lblDataOutput.Size = New System.Drawing.Size(448, 16)
        Me.lblDataOutput.TabIndex = 42
        Me.lblDataOutput.Text = "Data Output:"
        Me.lblDataOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(201, 118)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(122, 16)
        Me.label4.TabIndex = 41
        Me.label4.Text = "Non TradingDayValue:"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(496, 47)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(24, 20)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBox1.TabIndex = 40
        Me.pictureBox1.TabStop = False
        '
        'listboxFields
        '
        Me.listboxFields.Location = New System.Drawing.Point(184, 36)
        Me.listboxFields.Name = "listboxFields"
        Me.listboxFields.Size = New System.Drawing.Size(136, 43)
        Me.listboxFields.TabIndex = 39
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(184, 18)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(144, 16)
        Me.label3.TabIndex = 38
        Me.label3.Text = "Main Field:"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'listboxSecurities
        '
        Me.listboxSecurities.Location = New System.Drawing.Point(8, 36)
        Me.listboxSecurities.Name = "listboxSecurities"
        Me.listboxSecurities.Size = New System.Drawing.Size(160, 69)
        Me.listboxSecurities.TabIndex = 37
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(8, 17)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(144, 16)
        Me.label2.TabIndex = 36
        Me.label2.Text = "Securities:"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(8, 117)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(144, 16)
        Me.label1.TabIndex = 35
        Me.label1.Text = "Subscription Mode:"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSubMode
        '
        Me.cboSubMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubMode.Location = New System.Drawing.Point(8, 135)
        Me.cboSubMode.Name = "cboSubMode"
        Me.cboSubMode.Size = New System.Drawing.Size(176, 21)
        Me.cboSubMode.TabIndex = 34
        '
        'statusBar1
        '
        Me.statusBar1.Location = New System.Drawing.Point(0, 277)
        Me.statusBar1.Name = "statusBar1"
        Me.statusBar1.Size = New System.Drawing.Size(528, 16)
        Me.statusBar1.TabIndex = 33
        '
        'btnGetData
        '
        Me.btnGetData.Location = New System.Drawing.Point(416, 125)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.Size = New System.Drawing.Size(72, 32)
        Me.btnGetData.TabIndex = 32
        Me.btnGetData.Text = "Get Data"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(528, 293)
        Me.Controls.Add(Me.listBoxAspectFields)
        Me.Controls.Add(Me.label6)
        Me.Controls.Add(Me.txtDisplay)
        Me.Controls.Add(Me.cboNonTradingDayValue)
        Me.Controls.Add(Me.lblDataOutput)
        Me.Controls.Add(Me.label4)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.listboxFields)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.listboxSecurities)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.cboSubMode)
        Me.Controls.Add(Me.statusBar1)
        Me.Controls.Add(Me.btnGetData)
        Me.Name = "Form1"
        Me.Text = "Intraday Bars Request using API for .NET"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _securities() As String
    Private _fields() As Field
    Private _displayedFields() As String
    Private _aspectFields() As AspectField
    Private _selSecurity As String = ""
    Private _selField As Field
    Private _selAspectField As AspectField
    Private barsReq As RequestForIntradayBars
    Private _errCounter As Long
    Private ftbl As FieldTable
    Private strDisplay As StringBuilder

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Directions on how to run sample available as tooltip on pictureBox question-mark.
        directionsToolTip.SetToolTip(Me.pictureBox1, _
            "Instructions: Select one field/security pairing, along with a SubscriptionMode and NonTradingDayValue, and then click the Get Data button.")

        'Subscribe for status event
        AddHandler MarketDataAdapter.StatusEvent, AddressOf MarketDataAdapter_StatusEvent

        ' Now, startup the MarketDataAdapter.
        ' This will cause several things to happen:
        '   - Bloomberg configuration information will be acquired from the 
        '	   environment (Windows Registry, config files, the Bloomberg terminal, etc)
        '	 - A connection to the local bbcomm will be established.
        '	 - BBCOMM protocol handshaking will be initiated.
        '	 - Adapter resources will be allocated.
        Try
            ' The ThreadSafeResponseInterceptor ensures that the status and reply
            ' event handlers are called on the user interface threads. 
            MarketDataAdapter.Startup(New ThreadSafeResponseInterceptor(Me))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End Try

        'Subscribe for reply events
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf MarketDataAdapter_ReplyEvent

        ' After MarketDataAdapter.Startup has completed successfully, 
        ' get a reference to the FieldTable (factory) object that has been 
        ' loaded (during) MarketDataAdapter.Startup.
        ftbl = MarketDataAdapter.FieldTable
        _fields = New Field() {ftbl("LAST_PRICE"), ftbl("BEST_BID"), ftbl("BEST_ASK")}

        _displayedFields = New String() {"LAST_PRICE", "BEST_BID", "BEST_ASK"}
        _aspectFields = New AspectField() { _
                AspectField.AspectTime, AspectField.AspectOpen, _
                AspectField.AspectHigh, AspectField.AspectLow, _
                AspectField.AspectClose, AspectField.AspectVolume, _
                AspectField.AspectValue, AspectField.AspectCount}

        _securities = New String() { _
                "MSFT US Equity", "IBM US Equity", "GOOG US Equity", "SIRI US Equity", "CSCO US Equity"}

        ' Populate security listbox
        For Each s As String In _securities
            listboxSecurities.Items.Add(s)
        Next s

        ' Populate field listbox
        For Each f As String In _displayedFields
            listboxFields.Items.Add(f)
        Next f

        ' Populate AspectFields listbox with enumerator values
        listBoxAspectFields.Items.AddRange([Enum].GetNames(GetType(AspectField)))

        listboxSecurities.SetSelected(0, True)
        listboxFields.SetSelected(0, True)
        listBoxAspectFields.SetSelected(0, True)

        ' Populate SubscriptionMode combobox with enumerator values
        ' cboSubMode.Items.AddRange([Enum].GetValues(GetType(SubscriptionMode)))
        cboSubMode.DataSource = [Enum].GetValues(GetType(SubscriptionMode))
        cboSubMode.SelectedIndex = 0

        ' Populate NonTradingDayValue combobox with enumerator values
        cboNonTradingDayValue.DataSource = [Enum].GetValues(GetType(NonTradingDayValue))
        cboNonTradingDayValue.SelectedIndex = 0

        statusBar1.ShowPanels = False

    End Sub

    Private Sub MarketDataAdapter_ReplyEvent(ByVal reply As Reply)

        If (Not reply.ReplyError Is Nothing) Then
            statusBar1.Text = "Reply Error has occurred: " + _
                reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description
            Exit Sub
        End If

        _errCounter = 0

        Try
            strDisplay = New StringBuilder

            ' Loop through each security
            For Each sdi As SecurityDataItem In reply.GetSecurityDataItems()

                ' Loop through each bar field
                For Each fdi As AspectFieldDataItem In sdi.FieldsData

                    ' Loop through each aspect field 
                    For Each afi As AspectFieldItem In fdi.AspectFieldItems

                        ' Loop through each data point
                        For Each dp As DataPoint In afi.DataPoints

                            If (Not dp.Value Is Nothing) Then
                                If (dp.IsError) Then
                                    strDisplay.Append(dp.ReplyError.DisplayName.ToString() + vbCrLf)
                                    _errCounter += 1
                                Else
                                    strDisplay.Append(dp.Time.ToString() + " - " + dp.Value.ToString() + vbCrLf)
                                    statusBar1.Text = "Receiving data..."
                                End If
                            End If

                        Next dp

                    Next afi

                Next fdi

            Next sdi

            txtDisplay.Text = strDisplay.ToString()
            statusBar1.Text = " Finished receiving data with " + _errCounter.ToString() + " error(s)."

        Catch ex As Exception
            statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message.ToString()
        End Try

    End Sub

    Private Sub MarketDataAdapter_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        statusBar1.Text = "Status: " + status.ToString() + "  Desc: " + description.ToString()

    End Sub

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click

        ' Clear textbox
        txtDisplay.Clear()

        ' Check that a security has been selected
        If (Not listboxSecurities.SelectedIndex = -1) Then
            _selSecurity = listboxSecurities.SelectedItem.ToString()
        End If

        ' Check that a main field has been selected
        If (Not listboxFields.SelectedIndex = -1) Then
            _selField = ftbl(listboxFields.SelectedItem.ToString())
        End If

        ' Check that an aspectfield has been selected
        If (Not listBoxAspectFields.SelectedIndex = -1) Then
            _selAspectField = _aspectFields(listBoxAspectFields.SelectedIndex)
        End If

        statusBar1.Text = "Preparing request..."

        Subscribe()

    End Sub

    Private Sub Subscribe()

        ' Issue intraday bars request
        barsReq = New RequestForIntradayBars

        ' Add fields and security to RequestForIntradayBars object
        barsReq.Fields.Add(_selField)
        barsReq.Securities.Add(_selSecurity)
        barsReq.AspectFields.Add(_selAspectField)

        ' Set RequestForIntradayBars parameters
        barsReq.StartDateTime = DateTime.Now.Subtract(New TimeSpan(60, 0, 0, 0)) ' Maximum of 50 trading days back
        barsReq.EndDateTime = DateTime.Now
        barsReq.AspectTimeBarSize = 30 ' Barsize is stated in minutes
        barsReq.SubscriptionMode = CType(cboSubMode.SelectedValue, SubscriptionMode)
        ' barsReq.NonTradingDayValue = cboNonTradingDayValue.SelectedValue
        barsReq.ReverseChronological = False

        MarketDataAdapter.SendRequest(barsReq)

        lblDataOutput.Text = "Data Output (From " + barsReq.StartDateTime.ToString() + " to " + barsReq.EndDateTime.ToString() + "):"

    End Sub

End Class
