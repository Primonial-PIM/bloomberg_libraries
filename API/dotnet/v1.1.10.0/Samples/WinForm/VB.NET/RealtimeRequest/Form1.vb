'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnStop As System.Windows.Forms.Button
    Friend WithEvents lblDisplay As System.Windows.Forms.Label
    Friend WithEvents listboxFields As System.Windows.Forms.ListBox
    Friend WithEvents listboxSecurities As System.Windows.Forms.ListBox
    Friend WithEvents cboSubMode As System.Windows.Forms.ComboBox
    Friend WithEvents statusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents btnGetData As System.Windows.Forms.Button
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents directionsToolTip As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.label3 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnStop = New System.Windows.Forms.Button
        Me.lblDisplay = New System.Windows.Forms.Label
        Me.listboxFields = New System.Windows.Forms.ListBox
        Me.listboxSecurities = New System.Windows.Forms.ListBox
        Me.cboSubMode = New System.Windows.Forms.ComboBox
        Me.statusBar1 = New System.Windows.Forms.StatusBar
        Me.btnGetData = New System.Windows.Forms.Button
        Me.label1 = New System.Windows.Forms.Label
        Me.directionsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(184, 16)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(144, 16)
        Me.label3.TabIndex = 8
        Me.label3.Text = "Fields:"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(8, 16)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(144, 16)
        Me.label2.TabIndex = 7
        Me.label2.Text = "Securities:"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(392, 40)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(24, 20)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBox1.TabIndex = 19
        Me.pictureBox1.TabStop = False
        '
        'btnStop
        '
        Me.btnStop.Enabled = False
        Me.btnStop.Location = New System.Drawing.Point(392, 86)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(72, 32)
        Me.btnStop.TabIndex = 18
        Me.btnStop.Text = "Stop"
        '
        'lblDisplay
        '
        Me.lblDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDisplay.Location = New System.Drawing.Point(184, 132)
        Me.lblDisplay.Name = "lblDisplay"
        Me.lblDisplay.Size = New System.Drawing.Size(200, 24)
        Me.lblDisplay.TabIndex = 17
        Me.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'listboxFields
        '
        Me.listboxFields.Location = New System.Drawing.Point(184, 36)
        Me.listboxFields.Name = "listboxFields"
        Me.listboxFields.Size = New System.Drawing.Size(200, 69)
        Me.listboxFields.TabIndex = 16
        '
        'listboxSecurities
        '
        Me.listboxSecurities.Location = New System.Drawing.Point(8, 36)
        Me.listboxSecurities.Name = "listboxSecurities"
        Me.listboxSecurities.Size = New System.Drawing.Size(160, 69)
        Me.listboxSecurities.TabIndex = 15
        '
        'cboSubMode
        '
        Me.cboSubMode.Location = New System.Drawing.Point(8, 132)
        Me.cboSubMode.Name = "cboSubMode"
        Me.cboSubMode.Size = New System.Drawing.Size(160, 21)
        Me.cboSubMode.TabIndex = 14
        '
        'statusBar1
        '
        Me.statusBar1.Location = New System.Drawing.Point(0, 173)
        Me.statusBar1.Name = "statusBar1"
        Me.statusBar1.Size = New System.Drawing.Size(472, 16)
        Me.statusBar1.TabIndex = 13
        '
        'btnGetData
        '
        Me.btnGetData.Location = New System.Drawing.Point(392, 124)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.Size = New System.Drawing.Size(72, 32)
        Me.btnGetData.TabIndex = 12
        Me.btnGetData.Text = "Get Data"
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(8, 115)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(144, 16)
        Me.label1.TabIndex = 20
        Me.label1.Text = "Subscription Mode:"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(472, 189)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.lblDisplay)
        Me.Controls.Add(Me.listboxFields)
        Me.Controls.Add(Me.listboxSecurities)
        Me.Controls.Add(Me.cboSubMode)
        Me.Controls.Add(Me.statusBar1)
        Me.Controls.Add(Me.btnGetData)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label2)
        Me.Name = "Form1"
        Me.Text = "Realtime Request using API for .NET"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _ReplyCt As Integer
    Private _securityList() As String
    Private _fieldList() As Field
    Private _selSecurity As String = ""
    Private _selField As Field
    Private rtReq As RequestForRealtime
    Private _reqId As Guid
    Private ftbl As FieldTable

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Directions on how to run sample available as tooltip on pictureBox question-mark.
        directionsToolTip.SetToolTip(Me.pictureBox1, _
            "Instructions: Select one field/security pairing and then click the Get Data button.")

        'Subscribe for status events
        AddHandler MarketDataAdapter.StatusEvent, AddressOf MarketDataAdapter_StatusEvent

        ' Now, startup the MarketDataAdapter.
        ' This will cause several things to happen:
        ' - Bloomberg configuration information will be acquired from the 
        '   environment (Windows Registry, config files, the Bloomberg terminal, etc)
        ' - A connection to the local bbcomm will be established.
        ' - BBCOMM protocol handshaking will be initiated.
        ' - Adapter resources will be allocated.
        Try
            MarketDataAdapter.Startup()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End Try

        'Subscribe for reply events
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf MarketDataAdapter_ReplyEvent

        ' After MarketDataAdapter.Startup has completed successfully, 
        ' get a reference to the FieldTable (factory) object that has been 
        ' loaded (during) MarketDataAdapter.Startup.
        ftbl = MarketDataAdapter.FieldTable

        Dim _fieldList() As Field = {ftbl("LAST_PRICE"), ftbl("BID"), ftbl("ASK"), _
            ftbl("VOLUME"), ftbl("LAST_TRADE")}

        _securityList = New String() _
             {"MSFT US Equity", "IBM US Equity", "GOOG US Equity", "SIRI US Equity", "CSCO US Equity"}

        ' Populate security listbox
        For Each s As String In _securityList
            listboxSecurities.Items.Add(s)
        Next s

        ' Populate field listbox
        For Each f As Field In _fieldList
            listboxFields.Items.Add(f)
        Next f

        listboxSecurities.SetSelected(0, True)
        listboxFields.SetSelected(0, True)

        ' Populate SubscriptionMode combobox with enumerator values
        ' cboSubMode.Items.AddRange([Enum].GetValues(GetType(SubscriptionMode)))
        cboSubMode.DataSource = [Enum].GetValues(GetType(SubscriptionMode))
        cboSubMode.SelectedIndex = 0

        statusBar1.ShowPanels = False

    End Sub

    Private Sub MarketDataAdapter_ReplyEvent(ByVal reply As Reply)

        ' Check to see if we will need to marshall Reply to the Form's thread (we will).
        ' Then marshall back onto the Form's thread via the message pump.	
        If InvokeRequired Then
            Invoke(New ReplyEventHandler(AddressOf MarketDataAdapter_ReplyEvent), New Object() {reply})
            Return
        End If

        If (Not reply.ReplyError Is Nothing) Then
            statusBar1.Text = "Reply Error has occurred: " + _
                reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description
            Exit Sub
        End If

        ' Begin by searching for SecurityDataItem, then the FieldDataItem
        ' and finally the DataPoint.
        Try
            ' Loop through each security
            For Each sdi As SecurityDataItem In reply.GetSecurityDataItems()
                ' Loop through each field
                For Each fdi As FieldDataItem In sdi.FieldsData
                    ' Loop through each data point
                    For Each dp As DataPoint In fdi.DataPoints
                        If Not dp Is Nothing Then
                            lblDisplay.Text = _selField.Mnemonic + ": " + dp.Value.ToString()
                        End If
                    Next dp
                Next fdi
            Next sdi

            _ReplyCt = _ReplyCt + 1
            statusBar1.Text = "Reply Event fired " + _ReplyCt.ToString() + " times"

        Catch ex As Exception
            statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message.ToString()
        End Try

    End Sub

    Private Sub MarketDataAdapter_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        Try
            If InvokeRequired Then
                Invoke(New StatusEventHandler(AddressOf MarketDataAdapter_StatusEvent), New Object() {status, description})
            Else
                statusBar1.Text = "Status: " + status.ToString() + "  Desc: " + description.ToString()
            End If

        Catch ex As Exception
            statusBar1.Text = "Error has occurred in StatusEvent handler: " + ex.Message
        End Try

    End Sub

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click

        _ReplyCt = 0

        ' Check that a security has been selected
        If Not listboxSecurities.SelectedIndex = -1 Then
            _selSecurity = listboxSecurities.SelectedItem.ToString()
        End If

        ' Check that a field has been selected
        If Not listboxFields.SelectedIndex = -1 Then
            _selField = ftbl(listboxFields.SelectedItem.ToString())
        End If

        statusBar1.Text = "Preparing request..."

        ' Subscribe to realtime data.
        Subscribe()

        ' Disable Get Data button and enable Stop button
        btnGetData.Enabled = False
        btnStop.Enabled = True

    End Sub

    Private Sub Subscribe()

        ' Create a new RequestForRealtime object
        rtReq = New RequestForRealtime

        ' Add field and security to RequestForRealtime object
        rtReq.Fields.Add(_selField)
        rtReq.Securities.Add(_selSecurity)

        ' Set subscription mode
        rtReq.SubscriptionMode = CType(cboSubMode.SelectedValue(), SubscriptionMode)

        ' To monitor realtime requests, the Monitor property must be set to true (default)
        rtReq.Monitor = True

        statusBar1.Text = "Submitting request for " + _selSecurity

        ' Send the request to the Adapter 
        _reqId = MarketDataAdapter.SendRequest(rtReq)

    End Sub
    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click

        ' Pass guid that is returned from SendRequest() call
        MarketDataAdapter.CancelRequest(_reqId)

        btnGetData.Enabled = True
        btnStop.Enabled = False
        statusBar1.Text = "The request has been cancelled by user."

    End Sub
End Class
