'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports System.Text
Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboPeriodicity As System.Windows.Forms.ComboBox
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents listboxFields As System.Windows.Forms.ListBox
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents listboxSecurities As System.Windows.Forms.ListBox
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents cboSubMode As System.Windows.Forms.ComboBox
    Friend WithEvents statusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents btnGetData As System.Windows.Forms.Button
    Friend WithEvents txtDisplay As System.Windows.Forms.TextBox
    Friend WithEvents directionsToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents lblDataOutput As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.cboPeriodicity = New System.Windows.Forms.ComboBox
        Me.lblDataOutput = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.listboxFields = New System.Windows.Forms.ListBox
        Me.label3 = New System.Windows.Forms.Label
        Me.listboxSecurities = New System.Windows.Forms.ListBox
        Me.label2 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.cboSubMode = New System.Windows.Forms.ComboBox
        Me.statusBar1 = New System.Windows.Forms.StatusBar
        Me.btnGetData = New System.Windows.Forms.Button
        Me.txtDisplay = New System.Windows.Forms.TextBox
        Me.directionsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'cboPeriodicity
        '
        Me.cboPeriodicity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodicity.Location = New System.Drawing.Point(184, 133)
        Me.cboPeriodicity.Name = "cboPeriodicity"
        Me.cboPeriodicity.Size = New System.Drawing.Size(200, 21)
        Me.cboPeriodicity.TabIndex = 28
        '
        'lblDataOutput
        '
        Me.lblDataOutput.Location = New System.Drawing.Point(8, 164)
        Me.lblDataOutput.Name = "lblDataOutput"
        Me.lblDataOutput.Size = New System.Drawing.Size(360, 16)
        Me.lblDataOutput.TabIndex = 27
        Me.lblDataOutput.Text = "Data Output:"
        Me.lblDataOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(184, 116)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(144, 16)
        Me.label4.TabIndex = 26
        Me.label4.Text = "Periodicity:"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(392, 35)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(24, 20)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBox1.TabIndex = 24
        Me.pictureBox1.TabStop = False
        '
        'listboxFields
        '
        Me.listboxFields.Location = New System.Drawing.Point(184, 35)
        Me.listboxFields.Name = "listboxFields"
        Me.listboxFields.Size = New System.Drawing.Size(200, 69)
        Me.listboxFields.TabIndex = 23
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(184, 17)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(144, 16)
        Me.label3.TabIndex = 22
        Me.label3.Text = "Fields:"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'listboxSecurities
        '
        Me.listboxSecurities.Location = New System.Drawing.Point(8, 35)
        Me.listboxSecurities.Name = "listboxSecurities"
        Me.listboxSecurities.Size = New System.Drawing.Size(160, 69)
        Me.listboxSecurities.TabIndex = 21
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(8, 16)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(144, 16)
        Me.label2.TabIndex = 20
        Me.label2.Text = "Securities:"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(8, 115)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(144, 16)
        Me.label1.TabIndex = 19
        Me.label1.Text = "Subscription Mode:"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSubMode
        '
        Me.cboSubMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubMode.Location = New System.Drawing.Point(8, 133)
        Me.cboSubMode.Name = "cboSubMode"
        Me.cboSubMode.Size = New System.Drawing.Size(160, 21)
        Me.cboSubMode.TabIndex = 18
        '
        'statusBar1
        '
        Me.statusBar1.Location = New System.Drawing.Point(0, 277)
        Me.statusBar1.Name = "statusBar1"
        Me.statusBar1.Size = New System.Drawing.Size(472, 16)
        Me.statusBar1.TabIndex = 17
        '
        'btnGetData
        '
        Me.btnGetData.Location = New System.Drawing.Point(391, 121)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.Size = New System.Drawing.Size(72, 32)
        Me.btnGetData.TabIndex = 16
        Me.btnGetData.Text = "Get Data"
        '
        'txtDisplay
        '
        Me.txtDisplay.Location = New System.Drawing.Point(8, 184)
        Me.txtDisplay.Multiline = True
        Me.txtDisplay.Name = "txtDisplay"
        Me.txtDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDisplay.Size = New System.Drawing.Size(456, 80)
        Me.txtDisplay.TabIndex = 29
        Me.txtDisplay.Text = ""
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(472, 293)
        Me.Controls.Add(Me.txtDisplay)
        Me.Controls.Add(Me.cboPeriodicity)
        Me.Controls.Add(Me.lblDataOutput)
        Me.Controls.Add(Me.label4)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.listboxFields)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.listboxSecurities)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.cboSubMode)
        Me.Controls.Add(Me.statusBar1)
        Me.Controls.Add(Me.btnGetData)
        Me.Name = "Form1"
        Me.Text = "History Request using API for .NET"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _securityList() As String
    Private _selSecurity As String = ""
    Private _selField As Field
    Private histReq As RequestForHistory
    Private ftbl As FieldTable
    Private strDisplay As StringBuilder

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Directions on how to run sample available as tooltip on pictureBox question-mark.
        directionsToolTip.SetToolTip(Me.pictureBox1, _
            "Instructions: Select one field/security pairing, along with a SubscriptionMode and Periodicity, and then click the Get Data button.")

        'Subscribe for status event
        AddHandler MarketDataAdapter.StatusEvent, AddressOf MarketDataAdapter_StatusEvent

        ' Now, startup the MarketDataAdapter.
        ' This will cause several things to happen:
        ' - Bloomberg configuration information will be acquired from the 
        '   environment (Windows Registry, config files, the Bloomberg terminal, etc)
        ' - A connection to the local bbcomm will be established.
        ' - BBCOMM protocol handshaking will be initiated.
        ' - Adapter resources will be allocated.
        Try
            ' The ThreadSafeResponseInterceptor ensures that the status and reply
            ' event handlers are called on the user interface threads. 
            MarketDataAdapter.Startup(New ThreadSafeResponseInterceptor(Me))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End Try

        'Subscribe for reply event
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf MarketDataAdapter_ReplyEvent

        ' After MarketDataAdapter.Startup has completed successfully, 
        ' get a reference to the FieldTable (factory) object that has been 
        ' loaded (during) MarketDataAdapter.Startup.
        ftbl = MarketDataAdapter.FieldTable

        Dim _fieldList() As Field = {ftbl("PX_LAST"), ftbl("PX_BID"), ftbl("PX_ASK"), _
            ftbl("PX_OPEN"), ftbl("PX_HIGH")}

        _securityList = New String() _
             {"MSFT US Equity", "IBM US Equity", "GOOG US Equity", "SIRI US Equity", "CSCO US Equity"}

        ' Populate security listbox
        For Each s As String In _securityList
            listboxSecurities.Items.Add(s)
        Next s

        ' Populate field listbox
        For Each f As Field In _fieldList
            listboxFields.Items.Add(f.ToString())
        Next f

        listboxSecurities.SetSelected(0, True)
        listboxFields.SetSelected(0, True)

        ' Populate SubscriptionMode combobox with enumerator values
        ' cboSubMode.Items.AddRange([Enum].GetValues(GetType(SubscriptionMode)))
        cboSubMode.DataSource = [Enum].GetValues(GetType(SubscriptionMode))
        cboSubMode.SelectedIndex = 0

        ' Populate Periodicity combobox with enumerator values
        cboPeriodicity.DataSource = [Enum].GetValues(GetType(Periodicity))
        cboPeriodicity.SelectedIndex = 0

        statusBar1.ShowPanels = False

    End Sub

    Private Sub MarketDataAdapter_ReplyEvent(ByVal reply As Reply)

        If (Not reply.ReplyError Is Nothing) Then
            statusBar1.Text = "Reply Error has occurred: " + _
                reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description
            Exit Sub
        End If

        ' Begin by searching for SecurityDataItem, then the FieldDataItem
        ' and finally the DataPoint.
        Try
            strDisplay = New StringBuilder

            lblDataOutput.Text = "Data Output (" + _selSecurity + "):"
            ' Loop through each security
            For Each sdi As SecurityDataItem In reply.GetSecurityDataItems()
                ' Loop through each field
                For Each fdi As FieldDataItem In sdi.FieldsData
                    ' Loop through each data point
                    For Each dp As DataPoint In fdi.DataPoints
                        If Not dp Is Nothing Then
                            If dp.IsError Then
                                strDisplay.Append(dp.ReplyError.DisplayName.ToString())
                            Else
                                strDisplay.Append(dp.Time.ToShortDateString() + ": " + dp.Value.ToString() + vbCrLf)
                            End If
                        End If
                    Next dp
                Next fdi
            Next sdi

            txtDisplay.Text = strDisplay.ToString()
            statusBar1.Text = "Data received successfully."

        Catch ex As Exception
            statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message
        End Try

    End Sub

    Private Sub MarketDataAdapter_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        statusBar1.Text = "Status: " + status.ToString() + "  Desc: " + description.ToString()

    End Sub

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click

        ' Check that a security has been selected
        If Not listboxSecurities.SelectedIndex = -1 Then
            _selSecurity = listboxSecurities.SelectedItem.ToString()
        End If

        ' Check that a field has been selected
        If Not listboxFields.SelectedIndex = -1 Then
            _selField = ftbl(listboxFields.SelectedItem.ToString())
        End If

        statusBar1.Text = "Preparing request..."

        ' Request historical data.
        GetHistory()

    End Sub

    Private Sub GetHistory()

        ' Clear output box
        txtDisplay.Clear()

        ' Create a new RequestForHistory object
        histReq = New RequestForHistory

        ' Add field and security to RequestForHistory object
        histReq.Fields.Add(_selField)
        histReq.Securities.Add(_selSecurity)

        ' Set RequestForHistory parameters
        histReq.Currencies.Add(New Currency("USD"))

        'The following three blocks of code are equivalent ways 
        'of constructing a historical date range.

        'Construct date range using DateTime
        'histReq.StartDate = New ExtendedDate(DateTime.Now.Subtract(New TimeSpan(5, 0, 0, 0, 0)))
        'histReq.EndDate = New ExtendedDate(DateTime.Now)

        'Construct date range using ExtendedDate.Parse
        'histReq.StartDate = ExtendedDate.Parse("-5AD")
        'histReq.EndDate = ExtendedDate.Today

        'Construct date range using Anchor and Offset objects
        Dim anchor As DateAnchor = New DateAnchor(DateTime.Now) 'optional
        Dim offset As DateOffset = New DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day)
        histReq.StartDate = New ExtendedDate(anchor, offset)
        histReq.EndDate = ExtendedDate.Today

        histReq.Periodicity = CType(cboPeriodicity.SelectedValue, Periodicity)
        histReq.SubscriptionMode = CType(cboSubMode.SelectedValue, SubscriptionMode)
        histReq.DisplayNonTradingDays = DisplayNonTradingDays.AllCalendar
        histReq.NonTradingDayValue = NonTradingDayValue.PreviousDay
        histReq.ReverseChronological = False
        histReq.MaxPoints = 9999

        statusBar1.Text = "Submitting request for " + _selSecurity

        ' Cast RequestForHistory object to Request and then
        ' send the request to the Adapter 
        MarketDataAdapter.SendRequest(histReq)

    End Sub

End Class