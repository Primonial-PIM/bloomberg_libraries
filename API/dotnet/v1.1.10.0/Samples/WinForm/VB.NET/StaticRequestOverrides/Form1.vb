'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpOverrides As System.Windows.Forms.GroupBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents txt30YrYield As System.Windows.Forms.TextBox
    Friend WithEvents txt30YrPrice As System.Windows.Forms.TextBox
    Friend WithEvents txt20YrYield As System.Windows.Forms.TextBox
    Friend WithEvents txt20YrPrice As System.Windows.Forms.TextBox
    Friend WithEvents txt10YrYield As System.Windows.Forms.TextBox
    Friend WithEvents txt10YrPrice As System.Windows.Forms.TextBox
    Friend WithEvents txt5YrYield As System.Windows.Forms.TextBox
    Friend WithEvents txt5YrPrice As System.Windows.Forms.TextBox
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents pictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents label7 As System.Windows.Forms.Label
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents grpResults As System.Windows.Forms.GroupBox
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents label8 As System.Windows.Forms.Label
    Friend WithEvents lbl10YrYield As System.Windows.Forms.Label
    Friend WithEvents lbl30YrPrice As System.Windows.Forms.Label
    Friend WithEvents lbl5YrPrice As System.Windows.Forms.Label
    Friend WithEvents lbl20YrYield As System.Windows.Forms.Label
    Friend WithEvents lbl20YrPrice As System.Windows.Forms.Label
    Friend WithEvents lbl30YrYield As System.Windows.Forms.Label
    Friend WithEvents lbl5YrYield As System.Windows.Forms.Label
    Friend WithEvents lbl10YrPrice As System.Windows.Forms.Label
    Friend WithEvents statusBar1 As System.Windows.Forms.StatusBar
    Private WithEvents directionsToolTip As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.directionsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.grpOverrides = New System.Windows.Forms.GroupBox
        Me.btnReset = New System.Windows.Forms.Button
        Me.txt30YrYield = New System.Windows.Forms.TextBox
        Me.txt30YrPrice = New System.Windows.Forms.TextBox
        Me.txt20YrYield = New System.Windows.Forms.TextBox
        Me.txt20YrPrice = New System.Windows.Forms.TextBox
        Me.txt10YrYield = New System.Windows.Forms.TextBox
        Me.txt10YrPrice = New System.Windows.Forms.TextBox
        Me.txt5YrYield = New System.Windows.Forms.TextBox
        Me.txt5YrPrice = New System.Windows.Forms.TextBox
        Me.label3 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.pictureBox1 = New System.Windows.Forms.PictureBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.label7 = New System.Windows.Forms.Label
        Me.label6 = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.grpResults = New System.Windows.Forms.GroupBox
        Me.label1 = New System.Windows.Forms.Label
        Me.label8 = New System.Windows.Forms.Label
        Me.lbl10YrYield = New System.Windows.Forms.Label
        Me.lbl30YrPrice = New System.Windows.Forms.Label
        Me.lbl5YrPrice = New System.Windows.Forms.Label
        Me.lbl20YrYield = New System.Windows.Forms.Label
        Me.lbl20YrPrice = New System.Windows.Forms.Label
        Me.lbl30YrYield = New System.Windows.Forms.Label
        Me.lbl5YrYield = New System.Windows.Forms.Label
        Me.lbl10YrPrice = New System.Windows.Forms.Label
        Me.statusBar1 = New System.Windows.Forms.StatusBar
        Me.grpOverrides.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        Me.grpResults.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpOverrides
        '
        Me.grpOverrides.Controls.Add(Me.btnReset)
        Me.grpOverrides.Controls.Add(Me.txt30YrYield)
        Me.grpOverrides.Controls.Add(Me.txt30YrPrice)
        Me.grpOverrides.Controls.Add(Me.txt20YrYield)
        Me.grpOverrides.Controls.Add(Me.txt20YrPrice)
        Me.grpOverrides.Controls.Add(Me.txt10YrYield)
        Me.grpOverrides.Controls.Add(Me.txt10YrPrice)
        Me.grpOverrides.Controls.Add(Me.txt5YrYield)
        Me.grpOverrides.Controls.Add(Me.txt5YrPrice)
        Me.grpOverrides.Controls.Add(Me.label3)
        Me.grpOverrides.Controls.Add(Me.label2)
        Me.grpOverrides.Location = New System.Drawing.Point(120, 14)
        Me.grpOverrides.Name = "grpOverrides"
        Me.grpOverrides.Size = New System.Drawing.Size(216, 190)
        Me.grpOverrides.TabIndex = 17
        Me.grpOverrides.TabStop = False
        Me.grpOverrides.Text = "Overrides"
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(80, 157)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(64, 24)
        Me.btnReset.TabIndex = 8
        Me.btnReset.Text = "Reset"
        '
        'txt30YrYield
        '
        Me.txt30YrYield.Location = New System.Drawing.Point(117, 128)
        Me.txt30YrYield.Name = "txt30YrYield"
        Me.txt30YrYield.Size = New System.Drawing.Size(88, 20)
        Me.txt30YrYield.TabIndex = 7
        Me.txt30YrYield.Tag = "31"
        Me.txt30YrYield.Text = ""
        '
        'txt30YrPrice
        '
        Me.txt30YrPrice.Location = New System.Drawing.Point(19, 128)
        Me.txt30YrPrice.Name = "txt30YrPrice"
        Me.txt30YrPrice.Size = New System.Drawing.Size(88, 20)
        Me.txt30YrPrice.TabIndex = 6
        Me.txt30YrPrice.Tag = "30"
        Me.txt30YrPrice.Text = ""
        '
        'txt20YrYield
        '
        Me.txt20YrYield.Location = New System.Drawing.Point(117, 104)
        Me.txt20YrYield.Name = "txt20YrYield"
        Me.txt20YrYield.Size = New System.Drawing.Size(88, 20)
        Me.txt20YrYield.TabIndex = 5
        Me.txt20YrYield.Tag = "21"
        Me.txt20YrYield.Text = ""
        '
        'txt20YrPrice
        '
        Me.txt20YrPrice.Location = New System.Drawing.Point(19, 104)
        Me.txt20YrPrice.Name = "txt20YrPrice"
        Me.txt20YrPrice.Size = New System.Drawing.Size(88, 20)
        Me.txt20YrPrice.TabIndex = 4
        Me.txt20YrPrice.Tag = "20"
        Me.txt20YrPrice.Text = ""
        '
        'txt10YrYield
        '
        Me.txt10YrYield.Location = New System.Drawing.Point(117, 80)
        Me.txt10YrYield.Name = "txt10YrYield"
        Me.txt10YrYield.Size = New System.Drawing.Size(88, 20)
        Me.txt10YrYield.TabIndex = 3
        Me.txt10YrYield.Tag = "11"
        Me.txt10YrYield.Text = ""
        '
        'txt10YrPrice
        '
        Me.txt10YrPrice.Location = New System.Drawing.Point(19, 80)
        Me.txt10YrPrice.Name = "txt10YrPrice"
        Me.txt10YrPrice.Size = New System.Drawing.Size(88, 20)
        Me.txt10YrPrice.TabIndex = 2
        Me.txt10YrPrice.Tag = "10"
        Me.txt10YrPrice.Text = ""
        '
        'txt5YrYield
        '
        Me.txt5YrYield.Location = New System.Drawing.Point(117, 56)
        Me.txt5YrYield.Name = "txt5YrYield"
        Me.txt5YrYield.Size = New System.Drawing.Size(88, 20)
        Me.txt5YrYield.TabIndex = 1
        Me.txt5YrYield.Tag = "01"
        Me.txt5YrYield.Text = ""
        '
        'txt5YrPrice
        '
        Me.txt5YrPrice.Location = New System.Drawing.Point(19, 56)
        Me.txt5YrPrice.Name = "txt5YrPrice"
        Me.txt5YrPrice.Size = New System.Drawing.Size(88, 20)
        Me.txt5YrPrice.TabIndex = 0
        Me.txt5YrPrice.Tag = "00"
        Me.txt5YrPrice.Text = ""
        '
        'label3
        '
        Me.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(118, 28)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(85, 20)
        Me.label3.TabIndex = 6
        Me.label3.Text = "Yield"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label2
        '
        Me.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(21, 28)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(85, 20)
        Me.label2.TabIndex = 4
        Me.label2.Text = "Price"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pictureBox1
        '
        Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"), System.Drawing.Image)
        Me.pictureBox1.Location = New System.Drawing.Point(568, 4)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(24, 20)
        Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureBox1.TabIndex = 19
        Me.pictureBox1.TabStop = False
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.InitialDelay = 100
        Me.ToolTip1.ReshowDelay = 100
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.label7)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.label5)
        Me.groupBox1.Controls.Add(Me.label4)
        Me.groupBox1.Location = New System.Drawing.Point(16, 14)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(96, 160)
        Me.groupBox1.TabIndex = 16
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "US Treasuries"
        '
        'label7
        '
        Me.label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.Location = New System.Drawing.Point(27, 128)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(56, 20)
        Me.label7.TabIndex = 13
        Me.label7.Text = "30 Year:"
        Me.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label6
        '
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.Location = New System.Drawing.Point(27, 56)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(56, 20)
        Me.label6.TabIndex = 12
        Me.label6.Text = "5 Year:"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label5
        '
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.Location = New System.Drawing.Point(27, 104)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(56, 20)
        Me.label5.TabIndex = 11
        Me.label5.Text = "20 Year:"
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label4
        '
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.Location = New System.Drawing.Point(27, 80)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(56, 20)
        Me.label4.TabIndex = 10
        Me.label4.Text = "10 Year:"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpResults
        '
        Me.grpResults.Controls.Add(Me.label1)
        Me.grpResults.Controls.Add(Me.label8)
        Me.grpResults.Controls.Add(Me.lbl10YrYield)
        Me.grpResults.Controls.Add(Me.lbl30YrPrice)
        Me.grpResults.Controls.Add(Me.lbl5YrPrice)
        Me.grpResults.Controls.Add(Me.lbl20YrYield)
        Me.grpResults.Controls.Add(Me.lbl20YrPrice)
        Me.grpResults.Controls.Add(Me.lbl30YrYield)
        Me.grpResults.Controls.Add(Me.lbl5YrYield)
        Me.grpResults.Controls.Add(Me.lbl10YrPrice)
        Me.grpResults.Location = New System.Drawing.Point(344, 14)
        Me.grpResults.Name = "grpResults"
        Me.grpResults.Size = New System.Drawing.Size(248, 160)
        Me.grpResults.TabIndex = 18
        Me.grpResults.TabStop = False
        Me.grpResults.Text = "Results"
        '
        'label1
        '
        Me.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(128, 28)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(104, 20)
        Me.label1.TabIndex = 23
        Me.label1.Text = "Yield"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label8
        '
        Me.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.Location = New System.Drawing.Point(16, 28)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(104, 20)
        Me.label8.TabIndex = 22
        Me.label8.Text = "Price"
        Me.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl10YrYield
        '
        Me.lbl10YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl10YrYield.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl10YrYield.Location = New System.Drawing.Point(129, 80)
        Me.lbl10YrYield.Name = "lbl10YrYield"
        Me.lbl10YrYield.Size = New System.Drawing.Size(104, 21)
        Me.lbl10YrYield.TabIndex = 18
        Me.lbl10YrYield.Tag = "11"
        Me.lbl10YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl30YrPrice
        '
        Me.lbl30YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl30YrPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl30YrPrice.Location = New System.Drawing.Point(17, 128)
        Me.lbl30YrPrice.Name = "lbl30YrPrice"
        Me.lbl30YrPrice.Size = New System.Drawing.Size(104, 21)
        Me.lbl30YrPrice.TabIndex = 17
        Me.lbl30YrPrice.Tag = "30"
        Me.lbl30YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl5YrPrice
        '
        Me.lbl5YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl5YrPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5YrPrice.Location = New System.Drawing.Point(17, 56)
        Me.lbl5YrPrice.Name = "lbl5YrPrice"
        Me.lbl5YrPrice.Size = New System.Drawing.Size(104, 21)
        Me.lbl5YrPrice.TabIndex = 16
        Me.lbl5YrPrice.Tag = "00"
        Me.lbl5YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl20YrYield
        '
        Me.lbl20YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl20YrYield.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl20YrYield.Location = New System.Drawing.Point(129, 104)
        Me.lbl20YrYield.Name = "lbl20YrYield"
        Me.lbl20YrYield.Size = New System.Drawing.Size(104, 21)
        Me.lbl20YrYield.TabIndex = 19
        Me.lbl20YrYield.Tag = "21"
        Me.lbl20YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl20YrPrice
        '
        Me.lbl20YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl20YrPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl20YrPrice.Location = New System.Drawing.Point(17, 104)
        Me.lbl20YrPrice.Name = "lbl20YrPrice"
        Me.lbl20YrPrice.Size = New System.Drawing.Size(104, 21)
        Me.lbl20YrPrice.TabIndex = 15
        Me.lbl20YrPrice.Tag = "20"
        Me.lbl20YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl30YrYield
        '
        Me.lbl30YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl30YrYield.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl30YrYield.Location = New System.Drawing.Point(129, 128)
        Me.lbl30YrYield.Name = "lbl30YrYield"
        Me.lbl30YrYield.Size = New System.Drawing.Size(104, 21)
        Me.lbl30YrYield.TabIndex = 21
        Me.lbl30YrYield.Tag = "31"
        Me.lbl30YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl5YrYield
        '
        Me.lbl5YrYield.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl5YrYield.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl5YrYield.Location = New System.Drawing.Point(129, 56)
        Me.lbl5YrYield.Name = "lbl5YrYield"
        Me.lbl5YrYield.Size = New System.Drawing.Size(104, 21)
        Me.lbl5YrYield.TabIndex = 20
        Me.lbl5YrYield.Tag = "01"
        Me.lbl5YrYield.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl10YrPrice
        '
        Me.lbl10YrPrice.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl10YrPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl10YrPrice.Location = New System.Drawing.Point(17, 80)
        Me.lbl10YrPrice.Name = "lbl10YrPrice"
        Me.lbl10YrPrice.Size = New System.Drawing.Size(104, 21)
        Me.lbl10YrPrice.TabIndex = 14
        Me.lbl10YrPrice.Tag = "10"
        Me.lbl10YrPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'statusBar1
        '
        Me.statusBar1.Location = New System.Drawing.Point(0, 223)
        Me.statusBar1.Name = "statusBar1"
        Me.statusBar1.Size = New System.Drawing.Size(608, 22)
        Me.statusBar1.TabIndex = 20
        Me.statusBar1.Text = "statusBar1"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(608, 245)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.grpResults)
        Me.Controls.Add(Me.statusBar1)
        Me.Controls.Add(Me.grpOverrides)
        Me.Name = "Form1"
        Me.Text = "Static Overrides Request using API for .NET"
        Me.grpOverrides.ResumeLayout(False)
        Me.groupBox1.ResumeLayout(False)
        Me.grpResults.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private ftbl As FieldTable
    Private _securities() As String
    Private _fields() As Field
    Private reqOvr As RequestForStaticOverride
    Private colCount As Integer = 0
    Private rowCount As Integer = 0
    Private rowOfRequest As Integer = 0, colOfRequest As Integer = 0

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Directions on how to run sample available as tooltip on pictureBox question-mark.
        directionsToolTip.SetToolTip(Me.pictureBox1, _
         "Instructions: Enter an override value in one of the entry boxes and press the TAB key to see the results in the right-side frame.")

        'Subscribe for status events
        AddHandler MarketDataAdapter.StatusEvent, AddressOf MarketDataAdapter_StatusEvent

        ' Now, startup the MarketDataAdapter.
        ' This will cause several things to happen:
        ' - Bloomberg configuration information will be acquired from the 
        '   environment (Windows Registry, config files, the Bloomberg terminal, etc)
        ' - A connection to the local bbcomm will be established.
        ' - BBCOMM protocol handshaking will be initiated.
        ' - Adapter resources will be allocated.
        Try
            ' The ThreadSafeResponseInterceptor ensures that the status and reply
            ' event handlers are called on the user interface threads. 
            MarketDataAdapter.Startup(New ThreadSafeResponseInterceptor(Me))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End Try

        ' After MarketDataAdapter.Startup has completed successfully, 
        ' get a reference to the FieldTable (factory) object that has been 
        ' loaded (during) MarketDataAdapter.Startup.
        ftbl = MarketDataAdapter.FieldTable

        _fields = New Field() {ftbl("PX_ASK"), ftbl("YLD_CNV_ASK")}

        _securities = New String() _
             {"CT5 Govt", "CT10 Govt", "CT20 Govt", "CT30 Govt"}

        'Subscribe for reply events
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf MarketDataAdapter_ReplyEvent

        ' Attach Validating event handler to all textboxes in Overrides group
        AddHandler txt5YrPrice.Validating, AddressOf TabGetData
        AddHandler txt10YrPrice.Validating, AddressOf TabGetData
        AddHandler txt20YrPrice.Validating, AddressOf TabGetData
        AddHandler txt30YrPrice.Validating, AddressOf TabGetData
        AddHandler txt5YrYield.Validating, AddressOf TabGetData
        AddHandler txt10YrYield.Validating, AddressOf TabGetData
        AddHandler txt20YrYield.Validating, AddressOf TabGetData
        AddHandler txt30YrYield.Validating, AddressOf TabGetData

        MakeRequest()

        statusBar1.ShowPanels = False

    End Sub

    Private Sub MarketDataAdapter_ReplyEvent(ByVal reply As Reply)

        If (Not reply.ReplyError Is Nothing) Then
            statusBar1.Text = "Reply Error has occurred: " + _
                reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description
            Exit Sub
        End If

        ' Begin by searching for SecurityDataItem, then the FieldDataItem
        ' and finally the DataPoint.
        Try
            ' Loop through each security
            For Each sdi As SecurityDataItem In reply.GetSecurityDataItems()
                ' Loop through each field
                For Each fdi As FieldDataItem In sdi.FieldsData
                    ' Loop through each data point
                    For Each dp As DataPoint In fdi.DataPoints
                        If Not dp Is Nothing Then
                            If dp.IsError Then
                                statusBar1.Text = dp.ReplyError.DisplayName
                            Else
                                If rowOfRequest = -1 Then
                                    DisplayData(rowCount, colCount, dp.Value.ToString())
                                Else
                                    DisplayData(rowOfRequest, colCount, dp.Value.ToString())
                                End If

                                statusBar1.Text = "Data received successfully."
                            End If
                        End If
                    Next dp
                    colCount += 1 ' Update the column count
                Next fdi
                colCount = 0   'Reset the column count
                rowCount += 1  'Update the row count
            Next sdi

        Catch ex As Exception
            statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message
        End Try

    End Sub

    Private Sub MarketDataAdapter_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        statusBar1.Text = "Status: " + status.ToString() + "  Desc: " + description.ToString()

    End Sub

    Private Sub TabGetData(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)

        Dim t As TextBox = CType(sender, TextBox)

        If e.Cancel = False Then
            If t.Text <> "" Then
                MakeRequest(t)
            End If
        End If

    End Sub

    Private Sub MakeRequest()

        ' Issue new static request
        reqOvr = New RequestForStaticOverride

        ' Add field and security to RequestForStaticOverride object
        reqOvr.Fields.AddRange(_fields)
        reqOvr.Securities.AddRange(_securities)

        ' Set subscription mode
        reqOvr.SubscriptionMode = SubscriptionMode.ByRequest

        rowOfRequest = -1
        rowCount = 0  ' Reset the row count 
        statusBar1.Text = "Submitting initial request..."

        MarketDataAdapter.SendRequest(reqOvr)

    End Sub

    ' This overloaded method accepts the textbox object that contains the override for the request
    Private Sub MakeRequest(ByVal txtBox As TextBox)

        Dim strTag As String

        ' Issue new static request
        reqOvr = New RequestForStaticOverride

        strTag = txtBox.Tag.ToString()
        rowOfRequest = Convert.ToInt32(strTag.Substring(0, 1))
        colOfRequest = Convert.ToInt32(strTag.Substring(1, 1))

        ' Add field and security to RequestForStaticOverride object
        reqOvr.Fields.AddRange(_fields)
        reqOvr.Securities.Add(_securities(rowOfRequest))

        ' Set subscription mode
        reqOvr.SubscriptionMode = SubscriptionMode.BySecurity

        ' Add override fields
        ' By mnemonic
        Dim ofldval1 As OverrideFieldValue = New OverrideFieldValue(_fields(colOfRequest), txtBox.Text)
        reqOvr.OverrideFieldValues.Add(ofldval1)

        MarketDataAdapter.SendRequest(reqOvr)

        rowCount = 0  ' Reset the row count for subsequent requests
        statusBar1.Text = "Submitting override request..."

    End Sub

    Private Sub DisplayData(ByVal rowVal As Integer, ByVal colVal As Integer, ByVal dpVal As String)

        For Each cnt As Control In Me.grpResults.Controls
            If cnt.Tag <> Nothing Then
                If ((rowVal.ToString() + colVal.ToString()) = cnt.Tag.ToString()) Then
                    cnt.Text = dpVal
                End If
            End If
        Next cnt

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click

        For Each cnt As Control In Me.grpOverrides.Controls
            If (cnt.Tag <> Nothing) Then
                cnt.Text = ""
            End If
        Next cnt
        MakeRequest()

    End Sub

End Class

