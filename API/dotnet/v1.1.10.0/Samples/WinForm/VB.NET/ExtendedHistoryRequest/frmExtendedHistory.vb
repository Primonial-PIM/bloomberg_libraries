' *********************************************************
' * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
' * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
' * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
' * OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR    *
' * PURPOSE.											  *
' *********************************************************

Imports System.IO
Imports System.ComponentModel
Imports System.Text
Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class frmExtendedHistory
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region " Public Enum SECTION "

    Public Enum SECTION
        Security
        Field
        Data_Output
    End Enum

#End Region

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents groupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents label11 As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents label10 As System.Windows.Forms.Label
    Friend WithEvents label9 As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents btnSetDates As System.Windows.Forms.Button
    Friend WithEvents cbxDisplayAsYld As System.Windows.Forms.CheckBox
    Friend WithEvents cbxRevChron As System.Windows.Forms.CheckBox
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents cboDisplayNonTradDay As System.Windows.Forms.ComboBox
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cboNonTradDayVal As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriodicity As System.Windows.Forms.ComboBox
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents cboSubMode As System.Windows.Forms.ComboBox
    Friend WithEvents groupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents label8 As System.Windows.Forms.Label
    Friend WithEvents btnRemoveAllFlds As System.Windows.Forms.Button
    Friend WithEvents btnRemoveFld As System.Windows.Forms.Button
    Friend WithEvents btnAddField As System.Windows.Forms.Button
    Friend WithEvents txtField As System.Windows.Forms.TextBox
    Friend WithEvents listboxFields As System.Windows.Forms.ListBox
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents label7 As System.Windows.Forms.Label
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents btnRemoveAllSec As System.Windows.Forms.Button
    Friend WithEvents btnRemoveSec As System.Windows.Forms.Button
    Friend WithEvents btnImportSecurities As System.Windows.Forms.Button
    Friend WithEvents cboTickerType As System.Windows.Forms.ComboBox
    Friend WithEvents txtTicker As System.Windows.Forms.TextBox
    Friend WithEvents cboMrktSector As System.Windows.Forms.ComboBox
    Friend WithEvents listboxSecurities As System.Windows.Forms.ListBox
    Friend WithEvents btnAddSec As System.Windows.Forms.Button
    Friend WithEvents btnClearOutput As System.Windows.Forms.Button
    Friend WithEvents groupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDisplay As System.Windows.Forms.TextBox
    Friend WithEvents openFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents directionsToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents statusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents btnGetData As System.Windows.Forms.Button

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.groupBox3 = New System.Windows.Forms.GroupBox
        Me.label11 = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.label10 = New System.Windows.Forms.Label
        Me.label9 = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.btnSetDates = New System.Windows.Forms.Button
        Me.cbxDisplayAsYld = New System.Windows.Forms.CheckBox
        Me.cbxRevChron = New System.Windows.Forms.CheckBox
        Me.label3 = New System.Windows.Forms.Label
        Me.cboDisplayNonTradDay = New System.Windows.Forms.ComboBox
        Me.label2 = New System.Windows.Forms.Label
        Me.cboNonTradDayVal = New System.Windows.Forms.ComboBox
        Me.cboPeriodicity = New System.Windows.Forms.ComboBox
        Me.label4 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.cboSubMode = New System.Windows.Forms.ComboBox
        Me.groupBox2 = New System.Windows.Forms.GroupBox
        Me.label8 = New System.Windows.Forms.Label
        Me.btnRemoveAllFlds = New System.Windows.Forms.Button
        Me.btnRemoveFld = New System.Windows.Forms.Button
        Me.btnAddField = New System.Windows.Forms.Button
        Me.txtField = New System.Windows.Forms.TextBox
        Me.listboxFields = New System.Windows.Forms.ListBox
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.label7 = New System.Windows.Forms.Label
        Me.label6 = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.btnRemoveAllSec = New System.Windows.Forms.Button
        Me.btnRemoveSec = New System.Windows.Forms.Button
        Me.btnImportSecurities = New System.Windows.Forms.Button
        Me.cboTickerType = New System.Windows.Forms.ComboBox
        Me.txtTicker = New System.Windows.Forms.TextBox
        Me.cboMrktSector = New System.Windows.Forms.ComboBox
        Me.listboxSecurities = New System.Windows.Forms.ListBox
        Me.btnAddSec = New System.Windows.Forms.Button
        Me.btnClearOutput = New System.Windows.Forms.Button
        Me.groupBox4 = New System.Windows.Forms.GroupBox
        Me.txtDisplay = New System.Windows.Forms.TextBox
        Me.openFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.directionsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.statusBar1 = New System.Windows.Forms.StatusBar
        Me.btnGetData = New System.Windows.Forms.Button
        Me.groupBox3.SuspendLayout()
        Me.groupBox2.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        Me.groupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupBox3
        '
        Me.groupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.groupBox3.Controls.Add(Me.label11)
        Me.groupBox3.Controls.Add(Me.cboCurrency)
        Me.groupBox3.Controls.Add(Me.label10)
        Me.groupBox3.Controls.Add(Me.label9)
        Me.groupBox3.Controls.Add(Me.lblEndDate)
        Me.groupBox3.Controls.Add(Me.lblStartDate)
        Me.groupBox3.Controls.Add(Me.btnSetDates)
        Me.groupBox3.Controls.Add(Me.cbxDisplayAsYld)
        Me.groupBox3.Controls.Add(Me.cbxRevChron)
        Me.groupBox3.Controls.Add(Me.label3)
        Me.groupBox3.Controls.Add(Me.cboDisplayNonTradDay)
        Me.groupBox3.Controls.Add(Me.label2)
        Me.groupBox3.Controls.Add(Me.cboNonTradDayVal)
        Me.groupBox3.Controls.Add(Me.cboPeriodicity)
        Me.groupBox3.Controls.Add(Me.label4)
        Me.groupBox3.Controls.Add(Me.label1)
        Me.groupBox3.Controls.Add(Me.cboSubMode)
        Me.groupBox3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox3.Location = New System.Drawing.Point(13, 190)
        Me.groupBox3.Name = "groupBox3"
        Me.groupBox3.Size = New System.Drawing.Size(302, 271)
        Me.groupBox3.TabIndex = 43
        Me.groupBox3.TabStop = False
        Me.groupBox3.Text = "Properties"
        '
        'label11
        '
        Me.label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label11.Location = New System.Drawing.Point(12, 167)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(144, 16)
        Me.label11.TabIndex = 44
        Me.label11.Text = "Currency:"
        Me.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.BackColor = System.Drawing.SystemColors.Window
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.Location = New System.Drawing.Point(12, 185)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(142, 21)
        Me.cboCurrency.TabIndex = 43
        '
        'label10
        '
        Me.label10.BackColor = System.Drawing.SystemColors.Control
        Me.label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label10.ForeColor = System.Drawing.Color.Black
        Me.label10.Location = New System.Drawing.Point(14, 85)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(54, 20)
        Me.label10.TabIndex = 42
        Me.label10.Text = "EndDate: "
        Me.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label9
        '
        Me.label9.BackColor = System.Drawing.SystemColors.Control
        Me.label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label9.ForeColor = System.Drawing.Color.Black
        Me.label9.Location = New System.Drawing.Point(13, 58)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(55, 20)
        Me.label9.TabIndex = 41
        Me.label9.Text = "StartDate: "
        Me.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEndDate
        '
        Me.lblEndDate.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(221, Byte), CType(216, Byte))
        Me.lblEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblEndDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(69, 85)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(220, 20)
        Me.lblEndDate.TabIndex = 40
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(221, Byte), CType(216, Byte))
        Me.lblStartDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(69, 58)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(220, 20)
        Me.lblStartDate.TabIndex = 39
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSetDates
        '
        Me.btnSetDates.BackColor = System.Drawing.SystemColors.Control
        Me.btnSetDates.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetDates.Location = New System.Drawing.Point(12, 26)
        Me.btnSetDates.Name = "btnSetDates"
        Me.btnSetDates.Size = New System.Drawing.Size(88, 23)
        Me.btnSetDates.TabIndex = 13
        Me.btnSetDates.Text = "Set Dates..."
        '
        'cbxDisplayAsYld
        '
        Me.cbxDisplayAsYld.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDisplayAsYld.Location = New System.Drawing.Point(160, 167)
        Me.cbxDisplayAsYld.Name = "cbxDisplayAsYld"
        Me.cbxDisplayAsYld.Size = New System.Drawing.Size(107, 26)
        Me.cbxDisplayAsYld.TabIndex = 17
        Me.cbxDisplayAsYld.Text = "DisplayAsYield"
        '
        'cbxRevChron
        '
        Me.cbxRevChron.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRevChron.Location = New System.Drawing.Point(160, 186)
        Me.cbxRevChron.Name = "cbxRevChron"
        Me.cbxRevChron.Size = New System.Drawing.Size(135, 26)
        Me.cbxRevChron.TabIndex = 18
        Me.cbxRevChron.Text = "ReverseChronological"
        '
        'label3
        '
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(13, 219)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(130, 16)
        Me.label3.TabIndex = 33
        Me.label3.Text = "DisplayNonTradingDays:"
        '
        'cboDisplayNonTradDay
        '
        Me.cboDisplayNonTradDay.BackColor = System.Drawing.SystemColors.Window
        Me.cboDisplayNonTradDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplayNonTradDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisplayNonTradDay.Location = New System.Drawing.Point(12, 236)
        Me.cboDisplayNonTradDay.Name = "cboDisplayNonTradDay"
        Me.cboDisplayNonTradDay.Size = New System.Drawing.Size(140, 21)
        Me.cboDisplayNonTradDay.TabIndex = 19
        '
        'label2
        '
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(156, 218)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(118, 16)
        Me.label2.TabIndex = 31
        Me.label2.Text = "NonTradingDayValue:"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNonTradDayVal
        '
        Me.cboNonTradDayVal.BackColor = System.Drawing.SystemColors.Window
        Me.cboNonTradDayVal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNonTradDayVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNonTradDayVal.Location = New System.Drawing.Point(158, 236)
        Me.cboNonTradDayVal.Name = "cboNonTradDayVal"
        Me.cboNonTradDayVal.Size = New System.Drawing.Size(135, 21)
        Me.cboNonTradDayVal.TabIndex = 16
        '
        'cboPeriodicity
        '
        Me.cboPeriodicity.BackColor = System.Drawing.SystemColors.Window
        Me.cboPeriodicity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodicity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriodicity.Location = New System.Drawing.Point(160, 135)
        Me.cboPeriodicity.Name = "cboPeriodicity"
        Me.cboPeriodicity.Size = New System.Drawing.Size(132, 21)
        Me.cboPeriodicity.TabIndex = 15
        '
        'label4
        '
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.Location = New System.Drawing.Point(156, 118)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(64, 16)
        Me.label4.TabIndex = 28
        Me.label4.Text = "Periodicity:"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label1
        '
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(12, 117)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(144, 16)
        Me.label1.TabIndex = 27
        Me.label1.Text = "Subscription Mode:"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSubMode
        '
        Me.cboSubMode.BackColor = System.Drawing.SystemColors.Window
        Me.cboSubMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSubMode.Location = New System.Drawing.Point(12, 134)
        Me.cboSubMode.Name = "cboSubMode"
        Me.cboSubMode.Size = New System.Drawing.Size(142, 21)
        Me.cboSubMode.TabIndex = 14
        '
        'groupBox2
        '
        Me.groupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox2.Controls.Add(Me.label8)
        Me.groupBox2.Controls.Add(Me.btnRemoveAllFlds)
        Me.groupBox2.Controls.Add(Me.btnRemoveFld)
        Me.groupBox2.Controls.Add(Me.btnAddField)
        Me.groupBox2.Controls.Add(Me.txtField)
        Me.groupBox2.Controls.Add(Me.listboxFields)
        Me.groupBox2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox2.Location = New System.Drawing.Point(411, 7)
        Me.groupBox2.Name = "groupBox2"
        Me.groupBox2.Size = New System.Drawing.Size(241, 167)
        Me.groupBox2.TabIndex = 40
        Me.groupBox2.TabStop = False
        Me.groupBox2.Text = "Fields"
        '
        'label8
        '
        Me.label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.Location = New System.Drawing.Point(8, 21)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(88, 16)
        Me.label8.TabIndex = 13
        Me.label8.Text = "Field Mnemonic:"
        Me.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveAllFlds
        '
        Me.btnRemoveAllFlds.BackColor = System.Drawing.SystemColors.Control
        Me.btnRemoveAllFlds.Enabled = False
        Me.btnRemoveAllFlds.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveAllFlds.Location = New System.Drawing.Point(176, 97)
        Me.btnRemoveAllFlds.Name = "btnRemoveAllFlds"
        Me.btnRemoveAllFlds.Size = New System.Drawing.Size(32, 23)
        Me.btnRemoveAllFlds.TabIndex = 12
        Me.btnRemoveAllFlds.Text = "<<"
        Me.directionsToolTip.SetToolTip(Me.btnRemoveAllFlds, "Remove all fields")
        '
        'btnRemoveFld
        '
        Me.btnRemoveFld.BackColor = System.Drawing.SystemColors.Control
        Me.btnRemoveFld.Enabled = False
        Me.btnRemoveFld.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveFld.Location = New System.Drawing.Point(176, 69)
        Me.btnRemoveFld.Name = "btnRemoveFld"
        Me.btnRemoveFld.Size = New System.Drawing.Size(32, 23)
        Me.btnRemoveFld.TabIndex = 11
        Me.btnRemoveFld.Text = "<"
        Me.directionsToolTip.SetToolTip(Me.btnRemoveFld, "Remove Selected Field")
        '
        'btnAddField
        '
        Me.btnAddField.BackColor = System.Drawing.SystemColors.Control
        Me.btnAddField.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddField.Location = New System.Drawing.Point(176, 39)
        Me.btnAddField.Name = "btnAddField"
        Me.btnAddField.Size = New System.Drawing.Size(56, 22)
        Me.btnAddField.TabIndex = 9
        Me.btnAddField.Text = "Add"
        '
        'txtField
        '
        Me.txtField.BackColor = System.Drawing.SystemColors.Window
        Me.txtField.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtField.Location = New System.Drawing.Point(7, 39)
        Me.txtField.Name = "txtField"
        Me.txtField.Size = New System.Drawing.Size(161, 20)
        Me.txtField.TabIndex = 8
        Me.txtField.Text = ""
        '
        'listboxFields
        '
        Me.listboxFields.BackColor = System.Drawing.SystemColors.Window
        Me.listboxFields.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listboxFields.Location = New System.Drawing.Point(7, 64)
        Me.listboxFields.Name = "listboxFields"
        Me.listboxFields.Size = New System.Drawing.Size(161, 95)
        Me.listboxFields.TabIndex = 10
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.label7)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.label5)
        Me.groupBox1.Controls.Add(Me.btnRemoveAllSec)
        Me.groupBox1.Controls.Add(Me.btnRemoveSec)
        Me.groupBox1.Controls.Add(Me.btnImportSecurities)
        Me.groupBox1.Controls.Add(Me.cboTickerType)
        Me.groupBox1.Controls.Add(Me.txtTicker)
        Me.groupBox1.Controls.Add(Me.cboMrktSector)
        Me.groupBox1.Controls.Add(Me.listboxSecurities)
        Me.groupBox1.Controls.Add(Me.btnAddSec)
        Me.groupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox1.Location = New System.Drawing.Point(16, 7)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(384, 167)
        Me.groupBox1.TabIndex = 39
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Securities "
        '
        'label7
        '
        Me.label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.Location = New System.Drawing.Point(208, 21)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(88, 16)
        Me.label7.TabIndex = 10
        Me.label7.Text = "Ticker Type:"
        Me.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label6
        '
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.Location = New System.Drawing.Point(128, 21)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(72, 16)
        Me.label6.TabIndex = 9
        Me.label6.Text = "Sector:"
        Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label5
        '
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.Location = New System.Drawing.Point(8, 21)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(88, 16)
        Me.label5.TabIndex = 8
        Me.label5.Text = "Security Name:"
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveAllSec
        '
        Me.btnRemoveAllSec.BackColor = System.Drawing.SystemColors.Control
        Me.btnRemoveAllSec.Enabled = False
        Me.btnRemoveAllSec.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveAllSec.Location = New System.Drawing.Point(296, 97)
        Me.btnRemoveAllSec.Name = "btnRemoveAllSec"
        Me.btnRemoveAllSec.Size = New System.Drawing.Size(32, 23)
        Me.btnRemoveAllSec.TabIndex = 6
        Me.btnRemoveAllSec.Text = "<<"
        Me.directionsToolTip.SetToolTip(Me.btnRemoveAllSec, "Remove all securities")
        '
        'btnRemoveSec
        '
        Me.btnRemoveSec.BackColor = System.Drawing.SystemColors.Control
        Me.btnRemoveSec.Enabled = False
        Me.btnRemoveSec.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveSec.Location = New System.Drawing.Point(296, 69)
        Me.btnRemoveSec.Name = "btnRemoveSec"
        Me.btnRemoveSec.Size = New System.Drawing.Size(32, 23)
        Me.btnRemoveSec.TabIndex = 5
        Me.btnRemoveSec.Text = "<"
        Me.directionsToolTip.SetToolTip(Me.btnRemoveSec, "Remove Selected Security")
        '
        'btnImportSecurities
        '
        Me.btnImportSecurities.BackColor = System.Drawing.SystemColors.Control
        Me.btnImportSecurities.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImportSecurities.Location = New System.Drawing.Point(296, 135)
        Me.btnImportSecurities.Name = "btnImportSecurities"
        Me.btnImportSecurities.Size = New System.Drawing.Size(80, 23)
        Me.btnImportSecurities.TabIndex = 7
        Me.btnImportSecurities.Text = "Import..."
        '
        'cboTickerType
        '
        Me.cboTickerType.BackColor = System.Drawing.SystemColors.Window
        Me.cboTickerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTickerType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTickerType.Location = New System.Drawing.Point(206, 39)
        Me.cboTickerType.Name = "cboTickerType"
        Me.cboTickerType.Size = New System.Drawing.Size(106, 21)
        Me.cboTickerType.TabIndex = 2
        '
        'txtTicker
        '
        Me.txtTicker.BackColor = System.Drawing.SystemColors.Window
        Me.txtTicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTicker.Location = New System.Drawing.Point(8, 39)
        Me.txtTicker.Name = "txtTicker"
        Me.txtTicker.Size = New System.Drawing.Size(112, 20)
        Me.txtTicker.TabIndex = 0
        Me.txtTicker.Text = ""
        Me.directionsToolTip.SetToolTip(Me.txtTicker, "Enter ticker and exchange code (optional)")
        '
        'cboMrktSector
        '
        Me.cboMrktSector.BackColor = System.Drawing.SystemColors.Window
        Me.cboMrktSector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMrktSector.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMrktSector.Location = New System.Drawing.Point(125, 39)
        Me.cboMrktSector.Name = "cboMrktSector"
        Me.cboMrktSector.Size = New System.Drawing.Size(78, 21)
        Me.cboMrktSector.TabIndex = 1
        '
        'listboxSecurities
        '
        Me.listboxSecurities.BackColor = System.Drawing.SystemColors.Window
        Me.listboxSecurities.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listboxSecurities.Location = New System.Drawing.Point(8, 64)
        Me.listboxSecurities.Name = "listboxSecurities"
        Me.listboxSecurities.Size = New System.Drawing.Size(280, 95)
        Me.listboxSecurities.TabIndex = 4
        '
        'btnAddSec
        '
        Me.btnAddSec.BackColor = System.Drawing.SystemColors.Control
        Me.btnAddSec.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddSec.Location = New System.Drawing.Point(320, 39)
        Me.btnAddSec.Name = "btnAddSec"
        Me.btnAddSec.Size = New System.Drawing.Size(56, 22)
        Me.btnAddSec.TabIndex = 3
        Me.btnAddSec.Text = "Add"
        '
        'btnClearOutput
        '
        Me.btnClearOutput.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClearOutput.Location = New System.Drawing.Point(322, 434)
        Me.btnClearOutput.Name = "btnClearOutput"
        Me.btnClearOutput.Size = New System.Drawing.Size(78, 27)
        Me.btnClearOutput.TabIndex = 41
        Me.btnClearOutput.Text = "Clear Output"
        '
        'groupBox4
        '
        Me.groupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox4.Controls.Add(Me.txtDisplay)
        Me.groupBox4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox4.Location = New System.Drawing.Point(322, 190)
        Me.groupBox4.Name = "groupBox4"
        Me.groupBox4.Size = New System.Drawing.Size(331, 240)
        Me.groupBox4.TabIndex = 44
        Me.groupBox4.TabStop = False
        Me.groupBox4.Text = "Data Output"
        '
        'txtDisplay
        '
        Me.txtDisplay.AcceptsReturn = True
        Me.txtDisplay.BackColor = System.Drawing.SystemColors.Window
        Me.txtDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisplay.Location = New System.Drawing.Point(10, 28)
        Me.txtDisplay.Multiline = True
        Me.txtDisplay.Name = "txtDisplay"
        Me.txtDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDisplay.Size = New System.Drawing.Size(314, 204)
        Me.txtDisplay.TabIndex = 37
        Me.txtDisplay.Text = ""
        '
        'directionsToolTip
        '
        Me.directionsToolTip.AutoPopDelay = 5000
        Me.directionsToolTip.InitialDelay = 100
        Me.directionsToolTip.ReshowDelay = 100
        '
        'statusBar1
        '
        Me.statusBar1.Location = New System.Drawing.Point(0, 478)
        Me.statusBar1.Name = "statusBar1"
        Me.statusBar1.Size = New System.Drawing.Size(668, 16)
        Me.statusBar1.TabIndex = 37
        '
        'btnGetData
        '
        Me.btnGetData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGetData.BackColor = System.Drawing.SystemColors.Control
        Me.btnGetData.Location = New System.Drawing.Point(580, 434)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.Size = New System.Drawing.Size(72, 27)
        Me.btnGetData.TabIndex = 42
        Me.btnGetData.Text = "Get Data"
        '
        'frmExtendedHistory
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(668, 494)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.btnClearOutput)
        Me.Controls.Add(Me.groupBox4)
        Me.Controls.Add(Me.statusBar1)
        Me.Controls.Add(Me.btnGetData)
        Me.Controls.Add(Me.groupBox3)
        Me.Controls.Add(Me.groupBox2)
        Me.Name = "frmExtendedHistory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Extended History using API for .NET"
        Me.groupBox3.ResumeLayout(False)
        Me.groupBox2.ResumeLayout(False)
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _selSecurity As Security
    Private _selField As Field
    Private request As RequestForHistory = Nothing
    Private importSecurityDialogDir As String = ""
    Private _errCounter As Long
    Private ftbl As FieldTable = Nothing
    Private strDisplay As StringBuilder = Nothing
    Private SdOffset As DateOffset = Nothing
    Private SdAnchor As DateAnchor = Nothing
    Private SdExtDate As ExtendedDate = Nothing
    Private EdExtDate As ExtendedDate = Nothing
    Private arrIsoCodes As String() = Nothing

    Private Sub frmExtendedHistory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' Subscribe for any status event messages
        AddHandler MarketDataAdapter.StatusEvent, AddressOf OnHistory_StatusEvent

        ' Now, startup the MarketDataAdapter.
        ' This will cause several things to happen:
        '   - Bloomberg configuration information will be acquired from the 
        '	   environment (Windows Registry, config files, the Bloomberg terminal, etc)
        '	 - A connection to the local bbcomm will be established.
        '	 - BBCOMM protocol handshaking will be initiated.
        '	 - Adapter resources will be allocated.

        Try
            ' The ThreadSafeResponseInterceptor ensures that the status and reply
            ' event handlers are called on the user interface threads. 
            MarketDataAdapter.Startup(New ThreadSafeResponseInterceptor(Me))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Close()
        End Try

        ' Subscribe for any reply event messages
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf OnHistory_ReplyEvent

        ' Get a reference to the FieldTable factory object that has already
        ' been loaded during the MarketDataAdapter.Startup method
        ftbl = MarketDataAdapter.FieldTable

        ' Populate Market Sector dropdown list
        Dim arrMarketSectors() As String = [Enum].GetNames(GetType(MARKETSECTOR))
        Array.Sort(arrMarketSectors)
        cboMrktSector.Items.AddRange(arrMarketSectors)
        cboMrktSector.SelectedIndex = 5 ' Set to "Equity"

        ' Populate Ticker Type dropdown list
        Dim arrTickerTypes() As String = [Enum].GetNames(GetType(QUALIFIER))
        Array.Sort(arrTickerTypes)
        cboTickerType.Items.AddRange(arrTickerTypes)
        cboTickerType.SelectedIndex = 35 ' Set to "Ticker-Exchange"

        ' Populate SubscriptionMode combobox with enumerator values
        cboSubMode.DataSource = [Enum].GetValues(GetType(SubscriptionMode))
        cboSubMode.SelectedIndex = 0 ' Set to "ByField"

        ' Populate Periodicity combobox with enumerator values
        cboPeriodicity.DataSource = [Enum].GetValues(GetType(Periodicity))
        cboPeriodicity.SelectedIndex = 0 ' Set to "ActualDaily"

        ' Populate Currency combobox with ISO Codes and select USD as default
        arrIsoCodes = CurrencyTable.Instance.GetISOCodes()
        cboCurrency.DataSource = arrIsoCodes
        cboCurrency.SelectedItem = "USD"

        ' Populate DisplayNonTradingDays combobox with enumerator values
        cboDisplayNonTradDay.DataSource = [Enum].GetValues(GetType(DisplayNonTradingDays))
        cboDisplayNonTradDay.SelectedIndex = 2 ' Set to "AllCalendar"

        ' Populate NonTradingDayValue combobox with enumerator values
        cboNonTradDayVal.DataSource = [Enum].GetValues(GetType(NonTradingDayValue))
        cboNonTradDayVal.SelectedIndex = 1 ' Set to "PreviousDay"

        ' Set default date ranges for Start date (End date is today by default).
        SdOffset = New DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day)
        SdAnchor = Nothing ' Set to TODAY in relative date.
        SdExtDate = New ExtendedDate(SdAnchor, SdOffset)
        EdExtDate = New ExtendedDate

        ' Display values in Start/End date labels
        lblStartDate.Text = "-5AD"
        lblEndDate.Text = "Today"

        importSecurityDialogDir = "c:\\"

        statusBar1.ShowPanels = False

    End Sub

    Private Sub OnHistory_ReplyEvent(ByVal reply As Reply)

        If (Not reply.ReplyError Is Nothing) Then

            statusBar1.Text = "Reply Error has occurred: " + _
                reply.ReplyError.DisplayName + " - " + reply.ReplyError.Description
            Return

        End If

        _errCounter = 0

        strDisplay = New StringBuilder
        strDisplay.Append(txtDisplay.Text.ToString() + vbCrLf)

        Try
            ' Loop through each security
            For Each sdi As SecurityDataItem In reply.GetSecurityDataItems()

                strDisplay.Append(sdi.Security.FullName.ToString() + ":" + vbCrLf)

                ' Loop through each field
                For Each fdi As FieldDataItem In sdi.FieldsData
                    strDisplay.Append(fdi.Field.Mnemonic.ToString() + ":" + vbCrLf)

                    ' Loop through each data point
                    For Each dp As DataPoint In fdi.DataPoints
                        If Not dp Is Nothing Then
                            If (dp.IsError) Then
                                strDisplay.Append(dp.ReplyError.DisplayName + vbCrLf)
                                _errCounter += 1
                            Else
                                strDisplay.Append(dp.Time.ToShortDateString().ToString() + " - " + dp.Value.ToString() + vbCrLf)
                                statusBar1.Text = "Receiving data..."
                            End If
                        End If
                    Next
                Next

                strDisplay.Append(vbCrLf)
                txtDisplay.Text = strDisplay.ToString()
                statusBar1.Text = " Finished receiving data with " + _errCounter.ToString() + " error(s)."

            Next

        Catch ex As Exception
            statusBar1.Text = "Error has occurred in ReplyEvent handler: " + ex.Message

        End Try

    End Sub

    Private Sub OnHistory_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        statusBar1.Text = "Status: " + status.ToString() + "  Desc: " + description.ToString()

    End Sub

    Private Sub SetGetDataControlAsAcceptButton(ByVal sender As Object, ByVal e As System.EventArgs)

        ' Set Field Add button to accept Enter key
        Me.AcceptButton = Me.btnGetData

    End Sub

    Private Sub btnClearOutput_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearOutput.Click

        ' Clear output textbox
        txtDisplay.Clear()

    End Sub

    Private Sub Form1_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        MarketDataAdapter.Shutdown()

    End Sub

    Private Sub ToggleRemoveButtons(ByVal enableButtons As Boolean, ByVal sect As SECTION)

        Select Case (sect)
            Case SECTION.Field
                btnRemoveFld.Enabled = enableButtons
                btnRemoveAllFlds.Enabled = enableButtons

            Case SECTION.Security
                btnRemoveSec.Enabled = enableButtons
                btnRemoveAllSec.Enabled = enableButtons
        End Select

    End Sub

    Private Sub listboxFields_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles listboxFields.MouseHover

        directionsToolTip.SetToolTip(listboxFields, "There are currently " + _
            listboxFields.Items.Count.ToString() + " fields in this list.")

    End Sub

    Private Sub txtField_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtField.Enter

        ' Set Field Add button to accept Enter key
        Me.AcceptButton = Me.btnAddField

    End Sub

    Private Sub txtTicker_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTicker.Enter

        ' Set Security Add button to accept Enter key
        Me.AcceptButton = Me.btnAddSec

    End Sub

    Private Sub btnGetData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetData.Click

        ' Clear output textbox
        txtDisplay.Clear()

        ' Check that at least one security has been selected
        If (listboxSecurities.Items.Count < 1) Then
            MessageBox.Show(Me, "You must first add at least one security to the request before sending", "Missing Information")
            txtTicker.Select()
            Return
        End If

        ' Check that at least one field has been selected
        If (listboxFields.Items.Count < 1) Then
            MessageBox.Show(Me, "You must first add at least one field to the request before sending", "Missing Information")
            txtField.Select()
            Return
        End If

        ' Issue historical request
        request = New RequestForHistory

        ' Add securities to RequestForHistory object
        For Each s As String In listboxSecurities.Items
            request.Securities.Add(s)
        Next

        ' Add fields to RequestForHistory object
        For Each f As Field In listboxFields.Items
            request.Fields.Add(f)
        Next

        ' Set RequestForHistory parameters
        'request.Currencies.Add(new Currency("USD"))
        request.Periodicity = CType(cboPeriodicity.SelectedValue, Periodicity)
        request.SubscriptionMode = CType(cboSubMode.SelectedValue, SubscriptionMode)
        request.DisplayNonTradingDays = CType(cboDisplayNonTradDay.SelectedValue, DisplayNonTradingDays)
        request.NonTradingDayValue = CType(cboNonTradDayVal.SelectedValue, NonTradingDayValue)
        request.ReverseChronological = cbxRevChron.Checked
        request.DisplayAsYield = cbxDisplayAsYld.Checked
        request.Currencies.Add(arrIsoCodes(cboCurrency.SelectedIndex))

        Dim offset As DateOffset = New DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day)
        request.StartDate = SdExtDate
        request.EndDate = EdExtDate

        statusBar1.Text = "Submitting request(s)..."

        MarketDataAdapter.SendRequest(request)

    End Sub

    Private Sub btnRemoveAllFlds_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveAllFlds.Click

        Dim dr As DialogResult = MessageBox.Show(Me, "Are you sure you want to delete all of the fields in this list?", _
            "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If (dr = DialogResult.Yes) Then
            listboxFields.Items.Clear()
            ToggleRemoveButtons(False, SECTION.Field)
        End If

    End Sub

    Private Sub btnRemoveFld_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveFld.Click

        listboxFields.Items.RemoveAt(listboxFields.SelectedIndex)

        If (listboxFields.Items.Count < 1) Then
            ToggleRemoveButtons(False, SECTION.Field)
        Else
            listboxFields.SetSelected(0, True)
        End If

    End Sub

    Private Sub btnAddField_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddField.Click

        Try
            If (Not txtField.Text = String.Empty) Then

                ' An exception will be thrown if field is not found in the Field Table
                _selField = ftbl(txtField.Text.ToUpper())

                listboxFields.Items.Add(_selField)
                listboxFields.SetSelected(listboxFields.Items.Count - 1, True)
                ToggleRemoveButtons(True, SECTION.Field)
                txtField.SelectAll()

            End If

        Catch
            MessageBox.Show(txtField.Text.ToUpper() + " is an invalid field. Please try again.", "Field Not Found")
            txtField.SelectAll()
        End Try

    End Sub

    Private Sub btnSetDates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetDates.Click

        Dim frmDates As frmSetDates = New frmSetDates(SdExtDate, EdExtDate)
        frmDates.ShowDialog()

        If (frmDates.IsSaved()) Then
            lblStartDate.Text = frmDates.GetDisplayedDate(True)
            lblEndDate.Text = frmDates.GetDisplayedDate(False)
            SdExtDate = frmDates.GetExtendedDate(True)
            EdExtDate = frmDates.GetExtendedDate(False)
        End If

        frmDates = Nothing

    End Sub

    Private Sub btnImportSecurities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportSecurities.Click

        Dim myStream As Stream
        Dim isFileValid As Boolean = False

        Dim openFileDialog1 As OpenFileDialog = New OpenFileDialog
        openFileDialog1.Title = "Import List of Securities"
        openFileDialog1.InitialDirectory = importSecurityDialogDir
        openFileDialog1.Filter = "txt files (*.txt)|*.txt"

        If (openFileDialog1.ShowDialog() = DialogResult.OK) Then

            importSecurityDialogDir = openFileDialog1.FileName
            myStream = openFileDialog1.OpenFile()

            If (Not myStream Is Nothing) Then

                Dim File As StreamReader = New StreamReader(openFileDialog1.FileName)
                Dim oneLine As String = ""
                oneLine = File.ReadLine()

                While (Not oneLine = "" And Not oneLine = Nothing)
                    listboxSecurities.Items.Add(oneLine)
                    oneLine = File.ReadLine()
                    isFileValid = True
                End While

                File.Close()

            End If

            If (Not isFileValid) Then
                MessageBox.Show("Make sure you have one security per line with no empty lines", "File Import Failure")
            Else
                listboxSecurities.SetSelected(0, True)
                ToggleRemoveButtons(True, SECTION.Security)
            End If
        End If

    End Sub

    Private Sub btnRemoveSec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveSec.Click

        listboxSecurities.Items.RemoveAt(listboxSecurities.SelectedIndex)

        If (listboxSecurities.Items.Count < 1) Then
            ToggleRemoveButtons(False, SECTION.Security)
        Else
            listboxSecurities.SetSelected(0, True)
        End If

    End Sub

    Private Sub btnAddSec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSec.Click

        If (Not txtTicker.Text = String.Empty) Then

            ' Create Security object based on selected security 
            _selSecurity = New Security(txtTicker.Text.ToUpper() + " " + cboMrktSector.SelectedItem.ToString() + " " + _
                cboTickerType.SelectedItem.ToString())

            If (_selSecurity.Qualifier.CompareTo(QUALIFIER.TICKERX) = 0) Then ' If "Tickerx" is selected (default), assign empty string
                listboxSecurities.Items.Add(_selSecurity.Ticker + " " + _selSecurity.MarketSector.ToString())
            Else
                listboxSecurities.Items.Add(_selSecurity.FullName)
            End If

            listboxSecurities.SetSelected(listboxSecurities.Items.Count - 1, True)
            ToggleRemoveButtons(True, SECTION.Security)
            txtTicker.SelectAll()

        End If

    End Sub

    Private Sub btnRemoveAllSec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveAllSec.Click

        Dim dr As DialogResult = MessageBox.Show(Me, "Are you sure you want to delete all of the securities in this list?", _
            "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If (dr = DialogResult.Yes) Then
            listboxSecurities.Items.Clear()
            ToggleRemoveButtons(False, SECTION.Security)
        End If

    End Sub

    Private Sub listboxSecurities_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles listboxSecurities.MouseHover

        directionsToolTip.SetToolTip(listboxSecurities, "There are currently " + _
            listboxSecurities.Items.Count.ToString() + " securities in this list.")

    End Sub

End Class

