' *********************************************************
' * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
' * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
' * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
' * OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR    *
' * PURPOSE.											  *
' *********************************************************

Imports System.Drawing
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.Text
Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class frmSetDates
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    Public Sub New(ByVal sd As ExtendedDate, ByVal ed As ExtendedDate)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        SdExtendedDate = New ExtendedDate(sd.Anchor, sd.Offset)
        EdExtendedDate = New ExtendedDate(ed.Anchor, ed.Offset)

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents lblDisplayED As System.Windows.Forms.Label
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tabControlED As System.Windows.Forms.TabControl
    Friend WithEvents tabFixedED As System.Windows.Forms.TabPage
    Friend WithEvents spinFixedFisCalYearED As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboFixedSpecDateSD As System.Windows.Forms.ComboBox
    Friend WithEvents dtFixedSpecDateED As System.Windows.Forms.DateTimePicker
    Friend WithEvents optFixedFiscCalED As System.Windows.Forms.RadioButton
    Friend WithEvents optFixedSpecDateED As System.Windows.Forms.RadioButton
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents cbxFixedSpecDateTdyED As System.Windows.Forms.CheckBox
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents label7 As System.Windows.Forms.Label
    Friend WithEvents cboFixedSpecDateED As System.Windows.Forms.ComboBox
    Friend WithEvents cboFixedFisCalTypeED As System.Windows.Forms.ComboBox
    Friend WithEvents cboFixedFisCalQtySD As System.Windows.Forms.ComboBox
    Friend WithEvents cboFixedFisCalQtyED As System.Windows.Forms.ComboBox
    Friend WithEvents cboFixedFisCalPeriodED As System.Windows.Forms.ComboBox
    Friend WithEvents tabRelativeED As System.Windows.Forms.TabPage
    Friend WithEvents lblStartDateTextED As System.Windows.Forms.Label
    Friend WithEvents lblTodayTextED As System.Windows.Forms.Label
    Friend WithEvents dateCustomFiscalDateED As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboMovePeriodED As System.Windows.Forms.ComboBox
    Friend WithEvents cboMoveTypeED As System.Windows.Forms.ComboBox
    Friend WithEvents spinMoveQtyED As System.Windows.Forms.NumericUpDown
    Friend WithEvents spinCustomFiscalYearED As System.Windows.Forms.NumericUpDown
    Friend WithEvents chboxCustomFiscalED As System.Windows.Forms.CheckBox
    Friend WithEvents cboCustomFiscalQtyED As System.Windows.Forms.ComboBox
    Friend WithEvents optAsOfCustomED As System.Windows.Forms.RadioButton
    Friend WithEvents optAsOfStartDateED As System.Windows.Forms.RadioButton
    Friend WithEvents optAsOfTodayED As System.Windows.Forms.RadioButton
    Friend WithEvents label9 As System.Windows.Forms.Label
    Friend WithEvents cboCustomFiscalTypeED As System.Windows.Forms.ComboBox
    Friend WithEvents label13 As System.Windows.Forms.Label
    Friend WithEvents cboCustomFiscalPeriodED As System.Windows.Forms.ComboBox
    Friend WithEvents cboMoveDirectionED As System.Windows.Forms.ComboBox
    Friend WithEvents lblTodayLineED As System.Windows.Forms.Label
    Friend WithEvents lblStartDateLineED As System.Windows.Forms.Label
    Friend WithEvents lblCustomLineED As System.Windows.Forms.Label
    Friend WithEvents lblAsOfBoxED As System.Windows.Forms.Label
    Friend WithEvents lblDisplaySD As System.Windows.Forms.Label
    Friend WithEvents groupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tabControlSD As System.Windows.Forms.TabControl
    Friend WithEvents tabFixedSD As System.Windows.Forms.TabPage
    Friend WithEvents spinFixedFisCalYearSD As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtFixedSpecDateSD As System.Windows.Forms.DateTimePicker
    Friend WithEvents optFixedFiscCalSD As System.Windows.Forms.RadioButton
    Friend WithEvents optFixedSpecDateSD As System.Windows.Forms.RadioButton
    Friend WithEvents label10 As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents cbxFixedSpecDateTdySD As System.Windows.Forms.CheckBox
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents cboFixedFisCalPeriodSD As System.Windows.Forms.ComboBox
    Friend WithEvents cboFixedFisCalTypeSD As System.Windows.Forms.ComboBox
    Friend WithEvents tabRelativeSD As System.Windows.Forms.TabPage
    Friend WithEvents lblEndDateTextSD As System.Windows.Forms.Label
    Friend WithEvents lblTodayTextSD As System.Windows.Forms.Label
    Friend WithEvents dateCustomFiscalDateSD As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboMovePeriodSD As System.Windows.Forms.ComboBox
    Friend WithEvents cboMoveTypeSD As System.Windows.Forms.ComboBox
    Friend WithEvents spinMoveQtySD As System.Windows.Forms.NumericUpDown
    Friend WithEvents spinCustomFiscalYearSD As System.Windows.Forms.NumericUpDown
    Friend WithEvents chboxCustomFiscalSD As System.Windows.Forms.CheckBox
    Friend WithEvents cboCustomFiscalQtySD As System.Windows.Forms.ComboBox
    Friend WithEvents optAsOfCustomSD As System.Windows.Forms.RadioButton
    Friend WithEvents optAsOfEndDateSD As System.Windows.Forms.RadioButton
    Friend WithEvents optAsOfTodaySD As System.Windows.Forms.RadioButton
    Friend WithEvents label11 As System.Windows.Forms.Label
    Friend WithEvents cboCustomFiscalTypeSD As System.Windows.Forms.ComboBox
    Friend WithEvents label12 As System.Windows.Forms.Label
    Friend WithEvents cboCustomFiscalPeriodSD As System.Windows.Forms.ComboBox
    Friend WithEvents cboMoveDirectionSD As System.Windows.Forms.ComboBox
    Friend WithEvents lblTodayLineSD As System.Windows.Forms.Label
    Friend WithEvents lblEndDateLineSD As System.Windows.Forms.Label
    Friend WithEvents lblCustomLineSD As System.Windows.Forms.Label
    Friend WithEvents lblAsOfBoxSD As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.lblDisplayED = New System.Windows.Forms.Label
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.tabControlED = New System.Windows.Forms.TabControl
        Me.tabFixedED = New System.Windows.Forms.TabPage
        Me.spinFixedFisCalYearED = New System.Windows.Forms.NumericUpDown
        Me.cboFixedFisCalQtyED = New System.Windows.Forms.ComboBox
        Me.dtFixedSpecDateED = New System.Windows.Forms.DateTimePicker
        Me.optFixedFiscCalED = New System.Windows.Forms.RadioButton
        Me.optFixedSpecDateED = New System.Windows.Forms.RadioButton
        Me.label4 = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.cbxFixedSpecDateTdyED = New System.Windows.Forms.CheckBox
        Me.label6 = New System.Windows.Forms.Label
        Me.label7 = New System.Windows.Forms.Label
        Me.cboFixedFisCalPeriodED = New System.Windows.Forms.ComboBox
        Me.cboFixedFisCalTypeED = New System.Windows.Forms.ComboBox
        Me.tabRelativeED = New System.Windows.Forms.TabPage
        Me.lblStartDateTextED = New System.Windows.Forms.Label
        Me.lblTodayTextED = New System.Windows.Forms.Label
        Me.dateCustomFiscalDateED = New System.Windows.Forms.DateTimePicker
        Me.cboMovePeriodED = New System.Windows.Forms.ComboBox
        Me.cboMoveTypeED = New System.Windows.Forms.ComboBox
        Me.spinMoveQtyED = New System.Windows.Forms.NumericUpDown
        Me.spinCustomFiscalYearED = New System.Windows.Forms.NumericUpDown
        Me.chboxCustomFiscalED = New System.Windows.Forms.CheckBox
        Me.cboCustomFiscalQtyED = New System.Windows.Forms.ComboBox
        Me.optAsOfCustomED = New System.Windows.Forms.RadioButton
        Me.optAsOfStartDateED = New System.Windows.Forms.RadioButton
        Me.optAsOfTodayED = New System.Windows.Forms.RadioButton
        Me.label9 = New System.Windows.Forms.Label
        Me.cboCustomFiscalTypeED = New System.Windows.Forms.ComboBox
        Me.label13 = New System.Windows.Forms.Label
        Me.cboCustomFiscalPeriodED = New System.Windows.Forms.ComboBox
        Me.cboMoveDirectionED = New System.Windows.Forms.ComboBox
        Me.cboFixedSpecDateED = New System.Windows.Forms.ComboBox
        Me.cboFixedSpecDateSD = New System.Windows.Forms.ComboBox
        Me.lblTodayLineED = New System.Windows.Forms.Label
        Me.lblStartDateLineED = New System.Windows.Forms.Label
        Me.lblCustomLineED = New System.Windows.Forms.Label
        Me.lblAsOfBoxED = New System.Windows.Forms.Label
        Me.lblDisplaySD = New System.Windows.Forms.Label
        Me.groupBox3 = New System.Windows.Forms.GroupBox
        Me.tabControlSD = New System.Windows.Forms.TabControl
        Me.tabFixedSD = New System.Windows.Forms.TabPage
        Me.spinFixedFisCalYearSD = New System.Windows.Forms.NumericUpDown
        Me.cboFixedFisCalQtySD = New System.Windows.Forms.ComboBox
        Me.dtFixedSpecDateSD = New System.Windows.Forms.DateTimePicker
        Me.optFixedFiscCalSD = New System.Windows.Forms.RadioButton
        Me.optFixedSpecDateSD = New System.Windows.Forms.RadioButton
        Me.label10 = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.cbxFixedSpecDateTdySD = New System.Windows.Forms.CheckBox
        Me.label3 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.cboFixedFisCalPeriodSD = New System.Windows.Forms.ComboBox
        Me.cboFixedFisCalTypeSD = New System.Windows.Forms.ComboBox
        Me.tabRelativeSD = New System.Windows.Forms.TabPage
        Me.lblEndDateTextSD = New System.Windows.Forms.Label
        Me.lblTodayTextSD = New System.Windows.Forms.Label
        Me.dateCustomFiscalDateSD = New System.Windows.Forms.DateTimePicker
        Me.cboMovePeriodSD = New System.Windows.Forms.ComboBox
        Me.cboMoveTypeSD = New System.Windows.Forms.ComboBox
        Me.spinMoveQtySD = New System.Windows.Forms.NumericUpDown
        Me.spinCustomFiscalYearSD = New System.Windows.Forms.NumericUpDown
        Me.chboxCustomFiscalSD = New System.Windows.Forms.CheckBox
        Me.cboCustomFiscalQtySD = New System.Windows.Forms.ComboBox
        Me.optAsOfCustomSD = New System.Windows.Forms.RadioButton
        Me.optAsOfEndDateSD = New System.Windows.Forms.RadioButton
        Me.optAsOfTodaySD = New System.Windows.Forms.RadioButton
        Me.label11 = New System.Windows.Forms.Label
        Me.cboCustomFiscalTypeSD = New System.Windows.Forms.ComboBox
        Me.label12 = New System.Windows.Forms.Label
        Me.cboCustomFiscalPeriodSD = New System.Windows.Forms.ComboBox
        Me.cboMoveDirectionSD = New System.Windows.Forms.ComboBox
        Me.lblTodayLineSD = New System.Windows.Forms.Label
        Me.lblEndDateLineSD = New System.Windows.Forms.Label
        Me.lblCustomLineSD = New System.Windows.Forms.Label
        Me.lblAsOfBoxSD = New System.Windows.Forms.Label
        Me.groupBox1.SuspendLayout()
        Me.tabControlED.SuspendLayout()
        Me.tabFixedED.SuspendLayout()
        CType(Me.spinFixedFisCalYearED, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabRelativeED.SuspendLayout()
        CType(Me.spinMoveQtyED, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spinCustomFiscalYearED, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBox3.SuspendLayout()
        Me.tabControlSD.SuspendLayout()
        Me.tabFixedSD.SuspendLayout()
        CType(Me.spinFixedFisCalYearSD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabRelativeSD.SuspendLayout()
        CType(Me.spinMoveQtySD, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spinCustomFiscalYearSD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.SystemColors.Control
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(386, 397)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(68, 27)
        Me.btnCancel.TabIndex = 48
        Me.btnCancel.Text = "&Cancel"
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.SystemColors.Control
        Me.btnOk.Location = New System.Drawing.Point(309, 397)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(68, 27)
        Me.btnOk.TabIndex = 47
        Me.btnOk.Text = "&OK"
        '
        'lblDisplayED
        '
        Me.lblDisplayED.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblDisplayED.AutoSize = True
        Me.lblDisplayED.BackColor = System.Drawing.SystemColors.Control
        Me.lblDisplayED.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.lblDisplayED.ForeColor = System.Drawing.Color.Black
        Me.lblDisplayED.Location = New System.Drawing.Point(381, 205)
        Me.lblDisplayED.Name = "lblDisplayED"
        Me.lblDisplayED.Size = New System.Drawing.Size(67, 17)
        Me.lblDisplayED.TabIndex = 52
        Me.lblDisplayED.Text = "FY1111-2CM"
        Me.lblDisplayED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'groupBox1
        '
        Me.groupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox1.Controls.Add(Me.tabControlED)
        Me.groupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox1.Location = New System.Drawing.Point(15, 206)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(440, 174)
        Me.groupBox1.TabIndex = 51
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "End Date"
        '
        'tabControlED
        '
        Me.tabControlED.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabControlED.Controls.Add(Me.tabFixedED)
        Me.tabControlED.Controls.Add(Me.tabRelativeED)
        Me.tabControlED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabControlED.Location = New System.Drawing.Point(8, 27)
        Me.tabControlED.Name = "tabControlED"
        Me.tabControlED.SelectedIndex = 0
        Me.tabControlED.Size = New System.Drawing.Size(425, 139)
        Me.tabControlED.TabIndex = 43
        '
        'tabFixedED
        '
        Me.tabFixedED.Controls.Add(Me.spinFixedFisCalYearED)
        Me.tabFixedED.Controls.Add(Me.cboFixedFisCalQtyED)
        Me.tabFixedED.Controls.Add(Me.dtFixedSpecDateED)
        Me.tabFixedED.Controls.Add(Me.optFixedFiscCalED)
        Me.tabFixedED.Controls.Add(Me.optFixedSpecDateED)
        Me.tabFixedED.Controls.Add(Me.label4)
        Me.tabFixedED.Controls.Add(Me.label5)
        Me.tabFixedED.Controls.Add(Me.cbxFixedSpecDateTdyED)
        Me.tabFixedED.Controls.Add(Me.label6)
        Me.tabFixedED.Controls.Add(Me.label7)
        Me.tabFixedED.Controls.Add(Me.cboFixedFisCalPeriodED)
        Me.tabFixedED.Controls.Add(Me.cboFixedFisCalTypeED)
        Me.tabFixedED.Location = New System.Drawing.Point(4, 22)
        Me.tabFixedED.Name = "tabFixedED"
        Me.tabFixedED.Size = New System.Drawing.Size(417, 113)
        Me.tabFixedED.TabIndex = 0
        Me.tabFixedED.Text = "Fixed"
        '
        'spinFixedFisCalYearED
        '
        Me.spinFixedFisCalYearED.Location = New System.Drawing.Point(356, 69)
        Me.spinFixedFisCalYearED.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.spinFixedFisCalYearED.Minimum = New Decimal(New Integer() {1901, 0, 0, 0})
        Me.spinFixedFisCalYearED.Name = "spinFixedFisCalYearED"
        Me.spinFixedFisCalYearED.Size = New System.Drawing.Size(50, 21)
        Me.spinFixedFisCalYearED.TabIndex = 8
        Me.spinFixedFisCalYearED.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.spinFixedFisCalYearED.Value = New Decimal(New Integer() {2005, 0, 0, 0})
        '
        'cboFixedFisCalQtyED
        '
        Me.cboFixedFisCalQtyED.BackColor = System.Drawing.SystemColors.Window
        Me.cboFixedFisCalQtyED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFixedFisCalQtyED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFixedFisCalQtyED.ItemHeight = 13
        Me.cboFixedFisCalQtyED.Location = New System.Drawing.Point(295, 68)
        Me.cboFixedFisCalQtyED.Name = "cboFixedFisCalQtyED"
        Me.cboFixedFisCalQtyED.Size = New System.Drawing.Size(43, 21)
        Me.cboFixedFisCalQtyED.TabIndex = 7
        '
        'dtFixedSpecDateED
        '
        Me.dtFixedSpecDateED.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dtFixedSpecDateED.Location = New System.Drawing.Point(107, 27)
        Me.dtFixedSpecDateED.Name = "dtFixedSpecDateED"
        Me.dtFixedSpecDateED.Size = New System.Drawing.Size(86, 21)
        Me.dtFixedSpecDateED.TabIndex = 3
        '
        'optFixedFiscCalED
        '
        Me.optFixedFiscCalED.Location = New System.Drawing.Point(13, 68)
        Me.optFixedFiscCalED.Name = "optFixedFiscCalED"
        Me.optFixedFiscCalED.Size = New System.Drawing.Size(93, 24)
        Me.optFixedFiscCalED.TabIndex = 2
        Me.optFixedFiscCalED.Text = "Fisc/Calendar:"
        '
        'optFixedSpecDateED
        '
        Me.optFixedSpecDateED.Location = New System.Drawing.Point(13, 26)
        Me.optFixedSpecDateED.Name = "optFixedSpecDateED"
        Me.optFixedSpecDateED.Size = New System.Drawing.Size(93, 24)
        Me.optFixedSpecDateED.TabIndex = 0
        Me.optFixedSpecDateED.Text = "Specific Date:"
        '
        'label4
        '
        Me.label4.BackColor = System.Drawing.SystemColors.Control
        Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.ForeColor = System.Drawing.Color.Black
        Me.label4.Location = New System.Drawing.Point(176, 128)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(54, 20)
        Me.label4.TabIndex = 55
        Me.label4.Text = "EndDate: "
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label5
        '
        Me.label5.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(221, Byte), CType(216, Byte))
        Me.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.Location = New System.Drawing.Point(231, 128)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(220, 20)
        Me.label5.TabIndex = 53
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFixedSpecDateTdyED
        '
        Me.cbxFixedSpecDateTdyED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFixedSpecDateTdyED.Location = New System.Drawing.Point(199, 28)
        Me.cbxFixedSpecDateTdyED.Name = "cbxFixedSpecDateTdyED"
        Me.cbxFixedSpecDateTdyED.Size = New System.Drawing.Size(66, 20)
        Me.cbxFixedSpecDateTdyED.TabIndex = 4
        Me.cbxFixedSpecDateTdyED.Text = "Today"
        '
        'label6
        '
        Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.Location = New System.Drawing.Point(12, 138)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(47, 16)
        Me.label6.TabIndex = 51
        Me.label6.Text = "Move:"
        '
        'label7
        '
        Me.label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.Location = New System.Drawing.Point(339, 71)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(18, 16)
        Me.label7.TabIndex = 50
        Me.label7.Text = "of"
        Me.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFixedFisCalPeriodED
        '
        Me.cboFixedFisCalPeriodED.BackColor = System.Drawing.SystemColors.Window
        Me.cboFixedFisCalPeriodED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFixedFisCalPeriodED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFixedFisCalPeriodED.ItemHeight = 13
        Me.cboFixedFisCalPeriodED.Location = New System.Drawing.Point(198, 68)
        Me.cboFixedFisCalPeriodED.Name = "cboFixedFisCalPeriodED"
        Me.cboFixedFisCalPeriodED.Size = New System.Drawing.Size(96, 21)
        Me.cboFixedFisCalPeriodED.TabIndex = 6
        '
        'cboFixedFisCalTypeED
        '
        Me.cboFixedFisCalTypeED.BackColor = System.Drawing.SystemColors.Window
        Me.cboFixedFisCalTypeED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFixedFisCalTypeED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFixedFisCalTypeED.ItemHeight = 13
        Me.cboFixedFisCalTypeED.Location = New System.Drawing.Point(106, 68)
        Me.cboFixedFisCalTypeED.Name = "cboFixedFisCalTypeED"
        Me.cboFixedFisCalTypeED.Size = New System.Drawing.Size(90, 21)
        Me.cboFixedFisCalTypeED.TabIndex = 5
        '
        'tabRelativeED
        '
        Me.tabRelativeED.Controls.Add(Me.lblStartDateTextED)
        Me.tabRelativeED.Controls.Add(Me.lblTodayTextED)
        Me.tabRelativeED.Controls.Add(Me.dateCustomFiscalDateED)
        Me.tabRelativeED.Controls.Add(Me.cboMovePeriodED)
        Me.tabRelativeED.Controls.Add(Me.cboMoveTypeED)
        Me.tabRelativeED.Controls.Add(Me.spinMoveQtyED)
        Me.tabRelativeED.Controls.Add(Me.spinCustomFiscalYearED)
        Me.tabRelativeED.Controls.Add(Me.chboxCustomFiscalED)
        Me.tabRelativeED.Controls.Add(Me.cboCustomFiscalQtyED)
        Me.tabRelativeED.Controls.Add(Me.optAsOfCustomED)
        Me.tabRelativeED.Controls.Add(Me.optAsOfStartDateED)
        Me.tabRelativeED.Controls.Add(Me.optAsOfTodayED)
        Me.tabRelativeED.Controls.Add(Me.label9)
        Me.tabRelativeED.Controls.Add(Me.cboCustomFiscalTypeED)
        Me.tabRelativeED.Controls.Add(Me.label13)
        Me.tabRelativeED.Controls.Add(Me.cboCustomFiscalPeriodED)
        Me.tabRelativeED.Controls.Add(Me.cboMoveDirectionED)
        Me.tabRelativeED.Controls.Add(Me.lblTodayLineED)
        Me.tabRelativeED.Controls.Add(Me.lblStartDateLineED)
        Me.tabRelativeED.Controls.Add(Me.lblCustomLineED)
        Me.tabRelativeED.Controls.Add(Me.lblAsOfBoxED)
        Me.tabRelativeED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabRelativeED.Location = New System.Drawing.Point(4, 22)
        Me.tabRelativeED.Name = "tabRelativeED"
        Me.tabRelativeED.Size = New System.Drawing.Size(417, 113)
        Me.tabRelativeED.TabIndex = 1
        Me.tabRelativeED.Text = "Relative"
        Me.tabRelativeED.Visible = False
        '
        'lblStartDateTextED
        '
        Me.lblStartDateTextED.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.lblStartDateTextED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDateTextED.Location = New System.Drawing.Point(193, 43)
        Me.lblStartDateTextED.Name = "lblStartDateTextED"
        Me.lblStartDateTextED.Size = New System.Drawing.Size(63, 15)
        Me.lblStartDateTextED.TabIndex = 45
        Me.lblStartDateTextED.Text = "SD"
        Me.lblStartDateTextED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblStartDateTextED.Visible = False
        '
        'lblTodayTextED
        '
        Me.lblTodayTextED.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.lblTodayTextED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodayTextED.Location = New System.Drawing.Point(74, 43)
        Me.lblTodayTextED.Name = "lblTodayTextED"
        Me.lblTodayTextED.Size = New System.Drawing.Size(63, 15)
        Me.lblTodayTextED.TabIndex = 83
        Me.lblTodayTextED.Text = "TDY"
        Me.lblTodayTextED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTodayTextED.Visible = False
        '
        'dateCustomFiscalDateED
        '
        Me.dateCustomFiscalDateED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateCustomFiscalDateED.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dateCustomFiscalDateED.Location = New System.Drawing.Point(256, 40)
        Me.dateCustomFiscalDateED.MinDate = New Date(1901, 1, 1, 0, 0, 0, 0)
        Me.dateCustomFiscalDateED.Name = "dateCustomFiscalDateED"
        Me.dateCustomFiscalDateED.Size = New System.Drawing.Size(83, 21)
        Me.dateCustomFiscalDateED.TabIndex = 8
        Me.dateCustomFiscalDateED.Visible = False
        '
        'cboMovePeriodED
        '
        Me.cboMovePeriodED.BackColor = System.Drawing.SystemColors.Window
        Me.cboMovePeriodED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMovePeriodED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMovePeriodED.ItemHeight = 13
        Me.cboMovePeriodED.Location = New System.Drawing.Point(299, 76)
        Me.cboMovePeriodED.Name = "cboMovePeriodED"
        Me.cboMovePeriodED.Size = New System.Drawing.Size(102, 21)
        Me.cboMovePeriodED.TabIndex = 12
        '
        'cboMoveTypeED
        '
        Me.cboMoveTypeED.BackColor = System.Drawing.SystemColors.Window
        Me.cboMoveTypeED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoveTypeED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoveTypeED.ItemHeight = 13
        Me.cboMoveTypeED.Location = New System.Drawing.Point(206, 76)
        Me.cboMoveTypeED.Name = "cboMoveTypeED"
        Me.cboMoveTypeED.Size = New System.Drawing.Size(89, 21)
        Me.cboMoveTypeED.TabIndex = 11
        '
        'spinMoveQtyED
        '
        Me.spinMoveQtyED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spinMoveQtyED.Location = New System.Drawing.Point(148, 76)
        Me.spinMoveQtyED.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.spinMoveQtyED.Name = "spinMoveQtyED"
        Me.spinMoveQtyED.Size = New System.Drawing.Size(55, 21)
        Me.spinMoveQtyED.TabIndex = 10
        Me.spinMoveQtyED.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'spinCustomFiscalYearED
        '
        Me.spinCustomFiscalYearED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spinCustomFiscalYearED.Location = New System.Drawing.Point(287, 40)
        Me.spinCustomFiscalYearED.Maximum = New Decimal(New Integer() {2999, 0, 0, 0})
        Me.spinCustomFiscalYearED.Minimum = New Decimal(New Integer() {1965, 0, 0, 0})
        Me.spinCustomFiscalYearED.Name = "spinCustomFiscalYearED"
        Me.spinCustomFiscalYearED.Size = New System.Drawing.Size(52, 21)
        Me.spinCustomFiscalYearED.TabIndex = 6
        Me.spinCustomFiscalYearED.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.spinCustomFiscalYearED.Value = New Decimal(New Integer() {2005, 0, 0, 0})
        '
        'chboxCustomFiscalED
        '
        Me.chboxCustomFiscalED.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.chboxCustomFiscalED.Checked = True
        Me.chboxCustomFiscalED.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chboxCustomFiscalED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chboxCustomFiscalED.Location = New System.Drawing.Point(343, 42)
        Me.chboxCustomFiscalED.Name = "chboxCustomFiscalED"
        Me.chboxCustomFiscalED.Size = New System.Drawing.Size(56, 19)
        Me.chboxCustomFiscalED.TabIndex = 7
        Me.chboxCustomFiscalED.Text = "Fiscal"
        '
        'cboCustomFiscalQtyED
        '
        Me.cboCustomFiscalQtyED.BackColor = System.Drawing.SystemColors.Window
        Me.cboCustomFiscalQtyED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomFiscalQtyED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomFiscalQtyED.ItemHeight = 13
        Me.cboCustomFiscalQtyED.Location = New System.Drawing.Point(247, 40)
        Me.cboCustomFiscalQtyED.Name = "cboCustomFiscalQtyED"
        Me.cboCustomFiscalQtyED.Size = New System.Drawing.Size(39, 21)
        Me.cboCustomFiscalQtyED.TabIndex = 5
        '
        'optAsOfCustomED
        '
        Me.optAsOfCustomED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAsOfCustomED.Location = New System.Drawing.Point(290, 15)
        Me.optAsOfCustomED.Name = "optAsOfCustomED"
        Me.optAsOfCustomED.Size = New System.Drawing.Size(60, 20)
        Me.optAsOfCustomED.TabIndex = 2
        Me.optAsOfCustomED.Text = "Custom"
        '
        'optAsOfStartDateED
        '
        Me.optAsOfStartDateED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAsOfStartDateED.Location = New System.Drawing.Point(172, 15)
        Me.optAsOfStartDateED.Name = "optAsOfStartDateED"
        Me.optAsOfStartDateED.Size = New System.Drawing.Size(73, 20)
        Me.optAsOfStartDateED.TabIndex = 1
        Me.optAsOfStartDateED.Text = "Start Date"
        '
        'optAsOfTodayED
        '
        Me.optAsOfTodayED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAsOfTodayED.Location = New System.Drawing.Point(62, 15)
        Me.optAsOfTodayED.Name = "optAsOfTodayED"
        Me.optAsOfTodayED.Size = New System.Drawing.Size(55, 20)
        Me.optAsOfTodayED.TabIndex = 0
        Me.optAsOfTodayED.Text = "Today"
        '
        'label9
        '
        Me.label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label9.Location = New System.Drawing.Point(16, 79)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(39, 16)
        Me.label9.TabIndex = 64
        Me.label9.Text = "Move:"
        '
        'cboCustomFiscalTypeED
        '
        Me.cboCustomFiscalTypeED.BackColor = System.Drawing.SystemColors.Window
        Me.cboCustomFiscalTypeED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomFiscalTypeED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomFiscalTypeED.ItemHeight = 13
        Me.cboCustomFiscalTypeED.Location = New System.Drawing.Point(72, 40)
        Me.cboCustomFiscalTypeED.Name = "cboCustomFiscalTypeED"
        Me.cboCustomFiscalTypeED.Size = New System.Drawing.Size(88, 21)
        Me.cboCustomFiscalTypeED.TabIndex = 3
        '
        'label13
        '
        Me.label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label13.Location = New System.Drawing.Point(17, 16)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(40, 16)
        Me.label13.TabIndex = 63
        Me.label13.Text = "As of:"
        Me.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCustomFiscalPeriodED
        '
        Me.cboCustomFiscalPeriodED.BackColor = System.Drawing.SystemColors.Window
        Me.cboCustomFiscalPeriodED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomFiscalPeriodED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomFiscalPeriodED.ItemHeight = 13
        Me.cboCustomFiscalPeriodED.Location = New System.Drawing.Point(161, 40)
        Me.cboCustomFiscalPeriodED.Name = "cboCustomFiscalPeriodED"
        Me.cboCustomFiscalPeriodED.Size = New System.Drawing.Size(85, 21)
        Me.cboCustomFiscalPeriodED.TabIndex = 4
        '
        'cboMoveDirectionED
        '
        Me.cboMoveDirectionED.BackColor = System.Drawing.SystemColors.Window
        Me.cboMoveDirectionED.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoveDirectionED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoveDirectionED.ItemHeight = 13
        Me.cboMoveDirectionED.Location = New System.Drawing.Point(66, 76)
        Me.cboMoveDirectionED.Name = "cboMoveDirectionED"
        Me.cboMoveDirectionED.Size = New System.Drawing.Size(79, 21)
        Me.cboMoveDirectionED.TabIndex = 9
        '
        'lblTodayLineED
        '
        Me.lblTodayLineED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTodayLineED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodayLineED.ForeColor = System.Drawing.Color.White
        Me.lblTodayLineED.Location = New System.Drawing.Point(67, 21)
        Me.lblTodayLineED.Name = "lblTodayLineED"
        Me.lblTodayLineED.Size = New System.Drawing.Size(76, 15)
        Me.lblTodayLineED.TabIndex = 72
        Me.lblTodayLineED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStartDateLineED
        '
        Me.lblStartDateLineED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblStartDateLineED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDateLineED.ForeColor = System.Drawing.Color.White
        Me.lblStartDateLineED.Location = New System.Drawing.Point(177, 21)
        Me.lblStartDateLineED.Name = "lblStartDateLineED"
        Me.lblStartDateLineED.Size = New System.Drawing.Size(86, 15)
        Me.lblStartDateLineED.TabIndex = 73
        Me.lblStartDateLineED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCustomLineED
        '
        Me.lblCustomLineED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCustomLineED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomLineED.ForeColor = System.Drawing.Color.White
        Me.lblCustomLineED.Location = New System.Drawing.Point(296, 21)
        Me.lblCustomLineED.Name = "lblCustomLineED"
        Me.lblCustomLineED.Size = New System.Drawing.Size(104, 15)
        Me.lblCustomLineED.TabIndex = 74
        Me.lblCustomLineED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAsOfBoxED
        '
        Me.lblAsOfBoxED.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.lblAsOfBoxED.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAsOfBoxED.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOfBoxED.ForeColor = System.Drawing.Color.White
        Me.lblAsOfBoxED.Location = New System.Drawing.Point(67, 35)
        Me.lblAsOfBoxED.Name = "lblAsOfBoxED"
        Me.lblAsOfBoxED.Size = New System.Drawing.Size(333, 31)
        Me.lblAsOfBoxED.TabIndex = 61
        Me.lblAsOfBoxED.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDisplaySD
        '
        Me.lblDisplaySD.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblDisplaySD.AutoSize = True
        Me.lblDisplaySD.BackColor = System.Drawing.SystemColors.Control
        Me.lblDisplaySD.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.lblDisplaySD.ForeColor = System.Drawing.Color.Black
        Me.lblDisplaySD.Location = New System.Drawing.Point(381, 17)
        Me.lblDisplaySD.Name = "lblDisplaySD"
        Me.lblDisplaySD.Size = New System.Drawing.Size(67, 17)
        Me.lblDisplaySD.TabIndex = 50
        Me.lblDisplaySD.Text = "FY1111-2CM"
        Me.lblDisplaySD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'groupBox3
        '
        Me.groupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox3.Controls.Add(Me.tabControlSD)
        Me.groupBox3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupBox3.Location = New System.Drawing.Point(15, 18)
        Me.groupBox3.Name = "groupBox3"
        Me.groupBox3.Size = New System.Drawing.Size(440, 173)
        Me.groupBox3.TabIndex = 49
        Me.groupBox3.TabStop = False
        Me.groupBox3.Text = "Start Date"
        '
        'tabControlSD
        '
        Me.tabControlSD.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabControlSD.Controls.Add(Me.tabFixedSD)
        Me.tabControlSD.Controls.Add(Me.tabRelativeSD)
        Me.tabControlSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabControlSD.Location = New System.Drawing.Point(8, 27)
        Me.tabControlSD.Name = "tabControlSD"
        Me.tabControlSD.SelectedIndex = 0
        Me.tabControlSD.Size = New System.Drawing.Size(425, 138)
        Me.tabControlSD.TabIndex = 43
        '
        'tabFixedSD
        '
        Me.tabFixedSD.Controls.Add(Me.spinFixedFisCalYearSD)
        Me.tabFixedSD.Controls.Add(Me.cboFixedFisCalQtySD)
        Me.tabFixedSD.Controls.Add(Me.dtFixedSpecDateSD)
        Me.tabFixedSD.Controls.Add(Me.optFixedFiscCalSD)
        Me.tabFixedSD.Controls.Add(Me.optFixedSpecDateSD)
        Me.tabFixedSD.Controls.Add(Me.label10)
        Me.tabFixedSD.Controls.Add(Me.lblEndDate)
        Me.tabFixedSD.Controls.Add(Me.cbxFixedSpecDateTdySD)
        Me.tabFixedSD.Controls.Add(Me.label3)
        Me.tabFixedSD.Controls.Add(Me.label2)
        Me.tabFixedSD.Controls.Add(Me.cboFixedFisCalPeriodSD)
        Me.tabFixedSD.Controls.Add(Me.cboFixedFisCalTypeSD)
        Me.tabFixedSD.Location = New System.Drawing.Point(4, 22)
        Me.tabFixedSD.Name = "tabFixedSD"
        Me.tabFixedSD.Size = New System.Drawing.Size(417, 112)
        Me.tabFixedSD.TabIndex = 0
        Me.tabFixedSD.Text = "Fixed"
        '
        'spinFixedFisCalYearSD
        '
        Me.spinFixedFisCalYearSD.Location = New System.Drawing.Point(356, 69)
        Me.spinFixedFisCalYearSD.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.spinFixedFisCalYearSD.Minimum = New Decimal(New Integer() {1901, 0, 0, 0})
        Me.spinFixedFisCalYearSD.Name = "spinFixedFisCalYearSD"
        Me.spinFixedFisCalYearSD.Size = New System.Drawing.Size(50, 21)
        Me.spinFixedFisCalYearSD.TabIndex = 7
        Me.spinFixedFisCalYearSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.spinFixedFisCalYearSD.Value = New Decimal(New Integer() {2005, 0, 0, 0})
        '
        'cboFixedFisCalQtySD
        '
        Me.cboFixedFisCalQtySD.BackColor = System.Drawing.SystemColors.Window
        Me.cboFixedFisCalQtySD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFixedFisCalQtySD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFixedFisCalQtySD.ItemHeight = 13
        Me.cboFixedFisCalQtySD.Location = New System.Drawing.Point(295, 68)
        Me.cboFixedFisCalQtySD.Name = "cboFixedFisCalQtySD"
        Me.cboFixedFisCalQtySD.Size = New System.Drawing.Size(43, 21)
        Me.cboFixedFisCalQtySD.TabIndex = 6
        '
        'dtFixedSpecDateSD
        '
        Me.dtFixedSpecDateSD.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dtFixedSpecDateSD.Location = New System.Drawing.Point(107, 27)
        Me.dtFixedSpecDateSD.Name = "dtFixedSpecDateSD"
        Me.dtFixedSpecDateSD.Size = New System.Drawing.Size(86, 21)
        Me.dtFixedSpecDateSD.TabIndex = 2
        '
        'optFixedFiscCalSD
        '
        Me.optFixedFiscCalSD.Location = New System.Drawing.Point(13, 68)
        Me.optFixedFiscCalSD.Name = "optFixedFiscCalSD"
        Me.optFixedFiscCalSD.Size = New System.Drawing.Size(93, 24)
        Me.optFixedFiscCalSD.TabIndex = 1
        Me.optFixedFiscCalSD.Text = "Fisc/Calendar:"
        '
        'optFixedSpecDateSD
        '
        Me.optFixedSpecDateSD.Location = New System.Drawing.Point(13, 26)
        Me.optFixedSpecDateSD.Name = "optFixedSpecDateSD"
        Me.optFixedSpecDateSD.Size = New System.Drawing.Size(93, 24)
        Me.optFixedSpecDateSD.TabIndex = 0
        Me.optFixedSpecDateSD.Text = "Specific Date:"
        '
        'label10
        '
        Me.label10.BackColor = System.Drawing.SystemColors.Control
        Me.label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label10.ForeColor = System.Drawing.Color.Black
        Me.label10.Location = New System.Drawing.Point(176, 128)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(54, 20)
        Me.label10.TabIndex = 55
        Me.label10.Text = "EndDate: "
        Me.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEndDate
        '
        Me.lblEndDate.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(221, Byte), CType(216, Byte))
        Me.lblEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblEndDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(231, 128)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(220, 20)
        Me.lblEndDate.TabIndex = 53
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbxFixedSpecDateTdySD
        '
        Me.cbxFixedSpecDateTdySD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxFixedSpecDateTdySD.Location = New System.Drawing.Point(199, 28)
        Me.cbxFixedSpecDateTdySD.Name = "cbxFixedSpecDateTdySD"
        Me.cbxFixedSpecDateTdySD.Size = New System.Drawing.Size(66, 20)
        Me.cbxFixedSpecDateTdySD.TabIndex = 3
        Me.cbxFixedSpecDateTdySD.Text = "Today"
        '
        'label3
        '
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(12, 138)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(47, 16)
        Me.label3.TabIndex = 51
        Me.label3.Text = "Move:"
        '
        'label2
        '
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(339, 71)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(18, 16)
        Me.label2.TabIndex = 50
        Me.label2.Text = "of"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFixedFisCalPeriodSD
        '
        Me.cboFixedFisCalPeriodSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboFixedFisCalPeriodSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFixedFisCalPeriodSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFixedFisCalPeriodSD.ItemHeight = 13
        Me.cboFixedFisCalPeriodSD.Location = New System.Drawing.Point(198, 68)
        Me.cboFixedFisCalPeriodSD.Name = "cboFixedFisCalPeriodSD"
        Me.cboFixedFisCalPeriodSD.Size = New System.Drawing.Size(96, 21)
        Me.cboFixedFisCalPeriodSD.TabIndex = 5
        '
        'cboFixedFisCalTypeSD
        '
        Me.cboFixedFisCalTypeSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboFixedFisCalTypeSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFixedFisCalTypeSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFixedFisCalTypeSD.ItemHeight = 13
        Me.cboFixedFisCalTypeSD.Location = New System.Drawing.Point(106, 68)
        Me.cboFixedFisCalTypeSD.Name = "cboFixedFisCalTypeSD"
        Me.cboFixedFisCalTypeSD.Size = New System.Drawing.Size(90, 21)
        Me.cboFixedFisCalTypeSD.TabIndex = 4
        '
        'tabRelativeSD
        '
        Me.tabRelativeSD.Controls.Add(Me.lblEndDateTextSD)
        Me.tabRelativeSD.Controls.Add(Me.lblTodayTextSD)
        Me.tabRelativeSD.Controls.Add(Me.dateCustomFiscalDateSD)
        Me.tabRelativeSD.Controls.Add(Me.cboMovePeriodSD)
        Me.tabRelativeSD.Controls.Add(Me.cboMoveTypeSD)
        Me.tabRelativeSD.Controls.Add(Me.spinMoveQtySD)
        Me.tabRelativeSD.Controls.Add(Me.spinCustomFiscalYearSD)
        Me.tabRelativeSD.Controls.Add(Me.chboxCustomFiscalSD)
        Me.tabRelativeSD.Controls.Add(Me.cboCustomFiscalQtySD)
        Me.tabRelativeSD.Controls.Add(Me.optAsOfCustomSD)
        Me.tabRelativeSD.Controls.Add(Me.optAsOfEndDateSD)
        Me.tabRelativeSD.Controls.Add(Me.optAsOfTodaySD)
        Me.tabRelativeSD.Controls.Add(Me.label11)
        Me.tabRelativeSD.Controls.Add(Me.cboCustomFiscalTypeSD)
        Me.tabRelativeSD.Controls.Add(Me.label12)
        Me.tabRelativeSD.Controls.Add(Me.cboCustomFiscalPeriodSD)
        Me.tabRelativeSD.Controls.Add(Me.cboMoveDirectionSD)
        Me.tabRelativeSD.Controls.Add(Me.lblTodayLineSD)
        Me.tabRelativeSD.Controls.Add(Me.lblEndDateLineSD)
        Me.tabRelativeSD.Controls.Add(Me.lblCustomLineSD)
        Me.tabRelativeSD.Controls.Add(Me.lblAsOfBoxSD)
        Me.tabRelativeSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabRelativeSD.Location = New System.Drawing.Point(4, 22)
        Me.tabRelativeSD.Name = "tabRelativeSD"
        Me.tabRelativeSD.Size = New System.Drawing.Size(417, 112)
        Me.tabRelativeSD.TabIndex = 1
        Me.tabRelativeSD.Text = "Relative"
        Me.tabRelativeSD.Visible = False
        '
        'lblEndDateTextSD
        '
        Me.lblEndDateTextSD.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.lblEndDateTextSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDateTextSD.Location = New System.Drawing.Point(193, 43)
        Me.lblEndDateTextSD.Name = "lblEndDateTextSD"
        Me.lblEndDateTextSD.Size = New System.Drawing.Size(63, 15)
        Me.lblEndDateTextSD.TabIndex = 45
        Me.lblEndDateTextSD.Text = "ED"
        Me.lblEndDateTextSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblEndDateTextSD.Visible = False
        '
        'lblTodayTextSD
        '
        Me.lblTodayTextSD.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.lblTodayTextSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodayTextSD.Location = New System.Drawing.Point(74, 43)
        Me.lblTodayTextSD.Name = "lblTodayTextSD"
        Me.lblTodayTextSD.Size = New System.Drawing.Size(63, 15)
        Me.lblTodayTextSD.TabIndex = 83
        Me.lblTodayTextSD.Text = "TDY"
        Me.lblTodayTextSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTodayTextSD.Visible = False
        '
        'dateCustomFiscalDateSD
        '
        Me.dateCustomFiscalDateSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateCustomFiscalDateSD.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.dateCustomFiscalDateSD.Location = New System.Drawing.Point(256, 40)
        Me.dateCustomFiscalDateSD.MinDate = New Date(1901, 1, 1, 0, 0, 0, 0)
        Me.dateCustomFiscalDateSD.Name = "dateCustomFiscalDateSD"
        Me.dateCustomFiscalDateSD.Size = New System.Drawing.Size(83, 21)
        Me.dateCustomFiscalDateSD.TabIndex = 8
        Me.dateCustomFiscalDateSD.Visible = False
        '
        'cboMovePeriodSD
        '
        Me.cboMovePeriodSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboMovePeriodSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMovePeriodSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMovePeriodSD.ItemHeight = 13
        Me.cboMovePeriodSD.Location = New System.Drawing.Point(299, 76)
        Me.cboMovePeriodSD.Name = "cboMovePeriodSD"
        Me.cboMovePeriodSD.Size = New System.Drawing.Size(102, 21)
        Me.cboMovePeriodSD.TabIndex = 12
        '
        'cboMoveTypeSD
        '
        Me.cboMoveTypeSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboMoveTypeSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoveTypeSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoveTypeSD.ItemHeight = 13
        Me.cboMoveTypeSD.Location = New System.Drawing.Point(206, 76)
        Me.cboMoveTypeSD.Name = "cboMoveTypeSD"
        Me.cboMoveTypeSD.Size = New System.Drawing.Size(89, 21)
        Me.cboMoveTypeSD.TabIndex = 11
        '
        'spinMoveQtySD
        '
        Me.spinMoveQtySD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spinMoveQtySD.Location = New System.Drawing.Point(148, 76)
        Me.spinMoveQtySD.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.spinMoveQtySD.Name = "spinMoveQtySD"
        Me.spinMoveQtySD.Size = New System.Drawing.Size(55, 21)
        Me.spinMoveQtySD.TabIndex = 10
        Me.spinMoveQtySD.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'spinCustomFiscalYearSD
        '
        Me.spinCustomFiscalYearSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.spinCustomFiscalYearSD.Location = New System.Drawing.Point(287, 40)
        Me.spinCustomFiscalYearSD.Maximum = New Decimal(New Integer() {2999, 0, 0, 0})
        Me.spinCustomFiscalYearSD.Minimum = New Decimal(New Integer() {1965, 0, 0, 0})
        Me.spinCustomFiscalYearSD.Name = "spinCustomFiscalYearSD"
        Me.spinCustomFiscalYearSD.Size = New System.Drawing.Size(52, 21)
        Me.spinCustomFiscalYearSD.TabIndex = 6
        Me.spinCustomFiscalYearSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.spinCustomFiscalYearSD.Value = New Decimal(New Integer() {2005, 0, 0, 0})
        '
        'chboxCustomFiscalSD
        '
        Me.chboxCustomFiscalSD.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.chboxCustomFiscalSD.Checked = True
        Me.chboxCustomFiscalSD.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chboxCustomFiscalSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chboxCustomFiscalSD.Location = New System.Drawing.Point(343, 42)
        Me.chboxCustomFiscalSD.Name = "chboxCustomFiscalSD"
        Me.chboxCustomFiscalSD.Size = New System.Drawing.Size(56, 19)
        Me.chboxCustomFiscalSD.TabIndex = 7
        Me.chboxCustomFiscalSD.Text = "Fiscal"
        '
        'cboCustomFiscalQtySD
        '
        Me.cboCustomFiscalQtySD.BackColor = System.Drawing.SystemColors.Window
        Me.cboCustomFiscalQtySD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomFiscalQtySD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomFiscalQtySD.ItemHeight = 13
        Me.cboCustomFiscalQtySD.Location = New System.Drawing.Point(247, 40)
        Me.cboCustomFiscalQtySD.Name = "cboCustomFiscalQtySD"
        Me.cboCustomFiscalQtySD.Size = New System.Drawing.Size(39, 21)
        Me.cboCustomFiscalQtySD.TabIndex = 5
        '
        'optAsOfCustomSD
        '
        Me.optAsOfCustomSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAsOfCustomSD.Location = New System.Drawing.Point(290, 15)
        Me.optAsOfCustomSD.Name = "optAsOfCustomSD"
        Me.optAsOfCustomSD.Size = New System.Drawing.Size(60, 20)
        Me.optAsOfCustomSD.TabIndex = 2
        Me.optAsOfCustomSD.Text = "Custom"
        '
        'optAsOfEndDateSD
        '
        Me.optAsOfEndDateSD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAsOfEndDateSD.Location = New System.Drawing.Point(172, 15)
        Me.optAsOfEndDateSD.Name = "optAsOfEndDateSD"
        Me.optAsOfEndDateSD.Size = New System.Drawing.Size(69, 20)
        Me.optAsOfEndDateSD.TabIndex = 1
        Me.optAsOfEndDateSD.Text = "End Date"
        '
        'optAsOfTodaySD
        '
        Me.optAsOfTodaySD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAsOfTodaySD.Location = New System.Drawing.Point(67, 15)
        Me.optAsOfTodaySD.Name = "optAsOfTodaySD"
        Me.optAsOfTodaySD.Size = New System.Drawing.Size(55, 20)
        Me.optAsOfTodaySD.TabIndex = 0
        Me.optAsOfTodaySD.Text = "Today"
        '
        'label11
        '
        Me.label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label11.Location = New System.Drawing.Point(16, 79)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(39, 16)
        Me.label11.TabIndex = 64
        Me.label11.Text = "Move:"
        '
        'cboCustomFiscalTypeSD
        '
        Me.cboCustomFiscalTypeSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboCustomFiscalTypeSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomFiscalTypeSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomFiscalTypeSD.ItemHeight = 13
        Me.cboCustomFiscalTypeSD.Location = New System.Drawing.Point(72, 40)
        Me.cboCustomFiscalTypeSD.Name = "cboCustomFiscalTypeSD"
        Me.cboCustomFiscalTypeSD.Size = New System.Drawing.Size(88, 21)
        Me.cboCustomFiscalTypeSD.TabIndex = 3
        '
        'label12
        '
        Me.label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label12.Location = New System.Drawing.Point(17, 16)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(40, 16)
        Me.label12.TabIndex = 63
        Me.label12.Text = "As of:"
        Me.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCustomFiscalPeriodSD
        '
        Me.cboCustomFiscalPeriodSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboCustomFiscalPeriodSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomFiscalPeriodSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomFiscalPeriodSD.ItemHeight = 13
        Me.cboCustomFiscalPeriodSD.Location = New System.Drawing.Point(161, 40)
        Me.cboCustomFiscalPeriodSD.Name = "cboCustomFiscalPeriodSD"
        Me.cboCustomFiscalPeriodSD.Size = New System.Drawing.Size(85, 21)
        Me.cboCustomFiscalPeriodSD.TabIndex = 4
        '
        'cboMoveDirectionSD
        '
        Me.cboMoveDirectionSD.BackColor = System.Drawing.SystemColors.Window
        Me.cboMoveDirectionSD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoveDirectionSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoveDirectionSD.ItemHeight = 13
        Me.cboMoveDirectionSD.Location = New System.Drawing.Point(66, 76)
        Me.cboMoveDirectionSD.Name = "cboMoveDirectionSD"
        Me.cboMoveDirectionSD.Size = New System.Drawing.Size(79, 21)
        Me.cboMoveDirectionSD.TabIndex = 9
        '
        'lblTodayLineSD
        '
        Me.lblTodayLineSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTodayLineSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodayLineSD.ForeColor = System.Drawing.Color.White
        Me.lblTodayLineSD.Location = New System.Drawing.Point(67, 23)
        Me.lblTodayLineSD.Name = "lblTodayLineSD"
        Me.lblTodayLineSD.Size = New System.Drawing.Size(76, 13)
        Me.lblTodayLineSD.TabIndex = 72
        Me.lblTodayLineSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEndDateLineSD
        '
        Me.lblEndDateLineSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEndDateLineSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDateLineSD.ForeColor = System.Drawing.Color.White
        Me.lblEndDateLineSD.Location = New System.Drawing.Point(177, 22)
        Me.lblEndDateLineSD.Name = "lblEndDateLineSD"
        Me.lblEndDateLineSD.Size = New System.Drawing.Size(86, 14)
        Me.lblEndDateLineSD.TabIndex = 73
        Me.lblEndDateLineSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCustomLineSD
        '
        Me.lblCustomLineSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCustomLineSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomLineSD.ForeColor = System.Drawing.Color.White
        Me.lblCustomLineSD.Location = New System.Drawing.Point(296, 21)
        Me.lblCustomLineSD.Name = "lblCustomLineSD"
        Me.lblCustomLineSD.Size = New System.Drawing.Size(104, 15)
        Me.lblCustomLineSD.TabIndex = 74
        Me.lblCustomLineSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAsOfBoxSD
        '
        Me.lblAsOfBoxSD.BackColor = System.Drawing.Color.FromArgb(CType(224, Byte), CType(224, Byte), CType(224, Byte))
        Me.lblAsOfBoxSD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAsOfBoxSD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOfBoxSD.ForeColor = System.Drawing.Color.White
        Me.lblAsOfBoxSD.Location = New System.Drawing.Point(67, 35)
        Me.lblAsOfBoxSD.Name = "lblAsOfBoxSD"
        Me.lblAsOfBoxSD.Size = New System.Drawing.Size(333, 31)
        Me.lblAsOfBoxSD.TabIndex = 61
        Me.lblAsOfBoxSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmSetDates
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(471, 440)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.lblDisplayED)
        Me.Controls.Add(Me.lblDisplaySD)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.groupBox3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSetDates"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Set Start and End Dates"
        Me.groupBox1.ResumeLayout(False)
        Me.tabControlED.ResumeLayout(False)
        Me.tabFixedED.ResumeLayout(False)
        CType(Me.spinFixedFisCalYearED, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabRelativeED.ResumeLayout(False)
        CType(Me.spinMoveQtyED, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spinCustomFiscalYearED, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBox3.ResumeLayout(False)
        Me.tabControlSD.ResumeLayout(False)
        Me.tabFixedSD.ResumeLayout(False)
        CType(Me.spinFixedFisCalYearSD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabRelativeSD.ResumeLayout(False)
        CType(Me.spinMoveQtySD, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spinCustomFiscalYearSD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private SdExtendedDate As ExtendedDate
    Private EdExtendedDate As ExtendedDate
    Private SdDateAnchor As DateAnchor
    Private EdDateAnchor As DateAnchor
    Private SdDateOffset As DateOffset
    Private EdDateOffset As DateOffset
    Private SdCurrentType As DATE_TYPE
    Private EdCurrentType As DATE_TYPE

    Private moveDirection = New OffsetDirection() {OffsetDirection.Negative, OffsetDirection.Positive}
    Private calTypeAll = New CalendarType() {CalendarType.Actual, CalendarType.Calendar, CalendarType.Fiscal}
    Private calTypeCalFisc = New CalendarType() {CalendarType.Calendar, CalendarType.Fiscal}
    Private periodUnitActCal = New PeriodUnit() {PeriodUnit.Day, PeriodUnit.Week, PeriodUnit.Month, PeriodUnit.Quarter, _
                                            PeriodUnit.SemiAnnual, PeriodUnit.Year}
    Private periodUnitFiscal = New PeriodUnit() {PeriodUnit.Quarter, PeriodUnit.SemiAnnual, PeriodUnit.Year}
    Private strDisplayedDateSD As String = ""
    Private strDisplayedDateED As String = ""
    Private gIsFormSaved As Boolean = False


#Region " Public Enum SECTION "

    Enum AS_OF_TOPICS
        Today
        ED_SD
        Custom
    End Enum

    Enum DATE_TYPE
        Fixed
        Relative
    End Enum

#End Region

    Private Sub frmSetDates_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '**************************
        ' Populate dropdown lists:
        '**************************

        '  1) Fixed tab
        cboFixedFisCalTypeSD.DataSource = calTypeCalFisc.Clone()
        cboFixedFisCalTypeED.DataSource = calTypeCalFisc.Clone()
        cboFixedFisCalPeriodSD.DataSource = periodUnitFiscal.Clone()
        cboFixedFisCalPeriodED.DataSource = periodUnitFiscal.Clone()
        cboFixedFisCalQtySD.Items.AddRange(New Object() {"1", "2", "3", "4"})
        cboFixedFisCalQtyED.Items.AddRange(New Object() {"1", "2", "3", "4"})

        '  2) Relative Tab
        cboCustomFiscalTypeSD.DataSource = calTypeCalFisc.Clone()
        cboCustomFiscalTypeED.DataSource = calTypeCalFisc.Clone()
        cboCustomFiscalPeriodSD.DataSource = periodUnitFiscal.Clone()
        cboCustomFiscalPeriodED.DataSource = periodUnitFiscal.Clone()
        cboCustomFiscalQtySD.Items.AddRange(New Object() {"1", "2", "3", "4"})
        cboCustomFiscalQtyED.Items.AddRange(New Object() {"1", "2", "3", "4"})
        cboMoveDirectionSD.DataSource = moveDirection.Clone()
        cboMoveDirectionED.DataSource = moveDirection.Clone()
        cboMoveTypeSD.DataSource = calTypeAll.Clone()
        cboMoveTypeED.DataSource = calTypeAll.Clone()
        cboMovePeriodSD.DataSource = periodUnitActCal.Clone()
        cboMovePeriodED.DataSource = periodUnitActCal.Clone()

        AddHandler tabControlSD.Click, AddressOf tabSD_Click
        AddHandler tabControlED.Click, AddressOf tabED_Click

        ' Fixed screen Controls
        AddHandler optFixedSpecDateSD.CheckedChanged, AddressOf FixedSD_Click
        AddHandler optFixedSpecDateED.CheckedChanged, AddressOf FixedED_Click
        AddHandler cbxFixedSpecDateTdySD.CheckedChanged, AddressOf FixedSD_Click
        AddHandler cbxFixedSpecDateTdyED.CheckedChanged, AddressOf FixedED_Click
        AddHandler optFixedFiscCalSD.CheckedChanged, AddressOf FixedSD_Click
        AddHandler optFixedFiscCalED.CheckedChanged, AddressOf FixedED_Click
        AddHandler cboFixedSpecDateSD.Click, AddressOf cbxFixedSpecDateTdySD_CheckStateChanged
        AddHandler cboFixedSpecDateED.Click, AddressOf cbxFixedSpecDateTdyED_CheckStateChanged
        AddHandler dtFixedSpecDateSD.ValueChanged, AddressOf FixedSD_Click
        AddHandler dtFixedSpecDateED.ValueChanged, AddressOf FixedED_Click
        AddHandler cboFixedFisCalTypeSD.SelectionChangeCommitted, AddressOf FixedSD_Click
        AddHandler cboFixedFisCalTypeED.SelectionChangeCommitted, AddressOf FixedED_Click
        AddHandler cboFixedFisCalQtySD.SelectionChangeCommitted, AddressOf FixedSD_Click
        AddHandler cboFixedFisCalQtyED.SelectionChangeCommitted, AddressOf FixedED_Click
        AddHandler spinFixedFisCalYearSD.ValueChanged, AddressOf FixedSD_Click
        AddHandler spinFixedFisCalYearED.ValueChanged, AddressOf FixedED_Click
        AddHandler cboFixedFisCalPeriodSD.SelectionChangeCommitted, AddressOf FixedPeriodicitySD_Click
        AddHandler cboFixedFisCalPeriodED.SelectionChangeCommitted, AddressOf FixedPeriodicityED_Click

        ' Relative Anchor Controls
        AddHandler optAsOfTodaySD.CheckedChanged, AddressOf optAsOfSD_CheckedChanged
        AddHandler optAsOfTodayED.CheckedChanged, AddressOf optAsOfED_CheckedChanged
        AddHandler optAsOfEndDateSD.CheckedChanged, AddressOf optAsOfSD_CheckedChanged
        AddHandler optAsOfStartDateED.CheckedChanged, AddressOf optAsOfED_CheckedChanged
        AddHandler optAsOfCustomSD.CheckedChanged, AddressOf optAsOfSD_CheckedChanged
        AddHandler optAsOfCustomED.CheckedChanged, AddressOf optAsOfED_CheckedChanged
        AddHandler chboxCustomFiscalSD.CheckedChanged, AddressOf chboxCustomFiscalSD_CheckedChanged
        AddHandler chboxCustomFiscalED.CheckedChanged, AddressOf chboxCustomFiscalED_CheckedChanged
        AddHandler cboCustomFiscalTypeSD.SelectionChangeCommitted, AddressOf optAsOfSD_Click
        AddHandler cboCustomFiscalTypeED.SelectionChangeCommitted, AddressOf optAsOfED_Click
        AddHandler cboCustomFiscalPeriodSD.SelectionChangeCommitted, AddressOf cboCustomFiscalPeriodSD_SelectionChangeCommitted
        AddHandler cboCustomFiscalPeriodED.SelectionChangeCommitted, AddressOf cboCustomFiscalPeriodED_SelectionChangeCommitted
        AddHandler cboCustomFiscalQtySD.SelectedIndexChanged, AddressOf optAsOfSD_Click
        AddHandler cboCustomFiscalQtyED.SelectedIndexChanged, AddressOf optAsOfED_Click
        AddHandler spinCustomFiscalYearSD.ValueChanged, AddressOf optAsOfSD_Click
        AddHandler spinCustomFiscalYearED.ValueChanged, AddressOf optAsOfED_Click

        ' Relative Move Controls
        AddHandler cboMoveDirectionSD.SelectionChangeCommitted, AddressOf optAsOfSD_Click
        AddHandler cboMoveDirectionED.SelectionChangeCommitted, AddressOf optAsOfED_Click
        AddHandler spinMoveQtySD.ValueChanged, AddressOf optAsOfSD_Click
        AddHandler spinMoveQtyED.ValueChanged, AddressOf optAsOfED_Click
        AddHandler cboMoveTypeSD.SelectionChangeCommitted, AddressOf cboMoveTypeSD_SelectionChangeCommitted
        AddHandler cboMoveTypeED.SelectionChangeCommitted, AddressOf cboMoveTypeED_SelectionChangeCommitted
        AddHandler cboMovePeriodSD.SelectionChangeCommitted, AddressOf optAsOfSD_Click
        AddHandler cboMovePeriodED.SelectionChangeCommitted, AddressOf optAsOfED_Click

        SetDefaults()
        SetEnvironment()
        DisplayDates()

    End Sub

    Private Sub SetDefaults()

        ' Start Date section
        ' 1) Relative
        optAsOfTodaySD.Checked = True
        cboCustomFiscalQtySD.SelectedIndex = 0
        cboMoveDirectionSD.SelectedIndex = 0
        spinMoveQtySD.Value = 1
        cboMoveTypeSD.SelectedIndex = 0
        cboMovePeriodSD.SelectedIndex = 0
        ' 2) Fixed
        optFixedSpecDateSD.Checked = True
        cboFixedFisCalTypeSD.SelectedIndex = 0
        cboFixedFisCalPeriodSD.SelectedIndex = 0
        cboFixedFisCalQtySD.SelectedIndex = 0
        spinFixedFisCalYearSD.Value = DateTime.Now.Year

        ' End Date section
        ' 1) Relative
        optAsOfTodayED.Checked = True
        cboCustomFiscalQtyED.SelectedIndex = 0
        cboMoveDirectionED.SelectedIndex = 0
        spinMoveQtyED.Value = 1
        cboMoveTypeED.SelectedIndex = 0
        cboMovePeriodED.SelectedIndex = 0
        ' 2) Fixed
        optFixedSpecDateED.Checked = True
        cboFixedFisCalTypeED.SelectedIndex = 0
        cboFixedFisCalPeriodED.SelectedIndex = 0
        cboFixedFisCalQtyED.SelectedIndex = 0
        spinFixedFisCalYearED.Value = DateTime.Now.Year

    End Sub

    Private Sub SetEnvironment()

        ' **********************
        '   Start Date Section
        ' **********************
        ' Check if Today
        If (SdExtendedDate.IsToday) Then

            SdCurrentType = DATE_TYPE.Fixed
            tabControlSD.SelectedTab = tabFixedSD
            cbxFixedSpecDateTdySD.Checked = True
            Return

        End If

        ' Check if Relative or Fixed
        If (SdExtendedDate.IsFixed) Then

            SdCurrentType = DATE_TYPE.Fixed
            tabControlSD.SelectedTab = tabFixedSD

            ' Populate Quantity dropdown list
            Select Case (SdExtendedDate.Anchor.PeriodUnit)
                Case PeriodUnit.Quarter
                    cboFixedFisCalQtySD.Items.Clear()
                    cboFixedFisCalQtySD.Items.AddRange(New Object() {"1", "2", "3", "4"})

                Case PeriodUnit.SemiAnnual
                    cboFixedFisCalQtySD.Items.Clear()
                    cboFixedFisCalQtySD.Items.AddRange(New Object() {"1", "2"})

                Case PeriodUnit.Year
                    cboFixedFisCalQtySD.Items.Clear()
                    cboFixedFisCalQtySD.Items.Add("1")
            End Select

            ' Check if extended date - set applicable value in each control
            If (SdExtendedDate.IsExtended) Then
                cboFixedFisCalTypeSD.SelectedItem = SdExtendedDate.Anchor.CalendarType.ToString()
                cboFixedFisCalPeriodSD.SelectedItem = SdExtendedDate.Anchor.PeriodUnit.ToString()
                cboFixedFisCalQtySD.SelectedIndex = SdExtendedDate.Anchor.Count - 1
                spinFixedFisCalYearSD.Value = SdExtendedDate.Anchor.Year
                optFixedFiscCalSD.Checked = True
            Else
                dtFixedSpecDateSD.Value = SdExtendedDate.Anchor.Date
                optFixedSpecDateSD.Checked = True
            End If

        Else

            SdCurrentType = DATE_TYPE.Relative
            tabControlSD.SelectedTab = tabRelativeSD

            ' -- "AS OF" Section --
            ' Determine if TODAY is Anchor
            If (SdExtendedDate.Anchor Is Nothing) Then
                optAsOfTodaySD.Checked = True
                ToggleAsOfSection(True, AS_OF_TOPICS.Today)

                ' Determine if ED is Anchor
            ElseIf (SdExtendedDate.Anchor Is DateAnchor.End) Then
                optAsOfEndDateSD.Checked = True
                optAsOfStartDateED.Enabled = False
                ToggleAsOfSection(True, AS_OF_TOPICS.ED_SD)

                ' Custom is expected
            Else
                optAsOfCustomSD.Checked = True
                ToggleAsOfSection(True, AS_OF_TOPICS.Custom)

                ' Expected that the Anchor is a fiscal-calendar value
                If (Not SdExtendedDate.Anchor.CalendarType = CalendarType.None) Then
                    ToggleFiscalControls(True, True)

                    ' Populate Quantity dropdown list
                    Select Case (SdExtendedDate.Anchor.PeriodUnit)
                        Case PeriodUnit.Quarter
                            cboCustomFiscalQtySD.Items.Clear()
                            cboCustomFiscalQtySD.Items.AddRange(New Object() {"1", "2", "3", "4"})
                            cboCustomFiscalPeriodSD.SelectedIndex = 0

                        Case PeriodUnit.SemiAnnual
                            cboCustomFiscalQtySD.Items.Clear()
                            cboCustomFiscalQtySD.Items.AddRange(New Object() {"1", "2"})
                            cboCustomFiscalPeriodSD.SelectedIndex = 1

                        Case PeriodUnit.Year
                            cboCustomFiscalQtySD.Items.Clear()
                            cboCustomFiscalQtySD.Items.Add("1")
                            cboCustomFiscalPeriodSD.SelectedIndex = 2
                    End Select

                    cboCustomFiscalTypeSD.SelectedItem = SdExtendedDate.Anchor.CalendarType
                    cboCustomFiscalPeriodSD.SelectedItem = SdExtendedDate.Anchor.PeriodUnit
                    cboCustomFiscalQtySD.SelectedIndex = SdExtendedDate.Anchor.Count - 1
                    spinCustomFiscalYearSD.Value = SdExtendedDate.Anchor.Year

                    ' Then it is expected that the Anchor is a date/time value
                Else
                    ToggleFiscalControls(False, True)
                    dateCustomFiscalDateSD.Value = SdExtendedDate.Anchor.Date
                End If

                ' -- "MOVE" Section --
                ' Populate Quantity dropdown list according to what is selected
                Select Case (SdExtendedDate.Offset.CalendarType)
                    Case CalendarType.Actual ' Actual
                    Case CalendarType.Calendar ' Calendar
                        cboMovePeriodSD.DataSource = periodUnitActCal.Clone()

                    Case CalendarType.Fiscal ' Fiscal
                        cboMovePeriodSD.DataSource = periodUnitFiscal.Clone()
                End Select
            End If

            cboMoveDirectionSD.SelectedItem = SdExtendedDate.Offset.Direction
            spinMoveQtySD.Value = SdExtendedDate.Offset.Count
            cboMoveTypeSD.SelectedItem = SdExtendedDate.Offset.CalendarType
            cboMovePeriodSD.SelectedItem = SdExtendedDate.Offset.PeriodUnit
        End If

        ' ********************
        '   End Date Section
        ' ********************

        ' Check if Today
        If (EdExtendedDate.IsToday) Then
            EdCurrentType = DATE_TYPE.Fixed
            tabControlED.SelectedTab = tabFixedED
            cbxFixedSpecDateTdyED.Checked = True
            Return
        End If

        ' Check if Relative or Fixed
        If (EdExtendedDate.IsFixed) Then
            EdCurrentType = DATE_TYPE.Fixed
            tabControlED.SelectedTab = tabFixedED

            ' Populate Quantity dropdown list
            Select Case (EdExtendedDate.Anchor.PeriodUnit)
                Case PeriodUnit.Quarter
                    cboFixedFisCalQtyED.Items.Clear()
                    cboFixedFisCalQtyED.Items.AddRange(New Object() {"1", "2", "3", "4"})

                Case PeriodUnit.SemiAnnual
                    cboFixedFisCalQtyED.Items.Clear()
                    cboFixedFisCalQtyED.Items.AddRange(New Object() {"1", "2"})

                Case PeriodUnit.Year
                    cboFixedFisCalQtyED.Items.Clear()
                    cboFixedFisCalQtyED.Items.Add("1")
            End Select

            ' Check if extended date
            If (EdExtendedDate.IsExtended) Then
                cboFixedFisCalTypeED.SelectedItem = EdExtendedDate.Anchor.CalendarType
                cboFixedFisCalPeriodED.SelectedItem = EdExtendedDate.Anchor.PeriodUnit
                cboFixedFisCalQtyED.SelectedIndex = EdExtendedDate.Anchor.Count - 1
                spinFixedFisCalYearED.Value = EdExtendedDate.Anchor.Year
                optFixedFiscCalED.Checked = True
            Else
                dtFixedSpecDateED.Value = EdExtendedDate.Anchor.Date
                optFixedSpecDateED.Checked = True
            End If

        Else  ' Relative
            tabControlED.SelectedTab = tabRelativeED
            EdCurrentType = DATE_TYPE.Relative

            ' -- "AS OF" Section --
            ' Determine if TODAY is Anchor
            If (EdExtendedDate.Anchor Is Nothing) Then
                optAsOfTodayED.Checked = True
                ToggleAsOfSection(False, AS_OF_TOPICS.Today)

                ' Determine if SD is Anchor
            ElseIf (EdExtendedDate.Anchor Is DateAnchor.Start) Then
                optAsOfStartDateED.Checked = True
                optAsOfEndDateSD.Enabled = False
                ToggleAsOfSection(False, AS_OF_TOPICS.ED_SD)

                ' Custom is expected
            Else
                optAsOfCustomED.Checked = True
                ToggleAsOfSection(False, AS_OF_TOPICS.Custom)

                If (Not EdExtendedDate.Anchor.CalendarType = CalendarType.None) Then
                    ToggleFiscalControls(True, False)

                    ' Populate Quantity dropdown list
                    Select Case (EdExtendedDate.Anchor.PeriodUnit)
                        Case PeriodUnit.Quarter
                            cboCustomFiscalQtyED.Items.Clear()
                            cboCustomFiscalQtyED.Items.AddRange(New Object() {"1", "2", "3", "4"})

                        Case PeriodUnit.SemiAnnual
                            cboCustomFiscalQtyED.Items.Clear()
                            cboCustomFiscalQtyED.Items.AddRange(New Object() {"1", "2"})

                        Case PeriodUnit.Year
                            cboCustomFiscalQtyED.Items.Clear()
                            cboCustomFiscalQtyED.Items.Add("1")
                    End Select

                    cboCustomFiscalTypeED.SelectedItem = EdExtendedDate.Anchor.CalendarType
                    cboCustomFiscalPeriodED.SelectedItem = EdExtendedDate.Anchor.PeriodUnit
                    cboCustomFiscalQtyED.SelectedIndex = EdExtendedDate.Anchor.Count - 1
                    spinCustomFiscalYearED.Value = EdExtendedDate.Anchor.Year

                Else
                    ToggleFiscalControls(False, False)
                    dateCustomFiscalDateED.Value = EdExtendedDate.Anchor.Date
                End If

            End If

            ' -- "MOVE" Section --
            ' Populate Quantity dropdown list according to what is selected
            Select Case (EdExtendedDate.Offset.CalendarType)
                Case CalendarType.Actual ' Actual
                Case CalendarType.Calendar ' Calendar
                    cboMovePeriodED.DataSource = periodUnitActCal.Clone()

                Case CalendarType.Fiscal ' Fiscal
                    cboMovePeriodED.DataSource = periodUnitFiscal.Clone()
            End Select

            cboMoveDirectionED.SelectedItem = EdExtendedDate.Offset.Direction
            spinMoveQtyED.Value = EdExtendedDate.Offset.Count
            cboMoveTypeED.SelectedItem = EdExtendedDate.Offset.CalendarType
            cboMovePeriodED.SelectedItem = EdExtendedDate.Offset.PeriodUnit

        End If

    End Sub

    Private Sub SaveEnvironment()

        Dim selPeriodUnit As PeriodUnit

        ' ********************
        '  Start Date Section
        ' ********************
        If (SdCurrentType = DATE_TYPE.Fixed) Then       ' ** Fixed Date **
            SdDateOffset = Nothing                      ' Not applicable, therefore, set to null

            If (optFixedSpecDateSD.Checked) Then        ' "Specific Date" selected

                If (cbxFixedSpecDateTdySD.Checked) Then ' Today
                    SdDateAnchor = Nothing
                Else        ' Date-Time
                    SdDateAnchor = New DateAnchor(dtFixedSpecDateSD.Value)
                End If

            Else                                        ' "Fisc/Calendar" selected
                SdDateAnchor = New DateAnchor( _
                    calTypeCalFisc(cboFixedFisCalTypeSD.SelectedIndex), _
                    periodUnitFiscal(cboFixedFisCalPeriodSD.SelectedIndex), _
                    Convert.ToInt16(cboFixedFisCalQtySD.SelectedIndex + 1), _
                    Convert.ToInt16(spinFixedFisCalYearSD.Value))
            End If

        Else                                            ' ** Relative Date **

            ' Update Anchor attributes
            If (optAsOfTodaySD.Checked) Then            ' Today
                SdDateAnchor = Nothing

            ElseIf (optAsOfEndDateSD.Checked) Then      ' End Date
                SdDateAnchor = DateAnchor.End

            Else                                        ' Custom

                If (chboxCustomFiscalSD.Checked) Then   ' Fiscal-Calendar

                    SdDateAnchor = New DateAnchor( _
                        calTypeCalFisc(cboCustomFiscalTypeSD.SelectedIndex), _
                        periodUnitFiscal(cboCustomFiscalPeriodSD.SelectedIndex), _
                        Convert.ToInt16(cboCustomFiscalQtySD.SelectedIndex + 1), _
                        Convert.ToInt16(spinCustomFiscalYearSD.Value))

                Else                                    ' DateTime
                    SdDateAnchor = New DateAnchor(dateCustomFiscalDateSD.Value)
                End If

            End If

            ' Update Offset attributes
            If (2 = cboMoveTypeSD.SelectedIndex) Then   ' denotes Fiscal Type
                selPeriodUnit = periodUnitFiscal(cboMovePeriodSD.SelectedIndex)
            Else                                        ' denotes Calendar or Actual type
                selPeriodUnit = periodUnitActCal(cboMovePeriodSD.SelectedIndex)
            End If

            SdDateOffset = New DateOffset( _
                moveDirection(cboMoveDirectionSD.SelectedIndex), _
                Convert.ToInt16(spinMoveQtySD.Value), _
                calTypeAll(cboMoveTypeSD.SelectedIndex), _
                selPeriodUnit)

        End If

        ' ********************
        '   End Date Section
        ' ********************
        If (EdCurrentType = DATE_TYPE.Fixed) Then       ' ** Fixed Date **

            EdDateOffset = Nothing                         ' Not applicable, therefore, set to null

            If (optFixedSpecDateED.Checked) Then        ' "Specific Date" selected

                If (cbxFixedSpecDateTdyED.Checked) Then ' Today
                    EdDateAnchor = Nothing

                Else                                    ' Date-Time
                    EdDateAnchor = New DateAnchor(dtFixedSpecDateED.Value)
                End If

            Else                                        ' "Fisc/Calendar" selected
                EdDateAnchor = New DateAnchor( _
                     calTypeCalFisc(cboFixedFisCalTypeED.SelectedIndex), _
                     periodUnitFiscal(cboFixedFisCalPeriodED.SelectedIndex), _
                     Convert.ToInt16(cboFixedFisCalQtyED.SelectedIndex + 1), _
                     Convert.ToInt16(spinFixedFisCalYearED.Value))
            End If

        Else   ' ** Relative Date **

            ' Update Anchor attributes
            If (optAsOfTodayED.Checked) Then                ' Today
                EdDateAnchor = Nothing

            ElseIf (optAsOfStartDateED.Checked) Then        ' End Date
                EdDateAnchor = DateAnchor.End

            Else                                            ' Custom

                If (chboxCustomFiscalED.Checked) Then       ' Fiscal-Calendar

                    EdDateAnchor = New DateAnchor( _
                        calTypeCalFisc(cboCustomFiscalTypeED.SelectedIndex), _
                        periodUnitFiscal(cboCustomFiscalPeriodED.SelectedIndex), _
                        Convert.ToInt16(cboCustomFiscalQtyED.SelectedIndex + 1), _
                        Convert.ToInt16(spinCustomFiscalYearED.Value))

                Else                                        ' DateTime
                    EdDateAnchor = New DateAnchor(dateCustomFiscalDateSD.Value)
                End If

                ' Update Offset attributes
                If (2 = cboMoveTypeED.SelectedIndex) Then ' denotes Fiscal Type
                    selPeriodUnit = periodUnitFiscal(cboMovePeriodED.SelectedIndex)
                Else  ' denotes Calendar or Actual type
                    selPeriodUnit = periodUnitActCal(cboMovePeriodED.SelectedIndex)
                End If

                EdDateOffset = New DateOffset( _
                    moveDirection(cboMoveDirectionED.SelectedIndex), _
                    Convert.ToInt16(spinMoveQtyED.Value), _
                    calTypeAll(cboMoveTypeED.SelectedIndex), _
                    selPeriodUnit)
            End If
        End If

        ' Assign displayed dates to private variables
        strDisplayedDateSD = lblDisplaySD.Text
        strDisplayedDateED = lblDisplayED.Text

        SdExtendedDate = New ExtendedDate(SdDateAnchor, SdDateOffset)
        EdExtendedDate = New ExtendedDate(EdDateAnchor, EdDateOffset)

    End Sub

    Private Sub DisplayDates()

        DisplayDate(True)
        DisplayDate(False)

    End Sub

    Private Sub DisplayDate(ByVal isStartDate As Boolean)

        Dim strSD As StringBuilder = New StringBuilder
        Dim strED As StringBuilder = New StringBuilder

        ' ******************
        ' Start Date Section
        ' ******************
        If (isStartDate) Then

            If (SdCurrentType = DATE_TYPE.Fixed) Then       ' ** Fixed Date **

                If (optFixedSpecDateSD.Checked) Then        ' "Specific Date" selected

                    If (cbxFixedSpecDateTdySD.Checked) Then ' Today
                        strSD.Append("Today")
                    Else                                    ' Date-Time
                        strSD.Append(dtFixedSpecDateSD.Value.ToShortDateString().ToString())
                    End If

                Else         ' "Fisc/Calendar" selected
                    strSD.Append(cboFixedFisCalTypeSD.Text.ToString().Substring(0, 1))
                    strSD.Append(cboFixedFisCalPeriodSD.Text.ToString().Substring(0, 1))
                    strSD.Append(cboFixedFisCalQtySD.Text.ToString())
                    strSD.Append(spinFixedFisCalYearSD.Value.ToString())
                End If

            Else          ' ** Relative Date **
                If (optAsOfTodaySD.Checked) Then            ' Today
                    strSD.Append("")

                ElseIf (optAsOfEndDateSD.Checked) Then      ' End Date
                    strSD.Append("ED")

                Else    ' Custom
                    If (chboxCustomFiscalSD.Checked) Then   ' Fiscal-Calendar
                        strSD.Append(cboCustomFiscalTypeSD.Text.ToString().Substring(0, 1))
                        strSD.Append(cboCustomFiscalPeriodSD.Text.ToString().Substring(0, 1))
                        strSD.Append(cboCustomFiscalQtySD.Text.ToString())
                        strSD.Append(spinCustomFiscalYearSD.Value.ToString())

                    Else                                    ' DateTime
                        strSD.Append(dateCustomFiscalDateSD.Value.ToShortDateString().ToString())
                    End If
                End If

                ' Offset attributes
                If (cboMoveDirectionSD.SelectedIndex = 0) Then ' Negative
                    strSD.Append("-")
                Else
                    strSD.Append("+")
                End If

                strSD.Append(spinMoveQtySD.Value)
                strSD.Append(cboMoveTypeSD.Text.ToString().Substring(0, 1))
                strSD.Append(cboMovePeriodSD.Text.ToString().Substring(0, 1))

            End If

            ' Output to SD display box and anchor to right position
            lblDisplaySD.Text = strSD.ToString()
            AdjustDisplayArea(lblDisplaySD)

            ' ****************
            ' End Date Section
            ' ****************
        Else

            If (EdCurrentType = DATE_TYPE.Fixed) Then ' ** Fixed Date **

                If (optFixedSpecDateED.Checked) Then ' "Specific Date" selected

                    If (cbxFixedSpecDateTdyED.Checked) Then ' Today
                        strED.Append("Today")

                    Else        ' Date-Time
                        strED.Append(dtFixedSpecDateED.Value.ToShortDateString().ToString())
                    End If

                Else         ' "Fisc/Calendar" selected
                    strED.Append(cboFixedFisCalTypeED.Text.ToString().Substring(0, 1))
                    strED.Append(cboFixedFisCalPeriodED.Text.ToString().Substring(0, 1))
                    strED.Append(cboFixedFisCalQtyED.Text.ToString())
                    strED.Append(spinFixedFisCalYearED.Value.ToString())
                End If

            Else          ' ** Relative Date **

                If (optAsOfTodayED.Checked) Then            ' Today
                    strED.Append("")

                ElseIf (optAsOfStartDateED.Checked) Then    ' Start Date
                    strED.Append("SD")

                Else                                        ' Custom
                    If (chboxCustomFiscalED.Checked) Then ' Fiscal-Calendar
                        strED.Append(cboCustomFiscalTypeED.Text.ToString().Substring(0, 1))
                        strED.Append(cboCustomFiscalPeriodED.Text.ToString().Substring(0, 1))
                        strED.Append(cboCustomFiscalQtyED.Text.ToString())
                        strED.Append(spinCustomFiscalYearED.Value.ToString())

                    Else        ' DateTime
                        strED.Append(dateCustomFiscalDateED.Text)
                    End If
                End If

                ' Offset attributes
                If (cboMoveDirectionED.SelectedIndex = 0) Then ' Negative
                    strED.Append("-")
                Else
                    strED.Append("+")
                End If

                strED.Append(spinMoveQtyED.Value)
                strED.Append(cboMoveTypeED.Text.ToString().Substring(0, 1))
                strED.Append(cboMovePeriodED.Text.ToString().Substring(0, 1))

            End If

            ' Output to SD display box and anchor to right position
            lblDisplayED.Text = strED.ToString()
            AdjustDisplayArea(lblDisplayED)

        End If
    End Sub

    Private Sub AdjustDisplayArea(ByVal DisplayCtrl As Label)

        Const DISTANCE_FROM_RIGHT_SIDE As Integer = 30
        Dim xPos As Integer = 0

        xPos = Me.Size.Width - DisplayCtrl.Width - DISTANCE_FROM_RIGHT_SIDE
        DisplayCtrl.Location = New Point(xPos, DisplayCtrl.Location.Y)

    End Sub

    Private Sub ToggleFiscalControls(ByVal isFiscalOn As Boolean, ByVal isStartDate As Boolean)

        If (isStartDate) Then
            cboCustomFiscalTypeSD.Visible = isFiscalOn
            cboCustomFiscalPeriodSD.Visible = isFiscalOn
            cboCustomFiscalQtySD.Visible = isFiscalOn
            spinCustomFiscalYearSD.Visible = isFiscalOn
            dateCustomFiscalDateSD.Visible = Not isFiscalOn
            chboxCustomFiscalSD.Checked = isFiscalOn
        Else
            cboCustomFiscalTypeED.Visible = isFiscalOn
            cboCustomFiscalPeriodED.Visible = isFiscalOn
            cboCustomFiscalQtyED.Visible = isFiscalOn
            spinCustomFiscalYearED.Visible = isFiscalOn
            dateCustomFiscalDateED.Visible = Not isFiscalOn
            chboxCustomFiscalED.Checked = isFiscalOn
        End If

    End Sub

    Private Sub ToggleAsOfSection(ByVal isStartDate As Boolean, ByVal topic As AS_OF_TOPICS)

        If (isStartDate) Then

            Select Case (topic)

                Case AS_OF_TOPICS.Today
                    lblTodayLineSD.Visible = True
                    lblEndDateLineSD.Visible = False
                    lblCustomLineSD.Visible = False

                    lblTodayTextSD.Visible = True
                    lblEndDateTextSD.Visible = False
                    cboCustomFiscalTypeSD.Visible = False
                    cboCustomFiscalPeriodSD.Visible = False
                    cboCustomFiscalQtySD.Visible = False
                    spinCustomFiscalYearSD.Visible = False
                    dateCustomFiscalDateSD.Visible = False
                    chboxCustomFiscalSD.Visible = False

                    ' Change width of outline label
                    lblAsOfBoxSD.Width = 76

                Case AS_OF_TOPICS.ED_SD
                    lblTodayLineSD.Visible = False
                    lblEndDateLineSD.Visible = True
                    lblCustomLineSD.Visible = False

                    lblTodayTextSD.Visible = False
                    lblEndDateTextSD.Visible = True
                    cboCustomFiscalTypeSD.Visible = False
                    cboCustomFiscalPeriodSD.Visible = False
                    cboCustomFiscalQtySD.Visible = False
                    spinCustomFiscalYearSD.Visible = False
                    dateCustomFiscalDateSD.Visible = False
                    chboxCustomFiscalSD.Visible = False

                    ' Change width of outline label
                    lblAsOfBoxSD.Width = 196

                Case AS_OF_TOPICS.Custom
                    lblTodayLineSD.Visible = False
                    lblEndDateLineSD.Visible = False
                    lblCustomLineSD.Visible = True

                    lblTodayTextSD.Visible = False
                    lblEndDateTextSD.Visible = False
                    chboxCustomFiscalSD.Visible = True

                    If (chboxCustomFiscalSD.Checked) Then
                        cboCustomFiscalTypeSD.Visible = True
                        cboCustomFiscalPeriodSD.Visible = True
                        cboCustomFiscalQtySD.Visible = True
                        spinCustomFiscalYearSD.Visible = True
                        dateCustomFiscalDateSD.Visible = False
                    Else
                        cboCustomFiscalTypeSD.Visible = False
                        cboCustomFiscalPeriodSD.Visible = False
                        cboCustomFiscalQtySD.Visible = False
                        spinCustomFiscalYearSD.Visible = False
                        dateCustomFiscalDateSD.Visible = True
                    End If

                    ' Change width of outline label
                    lblAsOfBoxSD.Width = 333

            End Select

        Else

            Select Case (topic)

                Case AS_OF_TOPICS.Today
                    lblTodayLineED.Visible = True
                    lblStartDateLineED.Visible = False
                    lblCustomLineED.Visible = False

                    lblTodayTextED.Visible = True
                    lblStartDateTextED.Visible = False
                    cboCustomFiscalTypeED.Visible = False
                    cboCustomFiscalPeriodED.Visible = False
                    cboCustomFiscalQtyED.Visible = False
                    spinCustomFiscalYearED.Visible = False
                    dateCustomFiscalDateED.Visible = False
                    chboxCustomFiscalED.Visible = False

                    ' Change width of outline label
                    lblAsOfBoxED.Width = 76

                Case AS_OF_TOPICS.ED_SD
                    lblTodayLineED.Visible = False
                    lblStartDateLineED.Visible = True
                    lblCustomLineED.Visible = False

                    lblTodayTextED.Visible = False
                    lblStartDateTextED.Visible = True
                    cboCustomFiscalTypeED.Visible = False
                    cboCustomFiscalPeriodED.Visible = False
                    cboCustomFiscalQtyED.Visible = False
                    spinCustomFiscalYearED.Visible = False
                    dateCustomFiscalDateED.Visible = False
                    chboxCustomFiscalED.Visible = False

                    ' Change width of outline label
                    lblAsOfBoxED.Width = 196

                Case AS_OF_TOPICS.Custom
                    lblTodayLineED.Visible = False
                    lblStartDateLineED.Visible = False
                    lblCustomLineED.Visible = True

                    lblTodayTextED.Visible = False
                    lblStartDateTextED.Visible = False
                    chboxCustomFiscalED.Visible = True

                    If (chboxCustomFiscalED.Checked) Then
                        cboCustomFiscalTypeED.Visible = True
                        cboCustomFiscalPeriodED.Visible = True
                        cboCustomFiscalQtyED.Visible = True
                        spinCustomFiscalYearED.Visible = True
                        dateCustomFiscalDateED.Visible = False
                    Else
                        cboCustomFiscalTypeED.Visible = False
                        cboCustomFiscalPeriodED.Visible = False
                        cboCustomFiscalQtyED.Visible = False
                        spinCustomFiscalYearED.Visible = False
                        dateCustomFiscalDateED.Visible = True
                    End If

                    ' Change width of outline label
                    lblAsOfBoxED.Width = 333

            End Select
        End If
    End Sub

    Private Sub optAsOfSD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (optAsOfTodaySD.Checked) Then
            SdDateAnchor = Nothing
            ToggleAsOfSection(True, AS_OF_TOPICS.Today)
            optAsOfStartDateED.Enabled = True
        ElseIf (optAsOfEndDateSD.Checked) Then
            SdDateAnchor = New DateAnchor("ED")
            ToggleAsOfSection(True, AS_OF_TOPICS.ED_SD)
            optAsOfStartDateED.Enabled = False
        ElseIf (optAsOfCustomSD.Checked) Then
            ToggleAsOfSection(True, AS_OF_TOPICS.Custom)
            optAsOfStartDateED.Enabled = True
        End If

        DisplayDate(True)

    End Sub

    Private Sub optAsOfED_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (optAsOfTodayED.Checked) Then
            EdDateAnchor = Nothing
            ToggleAsOfSection(False, AS_OF_TOPICS.Today)
            optAsOfEndDateSD.Enabled = True
        ElseIf (optAsOfStartDateED.Checked) Then
            EdDateAnchor = New DateAnchor("SD")
            ToggleAsOfSection(False, AS_OF_TOPICS.ED_SD)
            optAsOfEndDateSD.Enabled = False
        ElseIf (optAsOfCustomED.Checked) Then
            ToggleAsOfSection(False, AS_OF_TOPICS.Custom)
            optAsOfEndDateSD.Enabled = True
        End If

        DisplayDate(False)

    End Sub

    Private Sub chboxCustomFiscalED_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ToggleFiscalControls(chboxCustomFiscalED.Checked, False)
        DisplayDate(False)

    End Sub

    Private Sub chboxCustomFiscalSD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        ToggleFiscalControls(chboxCustomFiscalSD.Checked, True)
        DisplayDate(True)

    End Sub

    Private Sub optAsOfSD_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        DisplayDate(True)

    End Sub

    Private Sub optAsOfED_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        DisplayDate(False)

    End Sub

    Private Sub FixedSD_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        DisplayDate(True)

    End Sub

    Private Sub FixedED_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        DisplayDate(False)

    End Sub

    Private Sub cboMoveTypeSD_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)

        ' Populate Quantity dropdown list according to what is selected
        Select Case (cboMoveTypeSD.SelectedIndex)
            Case 0 ' Actual
            Case 1 ' Calendar
                cboMovePeriodSD.DataSource = periodUnitActCal.Clone()

            Case 2 ' Fiscal
                cboMovePeriodSD.DataSource = periodUnitFiscal.Clone()
        End Select

        cboMovePeriodSD.SelectedIndex = 0

        DisplayDate(True)

    End Sub

    Private Sub cboMoveTypeED_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)

        ' Populate Quantity dropdown list according to what is selected
        Select Case (cboMoveTypeED.SelectedIndex)
            Case 0 ' Actual
            Case 1 ' Calendar
                cboMovePeriodED.DataSource = periodUnitActCal.Clone()

            Case 2 ' Fiscal
                cboMovePeriodED.SelectedIndex = 0
                cboMovePeriodED.DataSource = periodUnitFiscal.Clone()
        End Select

        cboMovePeriodED.SelectedIndex = 0

        DisplayDate(False)

    End Sub

    Private Sub cboCustomFiscalPeriodSD_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomFiscalPeriodSD.SelectionChangeCommitted

        cboCustomFiscalQtySD.Items.Clear()

        ' Populate Quantity dropdown list according to what is selected
        Select Case (cboCustomFiscalPeriodSD.SelectedIndex)
            Case 0 ' Quarterly
                cboCustomFiscalQtySD.Items.AddRange(New Object() {"1", "2", "3", "4"})

            Case 1 ' Semi-Annually
                cboCustomFiscalQtySD.Items.AddRange(New Object() {"1", "2"})

            Case 2 ' Yearly
                cboCustomFiscalQtySD.Items.Add("1")
        End Select

        cboCustomFiscalQtySD.SelectedIndex = 0
        DisplayDate(True)

    End Sub

    Private Sub cboCustomFiscalPeriodED_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomFiscalPeriodED.SelectionChangeCommitted

        cboCustomFiscalQtyED.Items.Clear()

        ' Populate Quantity dropdown list according to what is selected
        Select Case (cboCustomFiscalPeriodED.SelectedIndex)
            Case 0 ' Quarterly
                cboCustomFiscalQtyED.Items.AddRange(New Object() {"1", "2", "3", "4"})

            Case 1 ' Semi-Annually
                cboCustomFiscalQtyED.Items.AddRange(New Object() {"1", "2"})

            Case 2 ' Yearly
                cboCustomFiscalQtyED.Items.Add("1")
        End Select

        cboCustomFiscalQtyED.SelectedIndex = 0
        DisplayDate(False)

    End Sub

    Private Sub optFixedSD_Click(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles optFixedSpecDateSD.CheckedChanged, optFixedFiscCalSD.CheckedChanged

        If (optFixedSpecDateSD.Checked) Then
            toggleFixedGroups(True, True)
        Else
            toggleFixedGroups(True, False)
        End If

        DisplayDate(True)

    End Sub

    Private Sub optFixedED_Click(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles optFixedSpecDateED.CheckedChanged, optFixedFiscCalED.CheckedChanged

        If (optFixedSpecDateED.Checked) Then
            toggleFixedGroups(False, True)
        Else
            toggleFixedGroups(False, False)
        End If

        DisplayDate(False)

    End Sub

    Private Sub toggleFixedGroups(ByVal isStartDate As Boolean, ByVal isSpecificDate As Boolean)

        If (isStartDate) Then
            optFixedSpecDateSD.Checked = isSpecificDate
            optFixedFiscCalSD.Checked = Not isSpecificDate
            dtFixedSpecDateSD.Enabled = isSpecificDate
            cbxFixedSpecDateTdySD.Enabled = isSpecificDate
            cboFixedFisCalTypeSD.Enabled = Not isSpecificDate
            cboFixedFisCalPeriodSD.Enabled = Not isSpecificDate
            cboFixedFisCalQtySD.Enabled = Not isSpecificDate
            spinFixedFisCalYearSD.Enabled = Not isSpecificDate

        Else ' End Date
            optFixedSpecDateED.Checked = isSpecificDate
            optFixedFiscCalED.Checked = Not isSpecificDate
            dtFixedSpecDateED.Enabled = isSpecificDate
            cbxFixedSpecDateTdyED.Enabled = isSpecificDate
            cboFixedFisCalTypeED.Enabled = Not isSpecificDate
            cboFixedFisCalPeriodED.Enabled = Not isSpecificDate
            cboFixedFisCalQtyED.Enabled = Not isSpecificDate
            spinFixedFisCalYearED.Enabled = Not isSpecificDate
        End If

    End Sub

    Private Sub tabSD_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If (tabControlSD.SelectedTab Is tabFixedSD) Then
            SdCurrentType = DATE_TYPE.Fixed

            If (optAsOfStartDateED.Enabled = False) Then
                optAsOfStartDateED.Enabled = True
            End If
        Else
            SdCurrentType = DATE_TYPE.Relative

            If (optAsOfStartDateED.Checked) Then
                optAsOfTodaySD.Checked = True ' Set to Today.
            End If

            If (optAsOfEndDateSD.Checked) Then
                optAsOfStartDateED.Enabled = False ' Disable SD in End Date Section
            End If
        End If

        DisplayDate(True)

    End Sub

    Private Sub tabED_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If (tabControlED.SelectedTab Is tabFixedED) Then

            EdCurrentType = DATE_TYPE.Fixed

            If (optAsOfEndDateSD.Enabled = False) Then
                optAsOfEndDateSD.Enabled = True
            End If
        Else

            EdCurrentType = DATE_TYPE.Relative

            If (optAsOfEndDateSD.Checked) Then
                optAsOfTodayED.Checked = True ' Set to Today.
            End If

            If (optAsOfStartDateED.Checked) Then
                optAsOfEndDateSD.Enabled = False ' Disable ED in Start Date Section
            End If

        End If

        DisplayDate(False)

    End Sub

    Private Sub FixedPeriodicitySD_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        cboFixedFisCalQtySD.Items.Clear()

        ' Populate quantity combobox according to selected item
        Select Case (cboFixedFisCalPeriodSD.SelectedIndex)
            Case 0 ' Quarterly
                cboFixedFisCalQtySD.Items.AddRange(New Object() {"1", "2", "3", "4"})

            Case 1 ' Semi-Annually
                cboFixedFisCalQtySD.Items.AddRange(New Object() {"1", "2"})

            Case 2 ' Yearly
                cboFixedFisCalQtySD.Items.Add("1")
        End Select

        cboFixedFisCalQtySD.SelectedIndex = 0
        DisplayDate(True)

    End Sub

    Private Sub FixedPeriodicityED_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        cboFixedFisCalQtyED.Items.Clear()

        ' Populate quantity combobox according to selected item
        Select Case (cboFixedFisCalPeriodED.SelectedIndex)
            Case 0 ' Quarterly
                cboFixedFisCalQtyED.Items.AddRange(New Object() {"1", "2", "3", "4"})

            Case 1 ' Semi-Annually
                cboFixedFisCalQtyED.Items.AddRange(New Object() {"1", "2"})

            Case 2 ' Yearly
                cboFixedFisCalQtyED.Items.Add("1")
        End Select

        cboFixedFisCalQtyED.SelectedIndex = 0
        DisplayDate(False)

    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click

        SaveEnvironment()
        gIsFormSaved = True
        Me.Close()

    End Sub

    Private Sub frmSetDates_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        If (Not gIsFormSaved) Then

            Dim result As DialogResult

            result = MessageBox.Show("Closing this form will result in a loss of all changes. Do you wish to proceed?", _
                "Last Chance", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If (result = DialogResult.Yes) Then
                Me.Close()
            Else
                e.Cancel = True
            End If

        End If

    End Sub

    Private Sub cbxFixedSpecDateTdyED_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (cbxFixedSpecDateTdyED.Checked) Then
            dtFixedSpecDateED.Enabled = False
        Else
            dtFixedSpecDateED.Enabled = True
        End If

    End Sub

    Private Sub cbxFixedSpecDateTdySD_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (cbxFixedSpecDateTdySD.Checked) Then
            dtFixedSpecDateSD.Enabled = False
        Else
            dtFixedSpecDateSD.Enabled = True
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        gIsFormSaved = False

    End Sub

#Region "Public Functions"

    Public Function GetExtendedDate(ByVal isStartDate As Boolean) As ExtendedDate

        If (isStartDate) Then
            Return SdExtendedDate
        Else
            Return EdExtendedDate
        End If

    End Function

    Public Function GetDisplayedDate(ByVal isStartDate As Boolean) As String

        If (isStartDate) Then
            Return strDisplayedDateSD
        Else
            Return strDisplayedDateED
        End If

    End Function

    Public Function IsSaved() As Boolean

        Return gIsFormSaved

    End Function

#End Region

End Class
