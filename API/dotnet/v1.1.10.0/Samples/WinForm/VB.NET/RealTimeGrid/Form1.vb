'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************
Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Public Class Form1
    Inherits System.Windows.Forms.Form
#Region "Declarations - Private instance properties"
    'The status bar panel that will display the status messages returned from MarketDataAdapter
    Private _sbpAdapterStatus As StatusBarPanel

    'The list of fields will be populated and validated after we have started the MarketDataAdapter
    Private _fieldList() As Field
    ' DataSet and DataTable Used to bind the data returned from MarketDataAdpater.ReplyEvent 
    ' to the DataGrid (grdMonitor)
    Private _realTimeDataSet As DataSet
    Private _realTimeDataTable As DataTable

    ' This correlation id will be incremented for each row added to the grid.
    ' The current values will be passed as a "State" variable to the Request object 
    ' that is passed to the MarketDataAdapter.SendRequest method. This "State" variable
    ' will be returned by MarketDataAdapter.ReplyEvent in the Reply object. Logic
    ' in the ReplyEvent handler (see MarketDataAdapter_ReplyEvent below) will
    ' map this value (via the Hashtable _htRows) back to a particular row on the grid
    ' which will then be updated with the new values.
    Private _blpDataCorrelationId As Int32
    Private _htRows As Hashtable
#End Region

#Region "Windows Form Designer generated code, Constructor"

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        _htRows = New Hashtable

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            MarketDataAdapter.Shutdown()
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grdMonitor As System.Windows.Forms.DataGrid
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.grdMonitor = New System.Windows.Forms.DataGrid
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        CType(Me.grdMonitor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdMonitor
        '
        Me.grdMonitor.AllowDrop = True
        Me.grdMonitor.AlternatingBackColor = System.Drawing.Color.LightGray
        Me.grdMonitor.BackColor = System.Drawing.Color.Gainsboro
        Me.grdMonitor.BackgroundColor = System.Drawing.Color.Silver
        Me.grdMonitor.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdMonitor.CaptionBackColor = System.Drawing.Color.LightSteelBlue
        Me.grdMonitor.CaptionForeColor = System.Drawing.Color.MidnightBlue
        Me.grdMonitor.DataMember = ""
        Me.grdMonitor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdMonitor.FlatMode = True
        Me.grdMonitor.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.grdMonitor.ForeColor = System.Drawing.Color.Black
        Me.grdMonitor.GridLineColor = System.Drawing.Color.DimGray
        Me.grdMonitor.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None
        Me.grdMonitor.HeaderBackColor = System.Drawing.Color.MidnightBlue
        Me.grdMonitor.HeaderFont = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.grdMonitor.HeaderForeColor = System.Drawing.Color.White
        Me.grdMonitor.LinkColor = System.Drawing.Color.MidnightBlue
        Me.grdMonitor.Location = New System.Drawing.Point(0, 0)
        Me.grdMonitor.Name = "grdMonitor"
        Me.grdMonitor.ParentRowsBackColor = System.Drawing.Color.DarkGray
        Me.grdMonitor.ParentRowsForeColor = System.Drawing.Color.Black
        Me.grdMonitor.SelectionBackColor = System.Drawing.Color.CadetBlue
        Me.grdMonitor.SelectionForeColor = System.Drawing.Color.White
        Me.grdMonitor.Size = New System.Drawing.Size(832, 358)
        Me.grdMonitor.TabIndex = 0
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 336)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(832, 22)
        Me.StatusBar1.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(832, 358)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.grdMonitor)
        Me.Name = "Form1"
        Me.Text = "Realtime Grid Example: Drop a list (for example: ""Most<GO>"") on grid"
        CType(Me.grdMonitor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Form Load, Initialization"
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Create the StatusBarPanel for displaying adapter status
        _sbpAdapterStatus = New StatusBarPanel
        _sbpAdapterStatus.BorderStyle = StatusBarPanelBorderStyle.Sunken
        _sbpAdapterStatus.Text = "MarketDataAdapter uninitialized"
        _sbpAdapterStatus.AutoSize = StatusBarPanelAutoSize.Contents

        ' Add the newly created panel to the StatusBar
        StatusBar1.Panels.Add(_sbpAdapterStatus)

        'Create the DataTable that will contain the data elements
        ' returned from the MarketdDataAdapter.
        _realTimeDataTable = New DataTable("Realtime")
        _realTimeDataTable.DefaultView.ApplyDefaultSort = True

        ' The first column in the table (and hence the grid) will contain 
        ' the Security identifier.
        _realTimeDataTable.Columns.Add("Security")

        ' The grid must be bound to a DataSet (that will contain a single DataTable)
        _realTimeDataSet = New DataSet("DSMASTER")
        ' Add our DataTable to the DataSet
        _realTimeDataSet.Tables.Add(_realTimeDataTable)

        ' Bind the grid's DataSource to our DataSet 
        grdMonitor.DataSource = _realTimeDataSet
        ' Select the DataTable that we created above for binding to the grid.
        grdMonitor.DataMember = "Realtime"

        'subscribe for status event
        AddHandler MarketDataAdapter.StatusEvent, AddressOf MarketDataAdapter_StatusEvent

        ' Now startup the MarketDataAdapter.
        ' This will cause several things to happen:
        ' - Bloomberg configuration information will be acquired from the 
        '   environment (Windows Registry, config files, the Bloomberg terminal, etc)
        ' - A connection to the local bbcomm will be established.
        ' - BBCOMM protocol handshaking will be initiated.
        ' - Adapter resources will be allocated.
        Try
            ' The TimerSyncResponseInterceptor ensures that the status and reply
            ' event handlers are called on the user interface threads at a
            ' specified time interval. We have set it at 1 second (1000 milliseconds) 
            MarketDataAdapter.Startup(New TimerSyncResponseInterceptor(1000))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.Startup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
        End Try

        'subscribe for reply event
        AddHandler MarketDataAdapter.ReplyEvent, AddressOf MarketDataAdapter_ReplyEvent

        Dim ftbl As FieldTable
        ftbl = MarketDataAdapter.FieldTable

        ' After MarketDataAdapter.Startup has completed successfully, appropriate metadata 
        ' (bbfields.tbl, etc, ) information will have been loaded, so we can validate 
        ' the fields that we want to add to our fieldList against the metatdata.
        ' MarketDataAdapter.CreateValidField will throw an exception if you 
        ' try to add an invalid field.
        Try
            _fieldList = New Field() { _
                ftbl("Last Price"), _
                ftbl("Volume"), _
                ftbl("Bid"), _
                ftbl("Ask"), _
                ftbl("Time"), _
                ftbl("High Tdy"), _
                ftbl("Low Tdy"), _
                ftbl("Open Tdy")}

        Catch ex As Exception
            MessageBox.Show(ex.Message, "MarketDataAdapter.CreateValidField", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Close()
        End Try

        ' Add a column to the DataTable (and hence the grid) for each Field in the list above.
        ' We could do add some nice UI touches here, such as adjusting the size of 
        ' each column to the size of the Field Description...
        For Each fld As Field In _fieldList
            _realTimeDataTable.Columns.Add(New DataColumn(fld.Description, System.Type.GetType("System.String")))
        Next fld

    End Sub
#End Region

#Region "Drag/Drop  event handlers"
    ' This handler will be called as the cursor enters our DataGrid's window.
    ' It gives us the opportunit to indicate the format of the data 
    ' we can handle. In this case we want to consume text data only.
    Private Sub grdMonitor_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles grdMonitor.DragEnter
        If (e.Data.GetDataPresent(DataFormats.Text) = True) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    ' This handler will be called when a Data object is dropped on our DataGrid's window.
    ' At this point we will have to parse the CR/LF delimited text that is dropped on us.
    ' In a real application we would probably want to do more validation of the input...
    Private Sub grdMonitor_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles grdMonitor.DragDrop
        ' Get the entire text object that has been dropped on us.
        Dim tmp As String
        tmp = e.Data.GetData(DataFormats.Text).ToString()

        ' Tokenize the string into what (we hope) are Security strings
        Dim sep() As Char = {vbCr, vbLf, vbTab}

        Dim words() As String
        words = tmp.Split(sep)
        AddSecuritiesToGrid(words)

    End Sub
#End Region

#Region "Dropped securities list parser, Bloomberg Api Request processor"

    Private Sub AddSecuritiesToGrid(ByVal secs As String())

        Try
            ' Cancel any outstanding requests
            ' MarketDataAdapter.CancelAllRequests();
            ' Clear down the row mapper and data table
            _htRows.Clear()
            _realTimeDataTable.Clear()
            _realTimeDataTable.Rows.Clear()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Current Market: DesubscribeAll", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

        Try
            ' Now parse the security list, add a row to the grid for each security, and
            ' subscribe via the MarketDataAdapter
            MarketDataAdapterSubscribe(secs)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Current Market: AddRequests", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Sub MarketDataAdapterSubscribe(ByVal secs As String())

        If (secs.Length = 0) Then Exit Sub

        ' Temporary container for all the requests. We may be allocating 
        ' more than we need here. We can redimension the array later 
        ' after we have edited the input.
        Dim Reqs(secs.Length + 1) As IRequestBase
        Dim reqCt As Int32
        reqCt = 0

        ' This is an optimization - turns off notifications and index maintenance
        ' while loading the table
        _realTimeDataTable.BeginLoadData()

        ' Iterate through the input list of securities.
        For Each sec As String In secs

            ' Trim leading and trailing whitespace.
            Dim s As String
            s = sec.Trim()
            ' Check to see if we have anything left.
            If (s.Length > 0) Then
                ' Create a new row in the DataTable
                Dim realTimeDataRow As DataRow
                realTimeDataRow = _realTimeDataTable.NewRow()
                ' Add the Security to the "Security" column of the new row.
                realTimeDataRow("Security") = s
                ' Add the new row to the Datatable
                _realTimeDataTable.Rows.Add(realTimeDataRow)
                ' Add the new row to the hashtable - and use the current value
                ' of the correlation id as a key.
                _htRows.Add(_blpDataCorrelationId, realTimeDataRow)
                ' Create a new Request object
                Dim rtreq As RequestForRealtime
                rtreq = New RequestForRealtime
                ' Add the security, this will also validate the security string.
                ' An invalid string will generate an exception that will be
                ' caught by the caller of this method.
                rtreq.Securities.Add(s)
                ' Add all of the fields in the fieldList to the Request. These
                ' fields mnemonics will also be validated against the currently
                ' loaded (it was loaded when we called MarketDataAdapter.Startup) field table.
                rtreq.Fields.AddRange(_fieldList)
                ' Of course, we want to monitor the real time updates
                rtreq.Monitor = True
                ' All replies will be returned by field.
                rtreq.SubscriptionMode = SubscriptionMode.ByField
                ' Set the Request state object to the curent value of 
                ' the correlation id. This will be returned with the 
                ' reply and we will use it in the MarketDataAdapter_ReplyEvent (below)
                ' to quickly map a given reply to the correct row in the DataTable
                rtreq.State = _blpDataCorrelationId
                ' Add the request to the temporary storage
                Reqs(reqCt) = rtreq
                ' Increment the correlation id
                _blpDataCorrelationId = _blpDataCorrelationId + 1
                reqCt = reqCt + 1
            End If
        Next sec

        ' We're done modifying the data table.
        _realTimeDataTable.EndLoadData()

        If (reqCt = 0) Then Exit Sub

        ' Create a Request object for each object in the ArrayList
        ReDim Preserve Reqs(reqCt - 1)

        MarketDataAdapter.SendRequest(Reqs)
    End Sub

#End Region

#Region "Bloomberg.Api event Handlers"
    Private Sub MarketDataAdapter_ReplyEvent(ByVal reply As Reply)

        If (Not reply.ReplyError Is Nothing) Then Exit Sub

        Try

            ' Once again turn off notifications and index maintenance
            ' while loading the table
            _realTimeDataTable.BeginLoadData()

            ' Look up the row using the value in Request.State as a key.
            ' This value was generated from the correlation id we set in 
            ' MarketDataAdapterSubscribe above
            Dim state As Int32
            state = DirectCast(reply.Request.State, Int32)
            Dim rowMarket As DataRow
            rowMarket = DirectCast(_htRows(state), DataRow)

            If (Not rowMarket Is Nothing) Then

                ' Get the Securities Collection

                Dim sdcol As SecuritiesDataCollection
                sdcol = reply.GetSecurityDataItems()

                ' Get the Security Mnemonic from the appropriate cell on the grid.
                Dim securityMnemonic As String
                securityMnemonic = rowMarket(_realTimeDataTable.Columns("Security")).ToString()

                Dim sdi As SecurityDataItem
                sdi = Nothing

                Try

                    ' Find that security in the SecuritiesDataCollection, there should be 
                    ' only one security in the Collection.
                    sdi = sdcol(securityMnemonic)

                Catch ex As Exception
                    sdi = Nothing
                End Try

                ' We should always find the SecurityDataItem!
                If (Not sdi Is Nothing) Then

                    ' There may be data for several of the fields
                    For Each fdi As FieldDataItem In sdi.FieldsData
                        Dim dp As DataPoint
                        dp = fdi.DataPoints(0)
                        Dim val As String
                        val = Nothing

                        If Not dp Is Nothing Then
                            If (dp.IsError = True) Then
                                val = dp.ReplyError.DisplayName
                            Else
                                val = dp.ToString
                            End If

                            If (Not val Is Nothing) Then

                                For i As Int32 = 0 To _fieldList.Length Step 1
                                    If (fdi.Field.Equals(_fieldList(i))) Then

                                        ' Don't bother to update if the new value is equal to 
                                        ' the value that is already being displayed.
                                        If (rowMarket(i + 1) Is System.DBNull.Value Or val.CompareTo(rowMarket(i + 1).ToString()) <> 0) Then
                                            rowMarket(i + 1) = val
                                            Exit For
                                        End If
                                    End If
                                Next i
                            End If
                        End If
                    Next fdi

                End If
            End If
            ' We're done modifying the table.
            _realTimeDataTable.EndLoadData()
        Catch ex As Exception
            Throw
        End Try


    End Sub
    Private Sub MarketDataAdapter_StatusEvent(ByVal status As StatusCode, ByVal description As String)

        _sbpAdapterStatus.Text = description

    End Sub
#End Region

End Class

