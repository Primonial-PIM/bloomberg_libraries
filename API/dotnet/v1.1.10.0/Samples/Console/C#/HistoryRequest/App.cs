/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	class App
	{
		[MTAThread]
		static void Main(string[] args)
		{
			//start initialization process, while requests are being prepared
			MarketDataAdapter.Startup();

			//subscribe for any status messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnDataServices_StatusEvent);

			Request[] requests = {
									 InitHistoryRequest(SubscriptionMode.ByRequest), 
									 InitHistoryRequest(SubscriptionMode.ByField),
									 InitHistoryRequest(SubscriptionMode.BySecurity)
								 };
			//send request
			MarketDataAdapter.SendRequest(requests);

			Console.WriteLine("<<Press any key to exit>>");
			
			//wait for replies
			Console.Read();

			MarketDataAdapter.Shutdown();
		}
		private static void OnDataServices_StatusEvent(StatusCode status, string description)
		{
			//NOTE: this callback happens on a background thread.
			//When dealing with GUI elements please use 
			//Control.Invoke/Control.BeginInvoke to marshal the call back to 
			//the thread that created the Control.
			Console.WriteLine("Status event: ({0}) {1}", status, description);
		}
		private static void OnHistory_ReplyEvent(Reply reply)
		{
			try
			{
				//NOTE: this callback happens on a background thread.
				//When dealing with GUI elements please use 
				//Control.Invoke/Control.BeginInvoke to marshal the call back to 
				//the thread that created the Control.
				if(reply.ReplyError != null)
				{
					Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description);
					return;
				}

				Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode);

				//go through securities
				foreach(SecurityDataItem secItem in reply.GetSecurityDataItems())
				{
					Console.Write("\t");
					Console.WriteLine("Security: {0}", secItem.Security.FullName);

					//go through fields
					foreach(FieldDataItem fldItem in secItem.FieldsData)
					{
						Console.Write("\t\t");
						Console.WriteLine(fldItem.Field.Mnemonic + ": ");

						//go through field values
						foreach(DataPoint point in fldItem.DataPoints)
						{
							if(point.IsError)
								Console.WriteLine(point.ReplyError.DisplayName);
							else
							{
								Console.Write("\t\t\t");
								Console.WriteLine("Date: {0}", point.Time.ToShortDateString());
								Console.Write("\t\t\t");
								Console.WriteLine("Value: {0}", point.Value);
							}
						}
					}
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Exception occured in reply: {0}", ex.ToString());
			}
		}
		private static Request InitHistoryRequest(SubscriptionMode mode)
		{
			//issue history request
			RequestForHistory request = new RequestForHistory();

			//subscribe for reply event
			request.ReplyEvent += new ReplyEventHandler(OnHistory_ReplyEvent);

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapater.Startup method
			FieldTable ftbl = MarketDataAdapter.FieldTable;

			//add field(s)
			request.Fields.AddRange(new Field[]
				{ftbl["LAST_PRICE"], ftbl["ASK"], ftbl["VOLUME"], 
					ftbl["LOW"], ftbl["HIGH"], ftbl["BID"], ftbl["BEST_ASK"], 
					ftbl["MID"], ftbl["OPEN"]});

			//add securitie(s)
			request.Securities.AddRange(new string[]{"MSFT US Equity", "IBM US Equity"});

			//add currencies
			request.Currencies.AddRange(new string[] {"USD", "USD"}); //default is USD

			//set subscription mode
			request.SubscriptionMode = mode;

			request.DisplayNonTradingDays = DisplayNonTradingDays.AllCalendar;
			request.NonTradingDayValue = NonTradingDayValue.ShowNoNumber;
			request.Periodicity = Periodicity.CalendarDaily;
			
			//The following three blocks of code are equivalent ways 
			//of constructing a historical date range.

			//Construct date range using DateTime
			//request.StartDate = new ExtendedDate(DateTime.Now.Subtract(new TimeSpan(5, 0, 0, 0, 0)));
			//request.EndDate = new ExtendedDate(DateTime.Now);

			//Construct date range using ExtendedDate.Parse
			//request.StartDate = ExtendedDate.Parse("-5AD");
			//request.EndDate = ExtendedDate.Today;

			//Construct date range using Anchor and Offset objects
			DateAnchor anchor = new DateAnchor(DateTime.Now); //optional
			DateOffset offset = new DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day);
			request.StartDate = new ExtendedDate(anchor, offset);
			request.EndDate = ExtendedDate.Today;

			return request;
		}
	}
}
