/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	class App
	{
		[MTAThread]
		static void Main(string[] args)
		{
			//start initialization process, while requests are being prepared
			MarketDataAdapter.Startup();

			//subscribe for any status messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnDataServices_StatusEvent);

			Request[] requests = {
									 InitBulkRequest(SubscriptionMode.ByRequest), 
									 InitBulkRequest(SubscriptionMode.ByField),
									 InitBulkRequest(SubscriptionMode.BySecurity)
								 };
			//send request
			MarketDataAdapter.SendRequest(requests);

			Console.WriteLine("<<Press any key to exit>>");
			
			//wait for replies
			Console.Read();

			MarketDataAdapter.Shutdown();
		}
		private static void OnDataServices_StatusEvent(StatusCode status, string description)
		{
			//NOTE: this callback happens on a background thread.
			//When dealing with GUI elements please use 
			//Control.Invoke/Control.BeginInvoke to marshal the call back to 
			//the thread that created the Control.
			Console.WriteLine("Status event: ({0}) {1}", status, description);
		}
		private static void OnBulk_ReplyEvent(Reply reply)
		{
			try
			{
				//NOTE: this callback happens on a background thread.
				//When dealing with GUI elements please use 
				//Control.Invoke/Control.BeginInvoke to marshal the call back to 
				//the thread that created the Control.
				if(reply.ReplyError != null)
				{
					Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description);
					return;
				}

				Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode);

				//go through securities
				foreach(SecurityDataItem secItem in reply.GetSecurityDataItems())
				{
					Console.Write("\t");
					Console.WriteLine("Security: {0}", secItem.Security.FullName);

					//go through fields
					foreach(FieldDataItem fldItem in secItem.FieldsData)
					{
						Console.Write("\t\t");
						Console.WriteLine(fldItem.Field.Mnemonic + ": ");
						DisplayBulkPoints(fldItem.DataPoints);
					}
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Exception occured in reply: {0}", ex.ToString());
			}
		}
		private static Request InitBulkRequest(SubscriptionMode mode)
		{
			//issue history request
			Request request = new RequestForStatic();

			//subscribe for reply event
			request.ReplyEvent += new ReplyEventHandler(OnBulk_ReplyEvent);

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapater.Startup method
			FieldTable ftbl = MarketDataAdapter.FieldTable;

			//add field(s)
			//bulk field
			request.Fields.Add(ftbl["PRODUCT_SEG"]);
			request.Fields.Add(ftbl["COMPANY_ADDRESS"]);
			request.Fields.Add(ftbl["CIE_DES"]);
			//non-bulk field
			request.Fields.Add(ftbl["PX_BID"]);
			//add securitie(s)
			request.Securities.AddRange(new string[]{"MSFT US Equity", "GOOG US Equity"});
			//set subscription mode
			request.SubscriptionMode = mode;

			return request;
		}
		private static void DisplayBulkPoints(DataPointsCollection points)
		{
			//go through field values
			foreach(DataPoint point in points)
			{
				if(point.IsError)
					Console.WriteLine(point.ReplyError.DisplayName);
				else
				{
					if(point.HasChildren)
					{
						DisplayBulkPoints(point.DataPoints);
						Console.WriteLine();
					}
					else
					{
						Console.Write("\t\t\t");
						Console.WriteLine(point.Value);
					}
				}
			}
		}
	}
}
