/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/

using System;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	class App
	{
		[MTAThread]
		static void Main(string[] args)
		{
			//start initialization process, while requests are being prepared
			MarketDataAdapter.Startup();

			//subscribe for any status messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnDataServices_StatusEvent);

			Request[] requests = {
									 InitStaticOverrideRequest(SubscriptionMode.ByRequest), 
									 InitStaticOverrideRequest(SubscriptionMode.ByField),
									 InitStaticOverrideRequest(SubscriptionMode.BySecurity)
								 };
			//send request
			MarketDataAdapter.SendRequest(requests);

			Console.WriteLine("<<Press any key to exit>>");
			
			//wait for replies
			Console.Read();

			MarketDataAdapter.Shutdown();
		}
		private static void OnDataServices_StatusEvent(StatusCode status, string description)
		{
			//NOTE: this callback happens on a background thread.
			//When dealing with GUI elements please use 
			//Control.Invoke/Control.BeginInvoke to marshal the call back to 
			//the thread that created the Control.
			Console.WriteLine("Status event: ({0}) {1}", status, description);
		}
		private static void OnStatic_ReplyEvent(Reply reply)
		{
			try
			{
				//NOTE: this callback happens on a background thread.
				//When dealing with GUI elements please use 
				//Control.Invoke/Control.BeginInvoke to marshal the call back to 
				//the thread that created the Control.
				if(reply.ReplyError != null)
				{
					Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description);
					return;
				}

				Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode);

				//go through securities
				foreach(SecurityDataItem secItem in reply.GetSecurityDataItems())
				{
					Console.Write("\t");
					Console.WriteLine("Security: {0}", secItem.Security.FullName);

					//go through fields
					foreach(FieldDataItem fldItem in secItem.FieldsData)
					{
						Console.Write("\t\t");
						Console.Write(fldItem.Field.Mnemonic + ": ");

						//go through field values
						foreach(DataPoint point in fldItem.DataPoints)
						{
							if(point.IsError)
								Console.WriteLine(point.ReplyError.DisplayName);
							else
								Console.WriteLine(point.Value);
						}
					}
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Exception occured in reply: {0}", ex.ToString());
			}
		}
		private static Request InitStaticOverrideRequest(SubscriptionMode mode)
		{
			//issue static request
			RequestForStaticOverride request = new RequestForStaticOverride();

			//subscribe for reply event
			request.ReplyEvent += new ReplyEventHandler(OnStatic_ReplyEvent);

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapater.Startup method
			FieldTable ftbl = MarketDataAdapter.FieldTable;

			//add field(s)
			//overridable field
			request.Fields.Add(ftbl["CNVX_BID"]);	
			//regular field
			request.Fields.Add(ftbl["PX_ASK"]);
			
			//add security(s)
			request.Securities.Add("CT10 Govt");			
			//set subscription mode
			request.SubscriptionMode = mode;

			// Create override field/value pair for PX_BID/100
			OverrideFieldValue obid = new OverrideFieldValue(ftbl["PX_BID"], "100");
			//add override fields
			//by mnemonic
			request.OverrideFieldValues.Add(obid);

			// Create override field/value pair for SettlementDate/Nov 5 2007
			OverrideFieldValue odate = new OverrideFieldValue(ftbl["0x0457"], "20071105");
			//add override fields
			//by mnemonic
			request.OverrideFieldValues.Add(odate);

			return request;
		}
	}
}
