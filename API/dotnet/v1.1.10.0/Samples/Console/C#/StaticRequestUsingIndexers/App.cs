/*
 *********************************************************
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
 * WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
 * OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
 * PURPOSE.												 *
 *********************************************************
*/
using System;
using Bloomberg.Api;
using Bloomberg.Api.DataTypes;
using Bloomberg.Api.DataTypes.MetaData;

namespace Bloomberg.Api.Samples
{
	class App
	{
		[MTAThread]
		static void Main(string[] args)
		{
			//start initialization process, while requests are being prepared
			MarketDataAdapter.Startup();

			//subscribe for any status messages
			MarketDataAdapter.StatusEvent += new StatusEventHandler(OnDataServices_StatusEvent);

			Request[] requests = {
								  InitStaticOverrideRequest(SubscriptionMode.ByRequest), 
								  InitStaticOverrideRequest(SubscriptionMode.BySecurity)
								 };
			//send request
			MarketDataAdapter.SendRequest(requests);

			Console.WriteLine("<<Press any key to exit>>");
			
			//wait for replies
			Console.Read();

			MarketDataAdapter.Shutdown();
		}
		private static void OnDataServices_StatusEvent(StatusCode status, string description)
		{
			//NOTE: this callback happens on a background thread.
			//When dealing with GUI elements please use 
			//Control.Invoke/Control.BeginInvoke to marshal the call back to 
			//the thread that created the Control.
			Console.WriteLine("Status event: ({0}) {1}", status, description);
		}
		private static void OnStatic_ReplyEvent(Reply reply)
		{
			try
			{
				//NOTE: this callback happens on a background thread.
				//When dealing with GUI elements please use 
				//Control.Invoke/Control.BeginInvoke to marshal the call back to 
				//the thread that created the Control.
				if(reply.ReplyError != null)
				{
					Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description);
					return;
				}

				Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode);

				SecuritiesDataCollection securities =  reply.GetSecurityDataItems();

				FieldDataItem fieldData = securities["msft us equity"]["px_bid"];
				if(fieldData.DataPoints.Count > 0)
				{
					DataPoint data = fieldData.DataPoints[0];
					if(data.IsError)
						Console.WriteLine(data.ReplyError.DisplayName);
					else
						Console.WriteLine(data.Value);
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Exception occured in reply: {0}", ex.ToString());
			}
		}
		private static Request InitStaticOverrideRequest(SubscriptionMode mode)
		{
			//issue static request
			RequestForStatic request = new RequestForStatic();

			//subscribe for reply event
			request.ReplyEvent += new ReplyEventHandler(OnStatic_ReplyEvent);

			// Get a reference to the FieldTable factory object that has already
			// been loaded during the MarketDataAdapater.Startup method
			FieldTable ftbl = MarketDataAdapter.FieldTable;
			//add field(s)
			request.Fields.Add(ftbl["PX_BID"]);	
			//regular field
			request.Fields.Add(ftbl["PX_ASK"]);
			
			//add security(s)
			request.Securities.Add("MSFT US Equity");			
			//set subscription mode
			request.SubscriptionMode = mode;

			return request;
		}
	}
}
