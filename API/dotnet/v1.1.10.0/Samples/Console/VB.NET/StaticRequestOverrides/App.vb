'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Namespace Bloomberg.Api.Samples

    Module App

        Sub Main()

            'start initialization process, while requests are being prepared
            MarketDataAdapter.Startup()

            'subscribe for any status messages
            AddHandler MarketDataAdapter.StatusEvent, AddressOf OnDataServices_StatusEvent

            'send request
            MarketDataAdapter.SendRequest(InitStaticOverrideRequest(SubscriptionMode.ByRequest))
            MarketDataAdapter.SendRequest(InitStaticOverrideRequest(SubscriptionMode.ByField))
            MarketDataAdapter.SendRequest(InitStaticOverrideRequest(SubscriptionMode.BySecurity))

            Console.WriteLine("<<Press any key to exit>>")

            'wait for replies
            Console.Read()

            MarketDataAdapter.Shutdown()
        End Sub

        Private Sub OnDataServices_StatusEvent(ByVal status As StatusCode, ByVal description As String)
            'NOTE: this callback happens on a background thread.
            'When dealing with GUI elements please use 
            'Control.Invoke/Control.BeginInvoke to marshal the call back to 
            'the thread that created the Control.

            Console.WriteLine("Status event: ({0}) {1}", status, description)
        End Sub
        Private Sub OnStatic_ReplyEvent(ByVal reply As Reply)

            Dim secItem As SecurityDataItem
            Dim fldItem As FieldDataItem
            Dim point As DataPoint

            Try
                'NOTE: this callback happens on a background thread.
                'When dealing with GUI elements please use 
                'Control.Invoke/Control.BeginInvoke to marshal the call back to 
                'the thread that created the Control.
                If Not IsNothing(reply.ReplyError) Then
                    Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description)
                    Return
                End If

                Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode)

                'go through securities
                For Each secItem In reply.GetSecurityDataItems()
                    Console.Write(ControlChars.Tab)
                    Console.WriteLine("Security: {0}", secItem.Security.FullName)

                    'go through fields
                    For Each fldItem In secItem.FieldsData
                        Console.Write(ControlChars.Tab + ControlChars.Tab)
                        Console.Write(fldItem.Field.Mnemonic + ": ")

                        'go through field values
                        For Each point In fldItem.DataPoints
                            If (point.IsError) Then
                                Console.WriteLine(point.ReplyError.DisplayName)
                            Else
                                Console.WriteLine(point.Value)
                            End If
                        Next point
                    Next fldItem
                Next secItem
            Catch ex As Exception
                Console.WriteLine("Exception occured in reply: {0}", ex.ToString())
            End Try
        End Sub

        Private Function InitStaticOverrideRequest(ByVal mode As SubscriptionMode) As Request
            Dim request As RequestForStaticOverride
            'issue static request
            request = New RequestForStaticOverride

            'subscribe for reply event
            AddHandler request.ReplyEvent, AddressOf OnStatic_ReplyEvent

            'Get a reference to the FieldTable (factory) object that has been loaded during
            'MarketDataAdapter.Startup.
            Dim ftbl As FieldTable
            ftbl = MarketDataAdapter.FieldTable

            'add field(s)
            'overridable field
            request.Fields.Add(ftbl("CNVX_BID"))

            'regular field
            request.Fields.Add(ftbl("PX_ASK"))

            'add security(s)
            request.Securities.Add("CT10 Govt")
            request.Securities.Add("CT5 Govt")
            'set subscription mode
            request.SubscriptionMode = mode

            'add Field Value Pair for Bid/ "100"
            Dim obid As New OverrideFieldValue(ftbl("PX_BID"), "100")
            request.OverrideFieldValues.Add(obid)

            'add Field Value Pair for Settlement Date / Nov 5, 2007
            Dim odate As New OverrideFieldValue(ftbl("0x457"), "20071105")
            request.OverrideFieldValues.Add(odate)


            Return request.ToRequest()

        End Function

    End Module

End Namespace

