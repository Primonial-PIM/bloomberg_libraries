'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Namespace Bloomberg.Api.Samples

    Module App

        Sub Main()
            'start initialization process, while requests are being prepared
            MarketDataAdapter.Startup()

            'subscribe for any status messages
            AddHandler MarketDataAdapter.StatusEvent, AddressOf OnDataServices_StatusEvent

            'send request
            MarketDataAdapter.SendRequest(InitBulkRequest(SubscriptionMode.ByRequest))
            MarketDataAdapter.SendRequest(InitBulkRequest(SubscriptionMode.ByField))
            MarketDataAdapter.SendRequest(InitBulkRequest(SubscriptionMode.BySecurity))

            Console.WriteLine("<<Press any key to exit>>")

            'wait for replies
            Console.Read()

            MarketDataAdapter.Shutdown()
        End Sub

        Private Sub OnDataServices_StatusEvent(ByVal status As StatusCode, ByVal description As String)
            'NOTE: this callback happens on a background thread.
            'When dealing with GUI elements please use 
            'Control.Invoke/Control.BeginInvoke to marshal the call back to 
            'the thread that created the Control.

            Console.WriteLine("Status event: ({0}) {1}", status, description)
        End Sub

        Private Sub OnBulk_ReplyEvent(ByVal reply As Reply)

            Dim secItem As SecurityDataItem
            Dim fldItem As FieldDataItem
            Dim point As DataPoint

            Try

                'NOTE: this callback happens on a background thread.
                'When dealing with GUI elements please use 
                'Control.Invoke/Control.BeginInvoke to marshal the call back to 
                'the thread that created the Control.
                If Not IsNothing(reply.ReplyError) Then
                    Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description)
                    Return
                End If

                Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode)

                'go through securities
                For Each secItem In reply.GetSecurityDataItems()
                    Console.Write(ControlChars.Tab)
                    Console.WriteLine("Security: {0}", secItem.Security.FullName)

                    'go through fields
                    For Each fldItem In secItem.FieldsData
                        Console.Write(ControlChars.Tab + ControlChars.Tab)
                        Console.WriteLine(fldItem.Field.Mnemonic + ": ")

                        DisplayBulkPoints(fldItem.DataPoints)

                    Next fldItem
                Next secItem
            Catch ex As Exception
                Console.WriteLine("Exception occured in reply: {0}", ex.ToString())
            End Try
        End Sub

        Private Function InitBulkRequest(ByVal mode As SubscriptionMode) As Request
            Dim request As RequestForStatic
            'issue static request
            request = New RequestForStatic

            'subscribe for reply event
            AddHandler request.ReplyEvent, AddressOf OnBulk_ReplyEvent

            'Get a reference to the FieldTable (factory) object that has been loaded during
            'MarketDataAdapter.Startup.
            Dim ftbl As FieldTable
            ftbl = MarketDataAdapter.FieldTable

            'add field(s)
            'bulk field
            request.Fields.Add(ftbl("PRODUCT_SEG"))
            request.Fields.Add(ftbl("COMPANY_ADDRESS"))
            request.Fields.Add(ftbl("CIE_DES"))
            'non-bulk field
            request.Fields.Add(ftbl("PX_BID"))
            'add security(s)
            request.Securities.Add("MSFT US Equity")
            request.Securities.Add("GOOG US Equity")
            'set subscription mode
            request.SubscriptionMode = mode

            Return request.ToRequest()

        End Function

        Private Function DisplayBulkPoints(ByVal points As DataPointsCollection) As Boolean
            Dim point As DataPoint

            'go through field values
            For Each point In points
                If (point.IsError) Then
                    Console.WriteLine(point.ReplyError.DisplayName)
                Else
                    If (point.HasChildren) Then
                        DisplayBulkPoints(point.DataPoints)
                        Console.WriteLine()
                    Else
                        Console.Write(ControlChars.Tab + ControlChars.Tab + ControlChars.Tab)
                        Console.WriteLine(point.Value)
                    End If
                End If
            Next point

        End Function

    End Module

End Namespace