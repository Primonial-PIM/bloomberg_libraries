'*********************************************************
'* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT *
'* WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED,    *
'* INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES   *
'* OF MERCHANTABILITY AND/OR FITNESS FOR A  PARTICULAR   *
'* PURPOSE.											     *
'*********************************************************

Imports Bloomberg.Api
Imports Bloomberg.Api.DataTypes
Imports Bloomberg.Api.DataTypes.MetaData

Namespace Bloomberg.Api.Samples

    Module App

        Sub Main()

            'start initialization process, while requests are being prepared
            MarketDataAdapter.Startup()

            'subscribe for any status messages
            AddHandler MarketDataAdapter.StatusEvent, AddressOf OnDataServices_StatusEvent

            'send request
            MarketDataAdapter.SendRequest(InitHistoryRequest(SubscriptionMode.ByRequest))
            MarketDataAdapter.SendRequest(InitHistoryRequest(SubscriptionMode.ByField))
            MarketDataAdapter.SendRequest(InitHistoryRequest(SubscriptionMode.BySecurity))

            Console.WriteLine("<<Press any key to exit>>")

            'wait for replies
            Console.Read()

            MarketDataAdapter.Shutdown()
        End Sub

        Private Sub OnDataServices_StatusEvent(ByVal status As StatusCode, ByVal description As String)
            'NOTE: this callback happens on a background thread.
            'When dealing with GUI elements please use 
            'Control.Invoke/Control.BeginInvoke to marshal the call back to 
            'the thread that created the Control.

            Console.WriteLine("Status event: ({0}) {1}", status, description)
        End Sub

        Private Sub OnHistory_ReplyEvent(ByVal reply As Reply)

            Dim secItem As SecurityDataItem
            Dim fldItem As FieldDataItem
            Dim point As DataPoint

            Try

                'NOTE: this callback happens on a background thread.
                'When dealing with GUI elements please use 
                'Control.Invoke/Control.BeginInvoke to marshal the call back to 
                'the thread that created the Control.
                If Not IsNothing(reply.ReplyError) Then
                    Console.WriteLine("Error occured - {0}: {1}", reply.ReplyError.DisplayName, reply.ReplyError.Description)
                    Return
                End If

                Console.WriteLine("SubscriptionMode: {0}", reply.Request.SubscriptionMode)

                'go through securities
                For Each secItem In reply.GetSecurityDataItems()
                    Console.Write(ControlChars.Tab)
                    Console.WriteLine("Security: {0}", secItem.Security.FullName)

                    'go through fields
                    For Each fldItem In secItem.FieldsData
                        Console.Write(ControlChars.Tab + ControlChars.Tab)
                        Console.WriteLine(fldItem.Field.Mnemonic + ": ")

                        'go through field values
                        For Each point In fldItem.DataPoints
                            Console.Write(ControlChars.Tab + ControlChars.Tab + ControlChars.Tab)
                            If (point.IsError) Then
                                Console.WriteLine(point.ReplyError.DisplayName)
                            Else
                                Console.WriteLine("Date: {0}", point.Time.ToShortDateString())
                                Console.Write(ControlChars.Tab + ControlChars.Tab + ControlChars.Tab)
                                Console.WriteLine("Value: {0}", point.Value)
                            End If
                        Next point
                    Next fldItem
                Next secItem
            Catch ex As Exception
                Console.WriteLine("Exception occured in reply: {0}", ex.ToString())
            End Try
        End Sub

        Private Function InitHistoryRequest(ByVal mode As SubscriptionMode) As Request
            Dim request As RequestForHistory
            'issue history request
            request = New RequestForHistory

            'subscribe for reply event
            AddHandler request.ReplyEvent, AddressOf OnHistory_ReplyEvent

            'Get a reference to the FieldTable (factory) object that has been loaded during
            'MarketDataAdapter.Startup.
            Dim ftbl As FieldTable
            ftbl = MarketDataAdapter.FieldTable

            'add field(s)
            Dim fields() As Field = {ftbl("LAST_PRICE"), ftbl("ASK"), ftbl("VOLUME"), ftbl("LOW"), _
                    ftbl("HIGH"), ftbl("BID"), ftbl("BEST_ASK"), ftbl("MID"), ftbl("OPEN")}
            request.Fields.AddRange(fields)

            'add security(s)
            Dim securities() As String = {"MSFT US Equity", "IBM US Equity"}
            request.Securities.AddRange(securities)

            'add currencies
            request.Currencies.Add("USD") 'default is USD
            request.Currencies.Add("EUR") 'default is USD

            'set subscription mode
            request.SubscriptionMode = mode

            request.ReverseChronological = True

            request.DisplayNonTradingDays = DisplayNonTradingDays.AllCalendar
            request.NonTradingDayValue = NonTradingDayValue.ShowNoNumber
            request.Periodicity = Periodicity.CalendarDaily

            'The following three blocks of code are equivalent ways 
            'of constructing a historical date range.

            'Construct date range using DateTime
            'request.StartDate = New ExtendedDate(DateTime.Now.Subtract(New TimeSpan(5, 0, 0, 0, 0)))
            'request.EndDate = New ExtendedDate(DateTime.Now)

            'Construct date range using ExtendedDate.Parse
            'request.StartDate = ExtendedDate.Parse("-5AD")
            'request.EndDate = ExtendedDate.Today

            'Construct date range using Anchor and Offset objects
            Dim anchor As DateAnchor = New DateAnchor(DateTime.Now) 'optional
            Dim offset As DateOffset = New DateOffset(OffsetDirection.Negative, 5, CalendarType.Actual, PeriodUnit.Day)
            request.StartDate = New ExtendedDate(anchor, offset)
            request.EndDate = ExtendedDate.Today

            Return request.ToRequest()

        End Function
    End Module

End Namespace

