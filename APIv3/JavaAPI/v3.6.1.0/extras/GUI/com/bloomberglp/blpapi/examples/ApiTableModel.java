/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
 
package com.bloomberglp.blpapi.examples;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

public class ApiTableModel extends DefaultTableModel{
	private static final long serialVersionUID = 1L;
	private Boolean isEditable = false;
	public ApiTableModel() {
		super();
	}

	public void setIsEditable(Boolean b){
		isEditable = b;
	}
	
	public ApiTableModel(int rowCount, int columnCount) {
		super(rowCount, columnCount);
	}

	public ApiTableModel(Vector columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	public ApiTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	public ApiTableModel(Vector data, Vector columnNames) {
		super(data, columnNames);
	}

	public ApiTableModel(Object[][] data, Object[] columnNames) {
		super(data, columnNames);
	}

	public Vector<String> getColumnNames() {
		Vector<String> colNames = new Vector<String>();
		int colLen = this.getColumnCount();
		for (int i=0; i<colLen; ++i) {
			colNames.add(this.getColumnName(i));
		}
		return colNames;
	}



	@Override
	protected Object clone() throws CloneNotSupportedException {
		ApiTableModel model = new ApiTableModel();		
		model.setDataVector((Vector)this.getDataVector().clone(), getColumnNames());		
		return model;
	}
	
	protected Object cloneHeader() {
		ApiTableModel model = new ApiTableModel(getColumnNames(),0);
		return model;
	}

	public ArrayList<String> getRow(int r) {
		ArrayList<String> rowList = new  ArrayList<String>();
		int numRow = this.getRowCount();
		if (r>numRow) {
			return rowList;
		}
		else {
			int numCol = this.getColumnCount();
			for (int c=0; c<numCol; ++c) {
				if (this.getValueAt(r, c)!=null) {
					String value = this.getValueAt(r, c).toString();
					rowList.add(c, value);
				}
				else {
					rowList.add(c, "");
				}
			}
			return rowList;
		}
		
	}
	
	//overloaded to make cells not editable.
	public boolean isCellEditable(int row, int col) {   
		return isEditable; 
	}
	
	public void dump() {
		int numRows = this.getRowCount();
		int numColumn = this.getColumnCount();
		
		for (int i=0; i<numRows; ++i) {
			for (int j = 0; j<numColumn; ++j) {
				System.out.print(this.getValueAt(i, j) + " ");
			}
			System.out.println();
		}
	}
}
