/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
 

package com.bloomberglp.blpapi.examples;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.rtf.RTFEditorKit;
import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.EventHandler;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.MessageIterator;
import com.bloomberglp.blpapi.Name;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;

public class NonAgencyBondExample extends JFrame {
	private static final long serialVersionUID = -3518675418927250140L;
	private static NonAgencyBondExample app;
	private JPanel mainPanel, topPanel, dataPanel, analyticPanel, controlPanel, vectorPanel;
	private JTabbedPane dataTabbedPane;
	private JButton cmdGetData, cmdClearData, cmdGetAllData, cmdClearAllData;
	private JLabel lblSec;
	private JRadioButton rdoPrice, rdoYield;
	private JTextField txtSec, txtPrice1, txtPrice2, txtPrice3;
	private JComboBox ptsComboBox;
	private JScrollPane analyticDataScrollPane;
	private JTable histSpeedTable, bondDataTable, collateralTable, scenarioTable, analyticDataTable;
	private HashMap<String, String> fieldMap;
	private SessionOptions    d_sessionOptions;
	private Session           d_session;
	private boolean showSecurityErrorMsg = true;
	private static final Name SECURITY_DATA = new Name("securityData");
	private static final Name FIELD_DATA = new Name("fieldData");
	private static final Name SECURITY_ERROR = new Name("securityError");

	/* 
	 ***********************************************************************************
	 * constructor
	 ***********************************************************************************
	 */
	public NonAgencyBondExample() {
		setLookandFeel();
		init();
		buildGui();
	}

	/* 
	 ***********************************************************************************
	 * Initialize the fieldMap fild mapping.
	 ***********************************************************************************
	 */
	private void init() {
		fieldMap = new HashMap<String, String>(20);
		fieldMap.put("1M CPR", "MTG_PL_CPR_1M");
		fieldMap.put("1M CDR", "MTG_CDR_1M");
		fieldMap.put("1M VPR", "MTG_VPR_1M");
		fieldMap.put("1M SEVerity", "MTG_SEV_1M");

		fieldMap.put("Is Fully Credit Modeled", "CREDIT_MODELED_INDICATOR");
		fieldMap.put("Current Face", "MTG_AMT_OUT_FACE");
		fieldMap.put("Bond Factor", "MTG_FACTOR");
		fieldMap.put("Coupon", "CPN");
		fieldMap.put("Tranche Type", "MTG_TRANCHE_TYP_LONG");
		fieldMap.put("Orig S&P Rtg", "RTG_SP_INITIAL");
		fieldMap.put("Curr S&P Rtg", "RTG_SP");
		fieldMap.put("Orig Mdy Rtg", "RTG_MDY_INITIAL");
		fieldMap.put("Curr Mdy Rtg", "RTG_MOODY");
		fieldMap.put("Orig Credit Support", "ORIG_CREDIT_SUPPORT");
		fieldMap.put("Curr Credit Support", "CURR_CREDIT_SUPPORT");
		fieldMap.put("Coverage Ratio", "CREDIT_SUPPORT_COVERAGE");

		fieldMap.put("Collat Type", "COLLAT_TYP"); 
		fieldMap.put("Collat Factor", "MTG_POOL_FACTOR"); 
		fieldMap.put("Curr # Loans", "MTG_NUM_POOLS"); 
		fieldMap.put("WAC", "MTG_WACPN");
		fieldMap.put("WALA", "MTG_WHLN_WALA");
		fieldMap.put("30 Day Delq", "MTG_WHLN_30DLQ");
		fieldMap.put("60 Day Delq", "MTG_WHLN_60DLQ");
		fieldMap.put("90 Day Delq", "MTG_WHLN_90DLQ");
		fieldMap.put("% Bankruptcy", "BANKRUPT_PCT");
		fieldMap.put("% Foreclosure", "MTG_WHLN_FCLS");
		fieldMap.put("% REO", "MTG_WHLN_REO");
		fieldMap.put("60+ Delq", "MTG_DELQ_60PLUS_CUR");
		fieldMap.put("90+ Delq", "MTG_DELQ_90PLUS_CUR");
		fieldMap.put("% Collat Cum Loss", "CURR_CUM_LOSS_AMT");

		fieldMap.put("Yield", "YLD_CONV_ASK");
		fieldMap.put("Price", "PX_ASK");
		fieldMap.put("WAL", "MTG_WAL");
		fieldMap.put("Duration", "MTG_STATIC_MOD_DUR");
		fieldMap.put("Principal Window", "MTG_PRINC_WIN");
		fieldMap.put("I Spread", "I_SPRD_ASK");
		fieldMap.put("Z Spread", "Z_SPRD_ASK");
		fieldMap.put("N Spread", "N_SPRD_ASK");
		fieldMap.put("E Spread", "E_SPRD_ASK");
		fieldMap.put("1st Loss Dt", "FIRST_LOSS_DATE");
		fieldMap.put("Bond Loss $", "PROJ_BOND_CUM_LOSS_AMT");
		fieldMap.put("Collateral Loss $", "PROJ_COLL_CUM_LOSS_AMT");
		fieldMap.put("Bond Loss %", "PROJ_BOND_CUM_LOSS_PCT");
		fieldMap.put("Collateral Loss %", "PROJ_COLL_CUM_LOSS_PCT");
		
		//fieldMap.put("", "");
		fieldMap.put("Prepay Speed", "MTG_PREPAY_SPEED");
		fieldMap.put("Prepay Type", "MTG_PREPAY_TYP");
		fieldMap.put("Prepay Vector", " PREPAY_SPEED_VECTOR");
		fieldMap.put("Dynamics", "ALLOW_DYNAMIC_CASHFLOW_CALCS");
		fieldMap.put("Default Speed", "DEFAULT_PERCENT");
		fieldMap.put("Default Type", "DEFAULT_TYPE");
		fieldMap.put("Default Vector", "DEFAULT_SPEED_VECTOR");
		fieldMap.put("Severity Curve", "LOSS_SEVERITY");
		fieldMap.put("Recovery Lag", "RECOVERY_LAG");
		
		fieldMap.put("Bond Writedown (% of Current Face)", "PROJ_BOND_WRITEDWN_PCT_CURR_FACE");
		fieldMap.put("Collateral Writedown (% of Current Face)", "PROJ_COLL_WRITEDWN_PCT_CURR_FACE");
	}

	/* 
	 ***********************************************************************************
	 * Sets the OS default Look and Feel
	 ***********************************************************************************
	 */
	private void setLookandFeel() {
		try {
			//UIManager.setLookAndFeel(new WindowsLookAndFeel());
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 ***********************************************************************************
	 * This method will build the GUI components
	 ***********************************************************************************
	 */
	private void buildGui(){
		Container container = this.getContentPane();
		this.setTitle("Bloomberg Non Agency Bond Example");
		this.setSize(860, 640);
		this.setResizable(false);
		container.add(getMainPanel());

		this.addWindowListener(new WindowCloser());
		this.setVisible(true);
	}

	private JPanel getMainPanel(){
		if (mainPanel == null) {
			mainPanel = new JPanel(new BorderLayout(5,5));
			mainPanel.add(getTopPanel(), BorderLayout.NORTH);
			mainPanel.add(getControlPanel(), BorderLayout.CENTER);
			mainPanel.add(getDataTabbedPane(), BorderLayout.SOUTH);
		}
		return mainPanel;
	}

	private JPanel getTopPanel(){
		if (topPanel == null) {
			topPanel = new JPanel();
			topPanel.setBorder(BorderFactory.createEtchedBorder());

			JPanel titlePanel = new JPanel();

			ImageIcon imgLogo = new ImageIcon("./img/bberg.jpg");
			JLabel bb_logo = new JLabel(imgLogo);
			bb_logo.setBorder(BorderFactory.createEtchedBorder());
			titlePanel.add(bb_logo);

			ImageIcon imgTitle = new ImageIcon("./img/title.jpg");
			JLabel title = new JLabel(imgTitle);

			title.setBorder(BorderFactory.createEtchedBorder());
			titlePanel.add(title);

			topPanel.add(titlePanel);		
		}
		return topPanel;
	}

	private JPanel getControlPanel(){
		if (controlPanel == null) {
			controlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			controlPanel.setBorder(BorderFactory.createEtchedBorder());

			lblSec = new JLabel("Enter Security: ");
			txtSec=new JTextField("RFMSI 06-S1 1A3", 15);
			txtSec.setBackground(Color.WHITE);
			txtSec.setToolTipText("Enter a mortgage security (ie. RASC 06-emx1 a2 mtge)");
			//add keylistener to respond when user enter test
			txtSec.addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER ) {
						app.run(false);
					}		
				}
				public void keyReleased(KeyEvent e) {
					//don't have to do anything for this event
				}
				public void keyTyped(KeyEvent e) {
					//don't have to do anything for this event
				}		
			});
			controlPanel.add(lblSec);
			controlPanel.add(txtSec);
			
			JPanel buttonPanel = new JPanel(new GridLayout(1,4,5,5));
			
			buttonPanel.add(getCmdGetData());
			buttonPanel.add(getCmdClearData());
			buttonPanel.add(getCmdGetAllData());
			buttonPanel.add(getCmdClearAllData());
			
			controlPanel.add(buttonPanel);
		}
		return controlPanel;
	}

	private JTabbedPane getDataTabbedPane(){
		if(dataTabbedPane==null) {
			dataTabbedPane = new JTabbedPane(JTabbedPane.BOTTOM);
			dataTabbedPane.addTab("Data", getDataPanel());
			dataTabbedPane.addTab("Analytic", getAnalyticPanel());
			dataTabbedPane.addTab("Vectors", getVectorPanel());
			dataTabbedPane.setPreferredSize(new Dimension(200,500));

		}
		return dataTabbedPane;
	}
	
	private JPanel getVectorPanel(){
		if (vectorPanel == null) {
			vectorPanel = new JPanel(new FlowLayout(5,15,10));
			
			RTFEditorKit rtf_file = new RTFEditorKit();
			JEditorPane rtf_editor = new JEditorPane();
			rtf_editor.setEditorKit( rtf_file );
			rtf_editor.setBackground( Color.white );
			try {
				FileInputStream file = new FileInputStream("Vectors.rtf");
				rtf_file.read(file, rtf_editor.getDocument(), 0);
			}
			catch(Exception e) {
				//e.printStackTrace();
				JOptionPane.showMessageDialog(this.getContentPane(), "Unable to open \"Vectors.rtf\"!");
			}

			JScrollPane vectorScrollPane = new JScrollPane(rtf_editor);
			vectorScrollPane.setBorder(BorderFactory.createEtchedBorder());

			vectorScrollPane.setPreferredSize(new Dimension(820,450));
			vectorPanel.add(vectorScrollPane);
			
		}
		return vectorPanel;
	}

	private JPanel getDataPanel(){
		if (dataPanel == null) {
			dataPanel = new JPanel(new GridLayout(1,3,5,5));

			//Historical Speeds table
			Object[][] histSpeedData = {{"1M CPR", " "}, {"1M CDR", ""}, {"1M VPR", ""}, {"1M SEVerity", ""}};
			ApiTableModel mod = new ApiTableModel(histSpeedData, new Object[] {" ", " "});
			histSpeedTable = new JTable(mod);
			histSpeedTable.getColumnModel().getColumn(0).setPreferredWidth(120);
			histSpeedTable.getColumnModel().getColumn(1).setPreferredWidth(135);
			histSpeedTable.getTableHeader().setReorderingAllowed(false);
			histSpeedTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
			histSpeedTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			histSpeedTable.setBackground(Color.WHITE);
			JScrollPane histSpeedPane = new JScrollPane(histSpeedTable);
			histSpeedPane.setPreferredSize(new Dimension(260,435)); 
			JPanel histSpeedPanel = new JPanel();
			histSpeedPanel.setBorder(BorderFactory.createTitledBorder("Historical Speeds"));
			histSpeedPanel.add(histSpeedPane);

			//Bond Data table
			Object[][] bondData = {{"Is Fully Credit Modeled", " "}, 
					{"Current Face", ""}, 
					{"Bond Factor", ""}, 
					{"Coupon", ""},
					{"Tranche Type", ""},
					{"Orig S&P Rtg", ""},
					{"Curr S&P Rtg", ""},
					{"Orig Mdy Rtg", ""},
					{"Curr Mdy Rtg", ""},
					{"Orig Credit Support", ""},
					{"Curr Credit Support", ""},
					{"Coverage Ratio", ""}
			};
			mod = new ApiTableModel(bondData, new Object[] {" ", " "});
			bondDataTable = new JTable(mod);
			bondDataTable.getColumnModel().getColumn(0).setPreferredWidth(120);
			bondDataTable.getColumnModel().getColumn(1).setPreferredWidth(135);
			bondDataTable.getTableHeader().setReorderingAllowed(false);
			bondDataTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
			bondDataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			bondDataTable.setBackground(Color.WHITE);
			JScrollPane bondDataPane = new JScrollPane(bondDataTable);
			bondDataPane.setPreferredSize(new Dimension(260,435)); 
			JPanel bondDataPanel = new JPanel();
			bondDataPanel.setBorder(BorderFactory.createTitledBorder("Bond Data"));
			bondDataPanel.add(bondDataPane);

			//Collateral table
			Object[][] collateralData = {{"Collat Type", ""}, 
					{"Collat Factor", ""}, 
					{"Curr # Loans", ""}, 
					{"WAC", ""},
					{"WALA", ""},
					{"30 Day Delq", ""},
					{"60 Day Delq", ""},
					{"90 Day Delq", ""},
					{"% Bankruptcy", ""},
					{"% Foreclosure", ""},
					{"% REO", ""},
					{"60+ Delq", ""},
					{"90+ Delq", ""},
					{"% Collat Cum Loss", ""}
			};
			mod = new ApiTableModel(collateralData, new Object[] {" ", " "});
			collateralTable = new JTable(mod);
			collateralTable.getColumnModel().getColumn(0).setPreferredWidth(120);
			collateralTable.getColumnModel().getColumn(1).setPreferredWidth(135);
			collateralTable.getTableHeader().setReorderingAllowed(false);
			collateralTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
			collateralTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			collateralTable.setBackground(Color.WHITE);
			JScrollPane collateralPane = new JScrollPane(collateralTable);
			collateralPane.setPreferredSize(new Dimension(260,435)); 
			JPanel collateralPanel = new JPanel();
			collateralPanel.setBorder(BorderFactory.createTitledBorder("Collateral Details"));
			collateralPanel.add(collateralPane);

			dataPanel.add(histSpeedPanel);
			dataPanel.add(bondDataPanel);
			dataPanel.add(collateralPanel);
		}
		return dataPanel;
	}

	private JPanel getAnalyticPanel (){
		if (analyticPanel==null){
			analyticPanel = new JPanel(new FlowLayout(5,15,10));
			analyticPanel.setPreferredSize(new Dimension (1000,1000));

			JPanel optionPanel = new JPanel(new GridLayout(1,2));
			optionPanel.setPreferredSize(new Dimension(195, 20));
			optionPanel.setBorder(BorderFactory.createEtchedBorder());
			
			rdoPrice = new JRadioButton("Price", true);
			rdoYield = new JRadioButton("Yield", false);
			ButtonGroup synGroup = new ButtonGroup();
			synGroup.add(rdoPrice);
			synGroup.add(rdoYield);
			
			rdoPrice.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					try {
						clearAnalyticData();
						analyticDataTable.setValueAt("Price 1", 0, 0);
						analyticDataTable.setValueAt("Yield", 1, 0);
						analyticDataTable.setValueAt("Price 2", 17, 0);
						analyticDataTable.setValueAt("Yield", 18, 0);
						analyticDataTable.setValueAt("Price 3", 34, 0);
						analyticDataTable.setValueAt("Yield", 35, 0);
					}
					catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			});	
			
			rdoYield.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					try {
						clearAnalyticData();
						analyticDataTable.setValueAt("Yield 1", 0, 0);
						analyticDataTable.setValueAt("Price", 1, 0);
						analyticDataTable.setValueAt("Yield 2", 17, 0);
						analyticDataTable.setValueAt("Price", 18, 0);
						analyticDataTable.setValueAt("Yield 3", 34, 0);
						analyticDataTable.setValueAt("Price", 35, 0);
					}
					catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			});	
			
			
			optionPanel.add(rdoPrice);
			optionPanel.add(rdoYield);
			
			JPanel pricingPanel = new JPanel(new GridLayout(4,1,5,5));
			pricingPanel.setPreferredSize(new Dimension(195,110));
			pricingPanel.setBorder(BorderFactory.createEtchedBorder());

			//JPanel ptsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,2,2));
			JPanel ptsPanel = new JPanel(new GridLayout(1,2));
			
			ptsPanel.add(new JLabel("Vary Prices (Pts): "));

			ptsComboBox = new JComboBox(new String[] {"1",
					"2",
					"5",
					"10"});
			ptsComboBox.setPreferredSize(new Dimension(97,20));
			ptsComboBox.setSelectedItem("10");
			ptsComboBox.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					try {
						int varyPrice = Integer.parseInt(ptsComboBox.getSelectedItem().toString());
						int price1 = Integer.parseInt(txtPrice1.getText());
						if(varyPrice > 0) {
							price1 += varyPrice;
							txtPrice2.setText(String.valueOf(price1));
							price1 += varyPrice;
							txtPrice3.setText(String.valueOf(price1));
						}
					}
					catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			});	
			
			ptsPanel.add(ptsComboBox);
			
			//JPanel price1Panel = new JPanel(new FlowLayout(FlowLayout.LEFT,2,2));
			JPanel price1Panel = new JPanel(new GridLayout(1,2));
			price1Panel.add(new JLabel("Price 1                  "));
			price1Panel.add(txtPrice1 = new JTextField("75", 11));
			txtPrice1.setBackground(Color.WHITE);

			//JPanel price2Panel = new JPanel(new FlowLayout(FlowLayout.LEFT,2,2));
			JPanel price2Panel = new JPanel(new GridLayout(1,2));
			price2Panel.add(new JLabel("Price 2                  "));
			price2Panel.add(txtPrice2 = new JTextField("85", 11));
			txtPrice2.setBackground(Color.WHITE);

			//JPanel price3Panel = new JPanel(new FlowLayout(FlowLayout.LEFT,2,2));
			JPanel price3Panel = new JPanel(new GridLayout(1,2));
			price3Panel.add(new JLabel("Price 3                  "));
			price3Panel.add(txtPrice3 = new JTextField("95", 11));
			txtPrice3.setBackground(Color.WHITE);

			pricingPanel.add(ptsPanel);
			pricingPanel.add(price1Panel);
			pricingPanel.add(price2Panel);
			pricingPanel.add(price3Panel);
			
			JPanel leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,5,5));
			leftPanel.setPreferredSize(new Dimension(200,192));
			leftPanel.add(optionPanel);
			leftPanel.add(pricingPanel);

			//scenario table
			Object[][] scenarioData = {{"Prepay Speed", 	"100",	"100",	"100"}, 
					{"Prepay Type",	"CPR",	"CPR",	"CPR"}, 
					{"Prepay Vector",	"15",	"2 12 R 10", "2"}, 
					{"Dynamics",	"Y",	"Y",	"Y"}, 
					{"Default Speed",	"200",	"100",	"100"}, 
					{"Default Type",	"CDR",	"CDR",	"CDR"},
					{"Default Vector",	"2 10 R 8",	"2 4 6 8", "6 12 R 2"},
					{"Severity Curve",	"60 24 R 85",	"60 12 R 85",	"85 12 R 60"},
					{"Recovery Lag",	"0",	"0",	"0"}
			};

			ApiTableModel mod = new ApiTableModel(scenarioData, new Object[] {" ", "Scenario 1", "Scenario 2", "Scenario 3"});
			mod.setIsEditable(true);
			scenarioTable = new JTable(mod);
			scenarioTable.setBackground(new Color(250,250,173));
			scenarioTable.getColumnModel().getColumn(0).setPreferredWidth(149);
			scenarioTable.getColumnModel().getColumn(1).setPreferredWidth(149);
			scenarioTable.getColumnModel().getColumn(2).setPreferredWidth(149);
			scenarioTable.getColumnModel().getColumn(3).setPreferredWidth(149);
			scenarioTable.getTableHeader().setReorderingAllowed(false);
			scenarioTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
			scenarioTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			JScrollPane scenarioPane = new JScrollPane(scenarioTable);
			scenarioPane.setPreferredSize(new Dimension(600,183)); 
			
			analyticPanel.add(getNewLabel("Valuation Scenarios", 815,25));	
			analyticPanel.add(leftPanel);
			analyticPanel.add(scenarioPane);
			analyticPanel.add(getAnalyticDataScrollPane());
		}
		return analyticPanel;
	}

	private JButton getCmdClearData() {
		if (cmdClearData==null){
			cmdClearData = new JButton("Clear Data");
			//add keylistener to respond when user enter test
			cmdClearData.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					try {
						clear();
					}
					catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			});	
		}
		return cmdClearData;
	}

	private JButton getCmdClearAllData() {
		if (cmdClearAllData==null){
			cmdClearAllData = new JButton("Clear All Data");
			//add keylistener to respond when user enter test
			cmdClearAllData.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					try {
						clearData();
						clearAnalyticData();
					}
					catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			});	
		}
		return cmdClearAllData;
	}

	private JButton getCmdGetAllData() {
		if (cmdGetAllData==null){
			cmdGetAllData = new JButton("Get All Data");
			//add keylistener to respond when user enter test
			cmdGetAllData.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					try {
						if (txtSec.getText().trim().length()>0){
							app.run(true);
						}
						else {
							JOptionPane.showMessageDialog(app.getContentPane(), "Please enter a valid security.");
						}		
					}
					catch (Exception e2) {

					}
				}
			});
		}
		return cmdGetAllData;
	}

	private JButton getCmdGetData() {
		if (cmdGetData==null){
			cmdGetData = new JButton("Get Data");
			//add keylistener to respond when user enter test
			cmdGetData.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					app.run(false);
				}
			});
		}
		return cmdGetData;
	}

	private JLabel getNewLabel(String str, int x, int y) {
		Font  font = new Font("dialog", Font.BOLD, 14);
		Color  background = new Color(114,175,224);
		Color foreground = Color.white;
		JLabel localLabel = new JLabel(str) {
			public void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				super.paintComponent(g);
			}	      
		};	    
		localLabel.setFont(font);
		localLabel.setOpaque(true);
		localLabel.setBackground(background);
		localLabel.setForeground(foreground);
		localLabel.setPreferredSize(new Dimension(x,y));
		//localLabel.setBorder(BorderFactory.createEtchedBorder());
		localLabel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		localLabel.setHorizontalAlignment( SwingConstants.CENTER );
		return localLabel;
	}

	private JScrollPane getAnalyticDataScrollPane (){
		if (analyticDataScrollPane==null) {

			Object[][] analyticData = {
					{"Price 1", 	"",	"",	"", ""}, 
					{"Yield",	"",	"",	"", ""}, 
					{"WAL",	"",	"", ""}, 
					{"Duration",	"",	"",	"", ""}, 
					{"Principal Window",	"",	"",	"", ""}, 
					{"I Spread",	"",	"",	"", ""},
					{"Z Spread",	"",	"",	"", ""},
					{"N Spread",	"",	"", ""},
					{"E Spread",	"",	"",	"", ""},
					{"1st Loss Dt",	"",	"",	"", ""},
					{"Bond Loss $",	"",	"",	"", ""},
					{"Collateral Loss $",	"",	"",	"", ""},
					{"Bond Loss %",	"",	"",	"", ""},
					{"Collateral Loss %",	"",	"",	"", ""},
					{"Bond Writedown (% of Current Face)",	"",	"",	"", ""},
					{"Collateral Writedown (% of Current Face)",	"",	"",	"", ""},

					{"", 	"",	"",	"", ""}, 
					{"Price 2", 	"",	"",	"", ""}, 
					{"Yield",	"",	"",	"", ""}, 
					{"WAL",	"",	"", ""}, 
					{"Duration",	"",	"",	"", ""}, 
					{"Principal Window",	"",	"",	"", ""}, 
					{"I Spread",	"",	"",	"", ""},
					{"Z Spread",	"",	"",	"", ""},
					{"N Spread",	"",	"", ""},
					{"E Spread",	"",	"",	"", ""},
					{"1st Loss Dt",	"",	"",	"", ""},
					{"Bond Loss $",	"",	"",	"", ""},
					{"Collateral Loss $",	"",	"",	"", ""},
					{"Bond Loss %",	"",	"",	"", ""},
					{"Collateral Loss %",	"",	"",	"", ""},
					{"Bond Writedown (% of Current Face)",	"",	"",	"", ""},
					{"Collateral Writedown (% of Current Face)",	"",	"",	"", ""},

					{"", 	"",	"",	"", ""}, 
					{"Price 3", 	"",	"",	"", ""}, 
					{"Yield",	"",	"",	"", ""}, 
					{"WAL",	"",	"", ""}, 
					{"Duration",	"",	"",	"", ""}, 
					{"Principal Window",	"",	"",	"", ""}, 
					{"I Spread",	"",	"",	"", ""},
					{"Z Spread",	"",	"",	"", ""},
					{"N Spread",	"",	"", ""},
					{"E Spread",	"",	"",	"", ""},
					{"1st Loss Dt",	"",	"",	"", ""},
					{"Bond Loss $",	"",	"",	"", ""},
					{"Collateral Loss $",	"",	"",	"", ""},
					{"Bond Loss %",	"",	"",	"", ""},
					{"Collateral Loss %",	"",	"",	"", ""},
					{"Bond Writedown (% of Current Face)",	"",	"",	"", ""},
					{"Collateral Writedown (% of Current Face)",	"",	"",	"", ""}

			};

			ApiTableModel mod = new ApiTableModel(analyticData, new Object[] {" ", "Scenario 1", "Scenario 2", "Scenario 3", "Averages"});
			analyticDataTable = new JTable(mod) {
				public Component prepareRenderer(TableCellRenderer renderer,int Index_row, int Index_col) {
					Component comp = super.prepareRenderer(renderer, Index_row, Index_col);
					if (Index_row%17  == 0) {
						comp.setBackground(Color.LIGHT_GRAY);

					} 
					else {
						if (!isRowSelected(Index_row))
							comp.setBackground(Color.white);
					}					
					return comp;
				}
			};	

			analyticDataScrollPane = new JScrollPane(analyticDataTable);
			analyticDataScrollPane.setBorder(BorderFactory.createEtchedBorder());

			analyticDataScrollPane.setPreferredSize(new Dimension(815,215));

		}
		return analyticDataScrollPane;
	}

	/* 
	 ***********************************************************************************
	 * This method will create the session
	 ***********************************************************************************
	 */
	private boolean createSession() throws Exception
	{
		d_sessionOptions = new SessionOptions();
		d_sessionOptions.setServerHost("localhost");
		d_sessionOptions.setServerPort(8194);

		//if (d_session != null) d_session.stop();
		if (d_session != null) return true;

		d_session = new Session(d_sessionOptions, new ApiEventHandler());	
		//d_session = new Session(d_sessionOptions);
		return d_session.start();
	}

	private void run(boolean getAllData) {
		d_session=null;
		if (txtSec.getText().trim().length()<=0){
			JOptionPane.showMessageDialog(app.getContentPane(), "Please enter a valid security.");
			return;
		}
		if (getAllData) {
			try {
				getData();
				getAnalyticData();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		else {
			String name = dataTabbedPane.getTitleAt(dataTabbedPane.getSelectedIndex());
			try {
				if(name.equals("Data")) {
					getData();
				}
				else if(name.equals("Analytic")) {
					getAnalyticData();
				}
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private void getAnalyticData() throws Exception
	{
		showSecurityErrorMsg = true;
		clearAnalyticData();
		
		if(!createSession()) {
			System.out.println("Failed to start session.");
			return;
		}

		System.out.println("Connected successfully!");

		if (!d_session.openService("//blp/refdata")) {
			System.out.println("Failed to open //blp/refdata");
			return;
		}

		Service refDataService = d_session.getService("//blp/refdata");

		double price1 = Double.parseDouble(txtPrice1.getText());
		double price2 = Double.parseDouble(txtPrice2.getText());
		double price3 = Double.parseDouble(txtPrice3.getText());

		double[] prices = new double[] {price1, price2, price3};
		
		String overridePriceYieldField = "";
        String returnPriceYieldField = "";
        
        if (rdoPrice.isSelected()){
        	overridePriceYieldField = "PX_ASK";
        	returnPriceYieldField="YLD_CONV_ASK";
        }
        else {
        	overridePriceYieldField = "YLD_CONV_ASK";
        	returnPriceYieldField="PX_ASK";
        }
		
		int col = 1;
		int scenario = 1;
		for (double price: prices) {
			for (scenario = 1; scenario<4; ++scenario) {
				//create reference data request
				Request request = refDataService.createRequest("ReferenceDataRequest");
				Element securities = request.getElement("securities");
				securities.appendValue(txtSec.getText().trim().toUpperCase() + " Mtge");

				Element fields = request.getElement("fields");
				fields.appendValue(returnPriceYieldField);
				fields.appendValue("MTG_WAL");
				fields.appendValue("MTG_STATIC_MOD_DUR");
				fields.appendValue("MTG_PRINC_WIN");
				fields.appendValue("I_SPRD_ASK");
				fields.appendValue("Z_SPRD_ASK");
				fields.appendValue("N_SPRD_ASK");
				fields.appendValue("E_SPRD_ASK");
				fields.appendValue("FIRST_LOSS_DATE");
				fields.appendValue("PROJ_BOND_CUM_LOSS_AMT");
				fields.appendValue("PROJ_COLL_CUM_LOSS_AMT");
				fields.appendValue("PROJ_BOND_CUM_LOSS_PCT");
				fields.appendValue("PROJ_COLL_CUM_LOSS_PCT");
				fields.appendValue("PROJ_BOND_WRITEDWN_PCT_CURR_FACE");
                fields.appendValue("PROJ_COLL_WRITEDWN_PCT_CURR_FACE");

				//set overrides
				Element overrides = request.getElement("overrides");
				Element overrideField = overrides.appendElement();

				overrideField.setElement("fieldId", overridePriceYieldField);
				// set override value
				overrideField.setElement("value", String.valueOf(price)); 
				
				ApiTableModel model = (ApiTableModel)scenarioTable.getModel();
				int numRow = model.getRowCount();
				
				for (int r=0; r<numRow; ++r) {
					overrideField = overrides.appendElement();
					String field = (String)scenarioTable.getValueAt(r, 0);
					overrideField.setElement("fieldId", fieldMap.get(field));
					String value = (String)scenarioTable.getValueAt(r, scenario);
					overrideField.setElement("value", value); 
				}
						
				d_session.sendRequest(request, new CorrelationID("Analytic:" + 
						String.valueOf(col) + ":" + String.valueOf(scenario)));
				
				//System.out.println(request);
			}
			++col;
		}
	}

	/* 
	 ***********************************************************************************
	 * This method creates a new session and submit the request
	 ***********************************************************************************
	 */
	private void getData() throws Exception
	{
		clearData();
		if(!createSession()) {
			System.out.println("Failed to start session.");
			return;
		}

		System.out.println("Connected successfully!");

		if (!d_session.openService("//blp/refdata")) {
			System.out.println("Failed to open //blp/refdata");
			return;
		}

		Service refDataService = d_session.getService("//blp/refdata");
		Request request = refDataService.createRequest("ReferenceDataRequest");

		//append securities to request
		Element securities = request.getElement("securities");
		securities.appendValue(txtSec.getText().trim().toUpperCase() + " Mtge");

		Element fields = request.getElement("fields");
		//historical speeds fields
		ApiTableModel model = (ApiTableModel)histSpeedTable.getModel();
		int numRow = model.getRowCount();
		for (int i=0;i<numRow;++i) {
			String fld = (String)model.getValueAt(i, 0);
			String val = fieldMap.get(fld);
			fields.appendValue(val);
		}
		//bond data fields
		model = (ApiTableModel)bondDataTable.getModel();
		numRow = model.getRowCount();
		for (int i=0;i<numRow;++i) {
			String fld = (String)model.getValueAt(i, 0);
			String val = fieldMap.get(fld);
			fields.appendValue(val);
		} 
		//collateral data fields
		model = (ApiTableModel)collateralTable.getModel();
		numRow = model.getRowCount();
		for (int i=0;i<numRow;++i) {
			String fld = (String)model.getValueAt(i, 0);
			String val = fieldMap.get(fld);
			fields.appendValue(val);
		} 
		//System.out.println(request);
		d_session.sendRequest(request, new CorrelationID("Data"));		
	}

	/*
	 * ***************************************************************
	 * main
	 * ***************************************************************
	 */
	public static void main(String[] args) {
		if(app==null) {
			app = new NonAgencyBondExample();
		}
	}

	/* 
	 ***********************************************************************************
	 * This method will process the misc events
	 ***********************************************************************************
	 */
	private boolean processMiscEvents(Event event, Session session)
	{
		MessageIterator msgIter = event.messageIterator();
		while (msgIter.hasNext()) {
			Message msg = msgIter.next();
			String msgType = msg.messageType().toString();
			if (msgType.equals("SessionStarted")
					|| msgType.equals("SessionTerminated")
					|| msgType.equals("ServiceOpened")) {
				//Don't have to do anything here
			}
			else {
				System.out.println("Failure: " + msg);
			}
		}
		return true;
	}

	private void processAnalytic(Message msg) {
		//System.out.println(msg);
		String corrId = (String)msg.correlationID().object();

		String [] str_token = corrId.split(":");

		int start_row = 1;
		int col = Integer.parseInt(str_token[2]);		
		switch (Integer.parseInt(str_token[1])) {
			case 1: start_row = 1; break;
			case 2: start_row = 18; break;
			case 3: start_row = 35; break;
		}

		Element securityDataArray = msg.getElement(SECURITY_DATA);
		int numSecurities = securityDataArray.numValues();
		for (int i = 0; i < numSecurities; ++i) {
			Element securityData = securityDataArray.getValueAsElement(i);
			if (securityData.hasElement("securityError") && showSecurityErrorMsg)
            {
                JOptionPane.showMessageDialog(app.getContentPane(), "SECURITY FAILED: " + securityData.getElement(SECURITY_ERROR).toString());
                showSecurityErrorMsg = false;
                continue;
            }

			Element fieldData = securityData.getElement(FIELD_DATA);

			//History Speeds data fields
			ApiTableModel model = (ApiTableModel)analyticDataTable.getModel();
			//int totRow = histSpeedTable.getRowCount();
			for (int j = start_row; j < start_row+15; ++j) {
				String fld = fieldMap.get(model.getValueAt(j, 0)).trim();
				if (fieldData.hasElement(fld, true))
				{
					Element item = fieldData.getElement(fld);
					String v = item.getValueAsString();	
					//System.out.println(item.datatype() + " " + item.name() + " " + v);
					model.setValueAt(v, j, col);				
				}
			}
		}

	}

	private void processData(Message msg) {
		Element securityDataArray = msg.getElement(SECURITY_DATA);
		int numSecurities = securityDataArray.numValues();
		for (int i = 0; i < numSecurities; ++i) {
			Element securityData = securityDataArray.getValueAsElement(i);
			
			if (securityData.hasElement("securityError"))
            {
                JOptionPane.showMessageDialog(app.getContentPane(), "SECURITY FAILED: " + securityData.getElement(SECURITY_ERROR).toString());
                continue;
            }

			Element fieldData = securityData.getElement(FIELD_DATA);

			//History Speeds data fields
			ApiTableModel model = (ApiTableModel)histSpeedTable.getModel();
			int totRow = histSpeedTable.getRowCount();
			for (int j = 0; j < totRow; ++j) {
				String fld = fieldMap.get(model.getValueAt(j, 0));
				if (fieldData.hasElement(fld, true))
				{
					Element item = fieldData.getElement(fld);
					String v = item.getValueAsString();
					model.setValueAt(v, j, 1);
				}
			}

			//Bond Data fields
			model = (ApiTableModel)bondDataTable.getModel();
			totRow = bondDataTable.getRowCount();
			for (int j = 0; j < totRow; ++j) {
				String fld = fieldMap.get(model.getValueAt(j, 0));
				if (fieldData.hasElement(fld, true))
				{
					Element item = fieldData.getElement(fld);
					String v = item.getValueAsString();
					model.setValueAt(v, j, 1);
				}
			}

			//Collateral Data fields
			model = (ApiTableModel)collateralTable.getModel();
			totRow = collateralTable.getRowCount();
			for (int j = 0; j < totRow; ++j) {
				String fld = fieldMap.get(model.getValueAt(j, 0));
				if (fieldData.hasElement(fld, true))
				{
					Element item = fieldData.getElement(fld);
					String v = item.getValueAsString();
					model.setValueAt(v, j, 1);
				}
				else {
					model.setValueAt("-", j, 1);
				}
			}

		}
	}

	/* 
	 ***********************************************************************************
	 * This method will process the data events
	 ***********************************************************************************
	 */
	private void processRequestDataEvent(Event event, Session session)
	{
		MessageIterator msgIter = event.messageIterator();
		while (msgIter.hasNext()) {
			Message msg = msgIter.next();
			//System.out.println(msg);
			String corrId = (String)msg.correlationID().object();
			//System.out.println("Correlation ID: " + msg.correlationID());

			if(corrId.equals("Data")) {
				processData(msg);
			}
			else if (corrId.startsWith("Analytic")) {
				processAnalytic(msg);
			}
		}
	}

	private void clear(){
		String name = dataTabbedPane.getTitleAt(dataTabbedPane.getSelectedIndex());
			if(name.equals("Data")) {
				clearData();
			}
			else if(name.equals("Analytic")) {
				clearAnalyticData();
			}
	}
	
	private void clearData(){
		ApiTableModel model = (ApiTableModel)histSpeedTable.getModel();
		int numRow = model.getRowCount();
				
		for (int r=0; r<numRow; ++r) {
			histSpeedTable.setValueAt("", r, 1);
		}
		
		model = (ApiTableModel)bondDataTable.getModel();
		numRow = model.getRowCount();
		for (int r=0; r<numRow; ++r) {
			bondDataTable.setValueAt("", r, 1);
		}
		
		model = (ApiTableModel)collateralTable.getModel();
		numRow = model.getRowCount();
		for (int r=0; r<numRow; ++r) {
			collateralTable.setValueAt("", r, 1);
		}
	}
	
	private void clearAnalyticData(){
		ApiTableModel model = (ApiTableModel)analyticDataTable.getModel();
		int numRow = model.getRowCount();
		int numCol = model.getColumnCount();
		
		for (int r=0; r<numRow; ++r){
			for (int c=1; c<numCol; ++c){
				analyticDataTable.setValueAt("", r, c);				
			}
		}
	}
	
	public static boolean isValidNumber(String s) {
		try {
			Double.parseDouble(s);
			return true;
		}
		catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	private void calculateScenariosAverage(){
		ApiTableModel model = (ApiTableModel)analyticDataTable.getModel();
		int numRow = model.getRowCount();
		
		for (int r=1; r<numRow; ++r){
			double sum=0;
			for (int c=1; c<4; ++c){
				String v = (String)analyticDataTable.getValueAt(r, c);
				if(isValidNumber(v)) {
					sum=sum+Double.parseDouble(v);
				}
			}
			if(sum!=0.0) {
				analyticDataTable.setValueAt(sum/3, r, 4);
			}		
		}
	}
	
	/* 
	 ***********************************************************************************
	 * This class is used to implement Asynchronous request.
	 ***********************************************************************************
	 */
	class ApiEventHandler implements EventHandler
	{	
		public void processEvent(Event event, Session session) {	
			try
			{
				switch (event.eventType().intValue())
				{
				case Event.EventType.Constants.RESPONSE:
					processRequestDataEvent(event, session);
					calculateScenariosAverage();
					break;
				case Event.EventType.Constants.PARTIAL_RESPONSE:
					processRequestDataEvent(event, session);				
					break;
				default:
					processMiscEvents(event, session);
				break;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/*
	 * ***************************************************************
	 * This class is used to close and exit the app
	 * ***************************************************************
	 */
	class WindowCloser extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			System.exit(0);
		}
	}
}
