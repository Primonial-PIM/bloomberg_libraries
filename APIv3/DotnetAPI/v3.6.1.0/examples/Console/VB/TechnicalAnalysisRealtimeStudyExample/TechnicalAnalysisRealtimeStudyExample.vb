' Copyright 2012. Bloomberg Finance L.P.
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of
' this software and associated documentation files (the "Software"), to deal in
' the Software without restriction, including without limitation the rights to
' use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
' of the Software, and to permit persons to whom the Software is furnished to do
' so, subject to the following conditions:  The above copyright notice and this
' permission notice shall be included in all copies or substantial portions of
' the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
' EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
' OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
' ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.

'** TechnicalAnalysisRealtimeStudyExample.vb
'**
'** This Example shows how to use Technical Analysis service ("//blp/tasvc")
'** to retrieve realtime data for specified study request.
'** 
' --------------------------------------------------------------------------
Imports System.Collections.Generic
Imports System.Collections
Imports System.Text
Imports [Event] = Bloomberglp.Blpapi.Event
Imports Bloomberglp.Blpapi

Namespace Bloomberglp.Blpapi.Examples

    Module TechnicalAnalysisRealtimeStudyExample

        Private d_host As String
        Private d_port As Integer

        Sub Main(ByVal args As String())
            System.Console.WriteLine("Technical Analysis Realtime Study Example")

            d_host = "localhost"
            d_port = 8194

            If Not parseCommandLine(args) Then
                Return
            End If

            Dim sessionOptions As New SessionOptions()
            sessionOptions.ServerHost = d_host
            sessionOptions.ServerPort = d_port

            System.Console.WriteLine("Connecting to " & d_host & ":" & d_port)
            Dim session As New Session(sessionOptions)
            Dim sessionStarted As Boolean = session.Start()

            If Not sessionStarted Then
                System.Console.WriteLine("Failed to start session.")
                Return
            End If
            If Not session.OpenService("//blp/tasvc") Then
                System.Console.[Error].WriteLine("Failed to open //blp/tasvc")
                Return
            End If

            Dim tasvcService As Service = session.GetService("//blp/tasvc")

            sessionOptions.DefaultSubscriptionService = "//blp/tasvc"

            Dim subscriptions As New System.Collections.Generic.List(Of Subscription)()

            ' Create Technical Analysis WLPR Study Subscription
            Dim wlprSubscription As Subscription = createWLPRStudySubscription()
            System.Console.WriteLine("Subscribing to: " + wlprSubscription.SubscriptionString)
            subscriptions.Add(wlprSubscription)

            ' Create Technical Analysis MAO Study Subscription
            Dim maoSubscription As Subscription = createMAOStudySubscription()
            System.Console.WriteLine("Subscribing to: " + maoSubscription.SubscriptionString)
            subscriptions.Add(maoSubscription)

            ' Create Technical Analysis EMAVG Study Subscription
            Dim emavgSubscription As Subscription = createEMAVGStudySubscription()
            System.Console.WriteLine("Subscribing to: " + emavgSubscription.SubscriptionString)
            subscriptions.Add(emavgSubscription)

            ' NOTE: User must be entitled to receive realtime data for securities subscribed
            session.Subscribe(subscriptions)

            ' wait for events from session.
            eventLoop(session)

        End Sub

        ' Create Technical Analysis WLPR Study Subscription 
        Private Function createWLPRStudySubscription() As Subscription
            Dim wlprSubscription As Subscription
            Dim fields As New List(Of String)
            fields.Add("WLPR")

            Dim override As New List(Of String)
            override.Add("priceSourceClose=LAST_PRICE")
            override.Add("priceSourceHigh=HIGH")
            override.Add("priceSourceLow=LOW")
            override.Add("periodicitySelection=DAILY")
            override.Add("period=14")

            wlprSubscription = New Subscription("IBM US Equity", _
                                                fields, _
                                                override, _
                                                New CorrelationID("IBM US Equity_WLPR"))

            Return wlprSubscription

        End Function

        ' Create Technical Analysis MAO Study Subscription 
        ' Equivalent BTP formula for subscription below
        Private Function createMAOStudySubscription() As Subscription
            Dim maoSubscription As Subscription
            Dim fields As New List(Of String)
            fields.Add("MAO")

            Dim override As New List(Of String)
            override.Add("priceSourceClose1=LAST_PRICE")
            override.Add("priceSourceClose2=LAST_PRICE")
            override.Add("maPeriod1=6")
            override.Add("maPeriod2=36")
            override.Add("maType1=Simple")
            override.Add("maType2=Simple")
            override.Add("oscType=Difference")
            override.Add("periodicitySelection=DAILY")
            override.Add("sigPeriod=9")
            override.Add("sigType=Simple")

            maoSubscription = New Subscription("VOD LN Equity", _
                                                fields, _
                                                override, _
                                                New CorrelationID("VOD LN Equity_MAO"))

            Return maoSubscription

        End Function

        ' Create Technical Analysis EMAVG Study Subscription 
        ' Equivalent BTP formula for subscription below
        Private Function createEMAVGStudySubscription() As Subscription
            Dim emavgSubscription As Subscription
            Dim fields As New List(Of String)
            fields.Add("EMAVG")

            Dim override As New List(Of String)
            override.Add("priceSourceClose=LAST_PRICE")
            override.Add("periodicitySelection=DAILY")
            override.Add("period=14")

            emavgSubscription = New Subscription("6758 JT Equity", _
                                                fields, _
                                                override, _
                                                New CorrelationID("6758 JT Equity_EMAVG"))

            Return emavgSubscription

        End Function

        ''' <summary>
        ''' Polls for an event or a message in an event loop
        ''' and Processes the event generated
        ''' </summary>
        ''' <param name="session"></param>
        ''' <remarks></remarks>
        Private Sub eventLoop(ByVal session As Session)
            While (True)
                Dim eventObj As [Event] = session.NextEvent()
                Dim msg As Message
                For Each msg In eventObj.GetMessages()
                    If eventObj.Type = [Event].EventType.SUBSCRIPTION_STATUS Then
                        System.Console.WriteLine("Processing SUBSCRIPTION_STATUS")
                        Dim topic As String = CType(msg.CorrelationID.Object, String)
                        System.Console.WriteLine(System.DateTime.Now.ToString("s") & _
                                                ": " & _
                                                topic & _
                                                ": " & _
                                                msg.AsElement.ToString())
                    ElseIf eventObj.Type = [Event].EventType.SUBSCRIPTION_DATA Then
                        System.Console.WriteLine(vbCrLf & "Processing SUBSCRIPTION_DATA")
                        Dim topic As String = CType(msg.CorrelationID.Object, String)
                        System.Console.WriteLine(System.DateTime.Now.ToString("s") & _
                                                 ": " & _
                                                topic & _
                                                " - " & _
                                                msg.MessageType.ToString())
                        Dim field As Element
                        For Each field In msg.Elements
                            System.Console.WriteLine(vbTab & vbTab & field.Name.ToString() & _
                                                    " = " & _
                                                    field.GetValueAsString())
                        Next
                    Else
                        System.Console.WriteLine(msg.AsElement)
                    End If
                Next
            End While
        End Sub
        ''' <summary>
        ''' Parses the command line arguments
        ''' </summary>
        ''' <param name="args"></param>
        ''' <returns></returns>
        Private Function parseCommandLine(ByVal args As String()) As Boolean
            For i As Integer = 0 To args.Length - 1
                If String.Compare(args(i), "-ip", True) = 0 And i + 1 < args.Length Then
                    d_host = args(i + 1)
                ElseIf String.Compare(args(i), "-p", True) = 0 And i + 1 < args.Length Then
                    Dim outPort As Integer = 0
                    If Integer.TryParse(args(i + 1), outPort) Then
                        d_port = outPort
                    End If
                End If
                If String.Compare(args(i), "-h", True) = 0 Then
                    printUsage()
                    Return False
                End If
            Next
            Return True
        End Function

        ''' <summary>
        ''' Print usage of the Program
        ''' </summary>
        Private Sub printUsage()
            System.Console.WriteLine("Usage:")
            System.Console.WriteLine("  Technical Analysis Realtime Study Example ")
            System.Console.WriteLine("        [-ip    <ipAddress      = localhost>")
            System.Console.WriteLine("        [-p     <tcpPort        = 8194>")
        End Sub
    End Module
End Namespace
