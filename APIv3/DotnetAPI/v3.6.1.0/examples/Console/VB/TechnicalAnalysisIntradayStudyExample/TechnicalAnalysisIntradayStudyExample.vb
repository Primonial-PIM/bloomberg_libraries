' Copyright 2012. Bloomberg Finance L.P.
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of
' this software and associated documentation files (the "Software"), to deal in
' the Software without restriction, including without limitation the rights to
' use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
' of the Software, and to permit persons to whom the Software is furnished to do
' so, subject to the following conditions:  The above copyright notice and this
' permission notice shall be included in all copies or substantial portions of
' the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
' EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
' OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
' ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.

'** TechnicalAnalysisIntradayStudyExample.vb
'**
'** This Example shows how to use Technical Analysis service ("//blp/tasvc")
'** to retrieve Intraday data for specified study request.
'** 
' --------------------------------------------------------------------------
Imports System.Collections.Generic
Imports System.Text
Imports [Event] = Bloomberglp.Blpapi.Event
Imports Bloomberglp.Blpapi

Namespace Bloomberglp.Blpapi.Examples

    Module TechnicalAnalysisIntradayStudyExample

        Private ReadOnly SECURITY_NAME As New Name("securityName")
        Private ReadOnly SECURITY As New Name("security")
        Private ReadOnly STUDY_DATA As New Name("studyData")
        Private ReadOnly RESPONSE_ERROR As New Name("responseError")
        Private ReadOnly SECURITY_ERROR As New Name("securityError")
        Private ReadOnly FIELD_EXCEPTIONS As New Name("fieldExceptions")
        Private ReadOnly FIELD_ID As New Name("fieldId")
        Private ReadOnly ERROR_INFO As New Name("errorInfo")
        Private ReadOnly CATEGORY As New Name("category")
        Private ReadOnly MESSAGE As New Name("message")

        Private d_host As String
        Private d_port As Integer


        Sub Main(ByVal args As String())
            System.Console.WriteLine("Technical Analysis Intraday Study Example")

            d_host = "localhost"
            d_port = 8194

            If Not parseCommandLine(args) Then
                Return
            End If

            Dim sessionOptions As New SessionOptions()
            sessionOptions.ServerHost = d_host
            sessionOptions.ServerPort = d_port

            System.Console.WriteLine("Connecting to " & d_host & ":" & d_port)
            Dim session As New Session(sessionOptions)
            Dim sessionStarted As Boolean = session.Start()

            If Not sessionStarted Then
                System.Console.WriteLine("Failed to start session.")
                Return
            End If
            If Not session.OpenService("//blp/tasvc") Then
                System.Console.[Error].WriteLine("Failed to open //blp/tasvc")
                Return
            End If

            Dim tasvcService As Service = session.GetService("//blp/tasvc")

            ' Create DMI Study Request
            Dim dmiStudyRequest As Request = createDMIStudyRequest(tasvcService)
            System.Console.WriteLine("Sending Request: " & _
                                        dmiStudyRequest.AsElement().ToString())
            session.SendRequest(dmiStudyRequest, Nothing)
            ' wait for events from session.
            eventLoop(session)

            ' Create SMAVG Study Request
            Dim smavgStudyRequest As Request = createSMAVGStudyRequest(tasvcService)
            System.Console.WriteLine("Sending Request: " & _
                                        smavgStudyRequest.AsElement().ToString())
            session.SendRequest(smavgStudyRequest, Nothing)
            ' wait for events from session.
            eventLoop(session)

            session.Stop()
            System.Console.WriteLine("Press ENTER to quit")
            System.Console.Read()
        End Sub

        ' Create Technical Analysis Intraday - DMI Study Request
        Private Function createDMIStudyRequest(ByVal tasvcService As Service) As Request

            Dim request As Request = tasvcService.CreateRequest("studyRequest")
            Dim priceSource As Element = request.GetElement("priceSource")
            ' set security name
            priceSource.SetElement("securityName", "IBM US Equity")

            Dim dataRange As Element = priceSource.GetElement("dataRange")
            dataRange.SetChoice("intraday")

            Dim intraday As Element = dataRange.GetElement("intraday")
            ' intraday event type
            intraday.SetElement("eventType", "TRADE")
            ' intraday interval
            intraday.SetElement("interval", 60)
            ' set study start/end date
            intraday.SetElement("startDate", "2010-05-26T13:30:00")
            intraday.SetElement("endDate", "2010-05-27T13:30:00")

            Dim studyAttributes As Element = request.GetElement("studyAttributes")
            studyAttributes.SetChoice("dmiStudyAttributes")

            ' DMI study example - set study attributes
            Dim dmiStudy As Element = studyAttributes.GetElement("dmiStudyAttributes")
            ' DMI study interval
            dmiStudy.SetElement("period", 14)
            ' set intraday data price sources for study
            dmiStudy.SetElement("priceSourceHigh", "high")
            dmiStudy.SetElement("priceSourceClose", "close")
            dmiStudy.SetElement("priceSourceLow", "low")

            Return request
        End Function

        ' Create Technical Analysis Intraday - SMAVG Study Request
        Private Function createSMAVGStudyRequest(ByVal tasvcService As Service) As Request
            Dim request As Request = tasvcService.CreateRequest("studyRequest")
            Dim priceSource As Element = request.GetElement("priceSource")
            ' set security name
            priceSource.SetElement("securityName", "IBM US Equity")

            Dim dataRange As Element = priceSource.GetElement("dataRange")
            dataRange.SetChoice("intraday")

            Dim intraday As Element = dataRange.GetElement("intraday")
            ' intraday event type
            intraday.SetElement("eventType", "TRADE")
            ' intraday interval
            intraday.SetElement("interval", 60)
            ' set study start/end date
            intraday.SetElement("startDate", "2010-05-26T13:30:00")
            intraday.SetElement("endDate", "2010-05-27T13:30:00")

            Dim studyAttributes As Element = request.GetElement("studyAttributes")
            studyAttributes.SetChoice("smavgStudyAttributes")

            ' SMAVG study example - set study attributes
            Dim smavgStudy As Element = studyAttributes.GetElement("smavgStudyAttributes")
            ' SMAVG study interval
            smavgStudy.SetElement("period", 14)
            ' set intraday data price sources for study
            smavgStudy.SetElement("priceSourceClose", "close")

            Return request
        End Function

        Private Sub eventLoop(ByVal session As Session)
            Dim done As Boolean = False
            While Not done
                Dim eventObj As [Event] = session.NextEvent()
                If eventObj.Type = [Event].EventType.PARTIAL_RESPONSE Then
                    System.Console.WriteLine("Processing Partial Response")
                    processResponseEvent(eventObj)
                ElseIf eventObj.Type = [Event].EventType.RESPONSE Then
                    System.Console.WriteLine("Processing Response")
                    processResponseEvent(eventObj)
                    done = True
                Else
                    For Each msg As Message In eventObj.GetMessages()
                        System.Console.WriteLine(msg.AsElement)
                        If eventObj.Type = [Event].EventType.SESSION_STATUS Then
                            If msg.MessageType.Equals("SessionTerminated") Then
                                System.Console.WriteLine("SessionTerminated...Exiting")
                                Environment.Exit(0)
                            End If
                        End If
                    Next
                End If
            End While
        End Sub

        ''' <summary>
        ''' Function to handle response event
        ''' </summary>
        ''' <param name="eventObj"></param>
        Private Sub processResponseEvent(ByVal eventObj As [Event])
            For Each msg As Message In eventObj.GetMessages()
                If msg.HasElement(RESPONSE_ERROR) Then
                    printErrorInfo("REQUEST FAILED: ", msg.GetElement(RESPONSE_ERROR))
                    Continue For
                End If

                Dim security As Element = msg.GetElement(SECURITY_NAME)
                Dim ticker As String = security.GetValueAsString()
                System.Console.WriteLine(vbLf & "Ticker: " & ticker)
                If security.HasElement("securityError") Then
                    printErrorInfo(vbTab & "SECURITY FAILED: ", _
                                    security.GetElement(SECURITY_ERROR))
                    Continue For
                End If

                Dim fields As Element = msg.GetElement(STUDY_DATA)
                If fields.NumValues > 0 Then
                    Dim numValues As Integer = fields.NumValues
                    For j As Integer = 0 To numValues - 1
                        Dim field As Element = fields.GetValueAsElement(j)
                        For k As Integer = 0 To field.NumElements - 1
                            Dim element As Element = field.GetElement(k)
                            System.Console.WriteLine(vbTab + element.Name.ToString & _
                                                    " = " & element.GetValueAsString())
                        Next
                        System.Console.WriteLine("")
                    Next
                End If
                System.Console.WriteLine("")
                Dim fieldExceptions As Element = msg.GetElement(FIELD_EXCEPTIONS)
                If fieldExceptions.NumValues > 0 Then
                    System.Console.WriteLine("FIELD" & vbTab & vbTab & "EXCEPTION")
                    System.Console.WriteLine("-----" & vbTab & vbTab & "---------")
                    For k As Integer = 0 To fieldExceptions.NumValues - 1
                        Dim fieldException As Element = fieldExceptions.GetValueAsElement(k)
                        printErrorInfo(fieldException.GetElementAsString(FIELD_ID) & _
                                        vbTab & vbTab, fieldException.GetElement(ERROR_INFO))
                    Next
                End If
            Next
        End Sub

        ''' <summary>
        '''
        ''' </summary>
        ''' <param name="leadingStr"></param>
        ''' <param name="errorInfo"></param>
        Private Sub printErrorInfo(ByVal leadingStr As String, ByVal errorInfo As Element)
            System.Console.WriteLine(leadingStr & errorInfo.GetElementAsString(CATEGORY) & _
                                        " (" & errorInfo.GetElementAsString(MESSAGE) & ")")
        End Sub

        ''' <summary>
        ''' Parses the command line arguments
        ''' </summary>
        ''' <param name="args"></param>
        ''' <returns></returns>
        Private Function parseCommandLine(ByVal args As String()) As Boolean
            For i As Integer = 0 To args.Length - 1
                If String.Compare(args(i), "-ip", True) = 0 And i + 1 < args.Length Then
                    d_host = args(i + 1)
                ElseIf String.Compare(args(i), "-p", True) = 0 And i + 1 < args.Length Then
                    Dim outPort As Integer = 0
                    If Integer.TryParse(args(i + 1), outPort) Then
                        d_port = outPort
                    End If
                End If
                If String.Compare(args(i), "-h", True) = 0 Then
                    printUsage()
                    Return False
                End If
            Next
            Return True
        End Function

        ''' <summary>
        ''' Print usage of the Program
        ''' </summary>
        Private Sub printUsage()
            System.Console.WriteLine("Usage:")
            System.Console.WriteLine("  Technical Analysis Intraday Study Example ")
            System.Console.WriteLine("        [-ip    <ipAddress      = localhost>")
            System.Console.WriteLine("        [-p     <tcpPort        = 8194>")
        End Sub
    End Module
End Namespace
