' Copyright 2012. Bloomberg Finance L.P.
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of
' this software and associated documentation files (the "Software"), to deal in
' the Software without restriction, including without limitation the rights to
' use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
' of the Software, and to permit persons to whom the Software is furnished to do
' so, subject to the following conditions:  The above copyright notice and this
' permission notice shall be included in all copies or substantial portions of
' the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
' EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
' OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
' ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.

' *****************************************************************************
' Module1.vb: 
'	This program demonstrates how to make a subscription to Page Based data. 
'   It uses the Market Bar service(//blp/pagedata) 
'	provided by API. Program does the following:
'		1. Establishes a session which facilitates connection to the bloomberg 
'		   network.
'		2. Initiates the Page data Service(//blp/pagedata) for realtime
'		   data.
''		3. Creates and sends the request via the session.
'			- Creates a subscription list
'			- Adds Page data topic to subscription list.
'			- Subscribes to realtime Page data
'		4. Event Handling of the responses received.
'       5. Parsing of the message data.
' Usage: 
'         	-t			<Topic  	= "0708/012/0001">
'                                   i.e."Broker ID/Category/Page Number"
'      		-ip 		<ipAddress	= localhost>
'     		-p 			<tcpPort	= 8194>
'
'   example usage:
'	PageDataExample -t "0708/012/0001" -ip localhost -p 8194
'
'	Prints the response on the console of the command line requested data
'******************************************************************************/

Imports Bloomberglp.Blpapi
Imports Message = Bloomberglp.Blpapi.Message
Imports Name = Bloomberglp.Blpapi.Name
Imports Service = Bloomberglp.Blpapi.Service
Imports Session = Bloomberglp.Blpapi.Session
Imports ArrayList = System.Collections.ArrayList
Imports Thread = System.Threading.Thread
Imports Hashtable = System.Collections.Hashtable
Imports System.Collections.Generic
Imports StringBuilder = System.Text.StringBuilder

Namespace BB.Blpapi.Examples

    Module PageDataExample
        Private ReadOnly _EXCEPTIONS As New Name("exceptions")
        Private ReadOnly _FIELD_ID As New Name("fieldId")
        Private ReadOnly _REASON As New Name("reason")
        Private ReadOnly _CATEGORY As New Name("category")
        Private ReadOnly _DESCRIPTION As New Name("description")
        Private ReadOnly _PAGEUPDATE As New Name("PageUpdate")
        Private ReadOnly _ROWUPDATE As New Name("rowUpdate")
        Private ReadOnly _NUMROWS As New Name("numRows")
        Private ReadOnly _NUMCOLS As New Name("numCols")
        Private ReadOnly _ROWNUM As New Name("rowNum")
        Private ReadOnly _SPANUPDATE As New Name("spanUpdate")
        Private ReadOnly _STARTCOL As New Name("startCol")
        Private ReadOnly _LENGTH As New Name("length")
        Private ReadOnly _TEXT As New Name("text")

        Private d_host As String
        Private d_port As Integer
        Private d_topics As ArrayList
        Private d_topicTable As Hashtable
        Private d_sessionOptions As SessionOptions
        Private d_session As Session

        Sub Main(ByVal args As String())
            System.Console.WriteLine("Page Data Event Handler Example")

            d_host = "localhost"
            d_port = 8194
            d_topicTable = New Hashtable()
            d_topics = New ArrayList()

            If Not parseCommandLine(args) Then
                Return
            End If

            d_sessionOptions = New SessionOptions()

            d_sessionOptions.ServerHost = d_host
            d_sessionOptions.ServerPort = d_port

            Dim success As Boolean = createSession()

            If Not success Then
                Return
            End If

            System.Console.WriteLine("Connected successfully" & vbLf)

            If Not d_session.OpenService("//blp/pagedata") Then
                System.Console.Error.WriteLine("Failed to open service //blp/pagedata")
                d_session.Stop()
                Return
            End If

            System.Console.WriteLine("Subscribing..." & vbLf)
            subscribe()

            ' wait for enter key to exit application 
            System.Console.Read()

            d_session.Stop()
            System.Console.WriteLine("Exiting.")

        End Sub

        '*****************************************************************************
        '   Function    : createSession
        '  Description : This function creates a session object and opens the market 
        '                 bar service.  Returns false on failure of either.
        'Arguments: none()
        'Returns: bool()
        '*****************************************************************************/

        Private Function createSession() As Boolean

            If d_session IsNot Nothing Then
                d_session.Stop()
            End If

            System.Console.WriteLine("Connecting to " + d_host + ":" + d_port.ToString())
            d_session = New Session(d_sessionOptions, New EventHandler(AddressOf processEvent))
            Return d_session.Start()
        End Function

        '*****************************************************************************
        '   Function    : subscribe
        '  Description : This function will subscribe to page data.
        'Arguments: none()
        'Returns: none
        '*****************************************************************************/
        Private Sub subscribe()
            Dim subscriptions As List(Of Subscription) = New List(Of Subscription)
            Dim fields As List(Of String) = New List(Of String)

            d_topicTable.Clear()
            fields.Add("6-23")
            ' Following commented code shows some of the sample values 
            ' that can be used for field other than above
            ' e.g. fields.Add("1");
            '      fields.Add("1,2,3");
            '      fields.Add("1,6-10,15,16");

            For Each topic As String In d_topics
                subscriptions.Add(New Subscription("//blp/pagedata/" + topic, _
                                                    fields, _
                                                    New CorrelationID(topic)))
                d_topicTable.Add(topic, New ArrayList())
            Next
            d_session.Subscribe(subscriptions)
        End Sub
        '*****************************************************************************
        'Function    : processEvent
        'Description : Processes session events
        'Arguments   : Event, Session
        'Returns     : void
        '*****************************************************************************/

        Public Sub processEvent(ByVal eventObj As [Event], ByVal session As Session)

            Try
                Select Case eventObj.Type
                    Case [Event].EventType.SUBSCRIPTION_DATA
                        processSubscriptionDataEvent(eventObj, session)
                        Exit Select
                    Case [Event].EventType.SUBSCRIPTION_STATUS
                        processSubscriptionStatus(eventObj, session)
                        Exit Select
                    Case Else
                        processMiscEvents(eventObj, session)
                        Exit Select
                End Select
            Catch e As System.Exception
                System.Console.WriteLine(e.ToString())
            End Try

        End Sub
        '*****************************************************************************
        'Function    : processSubscriptionStatus
        'Description : Processes subscription status messages returned from Bloomberg
        'Arguments   : Event, Session
        'Returns     : void
        '*****************************************************************************/

        Private Sub processSubscriptionStatus(ByVal eventObj As [Event], ByVal session As Session)

            System.Console.WriteLine("Processing SUBSCRIPTION_STATUS")

            For Each msg As Message In eventObj.GetMessages()

                Dim topic As String = DirectCast(msg.CorrelationID.[Object], String)
                System.Console.WriteLine(System.DateTime.Now.ToString("s") + ": " + _
                topic.ToString() + " - " + msg.MessageType.ToString())
                If msg.HasElement(_REASON) Then
                    ' This can occur on SubscriptionFailure. 
                    Dim reason1 As Element = msg.GetElement(_REASON)
                    System.Console.WriteLine _
                    (vbTab + reason1.GetElement(_CATEGORY).GetValueAsString() + _
                    ": " + reason1.GetElement(_DESCRIPTION).GetValueAsString())
                End If
                If msg.HasElement(_EXCEPTIONS) Then
                    ' This can occur on SubscriptionStarted if at least 
                    ' one field is good while the rest are bad. 
                    Dim exceptions1 As Element = msg.GetElement(_EXCEPTIONS)
                    For i As Integer = 0 To exceptions1.NumValues - 1
                        Dim exInfo As Element = exceptions1.GetValueAsElement(i)
                        Dim fieldId As Element = exInfo.GetElement(_FIELD_ID)
                        Dim reason1 As Element = exInfo.GetElement(_REASON)
                        System.Console.WriteLine(vbTab + fieldId.GetValueAsString() + _
                            ": " + reason1.GetElement(_CATEGORY).GetValueAsString())
                    Next
                End If
                System.Console.WriteLine("")
            Next

        End Sub

        '*****************************************************************************
        'Function    : processSubscriptionDataEvent
        'Description : Processes all field data returned from Bloomberg
        'Arguments   : Event, Session
        'Returns     : void
        '*****************************************************************************/

        Private Sub processSubscriptionDataEvent(ByVal eventObj As [Event], ByVal session As Session)

            System.Console.WriteLine("Processing SUBSCRIPTION_DATA")

            For Each msg As Message In eventObj.GetMessages()
                Dim topic As String = DirectCast(msg.CorrelationID.Object, String)
                System.Console.WriteLine(System.DateTime.Now.ToString("s") + ": " + _
                topic + " - " + msg.MessageType.ToString())

                If (msg.MessageType.Equals("PageUpdate")) Then
                    processPageElement(msg.AsElement, topic)
                ElseIf (msg.MessageType.Equals("RowUpdate")) Then
                    processRowElement(msg.AsElement, topic)
                End If
            Next

        End Sub

        '*****************************************************************************
        'Function    : showUpdatedPage
        'Description : Show whole page content for specific topic
        'Arguments   : topic
        'Returns     : void
        '*****************************************************************************/
        Private Sub showUpdatedPage(ByVal topic As String)
            Dim rowList As ArrayList = CType(d_topicTable(topic), ArrayList)
            For Each str As StringBuilder In rowList
                System.Console.WriteLine(str.ToString())
            Next str
        End Sub

        '*****************************************************************************
        'Function    : processPageElement
        'Description : Process PageUpdate Event/PageUpdate Element
        'Arguments   : pageElement, topic
        'Returns     : void
        '*****************************************************************************/
        Private Sub processPageElement(ByVal pageElement As Element, ByVal topic As String)
            Dim eleNumRows As Element = pageElement.GetElement(_NUMROWS)
            Dim numRows As Integer = eleNumRows.GetValueAsInt32()
            Dim eleNumCols As Element = pageElement.GetElement(_NUMCOLS)
            Dim numCols As Integer = eleNumCols.GetValueAsInt32()
            System.Console.WriteLine("Page Contains " & numRows & " Rows & " & numCols & " Columns")
            Dim eleRowUpdates As Element = pageElement.GetElement(_ROWUPDATE)
            Dim numRowUpdates As Integer = eleRowUpdates.NumValues
            System.Console.WriteLine("Processing " & numRowUpdates & " RowUpdates")
            For i As Integer = 0 To numRowUpdates - 1
                Dim rowUpdate As Element = eleRowUpdates.GetValueAsElement(i)
                processRowElement(rowUpdate, topic)
            Next i
        End Sub

        '*****************************************************************************
        'Function    : processRowElement
        'Description : Process RowUpdate Event/ rowUpdate Element
        'Arguments   : pageElement, topic
        'Returns     : void
        '*****************************************************************************/
        Private Sub processRowElement(ByVal rowElement As Element, ByVal topic As String)
            Dim eleRowNum As Element = rowElement.GetElement(_ROWNUM)
            Dim rowNum As Integer = eleRowNum.GetValueAsInt32()
            Dim eleSpanUpdates As Element = rowElement.GetElement(_SPANUPDATE)
            Dim numSpanUpdates As Integer = eleSpanUpdates.NumValues

            For i As Integer = 0 To numSpanUpdates - 1
                Dim spanUpdate As Element = eleSpanUpdates.GetValueAsElement(i)
                processSpanElement(spanUpdate, rowNum, topic)
            Next i
        End Sub

        '*****************************************************************************
        'Function    : processSpanElement
        'Description : Process spanUpdate Element
        'Arguments   : spanElement, rowNum, topic
        'Returns     : void
        '*****************************************************************************/
        Private Sub processSpanElement(ByVal spanElement As Element, ByVal rowNum As Integer, ByVal topic As String)
            Dim eleStartCol As Element = spanElement.GetElement(_STARTCOL)
            Dim startCol As Integer = eleStartCol.GetValueAsInt32()
            Dim eleLength As Element = spanElement.GetElement(_LENGTH)
            Dim len As Integer = eleLength.GetValueAsInt32()
            Dim eleText As Element = spanElement.GetElement(_TEXT)
            Dim text As String = eleText.GetValueAsString()
            System.Console.WriteLine("Row : " & rowNum & _
                                     ", Col : " & startCol & _
                                     " (Len : " & len & ")" + _
                                     " New Text : " & text)
            Dim rowList As ArrayList = CType(d_topicTable(topic), ArrayList)
            Do While (rowList.Count < rowNum)
                rowList.Add(New StringBuilder())
            Loop

            Dim rowText As StringBuilder = CType(rowList(rowNum - 1), StringBuilder)
            If (rowText.Length = 0) Then
                rowText.Append(text.PadRight(80))
            Else
                Dim strToReplace As String = rowText.ToString().Substring(startCol - 1, len)
                rowText.Replace(strToReplace, text, startCol - 1, len)
                System.Console.WriteLine(rowText.ToString())
            End If
        End Sub

        '*****************************************************************************
        'Function    : processMiscEvents
        'Description : Processes any message returned from Bloomberg
        'Arguments   : Event, Session
        'Returns     : void
        '*****************************************************************************/

        Private Sub processMiscEvents(ByVal eventObj As [Event], ByVal session As Session)

            System.Console.WriteLine("Processing " + eventObj.Type.ToString())

            For Each msg As Message In eventObj.GetMessages()
                System.Console.WriteLine(System.DateTime.Now.ToString("s") + ": " + _
                msg.MessageType.ToString() + vbLf)
            Next

        End Sub

        '*****************************************************************************
        'Function    : parseCommandLine
        'Description : This function parses input arguments and/or sets default arguments
        '               Only returns false on -h.
        'Arguments   : string array
        'Returns: bool()
        '*****************************************************************************/
        Private Function parseCommandLine(ByVal args As String()) As Boolean

            For i As Integer = 0 To args.Length - 1
                If String.Compare(args(i), "-t", True) = 0 And i + 1 < args.Length Then
                    d_topics.Add(args(i + 1))
                ElseIf String.Compare(args(i), "-ip", True) = 0 And i + 1 < args.Length Then
                    d_host = args(i + 1)
                ElseIf String.Compare(args(i), "-p", True) = 0 And i + 1 < args.Length Then
                    d_port = Integer.Parse(args(i + 1))
                End If
                If String.Compare(args(i), "-h", True) = 0 Then
                    printUsage()
                    Return False
                End If
            Next

            ' set default topics if nothing is specified
            If d_topics.Count = 0 Then
                d_topics.Add("0708/012/0001")
                d_topics.Add("1102/1/274")
            End If

            Return True

        End Function

        '*****************************************************************************
        'Function    : printUsage
        'Description : This function prints instructions for use to the console
        'Arguments: none()
        'Returns: void()
        '*****************************************************************************/

        Private Sub printUsage()

            System.Console.WriteLine("Usage:")
            System.Console.WriteLine(vbTab & "Retrieve page based data using V3 API")
            System.Console.WriteLine(vbTab & vbTab & "[-t " & vbTab & vbTab & "<Topic" & vbTab & "= ""0708/012/0001"">]>")
            System.Console.WriteLine(vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "i.e.""Broker ID/Category/Page Number""")
            System.Console.WriteLine(vbTab & vbTab & "[-ip " & vbTab & vbTab & "<ipAddress" & vbTab & "= localhost>")
            System.Console.WriteLine(vbTab & vbTab & "[-p " & vbTab & vbTab & "<tcpPort" & vbTab & "= 8194>")
            System.Console.WriteLine("e.g. PageDataExample -t ""0708/012/0001"" -ip localhost -p 8194")
            System.Console.WriteLine("Press ENTER to quit")

        End Sub


    End Module
End Namespace