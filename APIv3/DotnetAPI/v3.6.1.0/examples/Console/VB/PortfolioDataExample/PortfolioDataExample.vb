' Copyright 2012. Bloomberg Finance L.P.
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of
' this software and associated documentation files (the "Software"), to deal in
' the Software without restriction, including without limitation the rights to
' use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
' of the Software, and to permit persons to whom the Software is furnished to do
' so, subject to the following conditions:  The above copyright notice and this
' permission notice shall be included in all copies or substantial portions of
' the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
' EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
' OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
' ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.

'*****************************************************************************
' PortfolioDataExample: 
'	This program shows client how
'    1. To download their portfolio holdings using API.
'    2. To view their portfolio positions as of a specific date in order to see 
'       how current market movements have affected their portfolio's constituent 
'       weights.

'	It uses Reference Data Service(//blp/refdata) provided by API.
'	It does following:
'		1. Establishing a session which facilitates connection to the bloomberg 
'		   network
'		2. Initiating the Reference Data Service(//blp/refdata) for static data.
'		3. Creating and sending request to the session.  
'			- Creating 'PortfolioDataRequest' request 
'			- Adding porfolio tickers/porfolio fields to request
'			- Sending the request
'		4. Event Handling of the responses received.

' * The fields available are
'      - PORTFOLIO_MEMBERS: Returns a list of Bloomberg identifiers representing 
'        the members of a user's custom portfolio.
'      - PORTFOLIO_MPOSITION: Returns a list of Bloomberg identifiers 
'        representing the members of a user's custom portfolio as well as the  
'        position for each security in the user's custom portfolio.
'      - PORTFOLIO_MWEIGHT: Returns a list of Bloomberg identifiers representing 
'        the members of a user's custom portfolio as well as the percentage 
'        weight for each security in the user's custom portfolio.
'      - PORTFOLIO_DATA: Returns a list of the Bloomberg identifiers, positions, 
'        market values, cost, cost date, and cost foreign exchange rate of each 
'        security in a user's custom portfolio. 

' Usage: 
'  Retrieve portfolio data
'      [-s         <security       = UXXXXXXX-X Client>
'      [-f         <field          = PORTFOLIO_DATA>
'      [-o         <Reference Date = 20091101>
'      [-ip        <ipAddress      = localhost>
'      [-p         <tcpPort        = 8194>

' * Note:The user's portfolio is identified by its Portfolio ID, which can be
'        found on the upper right hand corner of the toolbar on the portfolio's 
'        PRTU page. This information can also be accessed historically by using 
'        the REFERENCE_DATE override field and supplying the date in �eYYYYMMDD' 
'        format. Run {DOCS #2054005 <GO>} for an example of an API spreadsheet 
'        with the new portfolio fields.

' Example usage:
'    PortfolioDataRequest -h
'	   Print the usage for the program on the console

'	PortfolioDataRequest
'	   Run the program with default values specified for security, fields & 
'       override. Program parses the response of PortfolioDataRequest & 
'       prints the response message on the console. 	   

'	PortfolioDataRequest -ip localhost -p 8194 -s "5497224-1 Client" 
'                          -f PORTFOLIO_MEMBERS -f PORTFOLIO_DATA 
'       Download the portfolio holdings

' 	PortfolioDataRequest -s "5497224-1 Client" -f PORTFOLIO_MPOSITION -o 20091101
'       Specifying the REFERENCE_DATE override to view portfolio positions
'       as of a specific date

'	Program prints the response on the console of the command line requested data

'******************************************************************************

Imports [Event] = Bloomberglp.Blpapi.Event
Imports Element = Bloomberglp.Blpapi.Element
Imports InvalidRequestException = Bloomberglp.Blpapi.InvalidRequestException
Imports Message = Bloomberglp.Blpapi.Message
Imports Name = Bloomberglp.Blpapi.Name
Imports Request = Bloomberglp.Blpapi.Request
Imports Service = Bloomberglp.Blpapi.Service
Imports Session = Bloomberglp.Blpapi.Session
Imports SessionOptions = Bloomberglp.Blpapi.SessionOptions
Imports Datatype = Bloomberglp.Blpapi.Schema.Datatype

Imports ArrayList = System.Collections.ArrayList

Namespace Bloomberglp.Blpapi.Examples

    Class PortfolioDataExample

        Private Shared ReadOnly SECURITY_DATA As Name = New Name("securityData")
        Private Shared ReadOnly SECURITY As Name = New Name("security")
        Private Shared ReadOnly FIELD_DATA As Name = New Name("fieldData")
        Private Shared ReadOnly RESPONSE_ERROR As Name = New Name("responseError")
        Private Shared ReadOnly SECURITY_ERROR As Name = New Name("securityError")
        Private Shared ReadOnly FIELD_EXCEPTIONS As Name = New Name("fieldExceptions")
        Private Shared ReadOnly FIELD_ID As Name = New Name("fieldId")
        Private Shared ReadOnly ERROR_INFO As Name = New Name("errorInfo")
        Private Shared ReadOnly CATEGORY As Name = New Name("category")
        Private Shared ReadOnly MESSAGE As Name = New Name("message")

        Private d_host As String
        Private d_port As Integer
        Private d_securities As ArrayList
        Private d_fields As ArrayList
        Private d_override As String

        Shared Sub Main(ByVal args() As String)

            System.Console.WriteLine("Portfolio Data Example")
            Dim example As PortfolioDataExample = New PortfolioDataExample()
            example.run(args)

            System.Console.WriteLine("Press ENTER to quit")
            System.Console.Read()
        End Sub

        ''' <summary>
        ''' Constructor
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            d_host = "localhost"
            d_port = 8194
            d_securities = New ArrayList()
            d_fields = New ArrayList()
        End Sub

        ''' <summary>
        ''' Read command line arguments, 
        ''' Establish a Session
        ''' Identify and Open refdata Service
        ''' Send PortfolioDataRequest to the Service 
        ''' Event Loop and Response Processing
        ''' </summary>
        ''' <param name="args"></param>
        ''' <remarks></remarks>
        Private Sub run(ByVal args() As String)
            If Not parseCommandLine(args) Then
                Return
            End If

            Dim sessionOptions As SessionOptions = New SessionOptions()
            sessionOptions.ServerHost = d_host
            sessionOptions.ServerPort = d_port

            System.Console.WriteLine("Connecting to " & d_host + ":" & _
                                     d_port.ToString())
            Dim session As Session = New Session(sessionOptions)
            Dim sessionStarted As Boolean = session.Start()
            If Not sessionStarted Then
                System.Console.Error.WriteLine("Failed to start session.")
                Return
            End If
            If Not session.OpenService("//blp/refdata") Then
                System.Console.Error.WriteLine("Failed to open //blp/refdata")
                Return
            End If

            Try
                sendPortfolioDataRequest(session)
            Catch e As InvalidRequestException
                System.Console.WriteLine(e.ToString())
            End Try

            ' wait for events from session.
            eventLoop(session)

            session.Stop()

        End Sub

        ''' <summary>
        ''' Polls for an event or a message in an event loop
        ''' and Processes the event generated
        ''' </summary>
        ''' <param name="session"></param>
        ''' <remarks></remarks>
        Private Sub eventLoop(ByVal session As Session)
            Dim done As Boolean = False
            While Not done
                Dim eventObj As [Event] = session.NextEvent()
                If eventObj.Type = [Event].EventType.PARTIAL_RESPONSE Then
                    System.Console.WriteLine("Processing Partial Response")
                    processResponseEvent(eventObj)
                ElseIf eventObj.Type = [Event].EventType.RESPONSE Then
                    System.Console.WriteLine("Processing Response")
                    processResponseEvent(eventObj)
                    done = True
                Else
                    Dim msg As Message
                    For Each msg In eventObj.GetMessages()
                        System.Console.WriteLine(msg.AsElement)
                        If eventObj.Type = [Event].EventType.SESSION_STATUS Then
                            If msg.MessageType.Equals("SessionTerminated") Then
                                done = True
                            End If
                        End If
                    Next
                End If
            End While
        End Sub

        ''' <summary>
        ''' Function to handle response event
        ''' </summary>
        ''' <param name="eventObj"></param>
        ''' <remarks></remarks>
        Private Sub processResponseEvent(ByVal eventObj As [Event])
            Dim msg As Message
            For Each msg In eventObj.GetMessages()
                If msg.HasElement(RESPONSE_ERROR) Then
                    printErrorInfo("REQUEST FAILED: ", msg.GetElement(RESPONSE_ERROR))
                    Continue For
                End If
                Dim securities As Element = msg.GetElement(SECURITY_DATA)
                Dim numSecurities As Integer = securities.NumValues
                System.Console.WriteLine(vbCrLf & "Processing " & numSecurities & " securities:")
                Dim secCnt As Integer
                For secCnt = 0 To numSecurities - 1 Step +1
                    Dim eleSecurity As Element = securities.GetValueAsElement(secCnt)
                    Dim ticker As String = eleSecurity.GetElementAsString(SECURITY)
                    System.Console.WriteLine(vbCrLf & "Ticker: " & ticker)
                    If eleSecurity.HasElement("securityError") Then
                        printErrorInfo("SECURITY FAILED: ", _
                                        eleSecurity.GetElement(SECURITY_ERROR))
                        Continue For
                    End If
                    Dim fields As Element = eleSecurity.GetElement(FIELD_DATA)
                    If fields.NumElements > 0 Then
                        System.Console.WriteLine("FIELD" & vbTab & vbTab & "VALUE")
                        System.Console.WriteLine("-----" & vbTab & vbTab & "-----")
                        Dim numElements As Integer = fields.NumElements
                        Dim eleCtr As Integer
                        For eleCtr = 0 To numElements - 1 Step +1
                            Dim field As Element = fields.GetElement(eleCtr)
                            ' Checking if the field is Bulk field
                            If field.Datatype = Datatype.SEQUENCE Then
                                processBulkField(field)
                            Else
                                processRefField(field)
                            End If
                        Next
                    End If
                    System.Console.WriteLine("")
                    Dim fieldExceptions As Element = eleSecurity.GetElement(FIELD_EXCEPTIONS)
                    If fieldExceptions.NumValues > 0 Then
                        System.Console.WriteLine("FIELD" & vbTab & vbTab & "EXCEPTION")
                        System.Console.WriteLine("-----" & vbTab & vbTab & "---------")
                        Dim k As Integer
                        For k = 0 To fieldExceptions.NumValues - 1 Step +1
                            Dim fieldException As Element = _
                                            fieldExceptions.GetValueAsElement(k)
                            printErrorInfo(fieldException.GetElementAsString(FIELD_ID) _
                                            & vbTab & vbTab, _
                                            fieldException.GetElement(ERROR_INFO))

                        Next
                    End If
                Next
            Next
        End Sub

        ''' <summary>
        ''' Read the reference bulk field contents
        ''' </summary>
        ''' <param name="refBulkfield"></param>
        ''' <remarks></remarks>
        Private Sub processBulkField(ByVal refBulkfield As Element)
            System.Console.WriteLine(vbCrLf & refBulkfield.Name.ToString())
            ' Get the total number of Bulk data points
            Dim numofBulkValues As Integer = refBulkfield.NumValues
            Dim bvCtr As Integer
            For bvCtr = 0 To numofBulkValues - 1 Step bvCtr + 1
                Dim bulkElement As Element = refBulkfield.GetValueAsElement(bvCtr)
                ' Get the number of sub fields for each bulk data element
                Dim numofBulkElements As Integer = bulkElement.NumElements
                Dim beCtr As Integer
                ' Read each field in Bulk data
                For beCtr = 0 To numofBulkElements - 1
                    Dim elem As Element = bulkElement.GetElement(beCtr)
                    System.Console.WriteLine(vbTab & vbTab & elem.Name.ToString() & _
                                            " = " & elem.GetValueAsString())
                Next
                System.Console.WriteLine()
            Next
        End Sub

        ''' <summary>
        ''' Read the reference field contents
        ''' </summary>
        ''' <param name="reffield"></param>
        ''' <remarks></remarks>
        Private Sub processRefField(ByVal reffield As Element)
            System.Console.WriteLine(reffield.Name.ToString() & vbTab & vbTab & _
                                    reffield.GetValueAsString())
        End Sub

        ''' <summary>
        ''' Function to create and send PortfolioDataRequest
        ''' </summary>
        ''' <param name="session"></param>
        ''' <remarks></remarks>
        Private Sub sendPortfolioDataRequest(ByVal session As Session)
            Dim refDataService As Service = session.GetService("//blp/refdata")
            Dim request As Request = refDataService.CreateRequest("PortfolioDataRequest")

            ' Add securities to request
            Dim securities As Element = request.GetElement("securities")

            Dim i As Integer
            For i = 0 To d_securities.Count - 1 Step +1
                securities.AppendValue(CType(d_securities(i), String))
            Next

            ' Add fields to request
            Dim fields As Element = request.GetElement("fields")
            For i = 0 To d_fields.Count - 1 Step +1
                fields.AppendValue(CType(d_fields(i), String))
            Next

            ' If specified, use REFERENCE_DATE override field 
            ' to get portfolio information historically.
            ' The date must be in �eYYYYMMDD' format
            If d_override.Length <> 0 Then
                Dim ovrRefDate As Element = request.GetElement("overrides")
                Dim override1 As Element = ovrRefDate.AppendElement
                override1.SetElement("fieldId", "REFERENCE_DATE")
                override1.SetElement("value", d_override)
            End If

            System.Console.WriteLine("Sending Request: " & _
                                    request.AsElement().ToString())

            session.SendRequest(request, Nothing)
        End Sub

        ''' <summary>
        ''' Function to parse the command line arguments
        ''' </summary>
        ''' <param name="args"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function parseCommandLine(ByVal args() As String) As Boolean
            Dim i As Integer
            For i = 0 To args.Length - 1 Step +2
                If String.Compare(args(i), "-s", True) = 0 And (i + 1) < args.Length Then
                    d_securities.Add(args(i + 1))
                ElseIf String.Compare(args(i), "-f", True) = 0 And (i + 1) < args.Length Then
                    d_fields.Add(args(i + 1))
                ElseIf String.Compare(args(i), "-o", True) = 0 And (i + 1) < args.Length Then
                    d_override = args(i + 1)
                ElseIf String.Compare(args(i), "-ip", True) = 0 And (i + 1) < args.Length Then
                    d_host = args(i + 1)
                ElseIf String.Compare(args(i), "-p", True) = 0 And (i + 1) < args.Length Then
                    d_port = Integer.Parse(args(i + 1))
                ElseIf String.Compare(args(i), "-h", True) = 0 Then
                    printUsage()
                    Return False
                End If
            Next

            ' handle default arguments
            If d_securities.Count = 0 Then
                d_securities.Add("U5497224-1 Client")
            End If

            If d_fields.Count = 0 Then
                d_fields.Add("PORTFOLIO_MEMBERS")
                d_fields.Add("PORTFOLIO_MPOSITION")
                d_fields.Add("PORTFOLIO_MWEIGHT")
                d_fields.Add("PORTFOLIO_DATA")
            End If

            Return True
        End Function

        ''' <summary>
        ''' Prints error information
        ''' </summary>
        ''' <param name="leadingStr"></param>
        ''' <param name="errorInfo"></param>
        ''' <remarks></remarks>
        Private Sub printErrorInfo(ByVal leadingStr As String, _
                                    ByVal errorInfo As Element)
            System.Console.WriteLine(leadingStr & _
                                    errorInfo.GetElementAsString(CATEGORY) _
                                    & " (" _
                                    & errorInfo.GetElementAsString(MESSAGE) _
                                    & ")")
        End Sub

        ''' <summary>
        ''' Print usage of the Program
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub printUsage()
            System.Console.WriteLine("Usage:")
            System.Console.WriteLine("	Retrieve reference data/Bulk reference data" _
                                        & " using Server API")
            System.Console.WriteLine("      [-s         <security       = " & Chr(34) & "UXXXXXXX-X Client " _
                                        & Chr(34) & ">")
            System.Console.WriteLine("      [-f         <field          = PORTFOLIO_DATA>")
            System.Console.WriteLine("      [-o         <Reference Date = 20091101>")
            System.Console.WriteLine("      [-ip        <ipAddress      = localhost>")
            System.Console.WriteLine("      [-p         <tcpPort        = 8194>")
            System.Console.WriteLine("Notes: ")
            System.Console.WriteLine("1) Multiple securities & fields can be specified")
            System.Console.WriteLine("2) The user's portfolio is identified by its Portfolio ID, which can be")
            System.Console.WriteLine("   found on the upper right hand corner of the toolbar on the portfolio's")
            System.Console.WriteLine("   PRTU page. This information can also be accessed historically by using")
            System.Console.WriteLine("   the REFERENCE_DATE override field[-o] & supplying the date in 'YYYYMMDD'")
            System.Console.WriteLine("   format.")
        End Sub
    End Class

End Namespace
