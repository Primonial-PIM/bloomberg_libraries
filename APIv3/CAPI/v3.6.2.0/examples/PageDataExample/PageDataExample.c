/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

// *****************************************************************************
// PageDataExample.c: 
// This program demonstrates how to make a subscription to Page Based data. 
//   It uses the Market Bar service(//blp/pagedata) 
//	provided by API. Program does the following:
//		1. Establishes a session which facilitates connection to the bloomberg 
//		   network.
//		2. Initiates the Page data Service(//blp/pagedata) for realtime
//		   data.
//		3. Creates and sends the request via the session.
//			- Creates a subscription list
//			- Adds Page data topic to subscription list.
//			- Subscribes to realtime Page data
//		4. Event Handling of the responses received.
//       5. Parsing of the message data.
// Usage: 
//         	-t			<Topic  	= "0708/012/0001">
//                                   i.e."Broker ID/Category/Page Number"
//     		-ip 		<ipAddress	= localhost>
//     		-p 			<tcpPort	= 8194>
//
//   example usage:
//	PageDataExample -t "0708/012/0001" -ip localhost -p 8194
//
// Prints the response on the console of the command line requested data
//******************************************************************************/

#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <blpapi_subscriptionlist.h>
#include <time.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for memset(3C) */

#define TOPIC_MAX			256

static char*         d_host;
static int           d_port;
static char 		 d_topicArray[TOPIC_MAX][64];
static int			 d_topicCnt;

static blpapi_SubscriptionList_t *d_subscriptions = NULL;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("    Retrieve realtime page data from ServerApi using EventHandling approach\n");
	printf("        [-ip        <ipAddress  = localhost>\n");
	printf("        [-p         <tcpPort    = 8194>\n");
	printf("        [-t         <Topic	= /0708/012/0001>\n");
	printf("        [           i.e.\"Broker ID/Category/Page Number\"\n");
	printf("e.g. PageDataExample -t \"/0708/012/0001\" -ip localhost -p 8194\n");
	printf("Press ENTER to quit\n");
	getchar();
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/host/port
Argument	: Command line parameters
Returns		: int: 
			  0, if successfully set the input argument for the request 
              from command line or using default values otherwise -1
*****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Default value for host and port if not specified on command line
	d_host = "localhost";
	d_port = 8194;

	if (argc == 2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i],"-ip") && i+1 < argc) 
			d_host = argv[++i];
		else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-t") &&  i+1 < argc && d_topicCnt < TOPIC_MAX ) 
			strcpy(d_topicArray[d_topicCnt++], argv[++i]); 
		else { 
			printUsage();
			return -1;
		}
	}

	// Default topic if nothing is specified on command line
	if(d_topicCnt == 0){
		strcpy(d_topicArray[d_topicCnt++], "/0708/012/0001"); 
		strcpy(d_topicArray[d_topicCnt++], "/1102/1/274"); 
	}
    return 0;
}


/*****************************************************************************
Function    : getTimeStamp
Description : Sets current local time to string buffer
Argument    : Pointer to string buffer
			  bufSize - size of string buffer
Returns     : int - no.of characters placed in the buffer
*****************************************************************************/
int getTimeStamp(char *buffer, size_t bufSize)
{
    const char *format = "%Y-%m-%dT%X";

    time_t now = time(0);
#ifdef WIN32
	struct tm _timeInfo, *timeInfo;
	localtime_s(&_timeInfo, &now);
	timeInfo = &_timeInfo;
#else
    struct tm _timeInfo;
	struct tm *timeInfo = localtime_r(&now, &_timeInfo);
#endif
    return strftime(buffer, bufSize, format, timeInfo);
}

/*****************************************************************************
Function    : handleSubscriptionEvent
Description : This function handles subscription data and subscription status 
              event. This function reads update data messages in the event
			  element and prints them on the console.
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleSubscriptionEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
    char timeBuffer[64];
    const char *topic;
	assert(event);

	getTimeStamp(timeBuffer, sizeof(timeBuffer));

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = 0;
		assert(message);
		
		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		if(correlationId.valueType == BLPAPI_CORRELATION_TYPE_POINTER &&
			correlationId.value.ptrValue.pointer != NULL){
			topic = (char *) correlationId.value.ptrValue.pointer;
			printf("%s: %s - ", timeBuffer, topic);
		}

		messageElements = blpapi_Message_elements(message);
		
		// Get the message element and print it on console.
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
	}
	blpapi_MessageIterator_destroy(iter);
}


/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than subscription data and 
			  subscription status event. This function gets the messages from
			  the event and print them on the console. If the event is session 
			  terminate, then release allocated resources and exit the program.
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		
		// If session status is session terminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout,	"Terminating: %s\n",
					blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			exit(1);
		}
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : processEvent
Description : This function is an Event Handler to process events generated
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
			  Pointer to userData
Returns     : void
*****************************************************************************/
// Event Handler to process events generated
static void processEvent(blpapi_Event_t *event, 
						 blpapi_Session_t *session, 
						 void *buffer)
{
	assert(event);
	assert(session);
	switch (blpapi_Event_eventType(event)) {
		// Process events BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
		// & BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
		case BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA:
			printf("Processing SUBSCRIPTION_DATA\n");
			handleSubscriptionEvent(event, session);
			break;
		case BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS:
			printf("Processing SUBSCRIPTION_STATUS\n");
			handleSubscriptionEvent(event, session);
			break;
		default:
			// Process events other than BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
			// or BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
			handleOtherEvent(event, session);
			break;
	}
}

/*****************************************************************************
Function    : subscribe
Description : This function creates subscription list and subscribes to 
			  realtime data for securities specified on command line
Arguments   : Pointer to session
Returns     : void
*****************************************************************************/
void subscribe(blpapi_Session_t *session)
{
	blpapi_SubscriptionList_t *subscriptions = NULL;
    char  * topic;
	char **fields;
	const char **options = NULL;
	int i = 0;
	int fieldListSize = 1;

	fields = (char **) malloc(fieldListSize * sizeof(char *));
    fields[0] = "6-23";
    // Following commented code shows some of the sample values 
    // that can be used for field other than above
    // e.g. fields[1]("1");
    //      fields[2]("1,2,3");
    //      fields[3]("1,6-10,15,16");

	subscriptions = blpapi_SubscriptionList_create();
	assert(subscriptions);
	
	i = 0;
	while  (i < d_topicCnt ) {
		blpapi_CorrelationId_t subscriptionId;
        topic = (char *) d_topicArray[i];
        printf( "Subscribing to %s\n", topic );

		// Initialize Correlation object
		memset(&subscriptionId, '\0', sizeof(subscriptionId));
		subscriptionId.size = sizeof(subscriptionId);
		subscriptionId.valueType = BLPAPI_CORRELATION_TYPE_POINTER;
		subscriptionId.value.ptrValue.pointer = topic;

		blpapi_SubscriptionList_add(subscriptions, 
									topic, 
									&subscriptionId, 
									(const char **)fields, 
									options, 
									fieldListSize, 
									0);

		i++;
    }
    printf( "\n" );
	// Subscribing to realtime data
	blpapi_Session_subscribe(session, subscriptions, 0, 0, 0);

	free(fields);
	// release subscription list
	blpapi_SubscriptionList_destroy(subscriptions);
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate how to make 
			  subscription to particular security/ticker to get realtime 
			  streaming updates. It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitates connection to the 
			      bloomberg network. Also specifies Event handler to be 
				  called for events generated.
   		      3. Opens a mktdata service with the session. 
			  4. create and send subscription request.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = NULL;
	blpapi_Session_t *session = NULL;

	// read command line parameters
	if (parseCommandLine(argc, argv) == -1) {
		return -1;
	}

	// create sessionOptions instance. We are allocating resources for
	// sessionOptions, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);
	
	// Set the host and port for the session. 
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);
	blpapi_SessionOptions_setDefaultSubscriptionService(sessionOptions, "//blp/pagedata");

	printf("Connecting to %s:%d\n", d_host, d_port);
	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, &processEvent, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);

	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// Open mktdata Service
	if (0 != blpapi_Session_openService(session,"//blp/pagedata")){
		fprintf(stderr, "Failed to open service //blp/pagedata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// Make subscription to realtime streaming data
	subscribe(session);

	printf("Press ENTER to quit\n");
	getchar();

	// Destory/release allocated resources.
	blpapi_Session_destroy(session);

	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("SubscriptionExampleWithEventHandler\n");

	run(argc, argv);

	return 0;
}
