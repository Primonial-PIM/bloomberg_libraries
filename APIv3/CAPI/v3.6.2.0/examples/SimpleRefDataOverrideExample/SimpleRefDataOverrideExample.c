/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/****************************************************************************************************
 SimpleRefDataOverrrideExample.c: 
	This program provides static data with overrides. It uses Reference Data Service(//blp/refdata) 
	provided by Bloomberg API.
	It does following:
		1. Establishing a session which facilitate connection to the bloomberg network
		2. Initiating the Reference Data Service(//blp/refdata) for static data.
		3. Creating and sending request to the session.  
			- Creating 'ReferenceDataRequest' request 
			- Adding securities/fields/overrides to request
			- Sending the request
		4. Event Handling.
 Usage: 
    SimpleRefDataOverrideExample -help 
	SimpleRefDataOverrideExample -?
	   Print the usage for the program on the console

	SimpleRefDataOverrideExample
	   Run the program with default values. Print the response message on the console. 
	   Parsing of the response is not covered in this programe. For details on parsing the response 
	   of ReferenceDataReuest, please refer to ReferenceDataRequestExample.c available in SDK.	 

	SimpleRefDataOverrideExample -ip localhost -p 8194 -s "VOD LN Equity" -f PX_LAST -o VWAP_START_TIME=9:30 -o VWAP_END_TIME=10:30 

	SimpleRefDataOverrideExample -s "VOD LN Equity" -f PX_LAST -o VWAP_START_TIME=9:30 -o VWAP_END_TIME=10:30 
	  Print the response on the console of the command line requested data

**********************************************************************************************************/
#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) and memset(3C) */

#define SEC_MAX			256
#define FIELD_MAX		256
#define OVERRIDE_MAX     8

typedef struct overrideField
{  
	 char fldName[80];  
     char fldValue[80]; 
} overrideField_t;

static char*			d_host;       /* IP Address */ 
static int				d_port;       /* Port Number */
static char 			d_secArray[SEC_MAX][64];
static char 			d_fieldArray[FIELD_MAX][64];
static overrideField_t	d_overrideArray[OVERRIDE_MAX];
static int				d_secCnt;
static int				d_fldCnt;
static int				d_overrideCnt;

blpapi_Session_t *session = 0;
blpapi_Request_t *request = 0;


// Write specified data to specified stream
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************
printUsage
   This function prints the usage of the program on command line.
Argument: void
Returns: void
*****************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("     Retrieve reference data using override in Bloomberg API\n");
    printf("        [-ip        <ipAddress                  = localhost>\n");
    printf("        [-p         <tcpPort                    = 8194>\n");
	printf("        [-s         <security                   = \"IBM US Equity\">\n");
	printf("        [-f         <field                      = PX_LAST>\n");
	printf("        [-o         <override(fieldName:Value)  = VWAP_START_TIME=9:30>\n");
	printf("Notes:\n");
	printf("Multiple securities, fields & overrides can be specified.\n");
}

/***************************************************************************
parseCommandLine
   This function parses the command line arguments.If the command line 
   argument are not provided properly, it calls printUsage to print the 
   usage on commandline. If function name "SimpleRefDataOverrideExample"
   is specified on the command line, this fuction will set default values
   for security/fields/overrides

 Argument: Command line parameters
 Returns: int: 0 if successfully set the input argument for the request 
              from command line or using default values otherwise -1
****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
 	char *name, *value;
	char cTempStr[80];
	char *nextToken;
	int i = 0;
	// Default value for host and port if not specified on command line
	d_host = "localhost";
	d_port = 8194;

	if (argc ==2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || !strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	
	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i],"-ip") && i+1 < argc) 
			d_host = argv[++i];
		else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-s") &&  i+1 < argc && d_secCnt < SEC_MAX ) 
			strcpy(d_secArray[d_secCnt++], argv[++i]); 
		else if (!strcmp(argv[i],"-f") &&  i+1 < argc && d_fldCnt < FIELD_MAX ) 
			strcpy(d_fieldArray[d_fldCnt++], argv[++i]); 
		else if (!strcmp(argv[i],"-o") &&  i+1 < argc && d_overrideCnt < OVERRIDE_MAX ) {
			// get the field and value from the override option.
			strcpy(cTempStr, argv[i]);
			name = strtok_s(cTempStr, "=", &nextToken);
			if (name != NULL) {				
				value = strtok_s(NULL, "=", &nextToken);
				strcpy(d_overrideArray[d_overrideCnt].fldName, name);
				strcpy(d_overrideArray[d_overrideCnt++].fldValue, value);
			}
		}
		else {
			// print usage if user specify options other than the supported one.
			printUsage();
			return -1;
		}
	} // End of for


	// Default security and field if nothing is specified on command line
	if(d_secCnt == 0){
		strcpy(d_secArray[d_secCnt++], "IBM US Equity"); 
		strcpy(d_secArray[d_secCnt++], "MSFT US Equity"); 
	}
	if(d_fldCnt == 0){
		strcpy(d_fieldArray[d_fldCnt++], "PX_LAST"); 
		strcpy(d_fieldArray[d_fldCnt++], "DS002"); 
		strcpy(d_fieldArray[d_fldCnt++], "EQY_WEIGHTED_AVG_PX"); 
	}
	if(d_overrideCnt == 0){
		strcpy(d_overrideArray[d_overrideCnt].fldName, "VWAP_START_TIME");
		strcpy(d_overrideArray[d_overrideCnt++].fldValue,  "9:30");		
		strcpy(d_overrideArray[d_overrideCnt].fldName, "VWAP_END_TIME");
		strcpy(d_overrideArray[d_overrideCnt++].fldValue,  "10:30");		
	}
	return 0;
}

/*******************************************************************************
 handleResponseEvent
	This function handle response and partial response event. This function
	gets the messages from the event and print them on the console. In order to 
	understand how to traverse the message to fetch particular element, please 
	refer to refDataExample in SDK.

  Argument: Pointer to blpapi_Event_t
  Returns: void
********************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = 0;
		assert(message);

		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		printf("\n");
	}
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/******************************************************************************************
 handleOtherEvent
      This function handles events other than response and partial response event. This 
	  function gets the messages from the event and print them on the console. If the event 
	  is session terminate, then release allocated resources and exit the program. 

 Arguments: Pointer to blpapi_Event_t
 Returns: void
*******************************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate thruogh messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = 0;
		assert(message);

		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		printf("\n");
		
		// If session status is session terminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			blpapi_Request_destroy(request);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/***************************************************************************************************
 run                                                                                     
   	This function runs the application to demonstrate reference data request using override.
	It does following:
	  1. Reads command line arguments.
   	  2. Establishes a session which facilitate connection to the bloomberg network
   	  3. Opens a refData service with the session. 
    3. create and send request:  
		- Create a referenceData request 
        - Add security to request
        - Add field to request.
        - Add override field and value to request.		
    4. Event Loop and Response Handling.

 Arguments: Command line parameters.
 Returns: integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = 0;
	blpapi_Service_t *refDataSvc = 0;
	blpapi_Element_t *elements = 0;
	blpapi_Element_t *securitiesElements = 0;
	blpapi_Element_t *fieldsElements = 0;
	blpapi_Element_t *overrideElements = 0;
	blpapi_CorrelationId_t correlationId;
    char  * security;
    char  * field;
	blpapi_Element_t *override1 = 0;
	blpapi_Element_t *override2 = 0;
	int i = 0;

	int continueToLoop = 1;

	// read command line parameters.
	// read command line parameters
	if (parseCommandLine(argc, argv) == -1) {
		return -1;
	}
	
	// create options for session. We are allocating resources for 
	// session option, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host IP and port for the session. For more options 
	// please refer to WAPI<GO>.
	// TODO: what if setting host and port fails?
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Reference Data Service with session
	if (0 != blpapi_Session_openService(session, "//blp/refdata")) {
		fprintf(stderr, "Failed to open service //blp/refdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// get handle for the reference data service
	// TODO: What if getting handle fails?
	blpapi_Session_getService(session, &refDataSvc, "//blp/refdata");

	// Create Reference Data Request
	// TODO: What if creating the request fails?
	blpapi_Service_createRequest(refDataSvc, &request, "ReferenceDataRequest");
	assert(request);

	// Get request elements
	elements = blpapi_Request_elements(request);
	assert(elements);

	// Get "securities" element
	blpapi_Element_getElement(elements,	&securitiesElements, "securities", 0);
	assert(securitiesElements);

	i = 0;
	// Set securities specified on command line
	while  (i < d_secCnt ) {
        security = (char *) d_secArray[i];
		blpapi_Element_setValueString(securitiesElements, security, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }

	// Get "fields" element
	blpapi_Element_getElement(elements, &fieldsElements, "fields", 0);

	i = 0;
	// Set fields specified on command line
	while  (i < d_fldCnt ) {
        field = (char *) d_fieldArray[i];
		blpapi_Element_setValueString(fieldsElements, field, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }

	// Get override element
    blpapi_Element_getElement(elements, &overrideElements, "overrides", 0);
	i = 0;
	//Add overrides fields and values 
	while  (i < d_overrideCnt ) {
		blpapi_Element_t *overrideEle = 0;
		blpapi_Element_appendElement(overrideElements, &overrideEle);
		blpapi_Element_setElementString(overrideEle, "fieldId", 0, d_overrideArray[i].fldName);
		blpapi_Element_setElementString(overrideEle, "value", 0, d_overrideArray[i].fldValue);
		i++;
    }

	// Initialize Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;
	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);
	printf("\n");
	// Sending request
	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);


	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = 0;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_RESPONSE: 
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional events 
                // should be expected.	
				printf("Processing Response \n");
				handleResponseEvent(event);
				continueToLoop = 0; // fall through	
				break;
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event indicates	                    
       		    // that request has not been fully satisfied.
				printf("Processing Partial Response \n");
				handleResponseEvent(event);
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event);
				break;
		}
        // release the event
		blpapi_Event_release(event);
	}

	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Request_destroy(request);
	blpapi_Session_destroy(session);

	return 0;
}
/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{	
	printf("SimpleRefDataOverrideExample\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}
