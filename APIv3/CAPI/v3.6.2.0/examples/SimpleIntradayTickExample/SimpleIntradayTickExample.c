/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
SimpleIntradayTickExample.c: 
	This program demostrate how to request intraday ticks using Bloomberg C API.
	This program just print the intraday tick respnse on the out. In order to 
	know how to parse the response, please refer to IntradayTickExample 
	available in the SDK. It uses Reference Data Service(//blp/refdata) provided
	by Bloomberg API. It does following:
		1. Establishing a session which facilitate connection to the bloomberg
		    network
		2. Initiating the Reference Data Service(//blp/refdata) for tick data.
		3. Creating and sending request to the session.  
			- Creating 'IntradayTickRequest' request 
			- Specifying security/eventType to request
			- Specifying the startDateTime and endDateTime for IntradayTick data
			- Sending the request
		4. Event Handling.

 Usage: 
    SimpleIntradayTickExample -help 
	SimpleIntradayTickExample -?
	   Print the usage for the program on the console
	SimpleIntradayTickExample
	   Run the program with default values. Print the ticks in a formatted table
	   on the console.   
	SimpleIntradayTickExample -ip localhost -p 8194 -s "VOD LN Equity" -e TRADE 
						-sd 2008-08-11T13:30:00 -ed 2008-08-11T13:30:00 -cc 0
	SimpleIntradayTickExample -s "VOD LN Equity" -e TRADE -sd 2008-08-11T13:30:00 
						-ed 2008-08-11T13:30:00 -cc 0 
	  Print the response on the console of the command line requested data

Limitation/assumptions: When running the program on command line, you can 
specify only one security and one field. 

******************************************************************************/

#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) and memset(3C) */

static char*         d_host;
static int           d_port;
static char*         d_security;
static char*         d_eventType1;
static char*         d_eventType2;
static char*         d_barInterval;
static char*         d_startDate;
static char*         d_endDate;
static char*          d_conditionCode;

blpapi_Session_t *session = 0;
blpapi_Request_t *request = 0;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
    printf("    Retrieve Intraday Tick data using Bloomberg API\n");
    printf("        [-ip        <ipAddress       = localhost>]\n");
    printf("        [-p         <tcpPort         = 8194>]\n");
    printf("        [-s         <security        = \"IBM US Equity\">]\n");
    printf("        [-e         <event           = TRADE>]\n");
	printf("        [-sd        <startDateTime   = 2012-12-27T13:30:00>]\n");
    printf("        [-ed        <endDateTime     = 2012-12-27T13:31:00>]\n");
	printf("        [-cc        <ConditionCode   = true/false>]\n");
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/startdate/enddate/host/port
Argument	: Command line parameters
Returns		: int: 
			  '0', if successfully set the input argument for the request 
              from command line or using default values otherwise '1'
****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Default value for host and port if not specified on command line
	d_host = "localhost";
	d_port = 8194;
	// Set default security and field
	d_security = "IBM US Equity";
	d_eventType1 = "TRADE";

	//set the default startDate and EndDate
	d_startDate = "2012-12-27T13:30:00";
	d_endDate = "2012-12-27T13:31:00";
	d_conditionCode = "false";

	if (argc ==2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || !strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	
	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i],"-ip") && i+1 < argc) 
			d_host = argv[++i];
		else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-s") &&  i+1 < argc) 
			d_security = argv[++i];
		else if (!strcmp(argv[i],"-e") &&  i+1 < argc) 
			d_eventType1 = argv[++i];
		else if (!strcmp(argv[i],"-sd") &&  i+1 < argc) 
			d_startDate = argv[++i];
		else if (!strcmp(argv[i],"-ed") &&  i+1 < argc) 
			d_endDate  = argv[++i];
		else if (!strcmp(argv[i],"-cc") &&  i+1 < argc) 
			d_conditionCode = argv[++i];
		else {
			// print usage if user specify options other than the supported one.
			printUsage();
			return -1;
		}
	} // End of for
	return 0;
}

/*****************************************************************************
Function    : handleResponseEvent
Description : This function handle response and partial response event. This 
			  function gets the messages from the event and print them on the 
			  console. 
Argument    : Pointer to blpapi_Event_t
Returns     : void
*****************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);
	iter = blpapi_MessageIterator_create(event);
	assert(iter);

	// Event has one or more messages. Create message iterator for event
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *intradayTickResponse = 0;
		assert(message);

		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		printf("messageType =%s\n", blpapi_Message_typeString(message));

		// Get the message element and print it on console.
		intradayTickResponse = blpapi_Message_elements(message);
		assert(intradayTickResponse);
		blpapi_Element_print(intradayTickResponse, &streamWriter, stdout, 0, 4);
		printf("\n");
	}
	// release the resources for message iterator
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than response and partial 
			  response event. This function gets the messages from the event 
			  and print them on the console. If the event is session terminate, 
			  then release allocated resources and exit the program. 
Arguments   : Pointer to blpapi_Event_t
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate thruogh messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = 0;
		assert(message);

		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		printf("\n");
		
		// If session status is session terminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			blpapi_Request_destroy(request);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : sendIntradayTickRequest
Description : This function does following.
			     - Create a IntradayTickRequest request 
			     - Add security to request
			     - Add event type for the tick.
			     - Add start datetime and end datetime for the tick to the 
				   request.		
				 - Sends request to session.
Argument    : Pointer to session
Return      : void
*****************************************************************************/
void sendIntradayTickRequest(blpapi_Session_t *session)
{
	blpapi_Service_t *refDataSvc = 0;
	blpapi_Element_t *elements = 0;
	blpapi_Element_t *securitiesElements = 0;
	blpapi_Element_t *eventElement = 0;
	blpapi_Element_t *stDateElement = 0;
	blpapi_Element_t *endDateElement = 0;
	blpapi_CorrelationId_t correlationId;

	// get handle for the reference data service
	// TODO: What if getting handle fails?
	blpapi_Session_getService(session, &refDataSvc, "//blp/refdata");

	// Create Reference Data Request
	// TODO: What if creating the request fails?
	blpapi_Service_createRequest(refDataSvc, &request, "IntradayTickRequest");
	assert(request);

	// Get request elements
	elements = blpapi_Request_elements(request);
	assert(elements);

	// Set security element. Only one security per request
	// TODO: Any handling for return value. Also what are the posible value
	// one can pass for 3rd Argument.
    blpapi_Element_setElementString(elements, "security", 0, d_security);

	// Set event type. Only one eventType per request
	// TODO: Any handling for return value. Also what are the posible value 
	// one can pass for 4th Argument.
	blpapi_Element_getElement(elements, &eventElement, "eventTypes", 0);
	blpapi_Element_setValueString(eventElement, d_eventType1, 
									BLPAPI_ELEMENT_INDEX_END);
	if (d_eventType2 != NULL) 
		blpapi_Element_setValueString(eventElement, d_eventType2, 
									BLPAPI_ELEMENT_INDEX_END);

	// Set start and End date 
	// TODO: Any handling for return value. Also what are the posible value 
	// one can pass for 3rd Argument.
	blpapi_Element_setElementString(elements, "startDateTime", 0, d_startDate);
	blpapi_Element_setElementString(elements, "endDateTime", 0, d_endDate);
	

	//Include condition codes
    blpapi_Element_setElementString(elements, "includeConditionCodes", 0, 
									d_conditionCode);

	// Print the request contents
	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);
	printf("\n");

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;

	// Sending request
	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	blpapi_Request_destroy(request);
	return;
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate intraday tick 
			  request. It does following:
			  1. Reads command line arguments.
		  	  2. Establishes a session which facilitate connection to the
			     bloomberg network
			  3. Opens a refData service with the session. 
			  4. create and send request:  
			     - Create a IntradayTickRequest request 
			     - Add security to request
			     - Add event type for the tick.
			     - Add start datetime and end datetimefor the tick to the 
				   request.		
			  5. Event Loop and Response Handling.

Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = 0;
	int continueToLoop = 1;
	
	// read command line parameters.
    if (0 != parseCommandLine(argc, argv)) return -1;

	// create options for session. We are allocating resources for 
	// session option, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host IP and port for the session. For more options 
	// please refer to WAPI<GO>.
	// TODO: what if setting host and port fails?
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Reference Data Service
	if (0 != blpapi_Session_openService(session, "//blp/refdata")){
		fprintf(stderr, "Failed to open service //blp/refdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// create and send IntradayTickRequest to session
	sendIntradayTickRequest(session);

	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = 0;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_RESPONSE: // final event
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional  
                // events should be expected.	
				printf("Processing Response\n");
				handleResponseEvent(event);
				continueToLoop = 0; // fall through		
				break;
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event
       		    // indicates that request has not been fully satisfied.
				printf("Processing Partial Response\n");
				handleResponseEvent(event);
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event);
				break;
		}
		blpapi_Event_release(event);
	}
	//stop session and release resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);

	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("Simple Intraday Tick Example\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}