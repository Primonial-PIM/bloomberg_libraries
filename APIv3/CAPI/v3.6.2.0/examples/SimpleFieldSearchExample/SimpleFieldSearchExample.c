/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 SimpleFieldSearchExample.c: 
	This program demonstrate how to do the field search using V3.x C API. 
	It demonsatrate how to create the field search request and parse the 
	response to	get the field ID, Mnemonics and Description.
	It uses API field Service(//blp/apiflds) provided by Bloomberg API.
	It does following:
		1. Establishing a session which facilitates connection to the bloomberg
		   network
		2. Initiating the API Field Service(//blp/apiflds) for field search 
		   request.
		3. Creating and sending request to the session.  
			- Creating 'FieldSearchRequest' request 
			- Adding field searchSpec to the request
			- Sending the request
		4. Event Handling.
 Usage: 
    Run the program with default search specification which is
	searchSpec = mutual fund 
	returnFieldDocumentation = -1
	exclude 
	  productType = Govt
	  fieldType = Realtime

******************************************************************************/
#include <blpapi_session.h>
#include <blpapi_eventdispatcher.h>

#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_element.h>
#include <blpapi_name.h>
#include <blpapi_request.h>
#include <blpapi_subscriptionlist.h>
#include <blpapi_defs.h>
#include <blpapi_exception.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) and memset(3C) */

static char*         d_host;
static int           d_port;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printField
Description : This function parse the fieldData element and prints the fieldID,
			  Mnemonics and description on the screen.
Arguments   : Pointer to the fieldData element.
Returns     : void
*****************************************************************************/
void printField (blpapi_Element_t *field)
{
	blpapi_Element_t *fieldIdEle = NULL;
	const char *fieldId = NULL;
	// Get security element
	blpapi_Element_getElement(field, &fieldIdEle, "id", 0);
	assert(fieldIdEle);
	// Read the security specified
	blpapi_Element_getValueAsString(fieldIdEle, &fieldId, 0);
	assert(fieldId);
	if(blpapi_Element_hasElement(field, "fieldInfo", 0)){
		blpapi_Element_t *fieldInfoEle = NULL;
		const char *fieldInfo = NULL;
		blpapi_Element_t *fieldMnemonicEle = NULL;
		const char *fieldMnemonic = NULL;
		blpapi_Element_t *fieldDescrEle = NULL;
		const char *fieldDescr = NULL;
		blpapi_Element_t *fieldDocEle = NULL;
		const char *fieldDoc = NULL;

		// Get fieldInfo element
		blpapi_Element_getElement(field, &fieldInfoEle, "fieldInfo", 0);
		assert(fieldInfoEle);

		// Get Field Mnemonic element
		blpapi_Element_getElement(fieldInfoEle, &fieldMnemonicEle, "mnemonic", 0);
		assert(fieldMnemonicEle);
		// Read the Field Mnemonic specified
		blpapi_Element_getValueAsString(fieldMnemonicEle, &fieldMnemonic, 0);
		assert(fieldMnemonic);

		// Get Field description element
		blpapi_Element_getElement(fieldInfoEle, &fieldDescrEle, "description", 0);
		assert(fieldDescrEle);
		// Read the Field description specified
		blpapi_Element_getValueAsString(fieldDescrEle, &fieldDescr, 0);
		assert(fieldDescr);
		printf("%s\t%s\t%s\n", fieldId, fieldMnemonic, fieldDescr);
		// Get Field documentation element
		blpapi_Element_getElement(fieldInfoEle, &fieldDocEle, "documentation", 0);
		if(fieldDocEle != 0){
			// Read the Field documenation available
			blpapi_Element_getValueAsString(fieldDocEle, &fieldDoc, 0);
			printf("Documentation : \n%s\n\n", fieldDoc);			
		}
	}else{
		blpapi_Element_t *fieldErrorEle = NULL;
		const char *fieldError = NULL;
		// Get fieldError element
		blpapi_Element_getElement(field, &fieldErrorEle, "fieldError", 0);
		assert(fieldErrorEle);
		// Read the fieldError specified
		blpapi_Element_getValueAsString(fieldErrorEle, &fieldError, 0);
		assert(fieldError);
		printf("\n ERROR: %s - %s\n", fieldId, fieldError);
	}
}

/*****************************************************************************
Function    : PrintHeader
Description : This function print the header line for the output.
Arguments   : void
Returns     : void
*****************************************************************************/
void printHeader ()
{
	printf("FIELD ID\tMNEMONIC\tDESCRIPTION\n");
}


/*****************************************************************************
Function    : handleResponseEvent
Description : This function handle response and partial response event. This 
			  function gets the messages from the event and print them on the 
			  console. 
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event,
								blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		
		blpapi_Element_t *fieldInfoResponse = NULL;
		blpapi_Element_t *fieldDataArray = NULL;
		int numItems = 0;
		int i = 0;

		assert(message);
		fieldInfoResponse = blpapi_Message_elements(message);
		assert(fieldInfoResponse);
		
		// If a request cannot be completed for any reason, the responseError
		// element is returned in the response. responseError contains detailed 
		// information regarding the failure.
		// Printing the responseError on the console, release the allocated 
		// resources and exiting the program
		if (blpapi_Element_hasElement(fieldInfoResponse, "responseError", 0)) {
			fprintf(stderr, "has responseError\n");
			blpapi_Element_print(fieldInfoResponse, &streamWriter, stdout, 0, 4);
            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
		
		blpapi_Element_getElement(fieldInfoResponse, &fieldDataArray, "fieldData", 0);
		// Get the number of securities received in message
		numItems = blpapi_Element_numValues(fieldDataArray);		
		for (i = 0; i < numItems; ++i) {
			blpapi_Element_t *fieldData = 0;
			blpapi_Element_getValueAsElement(fieldDataArray, &fieldData, i);
			assert(fieldData);
			// Parse the response and print the same on console
			printField (fieldData);
		}
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than response and partial 
			  response event. This function gets the messages from the event 
			  and print them on the console. If the event is session terminate, 
			  then release allocated resources and exit the program. 
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
								blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		
		// If session status is sessionTerminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : sendFieldSearchRequest
Description : This function does following.
				 - Create a FieldSearchRequest request 
				 - Add searchSpec to request
				 - Add exclude creteria to request, if any.        		
				 - Sends request to session.
Argument    : Pointer to session
Return      : void
*****************************************************************************/
void sendFieldSearchRequest(blpapi_Session_t *session)
{
	blpapi_Service_t *fieldInfoService = NULL;
	blpapi_Request_t *request = NULL;
	blpapi_Element_t *elements = NULL;
	blpapi_Element_t *securitiesElements = NULL;
	blpapi_Element_t *fieldsElements = NULL;
	blpapi_Element_t *excludeElement = NULL;
	blpapi_CorrelationId_t correlationId;

	// get handle for the API Field service
	// TODO: What if getting handle fails or fieldInfoService is still null?
	blpapi_Session_getService(session, &fieldInfoService, "//blp/apiflds");

	// Create FieldSearchRequest 
	// TODO: What if creating the request fails or request is still null?
	blpapi_Service_createRequest(fieldInfoService, &request, 
									"FieldSearchRequest");
	assert(request);

	// Get request elements
	elements = blpapi_Request_elements(request);
	assert(elements);

	// set searchSpec element for the request. This is the mandatory element.
	blpapi_Element_setElementString(elements, "searchSpec", 0, "mutual fund");
	// Set returnFieldDocumentation element for the request. 
	// This is an optional element.
	blpapi_Element_setElementString(elements, "returnFieldDocumentation", 0, 
												"-1");

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;

	// Print the request on the output.
	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);

	// Sending request
	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	blpapi_Request_destroy(request);
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate field search 
			  request.It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitates connection to the 
			      bloomberg network
   		      3. Opens a /blp/apiflds service with the session. 
			  4. Create and send FieldSearchRequest.
				 - Create a  request 
				 - Add searchSpec to request
				 - Add exclude creteria to request.        		
			  5. Event Loop and Response Handling.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = NULL;
	blpapi_Session_t *session = NULL;
	blpapi_Service_t *fieldInfoService = NULL;
	blpapi_Request_t *request = NULL;
	blpapi_Element_t *elements = NULL;
	blpapi_Element_t *securitiesElements = NULL;
	blpapi_Element_t *fieldsElements = NULL;
	int continueToLoop = 1;
	
	// Set default value
    d_host = "localhost";
    d_port = 8194;
	
	// create options for session. We are allocating resources for 
	// session option, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host IP and port for the session. For more options 
	// please refer to WAPI<GO>.
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open API Field Service with session.
	if (0 != blpapi_Session_openService(session, "//blp/apiflds")){
		fprintf(stderr, "Failed to open service //blp/apiflds.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// create and send FieldSearchRequest to session
	sendFieldSearchRequest(session);

	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = NULL;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event
       		    // indicates that request has not been fully satisfied.
				printf("Processing Partial Response\n");
				handleResponseEvent(event, session);
				break;
			case BLPAPI_EVENTTYPE_RESPONSE: /* final event */
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional  
                // events should be expected.	
				printf("Processing Response\n");
				handleResponseEvent(event, session);
				continueToLoop = 0; /* fall through */
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event, session);
				break;
		}
		blpapi_Event_release(event);
	}

	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);

	return 0;
}

int main(int argc, char **argv)
{
	printf("SimpleFieldSearchExample\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}