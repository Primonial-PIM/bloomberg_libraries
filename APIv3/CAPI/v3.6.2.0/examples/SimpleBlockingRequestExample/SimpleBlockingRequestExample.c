/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 SimpleBlockingRequestExample.c: 
	This program shows how to request data and wait for the data to return 
	completely and use EventQueue. The EventQueue class allows an application to
	block and receive Events for requests in a synchronous manner.
	It does following:
		1. Establishing a session which facilitates connection to the bloomberg 
		   network.
		2. Initiating the Market Data Data Service(//blp/mktdata) for realtime
		   data.
		3. Initiating the Market Data Data Service(//blp/refdata) for reference
		   data request.
		4. Creating and sending subscription request to the session.
			- Creating subscription list
			- Add securities and fields to subscription list
			- Subscribe to realtime data
		5. Creating and sending reference data request to the session using 
		   Event queue to block and receive Events for requests in a synchronous
		   manner.
		6. Event Handling of the responses received.
 Usage: 
    SimpleBlockingRequestExample -help 
	SimpleBlockingRequestExample -?
	   Print the usage for the program on the console

	SimpleBlockingRequestExample
	   Run the program with default values. Program requests reference data 
	   in synchronous manner using EventQueue for following securities 
	   "IBM US Equity" & "MSFT US Equity" 
	   and fields "PX_LAST", "PX_BID", "PX_ASK"
	   Program also Subscribes to streaming updates and prints them on the 
	   console for following two default securities
	   1. Ticker - "IBM US Equity"
	   2. Cusip - 097023105

    example usage:
	SimpleBlockingRequestExample
	SimpleBlockingRequestExample -ip localhost -p 8194

	Prints the response on the console of the command line requested data
**********************************************************************************************************/

#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <blpapi_subscriptionlist.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for memset(3C) */

static char*         d_host;
static int           d_port;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("    Program shows how to request data in synchronous manner and \n"
		   "    wait for the data to return using EventQueue\n");
    printf("        [-ip        <ipAddress  = localhost>\n");
    printf("        [-p         <tcpPort    = 8194>\n");
	printf("Press ENTER to quit\n");
	getchar();
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/host/port
Argument	: Command line parameters
Returns		: int: 
			  0, if successfully set the input argument for the request 
              from command line or using default values otherwise -1
*****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Default value for host and port if not specified on command line
	d_host = "localhost";
	d_port = 8194;

	if (argc == 2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

    for (i = 1; i < argc; ++i) {
		if (i == 0) continue; // ignore the program name.
        if (!strcmp(argv[i],"-ip") && i+1 < argc) 
            d_host = argv[++i];
        else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
        else { 
			// print usage if user specify options other than the supported one.
            printUsage();
            return -1;
        }
    }
    return 0;
}

/*****************************************************************************
Function    : getTimeStamp
Description : Sets current local time to string buffer
Argument    : Pointer to string buffer
			  bufSize - size of string buffer
Returns     : int - no.of characters placed in the buffer
*****************************************************************************/
int getTimeStamp(char *buffer, size_t bufSize)
{
    const char *format = "%Y-%m-%dT%X";

    time_t now = time(0);
#ifdef WIN32
	struct tm _timeInfo, *timeInfo;
	localtime_s(&_timeInfo, &now);
	timeInfo = &_timeInfo;
#else
    struct tm _timeInfo;
	struct tm *timeInfo = localtime_r(&now, &_timeInfo);
#endif
    return strftime(buffer, bufSize, format, timeInfo);
}

/*****************************************************************************
Function    : handleSubscriptionEvent
Description : This function handles subscription data and subscription status 
              event. This function reads update data messages in the event
			  element and prints them on the console.
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleSubscriptionEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
    char timeBuffer[64];
    const char *topic;
	assert(event);

	getTimeStamp(timeBuffer, sizeof(timeBuffer));

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = 0;
		assert(message);
		
		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		if(correlationId.valueType == BLPAPI_CORRELATION_TYPE_POINTER &&
			correlationId.value.ptrValue.pointer != NULL){
			topic = (char *) correlationId.value.ptrValue.pointer;
			printf("%s: %s - ", timeBuffer, topic);
		}

		messageElements = blpapi_Message_elements(message);
		
		// Get the message element and print it on console.
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleResponseEvent
Description : This function handle response and partial response event. This 
			  function gets the messages from the event and print them on the 
			  console. In order to understand how to traverse the message to 
			  fetch particular element, please refer to refDataExample in SDK.
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event,
								blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	assert(session);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than subscription data and 
			  subscription status event, response and partial response event.
			  This function gets the messages from the event and print them 
			  on the console. If the event is session terminate, then release 
			  allocated resources and exit the program.
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		
		// If session status is session terminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout,	"Terminating: %s\n",
					blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			exit(1);
		}
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : processEvent
Description : This function is an Event Handler to process events generated
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
			  Pointer to userData
Returns     : void
*****************************************************************************/
// Event Handler to process events generated
static void processEvent(blpapi_Event_t *event, 
						 blpapi_Session_t *session, 
						 void *buffer)
{
	assert(event);
	assert(session);
	switch (blpapi_Event_eventType(event)) {
		// Process events BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
		// & BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
		case BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA:
			printf("Processing SUBSCRIPTION_DATA\n");
			handleSubscriptionEvent(event, session);
			break;
		case BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS:
			printf("Processing SUBSCRIPTION_STATUS\n");
			handleSubscriptionEvent(event, session);
			break;
		default:
			// Process events other than BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
			// or BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
			handleOtherEvent(event, session);
			break;
	}
}

/*****************************************************************************
Function    : subscribe
Description : This function creates subscription list and subscribes to 
			  realtime streaming updates for IBM US Equity and CUSIP 097023105
Arguments   : Pointer to session
Returns     : void
*****************************************************************************/
void subscribe(blpapi_Session_t *session)
{
	blpapi_SubscriptionList_t *subscriptions = NULL;

	/* IBM */
	const char *topic_IBM = "IBM US Equity";
	const char *fields_IBM[] = { "LAST_TRADE" };
	const char **options_IBM = NULL;
	int numFields_IBM = sizeof(fields_IBM)/sizeof(*fields_IBM);
	int numOptions_IBM = 0;

	/* CUSIP 097023105 */
	const char *topic_097023105 = 
					"/cusip/097023105?fields=LAST_PRICE";
	const char **fields_097023105 = NULL;
	const char **options_097023105 = NULL;
	int numFields_097023105 = 0;
	int numOptions_097023105 = 0;

	blpapi_CorrelationId_t subscriptionId_IBM;
	blpapi_CorrelationId_t subscriptionId_097023105;

	// Correlation ID for IBM US Equity
	memset(&subscriptionId_IBM, '\0', sizeof(subscriptionId_IBM));
	subscriptionId_IBM.size = sizeof(subscriptionId_IBM);
	subscriptionId_IBM.valueType = BLPAPI_CORRELATION_TYPE_POINTER;
	subscriptionId_IBM.value.ptrValue.pointer = (char *) topic_IBM;

	// Correlation ID for CUSIP 097023105
	memset(&subscriptionId_097023105, '\0', sizeof(subscriptionId_097023105));
	subscriptionId_097023105.size = sizeof(subscriptionId_097023105);
	subscriptionId_097023105.valueType = BLPAPI_CORRELATION_TYPE_POINTER;
	subscriptionId_097023105.value.ptrValue.pointer = (char *) topic_097023105;

	// Create subscription list
	subscriptions = blpapi_SubscriptionList_create();
	assert(subscriptions);
	
	// adding IBM US Equity to subscription list
	blpapi_SubscriptionList_add(subscriptions, 
								topic_IBM, 
								&subscriptionId_IBM, 
								fields_IBM, 
								options_IBM, 
								numFields_IBM, 
								numOptions_IBM);
	// adding CUSIP 097023105 to subscription list
	blpapi_SubscriptionList_add(subscriptions, 
								topic_097023105, 
								&subscriptionId_097023105, 
								fields_097023105,
								options_097023105,
								numFields_097023105,
								numOptions_097023105);

	// Subscribing to realtime data
	blpapi_Session_subscribe(session, subscriptions, 0, 0, 0);

	// release subscription list
	blpapi_SubscriptionList_destroy(subscriptions);

	return;
}

/*****************************************************************************
Function    : sendRefDataRequest
Description : Create and send reference data request to session using EventQueue
			  to block and receive Events for requests in a synchronous manner.
			  This function does following.
				 - Create a referenceData request 
				 - Adds securities to request
				 - Adds fields to request.
				 - Sends request to session using EventQueue.
				 - Handles response 
Argument    : Pointer to session
Return      : void
*****************************************************************************/
void sendRefDataRequest(blpapi_Session_t *session)
{
	blpapi_Service_t *refDataSvc = NULL;
	blpapi_Request_t *request = NULL;
	blpapi_Element_t *elements = NULL;
	blpapi_Element_t *securitiesElements = NULL;
	blpapi_Element_t *fieldsElements = NULL;
	blpapi_CorrelationId_t correlationId;
	blpapi_EventQueue_t *eventQueue;
	blpapi_Event_t *event;
	int continueToLoop = 1;

	assert(session);
	blpapi_Session_getService(session, &refDataSvc, "//blp/refdata");
	// Create Reference Data Request using //blp/refdata service
	blpapi_Service_createRequest(refDataSvc, &request, "ReferenceDataRequest");
	assert(request);

	elements = blpapi_Request_elements(request);
	assert(elements);

	// Get "securities" element
	blpapi_Element_getElement(elements,	&securitiesElements, "securities", 0);
	assert(securitiesElements);
	blpapi_Element_setValueString(securitiesElements, "IBM US Equity", 
									BLPAPI_ELEMENT_INDEX_END);
	blpapi_Element_setValueString(securitiesElements, "MSFT US Equity", 
									BLPAPI_ELEMENT_INDEX_END);

	// Get "fields" element
	blpapi_Element_getElement(elements, &fieldsElements, "fields", 0);
	blpapi_Element_setValueString(fieldsElements, "PX_LAST", 
									BLPAPI_ELEMENT_INDEX_END);
	blpapi_Element_setValueString(fieldsElements, "PX_ASK", 
									BLPAPI_ELEMENT_INDEX_END);
	blpapi_Element_setValueString(fieldsElements, "PX_BID", 
									BLPAPI_ELEMENT_INDEX_END);

	eventQueue = blpapi_EventQueue_create();

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;
	// Sending request using EventQueue to block and receive Events for 
	// requests in a synchronous manner. 
	blpapi_Session_sendRequest(session, request, &correlationId, 0, eventQueue,
								0, 0);
	event = blpapi_EventQueue_nextEvent(eventQueue, 0);
	assert(event);
	while(continueToLoop){
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event
       		    // indicates that request has not been fully satisfied.
				printf("Processing Partial Response\n");
				handleResponseEvent(event, session);
				break;
			case BLPAPI_EVENTTYPE_RESPONSE: /* final event */
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional  
                // events should be expected.	
				printf("Processing Response\n");
				handleResponseEvent(event, session);
				continueToLoop = 0; /* fall through */
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event, session);
				break;
		}
		// If response is received, just break the loop.
		if(continueToLoop == 0){
			break;
		}
		event = blpapi_EventQueue_nextEvent(eventQueue, 0);
	}
	blpapi_EventQueue_destroy(eventQueue);
	blpapi_Request_destroy(request);
	
	return;
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate how to make 
			  subscription to particular security/ticker to get realtime 
			  streaming updates. It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitates connection to the 
			      bloomberg network. Also specifies Event handler to be 
				  called for events generated.
   		      3. Opens a mktdata service with the session. 
			  4. create and send subscription request.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = NULL;
	blpapi_Session_t *session = NULL;

	// read command line parameters
	if (parseCommandLine(argc, argv) == -1) {
		return -1;
	}

	// create sessionOptions instance. We are allocating resources for
	// sessionOptions, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host and port for the session. 
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	printf("Connecting to %s:%d\n", d_host, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 
									&processEvent, 
									0, 
									0);
	assert(session);
	blpapi_SessionOptions_destroy(sessionOptions);

	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// Open mktdata Service
	if (0 != blpapi_Session_openService(session,"//blp/mktdata")){
		fprintf(stderr, "Failed to open service //blp/mktdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// Open Reference Data Service
	if (0 != blpapi_Session_openService(session, "//blp/refdata")){
		fprintf(stderr, "Failed to open service //blp/refdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// Make subscription to realtime streaming data
	subscribe(session);

	// create and send reference data request to session using EventQueue
	// to block and receive Events for requests in a synchronous manner
	sendRefDataRequest(session);

	printf("Press ENTER to quit\n");
	getchar();

	// Destory/release allocated resources.
	blpapi_Session_destroy(session);

	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("SimpleBlockingRequestExample\n");

	run(argc, argv);

	return 0;
}
