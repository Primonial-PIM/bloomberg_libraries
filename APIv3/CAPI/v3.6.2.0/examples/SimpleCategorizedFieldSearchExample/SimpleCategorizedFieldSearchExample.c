/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/****************************************************************************************************
 SimpleCategorizedFieldSearchExample.c: 
	This program demonstrate how to do the categorized field search using bloomberg v3.x C API. 
	It demonsatrate how to create the categorized field search request and parse the response to
	get the field ID, Mnemonics and Description.
	It uses API field Service(//blp/apifld) provided by Bloomberg API.
	It does following:
		1. Establishing a session which facilitate connection to the bloomberg network
		2. Initiating the API Field Service(//blp/refdata) for categorizrd field search request.
		3. Creating and sending request to the session.  
			- Creating 'CategorizedFieldSearch' request 
			- Adding various fields to request
			- Sending the request
		4. Event Handling.
 Usage: 
    SimpleCategorizedFieldSearchExample 
	  Run the program with default values. Prints the response message on the 
	   console. 
**********************************************************************************************************/

// Bloomberg API includes
#include <blpapi_session.h>
#include <blpapi_eventdispatcher.h>

#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_element.h>
#include <blpapi_name.h>
#include <blpapi_request.h>
#include <blpapi_subscriptionlist.h>
#include <blpapi_defs.h>
#include <blpapi_exception.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) and memset(3C) */

static char*         d_host;
static int           d_port;
blpapi_Session_t *session = 0;
blpapi_Request_t *request = 0;


/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/**********************************************************************************
printField
  This function parse the fieldData element and prints the fieldID, Mnemonics and 
  description on the screen.

 Arguments: Pointer to the fieldData element.
 Returns: Void
**********************************************************************************/
void printField (blpapi_Element_t *field)
{
	blpapi_Element_t *fieldIdEle = 0;
	const char *fieldId = 0;
	// Get field Id from fieldData element
	blpapi_Element_getElement(field, &fieldIdEle, "id", 0);
	assert(fieldIdEle);
	blpapi_Element_getValueAsString(fieldIdEle, &fieldId, 0);
	assert(fieldId);

	// If the fields has fieldInfo element, then get the desc and 
	// mnemonics
	if(blpapi_Element_hasElement(field, "fieldInfo", 0)){
		blpapi_Element_t *fieldInfoEle = 0;
		const char *fieldInfo = 0;
		blpapi_Element_t *fieldMnemonicEle = 0;
		const char *fieldMnemonic = 0;
		blpapi_Element_t *fieldDescrEle = 0;
		const char *fieldDescr = 0;

		// Get fieldInfo element
		blpapi_Element_getElement(field, &fieldInfoEle, "fieldInfo", 0);
		assert(fieldInfoEle);

		// Get Field Mnemonic element
		blpapi_Element_getElement(fieldInfoEle, &fieldMnemonicEle, "mnemonic", 0);
		assert(fieldMnemonicEle);
		// Read the Field Mnemonic specified
		blpapi_Element_getValueAsString(fieldMnemonicEle, &fieldMnemonic, 0);
		assert(fieldMnemonic);

		// Get Field description element
		blpapi_Element_getElement(fieldInfoEle, &fieldDescrEle, "description", 0);
		assert(fieldDescrEle);
		// Read the Field description specified
		blpapi_Element_getValueAsString(fieldDescrEle, &fieldDescr, 0);
		assert(fieldDescr);
		printf("%-6s\t%-30s\t%s\n", fieldId, fieldMnemonic, fieldDescr);
	}else{
		blpapi_Element_t *fieldErrorEle = 0;
		const char *fieldError = 0;
		// Get fieldError element
		blpapi_Element_getElement(field, &fieldErrorEle, "fieldError", 0);
		assert(fieldErrorEle);
		// Read the fieldError specified
		blpapi_Element_getValueAsString(fieldErrorEle, &fieldError, 0);
		assert(fieldError);
		printf("\n ERROR: %s - %s\n", fieldId, fieldError);
	}
}

/*****************************************************************************
PrintHeader
   This function print the header line for the output. 

 Argument: NULL
 Returns: Void
*****************************************************************************/
void printHeader ()
{
	printf("ID\tMNEMONIC\t\t\tDESCRIPTION\n");
	printf("--\t--------\t\t\t-----------\n\n");
}

/*******************************************************************************
 handleResponseEvent
	This function handle response and partial response event. This function
	gets the messages from the event and parse them to print FieldID, Mnemonics and Descritpion. 
	Printing of the elemnt is done by printElement function.

  Argument: Pointer to blpapi_Event_t
  Returns: void
********************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate thru messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		
		blpapi_Element_t *fieldInfoResponse = 0;
		blpapi_Element_t *categories = 0;
		int numCatergories = 0;
		int i = 0;

		assert(message);

		// Get the field info Response message element.		
		fieldInfoResponse = blpapi_Message_elements(message);
		assert(fieldInfoResponse);
		//blpapi_Element_print(fieldInfoResponse, &streamWriter, stdout, 0, 4);
		
        // check whether response message has element response_error. If yes
        // then no security_data element is present. destory the allocated resources
		// and exit.
		if (blpapi_Element_hasElement(fieldInfoResponse, "responseError", 0)) {
			fprintf(stderr, "has responseError\n");
			blpapi_Element_print(fieldInfoResponse, &streamWriter, stdout, 0, 4);

			// Destroy allocated resources
			blpapi_MessageIterator_destroy(iter);
			blpapi_Request_destroy(request);
			blpapi_Session_destroy(session);
			exit(1);
		}
		
        // If Response doesn't have response error, then it has category as per schema. 
		// Get category from the response.
		// TODO: Do we need any error handling here.
		blpapi_Element_getElement(fieldInfoResponse, &categories, "category", 0);

		// Get the number of category element from the categories in message
		numCatergories = blpapi_Element_numValues(categories);		
		for (i = 0; i < numCatergories; ++i) {
			blpapi_Element_t *fieldDataArray = 0;
			blpapi_Element_t *fieldData = 0;
			blpapi_Element_t *category = 0;
			blpapi_Element_t *categoryNameEle = 0;
			const char *categoryName = 0;
			blpapi_Element_t *categoryIdEle = 0;
			const char *categoryId = 0;
			int numElements = 0;
			int i = 0;

			// Get the category element from the array.
			blpapi_Element_getValueAsElement(categories, &category, i);
			assert(category);

			//get the category name and Id for the catgory and print it.
			blpapi_Element_getElement(category, &categoryNameEle, "categoryName", 0);
			blpapi_Element_getValueAsString(categoryNameEle, &categoryName, 0);
			blpapi_Element_getElement(category, &categoryIdEle, "categoryId", 0);
			blpapi_Element_getValueAsString(categoryIdEle, &categoryId, 0);
			printf(" \nCategory Name:%s\t\tID:%s\n", categoryName, categoryId);

			//Print the header for the output
			printHeader();

			// get the fieldDataarray from the category.
			blpapi_Element_getElement(category, &fieldDataArray, "fieldData", 0);
			numElements = blpapi_Element_numValues(fieldDataArray);
            for (i = 0; i < numElements; i++) {
				// for each fieldData in the array, print field data value.
				blpapi_Element_getValueAsElement(fieldDataArray, &fieldData, i);
                printField(fieldData);
            }
		}
	}
	blpapi_MessageIterator_destroy(iter);
}

/******************************************************************************************
 handleOtherEvent
      This function handles events other than response and partial response event. This 
	  function gets the messages from the event and print them on the console. If the event 
	  is session terminate, then release allocated resources and exit the program. 

 Arguments: Pointer to blpapi_Event_t
 Returns: void
*******************************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate thruogh messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = 0;
		assert(message);
		printf("messageType=%s\n", blpapi_Message_typeString(message));

		// Get the message elements and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		printf("\n");

		// If session status is session terminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", blpapi_Message_typeString(message));
			blpapi_MessageIterator_destroy(iter);
			blpapi_Request_destroy(request);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);	
}

/***************************************************************************************************
 run                                                                                     
   	This function runs the application to demonstrate categorized field search request.
	It does following:
	  1. Reads command line arguments.
   	  2. Establishes a session which facilitate connection to the bloomberg network
   	  3. Opens a /blp/apiflds service with the session. 
    3. create and send CategorizedFieldSearchRequest request:  
		- Create a  request 
        - Add searchSpec to request
        - Add exclude creteria to request.        		
    4. Event Loop and Response Handling.

 Arguments: Command line parameters.
 Returns: integer. returns 1 if error occurs; otherwise returns 0.
*****************************************************************************************************/
int run(int argc, char **argv)
{

	blpapi_SessionOptions_t *sessionOptions = 0;
	blpapi_Service_t *fieldInfoService = 0;
	blpapi_Element_t *elements = 0;
	blpapi_Element_t *securitiesElements = 0;
	blpapi_Element_t *excludeElement = 0;
	blpapi_Element_t *fieldsElements = 0;
	blpapi_CorrelationId_t correlationId;

	int continueToLoop = 1;
	
	// Set default value
    d_host = "localhost";
    d_port = 8194;

	// create options for session. We are allocating resources for 
	// session option, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host IP and port for the session. For more options 
	// please refer to WAPI<GO>.
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);

	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open API Field Service with session.
	if (0 != blpapi_Session_openService(session, "//blp/apiflds")){
		fprintf(stderr, "Failed to open service //blp/apiflds.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// get handle for the API Field service
	blpapi_Session_getService(session, &fieldInfoService, "//blp/apiflds");

	// CreateCatergorize Field Search Request
	blpapi_Service_createRequest(fieldInfoService, &request, "CategorizedFieldSearchRequest");
	assert(request);


	// Get request elements
	elements = blpapi_Request_elements(request);
	assert(elements);
	assert(request);

	// set searchSpec element for the request. This is the mandatory element.
	blpapi_Element_setElementString(elements, "searchSpec", 0, "mutual fund");

	// Set returnFieldDocumentation element or the request. This is an optional element.
	blpapi_Element_setElementString(elements, "returnFieldDocumentation", 0, "-1");

	// Get exclude element from request and set the exclude criteria.
	blpapi_Element_getElement(elements, &excludeElement, "exclude", 0);

	// Set the productType to exclude from the field search criteria. Possible value are:
	// {All, Govt, Corp, Mtge, Equity, Index, Cmdty, curncy, Pfd, M-Mkt, Muni} 
	blpapi_Element_setElementString(excludeElement, "productType", 0, "Govt");
	// set the field type. Possible values are:
	// {Static, RealTime, All}
    //blpapi_Element_setElementString(excludeElement, "fieldType", 0, "Static");

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;

	// Print the request on the output.
	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);
	printf("\n");

	// Sending request
	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = 0;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_RESPONSE: 
				// Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional events 
                // should be expected.
				printf("Processing Response\n");
				handleResponseEvent(event);
				continueToLoop = 0;
				break;
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event 
				// indicates that request has not been fully satisfied
				printf("Processing Partial Response\n");
				handleResponseEvent(event);
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event);
				break;
		}
		// release the event
		blpapi_Event_release(event);
	}
	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Request_destroy(request);
	blpapi_Session_destroy(session);

	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("Simple Categorized Field Search Example\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}