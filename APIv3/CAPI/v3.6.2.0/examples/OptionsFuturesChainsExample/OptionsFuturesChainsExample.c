/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 OptionsFuturesChainsExample.c: 
	This program shows how to request and process options and futures chains for 
	a security. It uses Reference Data Service(//blp/refdata) provided by API to obtain
	the options and futures chains information, and the Market Data Service (//blp/mktdata)
	provided by the API to obtain the real time quotes for the chains.
	It does following:
		1. Establishing a session which facilitate connection to the bloomberg 
		   network
		2. Initiating the Reference Data Service(//blp/refdata) for obtaining the
		options and futures chains information.
		3. Creating and sending request to the session.  
			- Creating 'ReferenceDataRequest' request
			  (The Reference Data Request enables you to retrieve real-time &
			   delayed snapshot data, including pricing, descriptive data, and
			   fundamental data)
			- Adding securities/fields to request
			- Sending the request
		4. Event Handling of the responses received.
		5. Initiating the Market Data Service(//blp/mktdata) for obtaining the
		real time streaming options and futures chains quotes.
		6. Creating and sending Subscription request to the session.  
			- Creating Subscription request
			- Adding securities/fields to request
			- Subscribing
		7. Event Handling of the responses received.
		 
 Usage: 
    OptionsFuturesChainsExample -help 
	OptionsFuturesChainsExample -?
	   Print the usage for the program on the console

	OptionsFuturesChainsExample
	   Run the program with default values. Parses the response of 
	   ReferenceDataReuest & Prints the response message on the console. 	   

    example usage:
	OptionsFuturesChainsExample -ip localhost -p 8194 -s "CAC Index"
	OptionsFuturesChainsExample -s "BAC US Equity" 
	OptionsFuturesChainsExample -s "IBM US Equity" -s "MSFT US Equity"

	Prints the response on the console of the command line requested data

******************************************************************************/
#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) */
#include <time.h>

#define SEC_MAX				256
#define SUBSCRIPTION_MAX	4096

static char*			d_host;       /* IP Address */ 
static int				d_port;       /* Port Number */
static char 			d_secArray[SEC_MAX][64];
static char 			d_subscriptionArray[SUBSCRIPTION_MAX][64];
static int				d_secCnt;
static int				d_subscriptionCnt;
static int		        d_numbulkFieldValues;

// Forward declaration
void subscribe(blpapi_Session_t *session);

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("    Retrieve Options & Futures Chains Data using ServerApi\n");
	printf("        [-ip        <ipAddress  = localhost>\n");
	printf("        [-p         <tcpPort    = 8194>\n");
 	printf("        [-s         <security   = \"IBM US Equity\">\n");
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/host/port
Argument	: Command line parameters
Returns		: int: 
			  '0', if successfully set the input argument for the request 
              from command line or using default values otherwise '1'
****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Default value for host and port if not specified on command line
	d_host = "localhost";
	d_port = 8194;

	if (argc == 2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i],"-ip") && i+1 < argc) 
			d_host = argv[++i];
		else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-s") &&  i+1 < argc && d_secCnt < SEC_MAX ) 
			strcpy(d_secArray[d_secCnt++], argv[++i]); 
		else { 
			// print usage if user specify options other than the supported one.
			printUsage();
			return -1;
		}
	}

	// Default security if nothing is specified on command line
	if(d_secCnt == 0){
		strcpy(d_secArray[d_secCnt++], "IBM US Equity"); 
	}
    return 0;
}

/*****************************************************************************
Function    : getTimeStamp
Description : Sets current local time to string buffer
Argument    : Pointer to string buffer
			  bufSize - size of string buffer
Returns     : size_t - no.of characters placed in the buffer
*****************************************************************************/
size_t getTimeStamp(char *buffer, size_t bufSize)
{
    const char *format = "%Y-%m-%dT%X";
    time_t now = time(0);
#ifdef WIN32
	struct tm _timeInfo, *timeInfo;
	localtime_s(&_timeInfo, &now);
	timeInfo = &_timeInfo;

    //tm *timeInfo = localtime_s(&now);
#else
    struct tm _timeInfo;
	struct tm *timeInfo = localtime_r(&now, &_timeInfo);
#endif
    return strftime(buffer, bufSize, format, timeInfo);
}

/*****************************************************************************
Function    : processFieldException
Description : This function reads fieldExceptionElement and prints its details
			  on the console specifying invalid fieldId, error catergory & 
			  message.
Argument    : Pointer to field Exception Element
Returns     : void
*****************************************************************************/
void processFieldException(blpapi_Element_t *fieldExceptionElement)
{
	int i = 0;
	int numExceptions = 0;
	const char *fieldId = 0;
	const char *category = 0;
	const char *message = 0;
	blpapi_Element_t *exception_Element = 0;
	blpapi_Element_t *errorInfo_Element = 0;
	blpapi_Element_t *fieldId_Element = 0;
	blpapi_Element_t *category_ele = 0;
	blpapi_Element_t *message_ele = 0;

	// Get the number of fieldExceptions received in message
	numExceptions = blpapi_Element_numValues(fieldExceptionElement);	
	if(numExceptions > 0){
		printf("FIELD\t\tEXCEPTION\n");
		printf("-----\t\t---------\n");
		for(i=0; i<numExceptions; i++){

			blpapi_Element_getValueAsElement(fieldExceptionElement, &exception_Element, i);
			assert(exception_Element);
			blpapi_Element_getElement(exception_Element, &errorInfo_Element, "errorInfo", 0);
			assert(errorInfo_Element);

			// read fieldId - (Calcroute ID or Mnemonic) 
			// Typically contains invalid field for the request.
			blpapi_Element_getElement(exception_Element, &fieldId_Element, "fieldId", 0);
			assert(fieldId_Element);
			blpapi_Element_getValueAsString(fieldId_Element, &fieldId, 0);

			// read error category
			blpapi_Element_getElement(errorInfo_Element, &category_ele, "category", 0);
			assert(category_ele);
			blpapi_Element_getValueAsString(category_ele, &category, 0);
			
			// read error message
			blpapi_Element_getElement(errorInfo_Element, &message_ele, "message", 0);
			assert(message_ele);
			blpapi_Element_getValueAsString(message_ele, &message, 0);
			
			printf("%s\t\t%s(%s)\n", fieldId, category, message);
		}	
	}

}

/*****************************************************************************
Function    : processRefField
Description : This function reads reference field element and prints it on the 
			  console.
Argument    : Pointer to field_Element
Returns     : void
*****************************************************************************/
void processRefField(blpapi_Element_t *field_Element)
{
	const char *fieldName = 0;
	const char *fieldValue = 0;

	fieldName = blpapi_Element_nameString (field_Element);
	blpapi_Element_getValueAsString(field_Element, &fieldValue, 0);

	printf("\t%s = %s\n", fieldName, fieldValue);
}

/*****************************************************************************
Function    : processBulkField
Description : This function reads Bulk reference field element, parses 
			  its contents and prints it on the console.
Argument    : Pointer to field_Element
Returns     : void
*****************************************************************************/
void processBulkField(blpapi_Element_t *field_Element, blpapi_Session_t *session)
{
	int bvCtr = 0;
	int beCtr = 0;
	int numOfBulkValues = 0;
	int numOfBulkEle = 0;
	const char *fieldName = 0;
	const char *bulkFieldName = 0;
	const char *bulkFieldValue = 0;
	blpapi_Element_t *bulkElement = 0;
	blpapi_Element_t *elem = 0;

	fieldName = blpapi_Element_nameString (field_Element);

	// Get the count of values for the field. 
	numOfBulkValues = blpapi_Element_numValues(field_Element);
	d_numbulkFieldValues = numOfBulkValues;
	printf("\t%s(%d) =", fieldName, d_numbulkFieldValues);
	printf("\n");

	for(bvCtr = 0; bvCtr < numOfBulkValues; bvCtr++){
		bulkElement = 0;
		blpapi_Element_getValueAsElement(field_Element, &bulkElement, bvCtr);
		assert(bulkElement);
		numOfBulkEle = blpapi_Element_numElements(bulkElement);
		for(beCtr = 0; beCtr < numOfBulkEle; beCtr++){
			elem = 0;
			blpapi_Element_getElementAt(bulkElement, &elem, beCtr);
			assert(elem);
			bulkFieldName = blpapi_Element_nameString (elem);
			blpapi_Element_getValueAsString(elem, &bulkFieldValue, 0);
			printf("\t%s = %s\n", bulkFieldName, bulkFieldValue);
			if(d_subscriptionCnt < SUBSCRIPTION_MAX ) 
			{
				strcpy(d_subscriptionArray[d_subscriptionCnt++], bulkFieldValue); 
			}
		}
		printf("\n");
	}
}

/*****************************************************************************
Function    : handleResponseEvent
Description : This function handle response and partial response event. This 
			  function gets the messages from the event, traverse the message 
			  to fetch particular element and prints them on the console.
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event,
								blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	assert(event);
	assert(session);
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *referenceDataResponse = 0;
		blpapi_Element_t *securityDataArray = 0;
		int numItems = 0;
		int i = 0;

		assert(message);

		referenceDataResponse = blpapi_Message_elements(message);
		assert(referenceDataResponse);
		
		// If a request cannot be completed for any reason, the responseError
		// element is returned in the response. responseError contains detailed 
		// information regarding the failure.
		// Printing the responseError on the console, release the allocated 
		// resources and exiting the program
		if (blpapi_Element_hasElement(referenceDataResponse, "responseError", 0)) {
			fprintf(stderr, "has responseError\n");
			blpapi_Element_print(referenceDataResponse, &streamWriter, stdout, 0, 4);
			exit(1);
		}
		
		// securityData Element contains Array of ReferenceSecurityData 
		// containing Response data for each security specified in the request.
		blpapi_Element_getElement(referenceDataResponse, &securityDataArray, "securityData", 0);
		// Get the number of securities received in message
		numItems = blpapi_Element_numValues(securityDataArray);	
		printf("\nProcessing %d security(s)\n", numItems);

		for (i = 0; i < numItems; ++i) {
			blpapi_Element_t *securityData = 0;
			blpapi_Element_t *securityElement = 0;
			const char *security = 0;
			blpapi_Element_t *sequenceNumberElement = 0;
			int sequenceNumber = -1;
			blpapi_Element_getValueAsElement(securityDataArray, &securityData, i);
			assert(securityData);

			blpapi_Element_getElement(securityData, &securityElement, "security", 0);
			assert(securityElement);

			blpapi_Element_getValueAsString(securityElement, &security, 0);
			assert(security);

			blpapi_Element_getElement(securityData, &sequenceNumberElement, "sequenceNumber", 0);
			assert(sequenceNumberElement);
			blpapi_Element_getValueAsInt32(sequenceNumberElement, &sequenceNumber, 0);

			// Checking if there is any Security Error
			if (blpapi_Element_hasElement(securityData, "securityError", 0)){
				blpapi_Element_t *securityErrorElement = 0;
				printf("\tSecurity = %s\n", security);
				blpapi_Element_getElement(securityData, &securityErrorElement, "securityError", 0);
				assert(securityErrorElement);
				blpapi_Element_print(securityErrorElement, &streamWriter, stdout, 0, 4);
				continue;
			}

			if (blpapi_Element_hasElement(securityData, "fieldData", 0)){
				int j = 0;
				int numElements = 0;
				blpapi_Element_t *fieldDataElement = 0;
				blpapi_Element_t *field_Element = 0;

				printf("\tSecurity = %s\n", security);
				printf("\tsequenceNumber = %d\n", sequenceNumber);

				blpapi_Element_getElement(securityData, &fieldDataElement, "fieldData", 0);
				assert(fieldDataElement);

				numElements = blpapi_Element_numElements(fieldDataElement);	
				printf("Processing %d field(s)\n", numElements);
				for(j=0; j<numElements; j++){
					int dataType = 0;
					blpapi_Element_getElementAt(fieldDataElement, &field_Element, j);
					assert(field_Element);
					dataType = blpapi_Element_datatype(field_Element);					
					if(dataType == BLPAPI_DATATYPE_SEQUENCE){
						processBulkField(field_Element, session);
					}else{
						processRefField(field_Element);
					}
				}
				printf("\n");
			}

			if (blpapi_Element_hasElement(securityData, "fieldExceptions", 0)){
				blpapi_Element_t *fieldExceptionElement = 0;
				blpapi_Element_getElement(securityData, &fieldExceptionElement, "fieldExceptions", 0);
				assert(fieldExceptionElement);
				processFieldException(fieldExceptionElement);
			}
		}
	}
}


/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than response and partial 
			  response event. This function gets the messages from the event 
			  and print them on the console. If the event is session terminate, 
			  then release allocated resources and exit the program. 
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;

	assert(event);
	assert(session);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate thru messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = 0;
		assert(message);
		
		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);
		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);

		// If session status is session terminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated", blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n",
			blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
	}
	blpapi_MessageIterator_destroy(iter);
}



/*****************************************************************************
Function    : handleDataEvent
Description : This function handlessubscription data and subscription status 
              event. This function reads update data messages in the event
			  element and prints them on the console.
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleDataEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = 0;
	blpapi_Message_t *message = 0;
	char timeBuffer[64];
    const char *topic;
	assert(event);

	getTimeStamp(timeBuffer, sizeof(timeBuffer));
	
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate thru messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = 0;
		assert(message);

		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		if(correlationId.valueType == BLPAPI_CORRELATION_TYPE_POINTER &&
			correlationId.value.ptrValue.pointer != NULL)
		{
			topic = (char *) correlationId.value.ptrValue.pointer;
			printf("%s: %s - ", timeBuffer, topic);
		}		
		messageElements = blpapi_Message_elements(message);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
	}
	blpapi_MessageIterator_destroy(iter);
}


/*****************************************************************************
Function    : subscribe
Description : This function creates subscription list and subscribes to 
			  realtime streaming updates for security
Arguments   : Pointer to session
Returns     : void
*****************************************************************************/
void subscribe(blpapi_Session_t *session)
{
	blpapi_SubscriptionList_t *subscriptions = 0;

	const char *fields[] = { "BID", "ASK" };
	const char **options = 0;
	char  * security;
	int numFields = sizeof(fields)/sizeof(*fields);
	int numOptions = 0;
	int i = 0;

	// Create subscription list
	subscriptions = blpapi_SubscriptionList_create();
	assert(subscriptions);

    printf( "Subscribing to %d securities now\n", d_subscriptionCnt );
	i = 0;
	while  (i < d_subscriptionCnt ) {
		blpapi_CorrelationId_t subscriptionId;
        security = (char *) d_subscriptionArray[i];
        printf( "Subscribing to %s\n", security );

		// Initialize Correlation object
		memset(&subscriptionId, '\0', sizeof(subscriptionId));
		subscriptionId.size = sizeof(subscriptionId);
		subscriptionId.valueType = BLPAPI_CORRELATION_TYPE_POINTER;
		subscriptionId.value.ptrValue.pointer = security;

		blpapi_SubscriptionList_add(subscriptions, 
									security, 
									&subscriptionId, 
									fields, 
									options, 
									numFields, 
									numOptions);
		i++;
    }
    printf( "\n" );

	// Subscribing to realtime data
	blpapi_Session_subscribe(session, subscriptions, 0, 0, 0);

	// release subscription list
	blpapi_SubscriptionList_destroy(subscriptions);

}

/*****************************************************************************
Function    : sendRefDataRequest
Description : This function does following.
				 - Create a referenceData request 
				 - Add security to request
				 - Add field to request.
				 - Sends request to session.
Argument    : Pointer to session
Return      : void
*****************************************************************************/
void sendRefDataRequest(blpapi_Session_t *session)
{
	blpapi_Service_t *refDataSvc = 0;
	blpapi_Request_t *request = 0;
	blpapi_Element_t *elements = 0;
	blpapi_Element_t *securitiesElements = 0;
	blpapi_Element_t *fieldsElements = 0;
	blpapi_CorrelationId_t correlationId;
	char  * security;
	int i = 0;

	assert(session);
	// Get //blp/refdata Service
	blpapi_Session_getService(session, &refDataSvc, "//blp/refdata");
	// Create Reference Data Requests for OPT_CHAIN and FUT_CHAIN
	blpapi_Service_createRequest(refDataSvc, &request, "ReferenceDataRequest");
	assert(request);
	// Get request elements for OPT_CHAIN and FUT_CHAIN
	elements = blpapi_Request_elements(request);
	assert(elements);

	// Get securities element for OPT_CHAIN and FUT_CHAIN
	blpapi_Element_getElement(elements,	&securitiesElements, "securities", 0);
	assert(securitiesElements);
	i = 0;
	// Set securities specified on command line
	while  (i < d_secCnt ) {
        security = (char *) d_secArray[i];
		blpapi_Element_setValueString(securitiesElements, security, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }

	// Get fields element for OPT_CHAIN and FUT_CHAIN
	blpapi_Element_getElement(elements,	&fieldsElements, "fields", 0);
	assert(fieldsElements);
	// Set fields specified in the program
	blpapi_Element_setValueString(fieldsElements, "OPT_CHAIN", BLPAPI_ELEMENT_INDEX_END);
	blpapi_Element_setValueString(fieldsElements, "FUT_CHAIN", BLPAPI_ELEMENT_INDEX_END);

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;

	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	// Destory/release allocated resources.
	blpapi_Request_destroy(request);

	return;
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate reference data
			  request. It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitate connection to the 
			      bloomberg network
   		      3. Opens a refData service with the session. 
			  4. create and send referenceData request.
			  5. Event Loop and Response Handling.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = 0;
	blpapi_Session_t *session = 0;
	int continueToLoop = 1;
	int ctr=0;
	
	// read command line parameters
    if (0 != parseCommandLine(argc, argv)) return -1;
	
	// create sessionOptions instance. We are allocating resources for 
	// sessionOptions, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host and port for the session. 
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Reference Data Service
	if (0 != blpapi_Session_openService(session, "//blp/refdata")){
		fprintf(stderr, "Failed to open service //blp/refdata.\n");
		blpapi_Session_stop(session);
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Market Data Service
	if (0 != blpapi_Session_openService(session, "//blp/mktdata")){
		fprintf(stderr, "Failed to open service //blp/mktdata.\n");
		blpapi_Session_stop(session);
		blpapi_Session_destroy(session);
		return -1;
	}

	// create and send reference data request
	sendRefDataRequest(session);

	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = 0;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_RESPONSE: /* final event */
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional  
                // events should be expected.	
				printf("Processing Response\n");
				continueToLoop = 0; /* fall through */
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event
       		    // indicates that request has not been fully satisfied.
				printf("Processing Partial Response\n");				
				handleResponseEvent(event, session);
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event, session);
				break;
		}
        // release the event
		blpapi_Event_release(event);
	}

	subscribe(session);

	// wait for events from session.
	while (1) {
		blpapi_Event_t *event = 0;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA:
				printf("Processing SUBSCRIPTION_DATA\n");
			case BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS:
				printf("Processing SUBSCRIPTION_STATUS\n");
				handleDataEvent(event, session);
			break;
			default:
				handleOtherEvent(event, session);
				break;
		}
        // release the event
		blpapi_Event_release(event);
	}

	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);
	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("OptionsFuturesChainsExample\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}