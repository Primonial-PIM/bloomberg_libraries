/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 SimpleHistoryExample.c: 
	This program shows how to make  Reference data request to get History data.
	It uses reference Data Service(//blp/refdata) provided by API.
	It does following:
		1. Establishing a session which facilitates connection to the bloomberg 
		   network
		2. Initiating the Reference Data Service(//blp/refdata) for history data.
		3. Creating and sending request to the session.  
			- Creating 'HistoricalDataRequest' request 
			  (The Historical Data Request lets you retrieve end-of-day data for
			  a specified period of time in increments of days, weeks, months,
			  quarters, or years.)
			- Adding securities/fields to request
			- Specifying the Start and end date for historical data
			- Sending the request
		4. Event Handling of the response received.
 Usage: 
    SimpleHistoryExample -help 
	SimpleHistoryExample -?
	   Print the usage for the program on the console

	SimpleHistoryExample
	   Run the program with default values as shown in Usage. 
	   Program prints the response message on the console. 

    Example Usage:
	SimpleHistoryExample -ip localhost -p 8194
	SimpleHistoryExample -s "IBM US Equity" -f VOLUME -sd 20090101 -ed 20090331
	SimpleHistoryExample -s "IBM US Equity" -f PX_LAST

	Print the response on the console of the command line requested data

******************************************************************************/

#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) and memset(3C) */

#define SEC_MAX			256
#define FIELD_MAX		256

static char*			d_host;       /* IP Address */ 
static int				d_port;       /* Port Number */
static char 			d_secArray[SEC_MAX][64];
static char 			d_fieldArray[FIELD_MAX][64];
static int				d_secCnt;
static int				d_fldCnt;
static char*			d_startDate;
static char*			d_endDate;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("    Retrieve Historical data using ServerApi\n");
    printf("        [-ip        <ipAddress  = localhost>\n");
    printf("        [-p         <tcpPort    = 8194>\n");
	printf("        [-s         <security   = \"IBM US Equity\">\n");
	printf("        [-f         <field      = PX_LAST>\n");
	printf("        [-sd        <startDate  = 20120601>]\n");
	printf("        [-ed        <endDate    = 20120901>]\n");
	printf("Notes:\n");
	printf("Multiple securities & fields can be specified.\n");
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/startdate/enddate/host/port
Argument	: Command line parameters
Returns		: int: 
			  0, if successfully set the input argument for the request 
              from command line or using default values otherwise -1
****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Set default value for host/port
    d_host = "localhost";
    d_port = 8194;
	// Set default startdate and enddate
    d_startDate = "20120601";
    d_endDate = "20120901";

	if (argc == 2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

	for (i = 1; i < argc; ++i) {
        if (!strcmp(argv[i],"-ip") && i+1 < argc) 
            d_host = argv[++i];
        else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-s") &&  i+1 < argc && d_secCnt < SEC_MAX ) 
			strcpy(d_secArray[d_secCnt++], argv[++i]); 
		else if (!strcmp(argv[i],"-f") &&  i+1 < argc && d_fldCnt < FIELD_MAX ) 
			strcpy(d_fieldArray[d_fldCnt++], argv[++i]); 
        else if (!strcmp(argv[i],"-sd") &&  i+1 < argc) 
			d_startDate = argv[++i];
        else if (!strcmp(argv[i],"-ed") &&  i+1 < argc) 
			d_endDate = argv[++i];
        else { 
 			// print usage if user specify options other than the supported one.
			printUsage();
			return -1;
        }
    }
	// Default security and field if nothing is specified on command line
	if(d_secCnt == 0){
		strcpy(d_secArray[d_secCnt++], "IBM US Equity"); 
		strcpy(d_secArray[d_secCnt++], "MSFT US Equity"); 
	}
	if(d_fldCnt == 0){
		strcpy(d_fieldArray[d_fldCnt++], "PX_BID"); 
		strcpy(d_fieldArray[d_fldCnt++], "PX_ASK"); 
		strcpy(d_fieldArray[d_fldCnt++], "PX_LAST"); 
	}
    return 0;
}

/*****************************************************************************
Function    : handleResponseEvent
Description : This function handle response and partial response event. This 
			  function gets the messages from the event and print them on the 
			  console. 
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event,
								blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	assert(session);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than response and partial 
			  response event. This function gets the messages from the event 
			  and print them on the console. If the event is session terminate, 
			  then release allocated resources and exit the program. 
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;

	assert(event);
	assert(session);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		
		// If session status is sessionTerminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", 
							blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : sendHistoricalDataRequest
Description : This function does following.
				 - Create a HistoricalDataRequest request 
				 - Add security to request
				 - Add field to request.
				 - Specify startdate and endate of history data in the request.
				 - Specify required options, if any. e.g.periodicitySelection 
				 - Sends request to session.
Argument    : Pointer to session
Return      : void
*****************************************************************************/
void sendHistoricalDataRequest(blpapi_Session_t *session)
{
	blpapi_Service_t *refDataSvc = NULL;
	blpapi_Request_t *request = NULL;
	blpapi_Element_t *elements = NULL;
	blpapi_Element_t *securitiesElements = NULL;
	blpapi_Element_t *fieldsElements = NULL;
	blpapi_Element_t *stDateElement = NULL;
	blpapi_Element_t *endDateElement = NULL;
	blpapi_CorrelationId_t correlationId;
    char  * security;
    char  * field;
	int i = 0;

	// Get //blp/refdata Service
	blpapi_Session_getService(session, &refDataSvc, "//blp/refdata");
	// Create Reference Data Request
	blpapi_Service_createRequest(refDataSvc, &request, "HistoricalDataRequest");
	assert(request);
	// Get request elements
	elements = blpapi_Request_elements(request);
	assert(elements);
	// Get securities element
	blpapi_Element_getElement(elements,	&securitiesElements, "securities", 0);
	assert(securitiesElements);

	i = 0;
	// Set securities specified on command line
	while  (i < d_secCnt ) {
        security = (char *) d_secArray[i];
		blpapi_Element_setValueString(securitiesElements, security, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }
	// Get Field element
	blpapi_Element_getElement(elements, &fieldsElements, "fields", 0);

	i = 0;
	// Set fields specified on command line
	while  (i < d_fldCnt ) {
        field = (char *) d_fieldArray[i];
		blpapi_Element_setValueString(fieldsElements, field, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }

	// Set start and End date 
	blpapi_Element_setElementString(elements, "startDate", 0, d_startDate);
	blpapi_Element_setElementString(elements, "endDate", 0, d_endDate);

	blpapi_Element_setElementString(elements, "nonTradingDayFillMethod", 0, 
										"PREVIOUS_VALUE");
	blpapi_Element_setElementString(elements, "nonTradingDayFillOption", 0, 
										"ALL_CALENDAR_DAYS");
	blpapi_Element_setElementString(elements, "periodicityAdjustment", 0, 
										"ACTUAL");
	blpapi_Element_setElementString(elements, "periodicitySelection", 0, 
										"DAILY");

	// Print the request on console
	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;
	
	// Sending request
	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	blpapi_Request_destroy(request);

	return;
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate Historical
			  data request. It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitates connection to the 
			      bloomberg network
   		      3. Opens a refData service with the session. 
			  4. Create and send HistoricalDataRequest.
			  5. Event Loop and Response Handling.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns -1 if error occurs otherwise returns 0
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = NULL;
	blpapi_Session_t *session = NULL;
	int continueToLoop = 1;

	// read command line parameters
	if (parseCommandLine(argc, argv) == -1) {
		return -1;
	}

	// create sessionOptions instance. We are allocating resources for 
	// sessionOptions, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host and port for the session. 
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Reference Data Service to make history data request
	if (0 != blpapi_Session_openService(session, "//blp/refdata")){
		fprintf(stderr, "Failed to open service //blp/refdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	// create and send historical data request to session
	sendHistoricalDataRequest(session);

	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = NULL;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event
       		    // indicates that request has not been fully satisfied.
				printf("Processing Partial Response\n");
				handleResponseEvent(event, session);
				break;
			case BLPAPI_EVENTTYPE_RESPONSE: /* final event */
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional  
                // events should be expected.	
				printf("Processing Response\n");
				handleResponseEvent(event, session);
				continueToLoop = 0; /* fall through */
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event, session);
				break;
		}
		blpapi_Event_release(event);
	}
	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);

	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("SimpleHistoryDataExample\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}