/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 IntradayBarExample.c: 
	This program shows how to make  Reference data request to get IntradayBar 
	data. It also demonstrate how to parse the response message and prints the 
	bar data on the console. It uses reference Data Service(//blp/refdata) 
	provided by API.
	It does following:
		1. Establishing a session which facilitates connection to the bloomberg 
		   network
		2. Initiating the Reference Data Service(//blp/refdata) for IntradayBar
		   data.
		3. Creating and sending request to the session.  
			- Creating 'IntradayBarRequest' request 
			  (The Intraday Bar Request enables you to get Intraday Bar data.)
			- Specifying security/eventType to request
			- Specifying the startDateTime and endDateTime for IntradayBar data
			  (Note: In v3.x API, the startdate & enddate to be specified for 
			  Intraday requests should be in GMT. Also the time received in 
			  response to the request for each event type are also in GMT.)
			- Sending the request
		4. Event Handling of the response received.
 Usage: 
    IntradayBarExample -help 
	IntradayBarExample -?
	   Print the usage for the program on the console

	IntradayBarExample
	   Run the program with default values as shown in Usage. 
	   Program prints the response message on the console. 

    Example Usage:
	IntradayBarExample -ip localhost -p 8194
	IntradayBarExample -s "IBM US Equity" -f TRADE -b 60
					-sd 2009-03-25T13:30:00	-ed 2009-03-27T13:30:00
	IntradayBarExample -s "IBM US Equity" -f TRADE -b 1

	Print the response on the console of the command line requested data

******************************************************************************/

#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) and memset(3C) */

static char*         d_host;
static int           d_port;
static char*         d_security;
static char*         d_eventType;
static char*         d_barInterval;
static char*         d_startDate;
static char*         d_endDate;
static char*         d_gapFill;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("    Retrieve Intraday Bar data using ServerApi\n");
	printf("        [-s         <security          = \"IBM US Equity\">]\n");
	printf("        [-e         <eventType         = TRADE>]\n");
	printf("        [-b         <barInterval       = 60>]\n");
	printf("        [-sd        <startDateTime     = 2012-12-26T13:30:00>]\n");
	printf("        [-ed        <endDateTime       = 2012-12-28T13:30:00>]\n");
	printf("        [-g         <gapFillInitialBar = true/false>\n");
	printf("        [-ip        <ipAddress         = localhost>]\n");
	printf("        [-p         <tcpPort           = 8194>]\n");
 	printf("Notes:\n");
	printf("Only one security and eventType can be specified in one request\n");
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/startdate/enddate/host/port
Argument	: Command line parameters
Returns		: int: 
			  0, if successfully set the input argument for the request 
              from command line or using default values otherwise -1
****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Set default values
    d_host = "localhost";
    d_port = 8194;
    d_security = "IBM US Equity";
    d_eventType = "TRADE";
	d_barInterval = "60";
	d_startDate = "2012-12-27T13:30:00";
	d_endDate = "2012-12-28T13:30:00";
	d_gapFill = "false";

	if (argc == 2) {
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

    for (i = 1; i < argc; ++i) {
        if (!strcmp(argv[i],"-ip") && i+1 < argc) 
            d_host = argv[++i];
        else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
        else if (!strcmp(argv[i],"-s") &&  i+1 < argc) 
			d_security = argv[++i];
        else if (!strcmp(argv[i],"-e") &&  i+1 < argc) 
			d_eventType = argv[++i];
        else if (!strcmp(argv[i],"-b") &&  i+1 < argc) 
			d_barInterval = argv[++i];
        else if (!strcmp(argv[i],"-sd") &&  i+1 < argc) 
			d_startDate = argv[++i];
        else if (!strcmp(argv[i],"-ed") &&  i+1 < argc) 
			d_endDate = argv[++i];
        else if (!strcmp(argv[i],"-g") &&  i+1 < argc) 
			d_gapFill = argv[++i];
        else { 
            printUsage();
            return -1;
        }
    }
    return 0;
}

/*****************************************************************************
Function    : handleIntradayResponse
Description : This function handle IntradayResponse. & demonstrate how to parse
		      the response message and prints the bar data on the console. 
Argument    : Pointer to blpapi_Element_t containing intradayBarResponse
Returns     : void
*****************************************************************************/
static void handleIntradayResponse(const blpapi_Element_t *intradayBarResponse)
{
		blpapi_Element_t *barData = NULL;
		blpapi_Element_t *barTickData = NULL;
		blpapi_Element_t *bar = NULL;
		int i = 0;
		int numBars = 0;

		blpapi_Element_getElement(intradayBarResponse, &barData, "barData", 0);
		assert(barData);
		blpapi_Element_getElement(barData, &barTickData, "barTickData", 0);
		assert(barTickData);
		// Get the number of barTickData received in message
		numBars = blpapi_Element_numValues(barTickData);		
		printf("Response contains %d bars\n", numBars);
		printf("Datetime\t\tOpen\t\tHigh\t\tLow\t\tClose\t\tNumEvents\tVolume");
		printf("\n");
	
        for (i = 0; i < numBars; ++i) {
			blpapi_Element_t *bar = NULL;
			blpapi_Element_t *time_ele = NULL;
			blpapi_Datetime_t time;// = 0;
			blpapi_Element_t *open_ele = NULL;
			double dblOpen = 0;
			blpapi_Element_t *high_ele = NULL;
			double dblHigh = 0;
			blpapi_Element_t *low_ele = NULL;
			double dblLow = 0;
			blpapi_Element_t *close_ele = NULL;
			double dblClose = 0;
			blpapi_Element_t *numEvents_ele = NULL;
			int numEvents = 0;
			blpapi_Element_t *volume_ele = NULL;
			long long lVolume = 0;

			blpapi_Element_getValueAsElement(barTickData, &bar, i);
			assert(bar);
			// Reading time element
			blpapi_Element_getElement(bar, &time_ele, "time", 0);
			assert(time_ele);
			blpapi_Element_getValueAsDatetime(time_ele, &time, 0);
			// Reading open element
			blpapi_Element_getElement(bar, &open_ele, "open", 0);
			assert(bar);
			blpapi_Element_getValueAsFloat64(open_ele, &dblOpen, 0);
			// Reading high element
			blpapi_Element_getElement(bar, &high_ele, "high", 0);
			assert(high_ele);
			blpapi_Element_getValueAsFloat64(high_ele, &dblHigh, 0);
			// Reading low element
			blpapi_Element_getElement(bar, &low_ele, "low", 0);
			assert(low_ele);
			blpapi_Element_getValueAsFloat64(low_ele, &dblLow, 0);
			// Reading close element
			blpapi_Element_getElement(bar, &close_ele, "close", 0);
			assert(close_ele);
			blpapi_Element_getValueAsFloat64(close_ele, &dblClose, 0);
			// Reading numEvents element
			blpapi_Element_getElement(bar, &numEvents_ele, "numEvents", 0);
			assert(numEvents_ele);
			blpapi_Element_getValueAsInt32(numEvents_ele, &numEvents, 0);
			// Reading volume element
			blpapi_Element_getElement(bar, &volume_ele, "volume", 0);
			assert(volume_ele);
			blpapi_Element_getValueAsInt64(volume_ele, &lVolume, 0);

			// Printing time
			printf("%04d-%02d-%02dT%02d:%02d:%02d", time.year, time.month, time.day,
				time.hours, time.minutes, time.seconds);			
			// Print open/high/low/close/numEvents/volume
			printf("\t\t%f\t\t%f\t\t%f\t\t%f\t\t%d\t%ld\n",
				dblOpen, dblHigh, dblLow, dblClose, numEvents, lVolume);
		}
}

/*****************************************************************************
Function    : handleResponseEvent
Description : This function handle response and partial response event. This 
			  function gets the messages from the event and print them on the 
			  console. 
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleResponseEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	assert(session);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *intradayBarResponse = NULL;
		assert(message);

		// Get the intradayBarResponse element
		intradayBarResponse = blpapi_Message_elements(message);
		assert(intradayBarResponse);

		// If a request cannot be completed for any reason, the responseError
		// element is returned in the response. responseError contains detailed 
		// information regarding the failure.
		// Printing the responseError on the console, release the allocated 
		// resources and exiting the program
		if (blpapi_Element_hasElement(intradayBarResponse, "responseError", 0)){
			fprintf(stderr, "has responseError\n");
			blpapi_Element_print(intradayBarResponse,&streamWriter,stdout,0,4);
            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
		// Parse intradayBarResponse received and print on console
		handleIntradayResponse(intradayBarResponse);

	}
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than response and partial 
			  response event. This function gets the messages from the event 
			  and print them on the console. If the event is session terminate, 
			  then release allocated resources and exit the program. 
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	assert(session);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		
		// If session status is sessionTerminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", 
							blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : sendIntradayBarRequest
Description : This function does following.
				 - Create a IntradayBarRequest request 
				 - Specify security/eventType to request
				 - Specify required bar interval
				 - Specify startDateTime and endDateTime of IntradayBarRequest.
				 - Specify required options, if any. e.g. gapFillInitialBar 
				 - Sends request to session.
Argument    : Pointer to session
Return      : void
*****************************************************************************/
void sendIntradayBarRequest(blpapi_Session_t *session)
{
	blpapi_Service_t *refDataSvc = NULL;
	blpapi_Request_t *request = NULL;
	blpapi_Element_t *elements = NULL;
	blpapi_Element_t *securitiesElements = NULL;
	blpapi_Element_t *fieldsElements = NULL;
	blpapi_Element_t *stDateElement = NULL;
	blpapi_Element_t *endDateElement = NULL;
	blpapi_CorrelationId_t correlationId;

	blpapi_Session_getService(session, &refDataSvc, "//blp/refdata");
	// Create Reference Data Request
	blpapi_Service_createRequest(refDataSvc, &request, "IntradayBarRequest");
	assert(request);
	// Get request elements
	elements = blpapi_Request_elements(request);
	assert(elements);

	// Set Security. Only one security per request
    blpapi_Element_setElementString(elements, "security", 0, d_security);
	// Set EventType. Only one eventType per request
	blpapi_Element_setElementString(elements, "eventType", 0, d_eventType);

	blpapi_Element_setElementString(elements, "interval", 0, d_barInterval);
	blpapi_Element_setElementString(elements, "startDateTime", 0, d_startDate);
	blpapi_Element_setElementString(elements, "endDateTime", 0, d_endDate);
	blpapi_Element_setElementString(elements, "gapFillInitialBar", 0, d_gapFill);

	// Print the request on console
	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);

	// Init Correlation ID object
	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = (blpapi_UInt64_t)1;

	// Sending request
	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	blpapi_Request_destroy(request);
}	
	
/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate IntradayBar
			  data request. It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitates connection to the 
			      bloomberg network
   		      3. Opens a refData service with the session. 
			  4. Create and send IntradayBarRequest.
			  5. Event Loop and Response Handling.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = NULL;
	blpapi_Session_t *session = NULL;
	blpapi_Service_t *refDataSvc = NULL;
	blpapi_Request_t *request = NULL;
	blpapi_Element_t *elements = NULL;
	blpapi_Element_t *securitiesElements = NULL;
	blpapi_Element_t *fieldsElements = NULL;
	blpapi_Element_t *stDateElement = NULL;
	blpapi_Element_t *endDateElement = NULL;

	int continueToLoop = 1;

	// read command line parameters
	if (parseCommandLine(argc, argv) == -1) {
		return -1;
	}

	// create sessionOptions instance. We are allocating resources for 
	// sessionOptions, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host and port for the session. 
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Reference Data Service
	if (0 != blpapi_Session_openService(session, "//blp/refdata")){
		fprintf(stderr, "Failed to open service //blp/refdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

	sendIntradayBarRequest(session);

	// Poll for the events from the session until complete response for
	// request is received. For each event received, do the desired processing.
	while (continueToLoop) {
		blpapi_Event_t *event = NULL;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_PARTIAL_RESPONSE:
				// Process the partial response event to get data. This event
       		    // indicates that request has not been fully satisfied.
				printf("Processing Partial Response\n");
				handleResponseEvent(event, session);
				break;
			case BLPAPI_EVENTTYPE_RESPONSE: /* final event */
		        // Process the response event. This event indicates that
                // request has been fully satisfied, and that no additional  
                // events should be expected.	
				printf("Processing Response\n");
				handleResponseEvent(event, session);
				continueToLoop = 0; /* fall through */
				break;
			default:
				// Process events other than PARTIAL_RESPONSE or RESPONSE.
				handleOtherEvent(event, session);
				break;
		}
		blpapi_Event_release(event);
	}

	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);

	return 0;
}

int main(int argc, char **argv)
{
	printf("IntradayBarExample\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}