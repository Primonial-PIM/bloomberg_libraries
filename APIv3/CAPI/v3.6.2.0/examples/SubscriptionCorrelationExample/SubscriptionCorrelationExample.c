/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/*****************************************************************************
 SubscriptionCorrelationExample.c: 
	This program shows how to use correlation. 
	It uses Market Data Service(//blp/mktdata) provided by API.
	To assist applications in matching incoming data to requests, the Bloomberg
	API allows applications to provide a CorrelationID object with each request.
	Subsequently, the Bloomberg infrastructure uses that identifier to tag the 
	events sent in response. On receipt of the Event object, the client can use 
	the identifier it supplied to match events to requests.	
	This program does following:
		1. Establishing a session which facilitates connection to the bloomberg
		   network
		2. Initiating the Market Data Service(//blp/mktdata) for streaming data.
		3. Creating and sending request to the session.  
			- creating Subscriptions 
			- Adding each security and its field to the subscritpion
			- Adding correlationID to the subscription.
			- subscribing to the array of subscriptions.
		4. Event Handling.
 Usage: 
    SubscriptionCorrelationExample -help 
	SubscriptionCorrelationExample -?
	   Print the usage for the program on the console

	SubscriptionCorrelationExample
	   Run the program with default values. Prints the response message on the 
	   console. 	 

    example usage:
	SubscriptionCorrelationExample -ip localhost -p 8194 -s "VOD LN Equity" -f LAST_PRICE
	SubscriptionCorrelationExample -s "IBM US Equity" -f LAST_PRICE

Output:
	Print the response on the console of the command line requested data

******************************************************************************/

#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

#define SEC_MAX     256
#define FIELD_MAX   256

static char*         d_host;
static int           d_port;
static char 		 d_secArray[SEC_MAX][64];
static char 		 d_fieldArray[FIELD_MAX][64];
static int           d_secCnt;
static int           d_fldCnt;

/**
 * A helper structure.
 * It will be used as an identifier to match incoming events to request.
 */
typedef struct Window {
    char *d_name;
	void (*fnPtr)(blpapi_Element_t *, long row );
}Window_t;

static Window_t		 d_secInfoWindow;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
			  stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : displaySecurityInfo
Description : prints the security window name and message contents
Argument    : pointer to Window variable
              long row 
Returns     : Void
*****************************************************************************/
void displaySecurityInfo(blpapi_Element_t* messageElements, long row)
{
    char  * security = 0;
	int count = 0;
	assert(messageElements);
	count = 0;
	while  (count < d_secCnt ) {
		if (count == row) {	
			security = (char *) d_secArray[count];
			break;
		}
		++count;
	}
	printf("%s : row %d got update for %s\n\n", d_secInfoWindow.d_name, row, security);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
	printf("Usage:\n");
	printf("	Retrieves subscription data using Bloomberg API & shows how to use Correlation \n");
    printf("        [-ip        <ipAddress  = localhost>]\n");
    printf("        [-p         <tcpPort    = 8194>]\n");
    printf("        [-s         <security   = \"IBM US Equity\">]\n");
    printf("        [-f         <field      = LAST_TRADE>]\n");
    printf("Notes:\n");
	printf("Multiple securities & fields can be specified.\n");

}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
			  line argument are not provided properly, it calls printUsage to 
			  print the usage on commandline. If no commnd line arguments are 
			  specified this fuction will set default values for 
			  security/fields/host/port
Argument	: Command line parameters
Returns		: int: 
			  0, if successfully set the input argument for the request 
              from command line or using default values otherwise -1
****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	// Default value for host and port if not specified on command line
	d_host = "localhost";
	d_port = 8194;

	if (argc == 2) {
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i],"-ip") && i+1 < argc) 
			d_host = argv[++i];
		else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
			d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-s") &&  i+1 < argc && d_secCnt < SEC_MAX ) 
			strcpy(d_secArray[d_secCnt++], argv[++i]); 
		else if (!strcmp(argv[i],"-f") &&  i+1 < argc && d_fldCnt < FIELD_MAX ) 
			strcpy(d_fieldArray[d_fldCnt++], argv[++i]); 
		else { 
			printUsage();
			return -1;
		}
	}

	// Default security and field if nothing is specified on command line
	if(d_secCnt == 0){
		strcpy(d_secArray[d_secCnt++], "IBM US Equity"); 
		strcpy(d_secArray[d_secCnt++], "MSFT US Equity"); 
	}
	if(d_fldCnt == 0){
		strcpy(d_fieldArray[d_fldCnt++], "BID"); 
		strcpy(d_fieldArray[d_fldCnt++], "ASK"); 
		strcpy(d_fieldArray[d_fldCnt++], "LAST_PRICE"); 
	}
    return 0;
}

/*****************************************************************************
Function    : handleDataEvent
Description : This function handlessubscription data and subscription status 
              event. This function reads update data messages in the event
			  element and prints them on the console.
Argument    : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleDataEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;
	assert(event);
	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);
	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = 0;
		assert(message);
		
		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		printf("correlationId=%d %d %d\n",
				correlationId.valueType,
				correlationId.classId,
				correlationId.value.intValue);
		(d_secInfoWindow.fnPtr)(blpapi_Message_elements(message), correlationId.value.intValue);


		messageElements = blpapi_Message_elements(message);
		
		// Get the message element and print it on console.
		//blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
	}
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than response and partial 
			  response event. This function gets the messages from the event 
			  and print them on the console. If the event is session terminate, 
			  then release allocated resources and exit the program. 
Arguments   : Pointer to blpapi_Event_t
			  Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
							 blpapi_Session_t *session)
{
	blpapi_MessageIterator_t *iter = NULL;
	blpapi_Message_t *message = NULL;

	assert(event);
	assert(session);

	// Event has one or more messages. Create message iterator for event
	iter = blpapi_MessageIterator_create(event);
	assert(iter);

	// Iterate through messages received
	while (0 == blpapi_MessageIterator_next(iter, &message)) {
		blpapi_CorrelationId_t correlationId;
		blpapi_Element_t *messageElements = NULL;
		assert(message);

		// get Correlation ID from message
		correlationId = blpapi_Message_correlationId(message, 0);
		printf("messageType=%s\n", blpapi_Message_typeString(message));
		
		// Get the message element and print it on console.
		messageElements = blpapi_Message_elements(message);
		assert(messageElements);		
		blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
		printf("\n");
		
		// If session status is sessionTerminated, release allocated resource
		// and exit the program.
		if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
			&& 0 == strcmp("SessionTerminated",
			blpapi_Message_typeString(message))){
			fprintf(stdout, "Terminating: %s\n", 
							blpapi_Message_typeString(message));

            blpapi_MessageIterator_destroy(iter);
			blpapi_Session_destroy(session);
			exit(1);
		}
	} 
	// Destroy the message iterator.
	blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : subscribe
Description : This function creates subscription list and subscribes to 
			  realtime data for securities specified on command line
Arguments   : Pointer to session
Returns     : void
*****************************************************************************/
void subscribe(blpapi_Session_t *session)
{
	blpapi_SubscriptionList_t *subscriptions = NULL;
    char  * security;
	char **fields;
	const char **options = NULL;
	int i = 0;

	assert(session);
	// Set Window name and function to handle the response received
	d_secInfoWindow.d_name = "SecurityInfo";
	d_secInfoWindow.fnPtr = &displaySecurityInfo;

	fields = (char **) malloc(d_fldCnt * sizeof(char *));
	i = 0;
	while  (i < d_fldCnt ) {
		fields[i] = (char *) d_fieldArray[i];
		i++;
	}

	subscriptions = blpapi_SubscriptionList_create();
	assert(subscriptions);
	
	
	i = 0;
	while  (i < d_secCnt ) {
		blpapi_CorrelationId_t subscriptionId;
        security = (char *) d_secArray[i];
        fprintf(stdout,  "Subscribing to %s\n", security );

		// Initialize Correlation object
		memset(&subscriptionId, '\0', sizeof(subscriptionId));
		subscriptionId.size = sizeof(subscriptionId);
		subscriptionId.valueType = BLPAPI_CORRELATION_TYPE_INT;
		subscriptionId.value.intValue = (blpapi_UInt64_t) i;

		blpapi_SubscriptionList_add(subscriptions, 
									security, 
									&subscriptionId, 
									(const char **)fields, 
									options, 
									d_fldCnt, 
									0);
		i++;
    }
    printf( "\n" );

	// Subscribing to realtime data
	blpapi_Session_subscribe(session, subscriptions, 0, 0, 0);

	free(fields);

	// release subscription list
	blpapi_SubscriptionList_destroy(subscriptions);
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate reference data
			  request. It does following:
			  1. Reads command line arguments.
			  2. Establishes a session which facilitates connection to the 
			      bloomberg network
   		      3. Opens a refData service with the session. 
			  4. create and send referenceData request.
			  5. Event Loop and Response Handling.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns -1 if error occurs otherwise returns 0
*****************************************************************************/
int run(int argc, char **argv)
{
	blpapi_SessionOptions_t *sessionOptions = NULL;
	blpapi_Session_t *session = NULL;
	int continueToLoop = 1;

	// read command line parameters
	if (parseCommandLine(argc, argv) == -1) {
		return -1;
	}

	// create sessionOptions instance. We are allocating resources for 
	// sessionOptions, remember to release resources using destroy.
	sessionOptions = blpapi_SessionOptions_create();
	assert(sessionOptions);

	// Set the host and port for the session. 
	blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	// Create the session now. We are allocating the resources for 
	//session, remember to release them.
	session = blpapi_Session_create(sessionOptions, 0, 0, 0);
	assert(session);

	blpapi_SessionOptions_destroy(sessionOptions);
	printf("Connecting to %s:%d\n", d_host, d_port);
	// Start a Session
	if (0 != blpapi_Session_start(session)) {
		fprintf(stderr, "Failed to start session.\n");
		blpapi_Session_destroy(session);
		return -1;
	}
	// Open Market Data Service
	if (0 != blpapi_Session_openService(session, "//blp/mktdata")){
		fprintf(stderr, "Failed to open service //blp/mktdata.\n");
		blpapi_Session_destroy(session);
		return -1;
	}

		// Make subscription to realtime streaming data
	subscribe(session);

	 // wait for events from session.
	while (1) {
		blpapi_Event_t *event = NULL;
		blpapi_Session_nextEvent(session, &event, 0);
		assert(event);
		switch (blpapi_Event_eventType(event)) {
			case BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA:
			case BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS:
				// Process events BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
				// & BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
				handleDataEvent(event, session);
				break;
			default:
				// Process events other than BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
				// or BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
				handleOtherEvent(event, session);
				break;
		}
        // release the event
		blpapi_Event_release(event);
	}
	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);
	return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
	printf("Subscritpion Correlation Example\n");

	run(argc, argv);

	printf("Press ENTER to quit\n");
	getchar();

	return 0;
}