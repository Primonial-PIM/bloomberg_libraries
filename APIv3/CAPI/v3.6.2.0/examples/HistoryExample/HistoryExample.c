/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <blpapi_event.h>
#include <blpapi_element.h>
#include <blpapi_message.h>
#include <blpapi_name.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp(3C) */

#define SEC_MAX			256
#define FIELD_MAX		256

static int streamWriter(const char* data, int length, void *stream);
void run(void);

static char*			d_host;       /* IP Address */ 
static int				d_port;       /* Port Number */
static char 			d_secArray[SEC_MAX][64];
static char 			d_fieldArray[FIELD_MAX][64];
static int				d_secCnt;
static int				d_fldCnt;
char*           		d_startDate = "null";
char*	          		d_endDate = "null";

#define REFDATA_SVC "//blp/refdata"
#define SECURITY_DATA "securityData"
#define SECURITY_ERROR "securityError"
#define SECURITY_NAME "security"
#define DATE "date"
#define FIELD_ID "fieldId"
#define FIELD_DATA "fieldData"
#define FIELD_EXCEPTIONS "fieldExceptions"
#define ERROR_INFO "errorInfo"
#define MESSAGE "message"
#define FIELD_ID "fieldId"

static int streamWriter(const char* data, int length, void *stream)
{
	assert(data);
	assert(stream);
	return (int) fwrite(data, length, 1, (FILE *)stream);
}

void printUsage()
{
	printf("Usage:\n");
	printf("	Retrieve historical data \n");
    printf("		[-s     <security       = IBM US Equity>\n");
    printf("		[-f     <field          = PX_LAST>\n");
    printf("		[-sd    <startDateTime  = 20091026\n");
    printf("		[-ed    <endDateTime    = 20091030\n");
	printf("		[-ip    <ipAddress      = localhost>\n");
	printf("		[-p     <tcpPort        = 8194>\n");
}

int parseCommandLine(int argc, char **argv)
{
	int i = 0;
	d_host = "localhost";
	d_port = 8194;

	if (argc == 2) {
		// print usage if user ask for help using following option
		if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
			!strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
			printUsage();
			return -1;
		}
	} 	

	for (i = 0; i < argc; ++i)
    {
	  if (!strcmp(argv[i],"-s") &&  i+1 < argc && d_secCnt < SEC_MAX ) 
	  {
			strcpy(d_secArray[d_secCnt++], argv[++i]); 
	  }
	  else if (!strcmp(argv[i],"-f") &&  i+1 < argc && d_fldCnt < FIELD_MAX ) 
	  {
			strcpy(d_fieldArray[d_fldCnt++], argv[++i]); 
	  }
      else if (!strcmp(argv[i],"-sd") && i+1 < argc )
      {
        d_startDate = argv[++i];
      }
      else if (!strcmp(argv[i],"-ed") && i+1 < argc)
      {
        d_endDate = argv[++i];
      }
      else if (!strcmp(argv[i],"-ip") && i+1 < argc)
      {
		    d_host = argv[++i];
      }
      else if (!strcmp(argv[i],"-p") &&  i+1 < argc)
      {
		    d_port = atoi(argv[++i]);
      }
    }

	// Default security and field if nothing is specified on command line
	if(d_secCnt == 0){
		strcpy(d_secArray[d_secCnt++], "IBM US Equity"); 
		strcpy(d_secArray[d_secCnt++], "MSFT US Equity"); 
	}
	if(d_fldCnt == 0){
		strcpy(d_fieldArray[d_fldCnt++], "PX_BID"); 
		strcpy(d_fieldArray[d_fldCnt++], "PX_ASK"); 
		strcpy(d_fieldArray[d_fldCnt++], "PX_LAST"); 
	}
    if (d_startDate == "null")
    {
      d_startDate = "20091101";
    }
    if (d_endDate == "null")
    {
      d_endDate = "20091201";
    }
    return 0;
}//end parseCommandLine

/********************************
 * Convert the to UpperCase *
 *********************************/
 void  ConvertToUpperCase(char * str)
 {
    int ch, i;

    for(i=0;i < strlen(str); i++)
    {
       ch = toupper(str[i]);
       str[i] = ch;
    }
 } 


int Process_Exceptions(blpapi_Message_t *message)
{
	int i = 0;
	blpapi_Element_t *exception_Element = NULL;
	blpapi_Element_t *errorInfo_Element = NULL;
	blpapi_Element_t *securityData = NULL;
	blpapi_Element_t *messageElements = NULL;
	blpapi_Element_t *fieldExceptions = NULL;
	blpapi_Element_t *numFieldExceptions = NULL;
	int num_of_Exceptions = NULL;

	messageElements = blpapi_Message_elements(message);
	blpapi_Element_getElement(messageElements, &securityData, SECURITY_DATA, 0);
	if(blpapi_Element_hasElement(securityData, FIELD_EXCEPTIONS, 0))
	{
		blpapi_Element_getElement(securityData, &fieldExceptions, FIELD_EXCEPTIONS, 0);
		num_of_Exceptions = blpapi_Element_numValues(fieldExceptions);

		if(num_of_Exceptions > 0)
		{
			for(i = 0; i < num_of_Exceptions; i++)
			{
				blpapi_Element_t *field_id = NULL;
				blpapi_Element_t *error_info = NULL;
				blpapi_Element_t *error_message = NULL;
				const char *_field_id = NULL;
				const char *_error_info = NULL;
				const char *_error_message = NULL;

				blpapi_Element_getValueAsElement(fieldExceptions, &exception_Element, i);
				assert(exception_Element);
				blpapi_Element_getElement(exception_Element, &errorInfo_Element, ERROR_INFO, 0);
				assert(errorInfo_Element);

				blpapi_Element_getElement(exception_Element, &field_id, FIELD_ID, 0);
				assert(field_id);
				blpapi_Element_getValueAsString(field_id, &_field_id, 0);
				printf("%s  ",_field_id);

				blpapi_Element_getElement(errorInfo_Element, &error_message, MESSAGE, 0);
				assert(error_message);
				blpapi_Element_getValueAsString(error_message, &_error_message, 0);
				printf("%s  \n", _error_message);
			}
			return 0;
		}
		return -1;
	}
}

int Process_Errors(blpapi_Message_t *message)
{
	blpapi_Element_t *securityData = NULL;
	blpapi_Element_t *securityError = NULL;
	blpapi_Element_t *messageElements = NULL;
	blpapi_Element_t *errorMessage = NULL;

	const char *_error_message = NULL;

	messageElements = blpapi_Message_elements(message);
	blpapi_Element_getElement(messageElements, &securityData, SECURITY_DATA, 0);

	if(blpapi_Element_hasElement(securityData, SECURITY_ERROR, 0))
	{
		blpapi_Element_getElement(securityData, &securityError, SECURITY_ERROR, 0);
		blpapi_Element_getElement(securityError, &errorMessage, MESSAGE, 0);
		assert(errorMessage);
		blpapi_Element_getValueAsString(errorMessage, &_error_message, 0);
		printf("%s  \n", _error_message);
		return 0;
	}
	return -1;
}

void Process_Fields(blpapi_Message_t *message)
{
	  blpapi_Element_t *messageElements = NULL;
	  blpapi_Element_t *securityData = NULL;
	  blpapi_Element_t *_field1 = NULL;
	  blpapi_Element_t *_datatype = NULL;
	  blpapi_Element_t *_date = NULL;
	  blpapi_Element_t *fieldData = NULL;
	  blpapi_Element_t *fieldData2 = NULL;
	  int numValues = NULL;
	  const char *date;
      char  * field;
	  int i = 0;
	  int j = 0;
      int datatype;

	  const char *delimiter = "\t\t";

	  // print out date column heading
	  printf("DATE");
	  printf(delimiter);
  	  i = 0;
	  // Set fields specified on command line
	  while  (i < d_fldCnt ) {
		field = (char *) d_fieldArray[i];
		printf("%s", field);
		printf(delimiter);
		i++;
	  }
	  printf("\n");

	  messageElements = blpapi_Message_elements(message);

	  blpapi_Element_getElement(messageElements, &securityData, "securityData", 0);
	  blpapi_Element_getElement(securityData, &fieldData, "fieldData", 0);

	  // need to get the individual field data elements out first

	  numValues = blpapi_Element_numValues(fieldData);
	  if(numValues > 0)
	  {
		  for(j = 0; j < numValues; j++)
		  {

			  blpapi_Element_getValueAsElement(fieldData, &fieldData2, j);
			  blpapi_Element_getElement(fieldData2, &_date, "date", 0);
			  blpapi_Element_getValueAsString(_date, &date, 0);
			  printf("%s ", date);
			  printf("\t");

			  i = 0;
			  // Set fields specified on command line
			  while  (i < d_fldCnt ) {
				field = (char *) d_fieldArray[i];
				ConvertToUpperCase(field);
			    if(blpapi_Element_hasElement(fieldData2, field, 0))
			    {
			  	  blpapi_Element_getElement(fieldData2, &_field1, field, 0);
				  datatype = blpapi_Element_datatype(_field1);
				  switch(datatype)
				  {
				  case BLPAPI_DATATYPE_BOOL://Bool
					  {
						 blpapi_Bool_t field1;
						 blpapi_Element_getValueAsBool(_field1, &field1, 0);
						 printf("%i	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_CHAR://Char
					  {
						 char field1;
						 blpapi_Element_getValueAsChar(_field1, &field1, 0);
						 printf("%s	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_INT32://Int32
					  {
						 blpapi_Int32_t field1;
						 blpapi_Element_getValueAsInt32(_field1, &field1, 0);
						 printf("%i	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_INT64://Int64
					  {
						 blpapi_Int64_t field1;
						 blpapi_Element_getValueAsInt64(_field1, &field1, 0);
						 printf("%i	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_FLOAT32://Float32
					  {
						 blpapi_Float32_t field1;
						 blpapi_Element_getValueAsFloat32(_field1, &field1, 0);
						 printf("%i	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_FLOAT64://Float64
					  {
						 blpapi_Float64_t field1;
						 blpapi_Element_getValueAsFloat64(_field1, &field1, 0);
						 printf("%f	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_STRING://String
					  {
						 const char *field1;
						 blpapi_Element_getValueAsString(_field1, &field1, 0);
						 printf("%s	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_DATE://Date
					  {
						 blpapi_Datetime_t field1;
						 blpapi_Element_getValueAsDatetime(_field1, &field1, 0);
						 printf("%s	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_TIME://Time
					  {
						 blpapi_Datetime_t field1;
						 blpapi_Element_getValueAsDatetime(_field1, &field1, 0);
						 printf("%s	", field1);
						 break;
					  }
				  case BLPAPI_DATATYPE_DATETIME://Datetime
					  {
						 blpapi_Datetime_t field1;
						 blpapi_Element_getValueAsDatetime(_field1, &field1, 0);
						 printf("%s	", field1);
						 break;
					  }
				  default:
					  {
						  const char *field1;
						  blpapi_Element_getValueAsString(_field1, &field1, 0);
						  printf("%s", field1);
						  break;
					  }
				  }
			    }
				i++;
			  }
			  printf("\n");
		  }
		  printf("\n\n");
	  }

}
void run(int argc, char **argv)
{
    char  * security;
    char  * field;
	int i = 0;
	int continueToLoop;
	blpapi_SessionOptions_t *sessionOptions;
	blpapi_Session_t *session;
	blpapi_Service_t *refDataService;
	blpapi_Request_t *request;
	blpapi_Element_t *elements;
	blpapi_Element_t *securitiesElements;
	blpapi_Element_t *fieldsElements;
	blpapi_CorrelationId_t correlationId;
    blpapi_MessageIterator_t *msgIter;
    blpapi_Message_t *message;
    blpapi_Element_t *messageElements;

	if (parseCommandLine(argc, argv) == -1)
    {
      return;
    }

	sessionOptions = blpapi_SessionOptions_create();

	blpapi_SessionOptions_setServerHost(sessionOptions, "localhost");
	blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

	printf("Connecting to %s:%d\n",
	blpapi_SessionOptions_serverHost(sessionOptions),
	blpapi_SessionOptions_serverPort(sessionOptions));

	session = blpapi_Session_create(sessionOptions, 0, 0, 0);

	if (blpapi_Session_start(session) != 0)
	{
	  fprintf(stderr, "Failed to start session.\n");
	  blpapi_Session_destroy(session);
	  return;
	}

	if (blpapi_Session_openService(session, REFDATA_SVC) != 0)
	{
	  fprintf(stderr, "Failed to open service %s.\n", REFDATA_SVC);
	  blpapi_Session_destroy(session);
	  return;
	}

	refDataService = NULL;
	blpapi_Session_getService(session, &refDataService, REFDATA_SVC);

	request = NULL;
	blpapi_Service_createRequest(refDataService, &request, "HistoricalDataRequest");

	elements = NULL;
	elements = blpapi_Request_elements(request);
	// Get securities element
	blpapi_Element_getElement(elements,	&securitiesElements, "securities", 0);
	assert(securitiesElements);

	i = 0;
	// Set securities specified on command line
	while  (i < d_secCnt ) {
        security = (char *) d_secArray[i];
		blpapi_Element_setValueString(securitiesElements, security, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }
	// Get Field element
	blpapi_Element_getElement(elements, &fieldsElements, "fields", 0);

	i = 0;
	// Set fields specified on command line
	while  (i < d_fldCnt ) {
        field = (char *) d_fieldArray[i];
		blpapi_Element_setValueString(fieldsElements, field, BLPAPI_ELEMENT_INDEX_END);
		i++;
    }


	blpapi_Element_setElementString(elements, "periodicitySelection", 0, "DAILY");
	blpapi_Element_setElementString(elements, "startDate", 0, d_startDate);
	blpapi_Element_setElementString(elements, "endDate", 0, d_endDate);

	blpapi_Element_print(elements, &streamWriter, stdout, 0, 4);
	printf("\n");

	memset(&correlationId, '\0', sizeof(correlationId));
	correlationId.size = sizeof(correlationId);
	correlationId.valueType = BLPAPI_CORRELATION_TYPE_INT;
	correlationId.value.intValue = 0;

	blpapi_Session_sendRequest(session, request, &correlationId, 0, 0, 0, 0);

	blpapi_Request_destroy(request);

	continueToLoop = 1;
	while (continueToLoop)
	{
	  blpapi_Event_t *event = NULL;
	  blpapi_Session_nextEvent(session, &event, 0);

	  if ( (blpapi_Event_eventType(event) != BLPAPI_EVENTTYPE_RESPONSE) &&
       (blpapi_Event_eventType(event) != BLPAPI_EVENTTYPE_PARTIAL_RESPONSE) )
	  {
		blpapi_Event_release(event);
		continue;
	  }
	  else
	  {
  		  if ( (blpapi_Event_eventType(event) == BLPAPI_EVENTTYPE_RESPONSE))
		  {
			  continueToLoop = 0;
		  }

		  msgIter = blpapi_MessageIterator_create(event);
		  message = NULL;
		  messageElements = NULL;

		  while (blpapi_MessageIterator_next(msgIter, &message) == 0)
		  {
			// Get the message element and print it on console.
			messageElements = blpapi_Message_elements(message);
			if(blpapi_Element_hasElement(messageElements, "securityData", 0))
			{
			  // check the message type
			  // if it's a history response, extract the appropriate data
			  blpapi_Element_t *securityName = NULL;
			  blpapi_Element_t *securityData = NULL;
			  const char *sec_name;

			  blpapi_Element_getElement(messageElements, &securityData, "securityData", 0);
			  blpapi_Element_getElement(securityData, &securityName, "security", 0);
			  blpapi_Element_getValueAsString(securityName, &sec_name, 0);
			  printf("%s\n\n", sec_name);

			  if(Process_Exceptions(message) == -1)
			  {
				if(Process_Errors(message) == -1)
				{
					Process_Fields(message);
				}
			  }
			}
		  }
		  blpapi_Event_release(event);
	  }
	}// end of while 0
	// Destory/release allocated resources.
	blpapi_Session_stop(session);
	blpapi_Session_destroy(session);
}

int main(int argc, char **argv)
{
  run(argc, argv);

  printf("Press ENTER to quit\n");
  getchar();

  return 0;
}

