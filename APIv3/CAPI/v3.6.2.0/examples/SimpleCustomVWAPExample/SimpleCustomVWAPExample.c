/* Copyright 2012. Bloomberg Finance L.P.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:  The above copyright notice and this
 * permission notice shall be included in all copies or substantial portions of
 * the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*****************************************************************************
 SimpleCustomVWAPExample.c: 
    This program demonstrate how to make subscription to particular 
    security/ticker to get realtime VWAP updates using Polling method. 
    It uses Market Data service(//blp/mktvwap) provided by API.
    It does following:
        1. Establishing a session which facilitates connection to the bloomberg 
           network.
        2. Initiating the Market Data Data Service(//blp/mktvwap) for realtime
           data.
        3. Creating and sending request to the session.
            - Creating subscription list
            - Add securities and fields to subscription list
            - Specifies VWAP Overrides option
            - Subscribe to realtime data
        4. Event Handling of the responses received.
 Usage: 
    SimpleCustomVWAPExample -help 
    SimpleCustomVWAPExample -?
       Print the usage for the program on the console

    SimpleCustomVWAPExample
       Run the program with default values. Prints the realtime VWAP updates 
       on the console for three default securities specfied
       1. Ticker - "IBM US Equity"
       2. Ticker - "6758 JT Equity" 
       3. Ticker - "VOD LN Equity"

    example usage:
    SimpleCustomVWAPExample
    SimpleCustomVWAPExample -ip localhost -p 8194	

    SimpleCustomVWAPExample -o VWAP_START_TIME=11:00 -o VWAP_END_TIME=15:00

    - Subscribing to Bloomberg defined VWAP and VWAP Volume
    SimpleCustomVWAPExample -s "AAPL US Equity" -f VWAP -f RT_VWAP_VOLUME 
                    -o VWAP_START_TIME=11:00
    
    - Subscribing to Market defined VWAP and VWAP Volume
    SimpleCustomVWAPExample -s "AAPL US Equity" -f MARKET_DEFINED_VWAP_REALTIME 
                    -f RT_MKT_VWAP_VOLUME -o VWAP_START_TIME=11:00
    
    - Subscribing to both Bloomberg defined & Market defined VWAP and VWAP Volume
    SimpleCustomVWAPExample -s "AAPL US Equity" -f VWAP -f RT_VWAP_VOLUME 
                    -f MARKET_DEFINED_VWAP_REALTIME -f RT_MKT_VWAP_VOLUME

    Prints the response on the console of the command line requested data

******************************************************************************/
#include <blpapi_correlationid.h>
#include <blpapi_element.h>
#include <blpapi_event.h>
#include <blpapi_message.h>
#include <blpapi_request.h>
#include <blpapi_session.h>
#include <blpapi_subscriptionlist.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h> /* for exit(2) */
#include <string.h> /* for strcmp(3C) and memset(3C) */

#define SEC_MAX			256
#define FIELD_MAX		256
#define OVERRIDE_MAX     8

static char*         d_host;
static int           d_port;
static char 		 d_secArray[SEC_MAX][64];
static char 		 d_fieldArray[FIELD_MAX][64];
static char 		 d_overrideArray[OVERRIDE_MAX][64];
static int           d_secCnt;
static int           d_fldCnt;
static int           d_overrideCnt;

/*****************************************************************************
Function    : streamWriter
Description : Write specified data to specified stream
Argument    : data - data to be written to stream
              length - length of data
              stream - stream to write data
Returns     : return the number of elements successfully written
*****************************************************************************/
static int streamWriter(const char* data, int length, void *stream)
{
    assert(data);
    assert(stream);
    return fwrite(data, length, 1, (FILE *)stream);
}

/*****************************************************************************
Function    : printUsage
Description : This function prints the usage of the program on command line.
Argument    : void
Returns     : void
*****************************************************************************/
void printUsage()
{
    printf("Usage:\n");
    printf("    Retrieve customized realtime vwap data using Bloomberg V3 API\n");
    printf("        [-s         <security   = \"IBM US Equity\">\n");
    printf("        [-f         <field      = VWAP>\n");
    printf("        [-o         <overrides  = VWAP_START_TIME=09:00>\n");
    printf("        [-ip        <ipAddress  = localhost>\n");
    printf("        [-p         <tcpPort    = 8194>\n");
    printf("Notes:\n");
    printf("Multiple securities, vwap fields & overrides can be specified.\n");
}

/*****************************************************************************
Function    : parseCommandLine
Description : This function parses the command line arguments.If the command
              line argument are not provided properly, it calls printUsage to 
              print the usage on commandline. If no commnd line arguments are 
              specified this fuction will set default values for 
              security/fields/overrides/host/port
Argument	: Command line parameters
Returns		: int: 
              0, if successfully set the input argument for the request 
              from command line or using default values otherwise -1
*****************************************************************************/
int parseCommandLine(int argc, char **argv)
{
    int i = 0;
    // Default value for host and port if not specified on command line
    d_host = "localhost";
    d_port = 8194;

    if (argc == 2) {
        // print usage if user ask for help using following option
        if (!strcmp(argv[1], "-?") || !strcmp(argv[1], "/?") || 
            !strcmp(argv[1],"-help") || !strcmp(argv[1],"-h")) {
            printUsage();
            return -1;
        }
    } 	

    for (i = 1; i < argc; ++i) {
        if (!strcmp(argv[i],"-ip") && i+1 < argc) 
            d_host = argv[++i];
        else if (!strcmp(argv[i],"-p") &&  i+1 < argc) 
            d_port = atoi(argv[++i]);
		else if (!strcmp(argv[i],"-s") &&  i+1 < argc && d_secCnt < SEC_MAX ) 
			strcpy(d_secArray[d_secCnt++], argv[++i]); 
		else if (!strcmp(argv[i],"-f") &&  i+1 < argc && d_fldCnt < FIELD_MAX ) 
			strcpy(d_fieldArray[d_fldCnt++], argv[++i]); 
		else if (!strcmp(argv[i],"-o") &&  i+1 < argc && d_overrideCnt < OVERRIDE_MAX ) 
			strcpy(d_overrideArray[d_overrideCnt++], argv[++i]); 
        else { 
            printUsage();
            return -1;
        }
    }

    // Default security and field if nothing is specified on command line
	if(d_secCnt == 0){
		strcpy(d_secArray[d_secCnt++], "IBM US Equity"); 
		strcpy(d_secArray[d_secCnt++], "VOD LN Equity"); 
		strcpy(d_secArray[d_secCnt++], "6758 JT Equity"); 
    }
	if(d_fldCnt == 0){
        // Subscribing to Bloomberg defined VWAP and VWAP Volume
		strcpy(d_fieldArray[d_fldCnt++], "VWAP"); 
		strcpy(d_fieldArray[d_fldCnt++], "RT_VWAP_VOLUME"); 

		// Subscribing to Market defined VWAP and VWAP Volume
		strcpy(d_fieldArray[d_fldCnt++], "MARKET_DEFINED_VWAP_REALTIME"); 
		strcpy(d_fieldArray[d_fldCnt++], "RT_MKT_VWAP_VOLUME"); 
    }
	if(d_overrideCnt == 0){
		strcpy(d_overrideArray[d_overrideCnt++], "VWAP_START_TIME=09:00"); 
	}
    return 0;
}

/*****************************************************************************
Function    : getTimeStamp
Description : Sets current local time to string buffer
Argument    : Pointer to string buffer
			  bufSize - size of string buffer
Returns     : size_t - no.of characters placed in the buffer
*****************************************************************************/
size_t getTimeStamp(char *buffer, size_t bufSize)
{
    const char *format = "%Y-%m-%dT%X";

    time_t now = time(0);
#ifdef WIN32
	struct tm _timeInfo, *timeInfo;
	localtime_s(&_timeInfo, &now);
	timeInfo = &_timeInfo;

    //tm *timeInfo = localtime_s(&now);
#else
    struct tm _timeInfo;
	struct tm *timeInfo = localtime_r(&now, &_timeInfo);
#endif
    return strftime(buffer, bufSize, format, timeInfo);
}


/*****************************************************************************
Function    : handleSubscriptionEvent
Description : This function handles subscription data and subscription status 
              event. This function reads update data messages in the event
              element and prints them on the console.
Argument    : Pointer to blpapi_Event_t
              Pointer to session
Returns     : void
*****************************************************************************/
static void handleSubscriptionEvent(const blpapi_Event_t *event,
                             blpapi_Session_t *session)
{
    blpapi_MessageIterator_t *iter = NULL;
    blpapi_Message_t *message = NULL;
    char timeBuffer[64];
    const char *topic;
    assert(event);

    getTimeStamp(timeBuffer, sizeof(timeBuffer));

    // Event has one or more messages. Create message iterator for event
    iter = blpapi_MessageIterator_create(event);
    assert(iter);
    // Iterate through messages received
    while (0 == blpapi_MessageIterator_next(iter, &message)) {
        blpapi_CorrelationId_t correlationId;
        blpapi_Element_t *messageElements = NULL;
        assert(message);
        // get Correlation ID from message
        correlationId = blpapi_Message_correlationId(message, 0);
        if(correlationId.valueType == BLPAPI_CORRELATION_TYPE_POINTER &&
            correlationId.value.ptrValue.pointer != NULL){
            topic = (char *) correlationId.value.ptrValue.pointer;
            printf("%s: %s - ", timeBuffer, topic);
        }

        messageElements = blpapi_Message_elements(message);
        
        // Get the message element and print it on console.
        blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
    }
    blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : handleOtherEvent
Description : This function handles events other than subscription data and 
              subscription status event. This function gets the messages from
              the event and print them on the console. If the event is session 
              terminate, then release allocated resources and exit the program.
Arguments   : Pointer to blpapi_Event_t
              Pointer to session
Returns     : void
*****************************************************************************/
static void handleOtherEvent(const blpapi_Event_t *event,
                             blpapi_Session_t *session)
{
    blpapi_MessageIterator_t *iter = NULL;
    blpapi_Message_t *message = NULL;
    assert(event);
    // Event has one or more messages. Create message iterator for event
    iter = blpapi_MessageIterator_create(event);
    assert(iter);
    // Iterate through messages received
    while (0 == blpapi_MessageIterator_next(iter, &message)) {
        blpapi_Element_t *messageElements = NULL;
        assert(message);

        printf("messageType=%s\n", blpapi_Message_typeString(message));
        // Get the message element and print it on console.
        messageElements = blpapi_Message_elements(message);
        assert(messageElements);		
        blpapi_Element_print(messageElements, &streamWriter, stdout, 0, 4);
        
        // If session status is session terminated, release allocated resource
        // and exit the program.
        if (BLPAPI_EVENTTYPE_SESSION_STATUS == blpapi_Event_eventType(event)
            && 0 == strcmp("SessionTerminated",
            blpapi_Message_typeString(message))){
            fprintf(stdout,	"Terminating: %s\n",
                    blpapi_Message_typeString(message));
            blpapi_MessageIterator_destroy(iter);
            exit(1);
        }
    }
    blpapi_MessageIterator_destroy(iter);
}

/*****************************************************************************
Function    : subscribe
Description : This function creates subscription list and subscribes to 
              realtime streaming VWAP updates for IBM US Equity and 
              6758 JT Equity & VOD LN Equity
Arguments   : Pointer to session
Returns     : void
*****************************************************************************/
void subscribe(blpapi_Session_t *session)
{
    blpapi_SubscriptionList_t *subscriptions = NULL;
    char  * security = NULL;
    char  * field = NULL;
    char **fields = NULL;
    char **options = NULL;
    int i = 0;

	fields = (char **) malloc(d_fldCnt * sizeof(char *));
	i = 0;
	while  (i < d_fldCnt ) {
		fields[i] = (char *) d_fieldArray[i];
		i++;
	}

	options = (char **) malloc(d_overrideCnt * sizeof(char *));
	i = 0;
	while  (i < d_overrideCnt ) {
		options[i] = (char *) d_overrideArray[i];
		i++;
	}

	subscriptions = blpapi_SubscriptionList_create();
    assert(subscriptions);
    
	i = 0;
	while  (i < d_secCnt ) {
		blpapi_CorrelationId_t subscriptionId;
        security = (char *) d_secArray[i];
        fprintf(stdout,  "Subscribing to %s\n", security );

        // Initialize Correlation object
        memset(&subscriptionId, '\0', sizeof(subscriptionId));
        subscriptionId.size = sizeof(subscriptionId);
        subscriptionId.valueType = BLPAPI_CORRELATION_TYPE_POINTER;
        subscriptionId.value.ptrValue.pointer = security;

        blpapi_SubscriptionList_add(subscriptions, 
                                    security, 
                                    &subscriptionId, 
                                    (const char **)fields, 
                                    (const char **)options, 
									d_fldCnt, 
									d_overrideCnt);	
		i++;
    }
    printf( "\n" );
    // Subscribing to realtime data
    blpapi_Session_subscribe(session, subscriptions, 0, 0, 0);

    free(fields);
    free(options);
    // release subscription list
    blpapi_SubscriptionList_destroy(subscriptions);
}

/*****************************************************************************
Function    : run                                                                                     
Description : This function runs the application to demonstrate how to make 
              subscription to particular security/ticker to get realtime 
              streaming updates. It does following:
              1. Reads command line argumens.
              2. Establishes a session which facilitates connection to the 
                  bloomberg network
              3. Opens a mktvwap service with the session. 
              4. create and send subscription request.
              5. Event Loop and Response Handling.
Arguments   : int argc, char **argv - Command line parameters.
Returns     : integer. returns 1 if error occurs otherwise returns 0.
*****************************************************************************/
int run(int argc, char **argv)
{
    blpapi_SessionOptions_t *sessionOptions = NULL;
    blpapi_Session_t *session = NULL;

    // read command line parameters
    if (parseCommandLine(argc, argv) == -1) {
        return -1;
    }

    // create sessionOptions instance. We are allocating resources for
    // sessionOptions, remember to release resources using destroy.
    sessionOptions = blpapi_SessionOptions_create();
    assert(sessionOptions);

    // Set the host and port for the session. 
    blpapi_SessionOptions_setServerHost(sessionOptions, d_host);
    blpapi_SessionOptions_setServerPort(sessionOptions, d_port);

    // Set default subscription service as //blp/mktvwap instead of 
    // default //blp/mktdata in order to get realtime market vwap data.
    blpapi_SessionOptions_setDefaultSubscriptionService(sessionOptions,
                                                            "//blp/mktvwap");

    // Create the session now. We are allocating the resources for 
    //session, remember to release them.
    session = blpapi_Session_create(sessionOptions, 0, 0, 0);
    assert(session);

    blpapi_SessionOptions_destroy(sessionOptions);

    printf("Connecting to %s:%d\n", d_host, d_port);
    // Start a Session
    if (0 != blpapi_Session_start(session)) {
        fprintf(stderr, "Failed to start session.\n");
        blpapi_Session_destroy(session);
        return -1;
    }
    // Open mktvwap Service
    if (0 != blpapi_Session_openService(session, "//blp/mktvwap")){
        fprintf(stderr, "Failed to open service //blp/mktvwap.\n");
        blpapi_Session_destroy(session);
        return -1;
    }

    // Make subscription to realtime streaming data
    subscribe(session);

     // wait for events from session.
    while (1) {
        blpapi_Event_t *event = NULL;
        blpapi_Session_nextEvent(session, &event, 0);
        assert(event);
        switch (blpapi_Event_eventType(event)) {
            // Process events BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
            // & BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
            case BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA:
                printf("Processing SUBSCRIPTION_DATA\n");
                handleSubscriptionEvent(event, session);
                break;
            case BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS:
                printf("Processing SUBSCRIPTION_STATUS\n");
                handleSubscriptionEvent(event, session);
                break;
            default:
                // Process events other than BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA
                // or BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS.
                handleOtherEvent(event, session);
                break;
        }
        // release the event
        blpapi_Event_release(event);
        printf("\n");
    }
    // Destory/release allocated resources.
    blpapi_Session_destroy(session);
    return 0;
}

/*********************************
Program entry point.
**********************************/
int main(int argc, char **argv)
{
    printf("Custom VWAP Example\n");

    run(argc, argv);

    printf("Press ENTER to quit\n");
    getchar();

    return 0;
}