# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
use Test::Simple tests => 9;
use Bloomberg::API::Exception;
use Bloomberg::API::Error;

my $testException = Bloomberg::API::Exception->new(
                  Bloomberg::API::Exception::DuplicateCorrelationIdException,
                  "This is an exception");
ok(defined $testException);
ok($testException->type eq Bloomberg::API::Exception::DuplicateCorrelationIdException);


my %errors = (
    Bloomberg::API::Error::BLPAPI_ERROR_DUPLICATE_CORRELATIONID => Bloomberg::API::Exception::InvalidArgumentException(),
    Bloomberg::API::Error::BLPAPI_INVALIDSTATE_CLASS => Bloomberg::API::Exception::InvalidStateException(),
    Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS => Bloomberg::API::Exception::InvalidArgumentException(),
    Bloomberg::API::Error::BLPAPI_CNVERROR_CLASS => Bloomberg::API::Exception::InvalidConversionException(),
    Bloomberg::API::Error::BLPAPI_BOUNDSERROR_CLASS => Bloomberg::API::Exception::IndexOutOfRangeException(),
    Bloomberg::API::Error::BLPAPI_FLDNOTFOUND_CLASS => Bloomberg::API::Exception::FieldNotFoundException(),
    Bloomberg::API::Error::BLPAPI_UNSUPPORTED_CLASS => Bloomberg::API::Exception::UnsupportedOperationException()
    );
my $key;
foreach $key (keys (%errors)) {
    eval {
        Bloomberg::API::Exception->throwException($key);
    };
    if($@) {
        ok($@->type() eq $errors{$key});

    }
    else {
        ok(1);
    }
}
