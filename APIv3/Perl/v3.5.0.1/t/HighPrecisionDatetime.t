# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Datetime;
use Bloomberg::API::Exception;
use Test::More;

BEGIN {
    my $numTests = 1;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        $numTests = 92
    }
    plan tests => $numTests;
}
if (Bloomberg::API::Exception->checkVersion(30500)) {

my $baseArgs = {
        year => 2011,
        month => 1,
        day => 29,
        hours => 4,
        minutes => 34,
        seconds => 28,
};
my $baseDatetime = Bloomberg::API::Datetime->new(%$baseArgs);

sub testBaseArgs
{
    my ($datetime, $msg) = @_;
    ok( 2011         == $datetime->year(), $msg."->year");
    ok( 1            == $datetime->month(), $msg."->month");
    ok( 29           == $datetime->day(), $msg."->day");
    ok( 4            == $datetime->hours(), $msg."->hours");
    ok( 34           == $datetime->minutes(), $msg."->minutes");
    ok( 28           == $datetime->seconds(), $msg."->seconds");
}

# test new with hash arg
{
    # setting just millisec
    my $datetime = Bloomberg::API::Datetime->new(
        %$baseArgs,
        milliseconds => 394,
        );
    testBaseArgs($datetime, "setting millisec constructor");
    ok( "2011-01-29T04:34:28.394" eq $datetime."", "millisec constructor->toString");
    ok( 394          == $datetime->milliseconds(), "millisec constructor->millisecond");
    ok( 394000       == $datetime->microseconds(), "millisec constructor->microsecond");
    ok( 394000000    == $datetime->nanoseconds() , "millisec constructor->nanosecond" );
    ok( 394000000000 == $datetime->picoseconds() , "millisec constructor->picosecond" );

    $baseDatetime->setMilliseconds(394);
    testBaseArgs($baseDatetime, "setting millisec setter", "millisec setter->toString");
    ok( "2011-01-29T04:34:28.394" eq $baseDatetime."");
    ok( 394           == $baseDatetime->milliseconds(), "millisec setter->millisecond");
    ok( 394000        == $baseDatetime->microseconds(), "millisec setter->microsecond");
    ok( 394000000     == $baseDatetime->nanoseconds() , "millisec setter->nanosecond" );
    ok( 394000000000  == $baseDatetime->picoseconds() , "millisec setter->picosecond" );
    ok( $baseDatetime == $datetime, "millisec construted=setter");
}

{
    # setting just microseconds
    my $datetime = Bloomberg::API::Datetime->new(
        %$baseArgs,
        microseconds => 23456,
        );
    testBaseArgs($datetime, "setting microsec constructor");
    ok( "2011-01-29T04:34:28.023456" eq $datetime."", "microsec constructor->toString");
    ok( 23           == $datetime->milliseconds(), "microsec constructor->millisecond");
    ok( 23456        == $datetime->microseconds(), "microsec constructor->microsecond");
    ok( 23456000     == $datetime->nanoseconds() , "microsec constructor->nanosecond" );
    ok( 23456000000  == $datetime->picoseconds() , "microsec constructor->picosecond" );

    $baseDatetime->setMicroseconds(23456);
    testBaseArgs($baseDatetime, "setting microsec setter");
    ok( "2011-01-29T04:34:28.023456" eq $baseDatetime."", "microsec setter->toString");
    ok( 23            == $baseDatetime->milliseconds(), "microsec setter->millisecond");
    ok( 23456         == $baseDatetime->microseconds(), "microsec setter->microsecond");
    ok( 23456000      == $baseDatetime->nanoseconds() , "microsec setter->nanosecond" );
    ok( 23456000000   == $baseDatetime->picoseconds() , "microsec setter->picosecond" );
    ok( $baseDatetime == $datetime, "microsec construted=setter");
}

{
    # setting just nanoseconds
    my $datetime = Bloomberg::API::Datetime->new(
        %$baseArgs,
        nanoseconds => 1,
        );
    ok( "2011-01-29T04:34:28.000000001" eq $datetime."", "nanosec constructor->toString");
    testBaseArgs($datetime, "setting nanosec constructor");
    ok( 0            == $datetime->milliseconds(), "nanosec constructor->millisecond");
    ok( 0            == $datetime->microseconds(), "nanosec constructor->microsecond");
    ok( 1            == $datetime->nanoseconds() , "nanosec constructor->nanosecond" );
    ok( 1000         == $datetime->picoseconds() , "nanosec constructor->picosecond" );

    $baseDatetime->setNanoseconds(1);
    ok( "2011-01-29T04:34:28.000000001" eq $baseDatetime."", "nanosec setter->toString");
    testBaseArgs($baseDatetime, "setting nanosecond setter");
    ok( 0            == $baseDatetime->milliseconds(), "nanosec setter->millisecond");
    ok( 0            == $baseDatetime->microseconds(), "nanosec setter->microsecond");
    ok( 1            == $baseDatetime->nanoseconds() , "nanosec setter->nanosecond" );
    ok( 1000         == $baseDatetime->picoseconds() , "nanosec setter->picosecond" );
    ok( $datetime    == $baseDatetime, "nanosec construted=setter");
}
{
    # setting just picoseconds
    my $datetime = Bloomberg::API::Datetime->new(
        %$baseArgs,
        picoseconds => 123456789123,
        );
    ok( "2011-01-29T04:34:28.123456789123" eq $datetime."", "picosec constructor->toString");
    testBaseArgs($datetime, "setting picosec constructor");
    ok( 123          == $datetime->milliseconds(), "picosec constructor->millisecond");
    ok( 123456       == $datetime->microseconds(), "picosec constructor->microsecond");
    ok( 123456789    == $datetime->nanoseconds() , "picosec constructor->nanosecond" );
    ok( 123456789123 == $datetime->picoseconds() , "picosec constructor->picosecond" );

    $baseDatetime->setPicoseconds(123456789123);
    ok( "2011-01-29T04:34:28.123456789123" eq $baseDatetime."", "picosec setter->toString");
    testBaseArgs($baseDatetime, "setting picosecond setter");
    ok( 123          == $baseDatetime->milliseconds(), "picosec setter->millisecond");
    ok( 123456       == $baseDatetime->microseconds(), "picosec setter->microsecond");
    ok( 123456789    == $baseDatetime->nanoseconds() , "picosec setter->nanosecond" );
    ok( 123456789123 == $baseDatetime->picoseconds() , "picosec setter->picosecond" );
    ok( $datetime    == $baseDatetime, "picosecond construted=setter");
}

}
else {
ok (1, "High Precision datetime not supported!!!");
}
