# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Test::Simple tests => 78;                      # last test to print

use Bloomberg::API::Datetime;
use Bloomberg::API::Exception;

my $datetime = Bloomberg::API::Datetime->new();
ok( defined $datetime);
ok( 1== $datetime->year());
ok( 1== $datetime->day());
ok( 1== $datetime->month());
ok( 0== $datetime->hours());
ok( 0== $datetime->minutes());
ok( 0== $datetime->seconds());
ok( 0== $datetime->milliSeconds());
ok( 0== $datetime->offset());
ok( 0== $datetime->parts());
ok( $datetime->isValid());

$datetime->setYear(2011);
ok( 2011 == $datetime->year());
ok( 0x1 == $datetime->parts());
ok( $datetime->isValid());
ok( $datetime->hasParts(0x1));
$datetime->setMonth(1);
ok( 1 == $datetime->month());
ok( 0x3 == $datetime->parts());
ok( $datetime->isValid());
$datetime->setDay(21);
ok( 21 == $datetime->day());
ok( 0x7 == $datetime->parts());
ok( $datetime->isValid());
$datetime->setHours(11);
ok( 11 == $datetime->hours());
ok( 0x17 == $datetime->parts());
ok( $datetime->isValid());
$datetime->setMinutes(29);
ok( 29 == $datetime->minutes());
ok( 0x37 == $datetime->parts());
ok( $datetime->isValid());
$datetime->setSeconds(59);
ok( 59 == $datetime->seconds());
ok( 0x77 == $datetime->parts());
ok( $datetime->isValid());
$datetime->setMilliSeconds(10);
ok( 10 == $datetime->milliSeconds());
ok( 0xF7 == $datetime->parts());
ok( $datetime->isValid());
$datetime->setOffset(20);
ok( 20 == $datetime->offset());
ok( 0xFF == $datetime->parts());
ok( $datetime->isValid());
$datetime->setParts(30);
ok( 30 == $datetime->parts());

$datetime->setParts(255);
#open($OUTP, '>', "dateTimeTest.txt")
#           or die("Cannot open file '$ARGV[1]' for writing\n");

#$datetime->print($OUTP);
#$datetime->print();

my $ldt = Bloomberg::API::Datetime->new();
ok( $ldt != $datetime);
$datetime->setParts(0);

# change of behaviour
if (Bloomberg::API::Exception->checkVersion(30500)) {
    ok( $ldt == $datetime);
}
else {
    ok( $ldt != $datetime);
}
my $rdt = Bloomberg::API::Datetime->new();
ok( $ldt == $rdt);

my $datetime2 = Bloomberg::API::Datetime->new();
$datetime2->setDate(2011, 2, 18);
ok( 2011 == $datetime2->year());
ok( 2 == $datetime2->month());
ok( 18 == $datetime2->day());
ok( Bloomberg::API::DatetimeParts::DATE == $datetime2->parts());
ok( $datetime2->isValid());

my $datetime3 = Bloomberg::API::Datetime->new();
$datetime3->setTime(12, 45, 30);
ok( 12 == $datetime3->hours());
ok( 45 == $datetime3->minutes());
ok( 30 == $datetime3->seconds());
ok( Bloomberg::API::DatetimeParts::TIME == $datetime3->parts());
ok( $datetime3->isValid());

my $datetime4 = Bloomberg::API::Datetime->new();
$datetime4->setTime(17, 46, 28, 11);
ok( 17 == $datetime4->hours());
ok( 46 == $datetime4->minutes());
ok( 28 == $datetime4->seconds());
ok( 11 == $datetime4->milliSeconds());
ok( Bloomberg::API::DatetimeParts::TIMEMILLI == $datetime4->parts());
ok( $datetime4->isValid());

# Tests for isvalid leap year
my $datetime5 = Bloomberg::API::Datetime->new();
$datetime5->setDate(2012, 2, 28);
ok($datetime5->isValid());
$datetime5->setDate(2012, 2, 29);
ok($datetime5->isValid());
$datetime5->setDate(2012, 2, 30);
ok(!$datetime5->isValid());
$datetime5->setDate(2011, 2, 30);
ok(!$datetime5->isValid());
$datetime5->setDate(2011, 2, 29);
ok(!$datetime5->isValid());
$datetime5->setDate(2011, 2, 28);
ok($datetime5->isValid());

# tests for isValid 31 days month
my $datetime6 = Bloomberg::API::Datetime->new();
$datetime6->setDate(2012, 1, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 3, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 5, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 7, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 8, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 10, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 12, 31);
ok($datetime6->isValid());
$datetime6->setDate(2012, 2, 31);
ok(!$datetime6->isValid());
$datetime6->setDate(2012, 4, 31);
ok(!$datetime6->isValid());
$datetime6->setDate(2012, 6, 31);
ok(!$datetime6->isValid());
$datetime6->setDate(2012, 9, 31);
ok(!$datetime6->isValid());
$datetime6->setDate(2012, 11, 31);
ok(!$datetime6->isValid());
$datetime6->setDate(2012, 4, 30);
ok($datetime6->isValid());
$datetime6->setDate(2012, 6, 30);
ok($datetime6->isValid());
$datetime6->setDate(2012, 9, 30);
ok($datetime6->isValid());
$datetime6->setDate(2012, 11, 30);
ok($datetime6->isValid());

