# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Test::Simple tests => 16;
use Bloomberg::API::Name;

my $testName123 = Bloomberg::API::Name->findName("Test Not available");
ok (not defined $testName123);

ok (!Bloomberg::API::Name->hasName("Test Not available"));

my $newName = Bloomberg::API::Name->new("Name");
ok(defined $newName);
ok($newName->string() eq "Name");
my $testName2 = Bloomberg::API::Name->findName("Name");
ok(defined $testName2);
ok (Bloomberg::API::Name->hasName("Name"));

my $value = 100;
my $newName3 = Bloomberg::API::Name->new($value);
ok("100" eq $newName3);
ok($newName3 eq "100");

ok($newName3."" eq "100");

my $newName4 = Bloomberg::API::Name->new("Name");
ok($newName eq $newName4);
ok($newName->equal($newName4));
ok($newName->string() eq "Name");
ok($newName->length() eq length("Name"));

my $cloneName = $newName4->clone();

ok($cloneName->equal($newName4));
$newName4 = undef;

ok($cloneName->string() eq "Name");
ok($cloneName->length() eq length("Name"));

