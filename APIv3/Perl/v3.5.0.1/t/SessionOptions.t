# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
use Test::Simple tests => 19;

use Bloomberg::API::SessionOptions;

my $sessionOption = Bloomberg::API::SessionOptions->new();
ok( defined $sessionOption);
$sessionOption->setServerHost("sundev9");
my $anotherSession = $sessionOption->clone();
ok( $anotherSession->serverHost() eq "sundev9");
my @hosts = (["host1", 1],["host2", 2],["host3", 3]);


$sessionOption->setServerAddresses(@hosts);
my @addr = $sessionOption->serverAddresses();
ok( 1== $sessionOption->serverPort());
ok ("host1" eq $addr[0][0]);
ok (1 == $addr[0][1]);
ok ("host2" eq $addr[1][0]);
ok (2 == $addr[1][1]);
ok ("host3" eq $addr[2][0]);
ok (3 == $addr[2][1]);

$sessionOption->setConnectTimeout(120);
ok ($sessionOption->connectTimeout() == 120);

$sessionOption->setDefaultService("my/test/service");
ok ($sessionOption->defaultService() eq "my/test/service");

$sessionOption->setDefaultSubscriptionService("my/default/sub/service");
ok ($sessionOption->defaultSubscriptionService() eq "my/default/sub/service");

$sessionOption->setDefaultTopicPrefix("topic_prefix");
ok ($sessionOption->defaultTopicPrefix() eq "topic_prefix");

$sessionOption->setAllowMultipleCorrelatorsPerMsg(1);
ok ($sessionOption->allowMultipleCorrelatorsPerMsg() == 1);

$sessionOption->setClientMode(1);
ok ($sessionOption->clientMode() == 1);

$sessionOption->setMaxPendingRequests(200);
ok ($sessionOption->maxPendingRequests() == 200);

$sessionOption->setAutoRestartOnDisconnection(1);
ok ($sessionOption->autoRestartOnDisconnection == 1);

$sessionOption->setAuthenticationOptions("blah");
ok ($sessionOption->authenticationOptions() eq "blah");

$sessionOption->setNumStartAttempts(120);
ok ($sessionOption->numStartAttempts() == 120);
