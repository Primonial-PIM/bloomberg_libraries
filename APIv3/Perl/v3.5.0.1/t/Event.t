# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
use Bloomberg::API::Event;
use Test::Simple tests => 2;

my $eventQueue = Bloomberg::API::EventQueue->new();
$eventQueue->purge();
my $nextEvent;
eval {
$nextEvent = $eventQueue->tryNextEvent(15);
};
ok(not defined $nextEvent);
$nextEvent = $eventQueue->nextEvent(5);
ok($nextEvent->eventType()."" eq Bloomberg::API::EventType::TIMEOUT."");

