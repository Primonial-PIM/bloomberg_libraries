# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
use Test::Simple tests => 2;

use Bloomberg::API::Error;

ok( Bloomberg::API::Error::BLPAPI_RESULTCODE(Bloomberg::API::Error::BLPAPI_INVALIDSTATE_CLASS | 1) eq 1);
ok(Bloomberg::API::Error::BLPAPI_RESULTCLASS(Bloomberg::API::Error::BLPAPI_ERROR_ILLEGAL_ARG)
                                        eq Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS);
