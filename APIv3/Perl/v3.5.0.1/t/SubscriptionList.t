# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
use Test::Simple tests => 17;
use strict;
use warnings;

use Bloomberg::API::SubscriptionList;
use Bloomberg::API::CorrelationId;

my $cid1 = Bloomberg::API::CorrelationId->new(1);
my $cid2 = Bloomberg::API::CorrelationId->new(2);
my $list = Bloomberg::API::SubscriptionList->new([
    {topic => "IBM US Equity",
     fields => "LAST_PRICE",
     correlationId => $cid1},
    {topic => "GOOG US Equity",
     fields => "LAST_PRICE",
     correlationId => $cid2}
    ]);

ok(2 == $list->size());

$list->add([{topic => "ZYZZ TEMP QWERT"}]);

ok(3 == $list->size());

$list->clear();
ok(0 == $list->size());

my @tempList = (
    {topic => "IBM US Equity",
     fields => "LAST_PRICE",
     correlationId => $cid1},
    {topic => "GOOG US Equity",
     correlationId => $cid2}
    );

$list->add(\@tempList);
my $list2 = Bloomberg::API::SubscriptionList->new();
$list2->append($list);
ok(2 == $list->size());
ok(2 == $list2->size());

my ($topic, $cid) = $list->getAt(0);
my $topic_2 = $list->topicStringAt(0);
my $cid_2 = $list->correlationIdAt(0);
ok( $cid1 == $cid);
ok( not $cid2 == $cid);

ok( $cid1 == $cid);
ok( not $cid2 == $cid);

($topic, $cid) = $list->getAt(1);
$topic_2 = $list->topicStringAt(1);
$cid_2 = $list->correlationIdAt(1);

ok( $cid2 == $cid);
ok( not $cid1 == $cid);
ok( $cid2 == $cid_2);
ok( not $cid1 == $cid_2);

($topic, $cid) = $list2->getAt(0);
ok( $cid1 == $cid);
ok( not $cid2 == $cid);

($topic, $cid) = $list2->getAt(1);
ok( $cid2 == $cid);
ok( not $cid1 == $cid);
#ok($cid1 == $tup[1]);

# $tup = $list->getAt(1);
# ok("GOOG US Equity" eq $tup[0]);
# #ok($cid2 == $tup[1]);
