# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
use Test::Simple tests => 28;                      # last test to print

use Bloomberg::API::CorrelationId;

eval {
my $correlationId = Bloomberg::API::CorrelationId->new("123");

ok( defined $correlationId);
ok( $correlationId->classId() == 0);
ok( $correlationId->valueType() == 1);
ok( $correlationId->asInteger() == 123);


my $correlationId2 = Bloomberg::API::CorrelationId->new(123);
ok( $correlationId == $correlationId2);
ok( "[ valueType=INT classId=0 value=123 ]" eq $correlationId2."");
ok(not defined $correlationId2->asObject());


my $correlationId3 = Bloomberg::API::CorrelationId->new();
ok( defined $correlationId3);
ok( $correlationId3->valueType() == Bloomberg::API::CorrelationId::UNSET_VALUE);
ok( "[ valueType=UNSET classId=0 value=Unknown ]" eq $correlationId3."");

ok(not defined $correlationId3->asObject());

my $obj = {key => "value"};
my $cidObj = Bloomberg::API::CorrelationId->new($obj);

ok($cidObj."" =~ /\[ valueType=OBJECT classId=0 value=HASH\(0x/);


my $retObj = $cidObj->asObject();

ok ($obj == $retObj);

ok ($retObj->{key} eq "value");

my $obj2 = {key => "value2"};
my $cidObj2 = Bloomberg::API::CorrelationId->createUsingObject($obj2);
ok($cidObj2."" =~ /\[ valueType=OBJECT classId=0 value=/);

my $retObj2 = $cidObj2->asObject();

ok ($obj2 == $retObj2);

ok ($retObj2->{key} eq "value2");
};
if($@) {
    #print "$@->type()           $@->description()\n";
    print $@;
}
my $obj2 = "TEST";
my $cidObj2 = Bloomberg::API::CorrelationId->createUsingObject($obj2);
ok($cidObj2."" =~ /\[ valueType=OBJECT classId=0 value=/);
my $retObj2 = $cidObj2->asObject();
ok ($retObj2 eq "TEST");


my %obj3 = (key => 1);
my $cidObj3 = Bloomberg::API::CorrelationId->createUsingObject(\%obj3);
ok($cidObj3."" =~ /\[ valueType=OBJECT classId=0 value=/);

$obj3{key} = 1.111;
my $retObj3Ref = $cidObj3->asObject();
my %retObj3 = %$retObj3Ref;
ok (%obj3 eq %retObj3);
ok ($retObj3{key} eq 1.111);

my $correlationId4 = Bloomberg::API::CorrelationId->new(1);
my $correlationId5 = Bloomberg::API::CorrelationId->new(2);
my $correlationId6 = Bloomberg::API::CorrelationId->new(3);
ok ($correlationId4 < $correlationId5);
ok ($correlationId5 < $correlationId6);
ok ($correlationId4 < $correlationId6);
ok (!($correlationId5 < $correlationId4));
ok (!($correlationId6 < $correlationId5));
ok (!($correlationId6 < $correlationId4));
