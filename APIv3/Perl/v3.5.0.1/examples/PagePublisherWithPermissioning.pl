# Copyright 2012. Bloomberg Finance L.P.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:  The above copyright
# notice and this permission notice shall be included in all copies or
# substantial portions of the Software.  THE SOFTWARE IS PROVIDED "AS IS",
# WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use strict;
use warnings;

use Bloomberg::API::CorrelationId;
use Bloomberg::API::Element;
use Bloomberg::API::Event;
use Bloomberg::API::EventFormatter;
use Bloomberg::API::Identity;
use Bloomberg::API::Message;
use Bloomberg::API::Name;
use Bloomberg::API::ProviderSession;
use Bloomberg::API::Service;
use Bloomberg::API::SessionOptions;
use Bloomberg::API::TopicList;

use Getopt::Long;

my $TOKEN_SUCCESS = Bloomberg::API::Name->new("TokenGenerationSuccess");
my $TOKEN_FAILURE = Bloomberg::API::Name->new("TokenGenerationFailure");
my $AUTHORIZATION_SUCCESS = Bloomberg::API::Name->new("AuthorizationSuccess");
my $TOKEN = Bloomberg::API::Name->new("token");
my $RESOLUTION_SUCCESS = Bloomberg::API::Name->new("ResolutionSuccess");
my $SESSION_TERMINATED = Bloomberg::API::Name->new("SessionTerminated");
my $TOPIC_CREATED = Bloomberg::API::Name->new("TopicCreated");
my $TOPIC_RECAP = Bloomberg::API::Name->new("TopicRecap");
my $TOPIC_SUBSCRIBED = Bloomberg::API::Name->new("TopicSubscribed");
my $TOPIC_UNSUBSCRIBED = Bloomberg::API::Name->new("TopicUnsubscribed");
my $TOPIC = Bloomberg::API::Name->new("topic");
my $TOPICS = Bloomberg::API::Name->new("topics");
my $PERMISSION_REQUEST = Bloomberg::API::Name->new("PermissionRequest");

my $AUTH_USER        = "AuthenticationType=OS_LOGON";
my $AUTH_APP_PREFIX  = "AuthenticationMode=APPLICATION_ONLY;".
    "ApplicationAuthenticationType=APPNAME_AND_KEY;".
    "ApplicationName=";
my $AUTH_DIR_PREFIX  = "AuthenticationType=DIRECTORY_SERVICE;".
    "DirSvcPropertyName=";

my $AUTH_OPTION_NONE = "none";
my $AUTH_OPTION_USER = "user";
my $AUTH_OPTION_APP  = "app=";
my $AUTH_OPTION_DIR  = "dir=";

my $AUTH_SERVICE = "//blp/apiauth";

my $g_running  = 1;
my %g_streams  = ();

# print all the messages in an event
sub printEvent
{
    my $event = shift;
    my $messageIter = Bloomberg::API::MessageIterator->new($event);
    while (my $message = $messageIter->next()) {
        $message->print();
    }
}

# process all the events in session's event queue
sub processEvents
{
    my ($session) = @_;
    my $event;
    my $func = $session->{d_eventHandler};
    while ($event = $session->tryNextEvent()) {
        eventHandler($event, $session);
    }
}

my @host;
my $port = "8194";
my $serviceName = "//viper/page";
my $priority = 10;
my $auth = $AUTH_OPTION_USER;
my $groupId;

sub eventHandler
{
    my ($event, $session) = @_;
    my $message;
    if ($event->eventType() == Bloomberg::API::EventType::SESSION_STATUS) {
        printEvent($event);
        my $messageIterator = Bloomberg::API::MessageIterator->new($event);
        while (my $message = $messageIterator->next()) {
            if ($message->messageType() eq $SESSION_TERMINATED) {
                $g_running = 0;
            }
        }
    }
    elsif ($event->eventType() == Bloomberg::API::EventType::TOPIC_STATUS) {
        printEvent($event);
        my $topicList = Bloomberg::API::TopicList->new();
        my $messageIterator = Bloomberg::API::MessageIterator->new($event);
        while (my $message = $messageIterator->next()) {
            if ($message->messageType() eq $TOPIC_SUBSCRIBED) {
                my $topic = $session->getTopic($message);
                my $topicStr = $message->getElementAsString($TOPIC);
                if (not defined $topic) {
                    $topicList->add($message);
                }
                else {
                    if (not defined ($g_streams{$topicStr})) {
                        $g_streams{$topicStr} = $topic;
                    }
                }
            }
            elsif ($message->messageType() eq $TOPIC_UNSUBSCRIBED) {
                my $topicStr = $message->getElementAsString($TOPIC);
                if (defined ($g_streams{$topicStr})) {
                    delete $g_streams{$topicStr};
                }
            }
            elsif ($message->messageType() eq $TOPIC_CREATED) {
                my $topic = $session->getTopic($message);
                my $topicStr = $message->getElementAsString($TOPIC);
                $g_streams{$topicStr} = $topic;
                my @keySet = keys %g_streams;
            }
            elsif ($message->messageType() eq $TOPIC_RECAP) {
                my $service = $session->getService($serviceName);
                my $topic = $session->getTopic($message);
                if ($topic->isActive()) {
                    my $publishEvent = $service->createPublishEvent();
                    my $eventFormatter = Bloomberg::API::EventFormatter->new(
                        $publishEvent);
                    $eventFormatter->appendRecapMessage($topic);
                    $eventFormatter->setElement("numRows", 25);
                    $eventFormatter->setElement("numCols", 80);
                    $eventFormatter->pushElement("rowUpdate");
                    for (my $i = 0; $i < 5; ++$i) {
                        $eventFormatter->appendElement();
                        $eventFormatter->setElement("rowNum", $i);
                        $eventFormatter->pushElement("spanUpdate");
                        $eventFormatter->appendElement();
                        $eventFormatter->setElement("startCol", 1);
                        $eventFormatter->setElement("length", 10);
                        $eventFormatter->setElement("text", "RECAP");
                        $eventFormatter->popElement();
                        $eventFormatter->popElement();
                        $eventFormatter->popElement();

                    }
                    $eventFormatter->popElement();
                    printEvent($publishEvent);
                    $session->publish($publishEvent);
                }
            }
        }
        if ($topicList->size()) {
            $session->createTopicsAsync($topicList);
        }
    }
    elsif ($event->eventType() == Bloomberg::API::EventType::RESOLUTION_STATUS) {
        printEvent($event);
    }
    elsif ($event->eventType() == Bloomberg::API::EventType::REQUEST) {
        printEvent($event);
        my $service = $session->getService($serviceName);
        my $messageIterator = Bloomberg::API::MessageIterator->new($event);
        if (my $msg = $messageIterator->next()) {
            if ($msg->messageType() eq $PERMISSION_REQUEST) {
                my ($cid) = $msg->correlationIds();
                my $response =
                    $service->createResponseEvent($cid);
                my $eventFormatter = Bloomberg::API::EventFormatter->new(
                        $response);
                my $permission = 1;
                if ($msg->hasElement("uuid")) {
                    my $uuid = $msg->getElementAsFloat64("uuid");
                    $permission = 0;
                }
                if ($msg->hasElement("applicationId")) {
                    my $applicationId =
                        $msg->getElementAsFloat64("applicationId");
                    $permission = 0;
                }
                $eventFormatter->appendResponse("PermissionResponse");
                $eventFormatter->pushElement("topicPermissions");

                my $topicsElement = $msg->getElementByName($TOPICS);
                for (my $i = 0; $i < $topicsElement->numValues(); ++$i) {
                    $eventFormatter->appendElement();
                    $eventFormatter->setElement("topic",
                            $topicsElement->getValueAsString($i));
                    $eventFormatter->setElement("result", $permission);
                    if ($permission) {
                        $eventFormatter->pushElement("reason");
                        $eventFormatter->setElement("source",
                                "My Publisher Name");
                        $eventFormatter->setElement("category",
                                "NOT_AUTHORIZED");
                        $eventFormatter->setElement("subcategory",
                                "Publisher Controlled");
                        $eventFormatter->setElement("description",
                                "Permission denied by My Publisher Name");
                        $eventFormatter->popElement();
                    }
                    $eventFormatter->popElement();
                }
                $eventFormatter->popElement();
                $session->sendResponse($response);
            }
            else {
                die "Unexpected request";
            }

        }
    }
    else {
        printEvent($event);
    }
}

sub authorize {
    my ($session, $ident) = @_;

# generating token
    my $eventQueue = Bloomberg::API::EventQueue->new();
    $session->generateToken(undef, $eventQueue);
    my $event = $eventQueue->nextEvent();
    my $token;
    if ($event->eventType() eq Bloomberg::API::EventType::TOKEN_STATUS) {
        my $messageItr = Bloomberg::API::MessageIterator->new($event);
        while (my $msg = $messageItr->next()) {
            $msg->print();
            if ($msg->messageType() eq $TOKEN_SUCCESS) {
                $token = $msg->getElementAsString($TOKEN);
            }
            else {
                return 0;
            }
        }
    }
    else {
        return 0;
    }

# sending authorization request
    my $authService = $session->getService($AUTH_SERVICE);
    my $authReq = $authService->createAuthorizationRequest();
    $authReq->setElement($TOKEN, $token);

    $session->sendAuthorizationRequest($authReq, $ident);
    my $done = 0;
    while (!$done) {
        my $event = $session->nextEvent(10000);
        if ($event->eventType() == Bloomberg::API::EventType::RESPONSE ||
            $event->eventType() == Bloomberg::API::EventType::REQUEST_STATUS ||
            $event->eventType() == Bloomberg::API::EventType::PARTIAL_RESPONSE) {
            my $messageIter = Bloomberg::API::MessageIterator->new($event);
            while (my $msg = $messageIter->next()) {
                $msg->print();
                if ($msg->messageType() eq $AUTHORIZATION_SUCCESS) {
                    return 1;  
                }
            }
        }
        return 0;
    }
}

sub usage
{
    print "Usage:\n";
    print "    Publish Page data\n";
    print "        [-ip   <ipAddress  = localhost>\n";
    print "        [-p    <tcpPort    = 8194>\n";
    print "        [-s    <service>]    \tservice name (default: //viper/mktdata)\n";
    print "        [-pri  <priority>]   \tpriority (default: 10)\n";
    print "        [-g    <groupId>]    \tpublisher groupId (defaults to unique value)\n";
    print "        [-auth <option>]     \tauthentication option: user|none|app=<app>|dir=<property>(default: user)\n";
    print "        [-n    <number>]     \tnumber of authorization requests (default: number of hosts)\n";
    exit 1;
}

#
# parsing commandline
#
my $help;
my $numAuth = -1;
usage() if (!GetOptions(
        'ip:s'   => \@host,
        'p:i'    => \$port,
        's:s'    => \$serviceName,
        'pri:i'  => \$priority,
        'g:s'    => \$groupId,
        'auth:s' => \$auth,
        'n:i'    => \$numAuth,
        'h'      => \$help
        ) or defined $help);

if (!scalar(@host)) {
    push(@host, "localhost");
}

my $authOptions = $AUTH_USER;
if ($auth eq $AUTH_OPTION_NONE) {
    $authOptions = "";
}
elsif ($auth eq $AUTH_OPTION_USER) {
    $authOptions = $AUTH_USER;
}
elsif (substr($auth, 0, length($AUTH_OPTION_APP))
        eq $AUTH_OPTION_APP) {
    $authOptions = "";
    $authOptions = $authOptions.$AUTH_APP_PREFIX;
    $authOptions = $authOptions.substr($auth,
            length($AUTH_OPTION_APP));
}
elsif (substr($auth, 0, length($AUTH_OPTION_DIR))
        eq $AUTH_OPTION_DIR) {
    $authOptions = "";
    $authOptions = $authOptions.$AUTH_DIR_PREFIX;
    $authOptions = $authOptions.substr($auth,
            length($AUTH_OPTION_DIR));
}
else {
    usage();
}

if ($numAuth < 0) {
    $numAuth = scalar (@host);
}

eval {
#
# creating SessionOptions
#
    my $sessionOption = Bloomberg::API::SessionOptions->new();
    my @hostTuple;
    foreach my $ip (@host) {
        print "Connecting to $ip:$port\n";
        push @hostTuple, [$ip, $port];
    }
    $sessionOption->setServerAddresses(@hostTuple);
    $sessionOption->setAuthenticationOptions($authOptions);
    $sessionOption->setAutoRestartOnDisconnection(1);
    $sessionOption->setNumStartAttempts(scalar(@host));

#
# starting session
#
    my $providerSession = Bloomberg::API::ProviderSession->new($sessionOption);
    my $rcode = $providerSession->start();
    if (!$rcode) {
        die "Failed to start session\n";
    }

    processEvents($providerSession);

#
# authorization
#
    my $ident = $providerSession->createIdentity();
    if (length($authOptions) != 0) {
        if (!$providerSession->openService($AUTH_SERVICE)) {
            die "Failed to open authorization service";
        }
        processEvents($providerSession);
        my $anyAuthorized = 0;
        for (my $i = 0; $i < $numAuth; $i++) {
            if (authorize($providerSession, $ident)) {
                $anyAuthorized = 1;
            }
        }
        if (!$anyAuthorized) {
            die "No authorization";
        }
    }

#
# registering service
#
    $providerSession->registerService($serviceName,
                                      $ident,
                                       {groupId  => $groupId,
                                        priority  => $priority});


#
# getting the topic and publishing
#
    my $service;
    $service = $providerSession->getService($serviceName);

    my $i =0;
    while ($g_running) {
        my $idle = 0;
        processEvents($providerSession);
        my $event = $service->createPublishEvent();
        my @keySet = keys %g_streams;
        if (0 == (scalar(@keySet))) {
            $idle = 1;
        }
        if ($idle) {
            sleep 1;
            next;
        }
        my $eventFormatter =  Bloomberg::API::EventFormatter->new($event);

        for my $key (keys %g_streams) {
            my $value = $g_streams{$key};
            if ($value->isActive()) {
                $eventFormatter->appendMessage("RowUpdate", $value);
                $eventFormatter->setElement("rowNum", 1);
                $eventFormatter->pushElement("spanUpdate");
                $eventFormatter->appendElement();
                $eventFormatter->setElement("startCol", $i++);
                $eventFormatter->setElement("length", 12);
                $eventFormatter->setElement("text", "text");
                $eventFormatter->popElement();
                $eventFormatter->popElement();
            }
        }
        printEvent($event);
        $providerSession->publish($event);
        sleep 1;
    }

#
# stopping the session
#
    $providerSession->stop();
};
if($@) {
    if($@->isa("Bloomberg::API::Exception")) {
        print $@->type()."       ".$@->description()."\n";
    }
    else {
        print "Exception Caught $@ \n";
    }
}

