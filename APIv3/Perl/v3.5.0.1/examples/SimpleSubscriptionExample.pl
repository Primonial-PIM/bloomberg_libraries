# Copyright 2012. Bloomberg Finance L.P.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:  The above copyright
# notice and this permission notice shall be included in all copies or
# substantial portions of the Software.  THE SOFTWARE IS PROVIDED "AS IS",
# WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use Bloomberg::API::AbstractSession;
use Bloomberg::API::SessionOptions;
use Bloomberg::API::Session;
use Bloomberg::API::Message;
use Bloomberg::API::SubscriptionList;
use Bloomberg::API::CorrelationId;

use Getopt::Long;

use strict;
use warnings;

# print all the messages in an event
sub printEvent
{
    my $event = shift;
    my $messageIter = Bloomberg::API::MessageIterator->new($event);
    while (my $msg = $messageIter->next()) {
        $msg->print();
    }
}

# keep on checking events in session's event queue and handle it if any
# until a maximum number of events received
sub blockingEventHandler
{
    my ($session, $maxEvents) = @_;
    my $count = 0;
    my $event;
    while ($event = $session->nextEvent()) {
        printEvent($event);
        if ($event->eventType() == Bloomberg::API::EventType::SUBSCRIPTION_DATA) {
            if (defined $maxEvents && $count++ >= $maxEvents) {
                last;
            }
        }
    }
}

sub usage
{
    print "Usage:\n";
    print "    Retrieve realtime data\n";
    print "        [-s         <security   = IBM US Equity>\n";
    print "        [-f         <field      = LAST_PRICE>\n";
    print "        [-ip        <ipAddress  = localhost>\n";
    print "        [-p         <tcpPort    = 8194>\n";
    print "        [-me        <MAX_EVENTS = UNBOUNDED>\n";
    exit 1;
}

my $host = "localhost";
my $port = "8194";
my @userSecurities;
my @userFields;
my $maxEvents;

# parsing command line
my $help;
usage() if (!GetOptions(
        'ip:s' => \$host,
        'p:i'  => \$port,
        's:s'  => \@userSecurities,
        'f:s'  => \@userFields,
        'me:i' => \$maxEvents,
        'h'    => \$help
        ) or defined $help );

if (!scalar(@userFields)) {
    push(@userFields, "LAST_PRICE");
}

if (!scalar(@userSecurities)) {
    push(@userSecurities, "IBM US Equity");
}

print "Connecting to host ".$host.":".$port."\n";

eval {
    # creating session option
    my $sessionOption = Bloomberg::API::SessionOptions->new();
    $sessionOption->setServerHost($host);
    $sessionOption->setServerPort($port);

    # creating and starting session
    my $session = Bloomberg::API::Session->new($sessionOption);
    $session->start();

    # creating subscription list
    my $userFieldsString = join(", ", @userFields);
    my $list = Bloomberg::API::SubscriptionList->new();
    foreach my $topic (@userSecurities) {
        $list->add([{topic => $topic, fields => $userFieldsString}]);
    }
    # subscribing
    $session->subscribe($list);
    blockingEventHandler($session, $maxEvents);

    # stopping session
    $session->stop();
};
if($@) {
    if($@->isa("Bloomberg::API::Exception")) {
        print $@->type()."       ".$@->description()."\n";
    }
    else {
        print "Exception Caught $@ \n";
    }
}
