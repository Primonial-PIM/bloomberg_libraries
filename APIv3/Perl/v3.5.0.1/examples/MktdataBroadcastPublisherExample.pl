# Copyright 2012. Bloomberg Finance L.P.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:  The above copyright
# notice and this permission notice shall be included in all copies or
# substantial portions of the Software.  THE SOFTWARE IS PROVIDED "AS IS",
# WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use strict;
use warnings;

use Bloomberg::API::ProviderSession;
use Bloomberg::API::SessionOptions;
use Bloomberg::API::Message;
use Bloomberg::API::CorrelationId;
use Bloomberg::API::Name;
use Bloomberg::API::Service;
use Bloomberg::API::Identity;
use Bloomberg::API::TopicList;
use Bloomberg::API::EventFormatter;

use Getopt::Long;

my $TOKEN_SUCCESS = Bloomberg::API::Name->new("TokenGenerationSuccess");
my $TOKEN_FAILURE = Bloomberg::API::Name->new("TokenGenerationFailure");
my $AUTHORIZATION_SUCCESS = Bloomberg::API::Name->new("AuthorizationSuccess");
my $SESSION_TERMINATED = Bloomberg::API::Name->new("SessionTerminated");
my $SESSION_STARTUP_FAILURE = Bloomberg::API::Name->new("SessionStartupFailure");
my $TOKEN = Bloomberg::API::Name->new("token");

my $AUTH_USER        = "AuthenticationType=OS_LOGON";
my $AUTH_APP_PREFIX  = "AuthenticationMode=APPLICATION_ONLY;".
                       "ApplicationAuthenticationType=APPNAME_AND_KEY;".
                       "ApplicationName=";
my $AUTH_DIR_PREFIX  = "AuthenticationType=DIRECTORY_SERVICE;".
                       "DirSvcPropertyName=";

my $AUTH_OPTION_NONE = "none";
my $AUTH_OPTION_USER = "user";
my $AUTH_OPTION_APP  = "app=";
my $AUTH_OPTION_DIR  = "dir=";

my $AUTH_SERVICE = "//blp/apiauth";

# print all the messages in an event
sub printEvent
{
    my $event = shift;
    my $messageIter = Bloomberg::API::MessageIterator->new($event);
    while (my $msg = $messageIter->next()) {
        $msg->print();
    }
}

# handle the events in session's event queue
sub eventHandler
{
    my ($session) = @_;
    my $event;
    while ($event = $session->tryNextEvent()) {
        printEvent($event);
        if ($event->eventType() == Bloomberg::API::EventType::SESSION_STATUS) {
            my $messageIter = Bloomberg::API::MessageIterator->new($event);
            while (my $msg = $messageIter->next()) {
                if ($msg->messageType() eq $SESSION_TERMINATED ||
                    $msg->messageType() eq $SESSION_STARTUP_FAILURE) {
                        return;
                }
            }
        }
    }
}

sub authorize {
    my ($session, $ident) = @_;

# generating token
    my $eventQueue = Bloomberg::API::EventQueue->new();
    $session->generateToken(undef, $eventQueue);
    my $event = $eventQueue->nextEvent();
    my $token;
    if ($event->eventType() eq Bloomberg::API::EventType::TOKEN_STATUS) {
        my $messageItr = Bloomberg::API::MessageIterator->new($event);
        while (my $msg = $messageItr->next()) {
            $msg->print();
            if ($msg->messageType() eq $TOKEN_SUCCESS) {
                $token = $msg->getElementAsString($TOKEN);
            }
            else {
                return 0;
            }
        }
    }
    else {
        return 0;
    }

# sending authorization request
    my $authService = $session->getService($AUTH_SERVICE);
    my $authReq = $authService->createAuthorizationRequest();
    $authReq->setElement($TOKEN, $token);

    $session->sendAuthorizationRequest($authReq, $ident);
    my $done = 0;
    while (!$done) {
        my $event = $session->nextEvent(10000);
        if ($event->eventType() == Bloomberg::API::EventType::RESPONSE ||
            $event->eventType() == Bloomberg::API::EventType::REQUEST_STATUS ||
            $event->eventType() == Bloomberg::API::EventType::PARTIAL_RESPONSE) {
            my $messageIter = Bloomberg::API::MessageIterator->new($event);
            while (my $msg = $messageIter->next()) {
                $msg->print();
                if ($msg->messageType() eq $AUTHORIZATION_SUCCESS) {
                    return 1;  
                }
            }
        }
        return 0;
    }
}

sub usage
{
    print "Usage:\n";
    print "    Publish Mktdata data\n";
    print "        [-ip   <ipAddress>]  \thost (default: localhost)\n";
    print "        [-p    <tcpPort>]    \tport (default: 8194)\n";
    print "        [-s    <service>]    \tservice name (default: //viper/mktdata)\n";
    print "        [-f    <field>]      \tfields (default: LAST_PRICE)\n";
    print "        [-m    <messageType>]\ttype of published event (default: MarketDataEvents)\n";
    print "        [-t    <topics>]     \ttopics (default: /ticker/IBM Equity>]\n";
    print "        [-g    <groupId>]    \tpublisher groupId (defaults to unique value)\n";
    print "        [-me   <maxEvents>]  \tmaximum number of events (defaults: 10)\n";
    print "        [-auth <option>]     \tauthentication option: user|none|app=<app>|dir=<property>(default: user)\n";
    print "        [-n    <number>]     \tnumber of authorization requests (default: number of hosts)\n";
    exit 1;
}

my @host;
my $port = "8194";
my $serviceName = "//viper/mktdata";
my @fields;
my $messageType = "MarketDataEvents";
my @topics;
my $groupId;
my $auth = $AUTH_OPTION_USER;
my $maxEvents = 10;
my $numAuth = -1;

# parsing command line
my $help;
usage() if (!GetOptions(
        'ip:s'   => \@host,
        'p:i'    => \$port,
        's:s'    => \$serviceName,
        'f:s'    => \@fields,
        'm:s'    => \$messageType,
        't:s'    => \@topics,
        'g:s'    => \$groupId,
        'auth:s' => \$auth,
        'me:i'   => \$maxEvents,
        'n:i'    => \$numAuth,
        'h'      => \$help
        ) or defined $help );

if (!scalar(@host)) {
    push(@host, "localhost");
}
if (!scalar(@topics)) {
    push(@topics, "/ticker/IBM Equity");
}
if (!scalar(@fields)) {
    push(@fields, "LAST_PRICE");
}

my $authOptions = $AUTH_USER;
if ($auth eq $AUTH_OPTION_NONE) {
    $authOptions = "";
}
elsif ($auth eq $AUTH_OPTION_USER) {
    $authOptions = $AUTH_USER;
}
elsif (substr($auth, 0, length($AUTH_OPTION_APP))
        eq $AUTH_OPTION_APP) {
    $authOptions = "";
    $authOptions = $authOptions.$AUTH_APP_PREFIX;
    $authOptions = $authOptions.substr($auth,
            length($AUTH_OPTION_APP));
}
elsif (substr($auth, 0, length($AUTH_OPTION_DIR))
        eq $AUTH_OPTION_DIR) {
    $authOptions = "";
    $authOptions = $authOptions.$AUTH_DIR_PREFIX;
    $authOptions = $authOptions.substr($auth,
            length($AUTH_OPTION_DIR));
}
else {
    usage();
}

if ($numAuth < 0) {
    $numAuth = scalar (@host);
}

eval {
#
# creating session option
#
    my $rcode;
    my $sessionOption = Bloomberg::API::SessionOptions->new();
    my @hostTuple;
    foreach my $ip (@host) {
        print "Connecting to $ip:$port\n";
        push @hostTuple, [$ip, $port];
    }
    $sessionOption->setServerAddresses(@hostTuple);
    $sessionOption->setAuthenticationOptions($authOptions);
    $sessionOption->setAutoRestartOnDisconnection(1);
    $sessionOption->setNumStartAttempts(scalar(@host));

#
# creating and starting session
#
    my $providerSession = Bloomberg::API::ProviderSession->new($sessionOption);
    $providerSession->start();
    eventHandler($providerSession);

#
# authorization
#
    my $ident = $providerSession->createIdentity();
    if (length($authOptions) != 0) {
        if (!$providerSession->openService($AUTH_SERVICE)) {
            die "Failed to open authorization service";
        }
        eventHandler($providerSession);
        my $anyAuthorized = 0;
        for (my $i = 0; $i < $numAuth; $i++) {
            if (authorize($providerSession, $ident)) {
                $anyAuthorized = 1;
            }
        }
        if (!$anyAuthorized) {
            die "No authorization";
        }
    }

#
# registering service
#
    $rcode = $providerSession->registerService($serviceName,
                                               $ident,
                                               { groupId => $groupId });
    eventHandler($providerSession);
    if (!$rcode) {
        die "failed to register service $serviceName";
    }

#
# creating topic list
#
    my $topicList = Bloomberg::API::TopicList->new();
    foreach my $topic (@topics) {
        $topicList->add($serviceName.$topic,
                    Bloomberg::API::CorrelationId->new($topic));
    }

    $providerSession->createTopics(
        $topicList,
        Bloomberg::API::ProviderSession::DONT_REGISTER_SERVICES,
        $ident);

    eventHandler($providerSession);

#
# getting the topic and publishing
#
    my $messageTypeName = Bloomberg::API::Name->new($messageType);
    my $tickcount = 0;
    my $service = $providerSession->getService($serviceName);
    while ($tickcount < $maxEvents) {
        eventHandler($providerSession);
        $tickcount++;

        my $event = $service->createPublishEvent();
        my $eventFormatter = Bloomberg::API::EventFormatter->new($event);

        for (my $index = 0; $index < $topicList->size(); ++$index) {
            my $topicMessage = $topicList->messageAt($index);
            my $topic = $providerSession->getTopic($topicMessage);
            $eventFormatter->appendMessage($messageTypeName, $topic);
            my $i = 0;
            foreach my $currField (@fields) {
                $eventFormatter->setElement(
                        $currField, $index * 100 + $tickcount + $i++);
            }
        }
        printEvent($event);
        $providerSession->publish($event);
        sleep 1;
    }
#
# stopping session
#
    $providerSession->stop();
};
if($@) {
    print "Exception Caught: $@ \n";
    if($@->isa("Bloomberg::API::Exception")) {
        print $@->type()."       ".$@->description()."\n";
    }
}
