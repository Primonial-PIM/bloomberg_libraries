# Copyright 2012. Bloomberg Finance L.P.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:  The above copyright
# notice and this permission notice shall be included in all copies or
# substantial portions of the Software.  THE SOFTWARE IS PROVIDED "AS IS",
# WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use strict;
use warnings;

use Bloomberg::API::AbstractSession;
use Bloomberg::API::Name;
use Bloomberg::API::SessionOptions;
use Bloomberg::API::Session;
use Bloomberg::API::Message;
use Bloomberg::API::SubscriptionList;
use Bloomberg::API::CorrelationId;

use Getopt::Long;

my $SECURITY_DATA           = Bloomberg::API::Name->new("securityData");
my $SECURITY                = Bloomberg::API::Name->new("security");
my $FIELD_DATA              = Bloomberg::API::Name->new("fieldData");
my $RESPONSE_ERROR          = Bloomberg::API::Name->new("responseError");
my $SECURITY_ERROR          = Bloomberg::API::Name->new("securityError");
my $FIELD_EXCEPTIONS        = Bloomberg::API::Name->new("fieldExceptions");
my $FIELD_ID                = Bloomberg::API::Name->new("fieldId");
my $ERROR_INFO              = Bloomberg::API::Name->new("errorInfo");
my $CATEGORY                = Bloomberg::API::Name->new("category");
my $MESSAGE                 = Bloomberg::API::Name->new("message");
my $SESSION_TERMINATED      = Bloomberg::API::Name->new("SessionTerminated");
my $SESSION_STARTUP_FAILURE = Bloomberg::API::Name->new("SessionStartupFailure");

# print all the messages in an event
sub printEvent
{
    my ($event) = @_;
    my $messageIter = Bloomberg::API::MessageIterator->new($event);
    while (my $msg = $messageIter->next()) {
        $msg->print();
    }
}

# handle the events in session's event queue
sub eventHandler
{
    my ($session) = @_;
    my $event;
    while ($event = $session->tryNextEvent()) {
        printEvent($event);
        if ($event->eventType() == Bloomberg::API::EventType::SESSION_STATUS) {
            my $messageIter = Bloomberg::API::MessageIterator->new($event);
            while (my $msg = $messageIter->next()) {
                if ($msg->messageType() eq $SESSION_TERMINATED ||
                        $msg->messageType() eq $SESSION_STARTUP_FAILURE) {
                    return;
                }
            }
        }
    }
}

sub printErrorInfo
{
    my ($leadingStr, $element) = @_;

    print $leadingStr
          .$element->getElementAsString($CATEGORY)
          ." ("
          .$element->getElementAsString($MESSAGE)
 .")\n";
}

sub processResponseEvent
{
    my ($event) = @_;
    my $messageIter = Bloomberg::API::MessageIterator->new($event);
    while (my $msg = $messageIter->next()) {
        if ($msg->hasElement($RESPONSE_ERROR)) {
            print "REQUEST FAILED:\n";
            next;
        }

        my $securities = $msg->getElementByName($SECURITY_DATA);
        my $numSecurities = $securities->numValues();
        print "Processing $numSecurities securities\n";

        for my $i (0..$numSecurities-1) {
            my $security = $securities->getValueAsElement($i);
            print "Ticker is ".$security->getElementAsString($SECURITY)."\n";
            if ($security->hasElement("securityError")) {
                printErrorInfo("SECURITY FAILED: ",
                               $security->getElementByName($SECURITY_ERROR));
                next;
            }
            if ($security->hasElement($FIELD_DATA)) {
                my $fields = $security->getElementByName($FIELD_DATA);
                if($fields->numValues() > 0) {
                    print "FIELD\t\tVALUE\n";
                    print "-----\t\t-----\n";
                    my $numFields = $fields->numElements();
                    for my $j (0..($numFields-1)) {
                        my $field = $fields->getElementAt($j);
                        print $field->name()."\t\t".$field->getValueAsString()
                            ."\n";
                    }
                }
            }

            print "\n";
            my $fieldExceptions = $security->getElementByName(
                $FIELD_EXCEPTIONS);
            my $exceptionsNum = $fieldExceptions->numValues();
            if ( $exceptionsNum > 0) {
                print "FIELD\t\tEXCEPTION\n";
                print "-----\t\t---------\n";
                for my $k (0..$exceptionsNum-1) {
                    my $fieldException =
                        $fieldExceptions->getValueAsElement($k);

                    my $errInfo = $fieldException->getElementByName(
                        $ERROR_INFO);
                    print $fieldException->getElementAsString($FIELD_ID)
                        ."\t\t"
                        .$errInfo->getElementAsString($CATEGORY)
                        ." ( "
                        .$errInfo->getElementAsString($MESSAGE)
                        ." )\n";

                }
            }
        }
    }
}

sub usage
{
    print "Usage:\n";
    print "    Retrieve reference data\n";
    print "        [-s         <security   = IBM US Equity>\n";
    print "        [-f         <field      = PX_LAST>\n";
    print "        [-ip        <ipAddress  = localhost>\n";
    print "        [-p         <tcpPort    = 8194>\n";
    exit 1;
}

my $host = "localhost";
my $port = "8194";
my @userSecurities = ();
my @userFields = ();

# parsing command line
my $help;
usage() if (!GetOptions(
        'ip:s' => \$host,
        'p:i'  => \$port,
        's:s'  => \@userSecurities,
        'f:s'  => \@userFields,
        'h'    => \$help
        ) or defined $help );

if (!scalar(@userFields)) {
    push(@userFields, "LAST_PRICE");
}

if (!scalar(@userSecurities)) {
    push(@userSecurities, "IBM US Equity");
}

print "Connecting to host ".$host.":".$port."\n";

eval {
    # creating SessionOptions
    my $sessionOption = Bloomberg::API::SessionOptions->new();
    $sessionOption->setServerHost($host);
    $sessionOption->setServerPort($port);

    # creating and starting Session
    my $session = Bloomberg::API::Session->new($sessionOption);
    my $rcode = $session->start();
    if (!$rcode) {
        die "Failed to start session\n";
    }

    # opening RefData service
    $rcode = $session->openService("//blp/refdata");
    if (!$rcode){
        die "Failed to open //blp/apiauth";
    }

    # handle the events currently in session's event queue
    eventHandler($session);

    # creating ReferenceDataRequest
    my $service = $session->getService("//blp/refdata");
    my $request = $service->createRequest("ReferenceDataRequest");
    my $element = $request->asElement();
    my $secElem = $element->getElementByName("securities");
    foreach my $security (@userSecurities) {
        $secElem->appendValue($security);
    }
    my $fields = $element->getElementByName("fields");
    foreach my $field (@userFields) {
        $fields->appendValue($field);
    }

    # sending Request
    $session->sendRequest($request);

    # checking for and processing events
    my $done = 0;
    while (!$done) {
        my $event = $session->nextEvent();
        if ($event->eventType() == Bloomberg::API::EventType::PARTIAL_RESPONSE) {
            processResponseEvent($event);
        }
        elsif ($event->eventType() == Bloomberg::API::EventType::RESPONSE) {
            processResponseEvent($event);
            $done=1;
        }
        else {
            my $messageIter = Bloomberg::API::MessageIterator->new($event);
            while (my $msg = $messageIter->next()) {
                if ($event->eventType() ==
                        Bloomberg::API::EventType::SESSION_STATUS) {
                    if ($msg->messageType() eq $SESSION_TERMINATED ||
                        $msg->messageType() eq $SESSION_STARTUP_FAILURE) {
                        $done = 1;
                    }
                }
                $msg->print();
            }
        }
    }

    print "Press any key to continue\n";
    my $input = <stdin>;
};
if($@) {
    if ($@->isa("Bloomberg::API::Exception")) {
        print $@->type()."       ".$@->description()."\n";
    }
    else {
        print "Exception Caught $@ \n";
    }
}

