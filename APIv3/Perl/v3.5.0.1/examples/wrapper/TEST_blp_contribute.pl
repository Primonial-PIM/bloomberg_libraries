use blp::contributor;
use blp::helper;
use strict;

my $app;
my $service;
my $event_type='MarketData';
my @hosts;
my $port=8196;
my @topics;
my @fields;

my $cmdline=[
	[ "app|a=s" => \$app => "EMRS Application Name" ],
	[ "service|s=s" => \$service => "Platform service - //FIRM/TOPIC" ],
	[ "eventType|e=s" => \$event_type => "Event type in schema for service" ],
	[ "hosts|h=s" => \@hosts, "Host to Connect to - may be specified multiple times" ],
	[ "port|p=i" => \$port => "Port to connect to on hosts" ],
	[ "topics|t=s" => \@topics => "Topics to run test on - may be specified multiple times" ],
	[ "fields|f=s" => \@fields => "Fields for Topics - may be specified multiple times" ],
];

blp::helper::parseCommandline($cmdline);
my $hostlist=blp::helper::make_host_list(\@hosts, $port);

my $contributor=new blp::contributor($app, $service);
die "Bad Connection" unless ($contributor->connect($hostlist));
my $ctr=0;

while(1)
{
	my @msgs=();
	for my $i (@topics)
	{
		print "Publishing for $i\n";
		my $data={};
		for my $f (@fields)
		{
			$ctr++;
			my $val=$ctr;
			$val=123 + $ctr%10000 / 999 unless ($f =~ /_SIZE/);
			print "\t$f = $val\n";
			$data->{$f}=$val;
		}
		my $topic=$service . '/ticker/' . $i;
		my $message=[$event_type, $topic, $data];
		push @msgs, $message;
	}
	$contributor->send(\@msgs);
	select(undef,undef,undef,0.5); ## Wait a bit
}

