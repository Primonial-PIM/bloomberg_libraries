use blp::subscribe;
use blp::helper;
use strict;

my $app;
my @hosts;
my $port=8194;
my @topics;
my @fields;
my @options;

my $cmdline=[
	[ "app|a=s" => \$app => "EMRS Application Name" ],
	[ "hosts|h=s" => \@hosts => "Host to Connect to - may be specified multiple times" ],
	[ "port|p=i" => \$port => "Port to connect to on hosts" ],
	[ "topics|t=s" => \@topics => "Topics to run test on - may be specified multiple times" ],
	[ "fields|f=s" => \@fields => "Fields for Topics - may be specified multiple times" ],
	[ "options|o" => \@options => "Subscription options - may be specified multiple times" ],
];

blp::helper::parseCommandline($cmdline);
my $hostlist=blp::helper::make_host_list(\@hosts, $port);

my $sub=new blp::subscribe($app);
$sub->connect($hostlist);
$sub->start(\@topics, \@fields, \@options);
