use blp::subscribe;
use blp::helper;
use strict;

my $app;
my $service;
my @hosts;
my $port=8196;
my @topics;

my $cmdline=[
	[ "app|a=s" => \$app => "EMRS Application Name" ],
	[ "service|s=s" => \$service => "Platform service - //FIRM/TOPIC" ],
	[ "hosts|h=s" => \@hosts, "Host to Connect to - may be specified multiple times" ],
	[ "port|p=i" => \$port => "Port to connect to on hosts" ],
	[ "topics|t=s" => \@topics => "Topics to run test on - may be specified multiple times" ],
];

blp::helper::parseCommandline($cmdline);
my $hostlist=blp::helper::make_host_list(\@hosts, $port);

my $securities=[];
for my $topic (@topics)
{
	my $fulltopic=$topic;
	$fulltopic=$service . '/' . $topic unless ($topic =~ /^\/\//); # must start with //
	push @$securities, $fulltopic;
}

my $sub=new blp::subscribe($app,1);
die "Bad Connection" unless ($sub->connect($hostlist));
$sub->start($securities);
