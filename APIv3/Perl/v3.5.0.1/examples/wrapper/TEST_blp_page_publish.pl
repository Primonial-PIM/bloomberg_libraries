use blp::page_publish;
use  blp::helper;
use strict;


my $app;
my $service;
my $rows=24;
my $cols=80;
my @hosts;
my $port=8196;

my $cmdline=[
	[ "app|a=s" => \$app => "EMRS Application Name" ],
	[ "service|s=s" => \$service => "Platform service - //FIRM/TOPIC" ],
	[ "hosts|h=s" => \@hosts, "Host to Connect to - may be specified multiple times" ],
	[ "port|p=i" => \$port => "Port to connect to on hosts" ],
	[ "rows|r=i" => \$rows => "Maximum number of rows on page" ],
	[ "columns|c=i" => \$cols => "Maximum number of columns on page" ],
];

blp::helper::parseCommandline($cmdline);
my $hostlist=blp::helper::make_host_list(\@hosts, $port);

my $subjects={};

my $publisher=new blp::page_publish($app, $service);
die "Bad Connection" unless ($publisher->connect($hostlist));
$publisher->start($rows, $cols, \&fetch, \&unfetch, \&ck_update);

sub fetch
{
	my $topic=shift;
print "FETCH: $topic\n";
	$subjects->{$topic}=1;

	return 0;
}

sub unfetch
{
	my $topic=shift;
print "UNFETCH: $topic\n";
	delete $subjects->{$topic};

	return 0;
}

sub ck_update
{
	my $txt='Update ';
	my $max_col=1+$cols-length($txt);

	for my $topic (keys %$subjects)
	{
		$publisher->update($topic,
			1+int(rand($rows)),
			1+int(rand($max_col)),
			$blp::page_publish::LIGHTBLUE
			| ($blp::page_publish::BG * $blp::page_publish::WHITE)
			| $blp::page_publish::UNDERLINE,
			$txt);
	}

	## Wait a bit
	select(undef,undef,undef,0.5); ## 500 milliseconds

	return 0;
}
