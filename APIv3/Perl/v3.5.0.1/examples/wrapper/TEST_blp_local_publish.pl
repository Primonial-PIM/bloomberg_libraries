use blp::publish;
use blp::helper;
use strict;

my $app;
my $service;
my $event_type='MarketDataEvents';
my @hosts;
my $port=8196;

my $cmdline=[
	[ "app|a=s" => \$app => "EMRS Application Name" ],
	[ "service|s=s" => \$service => "Platform service - //FIRM/TOPIC" ],
	[ "eventType|e=s" => \$event_type => "Event type in schema for service" ],
	[ "hosts|h=s" => \@hosts, "Host to Connect to - may be specified multiple times" ],
	[ "port|p=i" => \$port => "Port to connect to on hosts" ],
];
	
blp::helper::parseCommandline($cmdline);
my $hostlist=blp::helper::make_host_list(\@hosts, $port);

my $subjects={};

my $publisher=new blp::publish($app, $service);
die "Bad Connection" unless ($publisher->connect($hostlist));

$publisher->start($event_type, \&fetch, \&unfetch, \&ck_update);

sub fetch
{
	my $topic=shift;
print "FETCH: $topic\n";
	$subjects->{$topic}=1;

		my $what={};
		$what->{BID}=1000;
		$what->{LAST_PRICE}=1000;
	$publisher->update($topic, $what);
print "Published BID/LAST_PRICE for $topic\n";
	return 0;
}

sub unfetch
{
	my $topic=shift;
print "UNFETCH: $topic\n";
	delete $subjects->{$topic};

	return 0;
}

sub ck_update
{
	for my $topic (keys %$subjects)
	{
		my $bid=$subjects->{$topic};
		$bid += -1 + rand() * 3;
		$bid = 11 if ($bid < 10);
		$subjects->{$topic}=$bid;
		my $ask=$bid+rand();
		my $what={};
		$what->{BID}=$bid;
		$what->{ASK}=$ask;
		$publisher->update($topic, $what);
	}

	## Wait a bit
	select(undef,undef,undef,0.5); ## 500 milliseconds

	return 0;
}
