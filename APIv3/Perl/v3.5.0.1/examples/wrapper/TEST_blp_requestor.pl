use blp::requestor;
use blp::helper;
use strict;

my $app;
my @hosts;
my $port=8194;
my @topics;
my @fields;
my @options;
my $type=$blp::requestor::REF_REQUEST;
my $native=0;

my $cmdline=[
	[ "app|a=s" => \$app => "EMRS Application Name" ],
	[ "hosts|h=s" => \@hosts => "Host to Connect to - may be specified multiple times" ],
	[ "port|p=i" => \$port => "Port to connect to on hosts" ],
	[ "topics|t=s" => \@topics => "Topics to run test on - may be specified multiple times" ],
	[ "fields|f=s" => \@fields => "Fields for Topics - may be specified multiple times" ],
	[ "options|o=s" => \@options => "Additional request options - may be specified multiple times" ],
	[ "request|r=s" => \$type => "Request Type - Default is " . $type ],
	[ "native!" => \$native => "Show data in its native format as per schema" ],
];

blp::helper::parseCommandline($cmdline);
my $hostlist=blp::helper::make_host_list(\@hosts, $port);

my $requestor=new blp::requestor($app);
$requestor->connect($hostlist);

my $response=$requestor->request(\@topics, \@fields, \@options, $type, $native);

blp::helper::show($response);

unless ($native)
{
	my $sec=$topics[0];
	my $fld=$fields[0];
	if ($type eq $blp::requestor::REF_REQUEST)
	{
		my $test_val=$response->{$sec}->{$fld}; #show first security, first field
		print "\n\n$sec:$fld=$test_val\n";
	}
	elsif ($type eq $blp::requestor::HIST_REQUEST)
	{
		my $vals=$response->{$sec};
		print "\n";
		my $count=0;
		for my $dt (sort keys %$vals)
		{
			next if ($dt eq '_FIELD_ERRORS_');
			++$count;
			last if ($count > 5);
			my $test_val=$vals->{$dt}->{$fld}; #show first date value 
			print "$sec:$fld for $dt is $test_val\n";
		}
	}
	else
	{
		print "\nUnsupported Request: $type\n";
	}
}


