## PM to wait for subscriptions and then publish

use blp::util::corepub;
use strict;

package blp::publish;

sub new
{
	##      implicit arg for perl class
        my $proto = shift;

## new(app_name, service)
        my $app_name=shift;
        my $service=shift;

	my $class = ref($proto) || $proto; ## check for deref

        ## main object repository
	my $self  = {};

	$self->{PUB}=new blp::util::corepub($app_name, $service);

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub connect
{
	my $self=shift;
	my $servers=shift;
	return $self->{PUB}->connect($servers) ? 1 : 0;
}

sub start
{
	my $self=shift;
	my $type=shift;
	my $fetch=shift;
	my $unfetch=shift;
	my $ck_update=shift;
	my $perm_check=shift;

	$self->{PUB}->start($type, $fetch, $unfetch, $ck_update, $perm_check);
}

sub update
{
	my $self=shift;
	my $topic=shift;
	my $what=shift;
	$self->{PUB}->update($topic, $what);
}

1;

__END__

=pod

=head1 NAME

blp::publish - sample wrapper for local publishing

=head1 DESCRIPTION

The publish object can be used to locally publish data to any subscriber:
Excel with the Bloomberg addin, Launchpad Monitor, or an application
It is written as a framework with key callbacks specified by the user to handle the most
common events

This wrapper assumes a flat schema - that is a service with just a list of fields
The framework will keep a cache to service requests for initial images for new subscribers

=head1 EXAMPLE FILE

TEST_blp_local_publish.pl

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item * new($EMRS_Application, $serviceName)

Pass the EMRS application, and the service name

=item * connect($PlatformServers)

Pass an array with element 0 as the IP/Hostname and element 1 as the port for the Bloomberg
Platform.   For multiple hosts, pass an array of these arrays


=item * start($eventType, \&fetch, \&unfetch, \&ck_update, \&perm_check);

Starts the framework, will not return untill a callback returns true

=over 4

=item * $eventType - the string representing the name of the eventType

=item * &fetch - required callback for every new topic publisher is asked to service

fetch will be passed: the topic name - return any true value to stop framework

=item * &unfetch - required callback for every topic the publisher is no longer required to service

unfetch will be passed: the topic name - return any true value to stop framework

=item * &ck_update - required callback called whenever framework is idle.

This is to allow the publisher to update any pages with any data it has recieved
Note that the wrapper provides no throttling so it is important to provide some minimal delay
in the ck_update callback.  The following code will wait for one half a second:

select(undef,undef,undef,0.5);


=item * &perm_check - optional callback used when the schema has topic level permissioning

perm_check will be passed the topic name and a hash of all user identifiers passed by the platform
this will include 'uuid' - return false to grant access or an error string to deny it

=back

=item * update($topic, $FieldValues) - updates topic with all values in FieldValues

Where FieldValues is a hash whose key is the field name and its value is the field value


=back

