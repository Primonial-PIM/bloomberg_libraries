## PM for simple helper functions
use Getopt::Long;
use strict;

package blp::helper;

sub parseCommandline
{
	my $opts=shift;
	my @getopts=();
	my $helptext="Usage $0 --option\n\tWhere option can be:\n";
	my $max_size=22;
	my $fmt='%-*s'; ## left justify and pad to $max_size
	
	for my $item (@$opts)
	{
		my ($arg, $var, $desc) = @$item;
		push @getopts, $arg, $var;
		$helptext.="\t".sprintf($fmt, $max_size, $arg) . $desc . "\n";
	}
	my $help='help|?';
	$helptext.="\t".sprintf($fmt, $max_size, $help) . "Prints this help message\n\n";
	push @getopts, $help, sub {
		print $helptext;
		exit(-1);
	};
	Getopt::Long::GetOptions(@getopts);
}

sub make_host_list
{
	my $hosts=shift;
	my $port=shift;

	return undef unless((defined $hosts) && scalar(@$hosts));

	my $hostlist=[];
	for my $server (@$hosts)
	{
		push @$hostlist, [ $server, $port ];
	}

	return $hostlist;
}

sub indent
{
	my $indent=shift;
	my $spaces=' 'x80;
	print substr($spaces,0,$indent);
}

sub show
{
	my $what=shift;
	my $indent=shift;
	$indent=0 unless $indent;
	my $type=ref($what);
	if (!$type)
	{
		print "=$what\n";
	}
	elsif ($type eq 'ARRAY')
	{
		my $sz=$#$what;
		print '[' . ($sz + 1) . ']' . "=\n";
		for (my $i=0; $i <= $sz; ++$i)
		{
			indent($indent);
			print '[' . $i . ']';
			show($what->[$i], $indent+3);
		}
	}
	elsif ($type eq 'HASH')
	{
		print "=\n";
		for my $k (sort keys %$what)
		{
			indent($indent);
			print $k;
			show($what->{$k}, $indent+2);
		}
	}
	elsif ($type eq 'SCALAR')
	{
		show($$what);
	}
	else
	{
		print "$type - $what\n";
	}
}

sub trim
{
	my $what=shift;

	$what =~ s/^ *//g;
	$what =~ s/ *$//g;

	return $what;
}

1;

__END__

=pod

=head1 NAME

blp::helper - sample wrapper utility functions

=head1 DESCRIPTION

This module contains functions for ease of use in the wrapper

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item * parseCommandline(\%opts) - parges commandline

Expects an array of arrays of [ GetOpts Arg, Variable to set, Description ]
Adds a help function from the description and uses GetOpts to set variables

=item * make_host_list(\@hosts, port) - creates wrapper style host list

=item * indent($spaces) - prints $spaces spaces ' '

=item * show($message) - prints a blp wrapper message

=item * trim($string) - returns $string without leading or training spaces

=back
