## PM to contribute data to Bloomberg

use blp::util::corepub;
use strict;

package blp::contributor;

sub new
{
	##      implicit arg for perl class
        my $proto = shift;

## new(app_name)
        my $app_name=shift;
        my $service=shift;

	my $class = ref($proto) || $proto; ## check for deref

        ## main object repository
	my $self  = {};

	$self->{PUB}=new blp::util::corepub($app_name, $service);

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub connect
{
	my $self=shift;
	my $servers=shift;
	$self->{PUB}->connect($servers);
}

sub send
{
	my $self=shift;
	my $what=shift;

	$self->{PUB}->handle_events();
	$self->{PUB}->publish($what);
	$self->{PUB}->handle_events();
}

1;

__END__

=pod

=head1 NAME

blp::contributor - sample wrapper for contributing data to Bloomberg

=head1 DESCRIPTION

The contributor object can be used to contribute data to Bloomberg

It will establish a connection to Bloomberg Platforms as a specified EMRS application,
on a given service and then formatted messages can be sent to Bloomberg as data becomes
available.

Messages are constructed with normal perl collections, hashes and arrays 
and then an event array of the form [ [ EventTypeLabel, TopicString, hashOfFields ] ...]
The hashOfFields contains names and values - where values can be arrays for schemas that
need event sequences and hashes for submessages, element choices

The wrapper provides no throttling so it is important to provide this.  The following code
will wait for one half a second:

select(undef,undef,undef,0.5);

=head1 EXAMPLE FILE

TEST_blp_contribute.pl

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item * new($EMRS_Application, $serviceName)

Pass the EMRS application, and the service name

=item * connect($PlatformServers)

Pass an array with element 0 as the IP/Hostname and element 1 as the port for the Bloomberg
Platform.   For multiple hosts, pass an array of these arrays

=item * send($Message)

Sends the message structure detailed above to the Bloomberg Platform

=back

