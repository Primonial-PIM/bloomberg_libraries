## PM To handle page publishing

use blp::util::corepub;
use strict;

package blp::page_publish;

our $BLACK=0;
our $AMBER         =1;
our $DARKBLUE      =2;
our $DARKGREEN     =3;
our $DEEPBLUE      =4;
our $FLASHINGBLUE  =5;
our $FLASHINGRED   =6;
our $GRAY          =7;
our $LIGHTBLUE     =8;
our $LIGHTGREEN    =9;
our $ORANGE        =0xA;
our $PINK          =0xB;
our $RED           =0xC;
our $VIOLET        =0xD;
our $WHITE         =0xE;
our $YELLOW        =0xF;
our $FG_MASK       =0xF;
our $BG            =0x10; ## Background color = $BG * Color
our $BG_MASK       =0xF0; ## Background color = $BG * Color
our $BLINK         =0x100;
our $REVERSE       =0x200;
our $UNDERLINE     =0x400;


my %fgcolors=(
$AMBER=>'AMBER',
$BLACK=>'BLACK',
$DARKBLUE=>'DARKBLUE',
$DARKGREEN=>'DARKGREEN',
$DEEPBLUE=>'DEEPBLUE',
$FLASHINGBLUE=>'FLASHINGBLUE',
$FLASHINGRED=>'FLASHINGRED',
$GRAY=>'GRAY',
$LIGHTBLUE=>'LIGHTBLUE',
$LIGHTGREEN=>'LIGHTGREEN',
$ORANGE=>'ORANGE',
$PINK=>'PINK',
$RED=>'RED',
$VIOLET=>'VIOLET',
$WHITE=>'WHITE',
$YELLOW=>'YELLOW',
);

my %bgcolors=();
my %color2fg=();
my %color2bg=();
while (my ($val, $color) = each (%fgcolors))
{
	my $bgval=$BG*$val;
	$bgcolors{$bgval}=$color;
	$color2fg{$color}=$val;
	$color2bg{$color}=$bgval;
}

my %attr=(
$BLINK=>'BLINK',
$REVERSE=>'REVERSE',
$UNDERLINE=>'UNDERLINE',
);

my $ATTR_MASK=$BLINK|$REVERSE|$UNDERLINE;
my $COLOR_MASK=$BLINK-1; ## BLINK MUST BE FIRST

sub new
{
	##      implicit arg for perl class
	my $proto = shift;
## new(app_name, service)
	my $app_name=shift;
	my $service=shift;

	my $class = ref($proto) || $proto; ## check for deref

	## main object repository
	my $self  = {};

	$self->{PUB}=new blp::util::corepub($app_name, $service,
		\%fgcolors, $FG_MASK,
		\%bgcolors, $BG_MASK,
		\%attr);

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub connect
{
	my $self=shift;
	my $servers=shift;
	return $self->{PUB}->connect($servers) ? 1 : 0;
}

sub start
{
	my $self=shift;
	my $rows=shift;
	my $cols=shift;
	my $fetch=shift;
	my $unfetch=shift;
	my $ck_update=shift;
	my $perm_check=shift;

	$self->{PUB}->start_page($rows, $cols, $fetch, $unfetch, $ck_update, $perm_check);
}

sub update
{
	my $self=shift;
	my $topic=shift;
	my $row=shift;
	my $start_col=shift;
	my $attr=shift;
	my $what=shift;
	my $len=shift; ## Optional will be strlen of $what if not set
	$len=length $what unless (defined $len);

	$self->{PUB}->update_page($topic, $row, $start_col, $attr, $what, $len);
}

1;


__END__

=pod

=head1 NAME

blp::page_publish - sample wrapper for local publishing to the Bloomberg Launchpad Page Monitor

=head1 DESCRIPTION

The page_publish object can be used to locally publish data to the Bloomberg Launchpad Page Monitor 
It is written as a framework with key callbacks specified by the user to handle the most
common events
The framework will keep a cache to service requests for initial images for new subscribers

=head1 EXAMPLE FILE

TEST_blp_page_publish.pl

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item * new($EMRS_Application, $serviceName)

Pass the EMRS application, and the service name

=item * connect($PlatformServers)

Pass an array with element 0 as the IP/Hostname and element 1 as the port for the Bloomberg
Platform.   For multiple hosts, pass an array of these arrays


=item * start($rows, $columns, \&fetch, \&unfetch, \&ck_update, \&perm_check);
Starts the framework, will not return untill a callback returns true

=over 4

=item * $rows - the maximum number of rows to be sent

=item * $columns - the maximum number of columns to be sent

=item * &fetch - required callback for every new topic publisher is asked to service

fetch will be passed: the topic name - return any true value to stop framework

=item * &unfetch - required callback for every topic the publisher is no longer required to service

unfetch will be passed: the topic name - return any true value to stop framework

=item * &ck_update - required callback called whenever framework is idle.

This is to allow the publisher to update any pages with any data it has recieved
Note that the wrapper provides no throttling so it is important to provide some minimal delay
in the ck_update callback.  The following code will wait for one half a second:

select(undef,undef,undef,0.5);


=item * &perm_check - optional callback used when the schema has topic level permissioning

perm_check will be passed the topic name and a hash of all user identifiers passed by the platform
this will include 'uuid' - return false to grant access or an error string to deny it

=back

=item * update($topic, $row, $start_column, $attributes, $text, $length)

Updates the page with new data

=over 4

=item * $topic - Topic String of page being published - must begin with service name

=item * $row - Row to publish to

=item * $start_column - column to put first character of text

=item * $attributes - binary value used to represent attributes

this includes foreground and background colors as well as special attributes
background colors are indicated by multiplying the color by $blp::page_publish::BG
All attributes are then or'd together, so for example:
Underlined Light Blue text on a White background would be specified as -

$blp::page_publish::LIGHTBLUE | ($blp::page_publish::BG * $blp::page_publish::WHITE)
| $blp::page_publish::UNDERLINE

=item * $text - text to be displayed

=item * $length - optional pararemeter for length - will default to the string length of text

=back

=back

