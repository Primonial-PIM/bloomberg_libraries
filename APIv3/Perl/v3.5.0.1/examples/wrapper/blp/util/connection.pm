## Core connection util PM

use blp::util::core_util;
use Bloomberg::API::SessionOptions;
use Bloomberg::API::ProviderSession;
use Bloomberg::API::Session;
use Bloomberg::API::Event;
use Bloomberg::API::Message;

use strict;

package blp::util::connection;

sub new
{
##	implicit arg for perl class
	my $proto = shift;

## new(app_name, is_platform)
## app_name - name of application to connect as
## is_platform - if defined and not 0 or "" - connect to plaform - otherwise connect to MD
## is_publisher - if defined and not 0 or "" - connect for publishing - otherwise for subscriptions
	my $app_name=shift;
	my $is_platform=shift;
	my $is_publisher=shift;

	my $class = ref($proto) || $proto; ## check for deref

	## main object repository
	my $self  = {};

	## initialize object
	$self->{APP_NAME}=$app_name;
	$self->{IS_PLATFORM}=$is_platform;
	$self->{IS_PUBLISHER}=$is_publisher;
	$self->{CONNECTION}=undef;
	$self->{IDENTITY}=undef;

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub mode
{
	my $self=shift;
	return $self->{IS_PLATFORM} ? "Platform" : "Market Data";
}


sub connect
{
	my $self = shift;
	my $servers=shift;
	my $app_name=$self->{APP_NAME};
	my $is_platform=$self->{IS_PLATFORM};
	my $is_publisher=$self->{IS_PUBLISHER};

	## Set options
	if ($servers == undef)
	{
		## Check if default is set - otherwise will die
		$servers=blp::util::core_util::hosts($is_platform);
	}
	elsif (ref($servers) ne 'ARRAY')
	{
		print "Servers not specified as [host,port] or [[host1,port1],[host2,port2]...]\n";
		return undef;
	}

	if (ref($servers->[0]) ne 'ARRAY')
	{
		$servers = [$servers];
	}

	## apply options to SessionOptions object
	my $options=new Bloomberg::API::SessionOptions();
	my $phrase='Connecting to';
	foreach my $pair (@$servers)
	{
		print "$phrase $pair->[0]:$pair->[1]";
		$phrase=' or';
	}
	print "\n";


	my $auth=blp::util::core_util::auth_options($app_name);
	$options->setAuthenticationOptions($auth);

	$options->setServerAddresses(@$servers);

        ## let SDK do failover
	$options->setAutoRestartOnDisconnection(1);
	$options->setNumStartAttempts(scalar(@$servers) * 2);

	my $session;
	if ($is_publisher)
	{
		$session=new Bloomberg::API::ProviderSession($options);
	}
	else
	{
		$session=new Bloomberg::API::Session($options);
	}

	my $is_started=$session->start();

	if (!$is_started)
	{
		my $mode=$self->mode();
		printf "Failed to start session:$app_name $mode\n";
		exit(-1);
	}

	$self->{CONNECTION}=$session;

	unless (defined $app_name)
	{
		## Skip AUTHORIZATION for cases where it's not needed
		## $self->{IDENTITY} will be undef which follows API usage
		return $session;
	}

	#### AUTHORIZATION

	my $auth_svc=blp::util::core_util::auth_svc();
	my $is_open=$session->openService($auth_svc);

	if (!$is_open)
	{
		my $mode=$self->mode();
		printf "Failed to open service: $auth_svc for $app_name $mode\n";
		exit(-1);
	}

	my $eventQueue = new Bloomberg::API::EventQueue();

	## Now we ask for our auth token
	$session->generateToken(undef, $eventQueue);

	my $token;

	while (my $event = $eventQueue->nextEvent())
	{
		my $eventType=$event->eventType();

        	if ($eventType ==  Bloomberg::API::EventType::RESPONSE ||
                	$eventType ==  Bloomberg::API::EventType::TOKEN_STATUS) {
                	my $msgIter = new Bloomberg::API::MessageIterator($event);
                	while ($msgIter->next()) {
                        	my $msg = $msgIter->message();
                        	if ($msg->messageType() eq "TokenGenerationSuccess")
                        	{
                                	if ($msg->hasElement("token"))
					{
						$token=$msg->getElementAsString("token");
                                	}
                        	}
                        	last if (defined $token);
                	}
        	}
        	last if (defined $token);
	}

	print "TOKEN=$token\n";

	$eventQueue = new Bloomberg::API::EventQueue();
	my $identity = $session->createIdentity();

	my $authService = $session->getService($auth_svc);
	my $authRequest= $authService->createAuthorizationRequest();
	$authRequest->setElement("token", $token);

	$session->sendAuthorizationRequest($authRequest, $identity, undef, $eventQueue);

	my $more=1;
	while($more)
	{
		my $event = $eventQueue->nextEvent();
		if ($event->eventType() ==  Bloomberg::API::EventType::RESPONSE ||
	      	    $event->eventType() ==  Bloomberg::API::EventType::PARTIAL_RESPONSE ||
      		    $event->eventType() ==  Bloomberg::API::EventType::REQUEST_STATUS)
		{
        		my $msgIter = new Bloomberg::API::MessageIterator($event);
        		while ($msgIter->next())
			{
                		my $msg = $msgIter->message();
                		if ($msg->messageType() eq "AuthorizationSuccess")
                		{
                        		print "Authorization Granted\n";
					$more=0;
                        		last;
                		}
                		else
                		{
					my $mode=$self->mode();
					printf "Authorization FAILED for $app_name $mode :\n";
                        		$msg->print();
                        		return undef;
                		}
        		}
		}
	}

	$self->{IDENTITY}=$identity;

	return $session;
}

sub is_platform
{
	my $self=shift;
	my $topic=shift;
	my $bbg=$self->bloomberg();
	return 0 if ($topic =~ m-^$bbg-);
	return 1 if ($topic =~ m-^//[^/]+/.+-);  ## //XXX/YYY/ZZZ
	return 0;
}

sub identity
{
	my $self=shift;
	return $self->{IDENTITY};
}

1; ## to pass compilation
