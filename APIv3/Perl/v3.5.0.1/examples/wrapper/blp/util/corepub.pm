## Core publishing util PM

use blp::helper;
use blp::util::connection;
use blp::util::marshal;
use Bloomberg::API::Name;
use Bloomberg::API::Event;
use Bloomberg::API::Message;
use Bloomberg::API::EventFormatter;
use strict;

package blp::util::corepub;

my $NAME_topic             = new Bloomberg::API::Name("topic");
my $NAME_topics            = new Bloomberg::API::Name("topics");
my $NAME_TopicSubscribed   = new Bloomberg::API::Name("TopicSubscribed");
my $NAME_TopicUnsubscribed = new Bloomberg::API::Name("TopicUnsubscribed");
my $NAME_SessionTerminated = new Bloomberg::API::Name("SessionTerminated");
my $NAME_TopicActivated    = new Bloomberg::API::Name("TopicActivated");
my $NAME_TopicDeactivated  = new Bloomberg::API::Name("TopicDeactivated");
my $NAME_TopicRecap        = new Bloomberg::API::Name("TopicRecap");
my $NAME_TopicCreated      = new Bloomberg::API::Name("TopicCreated");
my $NAME_PermissionRequest = new Bloomberg::API::Name("PermissionRequest");
my $NAME_SessionStartupFailure = new Bloomberg::API::Name("SessionStartupFailure");

my %eventName = (
	Bloomberg::API::EventType::ADMIN => "ADMIN",
	Bloomberg::API::EventType::SESSION_STATUS => "SESSION_STATUS",
	Bloomberg::API::EventType::SUBSCRIPTION_STATUS => "SUBSCRIPTION_STATUS",
	Bloomberg::API::EventType::REQUEST_STATUS => "REQUEST_STATUS",
	Bloomberg::API::EventType::RESPONSE => "RESPONSE",
	Bloomberg::API::EventType::PARTIAL_RESPONSE => "PARTIAL_RESPONSE",
	Bloomberg::API::EventType::SUBSCRIPTION_DATA => "SUBSCRIPTION_DATA",
	Bloomberg::API::EventType::SERVICE_STATUS => "SERVICE_STATUS",
	Bloomberg::API::EventType::TIMEOUT => "TIMEOUT",
	Bloomberg::API::EventType::AUTHORIZATION_STATUS => "AUTHORIZATION_STATUS",
	Bloomberg::API::EventType::RESOLUTION_STATUS => "RESOLUTION_STATUS",
	Bloomberg::API::EventType::TOPIC_STATUS => "TOPIC_STATUS",
	Bloomberg::API::EventType::TOKEN_STATUS => "TOKEN_STATUS",
	Bloomberg::API::EventType::REQUEST => "REQUEST",
);

sub new
{
	##      implicit arg for perl class
	my $proto = shift;

## new(app_name)
	my $app_name=shift;
	my $service=shift;
	my $fgcolors=shift;
	my $fgmask=shift;
	my $bgcolors=shift;
	my $bgmask=shift;
	my $attrmap=shift;

	my $class = ref($proto) || $proto; ## check for deref

	## main object repository
	my $self = {};

	## initialize object
	$self->{APP_NAME}=$app_name;
	$self->{CONNECTION}=undef;
	$self->{EVENT_TYPE}=undef;
	$self->{CACHE}={};
	$self->{CREATED}={};
	$self->{WAIT}={};

	$self->{ROWS}=undef;
	$self->{COLS}=undef;
	$self->{FGMAP}=[$fgcolors, $fgmask];
	$self->{BGMAP}=[$bgcolors, $bgmask];
	$self->{ATTR}=$attrmap;

	$self->{SERVICE}=$service;
	$self->{IDENTITY}=undef;

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub connect
{
	my $self=shift;
	my $servers=shift;
	my $is_platform=1;
	my $is_publisher=1;
	my $connection=new blp::util::connection($self->{APP_NAME}, $is_platform, $is_publisher);

	my $conn=$connection->connect($servers);
	if ($conn)
	{
		$self->{CONNECTION}=$conn;
		print "Connected\n";
		my $identity=$connection->identity();
		$self->{IDENTITY}=$identity;
		my $svc=$self->{SERVICE};

		my $grp_name="BlpPerlWrapper";
		my $grp_priority=Bloomberg::API::ServiceRegistrationPriority::PRIORITY_MEDIUM;
		my $regopts=(
                        groupId => $grp_name,
                        priority=> $grp_priority,
                );
	
		my $registered=$conn->registerService($svc, $identity, $regopts);

		if ($registered)
		{
			print "Registered $svc\n";
			return $conn;
		}
		print "Could not register Service:$svc\n";
		return 0;
	}

	print "NOT Connected\n";
	return 0;
}

sub publish
{
	my $self=shift;
	my $messages=shift;
	my $async_resolve=shift;
	my $recap=shift;
	my $orig_msg=shift;
	my $conn=$self->{CONNECTION};

	my $service=$conn->getService($self->{SERVICE});

	my $event=$service->createPublishEvent();
	my $fmt=new Bloomberg::API::EventFormatter($event);
	for my $msg (@$messages)
	{
		my $type=$msg->[0];
		my $topic_name=$msg->[1];
		my $message=$msg->[2];

		my $topic_object=$self->{CREATED}->{$topic_name};
		if (!defined $topic_object)
		{
			if ($async_resolve)
			{
				print "NOT CREATED YET: $topic_name\n";
				## not created just skip
				next;
			}
			print "Requesting Topic: $topic_name\n";
			my $topicList = new Bloomberg::API::TopicList();
			my $tcid=new Bloomberg::API::CorrelationId($topic_name);
			$topicList->add($topic_name, $tcid);
			$conn->createTopics($topicList,
				Bloomberg::API::ProviderSession::AUTO_REGISTER_SERVICES,
				$self->{IDENTITY}
			);
			my $status=$topicList->status($tcid);
			if ($status=Bloomberg::API::TopicList::CREATED)
			{
				print "Topic Request for $topic_name Succeeded\n";
				my $tmsg=$topicList->message($tcid);
				$topic_object=$conn->getTopic($tmsg);
				$self->{CREATED}->{$topic_name}=$topic_object;
			}
			else
			{
				print "Topic Request for $topic_name Failed\n";
				next;
			}
		}

		if (!$topic_object->isActive())
		{
			print "NOT ACTIVE YET: $topic_name\n";
			next;
		}

		if ($recap)
		{
			if (defined $orig_msg)
			{
				## Solicited - sent to just requestor
				my @cids=$orig_msg->correlationIds();
				my $cid=$cids[0];
				$fmt->appendRecapMessage($topic_object, $cid);
			}
			else
			{
				## Unsolicited - sent to all
				$fmt->appendRecapMessage($topic_object, undef);
			}
		}
		else
		{
			## Update
			$fmt->appendMessage($type, $topic_object);
		}
		blp::util::marshal::to_message($fmt, $message);
	}
	$conn->publish($event);
}

sub start
{
	my $self=shift;
	my $event_type=shift;
	my $fetch=shift;
	my $unfetch=shift;
	my $ck_update=shift;
	my $perm_check=shift;
	my $is_page=0;

	$self->{EVENT_TYPE}=$event_type;
	loop($self, $fetch, $unfetch, $ck_update, $perm_check, $is_page);
}

sub start_page
{
	my $self=shift;
	my $rows=shift;
	my $cols=shift;
	my $fetch=shift;
	my $unfetch=shift;
	my $ck_update=shift;
	my $perm_check=shift;
	my $is_page=1;

	$self->{ROWS}=$rows;
	$self->{COLS}=$cols;
	loop($self, $fetch, $unfetch, $ck_update, $perm_check, $is_page);
}

sub handle_events
{
	my $self=shift;
	## Handle pending events then get out - quick version of loop

	my $conn=$self->{CONNECTION};
	while (1)
	{
		my $event=$conn->tryNextEvent();
		last unless (defined $event); ## No event waiting - all done
		my $type=$event->eventType();

		## Log every event
		my $typeName=$eventName{$type};
		$typeName="UNKNOWN" unless (defined $typeName);
		my $time=localtime();
		print "$time:GOT event: " . $typeName . "\n";
		my $msgIter = new Bloomberg::API::MessageIterator($event);
		while ($msgIter->next())
		{
			my $msg = $msgIter->message();
			$msg->print();
		}
		if ($type == Bloomberg::API::EventType::SESSION_STATUS)
		{
			my $msgIter = new Bloomberg::API::MessageIterator($event);
			while ($msgIter->next())
			{
				my $msg = $msgIter->message();
				if (($msg->messageType() eq $NAME_SessionTerminated)
			 	|| ($msg->messageType() eq $NAME_SessionStartupFailure))
				{
					## SDK handles failover, these events are not recoverable
					print "Session failure:\n";
					$msg->print();
					exit(-1);
				}
			}
		}
	}
}

sub loop
{
	my $self=shift;
	my $fetch=shift;	# Required
	my $unfetch=shift;	# Required
	my $ck_update=shift;	# Required
	my $perm_check=shift;
	my $is_page=shift;

	my $conn=$self->{CONNECTION};

	my $more=1; ## Callback can set to 0 to stop
	while ($more)
	{
		my $event=$conn->tryNextEvent();
		unless (defined $event) ## No event waiting
		{
			my $ck=&$ck_update();
			return $ck if($ck); ## Non zero means stop
			next;
		}

		my $type=$event->eventType();

		## Log every event
		my $typeName=$eventName{$type};
		$typeName="UNKNOWN" unless (defined $typeName);
		my $time=localtime();
		print "$time:GOT event: " . $typeName . "\n";
		my $msgIter = new Bloomberg::API::MessageIterator($event);
		while ($msgIter->next())
		{
			my $msg = $msgIter->message();
			$msg->print();
		}

		if ($type==Bloomberg::API::EventType::TOPIC_STATUS)
		{
			my $msgIter = new Bloomberg::API::MessageIterator($event);

			while ($msgIter->next())
			{
				my $msg = $msgIter->message();
				my $mtype=$msg->messageType();
				if ($mtype eq $NAME_TopicSubscribed)
				{
					my $topic_name=$msg->getElementAsString($NAME_topic);

					## Ignore dups
					next if (defined $self->{WAIT}->{$topic_name});
					next if (defined $self->{CREATED}->{$topic_name});

					## Check if API has topic cached
					my $topic_object=$conn->getTopic($msg);
					if (defined $topic_object && $topic_object->isActive())
					{
						print "Using cached Topic\n";
						$self->{CREATED}->{$topic_name}=$topic_object;
					}
					else
					{
						## Will get callback when topic is active
						my $topicList = new Bloomberg::API::TopicList();
						$topicList->add($msg);

						$conn->createTopicsAsync($topicList);
						$self->{WAIT}->{$topic_name}=1;
					}
					if ($is_page)
					{
						my $cache=[];
						my $rows=$self->{ROWS};
						my $cols=$self->{COLS};

						for (my $r=1; $r <= $rows; ++$r)
						{
							$cache->[$r]=[" " x $cols, undef];
						}

						$self->{CACHE}->{$topic_name}=$cache;
					}
					else
					{
						$self->{CACHE}->{$topic_name}={};
					}
					$more=&$fetch($topic_name) ? 0 : 1;
				}
				elsif (($mtype eq $NAME_TopicUnsubscribed)
				    || ($mtype eq $NAME_TopicDeactivated))
				{
					## Stop updates
					my $topic_name=$msg->getElementAsString($NAME_topic);

					### Clear any cache on unsubscribe for non-page
					if (($mtype eq $NAME_TopicUnsubscribed)
						&& !$is_page)
					{
						my $topic_object=$conn->getTopic($msg);
						$self->{CACHE}->{$topic_name}={};

						sendImage($self, $topic_name, 0);
					}

					## clean up storage for topic
					delete($self->{CACHE}->{$topic_name})
						if (defined $self->{CACHE}->{$topic_name});

					delete($self->{WAIT}->{$topic_name})
						if (defined $self->{WAIT}->{$topic_name});

					delete($self->{CREATED}->{$topic_name})
						if (defined $self->{CREATED}->{$topic_name});

					$more = &$unfetch($topic_name) ? 0 : 1;
				}
				elsif ($mtype eq $NAME_TopicActivated)
				{
					my $topic_name=$msg->getElementAsString($NAME_topic);
					my $topic_object=$conn->getTopic($msg);
					$self->{CREATED}->{$topic_name}=$topic_object;
					print "Got activation for $topic_name\n";

					delete($self->{WAIT}->{$topic_name})
						if (defined $self->{WAIT}->{$topic_name});

					sendImage($self, $topic_name, $is_page);
				}
				elsif ($mtype eq $NAME_TopicRecap)
				{
					my $topic_name=$msg->getElementAsString($NAME_topic);
					sendImage($self, $topic_name, $is_page, $msg);
				}
				elsif ($mtype eq $NAME_TopicCreated)
				{
					## Informational
				}
				else
				{
					print "Got unknown message type "
						. $mtype->string() ."\n";
				}
			}
		}
		elsif ($type == Bloomberg::API::EventType::SESSION_STATUS)
		{
			my $msgIter = new Bloomberg::API::MessageIterator($event);
			while ($msgIter->next())
			{
				my $msg = $msgIter->message();
				if (($msg->messageType() eq $NAME_SessionTerminated)
				 || ($msg->messageType() eq $NAME_SessionStartupFailure))
				{
					## SDK handles failover, these events are not recoverable
					print "Session failure:\n";
					$msg->print();
					exit(-1);
				}
			}
		}
		elsif ($type==Bloomberg::API::EventType::REQUEST)
		{
			my $msgIter = new Bloomberg::API::MessageIterator($event);
			my $conn=$self->{CONNECTION};
			my $service=$conn->getService($self->{SERVICE});
			while ($msgIter->next())
			{
				my $msg = $msgIter->message();
				if ($msg->messageType() eq $NAME_PermissionRequest)
				{
					# Topic level permissioning - handle directly
					my @cids=$msg->correlationIds();
					my $cid=$cids[0];
					my $response = $service->createResponseEvent($cid);
					my $ef=new Bloomberg::API::EventFormatter($response);
					$ef->appendResponse("PermissionResponse");
					$ef->pushElement("topicPermissions");

					## Get user info
					my $user={};
					my $values=$msg->asElement();
					my $num=$values->numElements();
					my $all_topics=undef;
					for (my $f=0; $f < $num; ++$f)
					{
						my $tuple=$values->getElementAt($f);
						my $name=$tuple->name();
						if ($name eq $NAME_topics)
						{
							$all_topics=$tuple;
							next;
						}
						my $field=blp::helper::trim($name->string());
						my $value=$tuple->getValueAsString();
						$user->{$field}=$value;
					}
					next unless (defined $all_topics);
					my $num_topics=$all_topics->numValues();
					for (my $i=0; $i< $num_topics; $i++)
					{
						$ef->appendElement();
						my $topic_name=$all_topics->getValueAsString($i);
						$ef->setElement($NAME_topic, $topic_name);

						## Do call back
						my $err=0; ## Default to allow if no callback
						if (defined $perm_check)
						{
							$err=&$perm_check($topic_name, $user);
						}
						unless ($err) ## Allow if 0, "" or undef
						{
print "Perm granted for " . $user->{UUID} . " on topic $topic_name\n";
							## 0 means Allow
							$ef->setElement("result", 0);
						}
						else
						{
print "Perm denied for " . $user->{UUID} . " on topic $topic_name\n";
							## 1 means Denied
							$ef->setElement("result", 1);

							# Give reason
							$ef->pushElement("reason");
							$ef->setElement("source", "Perl blp wrapper");
							$ef->setElement("category", "NOT_AUTHORIZED");
							$ef->setElement("subcategory","User Permission denied");
							$ef->setElement("description",$err);
							$ef->setElement("errorCode", 100);
							$ef->popElement();
						}
						$ef->popElement();
					}
					$ef->popElement();
					$conn->sendResponse($response);
				}
			}
		}
		elsif ($type==Bloomberg::API::EventType::RESOLUTION_STATUS)
		{
			## Informational only
		}
		elsif ($type==Bloomberg::API::EventType::SERVICE_STATUS)
		{
			## Informational only
		}
		elsif ($type==Bloomberg::API::EventType::ADMIN)
		{
			## Informational only
		}
		else
		{
			print "ERROR: Unhandled Event Type: " . $type . "\n";
		}
	}
}

sub sendImage
{
	my $self=shift;
	my $topic_name=shift;
	my $is_page=shift;
	my $msg=shift;

	my $what=$self->{CACHE}->{$topic_name};
	return unless (defined $what);

	my $type=undef;

	if ($is_page) ## Page schema
	{
		$what={};
		$what->{numRows}=$self->{ROWS};
		$what->{numCols}=$self->{COLS};
		my $update=[];
		
		my $fgmap=$self->{FGMAP}->[0];
		my $fgmask=$self->{FGMAP}->[1];
		my $bgmap=$self->{BGMAP}->[0];
		my $bgmask=$self->{BGMAP}->[1];
		my $attrmap =$self->{ATTR};
		for (my $i=1; $i <= $self->{ROWS}; ++$i)
		{
			my $rowdata=$self->{CACHE}->{$topic_name}->[$i];
			if ($rowdata)
			{
				my $txt=$rowdata->[0];
				my $attr=$rowdata->[1];
				my $len=length($txt);
				my $a=undef;
				$a=$attr->[0] if (defined $attr);
				my $last=0;
				for (my $c=0; $c <= $len; ++$c) ## one past
				{
					my $sa=undef;
					$sa=$attr->[$c] if (defined $attr);
					next if (($sa == $a) && ($c != $len));

					my $stxt=substr($txt, $last, $c-$last);
					my $rowinfo={};
					$rowinfo->{rowNum}=$i;
					my $span={};
					$span->{startCol}=$last+1;
					$span->{'length'}=$c-$last;
					$span->{text}=$stxt;
					my $ats=[];
					if (defined $a)
					{
						my $fg=$fgmap->{$a & $fgmask};
						my $bg=$bgmap->{$a & $bgmask};
						$span->{fgColor}=$fg;
						$span->{bgColor}=$bg;

						while (my ($att,$astr)=each %$attrmap)
						{
							if ($a & $att)
							{
								push(@$ats, $astr);
							}
						}
					}
					$span->{attr}=$ats;

					$rowinfo->{spanUpdate}=$span;
					push(@$update, $rowinfo);

					$last=$c;
					$a=$sa;
				}
			}
		}
		$what->{rowUpdate}=$update;
		$type="PageUpdate";
	}
	else ## Not page - just flat market data
	{
		$type=$self->{EVENT_TYPE};
	}
	my $message=[[$type, $topic_name, $what]];
	publish($self, $message, 1, 1, $msg); ## send as recap
}

sub update
{
	my $self=shift;
	my $topic_name=shift;
	my $what=shift;

	my $topic_object=$self->{CREATED}->{$topic_name};
	unless (defined $topic_object)
	{
		print "Topic Not Active: $topic_name\n";
                if (defined $self->{WAIT}->{$topic_name}) {
                        print "Caching updates...\n";
                }
                else {
			print "Not found\n";
                        return;
                }
	}

	while (my ($field, $value) = each %$what)
	{
		$self->{CACHE}->{$topic_name}->{$field}=$value;
	}

	if(defined $topic_object) {
		my $type=$self->{EVENT_TYPE};
		my $message=[[$type, $topic_name, $what]];
		publish($self, $message, 1);
	}
}

sub update_page
{
	my $self=shift;
	my $topic_name=shift;
	my $row=shift;
	my $start_col=shift;
	my $attr=shift;
	my $what=shift;
	my $len=shift;

	$start_col-=1;
	my $wlen=length($what);
	if ($wlen < $len)
	{
		$what.=" " x ($len - $wlen);
	}

	## Save to Cache
	my $endlen=$start_col + $len;
	my $rowdata=$self->{CACHE}->{$topic_name}->[$row];
	if (!$rowdata)
	{
		$self->{CACHE}->{$topic_name}->[$row]=[];
		$rowdata=$self->{CACHE}->{$topic_name}->[$row];
		$rowdata->[0]=" " x $endlen;
		$rowdata->[1]=undef;
	}
	my $rtxt=$rowdata->[0];
	my $rlen = length($rtxt);
	if ($rlen < $endlen)
	{
		$rtxt.=" " x ($endlen - $rlen);
	}
	substr($rtxt, $start_col, $len, $what);
	$rowdata->[0]=$rtxt;
	if (defined $attr)
	{
		$rowdata->[1]=[] unless ($rowdata->[1]);
		for (my $a=$start_col; $a < $endlen; ++$a)
		{
			$rowdata->[1]->[$a]=$attr;
		}
	}


	## Publish as Update
	my $type="RowUpdate";
	my $fgmap=$self->{FGMAP}->[0];
	my $fgmask=$self->{FGMAP}->[1];
	my $bgmap=$self->{BGMAP}->[0];
	my $bgmask=$self->{BGMAP}->[1];
	my $attrmap =$self->{ATTR};

	my $rowinfo={};
	$rowinfo->{rowNum}=$row;
	my $span={};
	$span->{startCol}=$start_col+1;
	$span->{'length'}=$len;
	$span->{text}=$what;
	my $ats=[];
	if (defined $attr)
	{
		my $fg=$fgmap->{$attr & $fgmask};
		my $bg=$bgmap->{$attr & $bgmask};
		$span->{fgColor}=$fg;
		$span->{bgColor}=$bg;
		while (my ($att,$astr)=each %$attrmap)
		{
			if ($attr & $att)
			{
				push(@$ats, $astr);
			}
		}
	}
	$span->{attr}=$ats;

	$rowinfo->{spanUpdate}=$span;
	my $message=[[$type, $topic_name, $rowinfo]];
	publish($self, $message,1);
}

1;
