## PM for util functions for othe blp PMs
## Note - this file should be changed to set per site defaults

package blp::util::core_util;
use strict;

### SET THESE

### Platform
### host/port to connect for platform pub/sub or contribution
### either [ host, port] or [ [host1,port1], [host2,port2] ... ]
### port is generally 8196
my $PLAT_HOSTS=undef;

### Market data
### host/port to connect for market data or ref data
### either [ host, port] or [ [host1,port1], [host2,port2] ... ]
### port is generally 8194
my $MD_HOSTS=undef;

### DO NOT CHANGE ANYTHING BELOW
##############################################################################
my $BLOOMBERG = "//blp";
my $AUTH_SVC = "$BLOOMBERG/apiauth";
my $AUTHENTICATION_MECHANISM = "AuthenticationMode=APPLICATION_ONLY;ApplicationAuthenticationType=APPNAME_AND_KEY;ApplicationName=";

sub hosts
{
	my $is_platform=shift;
	if ($is_platform)
	{
		die "No Platform server specified and Default Platform not set"
			unless (defined $PLAT_HOSTS);
		return $PLAT_HOSTS;
	}
	die "No Market Data server specified and Default Market Data host not set"
		unless (defined $MD_HOSTS);
	return $MD_HOSTS;
}

sub bloomberg
{
	return $BLOOMBERG;
}

sub auth_svc
{
	return $AUTH_SVC;
}

sub auth_options
{
	my $app_name=shift;

	return $AUTHENTICATION_MECHANISM . $app_name;
}

1; ## to pass compilation
