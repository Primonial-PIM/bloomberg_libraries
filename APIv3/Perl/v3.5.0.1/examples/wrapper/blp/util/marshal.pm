## PM for translating Blp Elements to perl and back

use blp::helper;
use strict;

package blp::util::marshal;

sub to_perl
{
	my $what=shift;
	my $fields=shift;

	my %req=();

	if ($fields)
	{
		for my $f (split /,/, $fields)
		{
			$req{$f}=1;
		}
	}
	my $ret;

	if ($what->isNull())
	{
		$ret=undef;
	}
	elsif ($what->isComplexType())
	{
		$ret={};
		my $num=$what->numElements();
		for (my $f=0; $f < $num; ++$f)
		{
			my $tuple=$what->getElementAt($f);
			my $name=blp::helper::trim($tuple->name()->string());

			if (!$fields || $req{$name})
			{
				$ret->{$name}=to_perl($tuple, $fields);
			}
		}
	}
	elsif($what->isArray())
	{
		$ret=[];
		my $num=$what->numValues();
		my $dtype=$what->dataType();
		if (($dtype == Bloomberg::API::DataType::SEQUENCE)
			|| ($dtype == Bloomberg::API::DataType::DATATYPE_CHOICE))
		{
			for (my $a=0; $a<$num; ++$a)
			{
				my $entry=$what->getValueAsElement($a);
				push @$ret, to_perl($entry, $fields);
			}
		}
		else
		{
			for (my $a=0; $a<$num; ++$a)
			{
				my $entry=$what->getValueAsString($a);
				push @$ret, $entry;
			}
		}
	}
	else ## Simple field
	{
		$ret=$what->getValueAsString();
	}

	return $ret;
}

sub to_message
{
	my $fmt=shift;
	my $what=shift;

	my $wtype=ref($what);
	return if ($wtype ne 'HASH');

	while (my ($field, $value) = each %$what)
	{
		my $type=ref($value);

		## resolve references to base types
		while ($type eq 'SCALAR')
		{
			$value=$$value;
			$type=ref($value);
		}

		if (!$type)
		{
			$fmt->setElement($field, $value);
		}
		elsif ($type eq 'ARRAY')
		{
			$fmt->pushElement("$field");
			for (my $i=0; $i <= $#$value; ++$i)
			{
				my $vtype=ref($value->[$i]);
				if ($vtype)
				{
					$fmt->appendElement();
					to_message($fmt, $value->[$i]);
					$fmt->popElement();
				}
				else
				{
					$fmt->appendValue($value->[$i]);
				}
			}
			$fmt->popElement();
		}
		elsif ($type eq 'HASH')
		{
			$fmt->pushElement("$field");
			$fmt->appendElement();
			to_message($fmt, $value);
			$fmt->popElement();
			$fmt->popElement();
		}
	}
}

1;
