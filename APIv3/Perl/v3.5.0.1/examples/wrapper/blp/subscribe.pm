## PM to subscribe to data

use blp::helper;
use blp::util::connection;
use blp::util::marshal;
use Bloomberg::API::Name;
use Bloomberg::API::SubscriptionList;
use Bloomberg::API::CorrelationId;

use strict;

package blp::subscribe;

my $NAME_SessionTerminated=new Bloomberg::API::Name('SessionTerminated');
my $NAME_SessionStartupFailure=new Bloomberg::API::Name('SessionStartupFailure');

sub new
{
	##      implicit arg for perl class
        my $proto = shift;

## new(app_name)
        my $app_name=shift;
	my $is_platform=shift;

	my $class = ref($proto) || $proto; ## check for deref

        ## main object repository
	my $self  = {};

        ## initialize object
	$self->{APP_NAME}=$app_name;
	$self->{CONNECTION}=undef;
	$self->{IDENTITY}=undef;
	$self->{ID}=0; ## ref data correlation id
	$self->{TOPICS}=[];
	$self->{SERVICES}={};
	push @{$self->{TOPICS}}, "AUTH";  ## Resreve 0 for auth

	$self->{IS_PLATFORM}=$is_platform;

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub connect
{
	my $self=shift;
	my $servers=shift;

	my $is_platform=$self->{IS_PLATFORM};
	my $connection=new blp::util::connection($self->{APP_NAME}, $is_platform);

	my $conn=$connection->connect($servers);
	if (!$conn)
	{
		print "NOT Connected\n";
		return 0;
	}

	$self->{CONNECTION}=$conn;
	$self->{IDENTITY}=$connection->identity();
	print "Connected\n";
	return $conn->openService('//blp/mktdata');
}

sub addSubscription
{
	my $self=shift;
	my $topics=shift;
	my $fields=shift;
	my $options=shift;

	my $field_list=$fields;
	if (ref($fields) eq 'ARRAY')
	{
		$field_list=join ',', @$fields;
	}

	my $option_list=$options;
	if (ref($options) eq 'ARRAY')
	{
		$option_list=join ',', @$options;
	}

	if (ref($topics) ne 'ARRAY')
	{
		$topics=[$topics];
	}


	my $conn=$self->{CONNECTION};
	my $slist=new Bloomberg::API::SubscriptionList();

	my $arglist=[];
	for my $sub (@$topics)
	{
print "Adding subscription for $sub |$field_list |$option_list\n";
		my $full_sub=$sub;
		if (defined $field_list)
		{
			if ($full_sub =~ /\?fields=/)
			{
				$full_sub .= ",";
			}
			else
			{
				$full_sub .= "?fields=";
			}
			$full_sub .= $field_list;
		}
		push @{$self->{TOPICS}}, $full_sub;
		$self->{ID}++;
		my $id=$self->{ID};
		my $corr=new Bloomberg::API::CorrelationId($id);
		my $args = {
			topic=>$sub,
			correlationId=>$corr
		};

		$args->{fields}=$field_list if (defined $field_list);
		$args->{options}=$option_list if (defined $option_list);

		push @$arglist, $args;
	}

	$slist->add($arglist);
	my $id=$self->{IDENTITY};
	$conn->subscribe($slist,$id);
}

sub default_callback
{
	my $topic=shift;
	my $message_type=shift;
	my $message=shift;

	print "$topic: $message_type";
	blp::helper::show($message);
	print "\n\n";

	return 0; ## keep processing
}

sub start
{
	my $self=shift;
	my $securities=shift;
	my $fields=shift;
	my $options=shift;
	my $call_back=shift;

	$call_back=\&default_callback unless $call_back;

	addSubscription($self, $securities, $fields, $options);

	my $conn=$self->{CONNECTION};

	my $more=1;
	while($more)
	{
		my $evt=$conn->nextEvent();
		my $iter=new Bloomberg::API::MessageIterator($evt);
		if ($evt->eventType() == Bloomberg::API::EventType::SUBSCRIPTION_DATA)
		{
			while ($iter->next())
			{
				my $msg=$iter->message();
				my $element=$msg->asElement();
				my @cids=$msg->correlationIds();
				my $id=$cids[0]->asInteger();
				my $topic_fields=$self->{TOPICS}->[$id];
				my $fields=undef;
				my $topic=$topic_fields;
				if ($topic_fields =~ /\?fields=/)
				{
					$fields=$topic_fields;
					$fields =~ s/^.*\?fields=//; ## JUST WHAT THEY ASKED FOR
			
					$topic =~ s/\?fields=.*$//;
				}
				my $msg_type=$msg->messageType()->string();
				my $result=blp::util::marshal::to_perl($element, $fields);
				my $size=scalar keys %$result;
				if ($size)
				{
					$more=$call_back->($topic, $msg_type, $result)
						? 0 : 1;
				}
			}
		}
		elsif ($evt->eventType() == Bloomberg::API::EventType::SESSION_STATUS)
		{
			while ($iter->next())
			{
				my $msg=$iter->message();
				print "Session Status:\n";
				$msg->print();
				my $mtype=$msg->messageType();
				if (($mtype eq $NAME_SessionTerminated)
				 || ($mtype eq $NAME_SessionStartupFailure))
				{
					print "Session ended. Exiting...\n";
					exit(-1);
				}
			}
		}
		else
		{
			while ($iter->next())
			{
				my $msg=$iter->message();
				print "\nUnhandled: START\n";
				my @cids=$msg->correlationIds();
				my $id=$cids[0]->asInteger();
				my $topic_fields=$self->{TOPICS}->[$id];
				print "Unhandled for Topic: $topic_fields\n";
				$msg->print();
				print "Unhandled: END\n\n";
			}
		}
	}
}

1;

__END__

=pod

=head1 NAME

blp::subscribe - sample wrapper for making subscription requests

=head1 DESCRIPTION

The requestor object can be used to make subscriptions to data from Managed B-Pipe or Platform
It is written as a framework with key callbacks specified by the user to handle the most
common events

=head1 EXAMPLE FILES

=over 4

=item * TEST_blp_MktData_subscribe.pl

Market Data from Managed B-Pipe

=item * TEST_blp_local_subscribe.pl

Locally published pages as published by:
TEST_blp_local_publish.pl and TEST_blp_page_publish.pl

=back

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item * new($EMRS_Application, $is_platform)

Pass the EMRS application, and then true if data is from platform

=item * connect($PlatformServers)

Pass an array with element 0 as the IP/Hostname and element 1 as the port for 
Managed B-Pipe or for the Bloomberg Platform.
For multiple hosts, pass an array of these arrays

=item * start($securities, $fields, $options, \&call_back)

=over 4

=item * $securities - see addSubscription()

=item * $fields - see addSubscription()

=item * $options - see addSubscription()

=item * $call_back - an optional call_back to handle the subscription data

If call_back is not provided - a default handler will print out the data received

call_back will be passed the topic string, the message type, and a wrapper message
The wrapper message is a hash of names and values - where values can be arrays for schemas that
need event sequences and hashes for submessages, element choices

=back

start will only return if the call_back returns true
the default call_back always returns false and if used, start will not return

=item * addSubscription($securities, $fields, $options)

The call_back from start() may call addSubscription to add new securities

=over 4

=item * $securities

Either a single security or an array of securities to subscribe to

=item * $fields

Either 'undef' or a comma separated list of fields or an array of fields to get for each security

Note that fields can also be specified by appending '?fields=' and then a comma
separated list of fields to each security, in that case pass 'undef'

=item * $options

Either 'undef' or a comma separated list of options or an array of options to apply to each security


=back

=back

