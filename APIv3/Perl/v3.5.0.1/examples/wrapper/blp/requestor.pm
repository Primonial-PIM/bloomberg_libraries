## PM for reference data requests

use blp::util::connection;
use blp::util::marshal;
use blp::helper;

use strict;

package blp::requestor;

## REQUEST TYPES
our $REF_REQUEST='ReferenceDataRequest';
our $HIST_REQUEST='HistoricalDataRequest';

my $NAME_securities=new Bloomberg::API::Name('securities');
my $NAME_security=new Bloomberg::API::Name('security');
my $NAME_securityData=new Bloomberg::API::Name('securityData');
my $NAME_securityError=new Bloomberg::API::Name('securityError');
my $NAME_fields=new Bloomberg::API::Name('fields');
my $NAME_fieldData=new Bloomberg::API::Name('fieldData');
my $NAME_fieldExceptions=new Bloomberg::API::Name('fieldExceptions');
my $NAME_fieldId=new Bloomberg::API::Name('fieldId');
my $NAME_errorInfo=new Bloomberg::API::Name('errorInfo');
my $NAME_SessionTerminated=new Bloomberg::API::Name('SessionTerminated');
my $NAME_SessionStartupFailure=new Bloomberg::API::Name('SessionStartupFailure');
my $NAME_date=new Bloomberg::API::Name('date');

my $RET_ERROR='_ERROR_';
my $RET_ERROR_TEXT='_ERROR_TEXT_';
my $RET_FIELD_ERRORS='_FIELD_ERRORS_';
my $REF_SVC='//blp/refdata';

sub new
{
	##      implicit arg for perl class
        my $proto = shift;

## new(app_name)
        my $app_name=shift;

	my $class = ref($proto) || $proto; ## check for deref

        ## main object repository
	my $self  = {};

        ## initialize object
	$self->{APP_NAME}=$app_name;
	$self->{CONNECTION}=undef;
	$self->{ID}=0; ## ref data correlation id
	$self->{IDENTITY}=undef;

	## Normal perl class setup
	bless($self, $class);
	return $self;
}

sub connect
{
	my $self=shift;
	my $servers=shift;
	my $connection=new blp::util::connection($self->{APP_NAME}, 0);

	my $conn=$connection->connect($servers);
	if ($conn)
	{
		$self->{CONNECTION}=$conn;
		print "Connected\n";
		my $is_open=$conn->openService($REF_SVC);
		if ($is_open)
		{
			print "Openned service: $REF_SVC\n";

			$self->{IDENTITY}=$connection->identity();

			return 1;
		}
		print "Cannot open service: $REF_SVC\n";
		return 0;
	}


	print "NOT Connected\n";

	return 0;
}

sub request
{
	my $self=shift;
	my $securities=shift;
	my $fields=shift;
	my $options=shift;
	my $type=shift;
	my $raw=shift;

	$type=$REF_REQUEST unless (defined $type);

	my $conn=$self->{CONNECTION};
	my $ref_svc=$conn->getService($REF_SVC);
	my $req=$ref_svc->createRequest($type);
	my $reqElement=$req->asElement();

	my $secElement=$reqElement->getElementByName("securities");
	foreach my $seq (@$securities)
	{
		$secElement->appendValue($seq);
	}

	my $fieldElement=$reqElement->getElementByName("fields");
	foreach my $fld (@$fields)
	{
		$fieldElement->appendValue($fld);
	}

	foreach my $opt (@$options)
	{
		my ($param, $value) = split '=', $opt;
		$reqElement->setElement($param, $value);
	}

	my $queue=new Bloomberg::API::EventQueue();      ## Make it Blocking
	$self->{ID}++;
	my $id=$self->{ID};
	my $fakeID=new Bloomberg::API::CorrelationId($id);
	my $serverId=$self->{IDENTITY};

	$conn->sendRequest($req, $serverId, $fakeID, $queue);

	my $ret;
	if ($raw)
	{
		$ret=[];
	}
	else
	{
		$ret={};
	}

	my $more=1;
	while($more)
	{
		my $evt=$queue->nextEvent();
		my $iter=new Bloomberg::API::MessageIterator($evt);

		if ($evt->eventType() == Bloomberg::API::EventType::RESPONSE)
		{
			$more=0; ## all done, process message then return
		}
		elsif ($evt->eventType() == Bloomberg::API::EventType::PARTIAL_RESPONSE)
		{
print "PARTIAL MESSAGE\n";
			$more=1; ## not last message, process message then loop more
		}
		elsif ($evt->eventType() == Bloomberg::API::EventType::SESSION_STATUS)
		{
			while ($iter->next())
			{
				my $msg=$iter->message();
				print "Session Status:\n";
				$msg->print();
				my $mtype=$msg->messageType();
				if (($mtype eq $NAME_SessionTerminated)
				 || ($mtype eq $NAME_SessionStartupFailure))
				{
					print "Session ended. Exiting...\n";
					exit(-1);
				}
			}
			next;
		}
		else
		{
			while ($iter->next())
			{
				my $msg=$iter->message();
				print "Status Message:\n";
				$msg->print();
			}
			next;
		}

		while ($iter->next())
		{
			my $msg=$iter->message();
			if ($raw)
			{
				my $element=$msg->asElement();
				my $result=blp::util::marshal::to_perl($element);
				push @$ret, $result;
				next;
			}
			my $array=$msg->getElementByName($NAME_securityData);
			my $num=$array->numValues();
			for (my $i=0; $i < $num; ++$i)
			{
				my $vals={};
				my $sec=undef;
				if ($type eq $HIST_REQUEST)
				{
					$sec=$array;
				}
				else
				{
					$sec=$array->getValueAsElement($i);
				}
				
				my $tick=$sec->getElementAsString($NAME_security);
				if ($sec->hasElement($NAME_securityError))
				{
					my $results=$sec->getElementByName($NAME_securityError);
					my $details=blp::util::marshal::to_perl($results);
					$vals->{$RET_ERROR}=$details;
					$ret->{$tick}=$vals;
					next;
				}
				if ($sec->hasElement($NAME_fieldData))
				{
					my $results=$sec->getElementByName($NAME_fieldData);
					if ($type eq $HIST_REQUEST)
					{
						$vals=get_formatted_hist($results);
					}
					else
					{
						$vals=blp::util::marshal::to_perl($results);
					}
				}
				if ($sec->hasElement($NAME_fieldExceptions))
				{
					my $exceptions=$sec->getElementByName($NAME_fieldExceptions);
					my $errs=$exceptions->numValues();
					my $elist={};
					my $found=0;
					for (my $e=0; $e < $errs; ++$e)
					{
						$found=1;
						my $elem=$exceptions->getValueAsElement($e);
						my $fid=$elem->getElementAsString($NAME_fieldId);
						my $info=$elem->getElementByName($NAME_errorInfo);
						my $details=blp::util::marshal::to_perl($info);
						$elist->{$fid}=$details;
					}
					$vals->{$RET_FIELD_ERRORS}=$elist if ($found);
				}
				$ret->{$tick}=$vals;
			}
		}
	}
	return $ret;
}

sub get_formatted_hist
{
	my $results=shift;
	my $ret={};
	my $num_results=$results->numValues();
	for (my $j=0; $j<$num_results; ++$j)
	{
		my $vallist=$results->getValueAsElement($j);
		##next unless ($vallist->hasElement($NAME_date));
		my $dt=$vallist->getElementAsString($NAME_date);
		my $num_vals=$vallist->numElements();
		for (my $k=0; $k<$num_vals; ++$k)
		{
			my $field=$vallist->getElementAt($k);
			my $nm=$field->name();
			my $val=$field->getValueAsString();
			next if ($NAME_date eq $nm);
			$ret->{$dt}={} unless (defined $ret->{$dt});
			$ret->{$dt}->{$nm}=$val;
		}
	}
	return $ret;
}

1;

__END__

=pod

=head1 NAME

blp::requestor - sample wrapper for making reference data requests

=head1 DESCRIPTION

The requestor object can be used to make reference data requests from Managed B-Pipe

=head1 EXAMPLE FILE

TEST_blp_requestor.pl

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item * new($EMRS_Application)

Pass the EMRS application

=item * connect($PlatformServers)

Pass an array with element 0 as the IP/Hostname and element 1 as the port for
Managed B-Pipe.   For multiple hosts, pass an array of these arrays

=item * request(\@topics, \@fields, \@options, $type, $native)

=over 4

=item * @topics - list of securities to request

=item * @fields - list of fields to request

=item * @options - list of request options each item in list is param=value

=item * @type - request type - ReferenceDataRequest is default

HistoricalDataRequest is also supported

=item * $native - use native formatting

=over 4

=item if $native is true

The return will be the raw return from the request as represented by native hashes and arrays as per the requested data schema

=item if $native is false (default)

The return will be a hash where the keys are the securities and the value is a hash where the key is the requested field and the values are the associated field values

=back

=back

=back
