# Copyright 2012. Bloomberg Finance L.P.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:  The above copyright
# notice and this permission notice shall be included in all copies or
# substantial portions of the Software.  THE SOFTWARE IS PROVIDED "AS IS",
# WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
# THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use strict;
use warnings;

use Bloomberg::API::AbstractSession;
use Bloomberg::API::SessionOptions;
use Bloomberg::API::Session;
use Bloomberg::API::Message;
use Bloomberg::API::SubscriptionList;
use Bloomberg::API::CorrelationId;
use Bloomberg::API::Name;
use Bloomberg::API::Element;
use Bloomberg::API::Identity;

use Getopt::Long;

my $TOKEN_SUCCESS = Bloomberg::API::Name->new("TokenGenerationSuccess");
my $TOKEN_FAILURE = Bloomberg::API::Name->new("TokenGenerationFailure");
my $AUTHORIZATION_SUCCESS = Bloomberg::API::Name->new("AuthorizationSuccess");
my $TOKEN = Bloomberg::API::Name->new("token");

my $AUTH_USER        = "AuthenticationType=OS_LOGON";
my $AUTH_APP_PREFIX  = "AuthenticationMode=APPLICATION_ONLY;".
                       "ApplicationAuthenticationType=APPNAME_AND_KEY;".
                       "ApplicationName=";
my $AUTH_DIR_PREFIX  = "AuthenticationType=DIRECTORY_SERVICE;".
                       "DirSvcPropertyName=";

my $AUTH_OPTION_NONE = "none";
my $AUTH_OPTION_USER = "user";
my $AUTH_OPTION_APP  = "app=";
my $AUTH_OPTION_DIR  = "dir=";

my $AUTH_SERVICE = "//blp/apiauth";

# print all the messages in an event
sub printEvent
{
    my $event = shift;
    my $messageIter = Bloomberg::API::MessageIterator->new($event);
    while (my $msg = $messageIter->next()) {
        $msg->print();
    }
}

# handle the events in session's event queue
sub eventHandler {
    my ($session) = @_;
    my $event;
    while ($event = $session->tryNextEvent()) {
        printEvent($event);
    }
}

# keep on checking events in session's event queue and handle it if any
# until a maximum number of events received
sub blockingEventHandler {
    my ($session, $maxEvents) = @_;
    my $count = 0;
    my $event;
    while ($event = $session->nextEvent()) {
        printEvent($event);
        if ($event->eventType() == Bloomberg::API::EventType::SUBSCRIPTION_DATA) {
            if (defined $maxEvents && $count++ >= $maxEvents) {
                last;
            }
        }
    }
}

sub authorize {
    my ($session, $ident) = @_;

# generating token
    my $eventQueue = Bloomberg::API::EventQueue->new();
    $session->generateToken(undef, $eventQueue);
    my $event = $eventQueue->nextEvent();
    my $token;
    if ($event->eventType() eq Bloomberg::API::EventType::TOKEN_STATUS) {
        my $messageItr = Bloomberg::API::MessageIterator->new($event);
        while (my $msg = $messageItr->next()) {
            $msg->print();
            if ($msg->messageType() eq $TOKEN_SUCCESS) {
                $token = $msg->getElementAsString($TOKEN);
            }
            else {
                return 0;
            }
        }
    }
    else {
        return 0;
    }

# sending authorization request
    my $authService = $session->getService($AUTH_SERVICE);
    my $authReq = $authService->createAuthorizationRequest();
    $authReq->setElement($TOKEN, $token);

    $session->sendAuthorizationRequest($authReq, $ident);
    my $done = 0;
    while (!$done) {
        my $event = $session->nextEvent(10000);
        if ($event->eventType() == Bloomberg::API::EventType::RESPONSE ||
            $event->eventType() == Bloomberg::API::EventType::REQUEST_STATUS ||
            $event->eventType() == Bloomberg::API::EventType::PARTIAL_RESPONSE) {
            my $messageIter = Bloomberg::API::MessageIterator->new($event);
            while (my $msg = $messageIter->next()) {
                $msg->print();
                if ($msg->messageType() eq $AUTHORIZATION_SUCCESS) {
                    return 1;  
                }
            }
        }
        return 0;
    }
}

sub usage
{
    print "Usage:\n";
    print "    Local subscription example\n";
    print "        [-ip   <ipAddress  = localhost>\n";
    print "        [-p    <tcpPort    = 8194>\n";
    print "        [-s    <service>]    \tservice name (default: //viper/mktdata)\n";
    print "        [-t    <topic>]      \ttopic (default: /ticker/IBM Equity>]\n";
    print "        [-me   <max_events]  \tmaximum number of events (default is unlimited)\n";
    print "        [-auth <option>]     \tauthentication option: user|none|app=<app>|dir=<property>(default: user)\n";
    print "        [-n    <number>]     \tnumber of authorization requests (default: number of hosts)\n";
    exit 1;
}

# Command line options
my @host;
my $port = "8194";
my $serviceName = "//viper/mktdata";
my @topics;
my $maxEvents;
my $auth = $AUTH_OPTION_USER;
my $numAuth = -1;

my $help;
usage() if (!GetOptions(
        'ip:s'   => \@host,
        'p:i'    => \$port,
        's:s'    => \$serviceName,
        't:s'    => \@topics,
        'auth:s' => \$auth,
        'me:i'   => \$maxEvents,
        'n:i'    => \$numAuth,
        'h'      => \$help
        ) or defined $help );

if (!scalar(@host)) {
    push(@host, "localhost");
}
if (!scalar(@topics)) {
    push(@topics, "/ticker/IBM Equity");
}

my $authOptions = $AUTH_USER;
if ($auth eq $AUTH_OPTION_NONE) {
    $authOptions = "";
}
elsif ($auth eq $AUTH_OPTION_USER) {
    $authOptions = $AUTH_USER;
}
elsif (substr($auth, 0, length($AUTH_OPTION_APP))
        eq $AUTH_OPTION_APP) {
    $authOptions = "";
    $authOptions = $authOptions.$AUTH_APP_PREFIX;
    $authOptions = $authOptions.substr($auth,
            length($AUTH_OPTION_APP));
}
elsif (substr($auth, 0, length($AUTH_OPTION_DIR))
        eq $AUTH_OPTION_DIR) {
    $authOptions = "";
    $authOptions = $authOptions.$AUTH_DIR_PREFIX;
    $authOptions = $authOptions.substr($auth,
            length($AUTH_OPTION_DIR));
}
else {
    usage();
}

if ($numAuth < 0) {
    $numAuth = scalar (@host);
}

eval {
# creating session option
    my $sessionOption = Bloomberg::API::SessionOptions->new();
    my @hostTuple;
    foreach my $ip (@host) {
        print "Connecting to $ip:$port\n";
        push @hostTuple, [$ip, $port];
    }
    $sessionOption->setServerAddresses(@hostTuple);
    $sessionOption->setAutoRestartOnDisconnection(1);
    $sessionOption->setNumStartAttempts(scalar(@hostTuple));
    $sessionOption->setAuthenticationOptions($authOptions);

# creating and starting session
    my $session = Bloomberg::API::Session->new($sessionOption);
    my $rcode = $session->start();
    if (!$rcode) {
        die "Failed to Start Session\n";
    }
    eventHandler($session);

# authorization
    my $ident = $session->createIdentity();
    if (length($authOptions) != 0) {
        if (!$session->openService($AUTH_SERVICE)) {
            die "Failed to open authorization service";
        }
        eventHandler($session);
        my $anyAuthorized = 0;
        for (my $i = 0; $i < $numAuth; $i++) {
            if (authorize($session, $ident)) {
                $anyAuthorized = 1;
            }
        }
        if (!$anyAuthorized) {
            die "No authorization";
        }
    }

# creating subscription list
    my $list = Bloomberg::API::SubscriptionList->new();
    foreach my $topic (@topics) {
        $list->add([{topic => $serviceName.$topic}]);
    }

# subscribing to topics
    $session->subscribe($list, $ident);
    blockingEventHandler($session, $maxEvents);

# stopping session
    $session->stop();
};

if($@) {
    if($@->isa("Bloomberg::API::Exception")) {
        print $@->type()."       ".$@->description()."\n";
    }
    else {
        print "Exception Caught $@ \n";
    }
}
