# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Exception;

package Bloomberg::API::Identity;

package Bloomberg::API::Identity::SeatType;
use constant INVALID_SEAT => $Bloomberg::API::Internal::BLPAPI_SEATTYPE_INVALID_SEAT;
use constant BPS => $Bloomberg::API::Internal::BLPAPI_SEATTYPE_BPS;
use constant NONBPS => $Bloomberg::API::Internal::BLPAPI_SEATTYPE_NONBPS;


package Bloomberg::API::Identity;

sub createInternally
{
    my $class = shift;
    my ($identity) = @_;
    $identity->isa("_p_blpapi_Identity")
        or die("Can be created Internally only");
    my ($self) = {
        d_identity_t => $identity
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_Identity_release($self->{d_identity_t});
}

sub hasEntitlementsForEids
{
    my $self = shift;
    my ($service, @eids) = @_;
    my $inArray = Bloomberg::API::Internal::blpapi_internal_IntArray_create(
        scalar(@eids));
    my $outArray = Bloomberg::API::Internal::blpapi_internal_IntArray_create(
        scalar(@eids));

    my $count = 0;
    my $i;
    foreach $i (@eids) {
        Bloomberg::API::Internal::blpapi_internal_IntArray_set($inArray, $i, $count++);
    }

    my ($rcode, $failedEntitlementsCount) =
        Bloomberg::API::Internal::blpapi_internal_Identity_hasEntitlements(
            $self->{d_identity_t},
            $service->{d_service_t},
            undef,
            $inArray,
            $outArray);

    my @failedEids;
    for $i (0..$failedEntitlementsCount-1) {
        my ($rc, $val) = Bloomberg::API::Internal::blpapi_internal_IntArray_get($outArray, $i);
        if($rc) {
            last;
        }
        push (@failedEids, $val);
    }
    Bloomberg::API::Internal::blpapi_internal_IntArray_destroy($inArray);
    Bloomberg::API::Internal::blpapi_internal_IntArray_destroy($outArray);
    return ($rcode, @failedEids);
}

sub isAuthorized
{
    my $self = shift;
    my ($service) = @_;
    return Bloomberg::API::Internal::blpapi_Identity_isAuthorized(
        $self->{d_identity_t},
        $service->{d_service_t});
}

sub getSeatType
{
    my $self = shift;
    my ($rcode, $seatType) =
        Bloomberg::API::Internal::blpapi_Identity_getSeatType(
            $self->{d_identity_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return $seatType;
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Identity - Represents the identification of a user.

=head1 DESCRIPTION

Provides access to the entitlements for a specific user.

An unauthorized Identity is created using
Session::createIdentity(). Once an Identity has been created
it can be authorized using
Session::sendAuthorizationRequest(). The authorized Identity
can then be queried or used in Session::subscribe() or
Session::sendRequest() calls.

Once authorized an Identity has access to the entitlements of
the user which it was validated for.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item hasEntitlementsForEids(service, (list of eids))

Checks if this Identity is authorized for all the
entitlement Ids contained in the specified 'entitlementIds' array
on the specified 'service'. Returns 1 if it is authorized, otherwise
return false and return a list of Eids which are not authorized.

=item isAuthorized(service)

Returns true if this Identity is valid. Valid does not
indicate that this Identity has been authorized. Use
hasEntitlementsForEids() to determine what (if anything)
entitlements an Identity has.

=item getSeatType

Returns the seat type for the identity

=back
