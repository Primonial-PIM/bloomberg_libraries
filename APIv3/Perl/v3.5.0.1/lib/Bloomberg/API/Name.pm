# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Internal;

use Scalar::Util;

package Bloomberg::API::Name;

use overload
    qw("") => \&string,
    qw(eq) => \&equal;

sub new
{
    my $class = shift;
    my ($arg) = @_;
    my $name_t = Bloomberg::API::Internal::blpapi_Name_create($arg."");
    my $self = {
        d_name_t => $name_t
    };

    bless $self, $class;
    return $self;
}

sub createInternally {
    my $class = shift;
    my ($arg) = @_;
    $arg->isa("_p_blpapi_Name") or die("Can be used Internally only");
    my ($self) = {
        d_name_t => $arg
        };
    bless $self, $class;
    return $self;
}

sub impl
{
    my $self = shift;
    return $self->{d_name_t};
}

# static function
sub findName
{
    my $class = shift;
    my ($string) = @_;
    my $name_t = Bloomberg::API::Internal::blpapi_Name_findName($string."");
    if( not defined $name_t ) {
        return undef;
    }
    my $self = {
        d_name_t => $name_t
    };

    bless $self, $class;
    return $self;

}

# static function
sub hasName
{
    my $class = shift;
    my ($string) = @_;
    my $name_t = Bloomberg::API::Internal::blpapi_Name_findName($string."");
    if( not defined $name_t ) {
        return 0;
    }
    return 1;
}

sub DESTROY
{

    my $self = shift;
    Bloomberg::API::Internal::blpapi_Name_destroy($self->{d_name_t});
}

sub clone
{
    my $self = shift;
    my $name_t = Bloomberg::API::Internal::blpapi_Name_duplicate($self->{d_name_t});
    my $anotherSelf = {
        d_name_t => $name_t,
    };
    bless $anotherSelf;
    return $anotherSelf;
}

sub length
{
    my $self = shift;
    my $len = Bloomberg::API::Internal::blpapi_Name_length($self->{d_name_t});
    return $len;
}

sub string
{
    my $self = shift;
    my $string = Bloomberg::API::Internal::blpapi_Name_string($self->{d_name_t});
    return $string;
}

sub equal
{
    my ($lhs, $rhs) = @_;
    my $rhsClass = Scalar::Util::blessed($rhs);
    if (defined $rhsClass && $rhs->isa("Bloomberg::API::Name")) {
        return Bloomberg::API::Internal::blpapi_internal_Name_equal(
                $lhs->{d_name_t}, $rhs->{d_name_t});
    }
    else {
        return $lhs->string() eq $rhs;
    }
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Name - a representation of a string for efficient comparison

=head1 DESCRIPTION

This component implements a representation of a string which is
efficient for string comparison.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item findName

If a Name already exists which matches the specified parameter it is returned.
Otherwise a Name which will compare equal to a Name created using the default
constructor is returned. The behavior is undefined if the specified parameter
is not of scalar type.

=item hasName

Returns true if a Name has been created which matches the specified parameter,
otherwise returns false. The behavior is undefined if the parameter is not of
scalar type.

=item new

Construct a Name from its parameter of scalar type.

=item createInternally

Construct a Name from its parameter of internal representation type.

=item impl

Get the internal implementation of this Name.

=item clone

Make a clone of an existing Name

=item length

Returns the length of the Names string representation (excluding a terminating
null).  Using name.length() is more efficient than getting the string and then
getting the length of the string.

=item operator "" / string

Return the underlying string for this Name.

=item operator eq / equal

Return true if the two Name objects have the same value, and false otherwise.
Two 'Name' objects have the same value if and only if the strings supplied when
they were created are the same.

=back
