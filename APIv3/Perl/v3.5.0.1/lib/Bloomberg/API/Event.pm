# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Exception;
use constant;

package Bloomberg::API::EventType;

use constant ADMIN => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_ADMIN;
use constant SESSION_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_SESSION_STATUS;
use constant SUBSCRIPTION_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_SUBSCRIPTION_STATUS;
use constant REQUEST_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_REQUEST_STATUS;
use constant RESPONSE => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_RESPONSE;
use constant PARTIAL_RESPONSE => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_PARTIAL_RESPONSE;
use constant SUBSCRIPTION_DATA => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_SUBSCRIPTION_DATA;
use constant SERVICE_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_SERVICE_STATUS;
use constant TIMEOUT => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_TIMEOUT;
use constant AUTHORIZATION_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_AUTHORIZATION_STATUS;
use constant RESOLUTION_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_RESOLUTION_STATUS;
use constant TOPIC_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_TOPIC_STATUS;
use constant TOKEN_STATUS => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_TOKEN_STATUS;
use constant REQUEST => $Bloomberg::API::Internal::BLPAPI_EVENTTYPE_REQUEST;

package Bloomberg::API::Event;

# this function cannot be created directly .. it needs to be created
# internally by the library
sub createInternally
{
    my $class = shift;
    my ($event) = @_;
    $event->isa("_p_blpapi_Event") or die("Can be created Internally only");
    my $self = {
        d_event_t => $event
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_Event_release($self->{d_event_t});
}

sub eventType
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Event_eventType($self->{d_event_t});
}

package Bloomberg::API::EventQueue;

sub new
{
    my $class = shift;
    my $eventQueue = Bloomberg::API::Internal::blpapi_EventQueue_create();
    my $self = {
        d_eventQueue_t => $eventQueue,
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    my $rcode = Bloomberg::API::Internal::blpapi_EventQueue_destroy(
                                                      $self->{d_eventQueue_t});
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub purge
{
    my $self = shift;
    my $rcode = Bloomberg::API::Internal::blpapi_EventQueue_purge(
                                                      $self->{d_eventQueue_t});
    Bloomberg::API::Exception->throwOnError($rcode);

}

sub nextEvent
{
    my $self = shift;
    my ($timeOut) = @_;
    $timeOut = 0 if (!defined $timeOut);
    my $eventHandle = Bloomberg::API::Internal::blpapi_EventQueue_nextEvent(
                                                       $self->{d_eventQueue_t},
                                                       $timeOut);
    if(not defined $eventHandle) {
        return undef;
    }
    return Bloomberg::API::Event->createInternally($eventHandle);

}

sub tryNextEvent
{
    my $self = shift;
    my ($eventHandle, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_EventQueue_tryNextEvent(
                                                      $self->{d_eventQueue_t});
    if($rcode) {
        return undef;
    }
    return Bloomberg::API::Event->createInternally($eventHandle);
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Event - Event related to subscription or request

=head1 DESCRIPTION

A single event resulting from a subscription or a request.

Event objects are created by the API and passed to the
application either through a EventQueue or returned from
the Session::nextEvent() method.
Event objects contain Message objects which can be
accessed using a MessageIterator.

The Event object is a handle to an event. The event is the
basic unit of work provided to applications. Each Event object
consists of an EventType attribute and zero or more Message
objects.


=head1 SUPPORTED OPERATIONS

The following operations are supported

=over 4

=item eventType

Returns the type of messages contained by this Event.

=back

=cut

=pod

=head1 NAME

Bloomberg::API::EventType - Possible types of event.

=head1 DESCRIPTION

The list of the constants that can be returned from
$event->eventType();

=head1 LIST

=over 4

ADMIN (Admin event)

SESSION_STATUS (Status updates for a session)

SUBSCRIPTION_STATUS (Status updates for a subscription)

REQUEST_STATUS (Status updates for a request)

RESPONSE (The final (possibly only) response to a request)

PARTIAL_RESPONSE (A partial response to a request)

SUBSCRIPTION_DATA (Data updates resulting from a subscription)

SERVICE_STATUS (Status updates for a service)

TIMEOUT (An Event returned from nextEvent() if it timed out)

AUTHORIZATION_STATUS (Status updates for user authorization)

RESOLUTION_STATUS (Status updates for a resolution operation)

TOPIC_STATUS (Status updates about topics for service providers)

TOKEN_STATUS (Status updates for a generate token request)

REQUEST (Request event)

UNKNOWN (Unknown event type)

=back

=cut

=pod

=head1 NAME

Bloomberg::API::EventQueue - Synchronous FIFO queue for events

=head1 DESCRIPTION

A construct used to handle replies to request.

An EventQueue can be supplied when using Session::sendRequest()
and Session::sendAuthorizationRequest() methods.

The EventQueue will only deliver responses to the request(s)
it is associated with.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new

Construct an empty event queue.

=item purge

Purge any Event objects in this EventQueue which have not
been processed and cancel any pending requests linked to
this EventQueue. The EventQueue can subsequently be
re-used for a subsequent request.


=item nextEvent($timeout = 0)

Returns the next Event available from the EventQueue. If
the specified 'timeout' is zero this will wait forever for
the next event. If the specified 'timeout' is non zero then
if no Event is available within the specified 'timeout' an
Event with a type() of TIMEOUT will be returned.

By default the 'timeout' is set to 0.

=item tryNextEvent

If the EventQueue is non-empty, return the next Event available.
If the EventQueue is empty, return an undef. This method never blocks.

=back

=cut

