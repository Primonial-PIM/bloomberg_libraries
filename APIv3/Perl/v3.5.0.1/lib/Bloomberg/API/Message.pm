# Copyright (C) 2010-2012 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::CorrelationId;
use Bloomberg::API::Element;
use Bloomberg::API::Internal;
use Bloomberg::API::Name;

package Bloomberg::API::Message::FragmentType;

use constant FRAGMENT_NONE => 0;
use constant FRAGMENT_START => 1;
use constant FRAGMENT_INTERMEDIATE => 2;
use constant FRAGMENT_END => 3;

package Bloomberg::API::Message;
our @ISA = qw(Bloomberg::API::Element);

sub messageType
{
    my $self = shift;
    my $name_t = Bloomberg::API::Internal::blpapi_Message_messageType(
                                                         $self->{d_message_t});
    return Bloomberg::API::Name->createInternally($name_t) if defined $name_t;
}

sub topicName
{
    my $self = shift;
    my $name_string = Bloomberg::API::Internal::blpapi_Message_topicName(
                                                         $self->{d_message_t});
    return $name_string;
}

sub correlationIds
{
    my $self = shift;
    my @correlationIdList = ();
    my $numCorrelationId = Bloomberg::API::Internal::blpapi_Message_numCorrelationIds(
        $self->{d_message_t});

    for my $i (0..$numCorrelationId - 1) {
        my $cid = Bloomberg::API::CorrelationId->new();
        Bloomberg::API::Internal::blpapi_internal_Message_correlationId(
            $cid->{d_correlationId_t},
            $self->{d_message_t},
            $i);
        push(@correlationIdList, $cid);
    }
    return @correlationIdList;
}

sub asElement
{
    my $self = shift;
    my $element_t = Bloomberg::API::Internal::blpapi_Message_elements(
        $self->{d_message_t});
    return Bloomberg::API::Element->createInternally($element_t) if defined $element_t;
}

sub getPrivateData
{
    my $self = shift;
    my ($privateData, $size) = Bloomberg::API::Internal::blpapi_Message_privateData(
        $self->{d_message_t});
    return $privateData;
}

sub fragmentType
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my $fragmentType = Bloomberg::API::Internal::blpapi_internal_Message_fragmentType(
        $self->{d_message_t});
    return $fragmentType;
}

sub createInternally
{
    my $class = shift;
    my ($message) = @_;
    $message->isa("_p_blpapi_Message_t") or
        die("Can be created Internally only");

    # adding a ref, any message that is returned is copy-able
    if (Bloomberg::API::Exception->checkVersion(30406)) {
        Bloomberg::API::Internal::blpapi_internal_Message_addRef($message);
    }

    my $element_t = Bloomberg::API::Internal::blpapi_Message_elements(
        $message);
    my ($self) = {
        d_message_t => $message,
        d_element_t => $element_t
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    if (Bloomberg::API::Exception->checkVersion(30406)) {
        Bloomberg::API::Internal::blpapi_internal_Message_release(
                                                $self->{d_message_t});
    }
}

sub impl
{
    my $self = shift;
    return $self->{d_message_t};
}

sub print
{
    my $self = shift;
    my ($fileHandle) = @_;
    $fileHandle = \*STDOUT if not defined $fileHandle;
    $self->asElement()->print($fileHandle);
}

package Bloomberg::API::MessageIterator;

sub new
{
    my $class = shift;
    my ($event) = @_;
    $event->isa("Bloomberg::API::Event") or die("Can be created on an Event only");
    my $messageIter = Bloomberg::API::Internal::blpapi_MessageIterator_create(
                                                          $event->{d_event_t});
    my ($self) = {
        d_messsageIterator_t => $messageIter,
        d_currentMessage => undef
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_MessageIterator_destroy(
                                                $self->{d_messsageIterator_t});
}

sub next
{
    my $self = shift;
    my $message_t = Bloomberg::API::Internal::blpapi_internal_MessageIterator_next(
                                                $self->{d_messsageIterator_t});
    if (defined $message_t) {
        my $message = Bloomberg::API::Message->createInternally($message_t);
        $self->{d_currentMessage} = $message;
        return $message;
    }
    return undef;
}

sub message
{
    my $self = shift;
    return $self->{d_currentMessage};
}

1;

__END__;

=pod

=head1 NAME

Bloomberg::API::Message - Defines a message containing elements inside an event.

=head1 DESCRIPTION

A handle to a single message.

Message objects are obtained from a MessageIterator. Each
Message is associated with a Service and with one or more
CorrelationId values. The Message contents are represented as
an Element and some convenient shortcuts are supplied to the
Element accessors.

A Message is a handle to a single underlying protocol
message.
For ease of use all the methods exported by elements can
also be used for Message. For example

..
    $message->getElementAsString("string");
    # instead of $message->asElement()->getElementAsString("string");
..

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item messsageType

Returns the type of this message.

=item topicName

Returns a string containing the topic string associated with this message.
If there is no topic associated with this message then an empty string
is returned.

=item correlationIds

Returns the list of CorrelationIds associated with this
message.

Note: A Message will have exactly one CorrelationId unless
'allowMultipleCorrelatorsPerMsg' option was enabled for the
Session this Message belongs to. When
'allowMultipleCorrelatorsPerMsg' is disabled (the default)
and more than one active subscription would result in the
same Message the Message is delivered multiple times
(without making physical copies). Each Message is
accompanied by a single CorrelationId. When
'allowMultipleCorrelatorsPerMsg' is enabled and more than
one active subscription would result in the same Message
the Message is delivered once with a list of corresponding
CorrelationId values.

=item asElement

Returns the content of this Message as a Element.

=item getPrivateData

Returns the private data associated with the message. If the message
has no private data undef is returned.

=item print

Prints the message to a file stream. If no file stream is passed,
the output is written to STDOUT.
The file descriptor is passed as a reference.
    For example,

..
    open ($fd, '>', 'fileTest.txt');
    $message = $messageIterator->next();
    $message->print($fd);
..

=item fragmentType

Call to determine if a message is a fragment of a larger message. Most
messages won't be fragmented so the above method will return FRAGMENT_NONE.
Consuming application should be prepared to receive a FRAGMENT_START before
receiving FRAGMENT_END in which case previously received message fragments
(start and any intermediate fragments) should be discarded.

=back

=head1 NAME

Bloomberg::API::MessageIterator - An iterator over the Messages within an Event

=head1 DESCRIPTION

An iterator over the Message objects within an Event.

MessageIterator objects are used to process the individual
Message objects in an Event received in an EventHandler, from
EventQueue::nextEvent() or from Session::nextEvent().

This class is used to iterate over each message in an
Event. The user must ensure that the Event this iterator is
created for is not destroyed before the iterator.


=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new

Construct a forward iterator to iterate over the message in
the specified 'event' object. The MessageIterator is
created in a state where the first message cannot be extracted
immediately. It takes in Bloomberg::API::Event as a parameter.

=item next

Attempts to advance this MessageIterator to the next
Message in this Event. Returns the next message on success and undef if
there are no more messages.

=item message

Returns the Message at the current position of this
iterator. Returns undef if the iterator has passed the last element.

=back

=cut

