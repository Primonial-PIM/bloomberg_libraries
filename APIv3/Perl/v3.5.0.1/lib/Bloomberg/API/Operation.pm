# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::Operation; 

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Exception;
use Bloomberg::API::Schema;

sub createInternally
{
    my $class = shift;
    my ($operation_t) = @_;
    $operation_t->isa("_p_blpapi_Operation") or
        die("Only able to create using _p_blpapi_Operation");
    my $self = {
        d_operation_t => $operation_t,
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
}

sub name
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Operation_name($self->{d_operation_t});
}

sub description 
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Operation_description($self->{d_operation_t});
}

sub numResponseDefinitions 
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Operation_numResponseDefinitions(
            $self->{d_operation_t});
}

sub handle
{
    my $self = shift;
    return $self->{d_operation_t};
}

sub isValid
{
    my $self = shift;
    return 1 if defined $self->{d_operation_t};
    return 0;
}

sub requestDefinition 
{
    my $self = shift;
    my ($definition, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Operation_requestDefinition(
                $self->{d_operation_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return
        Bloomberg::API::SchemaElementDefinition->createInternally($definition);
}

sub responseDefinition
{
    my $self = shift;
    my ($index) = @_;
    my ($definition, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Operation_responseDefinition(
                $self->{d_operation_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return
        Bloomberg::API::SchemaElementDefinition->createInternally($definition);
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Operation - Defines an operation which can be performed by a Service.

=head1 DESCRIPTION

Operation objects are obtained from a Service object. They provide read-only
access to the schema of the Operations Request and the schema of the possible
response.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

Returns a string which contains the name of this operation.

=item description

Returns a string which contains a human readable description of this
Operation.

=item isValid

Return true if this 'Operation' object is valid, false otherwise.

=item requestDefinition

Returns a SchemaElementDefinition which defines the schema for this Operation.

=item numResponseDefinitions

Returns the number of the response types that can be returned by this
Operation.

=item responseDefinition

Returns a SchemaElementDefinition which defines the schema for the response
that this Operation delivers.

=back
