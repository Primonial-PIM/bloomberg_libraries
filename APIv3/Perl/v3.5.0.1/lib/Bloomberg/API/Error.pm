# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;
package Bloomberg::API::Error;

use constant BLPAPI_UNKNOWN_CLASS                 => 0x00000 | 0;
use constant BLPAPI_INVALIDSTATE_CLASS            => 0x10000;
use constant BLPAPI_INVALIDARG_CLASS              => 0x20000;
use constant BLPAPI_IOERROR_CLASS                 => 0x30000;
use constant BLPAPI_CNVERROR_CLASS                => 0x40000;
use constant BLPAPI_BOUNDSERROR_CLASS             => 0x50000;
use constant BLPAPI_NOTFOUND_CLASS                => 0x60000;
use constant BLPAPI_FLDNOTFOUND_CLASS             => 0x70000;
use constant BLPAPI_UNSUPPORTED_CLASS             => 0x80000;

use constant BLPAPI_ERROR_UNKNOWN                 => (BLPAPI_UNKNOWN_CLASS | 1);
use constant BLPAPI_ERROR_ILLEGAL_ARG             => (BLPAPI_INVALIDARG_CLASS | 2);
use constant BLPAPI_ERROR_ILLEGAL_ACCESS          => 3;
use constant BLPAPI_ERROR_INVALID_SESSION         => (BLPAPI_INVALIDARG_CLASS | 4);
use constant BLPAPI_ERROR_DUPLICATE_CORRELATIONID => (BLPAPI_INVALIDARG_CLASS | 5);
use constant BLPAPI_ERROR_INTERNAL_ERROR          => (BLPAPI_UNKNOWN_CLASS | 6);
use constant BLPAPI_ERROR_RESOLVE_FAILED          => (BLPAPI_IOERROR_CLASS | 7);
use constant BLPAPI_ERROR_CONNECT_FAILED          => (BLPAPI_IOERROR_CLASS | 8);
use constant BLPAPI_ERROR_ILLEGAL_STATE           => (BLPAPI_INVALIDARG_CLASS | 9);
use constant BLPAPI_ERROR_CODEC_FAILURE           => 10;
use constant BLPAPI_ERROR_INDEX_OUT_OF_RANGE      => (BLPAPI_BOUNDSERROR_CLASS | 11);
use constant BLPAPI_ERROR_INVALID_CONVERSION      => (BLPAPI_CNVERROR_CLASS | 12);
use constant BLPAPI_ERROR_ITEM_NOT_FOUND          => (BLPAPI_NOTFOUND_CLASS | 13);
use constant BLPAPI_ERROR_IO_ERROR                => (BLPAPI_IOERROR_CLASS | 14);
use constant BLPAPI_ERROR_CORRELATION_NOT_FOUND   => (BLPAPI_NOTFOUND_CLASS | 15);
use constant BLPAPI_ERROR_SERVICE_NOT_FOUND       => (BLPAPI_NOTFOUND_CLASS | 16);
use constant BLPAPI_ERROR_LOGON_LOOKUP_FAILED     => (BLPAPI_UNKNOWN_CLASS | 17);
use constant BLPAPI_ERROR_DS_LOOKUP_FAILED        => (BLPAPI_UNKNOWN_CLASS | 18);
use constant BLPAPI_ERROR_UNSUPPORTED_OPERATION   => (BLPAPI_UNSUPPORTED_CLASS | 19);
use constant BLPAPI_ERROR_DS_PROPERTY_NOT_FOUND   => (BLPAPI_NOTFOUND_CLASS | 20);


sub BLPAPI_RESULTCODE
{
    my ($res) = @_;
    my $mask = 0xffff;
    return ($res & $mask) if (defined $res);
}

sub BLPAPI_RESULTCLASS
{
    my ($res) = @_;
    return ($res & 0xff0000) if (defined $res);
}


1;

__END__

=pod

=head1 NAME

Bloomberg::API::Error -  A collection of errors that library can generate.
