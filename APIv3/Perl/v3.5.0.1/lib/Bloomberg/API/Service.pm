# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::Service;

use strict;
use warnings;

use Bloomberg::API::Exception;
use Bloomberg::API::Internal;
use Bloomberg::API::Operation;
use Bloomberg::API::Request;
use Bloomberg::API::Schema;
use Bloomberg::API::Name;
use Bloomberg::API::Event;

use Scalar::Util;

use overload
    "=" => \&assign;

sub createInternally
{
    my $class = shift;
    my ($service_t) = @_;
    $service_t->isa("_p_blpapi_Service") or
        die("Only able to create using _p_blpapi_Service");
    my $self = {
        d_service_t => $service_t,
    };
    Bloomberg::API::Internal::blpapi_Service_addRef($service_t);
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    if(defined $self->{d_service_t}) {
        Bloomberg::API::Internal::blpapi_Service_release($self->{d_service_t});
    }
}

sub assign
{
    my ($lhs, $rhs) = @_;
    if ($lhs->{d_service_t} != $rhs->{d_service_t}) {
        if(defined $lhs->{d_service_t}) {
            Bloomberg::API::Internal::blpapi_Service_release($lhs->{d_service_t});
        }
        $lhs->{d_service_t} = $rhs->{d_service_t};
        if(defined $lhs->{d_service_t}) {
            Bloomberg::API::Internal::blpapi_Service_addRef($lhs->{d_service_t});
        }
    }
    return $lhs;
}

sub name
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Service_name($self->{d_service_t});
}

sub description
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Service_description($self->{d_service_t});
}

sub numOperations
{
    my $self = shift;
    return
        Bloomberg::API::Internal::blpapi_Service_numOperations($self->{d_service_t});
}

sub numEventDefinitions
{
    my $self = shift;
    return
        Bloomberg::API::Internal::blpapi_Service_numEventDefinitions($self->{d_service_t});
}

sub hasOperation
{
    my $self = shift;
    my ($arg) = @_;
    my ($operation, $rcode);
    if ($arg->isa("Bloomberg::API::Name")) {
        ($operation, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getOperationByName(
                $self->{d_service_t}, $arg->impl());
    }
    else {
        ($operation, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getOperationByString(
                $self->{d_service_t}, $arg);
    }
    return 1 if defined $operation;
}

sub getOperation
{
    my $self = shift;
    my ($arg) = @_;
    my $nameClass = Scalar::Util::blessed($arg);
    my ($operation_t, $rcode);
    if (defined $nameClass && $arg->isa("Bloomberg::API::Name")) {
        ($operation_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getOperationByName(
                $self->{d_service_t}, $arg->{d_name_t});
    }
    else {
        ($operation_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getOperationByString(
                $self->{d_service_t}, $arg."");
    }
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Operation->createInternally($operation_t);
}

sub getOperationAt
{
    my $self = shift;
    my ($index) = @_;
    my ($operation, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getOperationAt(
                $self->{d_service_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Operation->createInternally($operation);
}

sub hasEventDefinition
{
    my $self = shift;
    my ($arg) = @_;
    my ($eventDefinition_t, $rcode);

    if ($arg->isa("Bloomberg::API::Name")) {
        ($eventDefinition_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getEventDefinitionByName(
                $self->{d_service_t}, $arg->impl());
    }
    else {
        ($eventDefinition_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getEventDefinitionByString(
                $self->{d_service_t}, $arg);
    }
    return 1 if defined $eventDefinition_t;
    return 0;
}

sub getEventDefinition
{
    my $self = shift;
    my ($arg) = @_;
    my ($eventDefinition_t, $rcode);
    my $nameClass = Scalar::Util::blessed($arg);
    if (defined $nameClass && $arg->isa("Bloomberg::API::Name")) {
        ($eventDefinition_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getEventDefinitionByName(
                $self->{d_service_t}, $arg->impl());
    }
    else {
        ($eventDefinition_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getEventDefinitionByString(
                $self->{d_service_t}, $arg);
    }
    Bloomberg::API::Exception->throwOnError($rcode);

    return Bloomberg::API::SchemaElementDefinition->createInternally($eventDefinition_t);
}

sub getEventDefinitionAt
{
    my $self = shift;
    my ($index) = @_;
    my ($eventDefinition_t, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Service_getEventDefinitionAt(
                $self->{d_service_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return
        Bloomberg::API::SchemaElementDefinition->createInternally($eventDefinition_t);
}

sub authorizationServiceName
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Service_authorizationServiceName(
            $self->{d_service_t});
}

sub print
{
    my $self = shift;
    my ($fileHandle) = @_;
    $fileHandle = \*STDOUT if not defined $fileHandle;
    Bloomberg::API::Internal::blpapi_internal_print_Service(
        $self->{d_service_t}, 0, 4, $fileHandle);
}

sub createRequest
{
    my $self = shift;
    my ($operation) = @_;
    my ($request_t, $rcode) = Bloomberg::API::Internal::blpapi_internal_Service_createRequest(
            $self->{d_service_t}, $operation);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Request->createInternally($request_t);
}

sub createAuthorizationRequest
{
    my $self = shift;
    my ($operation) = @_;
    my ($request_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Service_createAuthorizationRequest(
            $self->{d_service_t}, $operation);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Request->createInternally($request_t);
}

sub createPublishEvent
{
    my $self = shift;
    my ($event_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Service_createPublishEvent(
            $self->{d_service_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Event->createInternally($event_t);
}

sub createAdminEvent
{
    my $self = shift;
    my ($event_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Service_createAdminEvent(
            $self->{d_service_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return $event_t;
}

sub createResponseEvent
{
    my $self = shift;
    my ($cid) = @_;
    my ($event_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Service_createResponseEvent(
                $self->{d_service_t}, $cid->{d_correlationId_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Event->createInternally($event_t);
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Service - Defines a service which provides access to API data.

=head1 DESCRIPTION

A Service object is obtained from a Session and contains the Operations
(each of which contains its own schema) and the schema for Events which this
Service may produce. A Service object is also used to create Request objects
used with a Session to issue requests.

All API data is associated with a service. Before accessing API data using
either request-reply or subscription, the appropriate Service must be opened
and, if necessary, authorized.

Provider services are created to generate API data and must be registered
before use.

The Service object is a handle to the underlying data which is owned by the
Session. Once a Service has been successfully opened in a Session it remains
accessible until the Session is terminated.

Operation objects are obtained from a Service object. They provide read-only
access to the schema of the Operations Request and the schema of the possible
response.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

Returns a string which contains the name of this Service.

=item description

Returns a string which contains a human-readable description of this Service.

=item numOperations

Returns the number of Operations defined by this Service.

=item numEventDefinitions

Returns the number of unsolicited events defined by this Service.

=item hasOperation

  hasOperation($name)
  hasOperation($nameString)

Returns true if the specified 'name' or 'nameString' identifies a valid Operation
in this Service. Otherwise returns false.

=item getOperation

  getOperation($name)
  getOperation($nameString)

Return the definition of the Operation identified by the specified 'name' or
'nameString. If this Service does not define an operation 'name' or
'nameString' an exception is thrown.

=item getOperationAt

  getOperationAt($index)

Returns the specified 'index'th Operation in this Service. If
'index'>=numOperations() then an exception is thrown.

=item hasEventDefinition

  hasEventDefinition($name)
  hasEventDefinition($nameString)

Returns true if the specified 'name' or 'nameString' identifies a valid event
in this Service.

=item getEventDefinition

  getEventDefinition($name)
  getEventDefinition($nameString)

Return the SchemaElementDefinition of the unsolicited event
defined by this Service identified by the specified
'name'. If this Service does not define an unsolicited
event 'name' an exception is thrown.

=item getEventDefinitionAt

getEventDefinitionAt($index)

Returns the SchemaElementDefinition of the specified 'index'th unsolicited
event defined by this service. If 'index'>=numEventDefinitions() an exception
is thrown.

=item authorizationServiceName

Returns the name of the Service which must be used in order  to authorize
access to restricted operations on this Service. If no authorization is
required to access operations on this service an empty string is returned.
Authorization services never require authorization to use.

=item print

  print($fileHandle)

Format this Service schema to the specified output 'fileHandle'. If no
'fileHandle' is specified, standard output will be used.

=item createRequest

  createRequest($operation)

Returns a empty Request object for the specified 'operation'. If 'operation'
does not identify a valid operation in the Service then an exception is thrown.

An application must populate the Request before issuing it using
Session::sendRequest().

=item createAuthorizationRequest

  createAuthorizationRequest($authorizationOperation)

Returns an empty Request object for the specified 'authorizationOperation'. If
the 'authorizationOperation' does not indentify a valid operation for this
Service then an exception is thrown.

An application must populate the Request before issuing it using
Session::sendAuthorizationRequest().

=item createPublishEvent

Create an Event suitable for publishing to this Service. Use an EventFormatter
to add Messages to the Event and set fields.

=item createAdminEvent

Create an Admin Event suitable for publishing to this Service. Use an
EventFormatter to add Messages to the Event and set fields.

=item createResponseEvent

Create a response Event to answer the request. Use an EventFormatter to add a
Message to the Event and set fields.

=item = \ assign

Assignment operator.

=back
