# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::ResolutionList;

use strict;
use warnings;

use Bloomberg::API::CorrelationId;
use Bloomberg::API::Exception;
use Bloomberg::API::Internal;
use Bloomberg::API::Message;

use constant NOT_CREATE => 0;
use constant CREATED => 1;
use constant FAILURE => 2;

sub new
{
    my $class = shift;
    my $resolutionList_t = Bloomberg::API::Internal::blpapi_ResolutionList_create(undef);
    my $self = {
        d_resolutionList_t => $resolutionList_t
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_ResolutionList_destroy($self->{d_resolutionList_t});
}

sub clone
{
    my $self = shift;
    my $resolutionList_t =
        Bloomberg::API::Internal::blpapi_ResolutionList_create($self->{d_resolutionList_t});
    my $copy = {
        d_resolutionList_t => $resolutionList_t
    };
    bless $copy;
    return $copy;
}

sub add
{
    my ($self, $secondArg, $cid) = @_;
    if (!defined $cid) {
        $cid = Bloomberg::API::CorrelationId->new();
    }
    my $argClass = Scalar::Util::blessed($secondArg);
    if ((defined $argClass) && ($secondArg->isa("Bloomberg::API::Message"))) {
        Bloomberg::API::Internal::blpapi_ResolutionList_addFromMessage(
                $self->{d_resolutionList_t},
                $secondArg->impl(), 
                $cid->impl());
    }
    else {
        return Bloomberg::API::Internal::blpapi_ResolutionList_add(
                $self->{d_resolutionList_t},
                $secondArg."", 
                $cid->impl());
    }
}

sub correlationIdAt
{
    my $self = shift;
    my ($index) = @_;
    my $cid_t = Bloomberg::API::Internal::blpapi_CorrelationId_t->new();
    my $rcode =
        Bloomberg::API::Internal::blpapi_ResolutionList_correlationIdAt(
                $self->{d_resolutionList_t},
                $cid_t, 
                $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::CorrelationId->createInternally($cid_t);
}

sub topicString
{
    my $self = shift;
    my ($cid) = @_;
    my ($topic, $rcode) = Bloomberg::API::Internal::blpapi_internal_ResolutionList_topicString(
            $self->{d_resolutionList_t},
            $cid->impl());
    Bloomberg::API::Exception->throwOnError($rcode);
    return $topic;
}

sub topicStringAt
{
    my $self = shift;
    my ($index) = @_;
    my ($topic, $rcode) = Bloomberg::API::Internal::blpapi_internal_ResolutionList_topicStringAt(
                $self->{d_resolutionList_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $topic;
}

sub status
{
    my $self = shift;
    my ($cid) = @_;
    my ($result, $rcode) = Bloomberg::API::Internal::blpapi_internal_ResolutionList_status(
            $self->{d_resolutionList_t}, $cid->impl());
    Bloomberg::API::Exception->throwOnError($rcode);
    return $result;
}

sub statusAt
{
    my $self = shift;
    my ($index) = @_;
    my ($result, $rcode) = Bloomberg::API::Internal::blpapi_internal_ResolutionList_statusAt(
            $self->{d_resolutionList_t}, $index); 
    Bloomberg::API::Exception->throwOnError($rcode);
    return $result;
}

sub message
{
    my $self = shift;
    my ($cid) = @_;
    my ($message, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_ResolutionList_message(
                $self->{d_resolutionList_t}, $cid->impl());
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Message->createInternally($message);
}

sub messageAt
{
    my $self = shift;
    my ($index) = @_;
    my ($message, $rcode) = Bloomberg::API::Internal::blpapi_internal_ResolutionList_messageAt(
            $self->{d_resolutionList_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Message->createInternally($message);
}

sub size
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_ResolutionList_size($self->{d_resolutionList_t});
}

sub impl
{
    my $self = shift;
    return $self->{d_resolutionList_t};
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::ResolutionList - Contains a list of topics which require resolution.

=head1 DESCRIPTION

Created from topic strings or from SUBSCRIPTION_STARTED messages.
This is passed to a resolve() call or resolveAsync() call on a
ProviderSession. It is updated and returned by the resolve() call.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new

Create an empty ResolutionList.

=item clone

Copy constructor.

=item add

  add($topic, $correlationId)
  add($subscriptionStartedMessage, $correlationId)

Add to this list the specified 'topic' string or the topic contained in the
specified 'subscriptionStartedMessage', optionally specifying a
'correlationId'. Returns 0 on success or negative number on failure.
After a successful call to add() the status for this entry is UNRESOLVED_TOPIC.

=item correlationIdAt

  correlationIdAt($index)

Returns the CorrelationId of the specified 'index'th entry in this
ResolutionList. If 'index' >= size() an exception is thrown.

=item topicString

  topicString($correlationId)

Returns a string for the topic of the entry identified by the specified
'correlationId'. If the 'correlationId' does not identify an entry in this
ResolutionList then an exception is thrown.

=item topicStringAt

Returns a string for the topic of the specified 'index'th entry. If 
'index' >= size() an exception is thrown.

=item status

  status($correlationId)

Returns the status of the entry in this ResolutionList identified by the
specified 'correlationId'. This may be 
UNRESOLVED, RESOLVED, RESOLUTION_FAILURE_BAD_SERVICE,
RESOLUTION_FAILURE_SERVICE_AUTHORIZATION_FAILED
RESOLUTION_FAILURE_BAD_TOPIC,
RESOLUTION_FAILURE_TOPIC_AUTHORIZATION_FAILED.  If the 'correlationId' does not
identify an entry in this  ResolutionList then an exception is thrown.

=item statusAt

  statusAt($index)

Returns the status of the specified 'index'th entry in this
ResolutionList. This may be UNRESOLVED,
RESOLVED, RESOLUTION_FAILURE_BAD_SERVICE,
RESOLUTION_FAILURE_SERVICE_AUTHORIZATION_FAILED
RESOLUTION_FAILURE_BAD_TOPIC,
RESOLUTION_FAILURE_TOPIC_AUTHORIZATION_FAILED.  If 'index'
> size() an exception is thrown.

=item message

  message($correlationId)

Returns the value of the message received during resolution of the topic
identified by the specified 'correlationId'. If 'correlationId' does not
identify an entry in this ResolutionList or if the status of the entry
identify by 'correlationId' is not RESOLVED an exception is thrown.

The message returned can be used when creating an instance of Topic.

=item messageAt

  messageAt($index)

Returns the value of the message received during resolution of the specified
'index'th entry in this ResolutionList. If 'index' >= size() or if the status
of the 'index'th entry is not RESOLVED an exception is thrown.

The message returned can be used when creating an instance of Topic.

=item size

Returns the number of entries in this list.

=back
