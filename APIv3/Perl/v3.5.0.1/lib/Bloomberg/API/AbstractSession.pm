# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::CorrelationId;
use Bloomberg::API::Error;
use Bloomberg::API::Event;
use Bloomberg::API::Exception;
use Bloomberg::API::Identity;
use Bloomberg::API::Internal;
use Bloomberg::API::Request;
use Bloomberg::API::Service;

package Bloomberg::API::AbstractSession;

sub cancel
{
    my $self = shift;
    my ($correlationIdListRef) = @_;
    my @correlationIdList = @$correlationIdListRef;
    my $correlationIdListType =
        Bloomberg::API::Internal::blpapi_internal_CorrelationIdArray_create(
            scalar(@correlationIdList));

    for my $i (0..scalar(@correlationIdList)-1) {
        Bloomberg::API::Internal::blpapi_internal_CorrelationIdArray_set(
            $correlationIdListType,
            $correlationIdList[$i]->{d_correlationId_t},
            $i);
    }

    my $cidarray =
        Bloomberg::API::Internal::blpapi_internal_CorrelationIdArray_getCorrelationIds(
            $correlationIdListType);

    my $rcode = Bloomberg::API::Internal::blpapi_AbstractSession_cancel(
        $self->{d_abstractsession_t},
        $cidarray,
        scalar(@correlationIdList),
        undef,
        0);
    Bloomberg::API::Internal::blpapi_internal_CorrelationIdArray_destroy(
        $correlationIdListType);
    return Bloomberg::API::Exception->throwOnError($rcode);
}

# sendAuthorizationRequest($authorizationRequest, $identity, $correlationId,
#                                                                  $eventQueue)
sub sendAuthorizationRequest
{
    my $self = shift;
    my ($authorizationRequest, $identity, $cid, $eventQueue) = @_;
    if (defined $authorizationRequest && defined $identity) {
        if (!defined $cid) {
            $cid = Bloomberg::API::CorrelationId->new();
        }

        my $requestLabel = undef;
        my $requestLabelLen = 0;
        my $rcode =
            Bloomberg::API::Internal::blpapi_AbstractSession_sendAuthorizationRequest(
                    $self->{d_abstractsession_t},
                    $authorizationRequest->{d_request_t},
                    $identity->{d_identity_t},
                    $cid->{d_correlationId_t},
                    defined $eventQueue ? $eventQueue->{d_eventQueue_t} : undef,
                    $requestLabel,
                    $requestLabelLen);

        Bloomberg::API::Exception->throwOnError($rcode);
        return $cid;
    }
    # Illegal args
    return Bloomberg::API::Exception->throwException(
        Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS,
        "Need atleast the Request as args for sendAuthorizationRequest");
}


sub openService
{
    my $self = shift;
    my ($serviceName) = @_;

    my $rcode = Bloomberg::API::Internal::blpapi_AbstractSession_openService(
        $self->{d_abstractsession_t},
        $serviceName);
    return !($rcode);

}

sub openServiceAsync
{
    my $self = shift;
    my ($serviceName, $correlationId) = @_;
    if (not defined $correlationId) {
        $correlationId = Bloomberg::API::CorrelationId->new();
    }
    my $rcode = Bloomberg::API::Internal::blpapi_AbstractSession_openServiceAsync(
        $self->{d_abstractsession_t},
        $serviceName,
        $correlationId->{d_correlationId_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return $correlationId;

}

# generateToken($correlationId, $eventQueue)
sub generateToken
{
    my $self = shift;
    my ($cid, $eventQueue) = @_;
    if (!defined $cid) {
        $cid = Bloomberg::API::CorrelationId->new();
    }
    my $rcode = Bloomberg::API::Internal::blpapi_AbstractSession_generateToken(
        $self->{d_abstractsession_t},
        $cid->{d_correlationId_t},
        defined $eventQueue ? $eventQueue->{d_eventQueue_t} : undef);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $cid;
}

sub getService
{
    my $self = shift;
    my ($serviceName) = @_;

    my ($service_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_AbstractSession_getService(
            $self->{d_abstractsession_t},
            $serviceName);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Service->createInternally($service_t);
}

sub createIdentity
{
    my $self = shift;
    my $identity_t = Bloomberg::API::Internal::blpapi_AbstractSession_createIdentity(
        $self->{d_abstractsession_t});
    if(not defined $identity_t ) {
        return undef;
    }
    return Bloomberg::API::Identity->createInternally($identity_t);
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::AbstractSession - Interface shared between publish and consumer session

=head1 DESCRIPTION

This class provides an abstract session which defines shared interface
between publish and consumer requests for Bloomberg

Sessions manage access to services either by requests and
responses or subscriptions. A Session can dispatch events and
replies in either a synchronous or asynchronous mode. The mode
of a Session is determined when it is constructed and cannot be
changed subsequently.

The nextEvent() method must be called to read incoming events.

Several methods in a Session take a CorrelationId parameter. The
application may choose to supply its own CorrelationId values
or allow the Session to create values. If the application
supplies its own CorrelationId values it must manage their
lifetime such that the same value is not reused for more than
one operation at a time.

The lifetime of a CorrelationId begins
when it is supplied in a method invoked on a Session and ends
either when it is explicitly cancelled using cancel() or
unsubscribe(), when a RESPONSE Event (not a PARTIAL_RESPONSE)
containing it is received or when a SUBSCRIPTION_STATUS Event
which indicates that the subscription it refers to has been
terminated is received.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item cancel([correlationIds])

For each value in the specified 'correlationIds' which
identifies a current request, cancel that request. Any
values in the specified 'correlationIds' which do not
identify a current request are ignored.

Once this call returns, the specified 'correlationIds' will
not be seen in any subsequent Message obtained from a
MessageIterator by calling next(). However, any Message
currently pointed to by a MessageIterator when
cancel() is called is not affected even if it has one
of the specified 'correlationIds'. Also any Message where a
reference has been retained by the application may still
contain one of the 'correlationIds'. For these reasons,
although technically an application is free to re-use any
of the 'correlationIds' as soon as this method returns, it
is preferable not to aggressively re-use correlation IDs.

=item sendAuthorizationRequest

Send the specified 'authorizationRequest' and update the
specified 'identity' with the results. If the optionally
specified 'correlationId' is supplied, it is used; otherwise create
a CorrelationId. The actual CorrelationId used is
returned. If the optionally specified 'eventQueue' is
supplied all Events relating to this Request will arrive on
that EventQueue.

The underlying user information must remain valid until the
Request has completed successfully or failed.

A successful request will generate zero or more
PARTIAL_RESPONSE Messages followed by exactly one RESPONSE
Message. Once the final RESPONSE Message has been received
the specified 'identity' will have been updated to contain
the users entitlement information and the CorrelationId
associated with this request may be re-used. If the request
fails at any stage a REQUEST_STATUS will be generated, the
specified 'identity' will not be modified and the
CorrelationId may be re-used.

The 'identity' supplied must have been returned from this
Session's createIdentity() method. For example

..
    my $handle = session.createIdentity();
    session->sendAuthorizationRequest($authRequest, $handle);

..

=item openService("uri")

Attempt to open the service identified by the specified
'uri' and block until the service is either opened
successfully or has failed to be opened. Return 'true' if
the service is opened successfully and 'false' if the
service cannot be successfully opened.

The 'uri' must contain a fully qualified service name. That
is, it must be of the form "//<namespace>/<service-name>".

Before openService() returns, a SERVICE_STATUS Event is
generated.

=item openServiceAsync($serviceName, $correlationId)

Begin the process to open the service identified by the
specified 'uri' and return immediately. The optional
specified 'correlationId' is used to track Events generated
as a result of this call. The actual correlationId which
will identify Events generated as a result of this call is
returned.

The 'uri' must contain a fully qualified service name. That
is, it must be of the form "//<namespace>/<service-name>".

The application must monitor events for a SERVICE_STATUS
Event which will be generated once the service has been
successfully opened or the opening has failed.

=item generateToken

Generate a token to be used for authorization.
If an invalid authentication option is specified in session option or
there is failure to get authentication information based on
authentication option, then an InvalidArgumentException is thrown.

=item getService($serviceName)

Return a Service object representing the service
identified by the specified 'uri'

The 'uri' must contain a fully qualified service name. That
is, it must be of the form "//<namespace>/<service-name>".

If the service identified by the specified 'uri' is not
open already then an InvalidStateException is thrown.


=item createIdentity

Return a Identity which has not been authorized.

=back

