# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::Constant;

use strict;
use warnings;

use Bloomberg::API::Datetime;
use Bloomberg::API::Exception;
use Bloomberg::API::Internal;
use Bloomberg::API::Name;

# private function
sub createInternally
{
    my $class = shift;
    my ($arg) = @_;

    $arg->isa("_p_blpapi_Constant") or
        die("Can be created with _p_blpapi_Constant only");
    my $self = {
        d_constant_t => $arg,
    };

    bless $self, $class;
    return $self;
}

sub name
{
    my $self = shift;
    my $name_t = Bloomberg::API::Internal::blpapi_Constant_name($self->{d_constant_t});
    return Bloomberg::API::Name->createInternally($name_t);
}

sub description
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Constant_description($self->{d_constant_t});
}

sub status
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Constant_status($self->{d_constant_t});
}

sub datatype
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Constant_datatype($self->{d_constant_t});
}

sub getValueAsString
{
    my $self = shift;
    my ($value, $rcode) = 
        Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsString(
                                                        $self->{d_constant_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return $value;
}

sub getValueAsFloat64
{
    my $self = shift;
    my ($value, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsFloat64(
            $self->{d_constant_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return $value;
}

sub getValue
{
    my $self = shift;
    my $dataType = $self->datatype();
    my $value;
    my $rcode = 0;
    if ($dataType == $Bloomberg::API::Internal::BLPAPI_DATATYPE_INT32)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsInt32(
                $self->{d_constant_t});
    }
    elsif ($dataType == $Bloomberg::API::Internal::BLPAPI_DATATYPE_INT64)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsInt64(
                $self->{d_constant_t});
    }
    elsif ($dataType == $Bloomberg::API::Internal::BLPAPI_DATATYPE_FLOAT32)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsFloat32(
                $self->{d_constant_t});
    }
    elsif ($dataType == $Bloomberg::API::Internal::BLPAPI_DATATYPE_FLOAT64)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsFloat64(
                $self->{d_constant_t});
    }
    elsif ($dataType == $Bloomberg::API::Internal::BLPAPI_DATATYPE_DATETIME)
    {
        ($value, $rcode) = $self->getValueAsDatetime();
    }
    elsif ($dataType == $Bloomberg::API::Internal::BLPAPI_DATATYPE_CHAR)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Constant_getValueAsChar(
                $self->{d_constant_t});
    }
    else {
        ($value, $rcode) = $self->getValueAsString();
    }
    Bloomberg::API::Exception->throwOnError($rcode);
    return $value;
}

sub getValueAsDatetime
{
    my $self = shift;
    my $datetime = Bloomberg::API::Datetime->new();
    my $rcode = Bloomberg::API::Internal::blpapi_Constant_getValueAsDatetime(
        $self->{d_constant_t},
        $datetime->getDatetime_t());
    Bloomberg::API::Exception->throwOnError($rcode);
    return $datetime;
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Constant - Represents a constant value in the schema.

=head1 DESCRIPTION

Constants can be any of the following DataTypes: BOOL, CHAR, BYTE, INT32,
INT64, FLOAT32, FLOAT64, STRING, DATE, TIME, DATETIME. As well as the
constants value this class also provides access to the symbolic name,
description and status of the constant.

Constants are read-only and always created by the API, never by the
application.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

Returns the symbolic name of this Constant.

=item description

Returns a string containing a human readable description of this Constant. 

=item status

Returns the status (Bloomberg::API::SchemaStatus) of this Constant.

=item datatype

Return the DataType used to represent the value of this constant.

=item getValue

Return the constant value which could be a scalar or Datetime.
based on the data type of the element (present in the schema).

=item getValueAsString

Returns this Constant's value as a string. If the value cannot be converted
to a string, an exception is thrown.

=item getValueAsFloat64

Returns this Constant's value as a Float64. If the value cannot be converted
to a Float64, an exception is thrown.

=item getValueAsDatetime

Returns this Constant's value as a Datetime. If the value cannot be converted
to a Datetime, an exception is thrown.

=back
