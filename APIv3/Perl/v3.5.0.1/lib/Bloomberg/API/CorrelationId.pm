# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::CorrelationId;

use strict;
use warnings;

use Bloomberg::API::Internal;
use Scalar::Util;

use overload
    "==" => \&equal,
    "<" => \&lessthan,
    qw("") => \&toString
        ;

use constant UNSET_VALUE => $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_UNSET;
use constant INT_VALUE => $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_INT;
use constant OBJECT_VALUE =>
                        $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_POINTER;
use constant AUTOGEN_VALUE =>
                        $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_AUTOGEN;

sub new
{
    my $class = shift;
    my ($value) = @_; # intValue == 0, if no args

    my $valueClass = Scalar::Util::blessed($value);
    if(not defined $valueClass) {
        if(!defined $value ||
           $value =~ /^\d+$/) {
            return $class->createUsingInt($value);
        }
    }
    return $class->createUsingObject($value);
}

sub clone
{
    my $self = shift;
    my $anotherSelf = {
        d_correlationId_t => $self->{d_correlationId_t}
    };
    bless $anotherSelf;
    return $anotherSelf;
}

# private function
sub createUsingInt
{
    my $class = shift;
    my ($intValue) = @_; # intValue == 0, if no args
    my $correlationIdHandle = Bloomberg::API::Internal::blpapi_CorrelationId_t->new();
    $correlationIdHandle->swig_size_set(0);
    $correlationIdHandle->swig_valueType_set(
            $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_INT); 
    my $value = $correlationIdHandle->swig_value_get();
    if (not defined $intValue) {
        $intValue = 0;
        $correlationIdHandle->swig_valueType_set(
            $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_UNSET);
    }
    $value->swig_intValue_set($intValue);
    $correlationIdHandle->swig_classId_set(0);

    my $self = {
       d_correlationId_t => $correlationIdHandle,
    };
    bless $self, $class;
    return $self;
}

# private function
sub createUsingObject
{
    my $class = shift;
    my ($object) = @_;
    my $int = Bloomberg::API::CorrelationId::ObjectHolder::createMapping($object);
    my $cid = $class->new($int);
    $cid->{d_correlationId_t}->swig_valueType_set(
            $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_POINTER);
    return $cid;
}

# private function
sub createInternally {
    my $class = shift;
    my ($arg) = @_;
    $arg->isa("Bloomberg::API::Internal::blpapi_CorrelationId_t")
        or die("Can be used Internally only");
    my ($self) = {
        d_correlationId_t => $arg
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;

    if(defined $self->{d_correlationId_t} &&
            ($self->{d_correlationId_t}->swig_valueType_get()) ==
            $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_POINTER) {
        my $value = $self->{d_correlationId_t}->swig_value_get();
        my $intValue = $value->swig_intValue_get();
        Bloomberg::API::CorrelationId::ObjectHolder::deleteObject($intValue);
    }
}

sub impl
{
    my $self = shift;
    return $self->{d_correlationId_t};
}

sub equal
{
    my ($leftCid, $rightCid) = @_;
    if((!defined $leftCid) || (!defined $rightCid)) {
        return 0;
    }
    if ($leftCid->valueType() != $rightCid->valueType()) {
        return 0;
    }
    if ($leftCid->classId() != $rightCid->classId()) {
        return 0;
    }
    my $valueLeft = $leftCid->{d_correlationId_t}->swig_value_get();
    my $intValueLeft = $valueLeft->swig_intValue_get();
    my $valueRight = $rightCid->{d_correlationId_t}->swig_value_get();
    my $intValueRight = $valueRight->swig_intValue_get();
    return $intValueLeft == $intValueRight;
}

sub lessthan
{
    my ($lhs, $rhs) = @_;
    return
        Bloomberg::API::Internal::blpapi_internal_CorrelationId_compare(
                $lhs->{d_correlationId_t},
                $rhs->{d_correlationId_t}) < 0;
}

sub toString
{
    my $cid = shift;
    my $valueType = "Unknown";
    my $value = "Unknown";
    if($cid->valueType() ==
       $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_UNSET) {
        $valueType = "UNSET";
    }
    elsif($cid->valueType() ==
       $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_INT) {
        $valueType = "INT";
        $value = $cid->asInteger();
    }
    elsif($cid->valueType() ==
       $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_POINTER) {
        $valueType = "OBJECT";
        $value = $cid->asObject();
    }
    elsif($cid->valueType() ==
       $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_AUTOGEN) {
        $valueType = "AUTOGEN";
        $value = $cid->asInteger();
    }

    my $returnString = "[ valueType=".$valueType." classId="
        .$cid->classId()." value=".$value
        ." ]";
    return $returnString;
}

sub valueType
{
    my $self = shift;
    my $valueType = $self->{d_correlationId_t}->swig_valueType_get();
    return $valueType;
}

sub classId
{
    my $self = shift;
    my $classId = $self->{d_correlationId_t}->swig_classId_get();
    return $classId;
}

sub asInteger
{
    my $self = shift;
    my $valueType = $self->{d_correlationId_t}->swig_valueType_get();
    if ($valueType == $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_INT ||
            $valueType == $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_AUTOGEN) {
        my $value = $self->{d_correlationId_t}->swig_value_get();
        my $intValue = $value->swig_intValue_get();
        return $intValue;
    }
    return undef;
}

sub asObject
{
    my $self = shift;
    if (($self->{d_correlationId_t}->swig_valueType_get()) ==
            $Bloomberg::API::Internal::BLPAPI_CORRELATION_TYPE_POINTER) {
        my $value = $self->{d_correlationId_t}->swig_value_get();
        my $intValue = $value->swig_intValue_get();
        return Bloomberg::API::CorrelationId::ObjectHolder::getObject($intValue);
    }
    return undef;

}

package Bloomberg::API::CorrelationId::ObjectHolder;

my %integerObjectHashMap = ();
my $nextIntger = 0;

# private function
sub createMapping
{
    my ($obj) = @_;
    $nextIntger++;
    $integerObjectHashMap{$nextIntger} = $obj;
    return $nextIntger;
}

# private function
sub getObject
{
    my ($int) = @_;
    return $integerObjectHashMap{$int};
}

# private function
sub deleteObject
{
    my ($int) = @_;
    delete $integerObjectHashMap{$int};
}

1;


__END__

=pod

=head1 NAME

Bloomberg::API::CorrelationId - Provide a key to identify individual subscriptions or
requests

=head1 DESCRIPTION

This component provides an identifier that is attached to individual
subscription and request. CorrelationIds are used to distinguish between
various subscriptions and are a way to find the response for an asynchronous
request.

CorrelationId objects are passed to many of the Session object methods which
initiate an asynchronous operations and are obtained from Message objects which
are delivered as a result of those asynchronous operations.

When subscribing or requesting information an application has the choice of
providing a CorrelationId constructed themselves or allowing the session to
construct one. If the application supplies a CorrelationId it must not
re-use the value contained in it in another CorrelationId while the original
request or subscription is still active.

It is possible that an application supplied CorrelationId and a CorrelationId
constructed by the API could return the same result for asInteger(). However,
they will not compare equal using the defined operator '==' for CorrelationId
and there is a consistent order defined using the overloaded operator '<' for
CorrelationIds.

A CorrelationId constructed by an application can contain either

- an integer,

- an object

=head1 LIST

=over 4

UNSET_VALUE
    The CorrelationId is unset. That is, it was created
    by the default CorrelationId constructor.

INT_VALUE
   The CorrelationId was created from an integer
   supplied by the user.


OBJECT_VALUE
    The CorrelationId was created from an object
    supplied by the user.

AUTOGEN_VALUE
    The CorrelationId was created internally by API.

=back

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item valueType

Return the type of this CorrelationId object.

=item classId

Return the user defined classification of this CorrelationId
object.

=item asInteger

Return the value of this CorrelationId as an integer value. The result is
undefined if this CorrelationId does not have valueType()==INT_VALUE or
valueType()==AUTOGEN_VALUE.

=item asObject

Return the value of this CorrelationId as a pointer value. The result is
undefined if this CorrelationId does not have valueType()==OBJECT_VALUE.

=item == / equal

  $lhs == $rhs
  equal($lhs, $rhs)

Return true if the specified 'lhs' and 'rhs' CorrelationId objects contain the
same value. Return false otherwise. Two CorrelationId objects contain the same
value if the result of valueType() is the same and the result of asObject() or
asInteger() as appropriate is also the same.

=item "" / toString

Return the human-readable string for this CorrelationId object.

=item < / lessthan

  $lhs < $rhs
  lessthan($lhs, $rhs)

Return true if the specified 'lhs' is less than the specified 'rhs'.

=back
