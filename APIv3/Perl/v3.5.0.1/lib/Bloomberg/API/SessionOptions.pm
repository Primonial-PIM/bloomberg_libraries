# Copyright (C) 2010-2012 Bloomberg Finance L.P.

package Bloomberg::API::SessionOptions;

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Exception;

sub new
{
    my $class = shift;
    my $optionHandle = Bloomberg::API::Internal::blpapi_SessionOptions_create();
    my $self = {
        d_sessionOptions_t => $optionHandle,
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_SessionOptions_destroy(
                                                  $self->{d_sessionOptions_t});

}

sub clone
{
    my $self = shift;
    my $optionHandle = Bloomberg::API::Internal::blpapi_SessionOptions_duplicate(
                                                  $self->{d_sessionOptions_t});
    my $anotherSelf = {
        d_sessionOptions_t => $optionHandle,
    };
    bless $anotherSelf;
    return $anotherSelf;

}

sub setServerHost
{
    my $self = shift;
    my ($serverHost) = @_;
    my $rcode = Bloomberg::API::Internal::blpapi_SessionOptions_setServerHost(
                                                   $self->{d_sessionOptions_t},
                                                   $serverHost);
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub setServerPort
{
    my $self = shift;
    my ($serverPort) = @_;
    my $rcode = Bloomberg::API::Internal::blpapi_SessionOptions_setServerPort(
                                                   $self->{d_sessionOptions_t},
                                                   $serverPort);
    Bloomberg::API::Exception->throwOnError($rcode);
}

# This takes in a array of tuples [("serverAddress", port), ("ser", port)]
sub setServerAddresses
{
    my $self = shift;
    my @serverAddresses = @_;
    for my $i (0..$#serverAddresses) {
        my $rcode =
            Bloomberg::API::Internal::blpapi_SessionOptions_setServerAddress(
                                                   $self->{d_sessionOptions_t},
                                                   $serverAddresses[$i][0],
                                                   $serverAddresses[$i][1],
                                                   $i);
        Bloomberg::API::Exception->throwOnError($rcode);

    }
}

sub setConnectTimeout
{
    my $self = shift;
    my ($connectTimeOut) = @_;
    my $rcode = Bloomberg::API::Internal::blpapi_SessionOptions_setConnectTimeout(
                                                   $self->{d_sessionOptions_t},
                                                   $connectTimeOut);
    Bloomberg::API::Exception->throwOnError($rcode);

}

sub setDefaultService
{
    my $self = shift;
    my ($defaultService) = @_;
    my $rcode = Bloomberg::API::Internal::blpapi_SessionOptions_setDefaultServices(
                                                   $self->{d_sessionOptions_t},
                                                   $defaultService);
    Bloomberg::API::Exception->throwOnError($rcode);

}

sub setDefaultSubscriptionService
{
    my $self = shift;
    my ($defaultService) = @_;
    my $rcode =
        Bloomberg::API::Internal::blpapi_SessionOptions_setDefaultSubscriptionService(
                                                   $self->{d_sessionOptions_t},
                                                   $defaultService);
    Bloomberg::API::Exception->throwOnError($rcode);

}

sub setDefaultTopicPrefix
{
    my $self = shift;
    my ($topicPrefix) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setDefaultTopicPrefix(
                                                   $self->{d_sessionOptions_t},
                                                   $topicPrefix);
}

sub setAllowMultipleCorrelatorsPerMsg
{
    my $self = shift;
    my ($allowMultipleCorrelatorsPerMsg) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setAllowMultipleCorrelatorsPerMsg(
                                              $self->{d_sessionOptions_t},
                                              $allowMultipleCorrelatorsPerMsg);
}

sub setClientMode
{
    my $self = shift;
    my ($clientMode) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setClientMode(
                                                   $self->{d_sessionOptions_t},
                                                   $clientMode);
}

sub setMaxPendingRequests
{
    my $self = shift;
    my ($maxPendingRequests) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setMaxPendingRequests(
                                                   $self->{d_sessionOptions_t},
                                                   $maxPendingRequests);
}

sub setAutoRestartOnDisconnection
{
    my $self = shift;
    my ($autoRestart) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setAutoRestartOnDisconnection(
                                                   $self->{d_sessionOptions_t},
                                                   $autoRestart);
}

sub setAuthenticationOptions
{
    my $self = shift;
    my ($authenticationOption) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setAuthenticationOptions(
                                                   $self->{d_sessionOptions_t},
                                                   $authenticationOption);
}

sub setNumStartAttempts
{
    my $self = shift;
    my ($numStartAttempt) = @_;
    Bloomberg::API::Internal::blpapi_SessionOptions_setNumStartAttempts(
                                                   $self->{d_sessionOptions_t},
                                                   $numStartAttempt);
}

sub setMaxEventQueueSize
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my ($maxEventQueueSize) = @_;
    Bloomberg::API::Internal::blpapi_internal_SessionOptions_setMaxEventQueueSize(
                                                   $self->{d_sessionOptions_t},
                                                   $maxEventQueueSize);
}

sub setSlowConsumerWarningHiWaterMark
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my ($hiWaterMark) = @_;
    my $rcode = Bloomberg::API::Internal::blpapi_internal_SessionOptions_setSlowConsumerWarningHiWaterMark(
                                                   $self->{d_sessionOptions_t},
                                                   $hiWaterMark);
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub setSlowConsumerWarningLoWaterMark
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my ($loWaterMark) = @_;
    my $rcode =
        Bloomberg::API::Internal::blpapi_internal_SessionOptions_setSlowConsumerWarningLoWaterMark(
                                                   $self->{d_sessionOptions_t},
                                                   $loWaterMark);
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub setDefaultKeepAliveInactivityTime
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my ($defaultKeepAliveInactivityTime) = @_;
    my $rcode =
    Bloomberg::API::Internal::blpapi_internal_SessionOptions_setDefaultKeepAliveInactivityTime(
                                              $self->{d_sessionOptions_t},
                                              $defaultKeepAliveInactivityTime);
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub setDefaultKeepAliveResponseTimeout
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my ($defaultKeepAliveResponseTimeout) = @_;
    my $rcode =
    Bloomberg::API::Internal::blpapi_internal_SessionOptions_setDefaultKeepAliveResponseTimeout(
                                             $self->{d_sessionOptions_t},
                                             $defaultKeepAliveResponseTimeout);
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub serverHost
{
    my $self = shift;
    my $host = Bloomberg::API::Internal::blpapi_SessionOptions_serverHost(
                                                  $self->{d_sessionOptions_t});
    return $host;
}

sub serverPort
{
    my $self = shift;
    my $port = Bloomberg::API::Internal::blpapi_SessionOptions_serverPort(
                                                  $self->{d_sessionOptions_t});
    return $port;
}

sub serverAddresses
{
    my $self = shift;
    my @serverList;
    my $numAddresses =
        Bloomberg::API::Internal::blpapi_SessionOptions_numServerAddresses(
                                                  $self->{d_sessionOptions_t});

    for my $i (0..$numAddresses) {
        my ($rcode, $serverAddress, $port) =
            Bloomberg::API::Internal::blpapi_SessionOptions_getServerAddress(
                                                   $self->{d_sessionOptions_t},
                                                   $i);
        if($rcode != 0) {
            last;
        }
        push @serverList, [$serverAddress, $port];
    }
    return @serverList;
}

sub connectTimeout
{
    my $self = shift;
    my $timeout = Bloomberg::API::Internal::blpapi_SessionOptions_connectTimeout(
                                                  $self->{d_sessionOptions_t});
    return $timeout;

}

sub defaultService
{
    my $self = shift;
    my $service =
        Bloomberg::API::Internal::blpapi_SessionOptions_defaultServices(
                                                 $self->{d_sessionOptions_t});
    return $service;
}

sub defaultSubscriptionService
{
    my $self = shift;
    my $service =
      Bloomberg::API::Internal::blpapi_SessionOptions_defaultSubscriptionService(
                                                  $self->{d_sessionOptions_t});
    return $service;
}

sub defaultTopicPrefix
{
    my $self = shift;
    my $topicPrefix =
        Bloomberg::API::Internal::blpapi_SessionOptions_defaultTopicPrefix(
                                                  $self->{d_sessionOptions_t});

    return $topicPrefix;
}

sub allowMultipleCorrelatorsPerMsg
{
    my $self = shift;
    my $val =
        Bloomberg::API::Internal::blpapi_SessionOptions_allowMultipleCorrelatorsPerMsg(
                                                  $self->{d_sessionOptions_t});
    return ($val != 0);
}

sub clientMode
{
    my $self = shift;
    my $mode =
        Bloomberg::API::Internal::blpapi_SessionOptions_clientMode(
                                                  $self->{d_sessionOptions_t});
    return $mode;
}

sub maxPendingRequests
{
    my $self = shift;
    my $req = Bloomberg::API::Internal::blpapi_SessionOptions_maxPendingRequests(
                                                  $self->{d_sessionOptions_t});
    return $req;
}

sub autoRestartOnDisconnection
{
    my $self = shift;
    my $restart =
        Bloomberg::API::Internal::blpapi_SessionOptions_autoRestartOnDisconnection(
                                                  $self->{d_sessionOptions_t});
    return ($restart != 0);
}

sub authenticationOptions
{
    my $self = shift;
    my $options =
        Bloomberg::API::Internal::blpapi_SessionOptions_authenticationOptions(
                                                  $self->{d_sessionOptions_t});
    return $options;
}

sub numStartAttempts
{
    my $self = shift;
    my $attempts =
        Bloomberg::API::Internal::blpapi_SessionOptions_numStartAttempts(
                                                  $self->{d_sessionOptions_t});
    return $attempts;
}

sub slowConsumerWarningHiWaterMark
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my $hiWaterMark =
        Bloomberg::API::Internal::blpapi_internal_SessionOptions_slowConsumerWarningHiWaterMark(
                                                  $self->{d_sessionOptions_t});
    return $hiWaterMark;

}

sub slowConsumerWarningLoWaterMark
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my $loWaterMark =
        Bloomberg::API::Internal::blpapi_internal_SessionOptions_slowConsumerWarningLoWaterMark($self->{d_sessionOptions_t});
    return $loWaterMark;
}

sub defaultKeepAliveInactivityTime
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my $inactTime =
        Bloomberg::API::Internal::blpapi_internal_SessionOptions_defaultKeepAliveInactivityTime(
                                                  $self->{d_sessionOptions_t});
    return $inactTime;
}

sub defaultKeepAliveResponseTimeout
{
    Bloomberg::API::Exception->checkVersionAndThrow(30406);
    my $self = shift;
    my $timeout =
        Bloomberg::API::Internal::blpapi_internal_SessionOptions_defaultKeepAliveResponseTimeout(
                                                  $self->{d_sessionOptions_t});
    return $timeout;
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::SessionOptions - Non default options on a Session

=head1 DESCRIPTION

To use non-default options on a Session, create a SessionOptions instance
and set the required options and then supply it when creating a Session.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item serverHost

The hostname or the ip address of the API server

=item serverPort

The port number where the API server accepts connections

=item maxSendCacheSize

Maximum number of bytes that will be cached by the Session before requests
will fail

=item maxEventQueueSize

The maximum number of events that can be buffered by the Session

=item maxPendingRequests

The maximum number of requests that can be pending

=item allowMultipeCorrelatorsPerMsg

If a message is to be delivered for multiple correlators then deliver one
message with all correlators folded into that one message

=item slowConsumerWarningHiWaterMark

When the number of events in the event queue exceeds the product of the
event queue size and the High water mark, a slow consumer warning event is
published. The following is applicable
0.0 <= slowConsumerWarningHiWaterMark <= 1.0

=item slowConsumerWarningLoWaterMark

If the session is currently marked a slow consumer and if the number of
events in the event queue drops below the product of the low water mark and
the event queue size, a slow consumer warning cleared event is published.
The following is applicable
0.0 <= slowConsumerWarningClearLoadFactor <= 1.0. and
slowConsumerWarningClearHiWaterMark < slowConsumerWarningLoWaterMark.

=item defaultSubscriptionService

The name of the service to be used by default for subscriptions

=item defaultTopicPrefix

The name of the topic prefix to be used for subscriptions if one is not
provided

=item clientMode

see #{@link SessionOptions.ClientMode}

=item serverAddresses

List of server addresses (hostname and port) where the API servers are
running.

=item autoRestartOnDisconnection

On disconnection from the backend, should the session connect again to the
next available API server ?

=item numStartAttempts

The number of connection failures before session gives up on connection

=item defaultKeepAliveInactivityTime

The inteval (in milliseconds) a connection has to remain inactive (receive no
data) before a keep alive probe will be sent.

=item defaultKeepAliveResponseTimeout

The time (in milliseconds) the library will wait for response to a keep alive
probe before declaring it lost.

=back

