# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::ConstantList;
use Bloomberg::API::Name;

use Scalar::Util;

package Bloomberg::API::SchemaStatus;

use constant ACTIVE => $Bloomberg::API::Internal::BLPAPI_STATUS_ACTIVE;
use constant DEPRECATED => $Bloomberg::API::Internal::BLPAPI_STATUS_DEPRECATED;
use constant INACTIVE => $Bloomberg::API::Internal::BLPAPI_STATUS_INACTIVE;
use constant PENDING_DEPRECATION => $Bloomberg::API::Internal::PENDING_DEPRECATION;

package Bloomberg::API::SchemaTypeDefinition;

sub createInternally
{
    my $class = shift;
    my ($schemaTypeDefHandle) = @_;
    my $self = {
        d_schemaTypeDef_t => $schemaTypeDefHandle,
    };
    bless $self, $class;
    return $self;
}

sub name
{
    my $self = shift;
    my $typeDef_t = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_name(
            $self->{d_schemaTypeDef_t});
    return Bloomberg::API::Name->createInternally($typeDef_t);
}

sub datatype
{
    my $self = shift;
    my $value = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_datatype(
            $self->{d_schemaTypeDef_t});
    return $value;
}

sub description
{
    my $self = shift;
    my $value = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_description(
            $self->{d_schemaTypeDef_t});
    return $value;
}

sub status
{
    my $self = shift;
    my $value = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_status(
            $self->{d_schemaTypeDef_t});
    return $value;
}

sub numElementDefinitions
{
    my $self = shift;
    my $value =
        Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_numElementDefinitions(
            $self->{d_schemaTypeDef_t});
    return $value;
}

sub hasElementDefinition
{
    my $self = shift;
    my ($name) = @_;
    my $elemDef_t;
    my $nameClass = Scalar::Util::blessed($name);
    if((defined $nameClass) && ($name->isa("Bloomberg::API::Name"))) {
        $elemDef_t =
            Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_getElementDefinition(
                    $self->{d_schemaTypeDef_t}, undef, $name->{d_name_t});
    }
    else {
        $elemDef_t = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_getElementDefinition(
            $self->{d_schemaTypeDef_t}, $name."", undef);
    }
    return $elemDef_t;
}

sub getElementDefinition
{
    my $self = shift;
    my ($name) = @_;
    my $elemDef_t;
    my $nameClass = Scalar::Util::blessed($name);
    if((defined $nameClass) && ($name->isa("Bloomberg::API::Name"))) {
        $elemDef_t =
            Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_getElementDefinition(
                    $self->{d_schemaTypeDef_t}, undef, $name->{d_name_t});
    }
    else {
        $elemDef_t = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_getElementDefinition(
            $self->{d_schemaTypeDef_t}, $name."", undef);
    }
    return Bloomberg::API::SchemaElementDefinition->createInternally($elemDef_t);

}

sub getElementDefinitionAt
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my $elementDef_t =
        Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_getElementDefinitionAt(
                $self->{d_schemaTypeDef_t}, $index);
    return Bloomberg::API::SchemaElementDefinition->createInternally($elementDef_t)
        if defined $elementDef_t;
    return undef;
}

sub isComplexType
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_isComplexType(
            $self->{d_schemaTypeDef_t});
}

sub isSimpleType
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_isSimpleType(
            $self->{d_schemaTypeDef_t});
}

sub isEnumerationType
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_isEnumerationType(
            $self->{d_schemaTypeDef_t});
}

sub print
{
    my $self = shift;
    my ($fileHandle) = @_;
    $fileHandle = \*STDOUT if not defined $fileHandle;

    Bloomberg::API::Internal::blpapi_internal_print_SchemaTypeDefinition(
        $self->{d_schemaTypeDef_t}, 0, 4, $fileHandle);
}

sub enumeration
{
    my $self = shift;

    my $constantList_t = Bloomberg::API::Internal::blpapi_SchemaTypeDefinition_enumeration(
            $self->{d_schemaTypeDef_t});
    return Bloomberg::API::ConstantList->createInternally($constantList_t) if defined
        $constantList_t;
    return undef;
}

package Bloomberg::API::SchemaElementDefinition;

use constant UNBOUNDED => $Bloomberg::API::Internal::BLPAPI_ELEMENTDEFINITION_UNBOUNDED;

sub createInternally
{
    my $class = shift;
    my ($schemaElemDefHandle) = @_;
    $schemaElemDefHandle->isa("_p_blpapi_SchemaElementDefinition_t") or
        die("Type _p_blpapi_SchemaElementDefinition_t expected");
    my $self = {
        d_schemaElemDef_t => $schemaElemDefHandle
    };
    bless $self, $class;
    return $self;
}

sub name
{
    my $self = shift;
    my $name = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_name(
            $self->{d_schemaElemDef_t});
    return Bloomberg::API::Name->createInternally($name);
}

sub description
{
    my $self = shift;
    my $description = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_description(
            $self->{d_schemaElemDef_t});
    return $description;
}

sub status
{
    my $self = shift;
    my $status = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_status(
            $self->{d_schemaElemDef_t});
    return $status;
}

sub typeDefinition
{
    my $self = shift;
    my $typeDefHandle =
        Bloomberg::API::Internal::blpapi_SchemaElementDefinition_type(
                $self->{d_schemaElemDef_t});
    my $typeDef = Bloomberg::API::SchemaTypeDefinition->createInternally($typeDefHandle);
    return $typeDef;
}

sub minValues
{
    my $self = shift;
    my $minValues = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_minValues(
            $self->{d_schemaElemDef_t});
    return $minValues;
}

sub maxValues
{
    my $self = shift;
    my $maxValues = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_maxValues(
            $self->{d_schemaElemDef_t});
    return $maxValues;
}

sub numAlternateNames
{
    my $self = shift;
    my $value = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_numAlternateNames(
            $self->{d_schemaElemDef_t});
    return $value;
}

sub getAlternateName
{
    my $self = shift;
    my ($index) = @_;
    my $name_t = Bloomberg::API::Internal::blpapi_SchemaElementDefinition_getAlternateName(
            $self->{d_schemaElemDef_t}, $index);
    return Bloomberg::API::Name->createInternally($name_t) if defined $name_t;
    return undef;
}

sub print
{
    my $self = shift;
    my ($fileHandle) = @_;
    $fileHandle = \*STDOUT if not defined $fileHandle;

    Bloomberg::API::Internal::blpapi_internal_print_SchemaElementDefinition(
        $self->{d_schemaElemDef_t}, 0, 4, $fileHandle);
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Schema - Provide a representation of schema

=head1 NAME

Bloomberg::API::SchemaStatus - The possible status for an item in the schema.

Possible status are ACTIVE, DEPRECATED, INACTIVE, PENDING_DEPRECATION.

=head1 NAME

Bloomberg::API::SchemaTypeDefinition - The definition of a type in the data schema.

=head1 DESCRIPTION

Each SchemaElementDefinition object has a SchemaTypeDefinition (which it may
share with other SchemaElementDefinition objects). SchemaTypeDefinition objects
are read-only and always created by the API, never by the application.

A SchemaTypeDefinition can define either items which are simple (that is,
they are a single value) or sequence or choice Elements (that is, they may
contain an item or items which are accessed by name)

For sequence or choice items the SchemaTypeDefinition provides access to the
individual SchemaElementDefinition objects that define each member of the
sequence.

In addition, the SchemaTypeDefinition provides access to the symbolic name of
the TypeDefinition, its description and its status.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

Returns the name of this SchemaTypeDefinition.

=item datatype

Returns the DataType of this SchemaTypeDefinition.

=item description

Returns a string which contains a human readable description of this
SchemaTypeDefinition.

=item status

Returns the status (SchemaStatus::Value) of this SchemaTypeDefinition.

=item numElementDefinitions

Returns the number of SchemaElementDefinition objects contained by this
SchemaTypeDefinition. If this SchemaTypeDefinition is neither a choice nor a
sequence this will return 0.

=item hasElementDefinition

  hasElementDefinition($name)
  hasElementDefinition($nameString)

Returns 'true' if this SchemaTypeDefinition contains an item with the specified
'name' of type Bloomberg::API::Name or 'nameString' of type string, otherwise returns
'false'.

=item getElementDefinition

  getElementDefinition($name)
  getElementDefinition($nameString)

Returns the definition of the item identified by the specified 'name' of type
Bloomberg::API::Name or 'nameString' of type string. If there is no item 'name' or
'nameString' an exception is thrown.

=item getElementDefinitionAt

  getElementDefinitionAt($index)

Returns the definition of the specified 'index'th item. If
'index'>=numElementDefinitions() an exception is thrown.

=item isComplexType

Returns 'true' if this SchemaTypeDefinition represents a sequence or choice
type.

=item isSimpleType

Returns 'true' if this SchemaTypeDefinition represents neither a sequence nor a
choice type.

=item isEnumerationType

Returns 'true' if this SchemaTypeDefinition represents an enumeration type.

=item print

  print($fileHandle)

Format this SchemaElementDefinition to the optionally specified output
'fileHandle'. If 'fileHandle' is not specified, standard output is used.

=item enumeration

If this SchemaTypeDefinition returns 'true' to isEnumerationType() this returns
a ConstantList which contains the possible values.

=back

=head1 NAME

Bloomberg::API::SchemaElementDefinition - The definition of an item in the data schema.

=head1 DESCRIPTION

SchemaElementDefinition are returned by Service objects and Operation objects
to define the content of requests, replies and events. The SchemaTypeDefinition
returned by SchemaElementDefinition::typeDefinition() may itself return
SchemaElementDefinition objects when the schema contains complex items.
SchemaElementDefinition objects are read-only and always created by the API,
never by the application.

The SchemaElementDefinition contains the symbolic name, any constraints on this
item, and the SchemaTypeDefinition for instances of this item.

An item which is optional in the schema has minValues()==0.

An item which is mandatory in the schema has minValues()>=1.

An item which is a single value has maxValues()==1.

An item which is an array has maxValues()>1.

An item which is an unbounded array has maxValues()==UNBOUNDED.

As well as the symbolic name, array constraints, and other constraints this
class also provides access to the description and status of the item.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

Returns the name of this item.

=item description

Returns a string containing a human readable description of this item.

=item status

Returns the status (SchemaStatus::Value) of this SchemaElementDefinition.

=item typeDefinition

Returns the SchemaTypeDefinition of this item.

=item minValues

Returns the minimum number of occurrences of this item. This value is always
>= 0.

=item maxValues

Returns the maximum number of occurrences of this item. This value is always
>= 1.

=item numAlternateNames

Returns the number of alternate names for this item.

=item getAlternateName

Returns the specified 'index'th alternate name for this item. If
'index'>=numAlternateNames() an exception is  thrown.

=item print

  print($fileHandle)

Format this SchemaElementDefinition to the optionally specified output
'fileHandle'. If 'fileHandle' is not specified, standard output is used.

=back
