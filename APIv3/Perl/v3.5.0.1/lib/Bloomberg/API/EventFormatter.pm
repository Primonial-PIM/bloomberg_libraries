# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Exception;
use Bloomberg::API::Event;
use Scalar::Util;

package Bloomberg::API::EventFormatter;

sub new
{
    my $class = shift;
    my ($arg) = @_;
    my $argClass = Scalar::Util::blessed($arg);
    if(defined $argClass and $argClass eq "Bloomberg::API::Event") {
        my $eventFormatter_t = Bloomberg::API::Internal::blpapi_EventFormatter_create(
            $arg->{d_event_t});
        my $version = Bloomberg::API::Internal::blpapi_internal_getVersionNumber();
        my $self = {
            d_eventFormatter_t => $eventFormatter_t,
            d_libraryVersion => $version
        };
        bless $self, $class;
        return $self;
    }
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_EventFormatter_destroy(
        $self->{d_eventFormatter_t});
}

sub appendMessage
{
    my $self = shift;
    my ($messageType, $topic, $sequenceNumber) = @_;
    my $messageTypeString = undef;
    my $messageTypeName = undef;
    my $messageClass = Scalar::Util::blessed($messageType);
    if(defined $messageClass and $messageClass eq "Bloomberg::API::Name") {
        $messageTypeName = $messageType->{d_name_t};
    }
    else {
        $messageTypeString = $messageType;
    }
    my $rcode;
    if (not defined $sequenceNumber) {
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendMessage(
            $self->{d_eventFormatter_t},
            $messageTypeString,
            $messageTypeName,
            $topic->{d_topic_t});
    }
    else {
        if ($self->{d_libraryVersion} >= 30405) {
            $rcode = Bloomberg::API::Internal::blpapi_internal_EventFormatter_appendMessageSeq(
                $self->{d_eventFormatter_t},
                $messageTypeString,
                $messageTypeName,
                $topic->{d_topic_t},
                $sequenceNumber,
                0); # 0 is the data source ID
        }
        else {
            return Bloomberg::API::Exception->throwException(
                Bloomberg::API::Error::BLPAPI_UNSUPPORTED_CLASS,
                "Function Not Supported");
        }

    }
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub appendResponse
{
    my $self = shift;
    my ($operationType) = @_;
    my $operationTypeString = undef;
    my $operationTypeName = undef;
    my $operationClass = Scalar::Util::blessed($operationType);
    if(defined $operationClass and $operationClass eq "Bloomberg::API::Name") {
        $operationTypeName = $operationType->{d_name_t};
    }
    else {
        $operationTypeString = $operationType;
    }
    my $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendResponse(
        $self->{d_eventFormatter_t},
        $operationTypeString,
        $operationTypeName);
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub appendRecapMessage
{
    my $self = shift;
    my ($topic, $cid, $sequenceNumber) = @_;
    my $cidClass = Scalar::Util::blessed($cid);
    if(defined $cidClass and $cidClass eq "Bloomberg::API::CorrelationId") {
        $cid = $cid->{d_correlationId_t};
    }
    my $rcode;
    if (not defined $sequenceNumber) {
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendRecapMessage(
            $self->{d_eventFormatter_t},
            $topic->{d_topic_t},
            $cid);
    }
    else {
        if ($self->{d_libraryVersion} >= 30405) {
            $rcode = Bloomberg::API::Internal::blpapi_internal_EventFormatter_appendRecapMessageSeq(
                $self->{d_eventFormatter_t},
                $topic->{d_topic_t},
                $cid,
                $sequenceNumber,
                0); # 0 is the data source ID
        }
        else {
            return Bloomberg::API::Exception->throwException(
                Bloomberg::API::Error::BLPAPI_UNSUPPORTED_CLASS,
                "Function Not Supported");
        }
    }
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub setElement
{
    my $self = shift;
    my ($valueType, $value) = @_;
    my $valueTypeString = undef;
    my $valueTypeName = undef;
    my $valueTypeClass = Scalar::Util::blessed($valueType);
    if (defined $valueTypeClass and $valueTypeClass eq "Bloomberg::API::Name") {
        $valueTypeName = $valueType->{d_name_t};
    }
    else {
        $valueTypeString = $valueType;
    }
    my $valueClass = Scalar::Util::blessed($value);
    my $rcode;
    if (not defined $valueClass) {
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_setValueString(
            $self->{d_eventFormatter_t},
            $valueTypeString,
            $valueTypeName,
            $value);
    }
    elsif($valueClass eq "Bloomberg::API::Name") {
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_setValueFromName(
            $self->{d_eventFormatter_t},
            $valueTypeString,
            $valueTypeName,
            $value->{d_name_t});
    }
    elsif($valueClass eq "Bloomberg::API::Datetime") {
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_setValueDatetime(
            $self->{d_eventFormatter_t},
            $valueTypeString,
            $valueTypeName,
            $value->getDatetime_t());
    }
    else {
        # hope the object has the opertator "" overloaded
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_setValueString(
            $self->{d_eventFormatter_t},
            $valueTypeString,
            $valueTypeName,
            $value);
    }
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub pushElement
{
    my $self = shift;
    my ($element) = @_;
    my $elementString = undef;
    my $elementName = undef;
    my $elementClass = Scalar::Util::blessed($element);
    if(defined $elementClass and $elementClass eq "Bloomberg::API::Name") {
        $elementName = $element->{d_name_t};
    }
    else {
        $elementString = $element."";
    }
    my $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_pushElement(
        $self->{d_eventFormatter_t},
        $elementString,
        $elementName);
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub popElement
{
    my $self = shift;
    my $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_popElement(
        $self->{d_eventFormatter_t});
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub appendValue
{
    my $self = shift;
    my ($value) = @_;

    my $valueClass = Scalar::Util::blessed($value);
    my $rcode;
    if (not defined $valueClass) {
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendValueString(
            $self->{d_eventFormatter_t},
            $value);
    }
    elsif($valueClass eq "Bloomberg::API::Name") {
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendValueFromName(
            $self->{d_eventFormatter_t},
            $value->{d_name_t});
    }
    elsif($valueClass eq "Bloomberg::API::Datetime") {
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendValueDatetime(
            $self->{d_eventFormatter_t},
            $value->getDatetime_t());
    }
    else {
        # hope the object has the opertator "" overloaded
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendValueString(
            $self->{d_eventFormatter_t},
            $value);
    }
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub appendElement
{
    my $self = shift;
    my $rcode = Bloomberg::API::Internal::blpapi_EventFormatter_appendElement(
        $self->{d_eventFormatter_t});
    return Bloomberg::API::Exception->throwOnError($rcode);
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::EventFormatter - Add information to an Event

=head1 DESCRIPTION

EventFormatter is used to create Events for publishing.

An EventFormatter is created from an Event obtained from
createPublishEvent() on a Service. Once the Message or Messages
have been appended to the Event using the EventFormatter the
Event can be published using publish() on the ProviderSession.

EventFormatter objects cannot be copied or assigned to
ensure there is no ambiguity about what happens if two
EventFormatters are both formatting the same Event.

The EventFormatter supports a write once only to each field. It
is an error to call setValue() or pushElement() for the same
name more than once at a particular level of the schema when
creating a message.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new


Create an EventFormatter to create Messages in the
specified 'Event'. An Event may only be referenced by one
EventFormatter at any time. Attempting to create a second
EventFormatter referencing the same Event will result in an
exception being thrown.

=item DESTROY

Destroy this EventFormatter object.

=item appendMessage

Append an (empty) message of the specified 'messageType'
that will be published under the specified 'topic' to the
Event referenced by this EventFormatter. After a message
has been appended its elements can be set using the various
setValue() methods.

=item appendResponse

Append an (empty) response message of the specified 'opType'
that will be sent in response to previously received
operation request. After a message has been appended its
elements can be set using the various setValue() methods.
Only one response can be appended.



=item appendRecapMessage

Append a (empty) recap message that will be published under the
specified 'topic' to the Publish Event referenced by this
EventFormatter. Specify the optional CorrelationId pointer 'cid'
if this recap message is added in response to a TOPIC_RECAP message.
After a message has been appended its elements can be set using
the various setElement() methods. It is an error to create append
a recap message to an Admin event.

=item setValue

Set the element with the specified 'name' to the specified
'value' in the current message in the Event referenced by
this EventFormatter. If the 'name' is invalid for the
current message, if appendMessage() has never been called
or if the element identified by 'name' has already been set
an exception is thrown.


=item pushElement

Changes the level at which this EventFormatter is operating
to the specified element 'name'. The element 'name' must
identify either a choice, a sequence or an array at the
current level of the schema or the behavior is
undefined. After this returns, the context of the
EventFormatter is set to the element 'name' in the schema
and any calls to setValue() or pushElement() are applied
at that level. If 'name' represents an array of scalars
then appendValue() must be used to add values. If 'name'
represents an array of complex types then appendElement()
creates the first entry and sets the context of the
EventFormatter to that element. Calling appendElement()
again will create another entry.

=item popElement

Undoes the most recent call to pushLevel() on this
EventFormatter and returns the context of the
EventFormatter to where it was before the call to
pushElement(). Once popElement() has been called it is
invalid to attempt to re-visit the same context.

=item appendValue

Appends the value 'value' to an array element. If an element
cannot be appended an exception is thrown.

=item appendElement

Appends a new element to work on. All future call for messages
will modify the new appended element.

=back

