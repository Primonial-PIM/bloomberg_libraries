# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::ConstantList;

use warnings;
use strict;

use Bloomberg::API::Constant;
use Bloomberg::API::Internal;
use Bloomberg::API::Name;

#private function
sub createInternally
{
    my $class = shift;
    my ($constantListHandle) = @_;

    $constantListHandle->isa("_p_blpapi_ConstantList") or
        die("Can be created with _p_blpapi_Constant only");
    my $self = {
        d_constantList_t => $constantListHandle,
    };

    bless $self, $class;
    return $self;
}

sub name
{
    my $self = shift;
    my $name_t =
        Bloomberg::API::Internal::blpapi_ConstantList_name($self->{d_constantList_t});
    return Bloomberg::API::Name->createInternally($name_t);
}

sub description
{
    my $self = shift;
    return
        Bloomberg::API::Internal::blpapi_ConstantList_description($self->{d_constantList_t});
}

sub status
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_ConstantList_status($self->{d_constantList_t});
}

sub datatype
{
    my $self = shift;
    return
        Bloomberg::API::Internal::blpapi_ConstantList_datatype($self->{d_constantList_t});
}

sub numConstant
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_ConstantList_numConstants(
        $self->{d_constantList_t});
}

sub getConstant
{
    my $self = shift;
    my ($arg) = @_;
    my $constant_t;
    if ($arg->isa("Bloomberg::API::Name")) {
        $constant_t = Bloomberg::API::Internal::blpapi_ConstantList_getConstant(
                $self->{d_constantList_t}, undef, $arg->{d_name_t});
    }
    else {
        $constant_t = Bloomberg::API::Internal::blpapi_ConstantList_getConstant(
                $self->{d_constantList_t}, $arg, undef);
    }
    return Bloomberg::API::Constant->createInternally($constant_t) if defined $constant_t;
    return undef;
}

sub getConstantAt
{
    my $self = shift;
    my ($arg) = @_;
    my $constant_t = Bloomberg::API::Internal::blpapi_ConstantList_getConstantAt(
            $self->{d_constantList_t}, $arg);
    return Bloomberg::API::Constant->createInternally($constant_t) if defined $constant_t;
    return undef;
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::ConstantList - Represents a list of constant values in the schema.

=head1 DESCRIPTION

As well as the list of Constant objects, this class also provides access
to the symbolic name, description and status of the ConstantsList. All
Constant objects in a ConstantsList are of the same DataType.

ConstantLists are read-only and always created by the API, never by the
application.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

Returns the symbolic name of this ConstantList.

=item description

Returns a string containing a human readable description of this ConstantList.

=item status

Returns the status (Bloomberg::API::SchemaStatus) of this ConstantList.

=item datatype

Return the DataType of the Constant objects contained in this ConstantsList.

=item getConstant

  getConstant($name)
  getConstant($nameString)

Return a Constant from this ConstantsList identified by the specified 'name' of
type Bloomberg::API::Name or 'nameString' of type string. If this ConstantsList does
not contain a Constant with the specified 'name' or 'namestring', an exception
is thrown.

=item getConstantAt

  getConstantAt($index)

Return a read only Constant from at the specified 'index' in this ConstantList.
If the 'index' is not in the range 0 to 'numConstants()-1', an exception is
thrown.

=back
