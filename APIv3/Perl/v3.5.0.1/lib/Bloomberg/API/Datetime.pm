# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Exception;

package Bloomberg::API::DatetimeParts;

# Bit flags and masks used to determine which parts of a Datetime are valid.
use constant YEAR         => $Bloomberg::API::Internal::BLPAPI_DATETIME_YEAR_PART;
use constant MONTH        => $Bloomberg::API::Internal::BLPAPI_DATETIME_MONTH_PART;
use constant DAY          => $Bloomberg::API::Internal::BLPAPI_DATETIME_DAY_PART;
use constant OFFSET       => $Bloomberg::API::Internal::BLPAPI_DATETIME_OFFSET_PART;
use constant HOURS        => $Bloomberg::API::Internal::BLPAPI_DATETIME_HOURS_PART;
use constant MINUTES      => $Bloomberg::API::Internal::BLPAPI_DATETIME_MINUTES_PART;
use constant SECONDS      => $Bloomberg::API::Internal::BLPAPI_DATETIME_SECONDS_PART;
use constant MILLISECONDS => $Bloomberg::API::Internal::BLPAPI_DATETIME_MILLISECONDS_PART;
use constant DATE         => $Bloomberg::API::Internal::BLPAPI_DATETIME_DATE_PART;
use constant TIME         => $Bloomberg::API::Internal::BLPAPI_DATETIME_TIME_PART;
use constant TIMEMILLI    => $Bloomberg::API::Internal::BLPAPI_DATETIME_TIMEMILLI_PART;
use constant FRACSECONDS  => $Bloomberg::API::Internal::BLPAPI_DATETIME_TIMEFRACSECONDS_PART;

package Bloomberg::API::Datetime;
use overload
    "==" => \&equal,
    "!=" => \&nequal,
    '""' => \&toString;

# private constants
use constant {
    MILLIPERMICRO => 1000,
    PICOSPERNANO  => 1000,
    PICOSPERMICRO => 1000 * 1000,
    MILLIPERNANO  => 1000 * 1000,
    MILLIPERPICO  => 1000 * 1000 * 1000,
};

# private function
sub getDatetime_t
{
    my $self = shift;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        return $self->{d_datetime_t}->swig_datetime_get();
    }
    return $self->{d_datetime_t};
}

# private function
sub isLeapYear
{
    my ($y) = @_;
    return 0 == $y % 4 && ($y <= 1752 || 0 != $y % 100 || 0 == $y % 400);
}

# private function
sub returnTrue
{
    return 1;
}

# private function
sub dayCheck30
{
    my ($year, $month, $day) = @_;
    if ($day > 30) {
        return 0;
    }
    else {
        return 1;
    }
}

# private function
sub leapYearCheck
{
    my ($year, $month, $day) = @_;
    if (isLeapYear($year)) {
        if ($day > 29) {
            return 0;
        }
        else {
            return 1;
        }
    }
    elsif ($day > 28) {
        return 0;
    }
    else {
        return 1;
    }

}

# private
my %g_validDateActionTable = (
    1  => \&returnTrue,
    2  => \&leapYearCheck,
    3  => \&returnTrue,
    4  => \&dayCheck30,
    5  => \&returnTrue,
    6  => \&dayCheck30,
    7  => \&returnTrue,
    8  => \&returnTrue,
    9  => \&dayCheck30,
    10 => \&returnTrue,
    11 => \&dayCheck30,
    12 => \&returnTrue);

# private function
# 1 for true and 0 for false
sub isValidDate
{
    my ($year, $month, $day) = @_;
    if (($year <= 0) || ($year > 9999) ||
        ($month <= 0) || ($month > 12) ||
        ($day <= 0) || ($day > 31) ) {
        return 0;
    }
    if ($day < 29) {
        return 1;
    }
    if ($year == 1752) {
        if ($month == 9 && $day > 2 && $day < 14) {
            return 0;
        }
    }

    if (defined $g_validDateActionTable{$month}) {
        $g_validDateActionTable{$month}->($year, $month, $day);
    } else {
        # assert false
        return 0;
    }
}

# private function
sub isValidTime
{
    my ($hours, $minutes, $seconds, $milliSeconds) = @_;

    if ($hours == 24) {
        if (($minutes != 0) || ($seconds != 0) || ($milliSeconds != 0)) {
            return 0;
        }
        else {
            return 1;
        }
    }
    else {
        return ($hours >= 0) && ($hours < 24) &&
            ($minutes >= 0) && ($minutes < 60) &&
            ($seconds >= 0) && ($seconds < 60) &&
            ($milliSeconds >= 0) && ($milliSeconds < 1000);
    }
}

# private function
sub addToParts
{
    my $self = shift;
    my ($value) = @_;
    my $parts = $self->getDatetime_t()->swig_parts_get() | $value;
    $self->getDatetime_t()->swig_parts_set($parts);
}

# private function
sub setParts
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_parts_set($value);
}

# takes a hashmap of the following form
# my $baseArgs = {
#         year         => 2011,
#         month        => 1,
#         day          => 29,
#         hours        => 4,
#         minutes      => 34,
#         seconds      => 28,
#         milliseconds => 394,
#         microseconds => 23456,
#         nanoseconds  => 1,
#         picoseconds  => 123456789123,
# };
sub new
{
    my $class = shift;

    my %args = (
        @_,
        );

    my $datetime;
    my $datetime_t;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        $datetime = Bloomberg::API::Internal::blpapi_HighPrecisionDatetime_tag->new();
        $datetime_t = $datetime->swig_datetime_get();
    }
    else {
        $datetime = Bloomberg::API::Internal::blpapi_Datetime_tag->new();
        $datetime_t = $datetime;
    }
    $datetime_t->swig_year_set(1);
    $datetime_t->swig_month_set(1);
    $datetime_t->swig_day_set(1);
    my $self = {
        d_datetime_t => $datetime,
    };
    bless $self, $class;
    $self->setYear($args{year}) if defined $args{year};
    $self->setMonth($args{month}) if defined $args{month};
    $self->setDay($args{day}) if defined $args{day};
    $self->setHours($args{hours}) if defined $args{hours};
    $self->setMinutes($args{minutes}) if defined $args{minutes};
    $self->setSeconds($args{seconds}) if defined $args{seconds};
    $self->setMilliseconds($args{milliseconds}) if defined $args{milliseconds};

    if (Bloomberg::API::Exception->checkVersion(30500)) {
        $self->setMicroseconds($args{microseconds}) if defined $args{microseconds};
        $self->setNanoseconds($args{nanoseconds}) if defined $args{nanoseconds};
        $self->setPicoseconds($args{picoseconds}) if defined $args{picoseconds};
    }

    return $self;
}

sub DESTROY
{
}

sub equal
{
    my ($lhs, $rhs) = @_;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        return 0 == Bloomberg::API::Internalc::blpapi_internal_HighPrecisionDatetime_compare(
            $lhs->{d_datetime_t},
            $rhs->{d_datetime_t});
    }
    if ($lhs->parts() == $rhs->parts()) {
        return 0 == Bloomberg::API::Internalc::blpapi_Datetime_compare(
            $lhs->{d_datetime_t},
            $rhs->{d_datetime_t});
    }
    return 0;
}

sub nequal
{
    my ($lhs, $rhs) = @_;
    return !($lhs == $rhs);
}

sub print
{
    my $self = shift;
    my ($fileHandle) = @_;
    $fileHandle = \*STDOUT if not defined $fileHandle;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        Bloomberg::API::Internal::blpapi_internal_print_HighPrecisionDatetime(
            $self->{d_datetime_t},
            0,
            4,
            $fileHandle);
    }
    else {
        Bloomberg::API::Internal::blpapi_internal_print_Datetime(
            $self->{d_datetime_t},
            0,
            4,
            $fileHandle);
    }
}

sub setDate
{
    my $self = shift;
    my ($year, $month, $day) = @_;
    $self->setYear($year);
    $self->setMonth($month);
    $self->setDay($day);
}

sub setTime
{
    my $self = shift;
    my ($hours, $minutes, $seconds, $milliSeconds) = @_;
    $self->setHours($hours);
    $self->setMinutes($minutes);
    $self->setSeconds($seconds);
    if (!defined $milliSeconds) {
        $self->getDatetime_t()->swig_milliSeconds_set(0);
        return;
    }
    $self->setMilliSeconds($milliSeconds);
}

sub setYear
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_year_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_YEAR_PART);
}

sub setMonth
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_month_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_MONTH_PART);
}

sub setDay
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_day_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_DAY_PART);
}

sub setHours
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_hours_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_HOURS_PART);
}

sub setMinutes
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_minutes_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_MINUTES_PART);
}

sub setSeconds
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_seconds_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_SECONDS_PART);
}

# deprecated
sub setMilliSeconds
{
    my $self = shift;
    $self->setMilliseconds(@_);
}

sub setMilliseconds
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_milliSeconds_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_MILLISECONDS_PART);
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_FRACSECONDS_PART);
    }
}

sub setMicroseconds
{
    Bloomberg::API::Exception->checkVersionAndThrow(30500);
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_milliSeconds_set(int($value / MILLIPERMICRO));
    $self->{d_datetime_t}->swig_picoseconds_set((int($value % MILLIPERMICRO) * PICOSPERMICRO));
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_FRACSECONDS_PART);
}

sub setNanoseconds
{
    Bloomberg::API::Exception->checkVersionAndThrow(30500);
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_milliSeconds_set(int($value / MILLIPERNANO));
    $self->{d_datetime_t}->swig_picoseconds_set((int($value % MILLIPERNANO) * PICOSPERNANO));
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_FRACSECONDS_PART);
}

sub setPicoseconds
{
    Bloomberg::API::Exception->checkVersionAndThrow(30500);
    my $self = shift;
    my ($value) = @_;
    my $millisec = $value / MILLIPERPICO;
    my $picos = $value % MILLIPERPICO;
    $self->getDatetime_t()->swig_milliSeconds_set(int($millisec));
    $self->{d_datetime_t}->swig_picoseconds_set(int($picos));
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_FRACSECONDS_PART);
}

sub setOffset
{
    my $self = shift;
    my ($value) = @_;
    $self->getDatetime_t()->swig_offset_set($value);
    $self->addToParts($Bloomberg::API::Internal::BLPAPI_DATETIME_OFFSET_PART);
}

sub year
{
    my $self = shift;
    my $year= $self->getDatetime_t()->swig_year_get();
    return $year;
}

sub month
{
    my $self = shift;
    my $month= $self->getDatetime_t()->swig_month_get();
    return $month;
}

sub day
{
    my $self = shift;
    my $day= $self->getDatetime_t()->swig_day_get();
    return $day;
}

sub hours
{
    my $self = shift;
    my $hours = $self->getDatetime_t()->swig_hours_get();
    return $hours;
}

sub minutes
{
    my $self = shift;
    my $minutes = $self->getDatetime_t()->swig_minutes_get();
    return $minutes;
}

sub seconds
{
    my $self = shift;
    my $seconds = $self->getDatetime_t()->swig_seconds_get();
    return $seconds;
}

# deprecated
sub milliSeconds
{
    my $self = shift;
    return $self->milliseconds();
}

sub milliseconds
{
    my $self = shift;
    my $milliSeconds = $self->getDatetime_t()->swig_milliSeconds_get();
    return $milliSeconds;
}

sub microseconds
{
    Bloomberg::API::Exception->checkVersionAndThrow(30500);
    my $self = shift;
    my $milliseconds = $self->getDatetime_t()->swig_milliSeconds_get();
    my $picoseconds = $self->{d_datetime_t}->swig_picoseconds_get();
    return int($milliseconds * MILLIPERMICRO + $picoseconds / PICOSPERMICRO);
}

sub nanoseconds
{
    Bloomberg::API::Exception->checkVersionAndThrow(30500);
    my $self = shift;
    my $milliseconds = $self->getDatetime_t()->swig_milliSeconds_get();
    my $picoseconds = $self->{d_datetime_t}->swig_picoseconds_get();
    return int($milliseconds * MILLIPERNANO + $picoseconds / PICOSPERNANO);
}

sub picoseconds
{
    Bloomberg::API::Exception->checkVersionAndThrow(30500);
    my $self = shift;
    my $milliseconds = $self->getDatetime_t()->swig_milliSeconds_get();
    my $picoseconds = $self->{d_datetime_t}->swig_picoseconds_get();
    return ($milliseconds * MILLIPERPICO) + $picoseconds;
}

sub offset
{
    my $self = shift;
    my $offset = $self->getDatetime_t()->swig_offset_get();
    return $offset;
}

sub parts
{
    my $self = shift;
    my $parts = $self->getDatetime_t()->swig_parts_get();
    return $parts;
}

sub hasParts
{
    my $self = shift;
    my ($parts) = @_;
    return $parts == ($self->getDatetime_t()->swig_parts_get() & $parts);
}

sub isValid
{
    my $self = shift;
    if ( ($self->hasParts(Bloomberg::API::DatetimeParts::YEAR)
          || $self->hasParts(Bloomberg::API::DatetimeParts::MONTH)
          || $self->hasParts(Bloomberg::API::DatetimeParts::DAY) )
         && !isValidDate($self->year(), $self->month(), $self->day()) ) {
        return 0;
    }
    if ( ($self->hasParts(Bloomberg::API::DatetimeParts::HOURS)
          || $self->hasParts(Bloomberg::API::DatetimeParts::MINUTES)
          || $self->hasParts(Bloomberg::API::DatetimeParts::SECONDS)
          || $self->hasParts(Bloomberg::API::DatetimeParts::MILLISECONDS))
         && !isValidTime($self->hours(), $self->minutes(), $self->seconds(),
             $self->milliSeconds()) ) {
        return 0;
    }
    return 1;
}

sub toString
{
    my $self = shift;
    my $string = "";
    my $fh;
    open($fh, ">", \$string);
    $self->print($fh);
    return $string;
}
1;

__END__

=pod

=head1 NAME

Bloomberg::API::DatetimeParts - Bit flags and masks used to determine which parts of a
Datetime are valid.

=head1 NAME

Bloomberg::API::Datetime - Represents a date and/or time.

=head1 DESCRIPTION

Represents a date and/or time.

Datetime can represent a date and/or a time or any combination of the
components of a date and time. The value is represented as eight parts which
can be set or queried independently.

These parts are: year; month; day (of month); hour; minute; second;
milliseconds and offset (of timezone from GMT in minutes).

Methods are provided to set and query the parts individually and in groups.
For example, setDate() and setTime(). It is also possible to determine which
parts of the Datetime have been set.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new

This class method accepts parameters for each date and time component:
"year", "month", "day", "hours", "minutes", "seconds"
and any one of the fraction of second "milliseconds", "microseconds",
"nanoseconds" and "picoseconds".
If more than one fractional second is used, the behaviour is undefined.
    my $dt = DateTime->new(
      year        => 2012,
      month       => 2,
      day         => 29,
      hours       => 12,
      minutes     => 0,
      seconds     => 0,
      nanoseconds => 800000000,
  );

=item equal and "=="

Return 'true' if the specified 'lhs' and 'rhs' datetimes have the same value,
and 'false' otherwise.  Two datetimes have the same value if they have the same
respective 'parts()' and the same value for the fields for which the parts have
been set.

=item nequal and "!="

Return 'true' if the specified 'lhs' and 'rhs' datetimes do not have the same
value, and 'false' otherwise.  Two datetimes do not have the same value if they
do not have the same 'parts()' and they do not have the same values for any of
fileds for which the parts have been set.

=item toString and '""'

Return the value of this object in a human-readable format.

=item print

Write the value of this object to the specified output 'fileHandle' in a
human-readable format. If the 'fileHandle' is not specified, this object will
be printed on standard output.

=item setDate

Set the date portions of the 'Datetime' object to the specified 'day', 'month',
and 'year' respectively in arguments. The behavior is undefined unless the
combined representation comprises a date for which 'isValidDate' returns true.

=item setTime

  setTime($hours, $minutes, $seconds);
  setTime($hours, $minutes, $seconds, $milliSeconds);

Set the time of the 'Datetime' object to the specified 'hours', 'minutes',
'seconds', and 'milliSecond respectively. If 'milliSecond' is not passed in, the
milliSecond value will be set to 0. The behavior is undefined unless the combined
representation comprises a time for which 'isValidTime' returns true.

=item setYear

  setYear($value)

Set the year value of this 'Datetime' object to the specified 'value'. The
behavior is undefined unless '1 <= $value <= 12'.

=item setMonth

  setMonth($value)

Set the month value of this 'Datetime' object to the specified 'value'. The
behavior is undefined unless '1 <= $value <= 12'.

=item setDay

  setDay($value)

Set the day value of this 'Datetime' object to the specified 'value'. The
behavior is undefined unless '1 <= $value <= 31'.

=item setHours

  setHours($value)

Set the hours value of this 'Datetime' object to the specified 'value'.
The behavior is undefined unless '0 <= $value <= 24'.

=item setMinutes

  setMinutes($value)

Set the minutes value of this 'Datetime' object to the specified 'value'.
The behavior is undefined unless '0 <= $value <= 59'.

=item setSeconds

  setSeconds($value)

Set the seconds value of this 'Datetime' object to the specified 'value'.
The behavior is undefined unless '0 <= $value <= 59'.

=item setMilliseconds

  setMilliseconds($value)

Set the fraction of a second of this datetime object to the specified
millisecond 'value'. The behavior is undefined unless '0 <= $value <= 999'.

=item setMicroseconds

  setMicroseconds($value)

Set the fraction of a second of this datetime object to the specified
microsecond 'value'. The behavior is undefined unless '0 <= $value <= 999,999'.

=item setNanoseconds

  setNanoseconds($value)

Set the fraction of a second of this datetime object to the specified
nanosecond 'value'. The behavior is undefined unless '0 <= $value <= 999,999,999'.

=item setPicoseconds

  setPicoseconds($value)

Set the fraction of a second of this datetime object to the specified
picosecond 'value'. The behavior is undefined unless '0 <= $value <= 999,999,999,999'.

=item setOffset

  setOffset($value)

Set the offset value of this 'Datetime' object to the specified 'value'.

=item year

Return the year value of this 'Datetime' object. The result is undefined unless
object has a year value set.

=item month

Return the month value of this 'Datetime' object. The result is undefined
unless object has a month value set.

=item day

Return the day value of this 'Datetime' object. The result is undefined unless
object has a day value set.

=item hours

Return the hours value of this 'Datetime' object. The result is undefined
unless object has a hours value set.

=item minutes

Return the minutes value of this 'Datetime' object. The result is undefined
unless object has a minutes value set.

=item seconds

Return the seconds value of this 'Datetime' object. The result is undefined
unless object has a seconds value set.

=item milliseconds

Return the number of (whole) millliseconds in the fraction-of-a-second
part of the value of this object. The result is undefined unless
object has a millisecond value set.

=item microseconds

Return the number of (whole) microseconds in the fraction-of-a-second
part of the value of this object. The result is undefined unless
object has a fraction-of-a-second value set.

=item nanoseconds

Return the number of (whole) nanosecond in the fraction-of-a-second
part of the value of this object. The result is undefined unless
object has a fraction-of-a-second value set.

=item picoseconds

Return the number of (whole) picosecond in the fraction-of-a-second
part of the value of this object. The result is undefined unless
object has a fraction-of-a-second value set.

=item offset

Return the offset value of this 'Datetime' object. The result is undefined
unless object has a offset value set.

=item parts

Return a bitmask of all parts that are set in this 'Datetime' object. This
can be compared to the values in the DatetimeParts enum using bitwise
operations.

=item hasParts

  hasParts($parts)

Return true if this 'Datetime' object has all of the specified 'parts' set.
The 'parts' parameter must be constructed by or'ing together values from the
DateTimeParts enum.

=item isValid

Check whether the value of this 'Datetime' is valid.

=back
