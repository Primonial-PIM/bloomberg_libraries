# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::Element;

use strict;
use warnings;

use Bloomberg::API::Datetime;
use Bloomberg::API::Exception;
use Bloomberg::API::Internal;
use Bloomberg::API::Name;
use Bloomberg::API::Schema;

use Scalar::Util;

package Bloomberg::API::DataType;
use constant BOOL            => $Bloomberg::API::Internal::BLPAPI_DATATYPE_BOOL;
use constant CHAR            => $Bloomberg::API::Internal::BLPAPI_DATATYPE_CHAR;
use constant BYTE            => $Bloomberg::API::Internal::BLPAPI_DATATYPE_BYTE;
use constant INT32           => $Bloomberg::API::Internal::BLPAPI_DATATYPE_INT32;
use constant INT64           => $Bloomberg::API::Internal::BLPAPI_DATATYPE_INT64;
use constant FLOAT32         => $Bloomberg::API::Internal::BLPAPI_DATATYPE_FLOAT32;
use constant FLOAT64         => $Bloomberg::API::Internal::BLPAPI_DATATYPE_FLOAT64;
use constant STRING          => $Bloomberg::API::Internal::BLPAPI_DATATYPE_STRING;
use constant BYTEARRAY       => $Bloomberg::API::Internal::BLPAPI_DATATYPE_BYTEARRAY;
use constant DATE            => $Bloomberg::API::Internal::BLPAPI_DATATYPE_DATE;
use constant TIME            => $Bloomberg::API::Internal::BLPAPI_DATATYPE_TIME;
use constant DECIMAL         => $Bloomberg::API::Internal::BLPAPI_DATATYPE_DECIMAL;
use constant DATETIME        => $Bloomberg::API::Internal::BLPAPI_DATATYPE_DATETIME;
use constant ENUMERATION     => $Bloomberg::API::Internal::BLPAPI_DATATYPE_ENUMERATION;
use constant SEQUENCE        => $Bloomberg::API::Internal::BLPAPI_DATATYPE_SEQUENCE;
use constant DATATYPE_CHOICE => $Bloomberg::API::Internal::BLPAPI_DATATYPE_CHOICE;
use constant CORRELATION_ID  => $Bloomberg::API::Internal::BLPAPI_DATATYPE_CORRELATION_ID;

package Bloomberg::API::Element;

use constant INDEX_END => 0xffffffff;

sub createInternally {
    my $class = shift;
    my ($arg) = @_;
    $arg->isa("_p_blpapi_Element") or die("Can be used Internally only");
    my ($self) = {
        d_element_t => $arg
        };
    bless $self, $class;
    return $self;
}

sub name
{
    my $self = shift;
    my $name_t = Bloomberg::API::Internal::blpapi_Element_name($self->{d_element_t});
    return Bloomberg::API::Name->createInternally($name_t);
}

sub elementDefinition
{
    my $self = shift;
    my $schemaElementDefinition_t =
        Bloomberg::API::Internal::blpapi_Element_definition($self->{d_element_t});
    if (defined $schemaElementDefinition_t) {
        return Bloomberg::API::SchemaElementDefinition->createInternally(
            $schemaElementDefinition_t);
    }
    return undef;
}

sub dataType
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_datatype($self->{d_element_t});

}

sub isComplexType
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_isComplexType(
        $self->{d_element_t});
}

sub isArray
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_isArray($self->{d_element_t});
}

sub isReadOnly
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_isReadOnly($self->{d_element_t});
}

sub isNullValue
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    return Bloomberg::API::Internal::blpapi_Element_isNullValue(
        $self->{d_element_t},
        $index);
}

sub isNull
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_isNull($self->{d_element_t});
}

sub numValues
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_numValues($self->{d_element_t});
}

sub numElements
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Element_numElements($self->{d_element_t});

}

# Takes in element name, and 1 to exclude null elements
sub hasElement
{
    my $self = shift;
    my ($element, $exculdeNull) = @_;
    my $elementString = undef;
    my $elementName = undef;
    my $elementClass = Scalar::Util::blessed($element);
    $exculdeNull = 0 if not defined $exculdeNull;
    if(defined $elementClass and $elementClass eq "Bloomberg::API::Name") {
        $elementName = $element->{d_name_t};
    }
    else {
        $elementString = $element;
    }

    my $rcode = Bloomberg::API::Internal::blpapi_Element_hasElementEx(
        $self->{d_element_t},
        $elementString,
        $elementName,
        $exculdeNull,
        0);
    return $rcode;
}

sub print
{
    my $self = shift;
    my ($fileHandle) = @_;
    $fileHandle = \*STDOUT if not defined $fileHandle;

    Bloomberg::API::Internal::blpapi_internal_print_Element(
        $self->{d_element_t}, 0, 4, $fileHandle);
}

sub getElementAt
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my ($element_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getElementAt(
            $self->{d_element_t},
            $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Element->createInternally($element_t) if defined $element_t;

}

sub getElementByName
{
    my $self = shift;
    my ($name) = @_;
    my $nameClass = Scalar::Util::blessed($name);
    my $nameName = undef;
    my $nameString = undef;
    if(defined $nameClass and $nameClass eq "Bloomberg::API::Name") {
        $nameName = $name->{d_name_t};
    }
    else {
        $nameString = $name;
    }
    my ($element_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getElement(
            $self->{d_element_t},
            $nameString,
            $nameName);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Element->createInternally($element_t) if defined $element_t;
}

sub getChoice
{
    my $self = shift;
    my ($element_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getChoice(
            $self->{d_element_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Element->createInternally($element_t) if defined $element_t;
}

sub getValueAsString
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my ($value, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getValueAsString(
            $self->{d_element_t},
            $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $value;
}

sub getValueAsElement
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my ($element_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getValueAsElement(
            $self->{d_element_t},
            $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Element->createInternally($element_t) if defined $element_t;
}

sub getValueAsDatetime
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my $datetime = Bloomberg::API::Datetime->new();
    my $rcode;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        $rcode = Bloomberg::API::Internal::blpapi_internal_Element_getValueAsHighPrecisionDatetime(
            $self->{d_element_t},
            $datetime->{d_datetime_t},
            $index);
    }
    else {
        $rcode = Bloomberg::API::Internal::blpapi_Element_getValueAsDatetime(
            $self->{d_element_t},
            $datetime->{d_datetime_t},
            $index);
    }
    Bloomberg::API::Exception->throwOnError($rcode);
    return $datetime;
}

sub getValueAsFloat64
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my ($value, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getValueAsFloat64(
            $self->{d_element_t},
            $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $value;

}

sub getValue
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my $dataType = $self->dataType();
    my $value;
    my $rcode = 0;
    if ($dataType == Bloomberg::API::DataType::BOOL
        || $dataType == Bloomberg::API::DataType::BYTE
        || $dataType == Bloomberg::API::DataType::INT32
        || $dataType == Bloomberg::API::DataType::INT64)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Element_getValueAsInt64(
                $self->{d_element_t},
                $index);
    }
    elsif ($dataType == Bloomberg::API::DataType::FLOAT32
           || $dataType == Bloomberg::API::DataType::FLOAT64)
    {
        ($value, $rcode) =
            Bloomberg::API::Internal::blpapi_internal_Element_getValueAsFloat64(
                $self->{d_element_t},
                $index);
    }
    elsif ($dataType == Bloomberg::API::DataType::ENUMERATION) {
        ($value, $rcode)  = $self->getValueAsName($index);
    }
    elsif ($dataType == Bloomberg::API::DataType::DATE
           || $dataType == Bloomberg::API::DataType::TIME
           || $dataType == Bloomberg::API::DataType::DATETIME)
    {
        ($value, $rcode) = $self->getValueAsDatetime($index);
    }
    elsif ($dataType == Bloomberg::API::DataType::SEQUENCE
           || $dataType == Bloomberg::API::DataType::DATATYPE_CHOICE)
    {
        ($value, $rcode) = $self->getValueAsElement($index);
    }
    else {
        ($value, $rcode) = $self->getValueAsString($index);
    }
    Bloomberg::API::Exception->throwOnError($rcode);
    return $value;
}

sub getValueAsName
{
    my $self = shift;
    my ($index) = @_;
    $index = 0 if !defined $index;
    my ($name_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_getValueAsName(
            $self->{d_element_t},
            $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Name->createInternally($name_t);
}

sub getElementAsString
{
    my $self = shift;
    my ($string) = @_;
    $string = $string."";
    my $elem =  $self->getElementByName($string);
    return $elem->getValueAsString() if defined $elem;
}

sub getElementAsDatetime
{
    my $self = shift;
    my ($string) = @_;
    $string = $string."";
    my $elem =  $self->getElementByName($string);
    return $elem->getValueAsDatetime() if defined $elem;
}

sub getElementAsFloat64
{
    my $self = shift;
    my ($string) = @_;
    $string = $string."";
    my $elem =  $self->getElementByName($string);
    return $elem->getValueAsFloat64() if defined $elem;
}

sub getElementValue
{
    my $self = shift;
    my ($string) = @_;
    $string = $string."";
    my $elem =  $self->getElementByName($string);
    return $elem->getValue() if defined $elem;
}

sub getElementAsName
{
    my $self = shift;
    my ($string) = @_;
    $string = $string."";
    my $elem =  $self->getElementByName($string);
    return $elem->getValueAsName() if defined $elem;
}

sub setValue
{
    my $self = shift;
    my ($value, $index) = @_;
    $index = 0 if !defined $index;
    my $rcode;
    my $valueClass = Scalar::Util::blessed($value);
    if (not defined $valueClass) {
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_Element_setValueString(
            $self->{d_element_t},
            $value,
            $index);
    }
    elsif ($value->isa("Bloomberg::API::Datetime")) {
        $self->setValueDatetimePrivate($value, $index);
    }
    elsif ($value->isa("Bloomberg::API::Name")) {
        $rcode = Bloomberg::API::Internal::blpapi_Element_setValueFromName(
            $self->{d_element_t},
            $value->{d_name_t},
            $index);
    }

    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub setElement
{
    my $self = shift;
    my ($string, $value) = @_;
    $string = $string."";
    my $rcode = 1;
    my $valueClass = Scalar::Util::blessed($value);
    if (not defined $valueClass) {
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_Element_setElementString(
            $self->{d_element_t},
            $string,
            undef,
            $value);
    }
    elsif ($value->isa("Bloomberg::API::Datetime")) {
        if (Bloomberg::API::Exception->checkVersion(30500)) {
            $rcode = Bloomberg::API::Internal::blpapi_internal_Element_setElementHighPrecisionDatetime(
                $self->{d_element_t},
                $string,
                undef,
                $value->{d_datetime_t});
        }
        else {
            $rcode = Bloomberg::API::Internal::blpapi_Element_setElementDatetime(
                $self->{d_element_t},
                $string,
                undef,
                $value->{d_datetime_t});
        }
    }
    elsif ($value->isa("Bloomberg::API::Name")) {
        $rcode = Bloomberg::API::Internal::blpapi_Element_setElementFromName(
            $self->{d_element_t},
            $string,
            undef,
            $value->{d_name_t});
    }

    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub setChoice
{
    my $self = shift;
    my ($name) = @_;
    $name = $name."";
    my $rcode = Bloomberg::API::Internal::blpapi_internal_Element_setChoice(
        $self->{d_element_t},
        $name);
    return $rcode;
}

sub appendElement
{
    my $self = shift;
    my ($newElement_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Element_appendElement(
        $self->{d_element_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Element->createInternally($newElement_t);

}

sub appendValue
{
    my $self = shift;
    my ($value) = @_;
    my $valueClass = Scalar::Util::blessed($value);
    my $rcode = -1;
    if ((not defined $valueClass) || $valueClass eq "Bloomberg::API::Name" ) {
        $value = $value."";
        $rcode = Bloomberg::API::Internal::blpapi_Element_setValueString(
            $self->{d_element_t},
            $value,
            $Bloomberg::API::Internal::BLPAPI_ELEMENT_INDEX_END);
    }
    elsif ($value->isa("Bloomberg::API::Datetime")) {
        $self->setValueDatetimePrivate($value, INDEX_END);
    }
    return Bloomberg::API::Exception->throwOnError($rcode);
}

# private
sub setValueDatetimePrivate
{
    my $self = shift;
    my ($value, $index) = @_;
    if (Bloomberg::API::Exception->checkVersion(30500)) {
        return Bloomberg::API::Internal::blpapi_internal_Element_setValueAsHighPrecisionDatetime(
            $self->{d_element_t},
            $value->{d_datetime_t},
            $index);
    }
    return Bloomberg::API::Internal::blpapi_Element_setValueDatetime(
        $self->{d_element_t},
        $value->{d_datetime_t},
        $index);
    
}
1;
__END__

=pod

=head1 NAME

Bloomberg::API::Element - A representation of an item in a message.

=head1 DESCRIPTION

Element represents an item in a message.

An Element can represent: a single value of any data type
supported by the Bloomberg API; an array of values; a sequence
or a choice.

The value(s) in an Element can be queried in a number of
ways. For an Element which represents a single value or an
array of values use the getValueAs() functions,
getValueAsBool(), etc. For an Element which represents a
sequence or choice use getElementValue(), etc. In addition, for
choices and sequences, hasElement() and getElement() are
useful.

This example shows how to access the value of a
scalar element 's':

..
     $f = $s->getValue();
..

Similarly, this example shows how to retrieve the third value
in an array element 'a', as a string:

..
     $f = $a->getValueAsString(2);
..

Use numValues() to determine the number of values
available. For single values, it will return either 0 or 1. For
arrays it will return the actual number of values in the array.

To retrieve values from a complex element types (sequences and
choices) use the getElementAs...() family of methods. This
example shows how to get the value of the element 'city' in
the sequence element 'address':

..
     $city = address->getElementAsString("city");
..

The value(s) of an Element can be set in a number of ways. For
an Element which represents a single value or an array of
values use the setValue() or appendValue() functions. For an
element which represents a sequence or a choice use the
setElement() functions.

This example shows how to set the value of an Element 's'
from an integer:
..
     $value=5;
     s->setValue(value);
..
This example shows how to append an integer a value to an array
element 'a':
..
     $value=5;
     s->appendValue(value);
..
To set values in a complex element (a sequence or a choice)
use the setElement() family of functions. This example shows
how to set the value of the element 'city' in the sequence
element 'address' to a string.
..
     $address = Bloomberg::API::Element=>new();
     ...
     $address->setElement("city", "New York");
..
Methods which specify a Element name come in two forms. One
which takes a Name as a parameter and one that takes a string.
The form that takes Name is more efficient. However, it
requires the Name to have been created in the global name
table. Both Name and  string can be passed to the same function
signatures.

The form that takes a string is less efficient but will
not cause a new Name to be created in the global Name
table. Because all valid Element names will have already been
placed in the global name table by the API if the supplied
string cannot be found in the global name table the appropriate
error or exception can be returned.

The API will convert data types as long as there is no loss of
precision involved.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item name

If this Element is part of a sequence or choice Element
then this returns the Name (Bloomberg::API::Name) of this Element within the
sequence or choice Element that owns it. If this Element
is not part of a sequence Element (that is, it is an entire
Request or Message) then the Name of the Request or Message
is returned.

=item dataType

Return the basic data type used to represent a value in
this element. The possible return values are enumerated in
DataType.

=item isComplexType

Returns true if datatype()==DataType::SEQUENCE or
datatype()==DataType::CHOICE.

=item isArray

Returns true if elementDefinition().maxValues()>1 or if
elementDefinition().maxValues()==UNBOUNDED.

=item isReadOnly

Returns true if this element cannot be modified.

=item isNullValue

Returns true if the value at the specified 'index' is a
"NULL" value. An exception is thrown if
'index'>=numValues().

=item isNull

Returns true if this element is in a NULL state. Otherwise
returns false.

=item numValues

Return the number of values contained in this element.  For
scalar element types, the value returned will always be in
the range defined by elementDefinition().minValues() and
elementDefinition().maxValues().

=item hasElement

Returns true if this element is a choice or sequence
(isComplexType()==true) and it contains an Element with the
specified 'name'.
Takes in element name, and optional 1 to exclude null elements

=item print

Prints the element to the optionally specified opened file stream.
If no file stream is provided, STDOUT will be used as the file stream.

=item getElementAt($position)

If this Element is either a sequence or a choice and
numElements() is greater than the specified 'position'
return the "position'th" element at the location specified
by 'result' and return 0. Otherwise, an exception is thrown.
If 'position' is not passed, the element at 0th position is returned.

=item getElementByName($name)

If this Element is either a sequence or a choice and
contains a valid Element identified by the specified 'name'
return that Element at the location specified by 'result'
and return 0. Otherwise, an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.

=item getChoice

Return the selection name of this element as Bloomberg::API::Element
if this element is a "choice" element.  Throw exception otherwise.

=item getValue($index)

Returns the specified 'index'th entry in the Element.
The returned value could be a scalar, Datetime, Element or Name
based on the data type of the element (present in the schema)
The 'index' is taken to be 0 if not specified.
Note that, if the datatype of the element is SEQUENCE or DATATYPE_CHOICE,
this method is equivalent to getValueAsElement.

=item getValueAsString($index)

Returns the specified 'index'th entry in the Element as a
String. An exception is thrown if the DataType of this
Element cannot be converted to String or if 'index' is
greater than the current size of this Element
(numValues()).
The 'index' is taken to be 0 if not specified.

=item getValueAsElement($index)

Returns the specified 'index'th entry in the Element as an
Element. An exception is thrown if the DataType of this
Element cannot be converted to an Element or if 'index' is
greater than the current size of this Element
(numValues()).
The 'index' is taken to be 0 if not specified.

=item getValueAsDatetime($index)

Returns the specified 'index'th entry in the Element as a
Datetime. An exception is thrown if the DataType of this
Element cannot be converted to Datetime or if 'index' is
greater than the current size of this Element
(numValues()).
The 'index' is taken to be 0 if not specified.

=item getValueAsFloat64($index)

Returns the specified 'index'th entry in the Element as a
Float64. An exception is thrown if the DataType of this
Element cannot be converted to Float64 or if 'index' is
greater than the current size of this Element
(numValues()).
The 'index' is taken to be 0 if not specified.
This method is deprecated and getValue() should be used instead.

=item getValueAsName($index)

Returns the specified 'index'th entry in the Element as a
Name. An exception is thrown if the DataType of this
Element cannot be converted to Name or if 'index' is
greater than the current size of this Element
(numValues()).

=item getElementValue($name)

Return a scalar, Datetime, Element or Name (based on the datatype)
if this Element is either a sequence or a choice and
contains a valid Element identified by the specified 'name'.
Otherwise an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.

=item getElementAsString($name)

If this Element is either a sequence or a choice and
contains a valid Element identified by the specified 'name',
a string is returned. Otherwise an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.

=item getElementAsDatetime($name)

If this Element is either a sequence or a choice and
contains a valid Element identified by the specified 'name'
which can be returned as Datetime then its value is
returned. Otherwise an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.

=item getElementAsFloat64($name)

If this Element is either a sequence or a choice and
contains a valid Element identified by the specified 'name'
which can be returned as a Float64 then its value is
returned. Otherwise an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.
This method is deprecated and getElementValue() should be used
instead.

=item getElementAsName($name)

If this Element is either a sequence or a choice and
contains a valid Element identified by the specified 'name'
which can be returned as a Name then its value is
returned. Otherwise an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.

=item setValue($value, $index=0)

Set the value of the specified 'index'th entry in this
Element to the specified 'value'. An exception is thrown if
this Element's DataType means it cannot be initialized from
an instance of the supplied 'value'. An exception is thrown
if 'index'>=numValues().
The 'index' is taken to be 0 if not specified.

=item setElement($name, $value)

Set the specified element 'name' within this sequence or
choice Element to the specified 'value'. An exception is
thrown if the specified 'name' is not valid for this
Element, or if the Element identified by the specified
'name' cannot be initialized from the type of the specified
'value'.
Note: For boolean value "true" or "false" must be entered.
The 'name' can be a string or Bloomberg::API::Name.


=item setChoice($name)

If this Element has datatype()==DataType::CHOICE then set
the active Element to the one specified by 'selectionName'
if 'selectionName' is valid for this Element and return a
reference to it. Otherwise an exception is thrown.
The 'name' can be a string or Bloomberg::API::Name.

=item appendElement()

If this Element is an array of sequence or choice Elements
append an element to this Element and return the appended
Element. Otherwise an exception is thrown.

=item appendValue($value)

Appends the specified 'value' to this Element as the last
element. An exception is thrown if this Element's DataType
means it cannot be initialized from an instance of the
supplied 'value', or if the current size of this Element
(numValues()) is equal to the maximum defined by
elementDefinition().maxValues().


=item elementDefinition

Return a reference to the read-only element definition
object that defines the properties of this elements value.

=back

=cut

