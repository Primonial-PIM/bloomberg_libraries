# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::AbstractSession;
use Bloomberg::API::CorrelationId;
use Bloomberg::API::Error;
use Bloomberg::API::Event;
use Bloomberg::API::Exception;
use Bloomberg::API::Identity;
use Bloomberg::API::Internal;
use Bloomberg::API::Request;
use Bloomberg::API::Service;

package Bloomberg::API::Session;

our @ISA = qw(Bloomberg::API::AbstractSession);

sub new
{
    my $class = shift;
    my ($sessionOptions) = @_;
    my $sessionHandle = Bloomberg::API::Internal::blpapi_internal_Session_create(
        $sessionOptions->{d_sessionOptions_t});

    my $abstractSession = Bloomberg::API::Internal::blpapi_Session_getAbstractSession(
        $sessionHandle);
    my $self = {
        d_session_t          => $sessionHandle,
        d_abstractsession_t  => $abstractSession,
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_Session_destroy($self->{d_session_t});

}

sub stop
{
    my $self = shift;
    return !Bloomberg::API::Internal::blpapi_Session_stop($self->{d_session_t});
}

sub stopAsync
{
    my $self = shift;
    return !Bloomberg::API::Internal::blpapi_Session_stopAsync($self->{d_session_t});
}

sub start
{
     my $self = shift;
     my $rcode = Bloomberg::API::Internal::blpapi_Session_start($self->{d_session_t});
     return !$rcode;
}

sub startAsync
{
     my $self = shift;
     my $rcode = Bloomberg::API::Internal::blpapi_Session_startAsync(
         $self->{d_session_t});
     return !$rcode;
}

sub nextEvent
{
    my $self = shift;
    my ($timeOut) = @_;
    $timeOut = 0 if (!defined $timeOut);
    my ($eventHandle, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Session_nextEvent(
                                                       $self->{d_session_t},
                                                       $timeOut);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Event->createInternally($eventHandle);
}

sub tryNextEvent
{
    my $self = shift;
    my ($eventHandle, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_Session_tryNextEvent(
                                                      $self->{d_session_t});
    if($rcode) {
        return undef;
    }
    return Bloomberg::API::Event->createInternally($eventHandle);
}

# subscribe($list, $identity, $requestLabel)
sub subscribe
{
    my $self = shift;

    my ($list, $identity, $requestLabel) = @_;
    if (defined $list) {
        $requestLabel = "" if !defined $requestLabel;
        my $requestLabelLen = length ($requestLabel);

        my $rcode =
            Bloomberg::API::Internal::blpapi_Session_subscribe(
                $self->{d_session_t},
                $list->{d_subscriptionList_t},
                (!defined $identity)?undef:$identity->{d_identity_t},
                $requestLabel,
                $requestLabelLen);
        return Bloomberg::API::Exception->throwOnError($rcode);
    }
    # Illegal args
    return Bloomberg::API::Exception->throwException(
        Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS,
        "Need atleast the SubscriptionList as args for subscribe");
}

sub resubscribe
{
    my $self = shift;

    my ($subList, $requestLabel) = @_;
    $requestLabel = "" if !defined $requestLabel;
    my $requestLabelLen = length ($requestLabel);
    my $rcode =
        Bloomberg::API::Internal::blpapi_Session_resubscribe(
            $self->{d_session_t},
            $subList->{d_subscriptionList_t},
            $requestLabel,
            $requestLabelLen);
    return Bloomberg::API::Exception->throwOnError($rcode);
}

sub unsubscribe
{
    my $self = shift;

    my ($subList) = @_;
    my $requestLabel = "";
    my $requestLabelLen = 0;
    my $rcode =
        Bloomberg::API::Internal::blpapi_Session_unsubscribe(
            $self->{d_session_t},
            $subList->{d_subscriptionList_t},
            $requestLabel,
            $requestLabelLen);
    return Bloomberg::API::Exception->throwOnError($rcode);
}

#sendRequest($request, $identity, $correlationId, $eventQueue, $requestLabel)
sub sendRequest
{
    my $self = shift;
    my ($request, $identity, $cid, $eventQueue, $requestLabel) = @_;
    if (defined $request) {

        if (!defined $cid) {
            $cid = Bloomberg::API::CorrelationId->new();
        }

        if (defined $identity) {
            $identity = $identity->{d_identity_t};
        }

        if (defined $eventQueue) {
            $eventQueue = $eventQueue->{d_eventQueue_t};
        }
        $requestLabel = "" if !defined $requestLabel;
        my $requestLabelLen = length ($requestLabel);

        my $rcode =
                Bloomberg::API::Internal::blpapi_Session_sendRequest(
                    $self->{d_session_t},
                    $request->{d_request_t},
                    $cid->{d_correlationId_t},
                    $identity,
                    $eventQueue,
                    $requestLabel,
                    $requestLabelLen);

        Bloomberg::API::Exception->throwOnError($rcode);
        return $cid;
    }
    # Illegal args
    return Bloomberg::API::Exception->throwException(
        Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS,
        "Need atleast the Request as args for sendRequest");
}

package Bloomberg::API::SubscriptionStatus;

use constant UNSUBSCRIBED =>
    $Bloomberg::API::Internal::BLPAPI_SUBSCRIPTIONSTATUS_UNSUBSCRIBED;
    # No longer active, terminated by API.
use constant SUBSCRIBING =>
    $Bloomberg::API::Internal::BLPAPI_SUBSCRIPTIONSTATUS_SUBSCRIBING;
    # Initiated but no updates received.
use constant SUBSCRIBED =>
    $Bloomberg::API::Internal::BLPAPI_SUBSCRIPTIONSTATUS_SUBSCRIBED;
    # Updates are flowing.
use constant CANCELLED =>
    $Bloomberg::API::Internal::BLPAPI_SUBSCRIPTIONSTATUS_CANCELLED;
    # No longer active, terminated by Application.
use constant PENDING_CANCELLATION =>
    $Bloomberg::API::Internal::BLPAPI_SUBSCRIPTIONSTATUS_PENDING_CANCELLATION;

package Bloomberg::API::SubscriptionIterator;

sub new
{
    my $class = shift;
    my ($session) = @_;
    my $subIter_t = Bloomberg::API::Internal::blpapi_SubscriptionItr_create(
        $session->{d_session_t});
    my $self = {
        d_subscriptionIterator_t => $subIter_t,
        d_isValid => 0,
        d_correlationId => Bloomberg::API::CorrelationId->new(),
        d_subscriptionString => "",
        d_status => Bloomberg::API::SubscriptionStatus::UNSUBSCRIBED
    };

    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_SubscriptionItr_destroy(
        $self->{d_subscriptionIterator_t});
}

sub next
{
    my $self = shift;
    ($self->{d_isValid}, $self->{d_subscriptionString}, $self->{d_status}) =
        Bloomberg::API::Internal::blpapi_SubscriptionItr_next(
            $self->{d_subscriptionIterator_t},
            $self->{d_correlationId}->{d_correlationId_t});
    $self->{d_isValid} = !$self->{d_isValid};
    return $self->{d_isValid};
}

sub isValid {
    my $self = shift;
    return $self->{d_isValid};
}

sub subscriptionString
{
    my $self = shift;
    if (!$self->{d_isValid}) {
        return undef;
    }
    return $self->{d_subscriptionString};
}

sub correlationId
{
    my $self = shift;
    if (!$self->{d_isValid}) {
        return undef;
    }
    return $self->{d_correlationId};
}

sub subscriptionStatus
{
    my $self = shift;
    if (!$self->{d_isValid}) {
        return undef;
    }
    return $self->{d_status};
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Session - Provide consumer session to get Bloomberg Services

=head1 DESCRIPTION

Sessions manage access to services either by requests and
responses or subscriptions.

Several methods in a Session take a CorrelationId parameter. The
application may choose to supply its own CorrelationId values
or allow the Session to create values. If the application
supplies its own CorrelationId values it must manage their
lifeTimes such that the same value is not reused for more than
one operation at a time. The lifetime of a CorrelationId begins
when it is supplied in a method invoked on a Session and ends
either when it is explicitly cancelled using cancel() or
unsubscribe(), when a RESPONSE Event (not a PARTIAL_RESPONSE)
containing it is received or when a SUBSCRIPTION_STATUS Event
which indicates that the subscription it refers to has been
terminated is received.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item start

Attempt to start this Session and blocks until the Session
has started or failed to start. If the Session is started
successfully 'true' is returned, otherwise 'false' is
returned. Before start() returns a SESSION_STATUS Event is
generated. A Session may only be started once.

=item startAsync

Attempt to begin the process to start this Session and
return 'true' if successful, otherwise return 'false'. The
application must monitor events for a SESSION_STATUS Event
which will be generated once the Session has started or if
the Session fails to start. A Session may only be started once.

=item stop

Stop operation of this session. Once a Session
has been stopped it can only be destroyed.

=item stopAsync

Begin the process to stop this Session and return
immediately. The application must monitor events for a
SESSION_STATUS Event which will be generated once the
Session has been stopped. Once a Session has been stopped
it can only be destroyed.

=item nextEvent($timeout = 0)

Returns the next available Event for this session. If there
is no event available this will block for up to the
specified 'timeoutMillis' milliseconds for an Event to
arrive. A value of 0 for 'timeoutMillis' (the default)
indicates nextEvent() should not timeout and will not
return until the next Event is available.

If nextEvent() returns due to a timeout it will return an
event of type 'EventType::TIMEOUT'.

=item tryNextEvent

If there are Events available for the session, returns the next Event
indicating success. If there is no event available for the session,
return an undef value. This method never blocks.

=item subscribe

subscribe($list <Bloomberg::API::SubscriptionList>,
          $identity <Bloomberg::API::Identity> #optional (can be undef),
          $requestLabel "" #optional (can be undef))

Begin subscriptions for each entry in the specified
'subscriptionList' using the specified 'identity' for
authorization. If the optional 'requestLabel' is  provided it
will be recorded along with any diagnostics for this
operation.

A SUBSCRIPTION_STATUS Event will be generated for each
entry in the 'subscriptionList'.

=item resubscribe($subscriptionList, $requestLabel="")

Modify each subscription in the specified
'subscriptionList' to reflect the modified options
specified for it. If the optional 'requestLabel' and
is provided it will be recorded along with any diagnostics for this
operation.

For each entry in the 'subscriptionList' which has a
correlation ID which identifies a current subscription the
modified options replace the current options for the
subscription and a SUBSCRIPTION_STATUS event will be
generated in the event stream before the first update based
on the new options. If the correlation ID of an entry in
the 'subscriptionList' does not identify a current
subscription then that entry is ignored.

=item unsubscribe($subscriptionList<Bloomberg::API::SubscriptionList>)

Cancel each of the current subscriptions identified by the
specified 'subscriptionList'. If the correlation ID of any
entry in the 'subscriptionList' does not identify a current
subscription then that entry is ignored. All entries which
have valid correlation IDs will be cancelled.

Once this call returns the correlation ids in the
'subscriptionList' will not be seen in any subsequent
Message obtained from a MessageIterator by calling
next(). However, any Message currently pointed to by a
MessageIterator when unsubscribe() is called is not
affected even if it has one of the correlation IDs in the
'subscriptionList'. Also any Message where a reference has
been retained by the application may still contain a
correlation ID from the 'subscriptionList'. For these
reasons, although technically an application is free to
re-use the correlation IDs as soon as this method returns
it is preferable not to aggressively re-use correlation
IDs.

=item sendRequest

sendRequest($request <Bloomberg::API::Request>,
            $identity <Bloomberg::API::Identity> #optinal,
            $correlationId <Bloomberg::API::CorrelationId> #optional,
            $eventQueue <Bloomberg::API::EventQueue> #optional,
            $requestLabel "" #optinal)

Send the specified 'request' using the optinally specified
'identity' for authorization. If the optionally specified
'correlationId' is supplied use it otherwise create a
CorrelationId. The actual CorrelationId used is
returned. If the optionally specified 'eventQueue' is
supplied all events relating to this Request will arrive on
that EventQueue. If the optional 'requestLabel' is provided
it will be recorded along with any diagnostics for this
operation.

A successful request will generate zero or more
PARTIAL_RESPONSE Messages followed by exactly one RESPONSE
Message. Once the final RESPONSE Message has been received
the CorrelationId associated with this request may be
re-used. If the request fails at any stage a REQUEST_STATUS
will be generated after which the CorrelationId associated
with the request may be re-used.

=back

=pod

=head1 NAME

Bloomberg::API::SubscriptionIterator - Iterator for subscriptions

=head1 DESCRIPTION

An iterator which steps through all the subscriptions in a Session.

The SubscriptionIterator can be used to iterate over all the
active subscriptions for a Session. The SubscriptionIterator is
guaranteed to never return the same subscription twice.
However, the subscription the iterator points to may no longer be active.
In this case the result of subscriptionStatus() will be UNSUBSCRIBED or
CANCELLED.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item next

Attempt to advance this iterator to the next subscription
record.  Returns 'true' on success and 'false' if there are
no more subscriptions. After next() returns true, isValid()
is guaranteed to return true until the next call to
next(). After next() returns false, isValid() will return
false.

=item isValid

Returns true if this iterator is currently positioned on a
valid subscription.  Returns false otherwise.

=item subscriptionString

Returns the subscription string for this subscription.

=item correlationId

Returns the CorrelationId for this subscription.

=item subscriptionStatus

Returns the status of this subscription.

=back
