# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::AbstractSession;
use Bloomberg::API::CorrelationId;
use Bloomberg::API::Error;
use Bloomberg::API::Event;
use Bloomberg::API::Exception;
use Bloomberg::API::Identity;
use Bloomberg::API::Internal;
use Bloomberg::API::ResolutionList;
use Bloomberg::API::Topic;
use Bloomberg::API::TopicList;

package Bloomberg::API::ProviderSession;

package Bloomberg::API::ServiceRegistrationPriority;

use constant PRIORITY_LOW =>
    $Bloomberg::API::Internal::BLPAPI_SERVICEREGISTRATIONOPTIONS_PRIORITY_LOW;
use constant PRIORITY_MEDIUM =>
    $Bloomberg::API::Internal::BLPAPI_SERVICEREGISTRATIONOPTIONS_PRIORITY_MEDIUM;
use constant PRIORITY_HIGH =>
    $Bloomberg::API::Internal::BLPAPI_SERVICEREGISTRATIONOPTIONS_PRIORITY_HIGH;

package Bloomberg::API::ProviderSession;

our @ISA = qw(Bloomberg::API::AbstractSession);

use constant AUTO_REGISTER_SERVICES 
            => $Bloomberg::API::Internal::BLPAPI_RESOLVEMODE_AUTO_REGISTER_SERVICES;
use constant DONT_REGISTER_SERVICES 
            => $Bloomberg::API::Internal::BLPAPI_RESOLVEMODE_DONT_REGISTER_SERVICES;

sub new
{
    my $class = shift;
    my ($sessionOptions) = @_;
    $sessionOptions = Bloomberg::API::SessionOptions->new()
        if !defined $sessionOptions;
    my $sessionHandle =
        Bloomberg::API::Internal::blpapi_internal_ProviderSession_create(
            $sessionOptions->{d_sessionOptions_t});

    my $abstractSession =
        Bloomberg::API::Internal::blpapi_ProviderSession_getAbstractSession(
        $sessionHandle);
    my $self = {
        d_session_t => $sessionHandle,
        d_abstractsession_t => $abstractSession,
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_ProviderSession_destroy(
                                                  $self->{d_session_t});
}

sub stop
{
    my $self = shift;
    return !Bloomberg::API::Internal::blpapi_ProviderSession_stop($self->{d_session_t});
}

sub stopAsync
{
    my $self = shift;
    return !Bloomberg::API::Internal::blpapi_ProviderSession_stopAsync($self->{d_session_t});
}

sub start
{
     my $self = shift;
     my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_start(
         $self->{d_session_t});
     return !$rcode;
}

sub startAsync
{
     my $self = shift;
     my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_startAsync(
         $self->{d_session_t});
     return !$rcode;
}

sub nextEvent
{
    my $self = shift;
    my ($timeOut) = @_;
    $timeOut = 0 if (!defined $timeOut);
    my ($eventHandle, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_ProviderSession_nextEvent(
                                                       $self->{d_session_t},
                                                       $timeOut);
    Bloomberg::API::Exception::throwOnError($rcode);
    return Bloomberg::API::Event->createInternally($eventHandle);
}

sub tryNextEvent
{
    my $self = shift;
    my ($eventHandle, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_ProviderSession_tryNextEvent(
                                                      $self->{d_session_t});
    if($rcode) {
        return undef;
    }
    return Bloomberg::API::Event->createInternally($eventHandle);
}

# registerService(uri, identity, {groupId => "groupId", priority=><number>})
sub registerService
{
    my $self = shift;
    my ($uri, $identity, $options) = @_;
    if (defined $uri) {
        if(defined $identity) {
            $identity = $identity->{d_identity_t};
        }
        my $serviceRegistrationOption_t = undef;
        if(defined $options) {
            $serviceRegistrationOption_t =
                Bloomberg::API::Internal::blpapi_ServiceRegistrationOptions_create();
            if (exists $options->{groupId} && defined($options->{groupId})) {
                Bloomberg::API::Internal::blpapi_ServiceRegistrationOptions_setGroupId(
                    $serviceRegistrationOption_t,
                    $options->{groupId},
                    length($options->{groupId}));
            }
            if (exists $options->{priority}) {
                Bloomberg::API::Internal::blpapi_ServiceRegistrationOptions_setServicePriority(
                    $serviceRegistrationOption_t,
                    $options->{priority});
            }
        }
        my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_registerService(
            $self->{d_session_t},
            $uri,
            $identity,
            $serviceRegistrationOption_t);
        return !$rcode;

    }
    return Bloomberg::API::Exception->throwException(
        Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS,
        "Need atleast the uri as args for registerService");
}

# registerService(uri, identity, correlationId,
#                 {groupId => "groupId", priority=><number>})
sub registerServiceAsync
{
    my $self = shift;
    my ($uri, $identity, $cid, $options) = @_;
    if (defined $uri) {
        if(defined $identity) {
            $identity = $identity->{d_identity_t};
        }
        my $serviceRegistrationOption_t = undef;
        if(defined $options) {
            $serviceRegistrationOption_t =
                Bloomberg::API::Internal::blpapi_ServiceRegistrationOptions_create();
            if (exists $options->{groupId}) {
                Bloomberg::API::Internal::blpapi_ServiceRegistrationOptions_setGroupId(
                    $serviceRegistrationOption_t,
                    $options->{groupId},
                    length($options->{groupId}));
            }
            if (exists $options->{priority}) {
                Bloomberg::API::Internal::blpapi_ServiceRegistrationOptions_setServicePriority(
                    $serviceRegistrationOption_t,
                    $options->{priority});
            }
        }
        if (not defined $cid) {
            $cid = Bloomberg::API::CorrelationId->new();
        }
        my $rcode =
            Bloomberg::API::Internal::blpapi_ProviderSession_registerServiceAsync(
                $self->{d_session_t},
                $uri,
                $identity,
                $cid->{d_correlationId_t},
                $serviceRegistrationOption_t);
        Bloomberg::API::Exception::throwOnError($rcode);
        return $cid;
    }
    return Bloomberg::API::Exception->throwException(
        Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS,
        "Need atleast the uri as args for registerServiceAsync");
}

# resolve(resolutionList, resolveMode = undef, identity = undef)
sub resolve
{
    my $self = shift;
    my ($resolutionList, $resolveMode, $identity) = @_;
    if (not defined $resolveMode) {
        $resolveMode = Bloomberg::API::ProviderSession::DONT_REGISTER_SERVICES;
    }
    if(defined $identity) {
        $identity = $identity->{d_identity_t};
    }

    if (defined($resolutionList) &&
        $resolutionList->isa("Bloomberg::API::ResolutionList")) {

        my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_resolve(
                $self->{d_session_t},
                $resolutionList->{d_resolutionList_t},
                $resolveMode,
                $identity);
        Bloomberg::API::Exception->throwOnError($rcode);
    }
    else {
        Bloomberg::API::Exception->throwException(
            Bloomberg::API::Error::BLPAPI_ERROR_ILLEGAL_ARG,
            "Invalid arguments in resolve");
    }
}

# resolveAsync(Bloomberg::API::ResolutionList, resolveMode = undef,
#              Bloomberg::API::Identity = undef)
sub resolveAsync
{
    my $self = shift;
    my ($resolutionList, $resolveMode, $identity) = @_;
    if (not defined $resolveMode) {
        $resolveMode = Bloomberg::API::ProviderSession::DONT_REGISTER_SERVICES;
    }
    if(defined $identity) {
        $identity = $identity->{d_identity_t};
    }

    if (defined $resolutionList and
        $resolutionList->isa("Bloomberg::API::ResolutionList")) {
        my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_resolveAsync(
                $self->{d_session_t},
                $resolutionList->{d_resolutionList_t},
                $resolveMode,
                $identity);
        Bloomberg::API::Exception->throwOnError($rcode);
    }
    else {
        Bloomberg::API::Exception->throwException(
            Bloomberg::API::Error::BLPAPI_ERROR_ILLEGAL_ARG,
            "Invalid arguments in resolveAsync");
    }

}

# createTopics(Bloomberg::API::TopicList, resolveMode = undef, Bloomberg::API::Identity=undef)
sub createTopics
{
    my $self = shift;
    my ($topicList, $resolveMode, $identity) = @_;
    if (not defined $resolveMode) {
        $resolveMode = Bloomberg::API::ProviderSession::DONT_REGISTER_SERVICES;
    }
    if(defined $identity) {
        $identity = $identity->{d_identity_t};
    }

    if (defined $topicList) {
        my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_createTopics(
                $self->{d_session_t},
                $topicList->{d_topicList_t},
                $resolveMode,
                $identity);
        Bloomberg::API::Exception->throwOnError($rcode);
    }
    else {
        Bloomberg::API::Exception->throwException(
            Bloomberg::API::Error::BLPAPI_ERROR_ILLEGAL_ARG,
            "Invalid arguments in createTopics");
    }
}

# createTopicsAsync(Bloomberg::API::TopicList, resolveMode = undef,
#                   Bloomberg::API::Identity=undef)
sub createTopicsAsync
{
    my $self = shift;
    my ($topicList, $resolveMode, $identity) = @_;
    if (not defined $resolveMode) {
        $resolveMode = Bloomberg::API::ProviderSession::DONT_REGISTER_SERVICES;
    }
    if(defined $identity) {
        $identity = $identity->{d_identity_t};
    }

    if (defined $topicList) {

        my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_createTopicsAsync(
                $self->{d_session_t},
                $topicList->{d_topicList_t},
                $resolveMode,
                $identity);
        Bloomberg::API::Exception->throwOnError($rcode);
    }
    else {
        Bloomberg::API::Exception->throwException(
            Bloomberg::API::Error::BLPAPI_ERROR_ILLEGAL_ARG,
            "Invalid arguments in createTopicsAsync");
    }
}

sub getTopic
{
    my $self = shift;
    my ($message) = @_;
    my ($topic_t, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_ProviderSession_getTopic(
                $self->{d_session_t}, $message->{d_message_t});
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Topic->createInternally($topic_t) if defined $topic_t;
    return undef;
}

sub publish
{
    my $self = shift;
    my ($event) = @_;
    my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_publish(
            $self->{d_session_t}, $event->{d_event_t});
    Bloomberg::API::Exception->throwOnError($rcode);
}

sub sendResponse
{
    my $self = shift;
    my ($event, $isPartialResponse) = @_;
    $isPartialResponse = 0 if !defined $isPartialResponse;
    my $rcode = Bloomberg::API::Internal::blpapi_ProviderSession_sendResponse(
        $self->{d_session_t},
        $event->{d_event_t},
        $isPartialResponse);
    Bloomberg::API::Exception->throwOnError($rcode);
}

1;

__END__;

=pod

=head1 NAME

Bloomberg::API::ProviderSession - A session that can be used for providing services

=head1 DESCRIPTION

This class provides a session that can be used for providing services.

It inherits from AbstractSession. In addition to the AbstractSession
functionality a ProviderSession provides the following
functions to application:

- Applications can register to provide Services using either
registerService() or registerServiceAsync(). Before registering
to provide a Service an application must have established its
identity.

- Applications start publishing by calling createTopics or createTopicsAsync
with a list of topics.

Events contain Messages addressed to specific topics.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new

Construct a Session using the optionally specified
'options'.

See the L<Bloomberg::API::SessionOptions> documentation for details on what
can be specified in the 'options'.

=item start

Attempt to start this Session and blocks until the Session
has started or failed to start. If the Session is started
successfully 'true' is returned, otherwise 'false' is
returned. Before start() returns a SESSION_STATUS Event is
generated. A Session may only be started once.

=item startAsync

Attempt to begin the process to start this Session and
return 'true' if successful, otherwise return 'false'. The
application must monitor events for a SESSION_STATUS Event
which will be generated once the Session has started or if
it fails to start. A Session may only be started once.

=item stop

Stop operations of this session. A SESSION_STATUS Event is
generated by this call. Once a Session has been stopped it
can only be destroyed.

=item stopAsync

Begin the process to stop this Session and return
immediately. The application must monitor events for a
SESSION_STATUS Event which will be generated once the
Session has been stopped. Once a Session has been stopped
it can only be destroyed.

=item nextEvent

Return the next available Event for this session. If there
is no event available this will block for up to the
specified 'timeoutMillis' milliseconds for an Event to
arrive. A value of 0 for 'timeoutMillis' (the default)
indicates nextEvent() should not timeout and will not
return until the next Event is available.

If nextEvent() returns due to a timeout it will return an
event of type 'EventType::TIMEOUT'.

=item tryNextEvent

If there are Events available for the session, return the next Event.
If there is no event available for the session, return undef value.
This method never blocks.

=item registerService

param(uri<"">,
      identity<Bloomberg::API::Identity>, #optional (pass undef)
      {    groupId => "groupId",
           priority=><number>
      } #optional)

Attempt to register the service identified by the specified
'uri' and block until the service is either registered
successfully or has failed to be registered. The optionally
specified 'identity' (can be passed null) is used to verify permissions
to provide the service being registered. The optionally
specified 'options' is used to specify the
group ID and service priority of the service being registered.
Returns 'true' if the service is registered successfully and
'false' if the service cannot be registered successfully.

The 'uri' must begin with a full qualified service
name. That is it must begin with
"//<namespace>/<service-name>[/]". Any portion of the 'uri'
after the service name is ignored.

Before registerService() returns a SERVICE_STATUS Event is
generated. If this is an asynchronous ProviderSession then
this Event may be processed by the registered Event before
registerService() has returned.

=item registerServiceAsync

param(uri<"">,
      identity<Bloomberg::API::Identity>, #optional (pass undef)
      correlationId<Bloomberg::API::CorrelationId> #optional
      {    groupId => "groupId",
           priority=><number>
      } #optional)

Begin the process of registering the service identified by
the specified 'uri' and return immediately. The optionally
specified 'identity' is used to verify permissions
to provide the service being registered. The optionally
specified 'correlationId' is used to track Events generated
as a result of this call. The actual correlationId that will
identify Events generated as a result of this call is
returned. The optionally specified 'registrationOptions' is
used to specify the group ID and service priority of the
service being registered.

The 'uri' must begin with a full qualified service
name. That is it must begin with
"//<namespace>/<service-name>[/]". Any portion of the 'uri'
after the service name is ignored.

The application must monitor events for a SERVICE_STATUS
Event which will be generated once the service has been
successfully registered or registration has failed.

=item resolve

resolve(resolutionList,
        resolveMode = DONT_REGISTER_SERVICES, #optional
        identity = undef #optional)

Resolves the topics in the specified 'resolutionList' and
updates the 'resolutionList' with the results of the
resolution process. If the specified 'resolveMode' is
DONT_REGISTER_SERVICES (the default) then all the services
referenced in the topics in the 'resolutionList' must
already have been registered using registerService(). If
'resolveMode' is AUTO_REGISTER_SERVICES then the specified
'identity' should be supplied and ProviderSession
will automatically attempt to register any services
referenced in the topics in the 'resolutionList' that have
not already been registered. Once resolve() returns
each entry in the 'resolutionList' will have been updated
with a new status.

Before resolve() returns one or more RESOLUTION_STATUS
events and, if 'resolveMode' is AUTO_REGISTER_SERVICES,
zero or more SERVICE_STATUS events are generated.

=item resolveAsync

resolveAsync(Bloomberg::API::ResolutionList,
             resolveMode = DONT_REGISTER_SERVICES, #optional
             Bloomberg::API::Identity = undef #optional)

Begin the resolution of the topics in the specified
'resolutionList'. If the specified 'resolveMode' is
DONT_REGISTER_SERVICES (the default) then all the services
referenced in the topics in the 'resolutionList' must
already have been registered using registerService(). If
'resolveMode' is AUTO_REGISTER_SERVICES then the specified
'providerIdentity' should be supplied and ProviderSession
will automatically attempt to register any services
reference in the topics in the 'resolutionList' that have
not already been registered.

One or more RESOLUTION_STATUS events will be delivered with
the results of the resolution. These events may be
generated before or after resolve() returns. If
AUTO_REGISTER_SERVICES is specified SERVICE_STATUS events
may also be generated before or after resolve() returns.

The 'resolutionList' is NOT be modified with the result.

=item createTopics

# createTopics(Bloomberg::API::TopicList,
               resolveMode = DONT_REGISTER_SERVICES,# optional
               Bloomberg::API::Identity=undef #optional)

Creates the topics in the specified 'topicList' and
updates the 'topicList' with the results of the
creation process. If service needs to be registered,
'identity' should be supplied.
Once a call to this function returns,
each entry in the 'topicList' will have been updated
with a new topic creation status.

Before createTopics() returns one or more RESOLUTION_STATUS
events, zero or more SERVICE_STATUS events, and one or more
TOPIC_STATUS events are generated.

=item createTopicsAsync

createTopicsAsync(Bloomberg::API::TopicList,
                  resolveMode = undef, #optional
                  Bloomberg::API::Identity=undef #optional)

Creates the topics in the specified 'topicList' and
updates the 'topicList' with the results of the
creation process. If service needs to be registered,
'identity' should be supplied.

One or more RESOLUTION_STATUS events, zero or more
SERVICE_STATUS events and one or more TOPIC_STATUS
events are generated.

The 'topicList' will NOT be modified with the result.

=item getTopic(Bloomberg::API::Message)

Finds a previously created Topic object based on the specified
'message'. The 'message' must be one of the following
types: TopicCreated, TopicActivated, TopicDeactivated,
TopicSubscribed, TopicUnsubscribed, TopicRecap.

=item publish(Bloomberg::API::Event)

Publish the specified 'event'.

=item sendResponse(Bloomberg::API::Event,
                   isPartialResponse = false)

Send the response event for previously received request with
the specified 'event' and optionally specified isPartialResponse
flag. By default the isPartialResponse is set to 0 (false).

=back

