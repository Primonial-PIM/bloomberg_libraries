# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Exception;
use Bloomberg::API::Error;
use Bloomberg::API::Internal;
use Bloomberg::API::Element;
use Scalar::Util;

package Bloomberg::API::Request;
our @ISA = qw(Bloomberg::API::Element);

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_Request_destroy($self->{d_request_t});
}

sub asElement
{
    my $self = shift;
    return Bloomberg::API::Element->createInternally($self->{d_element_t});
}

sub createInternally {
    my $class = shift;
    my ($arg) = @_;
    if(!Scalar::Util::blessed($arg)) {
        die("Incorrect Usage!!!");
    }
    $arg->isa("_p_blpapi_Request_t") or die("Can be used Internally only");
    my $element_t = Bloomberg::API::Internal::blpapi_Request_elements(
        $arg);
    my ($self) = {
        d_request_t => $arg,
        d_element_t => $element_t
        };
    bless $self, $class;
    return $self;
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Request - A request which can be sent for a service

=head1 DESCRIPTION

A single request to a single service.

Request objects are created using Service::createRequest() or
Service::createAuthorizationRequest(). They are used with
Session::sendRequest() or Session::sendAuthorizationRequest().

The Request object contains the parameters for a single request
to a single service. Once a Request has been created its fields
can be populated directly using the functions provided by Element
or using the Element interface on the Element returned by asElement().

The schema for the Request can be queried using the Element
interface.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item asElement

Returns the contents of this request as an Bloomberg::API::Element.

=back
