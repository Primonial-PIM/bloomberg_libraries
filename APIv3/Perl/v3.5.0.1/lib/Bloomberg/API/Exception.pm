# Copyright (C) 2010-2012 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::Error;
use Bloomberg::API::Internal;

package Bloomberg::API::Exception;
use overload
    qw("") => \&string;

sub new
{
    my $class = shift;
    my ($type, $description) = @_;

    my $self = {
        d_type        => $type,
        d_description => $description
    };

    bless $self, $class;
    return $self;

}

sub string
{
    my $self = shift;
    return "Bloomberg::API::Exception: ".$self->{d_type}.":\t".$self->{d_description};
}

sub description
{
    my $self = shift;
    return $self->{d_description};
}

sub type
{
    my $self = shift;
    return $self->{d_type};
}

sub DuplicateCorrelationIdException
{
    return "DuplicateCorrelationIdException";
}

sub InvalidStateException
{
    return "InvalidStateException";
}

sub InvalidArgumentException
{
    return "InvalidArgumentException";
}

sub InvalidConversionException
{
    return "InvalidConversionException";
}

sub IndexOutOfRangeException
{
    return "IndexOutOfRangeException";
}

sub FieldNotFoundException
{
    return "FieldNotFoundException";
}

sub UnknownErrorException
{
    return "UnknownErrorException";
}

sub UnsupportedOperationException
{
    return "UnsupportedOperationException";
}

sub throwException
{
    my $self = shift;
    my ($errorCode, $description) = @_;
    if (not defined $description) {
        $description =
            Bloomberg::API::Internal::blpapi_getLastErrorDescription($errorCode)
                                                     if (defined $errorCode);
    }
    if(not defined $description) {
        $description = "Unknown";
    }

    my $classCode = Bloomberg::API::Error::BLPAPI_RESULTCLASS($errorCode) if (defined $errorCode);
    my %actionTest = (
        Bloomberg::API::Error::BLPAPI_ERROR_DUPLICATE_CORRELATIONID => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::DuplicateCorrelationIdException(),
                $description);
                return 1;},
        Bloomberg::API::Error::BLPAPI_INVALIDSTATE_CLASS => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::InvalidStateException(),
                $description);
            return 1;},
        Bloomberg::API::Error::BLPAPI_INVALIDARG_CLASS => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::InvalidArgumentException(),
                $description);
            return 1;},
        Bloomberg::API::Error::BLPAPI_CNVERROR_CLASS => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::InvalidConversionException(),
                $description);
            return 1;},
        Bloomberg::API::Error::BLPAPI_BOUNDSERROR_CLASS => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::IndexOutOfRangeException(),
                $description);
            return 1;},
        Bloomberg::API::Error::BLPAPI_FLDNOTFOUND_CLASS => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::FieldNotFoundException(),
                $description);
            return 1;},
        Bloomberg::API::Error::BLPAPI_UNSUPPORTED_CLASS => sub {
            die Bloomberg::API::Exception->new(
                Bloomberg::API::Exception::UnsupportedOperationException(),
                $description);
            return 1;}
        );
    my $returnVal;
    my $key;

    $returnVal=$actionTest{$classCode} if (defined $classCode);
    if(not defined $returnVal) {
        die Bloomberg::API::Exception->new(
            Bloomberg::API::Exception::UnknownErrorException(),
            $description);
    }
    else {
        &$returnVal();
    }
}

sub throwOnError
{
    return 0 unless ($_[1]);
    my ($class, $rcode) = @_;
    $class->throwException($rcode);
}

my $libraryVersion = Bloomberg::API::Internal::blpapi_internal_getVersionNumber();

# private functions for library use
sub checkVersionAndThrow
{
    my ($class, $versionNumber) = @_;
    if ($libraryVersion < $versionNumber) {
        Bloomberg::API::Exception->throwException(
            Bloomberg::API::Error::BLPAPI_UNSUPPORTED_CLASS,
            "Function Not Supported");
    }
}

# private functions for library use
# return bool
sub checkVersion
{
    my ($class, $versionNumber) = @_;
    if ($libraryVersion < $versionNumber) {
        return 0;
    }
    return 1;
}

1;

=pod

=head1 NAME

Bloomberg::API::Exception - Defines Exceptions that can be thrown by the
    Bloomberg::API library.

=head1 DESCRIPTION

This package defines various exceptions that Bloomberg::API can throw.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

                       Bloomberg::API::Exception: Base class from all exceptions
 Bloomberg::API::DuplicateCorrelationIdException: Duplicate CorrelationId exception
           Bloomberg::API::InvalidStateException: Invalid state exception
        Bloomberg::API::InvalidArgumentException: Invalid argument exception
      Bloomberg::API::InvalidConversionException: Invalid conversion exception
        Bloomberg::API::IndexOutOfRangeException: Index out of range exception
          Bloomberg::API::FieldNotFoundException: Field not found exception
           Bloomberg::API::UnknownErrorException: Unknown error exception
   Bloomberg::API::UnsupportedOperationException: Unsupported operation exception
                   Bloomberg::API::ExceptionUtil: Internal exception generating class

=back

=cut

