# Copyright (C) 2010-2011 Bloomberg Finance L.P.

use strict;
use warnings;

use Bloomberg::API::CorrelationId;
use Bloomberg::API::Exception;
use Bloomberg::API::Internal;

package Bloomberg::API::SubscriptionList;

# we should an array of hashmaps
# [{topic => "", fields => "", correlationId => <>, options=""}]
#
# if using a list variable make sure to pass by reference

sub new
{
    my $class = shift;
    my @subarray = @_;
    my $subList_t = Bloomberg::API::Internal::blpapi_SubscriptionList_create();
    my $self = {
        d_subscriptionList_t => $subList_t
    };
    bless $self, $class;
    $self->add(@subarray);
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_SubscriptionList_destroy(
            $self->{d_subscriptionList_t});
}

# we should an array of hashmaps
# [{topic => "", fields => "", correlationId => <>, options=""}]
#
# if using a list variable make sure to pass by reference

sub add
{
    my $self = shift;
    my ($tempsubarray) = @_;
    return if not defined $tempsubarray;
    my @subarray = @$tempsubarray;
    my @rcodeList = ();
    for my $i (0 .. scalar(@subarray)-1) {
        my $subHash = $subarray[$i];
        next if (!exists $subHash->{topic});
        my $topic  = $subHash->{topic};
        my $fields = "";
        $fields = $subHash->{fields} if (exists $subHash->{fields});
        my $options = "";
        $options = $subHash->{options} if (exists $subHash->{options});
        my $cid;
        if(exists $subHash->{correlationId}) {
            $cid = $subHash->{correlationId}
        }
        else {
            $cid = Bloomberg::API::CorrelationId->new();
        }

        my $rcode = Bloomberg::API::Internal::blpapi_internal_SubscriptionList_add(
            $self->{d_subscriptionList_t},
            $topic,
            $cid->{d_correlationId_t},
            $fields,
            $options);
        push(@rcodeList, $rcode);
    }
    return @rcodeList;
}

# append(Bloomberg::API::SubscriptionList)
sub append
{
    my $self = shift;
    my ($oldList) = @_;
    my $rc = Bloomberg::API::Internal::blpapi_SubscriptionList_append(
        $self->{d_subscriptionList_t},
        $oldList->{d_subscriptionList_t});
    return $rc;
}

sub size
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_SubscriptionList_size(
        $self->{d_subscriptionList_t});
}

sub clear
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_SubscriptionList_clear(
        $self->{d_subscriptionList_t});
}

# get at an index
# return back (topicString, correlationId)
sub getAt
{
    my $self = shift;
    my ($index) = @_;
    my $cid = Bloomberg::API::CorrelationId->new();
    my ($topicString, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_SubscriptionList_topicStringAt(
            $self->{d_subscriptionList_t},
            $index);
    $rcode = Bloomberg::API::Internal::blpapi_SubscriptionList_correlationIdAt(
        $self->{d_subscriptionList_t},
        $cid->{d_correlationId_t},
        $index);

    return ($topicString, $cid);
}

sub correlationIdAt
{
    my $self = shift;
    my ($index) = @_;
    my $cid = Bloomberg::API::CorrelationId->new();
    my $rcode = Bloomberg::API::Internal::blpapi_SubscriptionList_correlationIdAt(
        $self->{d_subscriptionList_t},
        $cid->{d_correlationId_t},
        $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $cid;
}

sub topicStringAt
{
    my $self = shift;
    my ($index) = @_;
    my $cid = Bloomberg::API::CorrelationId->new();
    my ($topicString, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_SubscriptionList_topicStringAt(
            $self->{d_subscriptionList_t},
            $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $topicString;
}

1;

__END__;

=pod

=head1 NAME

Bloomberg::API::SubscriptionList - A list of subscriptions.

=head1 DESCRIPTION

Contains a list of subscriptions used when subscribing and
unsubscribing.

A SubscriptionList is used when calling Session::subscribe(),
Session::resubscribe() and Session::unsubscribe(). The entries
can be constructed in a variety of ways.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new([{topic => "", fields => "", correlationId => <>, options=""}])

Create a SubscriptionList with the optionally provided array added to the list.

=item add([{topic => "", fields => "", correlationId => <>, options=""}])

Add the specified 'topic', with the specified 'fields' and
the specified 'options', to this SubscriptionList,
associating the specified 'correlationId' with it. The
'fields' must be specified as a comma separated list. The
'options' must be specified as a ampersand separated list.


=item append(Bloomberg::API::SubscriptionList)

Append a copy of the specified SubscriptionList to
this SubscriptionList.

=item size

Return the number of subscriptions in this SubscriptionList.

=item clear

Remove all entries from this SubscriptionList.

=item correlationIdAt(index)

Return the CorrelationId of the specified 'index'th entry
in this SubscriptionList. An exception is thrown if
'index'>=size().

=item topicStringAt(index)

Return a string which contains
the full topic string (including any field and option
portions) of the 'index'th entry in this
SubscriptionList. An exception is thrown if 'index'>=size().

=item getAt(index)

Return the topic string and CorrelationId of the specified 'index'th entry
in this SubscriptionList. An exception is thrown if
'index'>=size().

=back

=cut

