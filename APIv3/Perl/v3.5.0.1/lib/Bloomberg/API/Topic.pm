# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::Topic;

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::Topic;

use overload
    "==" => \&equal,
    "!=" => \&nequal,
    "<"  => \&lessthan;

sub createInternally
{
    my $class = shift;
    my ($topic_t) = @_;
    $topic_t->isa("_p_blpapi_Topic_t")
        or die("Can be created Internally only");
    my $self = {
        d_topic_t => $topic_t,
        d_isShared_clone => 0
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    if(!$self->{d_isShared_clone}) {
        Bloomberg::API::Internal::blpapi_Topic_destroy($self->{d_topic_t});
    }
}

sub clone
{
    my $self = shift;
    my $topic_t = Bloomberg::API::Internal::blpapi_Topic_create($self->{d_topic_t});
    my $copy = {
        d_topic_t => $topic_t
    };
    bless $copy;
    return $copy;
}

sub isActive
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_Topic_isActive($self->{d_topic_t});
}

sub service
{
    my $self = shift;
    my $service_t = Bloomberg::API::Internal::blpapi_Topic_service($self->{d_topic_t});
    return Bloomberg::API::Service->createInternally($service_t);
}

sub impl
{
    my $self = shift;
    return $self->{d_topic_t};
}

sub equal
{
    my ($lhs, $rhs) = @_;
    return 0 == Bloomberg::API::Internal::blpapi_Topic_compare($lhs->impl(),
            $rhs->impl());
}

sub nequal
{
    my ($lhs, $rhs) = @_;
    return 0 != Bloomberg::API::Internal::blpapi_Topic_compare($lhs->impl(),
            $rhs->impl());

}

sub lessthan
{
    my ($lhs, $rhs) = @_;
    return 0 > Bloomberg::API::Internal::blpapi_Topic_compare($lhs->impl(),
            $rhs->impl());

}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::Topic - Used to identify the stream on which a message is published.

=head1 DESCRIPTION

Topic objects are obtained from createTopic() on ProviderSession. They are used
when adding a message to an Event for publishing using appendMessage() on
EventFormatter.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item clone

  clone($original)

Create a copy of the specified 'original' Topic.

=item isActive

Returns true if this topic was elected by the platform to become the primary
publisher.

=item service

Returns the service for which this topic was created.

=item == / equal

Return true if the two topics have the same value, false otherwise.

=item != / nequal

Return true if the two topics do not have the same value, false otherwise.

=item < / lessthan

Return true if the left topic is less than the right topic, false otherwise.

=back
