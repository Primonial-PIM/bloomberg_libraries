# Copyright (C) 2010-2011 Bloomberg Finance L.P.

package Bloomberg::API::TopicList;

use strict;
use warnings;

use Bloomberg::API::Internal;
use Bloomberg::API::CorrelationId;
use Bloomberg::API::Exception;
use Bloomberg::API::Message;

use Scalar::Util;

use constant NOT_CREATE => 0;
use constant CREATED => 1;
use constant FAILURE => 2;

sub new
{
    my $class = shift;
    my $topicList_t = Bloomberg::API::Internal::blpapi_TopicList_create(undef);
    my $self = {
        d_topicList_t => $topicList_t
    };
    bless $self, $class;
    return $self;
}

sub DESTROY
{
    my $self = shift;
    Bloomberg::API::Internal::blpapi_TopicList_destroy($self->{d_topicList_t});
}

sub clone
{
    my $self = shift;
    my $topicList_t =
        Bloomberg::API::Internal::blpapi_TopicList_create($self->{d_topicList_t});
    my $copy = {
        d_topicList_t => $topicList_t
    };
    bless $copy;
    return $copy;
}

sub add
{
    my ($self, $secondArg, $cid) = @_;
    if (not defined $cid) {
        $cid = Bloomberg::API::CorrelationId->new();
    }
    my $argClass = Scalar::Util::blessed($secondArg);
    if ((defined $argClass) && ($secondArg->isa("Bloomberg::API::Message"))) {
        Bloomberg::API::Internal::blpapi_TopicList_addFromMessage(
                $self->{d_topicList_t},
                $secondArg->impl(),
                $cid->impl());
    }
    else {
        return Bloomberg::API::Internal::blpapi_TopicList_add(
                $self->{d_topicList_t},
                $secondArg."",
                $cid->impl());
    }
}

sub correlationIdAt
{
    my $self = shift;
    my ($index) = @_;
    my $cid_t = Bloomberg::API::Internal::blpapi_CorrelationId_t->new();
    my $rcode =
        Bloomberg::API::Internal::blpapi_TopicList_correlationIdAt($self->{d_topicList_t},
                $cid_t, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::CorrelationId->createInternally($cid_t);
}

sub topicString
{
    my $self = shift;
    my ($cid) = @_;
    my ($topic, $rcode) = Bloomberg::API::Internal::blpapi_internal_TopicList_topicString(
            $self->{d_topicList_t},
            $cid->impl());
    Bloomberg::API::Exception->throwOnError($rcode);
    return $topic;
}

sub topicStringAt
{
    my $self = shift;
    my ($index) = @_;
    my ($topic, $rcode) = Bloomberg::API::Internal::blpapi_internal_TopicList_topicStringAt(
                $self->{d_topicList_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return $topic;
}

sub status
{
    my $self = shift;
    my ($cid) = @_;
    my ($result, $rcode) = Bloomberg::API::Internal::blpapi_internal_TopicList_status(
            $self->{d_topicList_t}, $cid->impl());
    Bloomberg::API::Exception->throwOnError($rcode);
    return $result;
}

sub statusAt
{
    my $self = shift;
    my ($index) = @_;
    my ($result, $rcode) = Bloomberg::API::Internal::blpapi_internal_TopicList_statusAt(
            $self->{d_topicList_t}, $index); 
    Bloomberg::API::Exception->throwOnError($rcode);
    return $result;
}

sub message
{
    my $self = shift;
    my ($cid) = @_;
    my ($message, $rcode) =
        Bloomberg::API::Internal::blpapi_internal_TopicList_message(
                $self->{d_topicList_t}, $cid->impl());
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Message->createInternally($message);
}

sub messageAt
{
    my $self = shift;
    my ($index) = @_;
    my ($message, $rcode) = Bloomberg::API::Internal::blpapi_internal_TopicList_messageAt(
            $self->{d_topicList_t}, $index);
    Bloomberg::API::Exception->throwOnError($rcode);
    return Bloomberg::API::Message->createInternally($message);
}

sub size
{
    my $self = shift;
    return Bloomberg::API::Internal::blpapi_TopicList_size($self->{d_topicList_t});
}

sub impl
{
    my $self = shift;
    return $self->{d_topicList_t};
}

1;

__END__

=pod

=head1 NAME

Bloomberg::API::TopicList - Contains a list of topics which require creation.

=head1 DESCRIPTION

Created from topic strings or from TOPIC_SUBSCRIBED or RESOLUTION_SUCCESS
messages. This is passed to a createTopics() call or createTopicsAsync()
call on a ProviderSession. It is updated and returned by the createTopics()
call.

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item new

Create an empty TopicList.

=item clone

Copy constructor.

=item add

  add($topic, $correlationId)
  add($message, $correlationId)

Add the specified 'topic' of type string or the topic contained in the
specified 'message' of type Bloomberg::API::Message ('topicSubscribedMessage' or
'resolutionSuccessMessage') to this list, optionally specifying a
'correlationId'. Returns 0 on success or negative number on failure.
After a successful call to add(), the status for this entry is NOT_CREATED.

=item correlationIdAt

  correlationIdAt($index)

Returns the CorrelationId of the specified 'index'th entry in this TopicList.
If 'index' >= size() an exception is thrown.

=item topicString

  topicString($correlationId)

Returns a string for the topic of the entry identified by the specified
'correlationId'. If the 'correlationId' does not identify an entry in this
TopicList then an exception is thrown.

=item topicStringAt

  topicStringAt($index)

Returns a string for the topic of the specified 'index'th entry. If
'index' >= size() an exception is thrown.

=item status

  status($correlationId)

Returns the status of the entry in this TopicList identified by the specified
'correlationId'. This may be NOT_CREATED, CREATED, and FAILURE. If the
'correlationId' does not identify an entry in this TopicList then an exception
is thrown.

=item statusAt

  statusAt($index)

Returns the status of the specified 'index'th entry in this TopicList. This may
be NOT_CREATED, CREATED, and FAILURE. If 'index' > size() an exception
is thrown.

=item message

  message($correlationId)

Returns the value of the message received during creation of the topic
identified by the specified 'correlationId'. If 'correlationId' does not
identify an entry in this TopicList or if the status of the entry
identify by 'correlationId' is not CREATED an exception is thrown.

The message returned can be used when creating an instance of Topic.

=item messageAt

  messageAt($index)

Returns the value of the message received during creation of the specified
'index'th entry in this TopicList. If 'index' >= size() or if the status of the
'index'th entry is not CREATED an exception is thrown.

The message returned can be used when creating an instance of Topic.

=item size

Returns the number of entries in this list.

=back
