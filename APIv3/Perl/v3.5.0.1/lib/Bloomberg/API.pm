use strict;
use warnings;

package Bloomberg::API;

our $VERSION = '3.5.0.1';

1;

__END__

=pod

=head1 NAME

Bloomberg::API - Version information for Bloomberg::API

=head1 DESCRIPTION

Version information for Bloomberg::API

=head1 SUPPORTED OPERATIONS

The following operations are supported:

=over 4

=item VERSION

A string which contains the version number associated with the current sdk.
